<?php
 session_start();

 if (!isset($_SESSION['userID'])) {
    header("Location: index.html");
 }
 include('header.html');
 ?>
 <div class="card-body">
  <div class="row">
    <div class="col-sm-6 col-md-6 col-lg-3">
        <div class="flip">
            <div class="widget-bg-color-icon card-box front">
                <div class="bg-icon pull-left">
                    <i class="ti-money text-warning"></i>
                </div>
                <div class="text-right">
                    <h3 class="text-dark"><b>$ 3752</b></h3>
                    <p>Today's Deposit</p>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="widget-bg-color-icon card-box back">
                <div class="text-center">
                    <span id="loadspark-chart"></span>
                    <hr>
                    <p>Check summary</p>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-6 col-lg-3">
        <div class="flip">
            <div class="widget-bg-color-icon card-box front">
                <div class="bg-icon pull-left">
                    <i class="ti-alarm-clock text-success"></i>
                </div>
                <div class="text-right">
                    <h3><b id="widget_count3">3251</b></h3>
                    <p>Rendered Hours</p>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="widget-bg-color-icon card-box back">
                <div class="text-center">
                    <span class="linechart" id="salesspark-chart"></span>
                    <hr>
                    <p>Check summary</p>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>

    <div class="col-sm-6 col-md-6 col-lg-3">
        <div class="flip">
            <div class="widget-bg-color-icon card-box front">
                <div class="bg-icon pull-left">
                    <i class="ti-calendar text-danger"></i>
                </div>
                <div class="text-right">
                    <h3 class="text-dark"><b>60</b></h3>
                    <p>AR Days</p>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="widget-bg-color-icon card-box back">
                <div class="text-center">
                    <span id="visitsspark-chart"></span>
                    <hr>
                    <p>Check summary</p>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-6 col-lg-3">
        <div class="flip">
            <div class="widget-bg-color-icon card-box front">
                <div class="bg-icon pull-left">
                    <i class="ti-user text-info"></i>
                </div>
                <div class="text-right">
                    <h3 class="text-dark"><b>10</b></h3>
                    <p>Clients</p>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="widget-bg-color-icon card-box back">
                <div class="text-center">
                    <span id="subscribers-chart"></span>
                    <hr>
                    <p>Check summary</p>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <div class="col-sm-5">
        <div class="panel">
            <div class="swiper-container swiper_news">
                <div class="swiper-wrapper">
                    <div class="swiper-slide slide-1 gradient-color">
                        <div class="slider-content">
                            <div class="news-head">
                                <h3>How ABA Came to Be the Only Scientifically-Proven Method for Treating ASD</h3>
                                <span class="pull-right">Yesterday</span>
                                <hr>
                            </div>
                            <div class="news-cont">
                                <h4>There are many fears confronting a parent when they learn their child has autism. And each stage of development brings its own set of challenges and new concerns. When a parent first receives the diagnosis, the initial weeks and months ...</h4>
                                <p class="text-right read-more"><a class="read-more" href="javascript:void(0)">Read
                                    more <i class="ti-angle-double-right"></i></a></p>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide slide-2 gradient-color">
                        <div class="slider-content">
                            <div class="news-head">
                                <h3>Can Psychedelics Help ABAs Conquer Drug Addictions?</h3>
                                <span class="pull-right">5min ago</span>
                                <hr>
                            </div>
                            <div class="news-cont">
                                <h4>In a calm, comfortably furnished room that looks like someone’s living room, a patient with eyeshades on lays down on the couch. Sitting next to them, a professional dressed casually in street clothes hands them a small cup. Inside the ...</h4>
                                <p class="text-right read-more"><a class="read-more" href="javascript:void(0)">Read
                                    more <i class="ti-angle-double-right"></i></a></p>

                            </div>
                        </div>
                    </div>
                    <!-- <div class="swiper-slide slide-3 gradient-color">
                        <div class="slider-content">
                            <div class="news-head">
                                <h3>First ever Largest open Air Purifier</h3>
                                <span class="pull-right">On 28th Oct</span>
                                <hr>
                            </div>
                            <div class="news-cont">
                                <h4>The strategy of adjusting and optimizing energy, using systems
                                    and
                                    procedures so as to reduce energy requirements per unit of
                                    output
                                    while holding ...</h4>
                                <p class="text-right read-more"><a class="read-more" href="javascript:void(0)">Read
                                    more <i class="ti-angle-double-right"></i></a></p>
                            </div>
                        </div>
                    </div> -->
                </div>

            </div>
        </div>
    </div>
    <div class="col-sm-7">
        <div class="panel real-timechart">
            <div class="panel-body">
                <div class="row" style="padding:1rem">
                    <div class="col-xs-6 text-center" style="padding:1rem">
                        <h3 class="">Real-Time Visits</h3>
                        <div class="real-value"><p><span></span>k</p></div>
                    </div>
                    <div class="col-xs-6 text-center" style="padding:1rem">
                        <h3 class="">Returning Visitors</h3>
                        <div class="return-value"><p><span></span>k</p></div>
                    </div>
                </div>
                <div id="realtime-views" class="real-chart"></div>
                <hr>
                <div class="row ratings" style="padding:1rem">
                    <div class="col-xs-4 text-center" style="padding:1rem">
                        <h4>81%</h4>
                        <p>Satisfied</p>
                    </div>
                    <div class="col-xs-4 text-center" style="padding:1rem">
                        <h4>8%</h4>
                        <p>Unsatisfied</p>
                    </div>
                    <div class="col-xs-4 text-center" style="padding:1rem">
                        <h4>11%</h4>
                        <p>NA</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>
</div>
<?php
    include('footer.html');
?>
 <script type="text/javascript" src="js/dashboard1.js"></script>
        <script type="text/javascript" src="js/custom_js/sparkline/jquery.flot.spline.js"></script>
 
<script type="text/javascript" src="vendors/flip/js/jquery.flip.min.js"></script>
<script type="text/javascript" src="vendors/lcswitch/js/lc_switch.min.js"></script>
<script type="text/javascript" src="vendors/flotchart/js/jquery.flot.js"></script>
<script type="text/javascript" src="vendors/flotchart/js/jquery.flot.resize.js"></script>
<script type="text/javascript" src="vendors/flotchart/js/jquery.flot.stack.js"></script>
<script type="text/javascript" src="vendors/flotspline/js/jquery.flot.spline.min.js"></script>
<script type="text/javascript" src="vendors/flot.tooltip/js/jquery.flot.tooltip.js"></script>
<!--swiper-->
<script type="text/javascript" src="vendors/swiper/js/swiper.min.js"></script>
<script src="vendors/chartjs/js/Chart.js"></script>
<!--nvd3 chart-->
<script type="text/javascript" src="js/nvd3/d3.v3.min.js"></script>
<script type="text/javascript" src="vendors/nvd3/js/nv.d3.min.js"></script>
<script type="text/javascript" src="vendors/moment/js/moment.min.js"></script>
<script type="text/javascript" src="vendors/advanced_newsTicker/js/newsTicker.js"></script>
