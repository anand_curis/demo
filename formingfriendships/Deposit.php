<?php
 session_start();

 if (!isset($_SESSION['userID'])) {
 	header("Location: index.html");
 }


if($_SESSION['uName'] == "chaks"){
    header("Location: index.html");
}

    include("header.html");
 ?>
 
    <div class="row">
    	<h2 style="color: #251367; margin-left:20px;">Deposit</h2>
    	<div class="col-md-12" style="margin-top:20px;">
        <div class="widget box" >
            <div class="widget-content">
            	<a href="javascript:void(0)" id="linkAddDep" class="btn btn-primary" style="text-decoration:none"><i class="fa fa-plus"></i>&nbsp;&nbsp;Add Deposit</a>
            	<a href="javascript:void(0)" id="uploadERA" class="btn btn-primary" style="text-decoration:none"><i class="fa fa-plus"></i>&nbsp;&nbsp;Upload ERA</a>
            	<form action="EditPatient.php" method="post">
    				<table id="deposit" class="table table-striped table-bordered table-hover table-checkable" cellspacing="0" style="overflow-x:scroll" data-horizontal-width="150%">
    				</table>
    			</form>
                <table id="depositDetails" class="table table-striped table-bordered table-hover table-checkable" cellspacing="0" style="overflow-x:scroll" data-horizontal-width="150%">
                </table>
            </div>
        </div>

    </div>
	<div class="modal fade" id="AddDeposit" tabindex="-1">
		<div class="modal-dialog modal-lg" style="width:75%">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Add New Deposit</h4>
				</div>
				<div class="modal-body" style="min-height:380px;">
					<div class="card-body">
						<div class="row">
							<div class="form-group col-md-6">
		                        <label class="control-label col-md-12">Select Payor Type </label>
		                        <div class="col-md-12">
		                            <select class="form-control" id="payorType">
		                            	<option value="0" selected></option>
		                            	<option value="1">Payor</option>
		                            	<option value="2">Client</option>
		                            </select>
		                        </div>
		                    </div>
		                    <div class="form-group col-md-6" id="divPayorType">
		                        <label class="control-label col-md-12" id="lblType">Payor </label>
		                        <div class="col-md-12">
		                            <input type="text" id="payor" class="form-control" />
		                        </div>
		                    </div>
		                    <div class="form-group col-md-6">
		                        <label class="control-label col-md-12">Deposit Date </label>
		                        <div class="col-md-12">
		                            <input type="text" id="depositDate" class="form-control" />
		                        </div>
		                    </div>
		                    <div class="form-group col-md-6">
		                        <label class="control-label col-md-12" id="lblType1">Payment Method </label>
		                        <div class="col-md-12">
		                            <select class="form-control" id="paymentType">
		                            	<option>Check</option>
		                            	<option>EFT</option>
		                            	<option>Credit Card</option>
		                            	<option>Cash</option>
		                            </select>
		                        </div>
		                    </div>
		                    <div class="form-group col-md-4">
		                        <label class="control-label col-md-12">Check # </label>
		                        <div class="col-md-12">
		                            <input type="text" id="instrumentNo" class="form-control" />
		                        </div>
		                    </div>
		                    <div class="form-group col-md-4">
		                        <label class="control-label col-md-12">Check Date </label>
		                        <div class="col-md-12">
		                            <input type="text" id="instrumentDate" class="form-control" />
		                        </div>
		                    </div>
		                    <div class="form-group col-md-4">
		                        <label class="control-label col-md-12" id="lblType">Amount </label>
		                        <div class="col-md-12">
		                            <input type="text" id="amount" class="form-control" />
		                        </div>
		                    </div>
		                    <div class="form-group col-md-8">
		                        <label class="control-label col-md-12" id="lblType">Notes </label>
		                        <div class="col-md-12">
		                            <textarea class="form-control" id="notes" cols="5" rows="10"></textarea>
		                        </div>
		                    </div>
		                </div>
		            </div>
				</div>
				<div class="modal-footer">
					<input type="button" class="btn btn-default" data-dismiss="modal" value="Exit" />
					<input type="button" id="btnSaveDep" class="btn btn-primary" data-dismiss="modal" value="Save" />
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div>
	<div class="modal fade" id="EditDeposit" tabindex="-1">
		<div class="modal-dialog modal-lg" style="width:75%">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Edit Deposit</h4>
				</div>
				<div class="modal-body" style="min-height:280px;">
					<div class="card-body">
						<div class="row">
							<input type="hidden" id="_hdnDepID"/>
							<div class="form-group col-md-6">
		                        <label class="control-label col-md-12">Select Payor Type </label>
		                        <div class="col-md-6">
		                            <select class="form-control" id="EditpayorType">
		                            	<option value="0"></option>
		                            	<option value="1">Payor</option>
		                            	<option value="2">Client</option>
		                            </select>
		                        </div>
		                    </div>
		                    <div class="form-group col-md-6" id="divPayorType">
		                        <label class="control-label col-md-12" id="lblType">Payor </label>
		                        <div class="col-md-6">
		                            <select class="form-control" id="Editpayor">
		                            </select>
		                        </div>
		                    </div>
		                    <div class="form-group col-md-6">
		                        <label class="control-label col-md-12">Deposit Date </label>
		                        <div class="col-md-6">
		                            <input type="text" id="EditdepositDate" class="form-control" />
		                        </div>
		                    </div>
		                    <div class="form-group col-md-6">
		                        <label class="control-label col-md-12" id="lblType">Payment Method </label>
		                        <div class="col-md-6">
		                            <select class="form-control" id="EditpaymentType">
		                            	<option>Check</option>
		                            	<option>EFT</option>
		                            	<option>Credit Card</option>
		                            	<option>Cash</option>
		                            </select>
		                        </div>
		                    </div>
		                    <div class="form-group col-md-4">
		                        <label class="control-label col-md-12">Check # </label>
		                        <div class="col-md-6">
		                            <input type="text" id="EditinstrumentNo" class="form-control" />
		                        </div>
		                    </div>
		                    <div class="form-group col-md-4">
		                        <label class="control-label col-md-12">Check Date </label>
		                        <div class="col-md-6">
		                            <input type="text" id="EditinstrumentDate" class="form-control" />
		                        </div>
		                    </div>
		                    <div class="form-group col-md-4">
		                        <label class="control-label col-md-12" id="lblType">Amount </label>
		                        <div class="col-md-6">
		                            <input type="text" id="Editamount" class="form-control" />
		                        </div>
		                    </div>
		                    <div class="form-group col-md-8">
		                        <label class="control-label col-md-12" id="lblType">Notes </label>
		                        <div class="col-md-10">
		                            <textarea class="form-control" id="Editnotes" col="5" row="6"></textarea>
		                        </div>
		                    </div>
		                </div>
		            </div>
				</div>
				<div class="modal-footer">
					<input type="button" class="btn btn-default" data-dismiss="modal" value="Exit" />
					<input type="button" id="btnEditDep" class="btn btn-primary" data-dismiss="modal" value="Save" />
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div>
    <?php
        include("footer.html");
    ?>
	<link href="https://cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css" rel='stylesheet' type='text/css'>
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
	<script>


	$(document).ready(function(){
		$(".ti-menu").click();
		$('#divPayorType').hide();
		var practiceId = sessionStorage.getItem('practiceId');
        var temp = sessionStorage.getItem("patientId");
        $(document).on('focusout','#amount',function() {
            var amount = $(this).val();
            amount = amount;
            document.getElementById("amount").value = amount;
        });
        $(document).on('focus','#payor',function() {
            var ptype = $("#payorType").val();
            if(ptype=="2"){
                var patientList = <?php include('getPat1.php') ?>;
                $("#payor").autocomplete({
                    source: patientList,
                    autoFocus:true
                });
            }
            
        });
        $('#depositDate').datetimepicker({
	        dayOfWeekStart: 1,
	        lang: 'en',
	        format: 'm-d-Y',
	        timepicker: false,
	        //minDate: '-2013/01/02',
	        //maxDate: '+2014/12/31',
	        formatDate: 'm-d-Y',
	        closeOnDateSelect: true
	    });
	    $('#depositDate').mask('00-00-0000');
	    
	    $('#instrumentDate').datetimepicker({
	        dayOfWeekStart: 1,
	        lang: 'en',
	        format: 'm-d-Y',
	        timepicker: false,
	        //minDate: '-2013/01/02',
	        //maxDate: '+2014/12/31',
	        formatDate: 'm-d-Y',
	        closeOnDateSelect: true
	    });
	    $('#instrumentDate').mask('00-00-0000');

		$('#btnSaveDep').click(function(){
			var payorType = $('#payorType').val();
			var payor = $('#payor').val();
			var depositDate = changeDate($('#depositDate').val());
			var paymentType = $('#paymentType').val();
			var instrumentNo = $('#instrumentNo').val();
			var instrumentDate = changeDate($('#instrumentDate').val());
			var amount = $('#amount').val();
			var notes = $('#notes').val();
			$.post("https://curismed.com/medService/deposits/create",
		    {
		        practiceID: practiceId,
		        chequeNo : instrumentNo,
		        chequeDate : instrumentDate,
		        payorType :payorType,
		        paymentType :paymentType,
		        payorID : payor,
		        amount : amount,
		        depositDate : depositDate,
		        notes : notes
		    },
		    function(data1, status){
		    	//alert(JSON.stringify(data1));
		    	window.location.reload();
			});
		});
		$('#btnEditDep').click(function(){
			var depositID = document.getElementById('_hdnDepID').value;
			var payorType = $('#EditpayorType').val();
			var payor = $('#Editpayor').val();
			var depositDate = changeDate($('#EditdepositDate').val());
			var paymentType = $('#EditpaymentType').val();
			var instrumentNo = $('#EditinstrumentNo').val();
			var instrumentDate = changeDate($('#EditinstrumentDate').val());
			var amount = $('#Editamount').val();
			var notes = $('#Editnotes').val();
			$.post("updateDeposit.php",
		    {
		    	depositID : depositID,
		        chequeNo : instrumentNo,
		        chequeDate : instrumentDate,
		        payorType :payorType,
		        paymentType :paymentType,
		        payorID : payor,
		        amount : amount,
		        depositDate : depositDate,
		        notes : notes
		    },
		    function(data1, status){
		    	//alert(JSON.stringify(data1));
		    	window.location.reload();
			});
		});
		$('#payorType').change(function(){
			//alert($(this).val());
			if($(this).val() == '1'){
				$('#divPayorType').show();
				document.getElementById('lblType').innerHTML = 'Payor';
				populatePayor();
			}
			else if($(this).val() == '2'){
				$('#divPayorType').show();
				document.getElementById('lblType').innerHTML = 'Client';
				
			}
		});
		$("#linkAddDep").click(function(){
	    	$('#AddDeposit').modal('show');
	    });
		$('#loader').show();
		var pracID = sessionStorage.getItem('practiceId');
		loadDep(pracID);
	});

	function populatePayor(){
        var insurances = <?php include('insuranceList1.php') ?>;
		//alert(data);
		// var list = [];
  //       $('#payor').html('');
  //       for(var x in data){
		//   list.push(data[x]);
		// }
		// //alert(list);
		// $.each(list,function(i,v) {
		// 	$('#payor').html('');
		// 	list.forEach(function(t) { 
	 //            $('#payor').append('<option value="'+t.insuranceID+'">'+t.insuranceID +' - '+t.payerName+'</option>');
	 //        });
		// });
            $("#payor").autocomplete({
            source: insurances,
            autoFocus:true
        });
	}
	function populateEditPayor(){
		var data = <?php include('insuranceListwithID.php'); ?>;
		//alert(data);
		var list = [];
        $('#Editpayor').html('');
        for(var x in data){
		  list.push(data[x]);
		}
		//alert(list);
		$.each(list,function(i,v) {
			$('#Editpayor').html('');
			list.forEach(function(t) { 
	            $('#Editpayor').append('<option value="'+t.insuranceID+'">'+t.insuranceID +' - '+t.payerName+'</option>');
	        });
		});
	}

	function loadDep(pracID){
		$.post("https://curismed.com/medService/deposits",
		    {
		        practiceID: pracID
		    },
		    function(data1, status){
				var dt = [];
				$.each(data1,function(i,v) {
                    var payerName = "";
					if(data1[i].Balance == null){
						data1[i].Balance = data1[i].amount;
					}
					if(data1[i].payorType == '1'){
						// data1[i].payorType = "Payor";
                        payerName = data1[i].payerName;
					}
					else{
						// data1[i].payorType = "Client";
                        $.ajax({
                          type: "POST",
                          url:"getPatName.php",
                          async : false,
                          data:{
                            patientID : data1[i].payorID
                          },success:function(result){
                            var res = JSON.parse(result);
                            payerName = res[0].fullName;
                          }
                      });
					}

					dt.push([data1[i].depositID,data1[i].depositDate,data1[i].chequeNo,data1[i].chequeDate,payerName,data1[i].payorType,data1[i].amount,data1[i].Balance,data1[i].paymentType,data1[i].notes,data1[i].notes,data1[i].payorID]);
				});
				var table = $('#deposit').DataTable({
		        "data": dt,
		        "bPaginate": false,
                "bDestroy": true,
		        "bProcessing": true,
		         "aoColumns": [
		         	{"mdata": "depositID","title":"Deposit ID", visible:false},
		            {"title":"Deposit Date","mdata": "Date",
		            	"render": function ( data, type, full, meta ) {
					      return changeDateFormat(data);
					    }
					},
		            {"title":"Cheque No","mdata": "Cheque No"},
		            {"title":"Cheque Date","mdata": "Cheque Date",
		            	"render": function ( data, type, full, meta ) {
					      return changeDateFormat(data);
					    }
					},
		            {"title":"Payor Name","mdata": "payorName"},
		            {"title":"Payor Type","mdata": "Payor Type", visible:false},
		            {"title":"Payment","mdata": "Payment",
		            	"render": function ( data, type, full, meta ) {
					      return '$'+data;
					    }
					},
		            {"title":"UnApplied","mdata": "Balance",
		            	"render": function ( data, type, full, meta ) {
					      return '$'+data;
					    }
					},
		            {"title":"Payment Method","mdata": "Payment Method"},
		            {"title":"Description","mdata": "Description"},
		            {
		            	"title":"Actions",
		            	"mdata": "Actions",
		            	//mRender: function (data, type, row) { return '<a href="javascript:void(0);" onclick="apply('+row[0]+','+row[11]+','+row[5]+','+row[6]+')">Apply</a>&nbsp;|&nbsp;<a href="javascript:void(0);" onclick="viewDepDetails('+row[0]+')">View Details</a>&nbsp;|&nbsp;<a href="javascript:void(0);" onclick="editDep('+row[0]+')"><i class="fa fa-edit"></i></a>&nbsp;|&nbsp;<a href="javascript:void(0);" onclick="deleteDep('+row[0]+')"><i class="fa fa-trash"></i></a>&nbsp;|&nbsp;<a href="javascript:void(0);" onclick="uploadDoc('+row[0]+')">Upload</a>'; }
		            	mRender: function (data, type, row) { return '<a href="javascript:void(0);" onclick="apply('+row[0]+','+row[11]+','+row[5]+','+row[6]+')">Apply</a>&nbsp;|&nbsp;<a href="javascript:void(0);" onclick="viewDepDetails('+row[0]+')">View Details</a>&nbsp;|&nbsp;<a href="javascript:void(0);" onclick="editDep('+row[0]+')"><i class="fa fa-edit"></i></a>&nbsp;|&nbsp;<a href="javascript:void(0);" onclick="deleteDep('+row[0]+')"><i class="fa fa-trash"></i></a>'; }
		        	},
		        	{"title":"payorID","mdata": "payorID", "visible": false}
			    ]
		        });
		        $('#loader').hide();
		    });
	}

    function uploadDoc(id){
        var depositID = id;
        sessionStorage.setItem("deposID",depositID);
        window.location.href="depUpload.php";
    }

    function viewDepDetails(id){
        $.ajax({
          type: "POST",
          url:"https://curismed.com/medService/claims/listbydeposit",
          data:{
            depositID : id
          },success:function(result){
            $('#depositDetails').show();
            var data1 = result;
            var dt = [];
            $.each(data1,function(i,v) {
                var currPayer = "";
                if(data1[i].adjustmentNotes == "1"){
                    data1[i].adjustmentNotes = "Patient Responsibility";
                }
                else if(data1[i].adjustmentNotes == "2"){
                    data1[i].adjustmentNotes = "Bill to Next Responsibility";
                }
                else{
                    data1[i].adjustmentNotes = "Closed";
                }
                if(data1[i].adjustmentCode == "" || data1[i].adjustmentCode==null || data1[i].adjustmentCode == undefined){
                	data1[i].adjustmentCode == "-";
                }
                $.ajax({
                      type: "POST",
                      url:"getInsbyClaimID.php",
                      async :false,
                      data:{
                        claimID : data1[i].claimID
                      },success:function(result){
                        currPayer = result;
                      }
                  });
                dt.push([id,data1[i].claimID,data1[i].fullName,data1[i].toDt,data1[i].proced,data1[i].total,data1[i].allowed,data1[i].paid,data1[i].adjustment,data1[i].claimBalance,data1[i].adjustmentCode,data1[i].adjustmentNotes,currPayer]);
            });
            var table = $('#depositDetails').DataTable({                
            "data": dt,
            "bPaginate": false,
            "bDestroy": true,
            "bProcessing": true,
             "aoColumns": [
                {"mdata": "depositID","title":"Deposit ID", visible:false},
                {"mdata": "claimID","title":"Claim ID", visible:false},
                {"title":"Client","mdata": "fromDt",
                    "render": function ( data, type, full, meta ) {
                      return data;
                    }
                },
                {"title":"DOS","mdata": "toDt",
                    "render": function ( data, type, full, meta ) {
                      return changeDateFormat(data);
                    }
                },
                {"title":"CPT","mdata": "proced",
                    "render": function ( data, type, full, meta ) {
                      return data;
                    }
                },
                {"title":"Total","mdata": "total",
                    "render": function ( data, type, full, meta ) {
                      return '$'+data;
                    }
                },
                {"title":"Allowed","mdata": "allowed",
                    "render": function ( data, type, full, meta ) {
                      return '$'+data;
                    }
                },
                {"title":"Paid","mdata": "paid",
                    "render": function ( data, type, full, meta ) {
                      return '$'+data;
                    }
                },
                {"title":"Adjustment","mdata": "adjustment",
                    "render": function ( data, type, full, meta ) {
                      return '$'+data;
                    }
                },
                {"title":"Balance","mdata": "claimBalance",
                    "render": function ( data, type, full, meta ) {
                      return '$'+data;
                    }
                },
                {"title":"Reason","mdata": "reason"},
                {"title":"Status","mdata": "status"},
                {"title":"Insurance","mdata": "insuranceID"}
            ]
            });
         }
      });
    }

	function editDep(id){
		$.ajax({
          type: "POST",
          url:"getDepInfo.php",
          async : false,
          data:{
            depositID : id
          },success:function(result){
            $('#EditDeposit').modal('show');
            populateEditPayor();
            var res = JSON.parse(result);
            if(res[0].payorType == '1'){
            	var ID = res[0].payorID;
	            $("#EditpayorType").val(res[0].payorType);
	            $.ajax({
                    type: "POST",
                    url:"getInsuranceName.php",
                    async: false,
                    data:{
                        "insuranceID" : res[0].payorID
                        },success:function(result){
                            var res = JSON.parse(result);
                            $("#Editpayor").val(ID);
                    }
                });
	        }
	        else{
                $.ajax({
                    type: "POST",
                    url:"getPatName.php",
                    async: false,
                    data:{
                        "patientID" : res[0].payorID
                        },success:function(result){
                            var res = JSON.parse(result);
                            $("#Editpayor").val(res[0].patientID+" - "+res[0].fullName);
                    }
                });
	        	$("#EditpayorType").val("2");
                document.getElementById('lblType').innerHTML = "Client";
	        }
	    	document.getElementById('EditpaymentType').value = res[0].paymentType;
	    	document.getElementById('EditinstrumentNo').value = res[0].chequeNo;
	    	document.getElementById('EditinstrumentDate').value = changeDateFormat(res[0].chequeDate);
	    	document.getElementById('Editamount').value = res[0].amount;
	    	document.getElementById('Editnotes').value = res[0].notes;
	    	document.getElementById('EditdepositDate').value = changeDateFormat(res[0].depositDate);
	    	document.getElementById('_hdnDepID').value = res[0].depositID;
         }
      });
	}

	function deleteDep(id){
		$.ajax({
          type: "POST",
          url:"checkDep.php",
          async : false,
          data:{
            depositID : id
          },success:function(result){
          	if(result=="red"){
				var x = confirm("Are you sure you want to delete?");
			    if (x){
			    	$.ajax({
			          type: "POST",
			          url:"deleteDep.php",
			          async : false,
			          data:{
			            depositID : id
			          },success:function(result){
			          	alert(result);
			          	window.location.reload();
			         }
			      });
			    }
			    else{
			      return false;
			  	}
			  }
			  else{
			  	alert("Deposit utilized, So you can't delete it");
			  }
		  }
		});
	}

	function apply(depositId,payorId,payorType,depAmt){
		sessionStorage.setItem("depID",depositId);
		sessionStorage.setItem("depPayor",payorId);
        sessionStorage.setItem("depPayorType",payorType);
		sessionStorage.setItem("depAmt",depAmt);
		window.location.href = "DepositSelectClient.php";
	}
	function changeDateFormat(inputDate){  // expects Y-m-d
        var splitDate = inputDate.split('-');
        if(splitDate.count == 0){
            return null;
        }

        var year = splitDate[0];
        var month = splitDate[1];
        var day = splitDate[2]; 

        return month + '-' + day + '-' + year;
    }
    function changeDate(inputDate){  // expects Y-m-d
        var splitDate = inputDate.split('-');
        if(splitDate.count == 0){
            return null;
        }

        var year = splitDate[2];
        var month = splitDate[0];
        var day = splitDate[1]; 

        return year+ '-' + month + '-' + day;
    }
	</script>
</body>
</html>