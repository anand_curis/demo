<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <title>Upload Spreadsheet | CRM</title>

    <!--=== CSS ===-->

    <!-- Bootstrap -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

    <!-- jQuery UI -->
    <!--<link href="plugins/jquery-ui/jquery-ui-1.10.2.custom.css" rel="stylesheet" type="text/css" />-->
    <!--[if lt IE 9]>
        <link rel="stylesheet" type="text/css" href="plugins/jquery-ui/jquery.ui.1.10.2.ie.css"/>
    <![endif]-->

    <!-- Theme -->
    <link href="assets/css/main.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/plugins.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" href="assets/css/fontawesome/font-awesome.min.css">
    <!--[if IE 7]>
        <link rel="stylesheet" href="assets/css/fontawesome/font-awesome-ie7.min.css">
    <![endif]-->

    <!--[if IE 8]>
        <link href="assets/css/ie8.css" rel="stylesheet" type="text/css" />
    <![endif]-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>

    <!--=== JavaScript ===-->

    <script type="text/javascript" src="assets/js/libs/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>

    <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/js/libs/lodash.compat.min.js"></script>

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
        <script src="assets/js/libs/html5shiv.js"></script>
    <![endif]-->

    <!-- Smartphone Touch Events -->
    <script type="text/javascript" src="plugins/touchpunch/jquery.ui.touch-punch.min.js"></script>
    <script type="text/javascript" src="plugins/event.swipe/jquery.event.move.js"></script>
    <script type="text/javascript" src="plugins/event.swipe/jquery.event.swipe.js"></script>

    <!-- General -->
    <script type="text/javascript" src="assets/js/libs/breakpoints.js"></script>
    <script type="text/javascript" src="plugins/respond/respond.min.js"></script> <!-- Polyfill for min/max-width CSS3 Media Queries (only for IE8) -->
    <script type="text/javascript" src="plugins/cookie/jquery.cookie.min.js"></script>
    <script type="text/javascript" src="plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script type="text/javascript" src="plugins/slimscroll/jquery.slimscroll.horizontal.min.js"></script>

    <!-- Page specific plugins -->

    <script type="text/javascript" src="plugins/daterangepicker/moment.min.js"></script>
    <script type="text/javascript" src="plugins/daterangepicker/daterangepicker.js"></script>
    <script type="text/javascript" src="plugins/blockui/jquery.blockUI.min.js"></script>

    <!-- Forms -->
    <script type="text/javascript" src="plugins/uniform/jquery.uniform.min.js"></script> <!-- Styled radio and checkboxes -->
    <script type="text/javascript" src="plugins/select2/select2.min.js"></script> <!-- Styled select boxes -->

    <!-- App -->
    <script type="text/javascript" src="assets/js/app.js"></script>
    <script type="text/javascript" src="assets/js/plugins.js"></script>
    <script type="text/javascript" src="assets/js/plugins.form-components.js"></script>
    <link href="https://cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css" rel='stylesheet' type='text/css'>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>

    <script>
    $(document).ready(function(){
        "use strict";

        App.init(); // Init layout and core plugins
        Plugins.init(); // Init all plugins
        FormComponents.init(); // Init all form-specific plugins
        $("#username").html(sessionStorage.getItem("loginName"));
        var facilityID = sessionStorage.getItem("facilityID");
        document.getElementById("_hdnFacility").value = facilityID;
    });
    </script>

</head>

<body>

    <!-- Header -->
    <header class="header navbar navbar-fixed-top" role="banner" style="background-image: url('assets/bg.jpg'); background-repeat: repeat-x;">
        <!-- Top Navigation Bar -->
        <div class="container">

            <!-- Only visible on smartphones, menu toggle -->
            <ul class="nav navbar-nav">
                <li class="nav-toggle"><a href="javascript:void(0);" title=""><i class="icon-reorder"></i></a></li>
            </ul>

            <!-- Logo -->
            <a class="navbar-brand" href="index.html" style="text-align:center">
                <img src="assets/img/logo.png" alt="logo" />
            </a>
            <!-- /logo -->

            <!-- Sidebar Toggler -->
            <!-- /Sidebar Toggler -->

            <!-- Top Left Menu -->
            <ul class="nav navbar-nav navbar-left hidden-xs hidden-sm">
                <li>
                    <a href="index.html">
                        Dashboard
                    </a>
                </li>
                <li>
                    <a href="ViewAllClaims.php">
                        Claims List
                    </a>
                </li>
                <li>
                    <a href="upload.php">
                        Upload Claims
                    </a>
                </li>
            </ul>
            <!-- /Top Left Menu -->

            <!-- Top Right Menu -->
            <ul class="nav navbar-nav navbar-right">
                <!-- User Login Dropdown -->
                <li class="dropdown user">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <!--<img alt="" src="assets/img/avatar1_small.jpg" />-->
                        <i class="icon-male"></i>
                        <span id="username"></span>
                        <i class="icon-caret-down small"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="select.html"><i class="icon-tasks"></i> Swap Facility</a></li>
                        <li class="divider"></li>
                        <li><a href="login.html"><i class="icon-key"></i> Log Out</a></li>
                    </ul>
                </li>
                <!-- /user login dropdown -->
            </ul>
            <!-- /Top Right Menu -->
        </div>
        <!-- /top navigation bar -->
    </header> <!-- /.header -->

    <div id="container">

        <div id="content">
            <div class="container">
                <br/><br/><br/>

            <form method="post">
                <input type="hidden" id="_hdnFacility" name="_hdnFacility" value="">
                <button type="submit" value="" name="btn_import">Upload Excel</button>
            </form>
            <?php
            $host = "localhost";
            $uname = "curis_user";
            $pass = "Curis@123";
            $faciID = "";
            //$host = "localhost";
            // $uname = "root";
            // $pass = "";
            $database = "curis_crm"; //Change Your Database Name
            //$database = "crm"; //Change Your Database Name
            $conn = new mysqli($host, $uname, $pass, $database,"3306")or die("No Connection");
            echo mysql_error();
    //MYSQL MYADDMINPHP TO CSV
            if (isset($_REQUEST['btn_export']))
            {
                $data_op = "";
                $sql = $conn->query("select * from users"); //Change Your Table Name          
                while ($row1 = $sql->fetch_field())
                {
                    $data_op .= '"' . $row1->name . '",';
                }
                $data_op .="\n";
                while ($row = $sql->fetch_assoc())
                {
                    foreach ($row as $key => $value)
                    {
                        $data_op .='"' . $value . '",';
                    }
                    $data_op .="\n";
                }
                $filename = "Database.csv"; //Change File type CSV/TXT etc
                header('Content-type: application/csv'); //Change File type CSV/TXT etc
                header('Content-Disposition: attachment; filename=' . $filename);
                echo $data_op;
                exit;
            }
    //CSV To MYSQL MYADDMINPHP
            if (isset($_REQUEST['btn_import']))
            {
                $sql3 = "TRUNCATE TABLE curis_crm.claim_temp";
                if (!$conn->query($sql3))
                {
                    echo $row[0];
                }
                $faciID = $_REQUEST['_hdnFacility'];
                $filename = 'uploads/Database.csv';
                $fp = fopen($filename, "r");
                while (($row = fgetcsv($fp, "1000", ",")) != FALSE)
                {
                    if ( ! isset($row[0])) {
                       $row[0] = null;
                    }
                    if ( ! isset($row[1])) {
                       $row[1] = null;
                    }
                    if ( ! isset($row[2])) {
                       $row[2] = null;
                    }
                    if ( ! isset($row[3])) {
                       $row[3] = null;
                    }
                    if ( ! isset($row[4])) {
                       $row[4] = null;
                    }
                    if ( ! isset($row[5])) {
                       $row[5] = null;
                    }
                    if ( ! isset($row[6])) {
                       $row[6] = null;
                    }
                    if ( ! isset($row[7])) {
                       $row[7] = null;
                    }
                    if ( ! isset($row[8])) {
                       $row[8] = null;
                    }
                    if ( ! isset($row[9])) {
                       $row[9] = null;
                    }
                    // else{
                    //     $result1 = mysql_query("select dispoID from disposition where DispoName='$row[9]'");
                    //     while($row1 = mysql_fetch_array($result1)){
                    //         $row[9] = $row1["dispoID"];
                    //     }
                    // }
                    
                    $sql = "INSERT INTO claim_temp(patientName, company, days, facility, dos, balance, charges, patientDOB, comments, dispo) 
                    VALUES('$row[0]','$row[1]','$row[2]','$row[3]','$row[4]','$row[5]','$row[6]','$row[7]','$row[8]','$row[9]')";
                    // $sql = "INSERT INTO claim_new(patientName, company, days, facility, dos, balance, charges, patientDOB, comments, dispo) 
                    // VALUES('$row[0]','$row[1]','$row[2]','$row[3]','$row[4]','$row[5]','$row[6]','$row[7]','$row[8]','$row[9]')";
                    if (!$conn->query($sql))
                    {
                        echo $row[0];
                    }
                }

                  $sql4 = "UPDATE claim_new LEFT JOIN claim_temp ON claim_new.patientName = claim_temp.patientName AND claim_new.dos = claim_temp.dos AND claim_new.company = claim_temp.company AND claim_new.facility = claim_temp.facility SET claim_new.tyreID = '4' WHERE claim_temp.claimID IS NULL AND claim_new.facility = '$faciID'";
                 if (!$conn->query($sql4))
                 {
                     echo $row[0];
                 }

                $sql1 = "INSERT INTO claim_new (patientName, company, days, facility, dos, balance, charges, patientDOB, comments, dispo) SELECT patientName, company, days, facility, dos, balance, charges, patientDOB, comments, dispo FROM claim_temp WHERE NOT EXISTS (SELECT * FROM claim_new WHERE claim_temp.patientName= claim_new.patientName AND claim_temp.facility= claim_new.facility AND claim_temp.dos= claim_new.dos AND claim_temp.company= claim_new.company)";
                if (!$conn->query($sql1))
                {
                    echo $row[0];
                }

                $sql2 = "DELETE a FROM claim_new a INNER JOIN claim_new b ON a.patientName = b.patientName AND a.company = b.company AND a.facility = b.facility AND a.dos = b.dos WHERE a.claimID > b.claimID ";
                if (!$conn->query($sql2))
                {
                    echo $row[0];
                }


                // echo "UPDATE claim_new inner join claim_temp on claim_temp.patientName = claim_new.patientName AND claim_temp.dos = claim_new.dos AND claim_temp.company = claim_new.company AND claim_temp.facility = claim_new.facility SET claims_new.tyreID= '4' WHERE claim_new.facility = '$faciID'";
                echo "Uploaded Successfully";
                fclose($fp);
            }
            ?>
        </body>
    </html>