<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>AMROMED LLC </title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <link rel="shortcut icon" href="img/favicon.ico"/>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <!-- global css -->
    <link type="text/css" rel="stylesheet" href="css/app.css"/>
    <!-- end of global css -->
    <!--page level css -->
    <link rel="stylesheet" href="vendors/swiper/css/swiper.min.css">
    <link href="vendors/nvd3/css/nv.d3.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="vendors/lcswitch/css/lc_switch.css">
    <link href="vendors/select2/css/select2.min.css" type="text/css" rel="stylesheet">
    <link href="vendors/select2/css/select2-bootstrap.css" rel="stylesheet">
    <link href="vendors/bootstrapvalidator/css/bootstrapValidator.min.css" rel="stylesheet">
    <link href="vendors/iCheck/css/all.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="css/custom.css">
    <link href="css/custom_css/wizard.css" rel="stylesheet">

     <link href="vendors/hover/css/hover-min.css" rel="stylesheet">
    <link rel="stylesheet" href="vendors/laddabootstrap/css/ladda-themeless.min.css">
    <link href="css/buttons_sass.css" rel="stylesheet">
    <link href="css/advbuttons.css" rel="stylesheet">
    <link href="css/custom_css/dashboard2.css" rel="stylesheet" type="text/css"/>
    <link href="vendors/weathericon/css/weather-icons.min.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="vendors/metrojs/css/MetroJs.min.css">

    <link href="css/custom_css/dashboard1.css" rel="stylesheet" type="text/css"/>
    <link href="css/custom_css/dashboard1_timeline.css" rel="stylesheet"/>
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <link href="css/jquery.datetimepicker.css" rel="stylesheet" />
    
    <link href="css/alertify.css" rel='stylesheet' type='text/css'>
    <link href="css/default.css" rel='stylesheet' type='text/css'>

	<link href="css/jquery-ui.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css" rel='stylesheet' type='text/css'>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
    <script src="assets/js/jquery.datetimepicker.full.min.js"></script>
	<link href="assets/css/jquery.datetimepicker.css" rel="stylesheet" />

	<style type="text/css">
	select option{
	  font-size: 10px;
	}
	</style>

	<script>
	$(document).ready(function(){

		var facilityID = sessionStorage.getItem("practiceName");

                $("#linkLogout").click(function(){
                     sessionStorage.removeItem("loginName");
                     sessionStorage.removeItem("facilityID");
                     window.location.href="login.html";
                });

                     var logName = sessionStorage.getItem("loginName");
                if(logName == null){
                     window.location.href="login.html";
                }
		
		$('.datepicker').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
           var tempDispo = sessionStorage.getItem("tempDispo");
		$.ajax({
            type: "POST",
            async:false,
            url:"getAllClaimsNew3.php",
            data:{
            	facilityID : facilityID,
                dispoValue : tempDispo
            },success:function(data){
                var dt = [];
                var data1 = JSON.parse(data);
				$.each(data1,function(i,v) {
					dt.push([data1[i].claimID,data1[i].claimID,data1[i].patientName,data1[i].company,data1[i].days,data1[i].facility,data1[i].dos,data1[i].balance,data1[i].charges,data1[i].patientDOB,unescape(data1[i].comments),data1[i].dispo]);
				});
				 $('.test').removeAttr('width').DataTable({
			        "iDisplayLength": 10,
			        dom: 'Bfrtip',
                 buttons: [
                    'csv', 'excel','print'
                 ],
				"data": dt,
				columnDefs: [
					{ "width": '30px', "targets": 1 },
		            { "width": '100px', "targets": 2 },
		            { "width": '100px', "targets": 3 },
		            { "width": '25px', "targets": 4},
		            { "width": '25px', "targets": 5 },
		            { "width": '40px', "targets": 6 },
		            { "width": '40px', "targets": 7 },
		            { "width": '40px', "targets": 8 },
		            { "width": '40px', "targets": 9 },
		            { "width": '200px', "targets": 10 },
		            { "width": '40px', "targets": 11 }
		        ],
				columns: [
					{"title": "claimID",visible:false},
					{"title": "Action",
						"render": function ( data, type, full, meta ) {
                          return '<a href="javascript:void(0)" class="OpenNotes'+data+' AddNotes"><span class="ti-comment-alt"></span></a>&nbsp; | &nbsp;<a href="javascript:void(0);" class="edit'+data+' editClaim"><span class="ti-pencil"></span></a>'
                        }
					},
					{"title": "Patient Name"},
					{"title": "Company"},
					{"title": "Days"},
					{"title": "Facilty"},
					{"title": "DOS",
						"render": function ( data, type, full, meta ) {
                          return changeDateFormat1(data);
                        }
					},
					{"title": "Balance"},
					{"title": "Charges"},
					{"title": "Patient DOB",
						"render": function ( data, type, full, meta ) {
                          return changeDateFormat1(data);
                        }
					},
					{"title": "AR Comment"},
					{"title": "Disposition"
						// ,"render": function ( data, type, full, meta ) {
      //                     return '<select class="tyre1 ddlDispo form-control"><option value="0"></option><option value="1">INFO FROM PROVIDER</option><option value="2">INFO FROM PATIENT</option><option value="3">TFL_NO PROOF</option><option value="4">DEDUCTIBLE</option><option value="5">APPEAL</option><option value="6">CAPITATION</option><option value="7">COB</option><option value="8">DX ISSUE</option><option value="9">MODIFIER ISSUE</option><option value="10">POS ISSUE</option><option value="11">PX ISSUE</option><option value="12">DUPLICATE</option><option value="13">IME - NO FAULT</option><option value="14">INS NEED MED REC</option><option value="15">BENEFIT EXHAUSTED</option><option value="16">NO AUTH</option><option value="17">NO COVERAGE</option><option value="18">PAID &amp; NOT CASHED</option><option value="19">PAID TO PATIENT</option><option value="20">PAID &amp; CASHED</option><option value="21">PRE EXISITING CONDITION</option><option value="22">PRIMARY PAID MAX</option><option value="23">CREDENTIALING ISSUE</option><option value="24">PURGED CLAIMS</option><option value="25">REBILLED DIFF INS</option><option value="26">PAID TO INCORR ADDR</option><option value="27">REFILED</option><option value="28">REFILE CORR_CLM</option><option value="29">REPROCESS</option><option value="30">RESOLVED</option><option value="31">FAXED</option><option value="32">UNDER PROCESS</option><option value="33">VOICE MAIL / CALLBACK</option><option value="34">REJECTION</option><option value="35">PT ENROLLED IN HOSPICE</option></select>'
      //                   }
					}
				]
				});

            }
        });

        //$(".test").find("thead th").attr("style","background-color:#eee;text-align:center;")
		

		$(document).on( 'click', '.AddNotes', function () {
			var temp = $(this).attr('class').split(' ')[0];
            var claimID = temp.replace('OpenNotes','');
            document.getElementById('hdnAddNotes').value = claimID;
		    $('#AddNotes').modal('show');
		    $.ajax({
              type: "GET",
              url:"getAllDisposition.php",
              data:{
              },success:function(result){
              	var arrCase = JSON.parse(result);
              	$.each(arrCase,function(i,v) {
		            $('#ddlDispo').html('<option value=""></option>');
		            arrCase.forEach(function(t) { 
		                    $('#ddlDispo').append('<option value="'+t.dispoID+'">'+t.dispoName+'</option>');
		                });
		        });
              }
          });
		});

		$(document).on( 'click', '.editClaim', function () {
			$('#editInfo').modal('show');
			var temp = $(this).attr('class').split(' ')[0];
            var claimID = temp.replace('edit','');
			$.ajax({
              type: "POST",
              url:"getHistory.php",
              data:{
              	claimID : claimID
              },success:function(result){
              	var res = JSON.parse(result);
              	var a="";
              	for(var i=0;i<res.length;i++){
              		a+="<p><strong>"+res[i].user+" ["+res[i].workedDate+"]</strong> : "+ res[i].dispo +" - "+unescape(res[i].comments)+"</p>";
              	}
              	$("#history").html(a);
              }
          });
		});


		$(document).on('click','#btnSaveNotes',function() {
			var claimID = document.getElementById('hdnAddNotes').value;
			var followupNotes = escape($('#followupNotes').val());
			var disposition = $('#ddlDispo').val();
			var user = $('#username').html();
			if($('#followupDate').val() != ""){
				var followupDate = changeDateFormat2($('#followupDate').val());
			}
			else{
				var followupDate = "0000-00-00";
			}
            $.ajax({
              type: "POST",
              url:"addNotes.php",
              data:{
                claimID : claimID,
                followupNotes : followupNotes,
                disposition : disposition,
                followupDate : followupDate,
                user : user
              },success:function(result){
              	alert(result);
              	window.location.reload();
              }
          });
        });
	});

	function changeFormat(inputDate){  // expects Y-m-d
        var splitDate = inputDate.split('/');
        if(splitDate.count == 0){
            return null;
        }

        var month = splitDate[0];
        var date = splitDate[1];
        var year = splitDate[2]; 

        return month + '/' + date + '/' + year;
    }

    function changeDateFormat2(inputDate){  // expects Y-m-d
        var splitDate = inputDate.split('-');
        if(splitDate.count == 0){
            return null;
        }

        var year = splitDate[0];
        var month = splitDate[1];
        var day = splitDate[2]; 

        return day + '-' + year + '-' + month;
    }

   function changeDateFormat1(inputDate){  // expects Y-m-d
        var splitDate = inputDate.split('-');
        if(splitDate.count == 0){
            return null;
        }

        var year = splitDate[0];
        var month = splitDate[1];
        var day = splitDate[2]; 

        return month+ '-' + day+ '-' +year;
    }

   function changeDateFormat(inputDate){  // expects Y-m-d
        var splitDate = inputDate.split('-');
        if(splitDate.count == 0){
            return null;
        }

        var year = splitDate[0];
        var month = splitDate[1];
        var day = splitDate[2]; 

        return month + '-' + year + '-' + day;
    }
	</script>



</head>

<body>

	<!-- header logo: style can be found in header-->
<header class="header">
    <nav class="navbar navbar-static-top" role="navigation">
        <a href="index.html" class="logo">
            <!-- Add the class icon to your logo image or logo icon to add the marginin -->
            <img src="img/logo_white.png" alt="logo"/>
        </a>
        <!-- Header Navbar: style can be found in header-->
        <!-- Sidebar toggle button-->
        <div class="mr-auto">
            <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button"> <i
                    class="fa fa-fw ti-menu"></i>
            </a>
        </div>
        <div class="navbar-right">
            <ul class="nav navbar-nav">
                <li class="dropdown messages-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-fw ti-email black"></i>
                        <span class="badge badge-pill badge-success">2</span>
                    </a>
                    <ul class="dropdown-menu dropdown-messages table-striped">
                        <li class="dropdown-title">New Messages</li>
                        <li>
                            <a href="" class="message striped-col dropdown-item">
                                <img class="message-image rounded-circle" src="img/authors/avatar7.jpg" alt="avatar-image">

                                <div class="message-body"><strong>Ernest Kerry</strong>
                                    <br>
                                    Can we Meet?
                                    <br>
                                    <small>Just Now</small>
                                    <span class="badge badge-success label-mini msg-lable">New</span>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="" class="message dropdown-item">
                                <img class="message-image rounded-circle" src="img/authors/avatar6.jpg" alt="avatar-image">

                                <div class="message-body"><strong>John</strong>
                                    <br>
                                    Dont forgot to call...
                                    <br>
                                    <small>5 minutes ago</small>
                                    <span class="badge badge-success label-mini msg-lable">New</span>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="" class="message striped-col dropdown-item">
                                <img class="message-image rounded-circle" src="img/authors/avatar5.jpg" alt="avatar-image">

                                <div class="message-body">
                                    <strong>Wilton Zeph</strong>
                                    <br>
                                    If there is anything else &hellip;
                                    <br>
                                    <small>14/10/2014 1:31 pm</small>
                                </div>

                            </a>
                        </li>
                        <li>
                            <a href="" class="message dropdown-item">
                                <img class="message-image rounded-circle" src="img/authors/avatar1.jpg" alt="avatar-image">
                                <div class="message-body">
                                    <strong>Jenny Kerry</strong>
                                    <br>
                                    Let me know when you free
                                    <br>
                                    <small>5 days ago</small>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="" class="message striped-col dropdown-item">
                                <img class="message-image rounded-circle" src="img/authors/avatar.jpg" alt="avatar-image">
                                <div class="message-body">
                                    <strong>Tony</strong>
                                    <br>
                                    Let me know when you free
                                    <br>
                                    <small>5 days ago</small>
                                </div>
                            </a>
                        </li>
                        <li class="dropdown-footer"><a href="#"> View All messages</a></li>
                    </ul>



                </li>
                <!--rightside toggle-->
                <li>
                    <a href="#" class="dropdown-toggle toggle-right" data-toggle="dropdown">
                        <i class="fa fa-fw ti-view-list black"></i>
                        <span class="badge badge-pill badge-danger">9</span>
                    </a>
                </li>
                <!-- User Account: style can be found in dropdown-->
                <li class="dropdown user user-menu">
                     <a href="#" class="dropdown-toggle padding-user d-block" data-toggle="dropdown">
                        <img src="img/authors/avatar1.jpg" width="35" class="rounded-circle img-fluid float-left"
                             height="35" alt="User Image">
                        <div class="riot">
                            <div id="practiceName">
                                Addison
                                <span><i class="fa fa-sort-down"></i></span>
                            </div>
                        </div>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="img/authors/avatar1.jpg" class="rounded-circle" alt="User Image">
                            <p id="practiceName1"> Addison</p>
                        </li>
                        <!-- Menu Body -->
                        <li class="p-t-3"><a href="javascript:void(0)" class="dropdown-item"> <i class="fa fa-fw ti-user"></i> My Profile </a>
                        </li>
                        <li role="presentation"></li>
                        <li><a href="javascript:void(0)" class="dropdown-item"><i class="fa fa-fw ti-settings"></i> Account Settings </a></li>
                        <li role="presentation" class="dropdown-divider"></li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="float-left">
                                <a href="lockscreen.html">
                                    <i class="fa fa-fw ti-lock"></i>
                                    Lock
                                </a>
                            </div>
                            <div class="float-right">
                                <a href="index.html" id="linkLogout">
                                    <i class="fa fa-fw ti-shift-right"></i>
                                    Logout
                                </a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
<div class="wrapper row-offcanvas row-offcanvas-left">
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="left-side sidebar-offcanvas">
        <!-- sidebar: style can be found in sidebar-->
        <section class="sidebar">
            <div id="menu" role="navigation">
                <div class="nav_profile">
                    <div class="media profile-left">
                        <a class="float-left profile-thumb" href="#">
                            <img src="img/authors/avatar1.jpg" class="rounded-circle" alt="User Image"></a>
                        <div class="content-profile">
                            <h4 class="media-heading" style="overflow:hidden" id="lblUser"></h4>
                            <ul class="icon-list">
                                <li>
                                    <a href="javascript:void(0)" title="user">
                                        <i class="fa fa-fw ti-user"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="lockscreen.html" title="lock">
                                        <i class="fa fa-fw ti-lock"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)" title="settings">
                                        <i class="fa fa-fw ti-settings"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="index.html" title="Login">
                                        <i class="fa fa-fw ti-shift-right"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <ul class="navigation">
                    <li id="navv1">
                        <a href="../ViewPatient.php">
                            <i class="menu-icon ti-desktop"></i>
                            <span class="mm-text ">Clients</span>
                        </a>
                    </li>
                    <li>
                        <a href="../ViewPhysician.php">
                            <i class="menu-icon ti-desktop"></i> 
                            <span class="mm-text ">Employees</span>
                        </a>
                    </li>
                    <li id="navv2">
                        <a href="../scheduler.php">
                            <i class="menu-icon ti-layout-list-large-image"></i>
                            <span class="mm-text ">Scheduler</span>
                        </a>
                    </li>
                    <li class="menu-dropdown"  id="navv3">
                        <a href="javascript:void(0)">
                            <i class="menu-icon ti-check-box"></i>
                            <span>EDI</span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li>
                                <a href="../generateAllEDI.php">
                                    <i class="fa fa-fw ti-receipt"></i> Generate EDI
                                    <span class="fa arrow"></span>
                                </a>
                            </li>
                            <li>
                                <a href="../EDIList.php">
                                    <i class="fa fa-fw ti-receipt"></i> EDI List
                                    <span class="fa arrow"></span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="menu-dropdown"  id="navv4">
                        <a href="javascript:void(0)">
                            <i class="menu-icon ti-check-box"></i>
                            <span>Statements</span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li>
                                <a href="../generateStatement.php">
                                    <i class="fa fa-fw ti-receipt"></i> Generate Statements
                                    <span class="fa arrow"></span>
                                </a>
                            </li>
                            <li>
                                <a href="../statementList.php">
                                    <i class="fa fa-fw ti-receipt"></i> Statements List
                                    <span class="fa arrow"></span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li id="navv5">
                        <a href="../Ledger.php">
                            <i class="menu-icon ti-layout-list-large-image"></i>
                            <span class="mm-text ">Ledger</span>
                        </a>
                    </li>
                    <li id="navv6">
                        <a href="../Deposit.php">
                            <i class="menu-icon ti-layout-list-large-image"></i>
                            <span class="mm-text ">Deposits</span>
                        </a>
                    </li>
                    <li id="navv7">
                        <a href="dashboard.html">
                            <i class="menu-icon ti-desktop"></i>
                            <span class="mm-text ">CRM</span>
                        </a>
                    </li>
                    <li id="navv8">
                        <a href="../Reports.php">
                            <i class="menu-icon ti-layout-list-large-image"></i>
                            <span class="mm-text ">Reports</span>
                        </a>
                    </li>
                    <li class="menu-dropdown" id="navv9">
                        <a href="javascript:void(0)">
                            <i class="menu-icon ti-check-box"></i>
                            <span>Settings</span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li>
                                <a href="../ViewPractice.php">
                                    <i class="fa fa-fw ti-receipt"></i> Practices
                                    <span class="fa arrow"></span>
                                </a>
                            </li>
                            <li>
                                <a href="../ViewServiceLoc.php">
                                    <i class="fa fa-fw ti-receipt"></i> Locations &amp; Facilities 
                                    <span class="fa arrow"></span>
                                </a>
                            </li>
                            <li>
                                <a href="../ViewCPT.php">
                                    <i class="fa fa-fw ti-receipt"></i> Procedure Codes
                                    <span class="fa arrow"></span>
                                </a>
                            </li>
                            <li>
                                <a href="../ViewDX.php">
                                    <i class="fa fa-fw ti-receipt"></i> Diagnosis Codes
                                    <span class="fa arrow"></span>
                                </a>
                            </li>
                            <li>
                                <a href="../insurances.php">
                                    <i class="fa fa-fw ti-receipt"></i> Payers List
                                    <span class="fa arrow"></span>
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
                <!-- / .navigation --> </div>
            <!-- menu --> </section>
        <!-- /.sidebar --> </aside>
    <aside class="right-side" style="background-color:#ebeff0;">

        <section class="content">
            <div class="row">
              <div class="col-md-12">
                <a href="dashboard.html"><button class="btn btn-primary" style="margin-top:5%">Dashboard</button></a>
              </div>
              <div class="col-md-12">
                <table class="test table table-striped table-bordered table-hover table-checkable"></table>
              </div>
            </div>
        </section>
        <!-- /.content --> </aside>
    <!-- /.right-side --> </div>
	<div class="modal fade" id="AddNotes" tabindex="-1">
        <div class="modal-dialog" style="width:60%;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Add FollowUp Notes</h4>
                </div>
                <div class="modal-body" style="min-height:200px;">
                	<input type="hidden" id="hdnAddNotes"/>
                	<div class="form-group col-md-12" style="padding-bottom:30px;">
                        <label class="control-label col-md-3">Disposition </label>
                        <div class="col-md-9">
                            <select class="form-control" id="ddlDispo"></select>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="form-group col-md-12" style="padding-bottom:30px;">
                        <label class="control-label col-md-3">Followup Date </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control datepicker" id="followupDate">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="form-group col-md-12" style="padding-bottom:30px;">
                        <label class="control-label col-md-3">Followup Notes </label>
                        <div class="col-md-9">
                            <textarea class="form-control notes" rows="5" id="followupNotes"></textarea>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="modal-footer">
                    <input type="button" class="btn btn-default" data-dismiss="modal" value="Exit" />
                    <input type="button" id="btnSaveNotes" class="btn btn-primary" data-dismiss="modal" value="Save" />
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
    <div class="modal fade" id="editInfo" tabindex="-1">
        <div class="modal-dialog" style="width:70%;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">View History</h4>
                </div>
                <div class="modal-body" style="min-height:300px;">
                	<input type="hidden" id="hdnAddNotes"/>
                	<div class="col-md-2">
                	</div>
	                <div class="col-md-8">
		                <div class="tabbable tabbable-custom">
	                        <ul class="nav nav-tabs">
	                            <li class="active"><a href="#history" data-toggle="tab" style="border-bottom:none">History</a></li>
	                        </ul>
	                        <div class="tab-content" style="min-height:250px; overflow:scroll;">
	                            <div class="tab-pane active" id="history">
	                            </div>
	                        </div>
	                    </div>
	                </div>
                </div>
                <div class="modal-footer">
                    <input type="button" class="btn btn-default" data-dismiss="modal" value="Exit" />
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
      <link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
<script src="js/app.js" type="text/javascript"></script>
<!-- end of global js -->

<!-- begining of page level js -->

<!--swiper-->
<script type="text/javascript" src="js/alertify.min.js"></script>
<script type="text/javascript" src="vendors/swiper/js/swiper.min.js"></script>
<!-- <script src="js/dashboard2.js" type="text/javascript"></script> -->
<!--chartjs-->
<script src="vendors/chartjs/js/Chart.js"></script>
<!--nvd3 chart-->
<script type="text/javascript" src="js/nvd3/d3.v3.min.js"></script>
<script type="text/javascript" src="vendors/nvd3/js/nv.d3.min.js"></script>
<script type="text/javascript" src="vendors/moment/js/moment.min.js"></script>
<script type="text/javascript" src="vendors/advanced_newsTicker/js/newsTicker.js"></script>
<script src="vendors/iCheck/js/icheck.js"></script>
<script src="vendors/moment/js/moment.min.js"></script>
<script src="vendors/select2/js/select2.js" type="text/javascript"></script>
<script src="vendors/bootstrapwizard/js/jquery.bootstrap.wizard.js" type="text/javascript"></script>
<script src="vendors/bootstrapvalidator/js/bootstrapValidator.min.js" type="text/javascript"></script>
<script src="js/custom_js/form_wizards.js" type="text/javascript"></script>
<link href="https://cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css" rel='stylesheet' type='text/css'>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
<link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
<!-- end of page level js -->
<script src="vendors/moment/js/moment.min.js" type="text/javascript"></script>
<!-- date-range-picker -->
<script src="vendors/daterangepicker/js/daterangepicker.js" type="text/javascript"></script>
<script src="js/jquery.datetimepicker.js"></script>
<script type="text/javascript" src="js/jquery.mask.min.js"></script>

<script type="text/javascript" src="vendors/Buttons/js/buttons.js"></script>
<script type="text/javascript" src="vendors/laddabootstrap/js/spin.min.js"></script>
<script type="text/javascript" src="vendors/laddabootstrap/js/ladda.min.js"></script>
<script type="text/javascript" src="js/custom_js/button_main.js"></script>

 <script>
    $(document).ready(function(){
        //alert(sessionStorage.getItem("patientId"));
        //document.getElementById('hdntoggleDX').value = "false";
        document.getElementById('practiceName').innerHTML = sessionStorage.getItem('practiceName');
        document.getElementById('practiceName1').innerHTML = sessionStorage.getItem('practiceName');
        document.getElementById("lblUser").innerHTML = sessionStorage.getItem('userName');
        $("#DataTables_Table_0_wrapper").css({"margin-top" : "2%"});
        $("#DataTables_Table_0").css({"width" : "100%"});
        $("#linkLogout").click(function(){
            sessionStorage.removeItem('practiceName');
            sessionStorage.removeItem('userID');
            sessionStorage.removeItem('userName');
            sessionStorage.removeItem('loginName');
        })
        $("#navv7").addClass("active");
    });
</script>

</body>

</html>