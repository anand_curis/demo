<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0" />
	<title>Find Claims | CRM</title>

	<!--=== CSS ===-->

	<!-- Bootstrap -->
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

	<!-- jQuery UI -->
	<!--<link href="plugins/jquery-ui/jquery-ui-1.10.2.custom.css" rel="stylesheet" type="text/css" />-->
	<!--[if lt IE 9]>
		<link rel="stylesheet" type="text/css" href="plugins/jquery-ui/jquery.ui.1.10.2.ie.css"/>
	<![endif]-->

	<!-- Theme -->
	<link href="assets/css/main.css" rel="stylesheet" type="text/css" />
	<link href="assets/css/plugins.css" rel="stylesheet" type="text/css" />
	<link href="assets/css/responsive.css" rel="stylesheet" type="text/css" />
	<link href="assets/css/icons.css" rel="stylesheet" type="text/css" />

	<link rel="stylesheet" href="assets/css/fontawesome/font-awesome.min.css">
	<!--[if IE 7]>
		<link rel="stylesheet" href="assets/css/fontawesome/font-awesome-ie7.min.css">
	<![endif]-->

	<!--[if IE 8]>
		<link href="assets/css/ie8.css" rel="stylesheet" type="text/css" />
	<![endif]-->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>

	<!--=== JavaScript ===-->

	<script type="text/javascript" src="assets/js/libs/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" src="plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>

	<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="assets/js/libs/lodash.compat.min.js"></script>

	<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
		<script src="assets/js/libs/html5shiv.js"></script>
	<![endif]-->

	<!-- Smartphone Touch Events -->
	<script type="text/javascript" src="plugins/touchpunch/jquery.ui.touch-punch.min.js"></script>
	<script type="text/javascript" src="plugins/event.swipe/jquery.event.move.js"></script>
	<script type="text/javascript" src="plugins/event.swipe/jquery.event.swipe.js"></script>

	<!-- General -->
	<script type="text/javascript" src="assets/js/libs/breakpoints.js"></script>
	<script type="text/javascript" src="plugins/respond/respond.min.js"></script> <!-- Polyfill for min/max-width CSS3 Media Queries (only for IE8) -->
	<script type="text/javascript" src="plugins/cookie/jquery.cookie.min.js"></script>
	<script type="text/javascript" src="plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script type="text/javascript" src="plugins/slimscroll/jquery.slimscroll.horizontal.min.js"></script>

	<!-- Page specific plugins -->
	<!-- Charts -->
	<script type="text/javascript" src="plugins/sparkline/jquery.sparkline.min.js"></script>

	<script type="text/javascript" src="plugins/daterangepicker/moment.min.js"></script>
	<script type="text/javascript" src="plugins/daterangepicker/daterangepicker.js"></script>
	<script type="text/javascript" src="plugins/blockui/jquery.blockUI.min.js"></script>

	<!-- Forms -->
	<script type="text/javascript" src="plugins/uniform/jquery.uniform.min.js"></script> <!-- Styled radio and checkboxes -->
	<script type="text/javascript" src="plugins/select2/select2.min.js"></script> <!-- Styled select boxes -->

	<!-- App -->
	<script type="text/javascript" src="assets/js/app.js"></script>
	<script type="text/javascript" src="assets/js/plugins.js"></script>
	<script type="text/javascript" src="assets/js/plugins.form-components.js"></script>
    <link href="https://cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css" rel='stylesheet' type='text/css'>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>

	<script>
	$(document).ready(function(){
		"use strict";

		App.init(); // Init layout and core plugins
		Plugins.init(); // Init all plugins
		FormComponents.init(); // Init all form-specific plugins
		$.ajax({
            type: "GET",
            url:"getAllClaims.php",
            data:{
            },success:function(data){
                var dt = [];
                var data1 = JSON.parse(data);
				$.each(data1,function(i,v) {
					dt.push([data1[i].claimID,data1[i].claimID,data1[i].claimID,data1[i].fullName,data1[i].DOS,data1[i].practice,data1[i].insuranceName,data1[i].dob,data1[i].idNo,data1[i].providerName,data1[i].pxCode,data1[i].dxCode,data1[i].modifier,data1[i].units,data1[i].billedDate,data1[i].chargedAmount,data1[i].allowedAmount,data1[i].balance,data1[i].paid,data1[i].checkNo,data1[i].patientResp,data1[i].followUpNotes]);
				});
				 $('.test').removeAttr('width').DataTable({
					scrollY:        "570px",
			        scrollX:        true,
			        scrollCollapse: true,
			        "iDisplayLength": 10,
				"data": dt,
				columnDefs: [
					{ "width": '30px', "targets": 1 },
		            { "width": '100px', "targets": 2 },
		            { "width": '100px', "targets": 3 },
		            { "width": '45px', "targets": 4},
		            { "width": '100px', "targets": 5 },
		            { "width": '100px', "targets": 6 },
		            { "width": '45px', "targets": 7 },
		            { "width": '60px', "targets": 8 },
		            { "width": '70px', "targets": 9 },
		            { "width": '45px', "targets": 10 },
		            { "width": '40px', "targets": 11 },
		            { "width": '40px', "targets": 12 },
		            { "width": '30px', "targets": 13 },
		            { "width": '20px', "targets": 14 },
		            { "width": '45px', "targets": 15 },
		            { "width": '30px', "targets": 16 }
		        ],
				columns: [
					{"title": "claimID",visible:false},
					{"title": "Action",
						"render": function ( data, type, full, meta ) {
                          return '<a href="javascript:void(0);" class="saveClaim'+data+' claimSave" style="font-size:16px;"><i class="icon icon-save"></i></a>&nbsp; | &nbsp;<a href="javascript:void(0);" class="OpenNotes'+data+' AddNotes"><i class="icon icon-pencil"></i></a>'
                        }
					},
					{"title": "Disposition",
						"render": function ( data, type, full, meta ) {
                          return '<select class="tyre1 ddlDispo form-control"><option value="0"></option><option value="1">INFO FROM PROVIDER</option><option value="2">INFO FROM PATIENT</option><option value="3">TFL_NO PROOF</option><option value="4">DEDUCTIBLE</option><option value="5">APPEAL</option><option value="6">CAPITATION</option><option value="7">COB</option><option value="8">DX ISSUE</option><option value="9">MODIFIER ISSUE</option><option value="10">POS ISSUE</option><option value="11">PX ISSUE</option><option value="12">DUPLICATE</option><option value="13">IME - NO FAULT</option><option value="14">INS NEED MED REC</option><option value="15">BENEFIT EXHAUSTED</option><option value="16">NO AUTH</option><option value="17">NO COVERAGE</option><option value="18">PAID &amp; NOT CASHED</option><option value="19">PAID TO PATIENT</option><option value="20">PAID &amp; CASHED</option><option value="21">PRE EXISITING CONDITION</option><option value="22">PRIMARY PAID MAX</option><option value="23">CREDENTIALING ISSUE</option><option value="24">PURGED CLAIMS</option><option value="25">REBILLED DIFF INS</option><option value="26">PAID TO INCORR ADDR</option><option value="27">REFILED</option><option value="28">REFILE CORR_CLM</option><option value="29">REPROCESS</option><option value="30">RESOLVED</option><option value="31">FAXED</option><option value="32">UNDER PROCESS</option><option value="33">VOICE MAIL / CALLBACK</option><option value="34">REJECTION</option><option value="35">PT ENROLLED IN HOSPICE</option></select>'
                        }
					},
					{"title": "fullName"},
					{"title": "DOS"},
					{"title": "practice"},
					{"title": "insuranceName"},
					{"title": "dob"},
					{"title": "idNo"},
					{"title": "providerName"},
					{"title": "pxCode"},
					{"title": "dxCode"},
					{"title": "modifier"},
					{"title": "units"},
					{"title": "billedDate"},
					{"title": "chargedAmount"},
					{"title": "allowedAmount"},
					{"title": "balance"},
					{"title": "paid"},
					{"title": "checkNo"},
					{"title": "Patient Resp"},
					{"title": "followUpNotes"}
				]
				});

            }
        });
		

		$(document).on( 'click', '.claimSave', function () {
		    var data1 = $(this).closest('tr').find('.ddlDispo').val();
		    var tyre = $(this).closest('tr').find('.ddlDispo').attr('class').split(' ')[0];
		    var tyreId = tyre.replace('tyre','');
		    var temp = $(this).attr('class').split(' ')[0];
            var claimID = temp.replace('saveClaim','');
            $.ajax({
              type: "POST",
              url:"moveRecord.php",
              data:{
                claimID : claimID,
                dispoID : data1,
                tyreID : tyreId,
                tyre : 'claims'
              },success:function(result){
              	alert("Successfully moved");
              	window.location.reload();
              }
          });
		});
		$(document).on( 'click', '.AddNotes', function () {
			var temp = $(this).attr('class').split(' ')[0];
            var claimID = temp.replace('OpenNotes','');
            document.getElementById('hdnAddNotes').value = claimID;
		    $('#AddNotes').modal('show');
		});


		$(document).on('click','#btnSaveNotes',function() {
			var claimID = document.getElementById('hdnAddNotes').value;
			var notes = $('.notes').val();
            $.ajax({
              type: "POST",
              url:"addNotes.php",
              data:{
                claimID : claimID,
                followUpNotes : notes
              },success:function(result){
              	alert(result.replace(/\s/g, ""));
              	window.location.reload();
              }
          });
        });
	});
	</script>

	<!-- Demo JS -->
	<script type="text/javascript" src="assets/js/custom.js"></script>

</head>

<body>

	<!-- Header -->
	<header class="header navbar navbar-fixed-top" role="banner">
		<!-- Top Navigation Bar -->
		<div class="container">

			<!-- Only visible on smartphones, menu toggle -->
			<ul class="nav navbar-nav">
				<li class="nav-toggle"><a href="javascript:void(0);" title=""><i class="icon-reorder"></i></a></li>
			</ul>

			<!-- Logo -->
			<a class="navbar-brand" href="index.html" style="text-align:center">
				<img src="assets/img/logo.png" alt="logo" />
				<strong>CRM</strong>
			</a>
			<!-- /logo -->

			<!-- Sidebar Toggler -->
			<a href="#" class="toggle-sidebar bs-tooltip" data-placement="bottom" data-original-title="Toggle navigation">
				<i class="icon-reorder"></i>
			</a>
			<!-- /Sidebar Toggler -->

			<!-- Top Left Menu -->
			<ul class="nav navbar-nav navbar-left hidden-xs hidden-sm">
				<li>
					<a href="index.html">
						Dashboard
					</a>
				</li>
				<li>
					<a href="ViewClaims.php">
						Claims List
					</a>
				</li>
				<li>
					<a href="SwapClaims.php">
						Move Claims
					</a>
				</li>
				<li>
					<a href="UploadFile.php">
						Upload Claims
					</a>
				</li>
			</ul>
			<!-- /Top Left Menu -->

			<!-- Top Right Menu -->
			<ul class="nav navbar-nav navbar-right">
				<!-- User Login Dropdown -->
				<li class="dropdown user">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<!--<img alt="" src="assets/img/avatar1_small.jpg" />-->
						<i class="icon-male"></i>
						<span class="username">Peter Browne</span>
						<i class="icon-caret-down small"></i>
					</a>
					<ul class="dropdown-menu">
						<li><a href="pages_user_profile.html"><i class="icon-user"></i> My Profile</a></li>
						<li><a href="pages_calendar.html"><i class="icon-calendar"></i> My Calendar</a></li>
						<li><a href="#"><i class="icon-tasks"></i> My Tasks</a></li>
						<li class="divider"></li>
						<li><a href="login.html"><i class="icon-key"></i> Log Out</a></li>
					</ul>
				</li>
				<!-- /user login dropdown -->
			</ul>
			<!-- /Top Right Menu -->
		</div>
		<!-- /top navigation bar -->

		<!--=== Project Switcher ===-->
		<div id="project-switcher" class="container project-switcher">
			<div id="scrollbar">
				<div class="handle"></div>
			</div>

			<div id="frame">
				<ul class="project-list">
					<li>
						<a href="javascript:void(0);">
							<span class="image"><i class="icon-desktop"></i></span>
							<span class="title">Lorem ipsum dolor</span>
						</a>
					</li>
					<li>
						<a href="javascript:void(0);">
							<span class="image"><i class="icon-compass"></i></span>
							<span class="title">Dolor sit invidunt</span>
						</a>
					</li>
					<li class="current">
						<a href="javascript:void(0);">
							<span class="image"><i class="icon-male"></i></span>
							<span class="title">Consetetur sadipscing elitr</span>
						</a>
					</li>
					<li>
						<a href="javascript:void(0);">
							<span class="image"><i class="icon-thumbs-up"></i></span>
							<span class="title">Sed diam nonumy</span>
						</a>
					</li>
					<li>
						<a href="javascript:void(0);">
							<span class="image"><i class="icon-female"></i></span>
							<span class="title">At vero eos et</span>
						</a>
					</li>
					<li>
						<a href="javascript:void(0);">
							<span class="image"><i class="icon-beaker"></i></span>
							<span class="title">Sed diam voluptua</span>
						</a>
					</li>
					<li>
						<a href="javascript:void(0);">
							<span class="image"><i class="icon-desktop"></i></span>
							<span class="title">Lorem ipsum dolor</span>
						</a>
					</li>
					<li>
						<a href="javascript:void(0);">
							<span class="image"><i class="icon-compass"></i></span>
							<span class="title">Dolor sit invidunt</span>
						</a>
					</li>
					<li>
						<a href="javascript:void(0);">
							<span class="image"><i class="icon-male"></i></span>
							<span class="title">Consetetur sadipscing elitr</span>
						</a>
					</li>
					<li>
						<a href="javascript:void(0);">
							<span class="image"><i class="icon-thumbs-up"></i></span>
							<span class="title">Sed diam nonumy</span>
						</a>
					</li>
					<li>
						<a href="javascript:void(0);">
							<span class="image"><i class="icon-female"></i></span>
							<span class="title">At vero eos et</span>
						</a>
					</li>
					<li>
						<a href="javascript:void(0);">
							<span class="image"><i class="icon-beaker"></i></span>
							<span class="title">Sed diam voluptua</span>
						</a>
					</li>
				</ul>
			</div> <!-- /#frame -->
		</div> <!-- /#project-switcher -->
	</header> <!-- /.header -->

	<div id="container">
		<div id="content">
			<div class="container">
				<br/>
				<table class="test table table-striped table-bordered table-hover table-checkable" width="150%">
				</table>
			</div>

		</div>
	</div>
	<div class="modal fade" id="AddNotes" tabindex="-1">
        <div class="modal-dialog" style="width:60%;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Add FollowUp Notes</h4>
                </div>
                <div class="modal-body" style="min-height:100px;">
                	<input type="hidden" id="hdnAddNotes"/>
                    <div class="form-group col-md-12">
                        <label class="control-label col-md-3">Notes </label>
                        <div class="col-md-9">
                            <textarea class="form-control notes"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="button" class="btn btn-default" data-dismiss="modal" value="Exit" />
                    <input type="button" id="btnSaveNotes" class="btn btn-primary" data-dismiss="modal" value="Save" />
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
</body>
</html>