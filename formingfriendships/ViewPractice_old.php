<?php
 session_start();

 if (!isset($_SESSION['userID'])) {
  header("Location: index.html");
 }
 include('header.html');
 ?>

				<div class="row">
					<h2 style="color: #251367; margin-left:20px;">Find Practices</h2>
					<div class="col-md-12" style="margin-top:20px;">
			        <div class="widget box" >
			            <div class="widget-content">
			            	<a href="javascript:void(0);" id="linkPractice" class="btn btn-primary"><i class="icon icon-plus"></i>&nbsp;Add New Practice</a>
			            	<form action="EditPatient.php" method="post">
								<table id="practiceList" class="table table-striped table-bordered table-hover table-checkable" cellspacing="0" style="overflow-x:scroll" data-horizontal-width="150%">
								</table>
							</form>
			            </div>
			        </div>

				</div>

				</div>
				<!-- /Page Content -->
			</div>
			<!-- /.container -->

		</div>
	</div>
	<div class="modal fade" id="myModal1" tabindex="-1">
		<div class="modal-dialog modal-lg" style="width:70%">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Add New Practice</h4>
				</div>
				<div class="modal-body" style="min-height:150px; padding-bottom:110px;">
                    <div class="card-body">
                        <div class="row">
        					<input type="hidden" id="hdnFlag"/>
        					<div class="form-group col-md-6">
        	                    <label class="control-label col-md-4">Practice Name </label>
        	                    <div class="col-md-8">
        	                        <input type="text" class="form-control required" id="practiceName1" name="PrimaryPhysiName" value="" />
        	                    </div>
        	                </div>

        	                <div class="form-group col-md-6">
        	                	<label class="control-label col-md-4">Speciality </label>
        	                    <div class="col-md-8">
        	                        <select class="form-control" id="phySpeciality">
        	                        	<option></option>
        	                        	<option>Psychologist</option>
        	                        	<option>Counselor</option>
        	                        </select>
        	                    </div>
        	            	</div>
        	            	<div class="clearfix"></div>

        	                <div class="form-group col-md-6">
        	                    <label class="control-label col-md-4">Group NPI </label>
        	                    <div class="col-md-8">
        	                        <input type="text" class="form-control required" id="phyNPI" name="txtPTNumber"  value="" />
        	                    </div>
        	                </div>

        	                <div class="form-group col-md-6">
        	                    <label class="control-label col-md-4">Date Of Birth </label>
        	                    <div class="col-md-8">
        	                        <input type="text" class="form-control required" id="phyDOB" name="txtPTNumber"  value="" />
        	                    </div>
        	                </div>

        	                <div class="form-group col-md-6">
        	                    <label class="control-label col-md-4">Tax ID </label>
        	                    <div class="col-md-8">
        	                        <input type="text" class="form-control required" id="phyEIN" name="txtPTNumber" value="" />
        	                    </div>
        	                </div>

        	            	<div class="form-group col-md-6">
        	                    <label class="control-label col-md-4">Social Security # </label>
        	                    <div class="col-md-8">
        	                        <input type="text" class="form-control required" id="phySSN" name="txtPTNumber"  value="" />
        	                    </div>
        	                </div>
        	                <div class="clearfix"></div>
        	                <hr/>
        	                <div class="form-group col-md-6">
        	                    <label class="control-label col-md-4">Address </label>
        	                    <div class="col-md-8">
        	                    	<textarea class="form-control" cols="5" rows="4" id="phyAddr"></textarea>
        	                    </div>
        	                </div>

        	                <div class="form-group col-md-6" >
        	                    <label class="control-label col-md-4">Home</label>
        	                    <div class="col-md-8">
        	                         <input type="text" class="form-control required" id="phyHomePhone" name="txtPTNumber"  value="" />
        	                    </div>
        	                </div>

        	                 <div class="form-group col-md-6">
        	                    <label class="control-label col-md-4">Work </label>
        	                    <div class="col-md-8">
        	                        <input type="text" class="form-control required" id="phyWorkPhone" name="txtPTNumber" value="" />
        	                    </div>
        	                </div>

        	                <div class="form-group col-md-6" >
        	                    <label class="control-label col-md-4">Mobile </label>
        	                    <div class="col-md-8">
        	                        <input type="text" class="form-control required" id="phyMobilePhone" name="txtPTNumber" value="" />
        	                    </div>
        	                </div>
        	                <div class="clearfix"></div>
        	                <div class="form-group col-md-6">
        	                    <label class="control-label col-md-4">Dept </label>
        	                    <div class="col-md-8">
        	                        <input type="text" class="form-control required" id="phyDept" name="txtPTNumber" value="" />
        	                    </div>
        	                </div>
        	                <div class="form-group col-md-6">
        	                    <label class="control-label col-md-4">Fax </label>
        	                    <div class="col-md-8">
        	                        <input type="text" class="form-control required" id="phyFax" name="txtPTNumber" value="" />
        	                    </div>
        	                </div>
        	                <div class="clearfix"></div>
        	                <hr/>
        	                <div class="form-group col-md-12">
        	                    <label class="control-label col-md-2">Notes </label>
        	                    <div class="col-md-8">
        	                    	<textarea class="form-control" cols="20" rows="6" id="phyNotes"></textarea>
        	                    </div>
        	                </div>
                        </div>
                    </div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<input type="button" value="Add Practice" class="btn btn-primary" id="btnAddPractice"/>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div>
    <?php
  include('footer.html');
 ?>
	<link href="https://cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css" rel='stylesheet' type='text/css'>
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
	<script>


	$(document).ready(function(){
		$.get("https://curismed.com/medService/practice", function(data1, status){
	        var dt = [];
				$.each(data1,function(i,v) {
					dt.push([data1[i].practiceID,data1[i].practiceName,data1[i].address,data1[i].npi,data1[i].EIN,data1[i].practiceID]);
				});
				//alert(dt);
				var table = $('#practiceList').DataTable({
		        "data": dt,
		        "bPaginate": false,
		        "bProcessing": true,
		         "aoColumns": [
		         	{"mdata": "practiceID","title":"Practice ID", visible:false},
		            {"title":"Practice Name","mdata": "practiceName"},
		            {"title":"Address","mdata": "address"},
		            {"title":"Group ID","mdata": "npi"},
		            {"title":"Tax ID","mdata": "EIN"},
		            {"mdata": "practiceID","title":"Action",
		            	"render": function ( data, type, full, meta ) {
					      return '<a href="javascript:void(0);" class="edit'+data+' editPractice"><i class="ti-pencil"></i></a>';
					    }
		        	},
			    ]
		        });
	    });
		$('#linkPractice').click(function(){
			document.getElementById("hdnFlag").value = 0;
			$('#myModal1').modal('show');
		});

		$(document).on('click','.editPractice',function() {
			var temp = $(this).attr('class').split(' ')[0];
			var phyID = temp.replace('edit','');
			document.getElementById("hdnFlag").value = phyID;
			$.ajax({
              type: "POST",
              url:"getPracticeInfo.php",
              async : false,
              data:{
                practiceID : phyID
              },success:function(result){
              	$('#myModal1').modal('show');
              	var res = JSON.parse(result);
              	document.getElementById("practiceName1").value = res[0].practiceName;
				document.getElementById("phySpeciality").value = res[0].speciality;
				document.getElementById("phyDOB").value = res[0].dob;
				document.getElementById("phyNPI").value = res[0].npi;
				document.getElementById("phySSN").value = res[0].ssn;
				document.getElementById("phyEIN").value = res[0].EIN;
				document.getElementById("phyAddr").value = res[0].address;
				document.getElementById("phyHomePhone").value = res[0].phoneHome;
				document.getElementById("phyWorkPhone").value = res[0].phoneWork;
				document.getElementById("phyMobilePhone").value = res[0].phoneMobile;
				document.getElementById("phyDept").value = res[0].dept;
				document.getElementById("phyFax").value = res[0].fax;
				document.getElementById("phyNotes").value = res[0].notes;
				document.getElementById("hdnFlag").value = res[0].practiceID;
				document.getElementById("btnAddPractice").value = "Update Practice";
              }
          	});
		});

		$("#btnAddPractice").click(function() {
			var hdnFlag = document.getElementById("hdnFlag").value;
			if(hdnFlag == 0){
	            var phyName = document.getElementById("practiceName1").value;
				var speciality = document.getElementById("phySpeciality").value;
				var dob = document.getElementById("phyDOB").value;
				var individualNPI = document.getElementById("phyNPI").value;
				var ssn = document.getElementById("phySSN").value;
				var EIN = document.getElementById("phyEIN").value;
				var address = document.getElementById("phyAddr").value;
				var phoneHome = document.getElementById("phyHomePhone").value;
				var phoneWork = document.getElementById("phyWorkPhone").value;
				var phoneMobile = document.getElementById("phyMobilePhone").value;
				var dept = document.getElementById("phyDept").value;
				var fax = document.getElementById("phyFax").value;
				var notes = document.getElementById("phyNotes").value;

	            $.ajax({
	                type: "POST",
	                url:"backend/add_practice.php",
	                data:{ 
	                    "practiceName" : phyName,
	                    "phyDOB" : dob,
	                    "phyNPI" : individualNPI,
	                    "phySSN" : ssn,
	                    "EIN" : EIN,
	                    "phyAddr" : address,
	                    "phyHomePhone" : phoneHome,
	                    "phyWorkPhone" : phoneWork,
	                    "phyMobilePhone" : phoneMobile,
	                    "dept" : dept,
	                    "phyFax" : fax,
	                    "phyNotes" : notes,
	                    "speciality" : speciality,
	                    },success:function(result){
	                        //alertify.success('PrimaryPhysiName updated successfully');
	                        $('#myModal1').modal('hide');
	                        //window.location.reload();
	                     }
	            });
	        }
	        else
	        {
	        	var practiceID = document.getElementById("hdnFlag").value;
	        	var phyName = document.getElementById("practiceName1").value;
				var speciality = document.getElementById("phySpeciality").value;
				var dob = document.getElementById("phyDOB").value;
				var individualNPI = document.getElementById("phyNPI").value;
				var ssn = document.getElementById("phySSN").value;
				var EIN = document.getElementById("phyEIN").value;
				var address = document.getElementById("phyAddr").value;
				var phoneHome = document.getElementById("phyHomePhone").value;
				var phoneWork = document.getElementById("phyWorkPhone").value;
				var phoneMobile = document.getElementById("phyMobilePhone").value;
				var dept = document.getElementById("phyDept").value;
				var fax = document.getElementById("phyFax").value;
				var notes = document.getElementById("phyNotes").value;

	            $.ajax({
	                type: "POST",
	                url:"backend/update_practice.php",
	                data:{ 
	                	"practiceID" : practiceID,
	                    "practiceName" : phyName,
	                    "phyDOB" : dob,
	                    "phyNPI" : individualNPI,
	                    "phySSN" : ssn,
	                    "EIN" : EIN,
	                    "phyAddr" : address,
	                    "phyHomePhone" : phoneHome,
	                    "phyWorkPhone" : phoneWork,
	                    "phyMobilePhone" : phoneMobile,
	                    "dept" : dept,
	                    "phyFax" : fax,
	                    "phyNotes" : notes,
	                    "speciality" : speciality,
	                    },success:function(result){
	                        //alertify.success('PrimaryPhysiName updated successfully');
	                        $('#myModal1').modal('hide');
	                        //window.location.reload();
	                     }
	            });
	        }
    	});
});
			</script>
</body>
</html>