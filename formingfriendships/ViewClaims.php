<?php
 session_start();

 if (!isset($_SESSION['userID'])) {
  header("Location: login.php");
 }
 ?>
 <!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0" />
	<title>Claims List | AMROMED LLC</title>

	<!--=== CSS ===-->

	<!-- Bootstrap -->
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

	<!-- jQuery UI -->
	<!--<link href="plugins/jquery-ui/jquery-ui-1.10.2.custom.css" rel="stylesheet" type="text/css" />-->
	<!--[if lt IE 9]>
		<link rel="stylesheet" type="text/css" href="plugins/jquery-ui/jquery.ui.1.10.2.ie.css"/>
	<![endif]-->

	<!-- Theme -->
	<link href="assets/css/main.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/plugins.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/alertify.css" rel='stylesheet' type='text/css'>
    <link href="assets/css/themes/default.css" rel='stylesheet' type='text/css'>

	<link rel="stylesheet" href="assets/css/fontawesome/font-awesome.min.css">
	<!--[if IE 7]>
		<link rel="stylesheet" href="assets/css/fontawesome/font-awesome-ie7.min.css">
	<![endif]-->

	<!--[if IE 8]>
		<link href="assets/css/ie8.css" rel="stylesheet" type="text/css" />
	<![endif]-->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>

	<!--=== JavaScript ===-->

	<script type="text/javascript" src="assets/js/libs/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" src="plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>
	<script type="text/javascript" src="plugins/bootstrap-switch/bootstrap-switch.min.js"></script>

	<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="assets/js/libs/lodash.compat.min.js"></script>

	<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
		<script src="assets/js/libs/html5shiv.js"></script>
	<![endif]-->

	<!-- Smartphone Touch Events -->
	<script type="text/javascript" src="plugins/touchpunch/jquery.ui.touch-punch.min.js"></script>
	<script type="text/javascript" src="plugins/event.swipe/jquery.event.move.js"></script>
	<script type="text/javascript" src="plugins/event.swipe/jquery.event.swipe.js"></script>

	<!-- General -->
	<script type="text/javascript" src="assets/js/libs/breakpoints.js"></script>
	<script type="text/javascript" src="plugins/respond/respond.min.js"></script> <!-- Polyfill for min/max-width CSS3 Media Queries (only for IE8) -->
	<script type="text/javascript" src="plugins/cookie/jquery.cookie.min.js"></script>
	<script type="text/javascript" src="plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script type="text/javascript" src="plugins/slimscroll/jquery.slimscroll.horizontal.min.js"></script>

	<!-- Page specific plugins -->
	<!-- Charts -->
	<script type="text/javascript" src="plugins/sparkline/jquery.sparkline.min.js"></script>

	<script type="text/javascript" src="plugins/daterangepicker/moment.min.js"></script>
	<script type="text/javascript" src="plugins/daterangepicker/daterangepicker.js"></script>
	<script type="text/javascript" src="plugins/blockui/jquery.blockUI.min.js"></script>

	<!-- Pickers -->
	<script type="text/javascript" src="plugins/pickadate/picker.js"></script>
	<script type="text/javascript" src="plugins/pickadate/picker.date.js"></script>
	<script type="text/javascript" src="plugins/pickadate/picker.time.js"></script>
	<script type="text/javascript" src="plugins/bootstrap-colorpicker/bootstrap-colorpicker.min.js"></script>

	<!-- Noty -->
	<script type="text/javascript" src="plugins/noty/jquery.noty.js"></script>
	<script type="text/javascript" src="plugins/noty/layouts/top.js"></script>
	<script type="text/javascript" src="plugins/noty/themes/default.js"></script>

	<!-- Slim Progress Bars -->
	<script type="text/javascript" src="plugins/nprogress/nprogress.js"></script>

	<!-- Bootbox -->
	<script type="text/javascript" src="plugins/bootbox/bootbox.min.js"></script>

	<!-- App -->
	<script type="text/javascript" src="assets/js/app.js"></script>
	<script type="text/javascript" src="assets/js/plugins.js"></script>
	<script type="text/javascript" src="assets/js/plugins.form-components.js"></script>
	<link href="plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />

	<script>
	$(document).ready(function(){
		"use strict";

		App.init(); // Init layout and core plugins
		Plugins.init(); // Init all plugins
		FormComponents.init(); // Init all form-specific plugins
		document.getElementById('practiceName').innerHTML = sessionStorage.getItem('practiceName');
		$('#loader').show();
		$(document).on('click','.redPat',function() {
			var temp = $(this).attr('class').split(' ')[0];
			var newTemp = temp.replace('redirectPatient','');
			sessionStorage.setItem("patientId", newTemp);
			window.location.href ="EditPatient.php";
		});

        var providerList = <?php include('primaryPhysician.php'); ?>;
            $("#provider").autocomplete({
    			source: providerList,
                autoFocus:true
            });

        var locationList = <?php include('getServiceLoc.php'); ?>;
            $("#location").autocomplete({
    			source: locationList,
                autoFocus:true
            });
            var temp = sessionStorage.getItem("patientId");
            $.ajax({
              type: "POST",
              url:"getAlerts.php",
              async : false,
              data:{
                alert_patientID : temp
              },success:function(result){
                var res = JSON.parse(result);
                if(res!=""){
                  if(res[0].alerts_ViewClaims == "1"){
                      $('#divAlert').show();
                      document.getElementById("alertTop").innerHTML = res[0].alert_desc;
                  }
                  else{
                    $('#divAlert').hide();
                  }
                }
            }
          });


		$('#btnUpdateClaim').click(function(){
			var claimID = document.getElementById('hdnClaimID').value;
			var fromDt = convertDate(document.getElementById('fromDt').value);
	    	var toDt = convertDate(document.getElementById('toDt').value);
	    	var proced = document.getElementById('proced').value;
	    	var billDt = convertDate(document.getElementById('billDt').value);
	    	var units = document.getElementById('units').value;
	    	var total = document.getElementById('total').value;
	    	var allowed = document.getElementById('allowed').value;
	    	var paid = document.getElementById('paid').value;
	    	var adjustment = document.getElementById('adjustment').value;
	    	var claimBalance = document.getElementById('claimBalance').value;
	    	var claimStatus = document.getElementById('claimStatus').value;
	    	var diag1 = document.getElementById('diag1').value;
	    	var diag2 = document.getElementById('diag2').value;
	    	var diag3 = document.getElementById('diag3').value;
	    	var diag4 = document.getElementById('diag4').value;
	    	var provider = document.getElementById('provider').value;
	    	var location = document.getElementById('location').value;
	    	var n1= provider.indexOf("-");
			var truncID1 = provider.substr(0,n1-1);
			var n2= location.indexOf("-");
			var truncID2 = location.substr(0,n2-1);
	    	var copay = document.getElementById('copay').value;
	    
			$.ajax({
	          type: "POST",
	          url:"updateClaim.php",
	          data:{
	            claimID : claimID,
	            fromDt : fromDt,
		    	toDt : toDt,
		    	proced : proced,
		    	billDt : billDt,
		    	units : units,
		    	total : total,
		    	allowed : allowed,
		    	paid : paid,
		    	adjustment : adjustment,
		    	claimBalance : claimBalance,
		    	claimStatus : claimStatus,
		    	diag1 : diag1,
		    	diag2 : diag2,
		    	diag3 : diag3,
		    	diag4 : diag4,
		    	primaryCarePhy : truncID1,
				serviceLocID : truncID2,
				copay : copay
	          },success:function(result){
	          	$('#EditClaim').modal('hide');
                $.ajax({
                    type: "POST",
                    url:"rebill.php",
                    data:{
                        claimID : claimID
                    },
                    success:function(result){
                        alertify.success("This claim is added to EDI Queue again");
                    }
                });
           
	          	window.location.reload();
	          }
	        });
		});
		var tempcaseText = "";
		$(document).on('click','.Editclaim',function() {
			var temp = $(this).attr('class').split(' ')[0];
			var newTemp = temp.replace('edit','');
			$.ajax({
	          type: "POST",
	          url:"getClaiminfo.php",
	          async : false,
	          data:{
	            claimID : newTemp
	          },success:function(result){
	            $('#EditClaim').modal('show');
	            var n = result.length;
	            result = result.substr(0,n-1);
	            var res = JSON.parse(result);
	            document.getElementById('fromDt').value = changeDateFormat(res[0].fromDt);
	            document.getElementById('hdnClaimID').value = res[0].claimID;
		    	document.getElementById('toDt').value = changeDateFormat(res[0].toDt);
		    	document.getElementById('proced').value = res[0].proced;
		    	document.getElementById('billDt').value = changeDateFormat(res[0].toDt);
		    	document.getElementById('units').value = res[0].units;
		    	document.getElementById('total').value = res[0].total;
		    	document.getElementById('allowed').value = res[0].allowed;
		    	document.getElementById('paid').value = res[0].paid;
		    	document.getElementById('adjustment').value = res[0].adjustment;
		    	document.getElementById('claimBalance').value = res[0].claimBalance;
		    	document.getElementById('diag1').value = res[0].diag1;
                document.getElementById('diag2').value = res[0].diag2;
                document.getElementById('diag3').value = res[0].diag3;
                document.getElementById('diag4').value = res[0].diag4;
                document.getElementById('hdnCaseChart').value = res[0].caseChartNo;
                document.getElementById('hdnBal').value = res[0].claimBalance;
                document.getElementById('hdnCurrInsurance').value = res[0].insuranceID;
                document.getElementById('provider').value = res[0].phyName;
                document.getElementById('location').value = res[0].billingName;
                document.getElementById('copay').value = res[0].copay;
                var currInsurance = document.getElementById('hdnCurrInsurance').value;
                tempcaseText = document.getElementById('hdnCaseChart').value;
                $.ajax({
	              type: "POST",
	              url:"getPatientID.php",
	              async : false,
	              data:{
	                claimID : res[0].claimID
	              },success:function(result){
	              	$.post("http://curismed.com/medService/cases",
				    {
				        patientID: result,
				    },
				    function(data, status){
				        var arrCase = [];
				        $('#case').html('');
				        for(var x in data){
						  arrCase.push(data[x]);
						}
						//alert(arrCase);
						$.each(arrCase,function(i,v) {
							$('#case').html('');
							arrCase.forEach(function(t) { 
					            $('#case').append('<option value="'+t.caseID+'">'+t.caseChartNo+'</option>');
					        });
						});
					})
					getCase(tempcaseText);
	              }
	          	});
		    	$("#claimStatus").val(res[0].claimStatus);
		    	$("#ddlInsurance").text(currInsurance);
		    	if(res[0].claimStatus == "COMPLETE"){
	            	document.getElementById('fromDt').disabled = true;
			    	document.getElementById('toDt').disabled = true;
			    	document.getElementById('proced').disabled = true;
			    	document.getElementById('billDt').disabled = true;
			    	document.getElementById('units').disabled = true;
			    	document.getElementById('total').disabled = true;
			    	document.getElementById('allowed').disabled = true;
			    	document.getElementById('paid').disabled = true;
			    	document.getElementById('adjustment').disabled = true;
			    	document.getElementById('claimBalance').disabled = true;
			    	document.getElementById('diag1').disabled = true;
	                document.getElementById('diag2').disabled = true;
	                document.getElementById('diag3').disabled = true;
	                document.getElementById('diag4').disabled = true;
	                document.getElementById('provider').disabled = true;
	                document.getElementById('location').disabled = true;
	                document.getElementById('copay').disabled = true;
	                //document.getElementById('case').disabled = true;
	            }
	            else{
	            	document.getElementById('fromDt').disabled = false;
			    	document.getElementById('toDt').disabled = false;
			    	document.getElementById('proced').disabled = false;
			    	document.getElementById('billDt').disabled = false;
			    	document.getElementById('units').disabled = false;
			    	document.getElementById('total').disabled = false;
			    	document.getElementById('allowed').disabled = false;
			    	document.getElementById('paid').disabled = false;
			    	document.getElementById('adjustment').disabled = false;
			    	document.getElementById('claimBalance').disabled = false;
			    	document.getElementById('diag1').disabled = false;
	                document.getElementById('diag2').disabled = false;
	                document.getElementById('diag3').disabled = false;
	                document.getElementById('diag4').disabled = false;
	                document.getElementById('provider').disabled = false;
	                document.getElementById('location').disabled = false;
	                document.getElementById('copay').disabled = false;
	                //document.getElementById('case').disabled = false;
	            }
	         }
	      });
		});
		$('#case').change(function(){
			var tempcaseText = document.getElementById('case').options[document.getElementById('case').selectedIndex].text;
			getCase(tempcaseText);
		});
		var fullName = "";
		var payorName = "";
		$.get("getAllClaims.php",function(data1){
	    	var data1 = JSON.parse(data1);
			var dt1 = [];
			var dt2 = [];
			var dt3 = [];
			$.each(data1,function(i,v) {
				if(data1[i].total == ""){
					data1[i].total = 0.00;
				}
				else{
					if(data1[i].total%1 == 0){
						data1[i].total = data1[i].total+'.00';
					}
					else{
						data1[i].total = data1[i].total;
					}
				}
				if(data1[i].allowed == ""){
					data1[i].allowed = 0+'.00';
				}
				else{
					if(data1[i].allowed%1 == 0){
						data1[i].allowed = data1[i].allowed+'.00';
					}
					else{
						data1[i].allowed = data1[i].allowed;
					}
				}
				if(data1[i].claimBalance == ""){
					if(data1[i].total%1 == 0){
						data1[i].claimBalance = data1[i].total+'.00';
					}
					else{
						data1[i].claimBalance = data1[i].total;
					}
				}
				else{
					if(data1[i].claimBalance%1 == 0){
						data1[i].claimBalance = data1[i].claimBalance+'.00';
					}
					else{
						data1[i].claimBalance = data1[i].claimBalance;
					}
				}
				if(data1[i].paid == ""){
					data1[i].paid = 0+'.00';
				}
				else{
					if(data1[i].paid%1 == 0){
						data1[i].paid = data1[i].paid+'.00';
					}
					else{
						data1[i].paid = data1[i].paid;
					}
				}
				if(data1[i].adjustment == ""){
					data1[i].adjustment = 0+'.00';
				}
				else{
					if(data1[i].adjustment%1 == 0){
						data1[i].adjustment = data1[i].adjustment+'.00';
					}
					else{
						data1[i].adjustment = data1[i].adjustment;
					}
				}
				$.ajax({
                  type: "POST",
                  url:"getPatientName.php",
                  async : false,
                  data:{
                    claimID : data1[i].claimID
                  },success:function(result){
                  	if(result!=""){
                  		fullName = result;
                  	}
                  	else{
                  		fullName = "";
                  	}
                  }
              });
				$.ajax({
                  type: "POST",
                  url:"getInsuranceName.php",
                  async : false,
                  data:{
                    insuranceID : data1[i].insuranceID
                  },success:function(result){
                  	var res = JSON.parse(result);
                  	if(res.length != 0){
                  		payorName = res[0].payerName;
                  	}
                  	else{
                  		payorName = "SELF";
                  	}
                  }
              });
				if(data1[i].claimStatus == "COMPLETE"){
					dt1.push([data1[i].claimID,data1[i].fromDt,data1[i].proced,data1[i].units,data1[i].toDt,data1[i].total,data1[i].allowed,data1[i].paid,data1[i].adjustment,data1[i].claimBalance,payorName,fullName,data1[i].claimID,data1[i].diag1,data1[i].diag2,data1[i].diag3,data1[i].diag4]);
				}
				else if(data1[i].claimStatus == "REVIEW"){
					dt2.push([data1[i].claimID,data1[i].fromDt,data1[i].proced,data1[i].units,data1[i].toDt,data1[i].total,data1[i].allowed,data1[i].paid,data1[i].adjustment,data1[i].claimBalance,payorName,fullName,data1[i].claimID,data1[i].diag1,data1[i].diag2,data1[i].diag3,data1[i].diag4]);
				}
				else if(data1[i].claimStatus == "DRAFT"){
					dt3.push([data1[i].claimID,data1[i].fromDt,data1[i].proced,data1[i].units,data1[i].toDt,data1[i].total,data1[i].allowed,data1[i].paid,data1[i].adjustment,data1[i].claimBalance,payorName,fullName,data1[i].claimID,data1[i].diag1,data1[i].diag2,data1[i].diag3,data1[i].diag4]);
				}
			});
			$('#test1').DataTable({
				"aaSorting": [[ 4, "desc" ]],
				"data": dt1,
		        columns: [
		        	{"title": "Claim ID","visible" : false},
		            {"title": "DOS",
		        		"render": function ( data, type, full, meta ) {
					      return changeDateFormat(data);
					    }
					},
		            {"title": "CPT"},
		            {"title": "UNITS"},
		            {"title": "Bill Date",
		            	"render": function ( data, type, full, meta ) {
					      return changeDateFormat(data);
					    }
		        	},
		            {"title": "Billed Amount",
		        		"render": function ( data, type, full, meta ) {
					      return '$'+data;
					    }
					},
		            {"title": "Allowed Amount",
		        		"render": function ( data, type, full, meta ) {
					      return '$'+data;
					    }
					},
		            {"title": "Paid",
		        		"render": function ( data, type, full, meta ) {
					      return '$'+data;
					    }
					},
		            {"title": "Adjustment",
		        		"render": function ( data, type, full, meta ) {
					      return '$'+data;
					    }
					},
		            {"title": "Balance",
		        		"render": function ( data, type, full, meta ) {
					      return '$'+data;
					    }
					},
		            {"title": "Billed To"},
		            {"title": "Patient Name",
					    "render": function ( data, type, full, meta ) {
					    	var tempData=data.indexOf("-");
							var truncID = data.substr(0,tempData);
							var tempName = data.replace(truncID,"");
							var tempDataName = tempName.replace("- ","");
					      return '<a href="#" class="redirectPatient'+truncID+' redPat">'+tempDataName+'</a>';
					    }
					},
					{"title": "Action",
					    "render": function ( data, type, full, meta ) {
					      return '<a href="javascript:void(0);" class="edit'+data+' Editclaim"><i class="icon icon-pencil"></i></a>';
					    }
					}
		        ]
			});
			$('#test2').DataTable({
				"aaSorting": [[ 4, "desc" ]],
				"data": dt2,
		        columns: [
		        	{"title": "Claim ID","visible" : false},
		            {"title": "DOS",
		        		"render": function ( data, type, full, meta ) {
					      return changeDateFormat(data);
					    }
					},
		            {"title": "CPT"},
		            {"title": "UNITS"},
		            {"title": "Bill Date",
		            	"render": function ( data, type, full, meta ) {
					      return changeDateFormat(data);
					    }
		        	},
		            {"title": "Billed Amount",
		        		"render": function ( data, type, full, meta ) {
					      return '$'+data;
					    }
					},
		            {"title": "Allowed Amount",
		        		"render": function ( data, type, full, meta ) {
					      return '$'+data;
					    }
					},
		            {"title": "Paid",
		        		"render": function ( data, type, full, meta ) {
					      return '$'+data;
					    }
					},
		            {"title": "Adjustment",
		        		"render": function ( data, type, full, meta ) {
					      return '$'+data;
					    }
					},
		            {"title": "Balance",
		        		"render": function ( data, type, full, meta ) {
					      return '$'+data;
					    }
					},
		            {"title": "Billed To"},
		            {"title": "Patient Name",
					    "render": function ( data, type, full, meta ) {
					    	var tempData=data.indexOf("-");
							var truncID = data.substr(0,tempData);
							var tempName = data.replace(truncID,"");
							var tempDataName = tempName.replace("- ","");
					      return '<a href="#" class="redirectPatient'+truncID+' redPat">'+tempDataName+'</a>';
					    }
					},
					{"title": "Action",
					    "render": function ( data, type, full, meta ) {
					      return '<a href="javascript:void(0);" class="edit'+data+' Editclaim"><i class="icon icon-pencil"></i></a>';
					    }
					}
		        ]
			});
			$('#test3').DataTable({
				"aaSorting": [[ 4, "desc" ]],
				"data": dt3,
		        columns: [
		        	{"title": "Claim ID","visible" : false},
		            {"title": "DOS",
		        		"render": function ( data, type, full, meta ) {
					      return changeDateFormat(data);
					    }
					},
		            {"title": "CPT"},
		            {"title": "UNITS"},
		            {"title": "Bill Date",
		            	"render": function ( data, type, full, meta ) {
					      return changeDateFormat(data);
					    }
		        	},
		            {"title": "Billed Amount",
		        		"render": function ( data, type, full, meta ) {
					      return '$'+data;
					    }
					},
		            {"title": "Allowed Amount",
		        		"render": function ( data, type, full, meta ) {
					      return '$'+data;
					    }
					},
		            {"title": "Paid",
		        		"render": function ( data, type, full, meta ) {
					      return '$'+data;
					    }
					},
		            {"title": "Adjustment",
		        		"render": function ( data, type, full, meta ) {
					      return '$'+data;
					    }
					},
		            {"title": "Balance",
		        		"render": function ( data, type, full, meta ) {
					      return '$'+data;
					    }
					},
		            {"title": "Billed To"},
		            {"title": "Patient Name",
					    "render": function ( data, type, full, meta ) {
					    	var tempData=data.indexOf("-");
							var truncID = data.substr(0,tempData);
							var tempName = data.replace(truncID,"");
							var tempDataName = tempName.replace("- ","");
					      return '<a href="#" class="redirectPatient'+truncID+' redPat">'+tempDataName+'</a>';
					    }
					},
					{"title": "Action",
					    "render": function ( data, type, full, meta ) {
					      return '<a href="javascript:void(0);" class="edit'+data+' Editclaim"><i class="icon icon-pencil"></i></a>';
					    }
					}
		        ]
			});
			$('#loader').hide();
		});

		document.getElementById('hdnDX').value = "true";
		$('#icd').on('change', function(event, state) {
          document.getElementById('hdnDX').value = event.target.checked; // jQuery event
        });
        
		$(document).on('focus','#diag1',function() {
            var toggleDX = document.getElementById('hdnDX').value;
            if(toggleDX != "false"){
                $("#diag1").autocomplete({
                    minlength : 3,
                    source: function(request, response) { 
                        $.ajax({
                            url: "dxSearch9.php",
                            data: { term: request.term },
                            type: "POST", 
                            success: function(data) { 
                                response(JSON.parse(data)); 
                            }
                        });
                    },
                    autoFocus:true
                });
            }
            else{
                $("#diag1").autocomplete({
                    minlength : 3,
                    source: function(request, response) { 
                        $.ajax({
                            url: "dxSearch10.php",
                            data: { term: request.term },
                            type: "POST", 
                            success: function(data) { 
                                response(JSON.parse(data)); 
                            }
                        });
                    },
                    autoFocus:true
                });
            }
        });

        $(document).on('focus','#diag2',function() {
            var toggleDX = document.getElementById('hdnDX').value;
            if(toggleDX != "false"){
                $("#diag2").autocomplete({
                    minlength : 3,
                    source: function(request, response) { 
                        $.ajax({
                            url: "dxSearch9.php",
                            data: { term: request.term },
                            type: "POST", 
                            success: function(data) { 
                                response(JSON.parse(data)); 
                            }
                        });
                    },
                    autoFocus:true
                });
            }
            else{
                $("#diag2").autocomplete({
                    minlength : 3,
                    source: function(request, response) { 
                        $.ajax({
                            url: "dxSearch10.php",
                            data: { term: request.term },
                            type: "POST", 
                            success: function(data) { 
                                response(JSON.parse(data)); 
                            }
                        });
                    },
                    autoFocus:true
                });
            }
        });

        $(document).on('focus','#diag3',function() {
            var toggleDX = document.getElementById('hdnDX').value;
            if(toggleDX != "false"){
                $("#diag3").autocomplete({
                    minlength : 3,
                    source: function(request, response) { 
                        $.ajax({
                            url: "dxSearch9.php",
                            data: { term: request.term },
                            type: "POST", 
                            success: function(data) { 
                                response(JSON.parse(data)); 
                            }
                        });
                    },
                    autoFocus:true
                });
            }
            else{
                $("#diag3").autocomplete({
                    minlength : 3,
                    source: function(request, response) { 
                        $.ajax({
                            url: "dxSearch10.php",
                            data: { term: request.term },
                            type: "POST", 
                            success: function(data) { 
                                response(JSON.parse(data)); 
                            }
                        });
                    },
                    autoFocus:true
                });
            }
        });

        $(document).on('focus','#diag4',function() {
            var toggleDX = document.getElementById('hdnDX').value;
            if(toggleDX != "false"){
                $("#diag4").autocomplete({
                    minlength : 3,
                    source: function(request, response) { 
                        $.ajax({
                            url: "dxSearch9.php",
                            data: { term: request.term },
                            type: "POST", 
                            success: function(data) { 
                                response(JSON.parse(data)); 
                            }
                        });
                    },
                    autoFocus:true
                });
            }
            else{
                $("#diag4").autocomplete({
                    minlength : 3,
                    source: function(request, response) { 
                        $.ajax({
                            url: "dxSearch10.php",
                            data: { term: request.term },
                            type: "POST", 
                            success: function(data) { 
                                response(JSON.parse(data)); 
                            }
                        });
                    },
                    autoFocus:true
                });
            }
        });
	});
	function getCase(tempcaseText){
		$('#ddlDiv').html('<select id="ddlInsurance" class="form-control"><option value="-100">SELF</option></select>');
	    for(var i=1; i<4;i++){
	        $.post("http://curismed.com/medService/policies/load",
	        {
	            policyEntity: i,
	            caseChartNo : tempcaseText
	        },
	        function(data1, status){
	            if(data1.length != 0){
	                var insName1 = data1[0].InsuranceName;
	                var insuranceID1 = data1[0].insuranceID;
	                $('#ddlInsurance').append('<option value="'+insuranceID1+'">'+insName1+'</option>');
	            }
	        });
	    }
	}
	</script>
	<style>
    .ui-autocomplete-input {
  border: none; 
  font-size: 14px;

  height: 24px;
  margin-bottom: 5px;
  padding-top: 2px;
  border: 1px solid #DDD !important;
  padding-top: 0px !important;
  position: relative;
}
.ui-menu .ui-menu-item a {
  font-size: 12px;
  color:#fff;
}
.ui-autocomplete {
  position: absolute;
  top: 0;
  left: 0;
  z-index: 1510 !important;
  float: left;
  display: none;
  min-width: 160px;
  width: 160px;
  padding: 4px 0;
  margin: 2px 0 0 0;
  list-style: none;
  background-color: #ffffff;
  border-color: #ccc;
  border-color: rgba(0, 0, 0, 0.2);
  border-style: solid;
  border-width: 1px;
  -webkit-border-radius: 2px;
  -moz-border-radius: 2px;
  border-radius: 2px;
  -webkit-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
  -moz-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
  box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
  -webkit-background-clip: padding-box;
  -moz-background-clip: padding;
  background-clip: padding-box;
  *border-right-width: 2px;
  *border-bottom-width: 2px;
}
.ui-menu-item > a.ui-corner-all {
    display: block;
    padding: 3px 15px;
    clear: both;
    font-weight: normal;
    line-height: 18px;
    color:#000;
    white-space: nowrap;
    text-decoration: none;
}
.ui-state-hover {
      color: #fff;
      text-decoration: none;
      background-color: #0088cc;
      border-radius: 0px;
      -webkit-border-radius: 0px;
      -moz-border-radius: 0px;
      background-image: none;
}
.ui-state-active {
      color: #fff;
      text-decoration: none;
      background-color: #0088cc;
      border-radius: 0px;
      -webkit-border-radius: 0px;
      -moz-border-radius: 0px;
      background-image: none;
}

    .icon:hover{
        text-decoration: none;
    }
    a:hover{
        text-decoration: none;
    }
    </style>

	<!-- Demo JS -->
	<script type="text/javascript" src="assets/js/custom.js"></script>
	<script type="text/javascript" src="assets/js/demo/ui_general.js"></script>
</head>

<body>

	<!-- Header -->
	<header class="header navbar navbar-fixed-top" role="banner" style="background-image: url('assets/bg.jpg'); background-repeat: repeat-x;">
		<!-- Top Navigation Bar -->
		<div class="container">

			<!-- Only visible on smartphones, menu toggle -->
			<ul class="nav navbar-nav">
				<li class="nav-toggle"><a href="javascript:void(0);" title=""><i class="icon-reorder"></i></a></li>
			</ul>

			<!-- Logo -->
			<a class="navbar-brand" style="text-align:center;padding-right:50px;" href="index.html">
				<img src="assets/logo2.png"/>
				<strong>medABA</strong>
			</a>
			<!-- /logo -->

			<!-- Sidebar Toggler -->
			<a href="#" class="toggle-sidebar bs-tooltip" data-placement="bottom" data-original-title="Toggle navigation">
				<i class="icon-reorder"></i>
			</a>
			<!-- /Sidebar Toggler -->

			<!-- Top Left Menu -->
            <ul class="nav navbar-nav navbar-left hidden-xs hidden-sm" style="list-style:none;">
                <li>
                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                        Appointment
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                        Patients
                        <i class="icon-caret-down small"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="AddPatient.php">
                            <i class="icon-angle-right"></i>
                            Add Patients
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="ViewPatient.php">
                            <i class="icon-angle-right"></i>
                            Find Patients
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                        Claims
                        <i class="icon-caret-down small"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="AddClaim.php">
                            <i class="icon-angle-right"></i>
                            Add Claims
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="ViewClaims.php">
                            <i class="icon-angle-right"></i>
                            View Claims
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="Ledger.php">
                            <i class="icon-angle-right"></i>
                            Ledger
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="javascript:void(0);" style="font-weight:600;">
                            EDI
                            </a>
                            <li>
                                <a href="EDIList.php">
                                <i class="icon-angle-right"></i>
                                View Claims batch
                                </a>
                            </li>
                            <li>
                                <a href="generateEDI.php">
                                <i class="icon-angle-right"></i>
                                Generate EDI
                                </a>
                            </li>
                            <li>
                                <a href="generateAllEDI.php">
                                <i class="icon-angle-right"></i>
                                Generate EDI Batch
                                </a>
                            </li>
                            <li>
                                <a href="generatePaperClaim.php">
                                <i class="icon-angle-right"></i>
                                Print Paper Claims
                                </a>
                            </li>
                            <li>
                                <a href="generateEOB.php">
                                <i class="icon-angle-right"></i>
                                Patient Statements
                                </a>
                            </li>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                        Financial
                        <i class="icon-caret-down small"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="Deposit.php">
                            <i class="icon-angle-right"></i>
                            View Deposits
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="charts.html" class="dropdown-toggle" data-toggle="dropdown">
                        Reports
                        <i class="icon-caret-down small"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="javascript:voic(0);">
                            <i class="icon-angle-right"></i>
                             Accounts Receivable
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="javascript:voic(0);">
                            <i class="icon-angle-right"></i>
                            Productivity
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="javascript:voic(0);">
                            <i class="icon-angle-right"></i>
                            Patients
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="javascript:voic(0);">
                            <i class="icon-angle-right"></i>
                            Appointments
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="javascript:voic(0);">
                            <i class="icon-angle-right"></i>
                            Claims
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="javascript:voic(0);">
                            <i class="icon-angle-right"></i>
                            Payments
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="javascript:voic(0);">
                            <i class="icon-angle-right"></i>
                            Refunds
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="charts.html" class="dropdown-toggle" data-toggle="dropdown">
                        Settings
                        <i class="icon-caret-down small"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="ViewPractice.php">
                            <i class="icon-angle-right"></i>
                            Practices
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="ViewPhysician.php">
                            <i class="icon-angle-right"></i>
                            Physicians
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="ViewServiceLoc.php">
                            <i class="icon-angle-right"></i>
                            Locations &amp; Facilities
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="javascript:void(0);" style="font-weight:600;">
                            Codes
                            </a>
                            <li>
                                <a href="ViewCPT.php">
                                <i class="icon-angle-right"></i>
                                CPT/Procedure
                                </a>
                            </li>
                            <li>
                                <a href="ViewDX.php">
                                <i class="icon-angle-right"></i>
                                ICD 10/ICD 9 Library
                                </a>
                            </li>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="insurances.php">
                            <i class="icon-angle-right"></i>
                            Insurances
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="selectThemes.php">
                            <i class="icon-angle-right"></i>
                            Themes
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="CRM.php">
                            <i class="icon-angle-right"></i>
                            CRM - AR
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- /Top Left Menu -->

			<!-- Top Right Menu -->
			<ul class="nav navbar-nav navbar-right">
				<!-- Notifications -->
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="icon-warning-sign"></i>
						<span class="badge">5</span>
					</a>
					<ul class="dropdown-menu extended notification">
						<li class="title">
							<p>You have 5 new notifications</p>
						</li>
						<li>
							<a href="javascript:void(0);">
								<span class="label label-success"><i class="icon-plus"></i></span>
								<span class="message">New user registration.</span>
								<span class="time">1 mins</span>
							</a>
						</li>
						<li>
							<a href="javascript:void(0);">
								<span class="label label-danger"><i class="icon-warning-sign"></i></span>
								<span class="message">High CPU load on cluster #2.</span>
								<span class="time">5 mins</span>
							</a>
						</li>
						<li>
							<a href="javascript:void(0);">
								<span class="label label-success"><i class="icon-plus"></i></span>
								<span class="message">New user registration.</span>
								<span class="time">10 mins</span>
							</a>
						</li>
						<li>
							<a href="javascript:void(0);">
								<span class="label label-info"><i class="icon-bullhorn"></i></span>
								<span class="message">New items are in queue.</span>
								<span class="time">25 mins</span>
							</a>
						</li>
						<li>
							<a href="javascript:void(0);">
								<span class="label label-warning"><i class="icon-bolt"></i></span>
								<span class="message">Disk space to 85% full.</span>
								<span class="time">55 mins</span>
							</a>
						</li>
						<li class="footer">
							<a href="javascript:void(0);">View all notifications</a>
						</li>
					</ul>
				</li>

				<!-- Tasks -->
				<li class="dropdown hidden-xs hidden-sm">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="icon-tasks"></i>
						<span class="badge">7</span>
					</a>
					<ul class="dropdown-menu extended notification">
						<li class="title">
							<p>You have 7 pending tasks</p>
						</li>
						<li>
							<a href="javascript:void(0);">
								<span class="task">
									<span class="desc">Preparing new release</span>
									<span class="percent">30%</span>
								</span>
								<div class="progress progress-small">
									<div style="width: 30%;" class="progress-bar progress-bar-info"></div>
								</div>
							</a>
						</li>
						<li>
							<a href="javascript:void(0);">
								<span class="task">
									<span class="desc">Change management</span>
									<span class="percent">80%</span>
								</span>
								<div class="progress progress-small progress-striped active">
									<div style="width: 80%;" class="progress-bar progress-bar-danger"></div>
								</div>
							</a>
						</li>
						<li>
							<a href="javascript:void(0);">
								<span class="task">
									<span class="desc">Mobile development</span>
									<span class="percent">60%</span>
								</span>
								<div class="progress progress-small">
									<div style="width: 60%;" class="progress-bar progress-bar-success"></div>
								</div>
							</a>
						</li>
						<li>
							<a href="javascript:void(0);">
								<span class="task">
									<span class="desc">Database migration</span>
									<span class="percent">20%</span>
								</span>
								<div class="progress progress-small">
									<div style="width: 20%;" class="progress-bar progress-bar-warning"></div>
								</div>
							</a>
						</li>
						<li class="footer">
							<a href="javascript:void(0);">View all tasks</a>
						</li>
					</ul>
				</li>

				<!-- Messages -->
				<li class="dropdown hidden-xs hidden-sm">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="icon-envelope"></i>
						<span class="badge">1</span>
					</a>
					<ul class="dropdown-menu extended notification">
						<li class="title">
							<p>You have 3 new messages</p>
						</li>
						<li>
							<a href="javascript:void(0);">
								<span class="photo"><img src="assets/img/demo/avatar-1.jpg" alt="" /></span>
								<span class="subject">
									<span class="from">Bob Carter</span>
									<span class="time">Just Now</span>
								</span>
								<span class="text">
									Consetetur sadipscing elitr...
								</span>
							</a>
						</li>
						<li>
							<a href="javascript:void(0);">
								<span class="photo"><img src="assets/img/demo/avatar-2.jpg" alt="" /></span>
								<span class="subject">
									<span class="from">Jane Doe</span>
									<span class="time">45 mins</span>
								</span>
								<span class="text">
									Sed diam nonumy...
								</span>
							</a>
						</li>
						<li>
							<a href="javascript:void(0);">
								<span class="photo"><img src="assets/img/demo/avatar-3.jpg" alt="" /></span>
								<span class="subject">
									<span class="from">Patrick Nilson</span>
									<span class="time">6 hours</span>
								</span>
								<span class="text">
									No sea takimata sanctus...
								</span>
							</a>
						</li>
						<li class="footer">
							<a href="javascript:void(0);">View all messages</a>
						</li>
					</ul>
				</li>

				<!-- .row .row-bg Toggler -->
				<li>
					<a href="#" class="dropdown-toggle row-bg-toggle">
						<i class="icon-resize-vertical"></i>
					</a>
				</li>


				<!-- User Login Dropdown -->
				<li class="dropdown user">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<!--<img alt="" src="assets/img/avatar1_small.jpg" />-->
						<i class="icon-male"></i>
						<span class="username" id="practiceName"></span>
						<i class="icon-caret-down small"></i>
					</a>
					<ul class="dropdown-menu">
						<li><a href="javascript:void(0);"><i class="icon-user"></i> My Profile</a></li>
						<li><a href="javascript:void(0);"><i class="icon-calendar"></i> My Calendar</a></li>
						<li><a href="javascript:void(0);"><i class="icon-tasks"></i> My Tasks</a></li>
						<li class="divider"></li>
						<li><a href="login.php"><i class="icon-key"></i> Log Out</a></li>
					</ul>
				</li>
				<!-- /user login dropdown -->
			</ul>
			<!-- /Top Right Menu -->
		</div>
		<!-- /top navigation bar -->

	</header> <!-- /.header -->

	<div id="container">

		<div id="content">
			<div class="container">
				<!-- Breadcrumbs line -->
                <div class="crumbs">
                  <ul id="breadcrumbs" class="breadcrumb">
                    <li class="current">
                      <i class="icon-home"></i>
                      <a href="index.html">Dashboard</a>
                    </li>
                    
                  </ul>
                  <a href="javascript:void(0);"><img src="assets/icons/Settings.png" title="Settings" style="float:right; padding-top:0.3em; padding-right:2em;"/></a>
					<a href="Ledger.php"><img src="assets/icons/Claim_search.png" title="Claim Search" style="float:right; padding-top:0.3em; padding-right:2em;"/></a>
					<a href="AddClaim.php"><img src="assets/icons/Claim_add.png" title="Add Claim" style="float:right; padding-top:0.3em; padding-right:2em;"/></a>
					<a href="ViewPatient.php"><img src="assets/icons/Patient_search.png" title="Patient Search" style="float:right; padding-top:0.3em; padding-right:2em;"/></a>
					<a href="AddPatient.php"><img src="assets/icons/Patient_add.png" title="Add Patient" style="float:right; padding-top:0.3em; padding-right:2em;"/></a>
					<a href="dashboard.html"><img src="assets/icons/Home.png" title="Home" style="float:right; padding-top:0.3em; padding-right:2em;"/></a>

                </div>
                <div class="clearfix"></div>
			    <div class="alert alert-warning fade in" style="margin-top:10px;" id="divAlert">
			        <i class="icon-remove close" data-dismiss="alert"></i>
			        <strong>Warning!</strong> <span id="alertTop">Your Payment Due is on 05/22/2016</span>
			    </div>
                <!-- /Breadcrumbs line -->


				<!--=== Page Content ===-->
				<!--=== Modals ===-->
				<div class="row">
					<img src="assets/beat.gif" id="loader" style="position:absolute; left:50%; z-index:99999; top:35%; width:160px; height:24px;" />
					<h2 style="color: #251367; margin-left:20px;">Claims List</h2>
					<div class="col-md-12" style="margin-top:20px;">
			        <div class="widget box" >
			            <div class="widget-content">
			            	<div class="tabbable tabbable-custom">
								<ul class="nav nav-tabs">
									<li class="active"><a href="#tab_1_1" data-toggle="tab">COMPLETED</a></li>
									<li><a href="#tab_1_2" data-toggle="tab">REVIEW</a></li>
									<li><a href="#tab_1_3" data-toggle="tab">DRAFT</a></li>
								</ul>
								<div class="tab-content">
									<div class="tab-pane active" id="tab_1_1">
										<table id="test1" class="display" cellspacing="0" width="100%">
										</table>
									</div>
									<div class="tab-pane" id="tab_1_2">
										<table id="test2" class="display" cellspacing="0" width="100%">
										</table>
									</div>
									<div class="tab-pane" id="tab_1_3">
										<table id="test3" class="display" cellspacing="0" width="100%">
										</table>
									</div>
								</div>
							</div>
			            </div>
			        </div>

				</div>

				</div>
				<!-- /Page Content -->
			</div>
			<!-- /.container -->

		</div>
	</div>
	<div class="modal fade" id="EditClaim" tabindex="-1">
		<div class="modal-dialog" style="width:70%;">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Edit Claim</h4>
				</div>
				<div class="modal-body" style="min-height:300px;">
					<input type="hidden" id="hdnClaimID" />
					<input type="hidden" id="hdnDX" />
                    <input type="hidden" id="hdnBal" />
                    <input type="hidden" id="hdnCaseChart" />
                    <input type="hidden" id="hdnCurrInsurance" />
                    <div class="form-group col-md-6" style="padding-left:100px;">
	                    <div class="make-switch" data-on="success" data-on-label="ICD-9" data-off-label="ICD-10" data-off="warning">
	                        <input type="checkbox" checked class="toggle" id="icd" />
	                    </div>
	                </div>
					<div class="form-group col-md-6">
                        <label class="control-label col-md-6">From </label>
                        <div class="col-md-6">
                            <input type="text" id="fromDt" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label class="control-label col-md-6">To </label>
                        <div class="col-md-6">
                            <input type="text" id="toDt" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label class="control-label col-md-6">CPT </label>
                        <div class="col-md-6">
                            <input type="text" id="proced" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label class="control-label col-md-6">Units </label>
                        <div class="col-md-6">
                            <input type="text" id="units" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label class="control-label col-md-6">Bill Date </label>
                        <div class="col-md-6">
                            <input type="text" id="billDt" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label class="control-label col-md-6">Billed Amount </label>
                        <div class="col-md-6">
                            <input type="text" id="total" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label class="control-label col-md-6">Allowed Amonut </label>
                        <div class="col-md-6">
                            <input type="text" id="allowed" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label class="control-label col-md-6">Paid </label>
                        <div class="col-md-6">
                            <input type="text" id="paid" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label class="control-label col-md-6">Adjustment </label>
                        <div class="col-md-6">
                            <input type="text" id="adjustment" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label class="control-label col-md-6">Balance </label>
                        <div class="col-md-6">
                            <input type="text" id="claimBalance" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label class="control-label col-md-6">Claim Status </label>
                        <div class="col-md-6">
                            <select class="form-control" id="claimStatus">
                            	<option value="DRAFT">DRAFT</option>
                            	<option value="REVIEW">REVIEW</option>
                            	<option value="COMPLETE">COMPLETE</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label class="control-label col-md-6">Diag 1 </label>
                        <div class="col-md-6">
                            <input type="text" id="diag1" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label class="control-label col-md-6">Diag 2 </label>
                        <div class="col-md-6">
                            <input type="text" id="diag2" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label class="control-label col-md-6">Diag 3 </label>
                        <div class="col-md-6">
                            <input type="text" id="diag3" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label class="control-label col-md-6">Diag 4 </label>
                        <div class="col-md-6">
                            <input type="text" id="diag4" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label class="control-label col-md-6">Provider </label>
                        <div class="col-md-6">
                            <input type="text" id="provider" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label class="control-label col-md-6">Location </label>
                        <div class="col-md-6">
                            <input type="text" id="location" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label class="control-label col-md-6">Copay </label>
                        <div class="col-md-6">
                            <input type="text" id="copay" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label class="control-label col-md-6">Case </label>
                        <div class="col-md-6" id="ddlCaseDiv"><select id="case" class="form-control"></select>
                        </div>
                        <div class="col-md-6"></div>
                        <div class="col-md-6" id="ddlDiv">
                        </div>
                    </div>
				</div>
				<div class="modal-footer">
					<input type="button" class="btn btn-default" data-dismiss="modal" value="Exit" />
					<input type="button" id="btnUpdateClaim" class="btn btn-primary" data-dismiss="modal" value="Save" />
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div>
	<link href="https://cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css" rel='stylesheet' type='text/css'>
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
	<script>
	function changeDateFormat(inputDate){  // expects Y-m-d
        var splitDate = inputDate.split('-');
        if(splitDate.count == 0){
            return null;
        }

        var year = splitDate[0];
        var month = splitDate[1];
        var day = splitDate[2].substr(0,2); 

        return month + '-' + day + '-' + year;
    }
    var convertDate = function(usDate) {
      var dateParts = usDate.split(/(\d{1,2})-(\d{1,2})-(\d{4})/);
      return dateParts[3] + "-" + dateParts[1] + "-" + dateParts[2];
    }
	</script>
</body>
</html>