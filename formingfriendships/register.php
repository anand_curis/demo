<!DOCTYPE html>
<html>
<head>
    <title>Login | AMROMED LLC</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="img/favicon.ico"/>
    <!-- Bootstrap -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <!-- end of bootstrap -->
    <!--page level css -->
    <link type="text/css" href="vendors/themify/css/themify-icons.css" rel="stylesheet"/>
    <link href="vendors/bootstrapvalidator/css/bootstrapValidator.min.css" rel="stylesheet"/>
    <link href="css/login.css" rel="stylesheet">
    <style type="text/css">
        body{
            color:#000;
        }
    </style>
    <!--end page level css-->
</head>

<body id="sign-in">
<div class="preloader">
    <div class="loader_img"><img src="img/loader.gif" alt="loading..." height="64" width="64"></div>
</div>
<div class="container">
    <div class="row">
        <div class="col-lg-4 col-10 col-sm-8 m-auto login-form">

                <h2 class="text-center logo_h2">
                    <img src="img/pages/clear_black.png" alt="Logo">
                </h2>

            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <form id="authentication" method="post" class="login_validator">
                            <div class="form-group">
                                <input type="text" class="form-control" style="padding:0.5rem 0.75rem; border-radius:15px;" id="txtFirstname" name="username"
                                       placeholder="First Name">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" style="padding:0.5rem 0.75rem; border-radius:15px;" id="txtLastname"
                                       name="password" placeholder="Last Name">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" style="padding:0.5rem 0.75rem; border-radius:15px;" id="txtUsername" name="username"
                                       placeholder="Username">
                            </div>
                            <div class="form-group">
                            	<select class="form-control" id="ddlRole" style="border-radius:15px; height:2.2rem">
                            		<option value="">--Select Role--</option>
                            		<option value="Admin">Admin</option>
                            		<option value="User">User</option>
                            		<option value="Manager">Manager</option>
                            		<option value="Provider">Provider</option>
                            	</select>
                            </div>
                            <div class="form-group">
                            	<select class="form-control" id="practiceList" style="border-radius:15px; height:2.2rem">
                            	</select>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" style="padding:0.5rem 0.75rem; border-radius:15px;" id="txtEmailID" name="username"
                                       placeholder="Email ID">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" style="padding:0.5rem 0.75rem; border-radius:15px;" id="txtMob" name="username"
                                       placeholder="Mobile #">
                            </div>
                            <div class="form-group">
                                <!-- <input type="button" value="Sign In" id="btnSignIn" class="btn btn-primary btn-block"/> -->
                                <img src="img/pages/arrow-right.png" id="btnAddUser" style="margin:0 50%; background-color: chocolate;border-radius: 15px;" alt="Go" width="30" height="30">
                            </div>
                            

                            <!-- <span class="float-right sign-up">New ? <a href="register.html">Sign Up</a></span> -->
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- global js -->
<script src="js/jquery.min.js" type="text/javascript"></script>

<script src="js/popper.min.js" type="text/javascript"></script>
<script src="js/bootstrap.min.js" type="text/javascript"></script>

<!-- end of global js -->
<!-- page level js -->

<script src="vendors/bootstrapvalidator/js/bootstrapValidator.min.js" type="text/javascript"></script>
<script type="text/javascript" src="js/custom_js/login.js"></script>

	<script>

	$(document).ready(function(){

		$.get("https://www.curismed.com/medService/practice", function(data, status){
	        //alert("Data: " + data + "\nStatus: " + status);
	        var list = [];
			for(var x in data){
			  list.push(data[x]);
			}
			//alert(arrCase);
			$.each(list,function(i,v) {
				$('#practiceList').html('<option value="">--Select Practice--</option>');
				list.forEach(function(t) { 
		            $('#practiceList').append('<option value="'+t.practiceID+'">'+t.practiceName+'</option>');
		        });
			});
	    });

		$('#btnAddUser').click(function(){

			var firstName = $("#txtFirstname").val();

			var lastName = $('#txtLastname').val();

			var userName = $('#txtUsername').val();

			var role = $('#ddlRole').val();
			var practiceID = $('#practiceList').val();

			var emailID = $('#txtEmailID').val();

			var mobileNo = $('#txtMob').val();

			$.ajax({

	            type: "POST",

	            url:"add_user.php",

	            data:{ 

	            	"firstName" : firstName,

	            	"lastName" : lastName,

	            	"userName" : userName,

	            	"role" : role,

	            	"emailID" : emailID,

	            	"mobileNo" : mobileNo,

	            	"practiceID" : practiceID,

	                },success:function(result){

	                	if(result=="exists"){

	                		alertify.error("Username already exists.")

	                	}

	                	else if(result=="success"){

	                		alertify.success("User created successfully.")

	                	}

	                	else if(result=="failed"){

	                		alertify.error("Problem in sending Email.")

	                	}

	                	window.location.reload();

	                 }



	        });

		});

	});

	</script>
</body>
</html>