
<!doctype html>
<head>
	<meta http-equiv="Content-type" content="text/html; charset=utf-8">
	<title>Basic initialization</title>
</head>
	<script src="Scheduler/dhtmlxscheduler.js" type="text/javascript" charset="utf-8"></script>
	<script src="Scheduler/dhtmlxscheduler_serialize.js" type="text/javascript" charset="utf-8"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.devbridge-autocomplete/1.2.27/jquery.autocomplete.min.js"></script>
	<link rel="stylesheet" href="Scheduler/dhtmlxscheduler_material.css" type="text/css" media="screen" title="no title" charset="utf-8">
	<link rel="stylesheet" href="Scheduler/dhtmlxCombo/dhtmlxcombo.css" type="text/css" media="screen" title="no title" charset="utf-8">
	<script src="Scheduler/dhtmlxCombo/dhtmlxcombo.js" type="text/javascript" charset="utf-8"></script>
	<script src="Scheduler/dhtmlxCombo/dhtmlxcommon.js" type="text/javascript" charset="utf-8"></script>
	<script src="Scheduler/sources/ext/dhtmlxscheduler_editors.js" type="text/javascript" charset="utf-8"></script>
	<script src="Scheduler/sources/ext/dhtmlxscheduler_limit.js" type="text/javascript" charset="utf-8"></script>
	<script src="Scheduler/sources/ext/dhtmlxscheduler_collision.js"></script>
	<script src="Scheduler/sources/ext/dhtmlxscheduler_recurring.js"></script>

	
<style type="text/css" media="screen">
		html, body {
			margin: 0px;
			padding: 0px;
			height: 100%;
			overflow: hidden;
		}
	</style>
<body onload="init();">
	<div id="scheduler_here" class="dhx_cal_container" style='width:100%; height:100%'>
		<div class="dhx_cal_navline">
			<div class="dhx_cal_prev_button">&nbsp;</div>
			<div class="dhx_cal_next_button">&nbsp;</div>
			<div class="dhx_cal_today_button"></div>
			<div class="dhx_cal_date"></div>
			<div class="dhx_cal_tab" name="day_tab" style="right:204px;"></div>
			<div class="dhx_cal_tab" name="week_tab" style="right:140px;"></div>
			<div class="dhx_cal_tab" name="month_tab" style="right:76px;"></div>
		</div>
		<div class="dhx_cal_header">
		</div>
		<div class="dhx_cal_data">
		</div>
	</div>
</body>

<script type="text/javascript" charset="utf-8">
	function init() {
		scheduler.config.multi_day = true;

			scheduler.config.event_duration = 30;
			scheduler.config.auto_end_date = true;
			scheduler.config.details_on_create = true;
			scheduler.config.details_on_dblclick = true;

			scheduler.config.first_hour = 9;
			scheduler.config.last_hour = 18;
			scheduler.config.hour_size_px = 400;
			scheduler.config.separate_short_events = true;

			scheduler.config.event_duration = 15; //specify event duration in minutes for auto end time
			scheduler.config.auto_end_date = true;
			scheduler.config.collision_limit = 2;
			scheduler.config.show_loading = true; 

			var step = 15;
			var format = scheduler.date.date_to_str("%H:%i");
			scheduler.config.hour_size_px=(60/step)*22;

			scheduler.templates.hour_scale = function(date){
			html1="";
			for (var i=0; i<60/step; i++){
				html1+="<div style='height:21px;line-height:21px;'>"+format(date)+"</div>";
				date = scheduler.date.add(date,step,"minute");
			}
			return html1;
			}

			var today = new Date();
			var dd = today.getDate();
			var mm = today.getMonth(); //January is 0!
			var yyyy = today.getFullYear();

			scheduler.init('scheduler_here', new Date(yyyy, mm, dd), "month");
			scheduler.config.separate_short_events = true;

			scheduler.attachEvent("onEventSave",function(id,ev){
				if (!ev.text) {
					dhtmlx.alert("Please select any Patient Name");
					return false;
				}
				else{
					var appID = sessionStorage.getItem("appID");
					if(appID == "" || appID == null){
						var truncID = "";
						var res = ev;
						var patientName = ev.text;
						var n1=patientName.indexOf(" -");
				        var remStr = patientName.substr(0,n1+3);
				        var patName = patientName.replace(remStr,"");
						var patID = patientName;

						var providerName = ev.doctor;
						var n11=providerName.indexOf(" - ");
				        var remStr1 = providerName.substr(0,n11+3);
				        var patName1 = providerName.replace(remStr1,"");
						var patID1 = providerName;
						var authNo = ev.auth;
						var activity = ev.activity1;

			            if(patID != ""){
				            var n=patID.indexOf(" -");
				            truncID = patID.substr(0,n);
				        }
				        if(patID1 != ""){
				            var n=patID1.indexOf(" -");
				            truncID1 = patID1.substr(0,n);
				        }
						var description = ev.description;
						var statusApp = ev.statusApp;
						var start_date = convertDt(ev.start_date);
						var end_date = convertDt(ev.end_date);
						$.ajax({
				          type: "POST",
				          url:"addAppointment.php",
				          data:{
				            patientName : patName,
				            description : description,
				            statusApp : statusApp,
				            start_date : start_date,
				            end_date : end_date,
				            patientID : truncID,
				            providerName : patName1,
				            providerID : truncID1,
				            authNo : authNo,
				            activity : activity
				          },success:function(result){
				          	alert("Appointment Added successfully");

				          }
				      	});
						return true;
					}
					else{
						var truncID = "";
						var res = ev;
						var patientName = ev.text;
						var n1=patientName.indexOf(" -");
				        var remStr = patientName.substr(0,n1+3);
				        var patName = patientName.replace(remStr,"");
						var patID = patientName;
			            var providerName = ev.doctor;
						var n11=providerName.indexOf(" - ");
				        var remStr1 = providerName.substr(0,n11+3);
				        var patName1 = providerName.replace(remStr1,"");
						var patID1 = providerName;
						var authNo = ev.auth;
						var activity = ev.activity1;

			            if(patID != ""){
				            var n=patID.indexOf(" -");
				            truncID = patID.substr(0,n);
				        }
				        if(patID1 != ""){
				            var n=patID1.indexOf(" -");
				            truncID1 = patID1.substr(0,n);
				        }
						var description = ev.description;
						var statusApp = ev.statusApp;
						var start_date = convertDt(ev.start_date);
						var end_date = convertDt(ev.end_date);
						$.ajax({
				          type: "POST",
				          url:"updAppointment.php",
				          data:{
				          	appID : appID,
				            patientName : patName,
				            description : description,
				            statusApp : statusApp,
				            start_date : start_date,
				            end_date : end_date,
				            patientID : truncID,
				            providerName : patName1,
				            providerID : truncID1,
				            authNo : authNo,
				            activity : activity
				          },success:function(result){
				          	alert("Appointment Updated successfully");
				          	sessionStorage.removeItem("appID");
				          }
				      	});
						return true;
					}
				}
			});

			scheduler.templates.event_class=function(start, end, event){
				var css = "";

				if(event.statusApp) // if event has statusApp property then special class should be assigned
					css += "event_"+event.statusApp;

				if(event.id == scheduler.getState().select_id){
					css += " selected";
				}

				return css; 
			};

			scheduler.config.xml_date = "%Y-%m-%d %H:%i";
			var appointments = <?php include('appointments.php'); ?>;

			scheduler.parse(appointments,"json");

			// scheduler.templates.event_class=function(start, end, event){
			// 	var css = "";

			// 	if(event.statusApp) // if event has statusApp property then special class should be assigned
			// 		css += "event_"+event.statusApp;

			// 	if(event.id == scheduler.getState().select_id){
			// 		css += " selected";
			// 	}

			// 	return css; 
			// };

			scheduler.attachEvent("onViewChange", function (new_mode , new_date){
				scheduler.templates.event_class=function(start, end, event){
					var css = "";

					if(event.statusApp) // if event has statusApp property then special class should be assigned
						css += "event_"+event.statusApp;

					if(event.id == scheduler.getState().select_id){
						css += " selected";
					}

					return css; 
				};
			    return true;
			});
			scheduler._click.buttons.details = function(id){
				sessionStorage.setItem("appID",id);
				scheduler.showLightbox(id);
				scheduler.formSection('auth').setValue('241666');
			};

			var statusApp = [
				{ key: 'Confirm', label: 'Confirmed' },
				{ key: 'UnConfirm', label: 'UnConfirmed' },
				{ key: 'NoShow', label: 'No Show' }
			];

			var auth = [];
			var activity1 = [];

			var client_onchange = function(options) {
				var val = ($(".dhx_combo_box").children())[1].value;
				var pos = val.indexOf(" ");
				var patientID = val.substr(0,pos);
				$.ajax({
		            type: "POST",
		            url:"getAuthbyPatient.php",
		            data:{
		              patientID : patientID
		            },success:function(result){
		            	result = JSON.parse(result);
		            	update_auth_options(scheduler.formSection('Auth').control, result);
		            }
		          });
			};

			var update_auth_options = function(select, options) { // helper function
				select.options.length = 0;
				select[0] = new Option("--Select Auth--", "");
				for (var i=0; i<options.length; i++) {
					var option = options[i];
					select[i+1] = new Option(option.label, option.key);
				}
			};

			var auth_onchange = function(options) {
				var authNo = $(".dhx_cal_ltext")[0].children[0].value;
				$.ajax({
		            type: "POST",
		            url:"getActivitySch.php",
		            data:{
		              authNo : authNo
		            },success:function(result){
		            	result = JSON.parse(result);
		            	update_activity_options(scheduler.formSection('Activity').control, result);
		            }
		          });
			};

			var update_activity_options = function(select, options) { // helper function
				select.options.length = 0;
				select[0] = new Option("--Select Activity--", "");
				for (var i=0; i<options.length; i++) {
					var option = options[i];
					select[i+1] = new Option(option.label, option.key);
				}
			};



			scheduler.locale.labels.section_snack = "Patient Name :";
			scheduler.locale.labels.section_doctors = "Provider Name :";

			scheduler._click.buttons.edit = function(id,text){
				var patientId = scheduler._events[id].text;
	            if(patientId != ""){
		            var n=patientId.indexOf(" -");
		            var truncID = patientId.substr(0,n);
		            sessionStorage.setItem("patientId", truncID);
			         window.location.href ="EditPatient.php";
		        }
			};

			scheduler.attachEvent("onEventDeleted", function(id){
			    $.ajax({
		          type: "POST",
		          url:"delAppointment.php",
		          data:{
		            id : id
		          },success:function(result){
		          	window.location.reload();
		          }
		      	});
			});

			var patientList = <?php include('getPat.php'); ?>;
			var providerList = <?php include('getPhysician.php'); ?>;

			scheduler.config.lightbox.sections = [
				{ name: "snack", options: patientList, map_to: "text", type: "combo", image_path: "../common/dhtmlxCombo/imgs/", height:30, filtering: true, onchange:client_onchange },
				{ name: "Auth", height: 50, options: auth, map_to: "auth", type: "select", focus: true, onchange:auth_onchange },
				{ name: "Activity", height: 50, options: activity1, map_to: "activity1", type: "select", focus: true },
				{ name: "doctors", options: providerList, map_to: "doctor", type: "combo", image_path: "../common/dhtmlxCombo/imgs/", height:30, filtering: true },
				{ name: "Status", height: 50, options: statusApp, map_to: "statusApp", type: "select", focus: true },
				{ name: "description", height: 50, map_to: "description", type: "textarea", focus: true },
				{ name:"recurring", height:115, type:"recurring", map_to:"rec_type", button:"recurring"},
				{ name: "time", height: 72, type: "time", map_to: "auto"}
			];
		
	}
	function convertDt(i){
		 var res = i.toLocaleDateString();
		 var yy = i.getFullYear();
		 var mm = i.getMonth()+1;
		 var dd = i.getDate();
		 var hours = i.getHours();
		 var min = i.getMinutes();
		 return yy+"-"+mm+"-"+dd+" "+hours+":"+min+":00";
		}
</script>
</html>