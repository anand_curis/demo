<?php
 session_start();

 if (!isset($_SESSION['userID'])) {
    header("Location: index.html");
 }
 if($_SESSION['uName'] == "chaks"){
    header("Location: index.html");
}
 include('header.html');
 ?>
  
  
  

    <div id="content">
      <!-- <img src="assets/beat.gif" id="loader" style="position:absolute; left:50%; top:30%;" /> -->
      <h3 style="margin:20px 20px 20px 20px;">Reports</h3>
      <div class="card-body">
        <div class="row">
          <div class="col-sm-4">
              <div class="form-group row">
                  <div class="col-5">
                    <label for="input-text" class="control-label float-right txt_media1">Report Type </label>
                  </div>
                  <div class="col-7">
                    <select class="form-control" id="reportType">
                      <option value="">--Report Type--</option>
                      <option value="auth">Account Legder</option>
                      <option value="deposit">Auth Report</option>
                      <option value="payment">Deposit Report</option>
                      <option value="scheduler">Deposit Detail Report</option>
                      <option value="insurance">Ledger by Insurance</option>
                      <option value="client">Ledger by Client</option>
                      <option value="scheduler">Scheduler</option>
                      <option value="scheduler">AR Comprehensive Report </option>
                      <option value="scheduler">Employee Details Report</option>
                      <option value="scheduler">Payroll Session by Staff</option>
                      <option value="scheduler">Insurance Rate List</option>
                      <option value="scheduler">Timesheet Report</option>
                    </select>
                  </div>
              </div>
          </div>
          <div class="col-sm-4">
              <div class="form-group row">
                  <div class="col-5">
                    <label for="input-text" class="control-label float-right txt_media1">Filter From </label>
                  </div>
                  <div class="col-7">
                    <input type="text" class="form-control" id="filterFrom" />
                  </div>
              </div>
          </div>
          <div class="col-sm-4">
              <div class="form-group row">
                  <div class="col-5">
                    <label for="input-text" class="control-label float-right txt_media1">Filter To </label>
                  </div>
                  <div class="col-7">
                    <input type="text" class="form-control" id="filterTo" />
                  </div>
              </div>
          </div>
          <div class="col-sm-4">
              
          </div>
          <div class="col-sm-12" style="padding-top:5%">
              
          </div>
          <div class="form-group col-md-2" >
              
              <!-- <input type="button" class="btn btn-primary float-right" value="AR Insurance Report" id="btn3"/> -->
          </div>
          <div class="form-group col-md-2" >
              <input type="button" class="btn btn-primary float-right" value="Deposit Report" id="btn5"/>
          </div>
          <div class="form-group col-md-2" >
              <input type="button" class="btn btn-primary float-right" value="Deposit Detail Report" id="btn6"/>
          </div>

        </div>
      </div>
      <div class="col-md-12" style="margin-top:20px;">
        <div class="card-body">
          <div class="row">
            <div class="col-sm-4">
              <div class="form-group row">
                  <div class="col-5">
                    <label for="input-text" class="control-label float-right txt_media1"> </label>
                  </div>
                  <div class="col-7">
                  </div>
              </div>
            </div>
            <div class="col-sm-4" id="Divpayer">
              <div class="form-group row">
                  <div class="col-5">
                    <label for="input-text" class="control-label float-right txt_media1">Payer Name </label>
                  </div>
                  <div class="col-7">
                    <input type="text" class="form-control" id="payerName" />
                  </div>
              </div>
            </div>
            <div class="form-group col-md-4" >
            </div>
          </div>
        </div>
      </div>

      <table id="test1" class="table table-bordered display"  cellspacing="0" width="100%">
      </table>
      <table id="test2" class="table table-bordered display"  cellspacing="0" width="100%">
      </table>

    </div>
  </div>

  <script>
  $(window).load(function() {
    document.getElementById('practiceName').innerHTML = sessionStorage.getItem('practiceName');
    $('#filterFrom').datetimepicker({
        dayOfWeekStart: 1,
        lang: 'en',
        format: 'm-d-Y',
        timepicker: false,
        //minDate: '-2013/01/02',
        //maxDate: '+2014/12/31',
        formatDate: 'm-d-Y',
        closeOnDateSelect: true
    });
    $('#filterTo').datetimepicker({
        dayOfWeekStart: 1,
        lang: 'en',
        format: 'm-d-Y',
        timepicker: false,
        //minDate: '-2013/01/02',
        //maxDate: '+2014/12/31',
        formatDate: 'm-d-Y',
        closeOnDateSelect: true
    });
    $("#Divpayer").hide();
    $("#test1").hide();
    $("#test2").hide();
     var insurances = <?php include('insuranceList1.php') ?>;
    $("#payerName").autocomplete({
        source: insurances,
        autoFocus:true
    });

    $("#reportType").change(function(){
      if(($("#reportType").val() != "insurance") || ($("#reportType").val() != "client")  ){
        $("#Divpayer").hide();
      }
      else{
        $("#Divpayer").show();
      }
    });

      $("#btn5").click(function(){
        $("#test2_wrapper").hide();
        $("#test2").hide();
        $("#test1_wrapper").show();
        $("#test1").show();
        var filterFrom = convertDate($("#filterFrom").val());
        var filterTo = convertDate($("#filterTo").val());
        // var payerName = $("#payerName").val();
        // var n1=payerName.indexOf("-");
        // payerName = payerName.substr(0,n1-1);
        $.ajax({
            type: "POST",
            url:"report5.php",
            data:{
              filterFrom : filterFrom,
              filterTo : filterTo,
              //payerName : payerName
            },success:function(result){
              var data1 = JSON.parse(result);
              var dt = [];
              $.each(data1,function(i,v) {
                  dt.push([data1[i].depositID,data1[i].depositDate,data1[i].payerName,data1[i].chequeNo,data1[i].chequeDate,data1[i].amount,data1[i].unapplied,data1[i].created_At]);
              });
              $('#test1').DataTable({
                  "aaSorting": [[ 0, "desc" ]],
                  "destroy": true,
                  "data" : dt,
                    dom: 'Bfrtip',
                   buttons: [
                      'csv', 'excel', 'pdf', 'print'
                   ], 
                  
                  columns: [
                      {"title": "Deposit ID","visible" : false},
                      {"title": "Deposit Date",
                          "render": function ( data, type, full, meta ) {
                            return changeDateFormat(data);
                          }
                      },
                      {"title": "Payor Name"},
                      {"title": "Cheque #"},
                      {"title": "Cheque Date",
                          "render": function ( data, type, full, meta ) {
                            return changeDateFormat(data);
                          }
                      },
                      {"title": "Amount"},
                      {"title": "Unapplied"},
                      {"title": "Created Date",
                          "render": function ( data, type, full, meta ) {
                            return changeDateFormat(data);
                          }
                      }
                  ]
              });
            }
        });
      });
      
      $("#btn6").click(function(){
        $("#test1_wrapper").hide();
        $("#test1").hide();
        $("#test2_wrapper").show();
        $("#test2").show();
        var filterFrom = convertDate($("#filterFrom").val());
        var filterTo = convertDate($("#filterTo").val());
        // var payerName = $("#payerName").val();
        // var n1=payerName.indexOf("-");
        // payerName = payerName.substr(0,n1-1);
        $.ajax({
            type: "POST",
            url:"report6.php",
            data:{
              filterFrom : filterFrom,
              filterTo : filterTo,
              //payerName : payerName
            },success:function(result){
              var data1 = JSON.parse(result);
              var dt = [];
              $.each(data1,function(i,v) {
                  dt.push([data1[i].depositID,data1[i].depositDate,data1[i].payorType,data1[i].payerName,data1[i].chequeNo,data1[i].chequeDate,data1[i].amount,data1[i].amountAdjusted,data1[i].unapplied,data1[i].fullName,data1[i].fromDt,data1[i].created_At]);
              });
              $('#test2').DataTable({
                  "aaSorting": [[ 0, "desc" ]],
                  "destroy": true,
                  "data" : dt,
                    dom: 'Bfrtip',
                   buttons: [
                      'csv', 'excel', 'pdf', 'print'
                   ], 
                  
                  columns: [
                      {"title": "Deposit ID","visible" : false},
                      {"title": "Deposit Date",
                          "render": function ( data, type, full, meta ) {
                            return changeDateFormat(data);
                          }
                      },
                      {"title": "Payor Type"},
                      {"title": "Payor Name"},
                      {"title": "Cheque #"},
                      {"title": "Cheque Date",
                          "render": function ( data, type, full, meta ) {
                            return changeDateFormat(data);
                          }
                      },
                      {"title": "Deposit Amount"},
                      {"title": "Paid"},
                      {"title": "Unapplied"},
                      {"title": "Patient Name"},
                      {"title": "DOS",
                          "render": function ( data, type, full, meta ) {
                            return changeDateFormat(data);
                          }
                      },
                      {"title": "Created Date",
                          "render": function ( data, type, full, meta ) {
                            return changeDateFormat(data);
                          }
                      }
                  ]
              });
            }
        });
      });

      // $("#btn5").click(function(){
      // var filterFrom = convertDate($("#filterFrom").val());
      // var filterTo = convertDate($("#filterTo").val());
      // $.ajax({
      //     type: "POST",
      //     url:"report5.php",
      //     data:{
      //       filterFrom : filterFrom,
      //       filterTo : filterTo,
      //     },success:function(result){
      //       window.location.href="report5.php";
      //     }
      // });
      // });

  });

function changeDateFormat(inputDate){  // expects Y-m-d
        var splitDate = inputDate.split('-');
        
        if(splitDate.count == 0){
            return null;
        }

        var year = splitDate[0];
        var month = splitDate[1];
        var day = splitDate[2]; 

        var consolidated = month + '-' + day + '-' + year;
       
        return consolidated;
    }
function pad(number, length) {
   
    var str = '' + number;
    while (str.length < length) {
        str = '0' + str;
    }
   
    return str;

}
  var convertDate = function(usDate) {
        if(usDate != "" && usDate != null && usDate != undefined){
            var dateParts = usDate.split(/(\d{1,2})-(\d{1,2})-(\d{4})/);
            return dateParts[3] + "-" + dateParts[1] + "-" + dateParts[2];
        }
    }
  </script>
  <style>
  .icon:hover{
    text-decoration: none;
  }
  a:hover{
    text-decoration: none;
  }
  </style>
  <?php
    include('footer.html');
 ?>