<?php
 session_start();

 if (!isset($_SESSION['userID'])) {
    header("Location: index.html");
 } 
 include('header.html');
 ?>
<div class="col-12">
    <div class="card ">
        
        <div class="card-body">
            <div class="stepwizard">
                <div class="stepwizard-row setup-card">
                    <div class="stepwizard-step">
                        <a href="#step-1" id="up1" class="btn btn-primary btn-block">1</a>
                        <p><strong>Demographics</strong></p>
                    </div>
                    <div class="stepwizard-step">
                        <a href="#step-2" id="up2" class="btn btn-default btn-block">2</a>
                        <p><strong>Insurance Info</strong></p>
                    </div>
                    <div class="stepwizard-step">
                        <a href="#step-3" id="up3" class="btn btn-default btn-block">3</a>
                        <p><strong>Activity Info</strong></p>
                    </div>
                </div>
            </div>
            <form role="form">
                <div class="row setup-content" id="step-1">
                    <div class="col-12">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="">
                                        <input type="hidden" id="hdnChkPat" />
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <label class="control-label" ><span style="color:orange "><u>Demographics</u></span></label>
                                            </div>
                                            <!-- <div class="col-sm-2">
                                                <div class="form-group row">
                                                    <div class="col-12">
                                                        <select class="form-control" style="font-size:12px;" id="nameTitle">
                                                            <option>-Title-</option>
                                                            <option>Dr.</option>
                                                            <option>Miss.</option>
                                                            <option>Mr.</option>
                                                            <option>Ms.</option>
                                                            <option>Mrs.</option>
                                                            <option>Prof.</option>
                                                            <option>Rev.</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div> -->
                                            <div class="col-sm-4">
                                                <div class="form-group row">
                                                    <div class="col-12">
                                                        <input type="text" class="form-control validateGeneral" id="patFirstName" name="txtFirstName" placeholder="First Name" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group row">
                                                    <div class="col-12">
                                                        <input type="text" class="form-control" id="patMiddleName" name="txtMiddleName" placeholder="Middle Name" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group row">
                                                    <div class="col-12">
                                                        <input type="text" class="form-control validateGeneral" id="patLastName" name="txtLastName" placeholder="Last Name" />
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- <div class="col-sm-2">
                                                <div class="form-group row">
                                                    <div class="col-12">
                                                        <select class="form-control" style="font-size:12px;" id="txtSuffix">
                                                            <option>-Suffix-</option>
                                                            <option>I</option>
                                                            <option>II</option>
                                                            <option>III</option>
                                                            <option>IV</option>
                                                            <option>V</option>
                                                            <option>Jr.</option>
                                                            <option>Sr.</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div> -->
                                            <div class="col-sm-4">
                                                <div class="form-group row">
                                                    <div class="col-12">
                                                        <select class="form-control" id="gender">
                                                            <option value="">--Gender--</option>
                                                            <option value="Male">Male</option>
                                                            <option value="Female">Female</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group row">
                                                    <div class="col-12">
                                                        <input type="text" class="form-control validateGeneral" id="dob" placeholder="Date Of Birth" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group row">
                                                    <div class="col-12">
                                                        <!-- <input type="text" class="form-control validateGeneral" id="ssn" onKeyUp="maskSSN()" data-mask="999-99-9999" placeholder="Enter the Social Security #" /> -->
                                                        <input type="text" class="form-control validateGeneral" id="ssn" data-inputmask='"mask": "(999) 999-9999"' placeholder="Social Security #" data-mask/>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- <div class="col-sm-3">
                                                <div class="form-group row">
                                                    <div class="col-12">
                                                        <select class="form-control" style="font-size:12px;" style="font-size:12px;" id="employment">
                                                            <option>--Employment Status--</option>
                                                            <option>Employed</option>
                                                            <option>Self-Employed</option>
                                                            <option>Retired</option>
                                                            <option>Student. Full-time</option>
                                                            <option>Student. Part-time</option>
                                                            <option>Unknown</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div> -->
                                            <div class="col-12">
                                                <label class="control-label"><span style="color:orange"><u>Contact Information</u></span></label>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group row" >
                                                    <div class="col-12">
                                                        <input type="text" class="form-control validateGeneral"  id="addrStreet1" placeholder="Enter the Street address" value="" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group row">
                                                    <div class="col-12">
                                                        <input type="text" class="form-control required" id="addrStreet2" placeholder="Enter the Street address" value="" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group row">
                                                    <div class="col-12">
                                                        <input type="text" class="form-control validateGeneral" id="addrZip" placeholder="Enter the Zipcode" value="" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group row">
                                                    <div class="col-12">
                                                        <input type="text" class="form-control required" id="addrCity" disabled placeholder="City" value="" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group row">
                                                    <div class="col-12">
                                                        <input type="text" class="form-control required" id="addrState" disabled placeholder="State" value="" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group row">
                                                    <div class="col-12">
                                                        <input type="text" class="form-control" id="phoneHome" placeholder="Enter the Home Phone" />
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- <div class="col-sm-3">
                                                <div class="form-group row">
                                                    <div class="col-12">
                                                        <input type="text" class="form-control required" id="phoneWork" placeholder="Enter the Work Phone" />
                                                    </div>
                                                </div>
                                            </div> -->
                                            <div class="col-sm-3">
                                                <div class="form-group row">
                                                    <div class="col-12">
                                                        <input type="text" class="form-control required" id="phoneMobile" placeholder="Enter the Cell #" />
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- <div class="col-sm-3">
                                                <div class="form-group row" style="margin-bottom:1rem">
                                                    <div class="col-12">
                                                        <input type="checkbox" class="uniform" style="margin-left:5px;" value="" id="notifyEmail"> &nbsp;&nbsp;Send Email Notification
                                                    </div>
                                                </div>
                                            </div> -->
                                            <div class="col-sm-3">
                                                <div class="form-group row">
                                                    <div class="col-12">
                                                        <input type="text" class="form-control required"  id="email" placeholder="Enter the Email Address" value="" />
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- <div class="col-sm-6">
                                                <div class="form-group row">
                                                    <div class="col-12">
                                                        <input type="checkbox" class="uniform" value="" id="notifyPhone"> Enable Auto Phone call Remainders
                                                    </div>
                                                </div>
                                            </div> -->
                                            <div class="col-12">
                                                <label class="control-label" ><span style="color:orange"><u>Emergency Contact Information</u></span></label>
                                            </div>

                                            <div class="col-sm-4">
                                                <div class="form-group row">
                                                    <div class="col-12">
                                                        <input type="text" class="form-control" id="phoneEmerHome" placeholder="Enter the Home Phone" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group row">
                                                    <div class="col-12">
                                                        <input type="text" class="form-control" id="phoneEmerMobile" placeholder="Enter the Cell #" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-4" >
                                                <div class="form-group row">
                                                    <div class="col-12">
                                                        <input type="text" class="form-control"  id="Emeremail" placeholder="Enter the Email Address" value="" />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-sm-4">
                                                <div class="form-group row">
                                                    <div class="col-12">
                                                        <input type="text" class="form-control validateGeneral"  id="primaryCarePhy" name="DRenderProvider" placeholder="Enter the Rendering Provider" value="" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group row">
                                                    <div class="col-12">
                                                        <input type="checkbox" id="IsFinancialResp" /> <span style="color:green">If Guarantor is available?</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- <div class="col-sm-6">
                                                <div class="form-group row">
                                                    <div class="col-12">
                                                        <input type="text" class="form-control required" id="defaultServiceLoc" name="DServiceLoc" placeholder="Enter the Service Location" value="" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group row">
                                                    <div class="col-12">
                                                    </div>
                                                </div>
                                            </div> -->
                                            <div class="col-sm-4">
                                                <div class="form-group row">
                                                    <div class="col-12">
                                                        <label class="checkbox-inline icheckbox">
                                                            <input type="checkbox" class="uniform" id="homeChk" value="11"> Home
                                                        </label>
                                                        <label class="checkbox-inline icheckbox">
                                                            <input type="checkbox" class="uniform" id="officeChk" value="12"> Office
                                                        </label>
                                                        <label class="checkbox-inline icheckbox">
                                                            <input type="checkbox" class="uniform" id="schoolChk" value="3"> School
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <!-- <div class="col-6">
                                        <label class="control-label col-6" style="text-align:left;"><span style="color:black">Guarantor<span></label>
                                    </div>
                                    <div class="clearfix"></div>

                                    <div class="col-12">
                                        <div class="col-1"></div>
                                        <label class="checkbox-inline col-8">
                                            <input type="checkbox" class="uniform" value="" id="IsFinancialResp"> Personally Financially responsible(a.k.a. Guarantor) is different than Patient
                                        </label>
                                    </div> -->
                                    <input class="btn btn-primary nextBtn float-right" type="button" value="Next" id="btnNext1"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row setup-content insuranceinfo" id="step-2">
                    <div class="col-12">
                        <div class="">
                            <div id="accordion" role="tablist">
                                <input type="hidden" id="lblCaseNo" />
                                <input type="hidden" id="hdnPatientID" />
                                <input type="hidden" id="hdnChkCase" />
                                <div class="card">
                                    <div class="card-header" role="tab" id="headingTwo">
                                        <a id="acc1" data-toggle="collapse" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                                            Primary Insurance
                                        </a>
                                    </div>
                                    <div id="collapseTwo" class="collapse show" role="tabpanel" aria-labelledby="headingTwo" data-parent="#accordion">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <label class="control-label"><span style="color:orange"><u>Insurance Info</u></span></label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group row">
                                                        <div class="col-5">
                                                            <label for="input-text" class="control-label float-right txt_media1">Insurance</label>
                                                        </div>
                                                        <div class="col-7">
                                                            <input type="text" id="policyInsurance1" class="form-control validateGeneral1"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group row">
                                                        <div class="col-12">
                                                            <span id="NewspanAddress1" ></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group row">
                                                        <div class="col-5">
                                                            <label for="input-text" class="control-label float-right txt_media1">Client's Relationship</label>
                                                        </div>
                                                        <div class="col-7">
                                                            <select class="form-control validateGeneral1" id="policyRelationInsured1">
                                                                <option>Self</option>
                                                                <option>Spouse</option>
                                                                <option>Other</option>
                                                                <option>Child</option>
                                                                <option>Grandfather Or Grandmother</option>
                                                                <option>Grandson Or Granddaughter</option>
                                                                <option>Nephew Or Niece</option>
                                                                <option>Adopted Child</option>
                                                                <option>Foster Child</option>
                                                                <option>Stepson</option>
                                                                <option>Ward</option>
                                                                <option>Stepdaughter</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group row">
                                                        <div class="col-5">
                                                            <label for="input-text" class="control-label float-right txt_media1">Policy Holder</label>
                                                        </div>
                                                        <div class="col-7">
                                                            <input type="text" id="policyHolder1" class="form-control validateGeneral1" disabled/>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="col-sm-6">
                                                    <div class="form-group row">
                                                        <div class="col-5">
                                                            <label for="input-text" class="control-label float-right txt_media1">DOB</label>
                                                        </div>
                                                        <div class="col-7">
                                                            <input type="text" id="policyDOB1" class="form-control validateGeneral1" disabled/>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-6">
                                                    <div class="form-group row">
                                                        <div class="col-5">
                                                            <label for="input-text" class="control-label float-right txt_media1">Policy ID</label>
                                                        </div>
                                                        <div class="col-7">
                                                            <input type="text" id="policyNo1" class="form-control validateGeneral1"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group row">
                                                        <div class="col-5">
                                                            <label for="input-text" class="control-label float-right txt_media1">Policy Start</label>
                                                        </div>
                                                        <div class="col-7">
                                                            <input type="text" id="policyStartDt1" class="form-control validateGeneral1"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group row">
                                                        <div class="col-5">
                                                            <label for="input-text" class="control-label float-right txt_media1">Policy End</label>
                                                        </div>
                                                        <div class="col-7">
                                                            <input type="text" id="policyEndDt1" class="form-control validateGeneral1"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- <div class="col-sm-6">
                                                    <div class="form-group row">
                                                        <div class="col-5">
                                                            <label for="input-text" class="control-label float-right txt_media1">Total Auth Visits</label>
                                                        </div>
                                                        <div class="col-4">
                                                            <select class="form-control" style="font-size:12px;" style="font-size:12px;" id="totAuthType">
                                                                <option value=""></option>
                                                                <option value="Week">Week</option>
                                                                <option value="Total Auth">Total Auth</option>
                                                                <option value="Session">Session</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-3">
                                                            <input type="text" id="AuthtotalVisits" class="form-control validateGeneral1"/>
                                                        </div>
                                                    </div>
                                                </div> -->
                                                <div class="col-sm-6">
                                                    <div class="form-group row">
                                                        <div class="col-5">
                                                            <label for="input-text" class="control-label float-right txt_media1">Group ID </label>
                                                        </div>
                                                        <div class="col-7">
                                                            <input type="text" id="policyGroupNo1" class="form-control"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group row">
                                                        <div class="col-5">
                                                            <label for="input-text" class="control-label float-right txt_media1">Group Name</label>
                                                        </div>
                                                        <div class="col-7">
                                                            <input type="text" id="policyGroupName1" class="form-control"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group row">
                                                        <div class="col-12">
                                                            <div class="">
                                                                <div class="row">
                                                                    <div class="col-6">
                                                                        <input type="text" id="principalDiag" class="form-control validateGeneral1"  placeholder="Diagnosis 1"/>
                                                                    </div>
                                                                    <div class="col-6">
                                                                        <input type="text" id="defDiag2" class="form-control" placeholder="Diagnosis 2"/>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group row">
                                                        <div class="col-5">
                                                            <label for="input-text" class="control-label float-right txt_media1">Copay</label>
                                                        </div>
                                                        <div class="col-7">
                                                            <input type="text" id="policyCopay1" class="form-control"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group row">
                                                        <div class="col-12">
                                                            <div class="">
                                                                <div class="row">
                                                                    <div class="col-6">
                                                                        <input type="text" id="defDiag3" class="form-control"  placeholder="Diagnosis 3"/>
                                                                    </div>
                                                                    <div class="col-6">
                                                                        <input type="text" id="defDiag4" class="form-control" placeholder="Diagnosis 4"/>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group row">
                                                        <div class="col-5">
                                                            <label for="input-text" class="control-label float-right txt_media1">Deductible</label>
                                                        </div>
                                                        <div class="col-7">
                                                            <input type="text" id="policyDeduc1" class="form-control"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <label class="control-label" ><span style="color:orange"><u>Authorization Info</u></span></label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group row">
                                                        <div class="col-5">
                                                            <label for="input-text" class="control-label float-right txt_media1">Auth No</label>
                                                        </div>
                                                        <div class="col-7">
                                                            <input type="text" id="authNo1" class="form-control validateGeneral1"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group row">
                                                        <div class="col-5">
                                                            <label for="input-text" class="control-label float-right txt_media1">Type of Service</label>
                                                        </div>
                                                        <div class="col-7">
                                                            <select id="tos1" class="form-control validateGeneral1">
                                                                <option></option>
                                                                <option value="Behaviour Therapy">Behaviour Therapy</option>
                                                                <option value="Physical Therapy">Physical Therapy</option>
                                                                <option value="Occupational Therapy">Occupational Therapy</option>
                                                                <option value="Speech Therapy">Speech Therapy</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group row">
                                                        <div class="col-5">
                                                            <label for="input-text" class="control-label float-right txt_media1">Eff. Start Date</label>
                                                        </div>
                                                        <div class="col-7">
                                                           <input type="text" id="AuthstartDt1" class="form-control validateGeneral1"/> 
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group row">
                                                        <div class="col-5">
                                                            <label for="input-text" class="control-label float-right txt_media1">Eff. End Date</label>
                                                        </div>
                                                        <div class="col-7">
                                                            <input type="text" id="AuthendDt1" class="form-control validateGeneral1"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" role="tab" id="headingThree">
                                        <a class="collapsed" id="acc2" data-toggle="collapse" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                            Secondary Insurance
                                        </a>
                                    </div>
                                    <div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree" data-parent="#accordion">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group row">
                                                        <div class="col-5">
                                                            <label for="input-text" class="control-label float-right txt_media1">Insurance</label>
                                                        </div>
                                                        <div class="col-7">
                                                            <input type="text" id="policyInsurance2" class="form-control"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group row">
                                                        <div class="col-12">
                                                            <span id="NewspanAddress2"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group row">
                                                        <div class="col-5">
                                                            <label for="input-text" class="control-label float-right txt_media1">Client's Relationship</label>
                                                        </div>
                                                        <div class="col-7">
                                                            <select class="form-control" id="policyRelationInsured2">
                                                                <option>Self</option>
                                                                <option>Spouse</option>
                                                                <option>Other</option>
                                                                <option>Child</option>
                                                                <option>Grandfather Or Grandmother</option>
                                                                <option>Grandson Or Granddaughter</option>
                                                                <option>Nephew Or Niece</option>
                                                                <option>Adopted Child</option>
                                                                <option>Foster Child</option>
                                                                <option>Stepson</option>
                                                                <option>Ward</option>
                                                                <option>Stepdaughter</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group row">
                                                        <div class="col-5">
                                                            <label for="input-text" class="control-label float-right txt_media1">Policy Holder</label>
                                                        </div>
                                                        <div class="col-7">
                                                            <input type="text" id="policyHolder2" class="form-control"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group row">
                                                        <div class="col-5">
                                                            <label for="input-text" class="control-label float-right txt_media1">DOB</label>
                                                        </div>
                                                        <div class="col-7">
                                                            <input type="text" id="policyDOB2" class="form-control"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group row">
                                                        <div class="col-5">
                                                            <label for="input-text" class="control-label float-right txt_media1">Policy ID</label>
                                                        </div>
                                                        <div class="col-7">
                                                            <input type="text" id="policyNo2" class="form-control"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group row">
                                                        <div class="col-5">
                                                            <label for="input-text" class="control-label float-right txt_media1">Policy Start</label>
                                                        </div>
                                                        <div class="col-7">
                                                            <input type="text" id="policyStartDt2" class="form-control"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group row">
                                                        <div class="col-5">
                                                            <label for="input-text" class="control-label float-right txt_media1">Policy End</label>
                                                        </div>
                                                        <div class="col-7">
                                                            <input type="text" id="policyEndDt2" class="form-control"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group row">
                                                        <div class="col-5">
                                                            <label for="input-text" class="control-label float-right txt_media1">Group ID </label>
                                                        </div>
                                                        <div class="col-7">
                                                            <input type="text" id="policyGroupNo2" class="form-control"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group row">
                                                        <div class="col-5">
                                                            <label for="input-text" class="control-label float-right txt_media1">Group Name</label>
                                                        </div>
                                                        <div class="col-7">
                                                            <input type="text" id="policyGroupName2" class="form-control"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group row">
                                                        <div class="col-5">
                                                            <label for="input-text" class="control-label float-right txt_media1">Copay </label>
                                                        </div>
                                                        <div class="col-7">
                                                            <input type="text" id="policyCopay2" class="form-control"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group row">
                                                        <div class="col-5">
                                                            <label for="input-text" class="control-label float-right txt_media1">Deductible </label>
                                                        </div>
                                                        <div class="col-7">
                                                            <input type="text" id="policyDeduc2" class="form-control"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <label class="control-label" ><span style="color:orange"><u>Authorization Info</u></span></label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group row">
                                                        <div class="col-5">
                                                            <label for="input-text" class="control-label float-right txt_media1">Auth No</label>
                                                        </div>
                                                        <div class="col-7">
                                                            <input type="text" id="authNo2" class="form-control"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group row">
                                                        <div class="col-5">
                                                            <label for="input-text" class="control-label float-right txt_media1">Type of Service</label>
                                                        </div>
                                                        <div class="col-7">
                                                            <select id="tos2" class="form-control">
                                                                <option></option>
                                                                <option value="Behaviour Therapy">Behaviour Therapy</option>
                                                                <option value="Physical Therapy">Physical Therapy</option>
                                                                <option value="Occupational Therapy">Occupational Therapy</option>
                                                                <option value="Speech Therapy">Speech Therapy</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group row">
                                                        <div class="col-5">
                                                            <label for="input-text" class="control-label float-right txt_media1">Eff. Start Date</label>
                                                        </div>
                                                        <div class="col-7">
                                                           <input type="text" id="AuthstartDt2" class="form-control"/> 
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group row">
                                                        <div class="col-5">
                                                            <label for="input-text" class="control-label float-right txt_media1">Eff. End Date</label>
                                                        </div>
                                                        <div class="col-7">
                                                            <input type="text" id="AuthendDt2" class="form-control"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" role="tab" id="headingFour">
                                        <a class="collapsed" id="acc3" data-toggle="collapse" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                            Teritary Insurance
                                        </a>
                                    </div>
                                    <div id="collapseFour" class="collapse" role="tabpanel" aria-labelledby="headingFour" data-parent="#accordion">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group row">
                                                        <div class="col-5">
                                                            <label for="input-text" class="control-label float-right txt_media1">Insurance</label>
                                                        </div>
                                                        <div class="col-7">
                                                            <input type="text" id="policyInsurance3" class="form-control"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group row">
                                                        <div class="col-12">
                                                            <span id="NewspanAddress3" ></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group row">
                                                        <div class="col-5">
                                                            <label for="input-text" class="control-label float-right txt_media1">Client's Relationship</label>
                                                        </div>
                                                        <div class="col-7">
                                                            <select class="form-control"  id="policyRelationInsured3">
                                                                <option>Self</option>
                                                                <option>Spouse</option>
                                                                <option>Other</option>
                                                                <option>Child</option>
                                                                <option>Grandfather Or Grandmother</option>
                                                                <option>Grandson Or Granddaughter</option>
                                                                <option>Nephew Or Niece</option>
                                                                <option>Adopted Child</option>
                                                                <option>Foster Child</option>
                                                                <option>Stepson</option>
                                                                <option>Ward</option>
                                                                <option>Stepdaughter</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group row">
                                                        <div class="col-5">
                                                            <label for="input-text" class="control-label float-right txt_media1">Policy Holder</label>
                                                        </div>
                                                        <div class="col-7">
                                                            <input type="text" id="policyHolder3"  class="form-control"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group row">
                                                        <div class="col-5">
                                                            <label for="input-text" class="control-label float-right txt_media1">DOB</label>
                                                        </div>
                                                        <div class="col-7">
                                                            <input type="text" id="policyDOB3" class="form-control"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group row">
                                                        <div class="col-5">
                                                            <label for="input-text" class="control-label float-right txt_media1">Policy ID</label>
                                                        </div>
                                                        <div class="col-7">
                                                            <input type="text" id="policyNo3" class="form-control"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group row">
                                                        <div class="col-5">
                                                            <label for="input-text" class="control-label float-right txt_media1">Policy Start</label>
                                                        </div>
                                                        <div class="col-7">
                                                            <input type="text" id="policyStartDt3" class="form-control"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group row">
                                                        <div class="col-5">
                                                            <label for="input-text" class="control-label float-right txt_media1">Policy End</label>
                                                        </div>
                                                        <div class="col-7">
                                                            <input type="text" id="policyEndDt3" class="form-control"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group row">
                                                        <div class="col-5">
                                                            <label for="input-text" class="control-label float-right txt_media1">Group ID </label>
                                                        </div>
                                                        <div class="col-7">
                                                            <input type="text" id="policyGroupNo3" class="form-control"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group row">
                                                        <div class="col-5">
                                                            <label for="input-text" class="control-label float-right txt_media1">Group Name</label>
                                                        </div>
                                                        <div class="col-7">
                                                            <input type="text" id="policyGroupName3" class="form-control"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group row">
                                                        <div class="col-5">
                                                            <label for="input-text" class="control-label float-right txt_media1">Copay </label>
                                                        </div>
                                                        <div class="col-7">
                                                            <input type="text" id="policyCopay3" class="form-control"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group row">
                                                        <div class="col-5">
                                                            <label for="input-text" class="control-label float-right txt_media1">Deductible </label>
                                                        </div>
                                                        <div class="col-7">
                                                            <input type="text" id="policyDeduc3" class="form-control"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button class="btn btn-primary prevBtn pull-left" type="button">
                                Previous
                            </button>
                            <input class="btn btn-primary nextBtn float-right" type="button" value="Next" id="btnNext2"/>
                        </div>
                    </div>
                </div>
                <div class="row setup-content" id="step-3">
                    <div class="col-12">
                        <div class="">
                            <input type="hidden" id="hdnAuth" />
                            <input type="hidden" id="hdncaseID" />
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group row">
                                            <div class="col-5">
                                                <label for="input-text" class="control-label float-right txt_media1">Authorization </label>
                                            </div>
                                            <div class="col-7">
                                                <select class="form-control validateGeneral1" id="ddlAuth">
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group row">
                                            <div class="col-5">
                                                <label for="input-text" class="control-label float-right txt_media1">Activity </label>
                                            </div>
                                            <div class="col-7">
                                                <select class="form-control validateGeneral1" id="Activity">
                                                    <option value=""></option>
                                                    <option value="Assessment">Assessment</option>
                                                    <option value="Direct Behavior Theraphy">Direct Behavior Theraphy</option>
                                                    <option value="Supervision">Supervision</option>
                                                    <option value="Supervision Parent Training">Supervision Parent Training</option>
                                                    <option value="Report Writing Send to Editor">Report Writing Send to Editor</option>
                                                    <option value="Report Writing Send to Insurance">Report Writing Send to Insurance</option>
                                                    <option value="Report Writing Progress Reports">Report Writing Progress Reports</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group row">
                                            <div class="col-5">
                                                <label for="input-text" class="control-label float-right txt_media1">CPT Code </label>
                                            </div>
                                            <div class="col-7">
                                                <select class="form-control validateGeneral1" id="ActCPT">
                                                    <option value=""></option>
                                                    <option value="0359T">0359T</option>
                                                    <option value="0360T">0360T</option>
                                                    <option value="0361T">0361T</option>
                                                    <option value="0364T">0364T</option>
                                                    <option value="0365T">0365T</option>
                                                    <option value="0368T">0368T</option>
                                                    <option value="0369T">0369T</option>
                                                    <option value="0370T">0370T</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group row">
                                            <div class="col-5">
                                                <label for="input-text" class="control-label float-right txt_media1">Modifiers </label>
                                            </div>
                                            <div class="col-7">
                                                <div class="row">
                                                    <div class="col-3" >
                                                        <input type="text" id="ActMod1" class="form-control" maxlength="2"/> 
                                                    </div>
                                                    <div class="col-3" >
                                                        <input type="text" id="ActMod2" class="form-control" maxlength="2"/> 
                                                    </div>
                                                    <div class="col-3" >
                                                        <input type="text" id="ActMod3" class="form-control" maxlength="2"/> 
                                                    </div>
                                                    <div class="col-3" >
                                                        <input type="text" id="ActMod4" class="form-control" maxlength="2"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group row">
                                            <div class="col-5">
                                                <label for="input-text" class="control-label float-right txt_media1">Billed per </label>
                                            </div>
                                            <div class="col-7">
                                                <select class="form-control validateGeneral1"  id="ActPer">
                                                    <option value=""></option>
                                                    <option value="15 mins">15 mins</option>
                                                    <option value="30 mins">30 mins</option>
                                                    <option value="Hour">Hour</option>
                                                    <option value="Session">Session</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group row">
                                            <div class="col-5">
                                                <label for="input-text" class="control-label float-right txt_media1">Units or Hours? </label>
                                            </div>
                                            <div class="col-7">
                                                <select class="form-control validateGeneral1" id="ddlApprove">
                                                    <option value=""></option>
                                                    <option value="Approved Units">Approved Units</option>
                                                    <option value="Approved Hours">Approved Hours</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group row">
                                            <div class="col-5">
                                                <label for="input-text" class="control-label float-right txt_media1" id="lblUnits"> </label>
                                            </div>
                                            <div class="col-7">
                                                <input type="text" id="calculate" class="form-control validateGeneral1"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-6" id="divHours">
                                        <div class="form-group row">
                                            <div class="col-5">
                                                <label for="input-text" class="control-label float-right txt_media1">Auth Type </label>
                                            </div>
                                            <div class="col-7">
                                                <select class="form-control" id="ddlApproveHours">
                                                    <option value=""></option>
                                                    <option value="Weekly">Weekly</option>
                                                    <option value="Monthly">Monthly</option>
                                                    <option value="Total Auth">Total Auth</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group row">
                                            <div class="col-5">
                                                <label for="input-text" class="control-label float-right txt_media1"> Total Units</label>
                                            </div>
                                            <div class="col-7">
                                                <input type="text" id="totAuth" class="form-control validateGeneral1"/>
                                            </div>
                                        </div>
                                    </div>
                                     <div class="col-sm-6">
                                        <div class="form-group row">
                                            <div class="col-5">
                                                <label for="input-text" class="control-label float-right txt_media1">Rate (s) </label>
                                            </div>
                                            <div class="col-7">
                                                <input type="text" id="ActRate" class="form-control validateGeneral1"/>
                                            </div>
                                        </div>
                                    </div>

                                   <!--  <div class="col-12">
                                        <label class="control-label col-6" style="text-align:left;"><span style="color:black">Maximum Frequency Allowed</span></label>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group row">
                                            <div class="col-4">
                                                <label for="input-text" class="control-label float-right txt_media1">Maximum </label>
                                            </div>
                                            <div class="col-8">
                                                <select class="form-control" style="font-size:12px;" id="ActMaxi1">
                                                    <option value="Unit">Unit</option>
                                                    <option value="Amount">Amount</option>
                                                    <option value="Sessions">Sessions</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group row">
                                            <div class="col-4">
                                                <label for="input-text" class="control-label float-right txt_media1">per </label>
                                            </div>
                                            <div class="col-8">
                                                <select class="form-control" style="font-size:12px;" id="ActPer1">
                                                    <option value="Day">Day</option>
                                                    <option value="Week">Week</option>
                                                    <option value="Month">Month</option>
                                                    <option value="Total Auth">Total Auth</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group row">
                                            <div class="col-4">
                                                <label for="input-text" class="control-label float-right txt_media1">Is </label>
                                            </div>
                                            <div class="col-8">
                                                <input type="text" id="ActIs1" class="form-control"/> And
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group row">
                                            <div class="col-4">
                                                <label for="input-text" class="control-label float-right txt_media1">Maximum </label>
                                            </div>
                                            <div class="col-8">
                                                <select class="form-control" style="font-size:12px;" id="ActMaxi2">
                                                    <option value="Unit">Unit</option>
                                                    <option value="Amount">Amount</option>
                                                    <option value="Sessions">Sessions</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group row">
                                            <div class="col-4">
                                                <label for="input-text" class="control-label float-right txt_media1">per </label>
                                            </div>
                                            <div class="col-8">
                                                <select class="form-control" style="font-size:12px;" id="ActPer2">
                                                    <option value="Day">Day</option>
                                                    <option value="Week">Week</option>
                                                    <option value="Month">Month</option>
                                                    <option value="Total Auth">Total Auth</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group row">
                                            <div class="col-4">
                                                <label for="input-text" class="control-label float-right txt_media1">Is </label>
                                            </div>
                                            <div class="col-8">
                                                <input type="text" id="ActIs2" class="form-control"/> And
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group row">
                                            <div class="col-4">
                                                <label for="input-text" class="control-label float-right txt_media1">Maximum </label>
                                            </div>
                                            <div class="col-8">
                                                <select class="form-control" style="font-size:12px;" id="ActMaxi3">
                                                    <option value="Unit">Unit</option>
                                                    <option value="Amount">Amount</option>
                                                    <option value="Sessions">Sessions</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group row">
                                            <div class="col-4">
                                                <label for="input-text" class="control-label float-right txt_media1">per </label>
                                            </div>
                                            <div class="col-8">
                                                <select class="form-control" style="font-size:12px;" id="ActPer3">
                                                    <option value="Day">Day</option>
                                                    <option value="Week">Week</option>
                                                    <option value="Month">Month</option>
                                                    <option value="Total Auth">Total Auth</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group row">
                                            <div class="col-4">
                                                <label for="input-text" class="control-label float-right txt_media1">Is </label>
                                            </div>
                                            <div class="col-8">
                                                <input type="text" id="ActIs3" class="form-control"/>
                                            </div>
                                        </div>
                                    </div> -->
                                   
                                    <div class="col-sm-8">
                                        <div class="form-group row">
                                            <div class="col-8">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group row">
                                            <div class="col-12">
                                                <input type="button" id="btnAddAct" value="Add Activity" class="btn btn-primary float-right"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <table id="activityTab" class="table-bordered" style="width:100%">
                                        </table>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group row">
                                            <div class="col-5">
                                                <label for="input-text" class="control-label float-right txt_media1"> </label>
                                            </div>
                                            <div class="col-7">
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary prevBtn pull-left" type="button">
                                    Previous
                                </button>
                                <a href="ViewPatient.php"><input class="button button-royal-flat  float-right" type="button" value="Finish" id="btnFinish"/></a>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        
        </div>
    </div>
</div>
    <div class="modal fade" id="guarantorModal" tabindex="-1" style="z-index:9999">
        <div class="modal-dialog modal-lg" style="width:58%; " >
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Add Guarantor</h4>
                </div>
                <div class="modal-body" style="min-height:150px;">
                    <div class="card-body">
                        <div class="row">
                            <div class="form-group col-md-6">
                            </div>
                            <div class="form-group col-md-6">
                                <input type="checkbox" id="getAddr" />Same as Client Address
                            </div>
                            <div class="form-group col-md-6">
                                <label class="control-label col-md-12">Guarantor FirstName</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control required" id="guarantorFirstName" name="txtFirstName" placeholder="Enter the First Name" />
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="control-label col-md-12">Guarantor MiddleName</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control required" id="guarantorMiddleName" name="txtFirstName" placeholder="Enter the Middle Name" />
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="control-label col-md-12">Guarantor LastName</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control required" id="guarantorLastName" name="txtFirstName" placeholder="Enter the Last Name" />
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="control-label col-md-12">DOB</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control required" id="guarantorDOB" name="txtFirstName" placeholder="Enter the DOB" />
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="control-label col-md-12">Street</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control required" id="guarantorAddrStreet" name="txtFirstName" placeholder="Enter the Street" />
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="control-label col-md-12">Zip code </label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control required" id="guarantorAddrZip" placeholder="Enter the Zipcode" />
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="control-label col-md-12">City</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control required" id="guarantorAddrCity" name="txtFirstName" placeholder="Enter the City" disabled />
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="control-label col-md-12">State </label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control required" id="guarantorAddrState" name="txtMiddleName" placeholder="Enter the State" disabled/>
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="control-label col-md-12">Phone </label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control required" id="guarantorPhoneHome" name="txtMiddleName" placeholder="Enter the Phone"/>
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="control-label col-md-12">Relationship to Guarantor </label>
                                <div class="col-md-12">
                                    <select class="form-control" id="guarantorRelation">
                                        <option></option>
                                        <option>Spouse</option>
                                        <option>Other</option>
                                        <option>Child</option>
                                        <option>Grandfather Or Grandmother</option>
                                        <option>Grandson Or Granddaughter</option>
                                        <option>Nephew Or Niece</option>
                                        <option>Adopted Child</option>
                                        <option>Foster Child</option>
                                        <option>Stepson</option>
                                        <option>Ward</option>
                                        <option>Stepdaughter</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="button" class="btn btn-default" data-dismiss="modal" value="Exit" />
                    <input type="button" id="btnSaveFullname" class="btn btn-primary" data-dismiss="modal" value="Save" />
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
<div class="modal fade" id="modalActivity" tabindex="-1" style="z-index:99999">
    <div class="modal-dialog modal-lg" style="width:75%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="ActTitleMod"></h4>
            </div>
            <div class="modal-body" style="min-height:350px;">
                <input type="hidden" id="hdnActID" />
                <input type="hidden" id="hdnFlagAct" />
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group row">
                                <div class="col-5">
                                    <label for="input-text" class="control-label float-right txt_media1">Authorization </label>
                                </div>
                                <div class="col-7">
                                    <select class="form-control" id="modalddlAuth">
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group row">
                                <div class="col-5">
                                    <label for="input-text" class="control-label float-right txt_media1">Activity </label>
                                </div>
                                <div class="col-7">
                                    <select class="form-control" id="Activity">
                                        <option value=""></option>
                                        <option value="Assessment">Assessment</option>
                                        <option value="Direct Behavior Theraphy">Direct Behavior Theraphy</option>
                                        <option value="Supervision">Supervision</option>
                                        <option value="Supervision Parent Training">Supervision Parent Training</option>
                                        <option value="Report Writing Send to Editor">Report Writing Send to Editor</option>
                                        <option value="Report Writing Send to Insurance">Report Writing Send to Insurance</option>
                                        <option value="Report Writing Progress Reports">Report Writing Progress Reports</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group row">
                                <div class="col-5">
                                    <label for="input-text" class="control-label float-right txt_media1">CPT Code </label>
                                </div>
                                <div class="col-7">
                                    <select class="form-control"  id="ActCPT">
                                        <option value=""></option>
                                        <option value="0359T">0359T</option>
                                        <option value="0360T">0360T</option>
                                        <option value="0361T">0361T</option>
                                        <option value="0364T">0364T</option>
                                        <option value="0365T">0365T</option>
                                        <option value="0368T">0368T</option>
                                        <option value="0369T">0369T</option>
                                        <option value="0370T">0370T</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group row">
                                <div class="col-5">
                                    <label for="input-text" class="control-label float-right txt_media1">Modifiers </label>
                                </div>
                                <div class="col-7">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-3" >
                                                <input type="text" id="ActMod1" class="form-control"/> 
                                            </div>
                                            <div class="col-3">
                                                <input type="text" id="ActMod2" class="form-control"/> 
                                            </div>
                                            <div class="col-3">
                                                <input type="text" id="ActMod3" class="form-control"/> 
                                            </div>
                                            <div class="col-3" >
                                                <input type="text" id="ActMod4" class="form-control"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group row">
                                <div class="col-5">
                                    <label for="input-text" class="control-label float-right txt_media1">Billed per </label>
                                </div>
                                <div class="col-7">
                                    <select class="form-control" id="ActPer">
                                        <option value="15 mins">15 mins</option>
                                        <option value="30 mins">30 mins</option>
                                        <option value="Hour">Hour</option>
                                        <option value="Session">Session</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group row">
                                <div class="col-5">
                                    <label for="input-text" class="control-label float-right txt_media1">Units or Hours? </label>
                                </div>
                                <div class="col-7">
                                    <select class="form-control" id="ddlApprove">
                                        <option value="Approved Units">Approved Units</option>
                                        <option value="Approved Hours">Approved Hours</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group row">
                                <div class="col-5">
                                    <label for="input-text" class="control-label float-right txt_media1" id="lblUnits"> </label>
                                </div>
                                <div class="col-7">
                                    <input type="text" id="calculate" class="form-control"/>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-2">
                            <div class="form-group row">
                                <div class="col-12" id="divHours">
                                    <select class="form-control" id="ddlApproveHours">
                                        <option value="Weekly">Weekly</option>
                                        <option value="Monthly">Monthly</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group row">
                                <div class="col-4">
                                    <label for="input-text" class="control-label float-right txt_media1"> Total Units</label>
                                </div>
                                <div class="col-8">
                                    <input type="text" id="totAuth" class="form-control"/>
                                </div>
                            </div>
                        </div>
                         <div class="col-sm-6">
                            <div class="form-group row">
                                <div class="col-5">
                                    <label for="input-text" class="control-label float-right txt_media1"> </label>
                                </div>
                                <div class="col-7">
                                </div>
                            </div>
                        </div>

                         <div class="col-sm-6">
                            <div class="form-group row">
                                <div class="col-5">
                                    <label for="input-text" class="control-label float-right txt_media1">Rate (s) </label>
                                </div>
                                <div class="col-7">
                                    <input type="text" id="ActRate" class="form-control"/>
                                </div>
                            </div>
                        </div>

                        <!-- <div class="col-12">
                            <label class="control-label col-6" style="text-align:left;"><span style="color:black">Maximum Frequency Allowed</span></label>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group row">
                                <div class="col-4">
                                    <label for="input-text" class="control-label float-right txt_media1">Maximum </label>
                                </div>
                                <div class="col-8">
                                    <select class="form-control" style="font-size:12px;" id="ActMaxi1">
                                        <option value="Unit">Unit</option>
                                        <option value="Amount">Amount</option>
                                        <option value="Sessions">Sessions</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group row">
                                <div class="col-4">
                                    <label for="input-text" class="control-label float-right txt_media1">per </label>
                                </div>
                                <div class="col-8">
                                    <select class="form-control" style="font-size:12px;" id="ActPer1">
                                        <option value="Day">Day</option>
                                        <option value="Week">Week</option>
                                        <option value="Month">Month</option>
                                        <option value="Total Auth">Total Auth</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group row">
                                <div class="col-4">
                                    <label for="input-text" class="control-label float-right txt_media1">Is </label>
                                </div>
                                <div class="col-8">
                                    <input type="text" id="ActIs1" class="form-control"/> And
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group row">
                                <div class="col-4">
                                    <label for="input-text" class="control-label float-right txt_media1">Maximum </label>
                                </div>
                                <div class="col-8">
                                    <select class="form-control" style="font-size:12px;" id="ActMaxi2">
                                        <option value="Unit">Unit</option>
                                        <option value="Amount">Amount</option>
                                        <option value="Sessions">Sessions</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group row">
                                <div class="col-4">
                                    <label for="input-text" class="control-label float-right txt_media1">per </label>
                                </div>
                                <div class="col-8">
                                    <select class="form-control" style="font-size:12px;" id="ActPer2">
                                        <option value="Day">Day</option>
                                        <option value="Week">Week</option>
                                        <option value="Month">Month</option>
                                        <option value="Total Auth">Total Auth</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group row">
                                <div class="col-4">
                                    <label for="input-text" class="control-label float-right txt_media1">Is </label>
                                </div>
                                <div class="col-8">
                                    <input type="text" id="ActIs2" class="form-control"/> And
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group row">
                                <div class="col-4">
                                    <label for="input-text" class="control-label float-right txt_media1">Maximum </label>
                                </div>
                                <div class="col-8">
                                    <select class="form-control" style="font-size:12px;" id="ActMaxi3">
                                        <option value="Unit">Unit</option>
                                        <option value="Amount">Amount</option>
                                        <option value="Sessions">Sessions</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group row">
                                <div class="col-4">
                                    <label for="input-text" class="control-label float-right txt_media1">per </label>
                                </div>
                                <div class="col-8">
                                    <select class="form-control" style="font-size:12px;" id="ActPer3">
                                        <option value="Day">Day</option>
                                        <option value="Week">Week</option>
                                        <option value="Month">Month</option>
                                        <option value="Total Auth">Total Auth</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group row">
                                <div class="col-4">
                                    <label for="input-text" class="control-label float-right txt_media1">Is </label>
                                </div>
                                <div class="col-8">
                                    <input type="text" id="ActIs3" class="form-control"/>
                                </div>
                            </div>
                        </div> -->
                        <div class="col-12">
                            <table id="activityTab" class="table-bordered" style="width:100%">
                            </table>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group row">
                                <div class="col-5">
                                    <label for="input-text" class="control-label float-right txt_media1"> </label>
                                </div>
                                <div class="col-7">
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <input type="button" class="btn btn-primary" id="btnAddActi" value="Update Activity" />
                <input type="button" class="btn btn-default" data-dismiss="modal" value="Exit" />
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<style type="text/css">
#step-1 .form-group{
    margin-bottom: 1rem;
}
</style>
<script type="text/javascript">
$(document).ready(function(){
    
    document.getElementById('practiceName').innerHTML = sessionStorage.getItem('practiceName');
    var practiceID = sessionStorage.getItem("practiceId");
    document.getElementById('hdnChkPat').value = "0";
    document.getElementById('hdnChkCase').value = "0";

    $(".navigation li").parent().find('li').removeClass("active");

    $("#navv1").addClass("active");

    $("#divHours").hide();
    $("#lblUnits").html("Units");
    $("#ddlApprove").change(function(){
        $("#calculate").val();
        if($(this).val()=="Approved Units"){
            $("#lblUnits").html("Units");
            $("#divUnits").show();
            $("#divHours").hide();
        }
        else{
            $("#lblUnits").html("Hours");
            $("#divHours").show();
        }
    })
    $('#IsFinancialResp').change(function(){
        if($('#IsFinancialResp').is(':checked') == true){
            $('#guarantorModal').modal('show');
            $("#guarantorFirstName").focus();
        }
        else{
            $('#guarantorModal').modal('hide');
        }
    })

    $('body').on('shown.bs.modal', '#guarantorModal', function () {
        $("#guarantorFirstName").focus();
    })
    $("#ActPer").change(function(){
        if($(this).val() == "Session"){
            $("#lblUnits").hide();
            $("#divHours").show();
        }
        else{
            if($("#calculate").val() != ""){
                var hours = document.getElementById("ddlApproveHours").value;
                if(hours == "Weekly"){
                    var date1 = "";
                    var date2 = "";
                    var authAuth = $("#ddlAuth").val();
                    $.ajax({
                    type: "POST",
                    url:"getAuthinfo.php",
                    async: false,
                    data:{
                        authNo : authAuth
                    },
                    success:function(data){
                      var n = data.length;
                      data = data.substr(0,n-1);
                      var res = JSON.parse(data);
                      if(res[0].startDt != ""){date1 = res[0].startDt;}
                      if(res[0].endDt != ""){date2 = res[0].endDt;}
                    }
                    });
                    var splitDate = date1.split('-');
                    var splitDate1 = date2.split('-');

                    date1 = new Date(splitDate[0],splitDate[1],splitDate[2]); 
                    date2 = new Date(splitDate1[0],splitDate1[1],splitDate1[2]);
                    var timeDiff = Math.abs(date2.getTime() - date1.getTime());
                    var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
                    var tem = diffDays / 7;
                    tem = document.getElementById("calculate").value * tem;
                    if(document.getElementById("ActPer").value == "15 mins"){
                        tem = tem*4;
                    }
                    else if(document.getElementById("ActPer").value == "30 mins"){
                        tem = tem*2;
                    }
                    else {
                        tem = tem*1;
                    }
                    document.getElementById("totAuth").value = Math.floor(tem);
                }
                else if(hours == "Monthly"){
                    var date1 = "";
                    var date2 = "";
                    var authAuth = $("#ddlAuth").val();
                    $.ajax({
                    type: "POST",
                    url:"getAuthinfo.php",
                    async: false,
                    data:{
                        authNo : authAuth
                    },
                    success:function(data){
                      var n = data.length;
                      data = data.substr(0,n-1);
                      var res = JSON.parse(data);
                      if(res[0].startDt != ""){date1 = res[0].startDt;}
                      if(res[0].endDt != ""){date2 = res[0].endDt;}
                    }
                    });
                    var splitDate = date1.split('-');
                    var splitDate1 = date2.split('-');

                    date1 = new Date(splitDate[0],splitDate[1],splitDate[2]); 
                    date2 = new Date(splitDate1[0],splitDate1[1],splitDate1[2]);
                    var timeDiff = Math.abs(date2.getTime() - date1.getTime());
                    var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
                    var tem = diffDays / 30;
                    tem = document.getElementById("calculate").value * tem;
                    if(document.getElementById("ActPer").value == "15 mins"){
                        tem = tem*4;
                    }
                    else if(document.getElementById("ActPer").value == "30 mins"){
                        tem = tem*2;
                    }
                    else {
                        tem = tem*1;
                    }
                    document.getElementById("totAuth").value = Math.floor(tem);
                }
                else if(hours=="Monthly"){
                    var date1 = "";
                    var date2 = "";
                    var authAuth = $("#ddlAuth").val();
                    $.ajax({
                    type: "POST",
                    url:"getAuthinfo.php",
                    async: false,
                    data:{
                        authNo : authAuth
                    },
                    success:function(data){
                      var n = data.length;
                      data = data.substr(0,n-1);
                      var res = JSON.parse(data);
                      if(res[0].startDt != ""){date1 = res[0].startDt;}
                      if(res[0].endDt != ""){date2 = res[0].endDt;}
                    }
                    });
                    var splitDate = date1.split('-');
                    var splitDate1 = date2.split('-');

                    date1 = new Date(splitDate[0],splitDate[1],splitDate[2]); 
                    date2 = new Date(splitDate1[0],splitDate1[1],splitDate1[2]);
                    var timeDiff = Math.abs(date2.getTime() - date1.getTime());
                    var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
                    var tem = diffDays / 30;
                    tem = document.getElementById("calculate").value * tem;
                    if(document.getElementById("ActPer").value == "15 mins"){
                        tem = tem*4;
                    }
                    else if(document.getElementById("ActPer").value == "30 mins"){
                        tem = tem*2;
                    }
                    else {
                        tem = tem*1;
                    }
                    document.getElementById("totAuth").value = Math.floor(tem);
                }
                else{
                    var date1 = "";
                    var date2 = "";
                    var authAuth = $("#ddlAuth").val();
                    $.ajax({
                    type: "POST",
                    url:"getAuthinfo.php",
                    async: false,
                    data:{
                        authNo : authAuth
                    },
                    success:function(data){
                      var n = data.length;
                      data = data.substr(0,n-1);
                      var res = JSON.parse(data);
                      if(res[0].startDt != ""){date1 = res[0].startDt;}
                      if(res[0].endDt != ""){date2 = res[0].endDt;}
                    }
                    });
                    var splitDate = date1.split('-');
                    var splitDate1 = date2.split('-');

                    date1 = new Date(splitDate[0],splitDate[1],splitDate[2]); 
                    date2 = new Date(splitDate1[0],splitDate1[1],splitDate1[2]);
                    var timeDiff = Math.abs(date2.getTime() - date1.getTime());
                    var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
                    //var tem = diffDays / 1;
                    var tem = document.getElementById("calculate").value / 1
                    if(document.getElementById("ActPer").value == "15 mins"){
                        tem = tem*4;
                    }
                    else if(document.getElementById("ActPer").value == "30 mins"){
                        tem = tem*2;
                    }
                    else {
                        tem = tem*1;
                    }
                    document.getElementById("totAuth").value = Math.floor(tem);

                }
            }
        }
    })
    $("#ddlApproveHours").change(function(){
        var hours = document.getElementById("ddlApproveHours").value;
        if(hours == "Weekly"){
            var date1 = "";
            var date2 = "";
            var authAuth = $("#ddlAuth").val();
            $.ajax({
            type: "POST",
            url:"getAuthinfo.php",
            async: false,
            data:{
                authNo : authAuth
            },
            success:function(data){
              var n = data.length;
              data = data.substr(0,n-1);
              var res = JSON.parse(data);
              if(res[0].startDt != ""){date1 = res[0].startDt;}
              if(res[0].endDt != ""){date2 = res[0].endDt;}
            }
            });
            var splitDate = date1.split('-');
            var splitDate1 = date2.split('-');

            date1 = new Date(splitDate[0],splitDate[1],splitDate[2]); 
            date2 = new Date(splitDate1[0],splitDate1[1],splitDate1[2]);
            var timeDiff = Math.abs(date2.getTime() - date1.getTime());
            var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
            var tem = diffDays / 7;
            tem = document.getElementById("calculate").value * tem;
            if(document.getElementById("ActPer").value == "15 mins"){
                tem = tem*4;
            }
            else if(document.getElementById("ActPer").value == "30 mins"){
                tem = tem*2;
            }
            else {
                tem = tem*1;
            }
            document.getElementById("totAuth").value = Math.floor(tem);
        }
        else if(hours=="Monthly"){
            var date1 = "";
            var date2 = "";
            var authAuth = $("#ddlAuth").val();
            $.ajax({
            type: "POST",
            url:"getAuthinfo.php",
            async: false,
            data:{
                authNo : authAuth
            },
            success:function(data){
              var n = data.length;
              data = data.substr(0,n-1);
              var res = JSON.parse(data);
              if(res[0].startDt != ""){date1 = res[0].startDt;}
              if(res[0].endDt != ""){date2 = res[0].endDt;}
            }
            });
            var splitDate = date1.split('-');
            var splitDate1 = date2.split('-');

            date1 = new Date(splitDate[0],splitDate[1],splitDate[2]); 
            date2 = new Date(splitDate1[0],splitDate1[1],splitDate1[2]);
            var timeDiff = Math.abs(date2.getTime() - date1.getTime());
            var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
            var tem = diffDays / 30;
            tem = document.getElementById("calculate").value * tem;
            if(document.getElementById("ActPer").value == "15 mins"){
                tem = tem*4;
            }
            else if(document.getElementById("ActPer").value == "30 mins"){
                tem = tem*2;
            }
            else {
                tem = tem*1;
            }
            document.getElementById("totAuth").value = Math.floor(tem);
        }
        else if(hours=="Monthly"){
            var date1 = "";
            var date2 = "";
            var authAuth = $("#ddlAuth").val();
            $.ajax({
            type: "POST",
            url:"getAuthinfo.php",
            async: false,
            data:{
                authNo : authAuth
            },
            success:function(data){
              var n = data.length;
              data = data.substr(0,n-1);
              var res = JSON.parse(data);
              if(res[0].startDt != ""){date1 = res[0].startDt;}
              if(res[0].endDt != ""){date2 = res[0].endDt;}
            }
            });
            var splitDate = date1.split('-');
            var splitDate1 = date2.split('-');

            date1 = new Date(splitDate[0],splitDate[1],splitDate[2]); 
            date2 = new Date(splitDate1[0],splitDate1[1],splitDate1[2]);
            var timeDiff = Math.abs(date2.getTime() - date1.getTime());
            var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
            var tem = diffDays / 30;
            tem = document.getElementById("calculate").value * tem;
            if(document.getElementById("ActPer").value == "15 mins"){
                tem = tem*4;
            }
            else if(document.getElementById("ActPer").value == "30 mins"){
                tem = tem*2;
            }
            else {
                tem = tem*1;
            }
            document.getElementById("totAuth").value = Math.floor(tem);
        }
        else{
            var date1 = "";
            var date2 = "";
            var authAuth = $("#ddlAuth").val();
            $.ajax({
            type: "POST",
            url:"getAuthinfo.php",
            async: false,
            data:{
                authNo : authAuth
            },
            success:function(data){
              var n = data.length;
              data = data.substr(0,n-1);
              var res = JSON.parse(data);
              if(res[0].startDt != ""){date1 = res[0].startDt;}
              if(res[0].endDt != ""){date2 = res[0].endDt;}
            }
            });
            var splitDate = date1.split('-');
            var splitDate1 = date2.split('-');

            date1 = new Date(splitDate[0],splitDate[1],splitDate[2]); 
            date2 = new Date(splitDate1[0],splitDate1[1],splitDate1[2]);
            var timeDiff = Math.abs(date2.getTime() - date1.getTime());
            var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
            //var tem = diffDays / 1;
            var tem = document.getElementById("calculate").value / 1
            if(document.getElementById("ActPer").value == "15 mins"){
                tem = tem*4;
            }
            else if(document.getElementById("ActPer").value == "30 mins"){
                tem = tem*2;
            }
            else {
                tem = tem*1;
            }
            document.getElementById("totAuth").value = Math.floor(tem);

        }
    })
    

    $(document).on('focusout','#calculate',function() {
        if($("#ddlApprove").val()=="Approved Units"){
            var temp = $(this).val();
            document.getElementById("totAuth").value = temp;
        }
        else{
            var hours = document.getElementById("ddlApproveHours").value;
            if(hours == "Weekly"){
                var date1 = "";
                var date2 = "";
                var authAuth = $("#ddlAuth").val();
                $.ajax({
                type: "POST",
                url:"getAuthinfo.php",
                async: false,
                data:{
                    authNo : authAuth
                },
                success:function(data){
                  var n = data.length;
                  data = data.substr(0,n-1);
                  var res = JSON.parse(data);
                  if(res[0].startDt != ""){date1 = res[0].startDt;}
                  if(res[0].endDt != ""){date2 = res[0].endDt;}
                }
                });
                var splitDate = date1.split('-');
                var splitDate1 = date2.split('-');

                date1 = new Date(splitDate[0],splitDate[1],splitDate[2]); 
                date2 = new Date(splitDate1[0],splitDate1[1],splitDate1[2]);
                var timeDiff = Math.abs(date2.getTime() - date1.getTime());
                var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
                var tem = diffDays / 7;
                tem = document.getElementById("calculate").value * tem;
                if(document.getElementById("ActPer").value == "15 mins"){
                    tem = tem*4;
                }
                else if(document.getElementById("ActPer").value == "30 mins"){
                    tem = tem*2;
                }
                else {
                    tem = tem*1;
                }
                document.getElementById("totAuth").value = Math.floor(tem);
            }
            else if(hours=="Monthly"){
                var date1 = "";
                var date2 = "";
                var authAuth = $("#ddlAuth").val();
                $.ajax({
                type: "POST",
                url:"getAuthinfo.php",
                async: false,
                data:{
                    authNo : authAuth
                },
                success:function(data){
                  var n = data.length;
                  data = data.substr(0,n-1);
                  var res = JSON.parse(data);
                  if(res[0].startDt != ""){date1 = res[0].startDt;}
                  if(res[0].endDt != ""){date2 = res[0].endDt;}
                }
                });
                var splitDate = date1.split('-');
                var splitDate1 = date2.split('-');

                date1 = new Date(splitDate[0],splitDate[1],splitDate[2]); 
                date2 = new Date(splitDate1[0],splitDate1[1],splitDate1[2]);
                var timeDiff = Math.abs(date2.getTime() - date1.getTime());
                var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
                var tem = diffDays / 30;
                tem = document.getElementById("calculate").value * tem;
                if(document.getElementById("ActPer").value == "15 mins"){
                    tem = tem*4;
                }
                else if(document.getElementById("ActPer").value == "30 mins"){
                    tem = tem*2;
                }
                else {
                    tem = tem*1;
                }
                document.getElementById("totAuth").value = Math.floor(tem);
            }
            else{
                var date1 = "";
                var date2 = "";
                var authAuth = $("#ddlAuth").val();
                $.ajax({
                type: "POST",
                url:"getAuthinfo.php",
                async: false,
                data:{
                    authNo : authAuth
                },
                success:function(data){
                  var n = data.length;
                  data = data.substr(0,n-1);
                  var res = JSON.parse(data);
                  if(res[0].startDt != ""){date1 = res[0].startDt;}
                  if(res[0].endDt != ""){date2 = res[0].endDt;}
                }
                });
                var splitDate = date1.split('-');
                var splitDate1 = date2.split('-');

                date1 = new Date(splitDate[0],splitDate[1],splitDate[2]); 
                date2 = new Date(splitDate1[0],splitDate1[1],splitDate1[2]);
                var timeDiff = Math.abs(date2.getTime() - date1.getTime());
                var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
                //var tem = diffDays / 1;
                var tem = document.getElementById("calculate").value / 1
                if(document.getElementById("ActPer").value == "15 mins"){
                    tem = tem*4;
                }
                else if(document.getElementById("ActPer").value == "30 mins"){
                    tem = tem*2;
                }
                else {
                    tem = tem*1;
                }
                document.getElementById("totAuth").value = Math.floor(tem);

            }
        }
    });

        
    $(document).on('focusout','#email',function() {
        if($(this).val()!=""){
            if( !validateEmail($(this).val())) {
                alertify.error("Please enter a valid email address");
                $("#email").focus();
            }
        }
    });

    $(document).on('focusout','#Emeremail',function() {
        if($(this).val()!=""){
            if( !validateEmail($(this).val())) {
                alertify.error("Please enter a valid email address");
                $("#Emeremail").focus();
            }
        }
    });

    var dt=[];
    $('#activityTab').DataTable({
    "bPaginate": false,
        "bFilter": false,
        "bInfo": false,
    destroy: true,
    "data": dt,
      columns: [
       {"title": "Activity Name"},
      {"title": "Start Date"},
      {"title": "End Date"},
      {"title": "Total Auth"},
      {"title": "Remaining Auth"},
      {"title": "Action",
          "render": function ( data, type, full, meta ) {
             return '<a href="javascript:void(0);" class="act act'+data+'"><span class="ti-pencil"></span></a> | <a href="javascript:void(0);" class="del del'+data+'"><span class="ti-trash"></span></a>';
          }
      }
      ]
    });
    $('#dob').datetimepicker({
        dayOfWeekStart: 1,
        lang: 'en',
        format: 'm-d-Y',
        timepicker: false,
        //minDate: '-2013/01/02',
        //maxDate: '+2014/12/31',
        formatDate: 'm-d-Y',
        closeOnDateSelect: true
    });
    $('#dob').mask('00-00-0000');


  $('#policyStartDt1').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
  $('#AuthstartDt1').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
  $('#AuthstartDt2').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });

        $('#guarantorDOB').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });

    
        $('#guarantorDOB').mask('00-00-0000');
        $('#policyDOB1').mask('00-00-0000');
        $('#policyDOB2').mask('00-00-0000');
        $('#policyDOB3').mask('00-00-0000');

        $('#policyDOB1').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#policyDOB2').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#policyDOB3').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });

  
        $('#AuthendDt1').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#AuthendDt2').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#AuthstartDt1').mask('00-00-0000');
        $('#AuthendDt1').mask('00-00-0000');
        $('#AuthstartDt2').mask('00-00-0000');
        $('#AuthendDt2').mask('00-00-0000');
        $('#policyStartDt1').mask('00-00-0000');
        $('#policyEndDt1').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#policyEndDt1').mask('00-00-0000');
        $('#policyStartDt2').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#policyStartDt2').mask('00-00-0000');
        $('#policyEndDt2').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#policyEndDt2').mask('00-00-0000');
        $('#policyStartDt3').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#policyStartDt3').mask('00-00-0000');
        $('#policyEndDt3').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#policyEndDt3').mask('00-00-0000');

        var PrimaryPhy = <?php include('primaryPhysician.php'); ?>;
            $("#primaryCarePhy").autocomplete({
                source: PrimaryPhy,
                autoFocus:true
            });

            var insurances = <?php include('insuranceList.php'); ?>;
            $("#policyInsurance1").autocomplete({
                source: function(request, response) {
                    var results = $.ui.autocomplete.filter(insurances, request.term);

                    if (!results.length) {
                        results = ["No matches found"];
                    }

                    response(results);
                },
                autoFocus:true,
                focus: function (event, ui) {
                       var val = ui.item.id;
                       if(val != undefined){
                           $.ajax({
                                type: "POST",
                                url:"getInsuranceAddrbyID.php",
                                async: false,
                                data:{
                                    insuranceID : val
                                },
                                success:function(result){
                                    $("#NewspanAddress1").html(result);
                                }
                            });
                           event.preventDefault(); // Prevent the default focus behavior.
                       }
                       else{
                            $("#NewspanAddress1").html("<a href='javascript:void(0);' id='linkIns'><i class='icon icon-plus'></i> Add New Insurance</a>");
                       }
                },
                select: function (event, ui) {
                    var label = ui.item.id;
                    var value = ui.item.value;
                    var currValue = label;
                    $.ajax({
                        type: "POST",
                        url:"getInsuranceAddrbyID.php",
                        async: false,
                        data:{
                            insuranceID : label
                        },
                        success:function(result){
                            $("#NewspanAddress1").html(result);
                        }
                    });
                }
            }); 
            $("#policyInsurance2").autocomplete({
                source: insurances,
                autoFocus:true,
                focus: function (event, ui) {
                       var val = ui.item.id;
                       $.ajax({
                            type: "POST",
                            url:"getInsuranceAddrbyID.php",
                            async: false,
                            data:{
                                insuranceID : val
                            },
                            success:function(result){
                                $("#NewspanAddress2").html(result);
                            }
                        });
                       event.preventDefault(); // Prevent the default focus behavior.
                },
                select: function (event, ui) {
                    var label = ui.item.id;
                    var value = ui.item.value;
                    var currValue = label;
                    $.ajax({
                        type: "POST",
                        url:"getInsuranceAddrbyID.php",
                        async: false,
                        data:{
                            insuranceID : label
                        },
                        success:function(result){
                            $("#NewspanAddress2").html(result);
                        }
                    });
                }
            }); 
            $("#policyInsurance3").autocomplete({
                source: insurances,
                autoFocus:true,
                focus: function (event, ui) {
                       var val = ui.item.id;
                       $.ajax({
                            type: "POST",
                            url:"getInsuranceAddrbyID.php",
                            async: false,
                            data:{
                                insuranceID : val
                            },
                            success:function(result){
                                $("#NewspanAddress3").html(result);
                            }
                        });
                       event.preventDefault(); // Prevent the default focus behavior.
                },
                select: function (event, ui) {
                    var label = ui.item.id;
                    var value = ui.item.value;
                    var currValue = label;
                    $.ajax({
                        type: "POST",
                        url:"getInsuranceAddrbyID.php",
                        async: false,
                        data:{
                            insuranceID : label
                        },
                        success:function(result){
                            $("#NewspanAddress3").html(result);
                        }
                    });
                }
            }); 
            $("#principalDiag").autocomplete({
                minlength : 1,
                source: function(request, response) { 
                    $.ajax({
                        url: "dxSearch10.php",
                        data: { term: request.term },
                        type: "POST", 
                        success: function(data) { 
                            response(JSON.parse(data)); 
                        }
                    });
                },
                autoFocus:true
            });
            $("#defDiag2").autocomplete({
                minlength : 1,
                source: function(request, response) { 
                    $.ajax({
                        url: "dxSearch10.php",
                        data: { term: request.term },
                        type: "POST", 
                        success: function(data) { 
                            response(JSON.parse(data)); 
                        }
                    });
                },
                autoFocus:true
            });
            $("#defDiag3").autocomplete({
                minlength : 1,
                source: function(request, response) { 
                    $.ajax({
                        url: "dxSearch10.php",
                        data: { term: request.term },
                        type: "POST", 
                        success: function(data) { 
                            response(JSON.parse(data)); 
                        }
                    });
                },
                autoFocus:true
            });
            $("#defDiag4").autocomplete({
                minlength : 1,
                source: function(request, response) { 
                    $.ajax({
                        url: "dxSearch10.php",
                        data: { term: request.term },
                        type: "POST", 
                        success: function(data) { 
                            response(JSON.parse(data)); 
                        }
                    });
                },
                autoFocus:true
            });

  var patientList = <?php include('patientList.php'); ?>;
    $("#policyHolder1").autocomplete({
        source: patientList,
        autoFocus:true
    });
    $('#policyHolder1').keypress(function (e) {
        var regex = new RegExp("^[a-zA-Z-]+$");
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (regex.test(str)) {
            return true;
        }

        e.preventDefault();
        return false;
    });
    $("#policyHolder2").autocomplete({
        source: patientList,
        autoFocus:true
    });
    $('#policyHolder2').keypress(function (e) {
        var regex = new RegExp("^[a-zA-Z-]+$");
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (regex.test(str)) {
            return true;
        }

        e.preventDefault();
        return false;
    });
    $("#policyHolder3").autocomplete({
        source: patientList,
        autoFocus:true
    });
    $('#policyHolder3').keypress(function (e) {
        var regex = new RegExp("^[a-zA-Z-]+$");
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (regex.test(str)) {
            return true;
        }

        e.preventDefault();
        return false;
    });

    $('#policyGroupName1').keypress(function (e) {
        var regex = new RegExp(/^[a-zA-Z\s]+$/);
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (regex.test(str)) {
            return true;
        }

        e.preventDefault();
        return false;
    });
    $('#policyGroupName2').keypress(function (e) {
        var regex = new RegExp(/^[a-zA-Z\s]+$/);
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (regex.test(str)) {
            return true;
        }

        e.preventDefault();
        return false;
    });
    $('#policyGroupName3').keypress(function (e) {
        var regex = new RegExp(/^[a-zA-Z\s]+$/);
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (regex.test(str)) {
            return true;
        }

        e.preventDefault();
        return false;
    });
    $('#policyCopay1').keypress(function (e) {
        var regex = new RegExp(/^[0-9]*$/);
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (regex.test(str)) {
            return true;
        }

        e.preventDefault();
        return false;
    });
    $('#policyCopay2').keypress(function (e) {
        var regex = new RegExp(/^[0-9]*$/);
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (regex.test(str)) {
            return true;
        }

        e.preventDefault();
        return false;
    });
    $('#policyCopay3').keypress(function (e) {
        var regex = new RegExp(/^[0-9]*$/);
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (regex.test(str)) {
            return true;
        }

        e.preventDefault();
        return false;
    });
    $('#policyDeduc1').keypress(function (e) {
        var regex = new RegExp(/^[0-9]*$/);
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (regex.test(str)) {
            return true;
        }

        e.preventDefault();
        return false;
    });
    $('#policyDeduc2').keypress(function (e) {
        var regex = new RegExp(/^[0-9]*$/);
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (regex.test(str)) {
            return true;
        }

        e.preventDefault();
        return false;
    });
    $('#policyDeduc3').keypress(function (e) {
        var regex = new RegExp(/^[0-9]*$/);
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (regex.test(str)) {
            return true;
        }

        e.preventDefault();
        return false;
    });
    $(document).on('focusout','#policyCopay1',function() {
        if($(this).val() !=""){
            var amount = $(this).val();
            amount = parseInt(amount).toFixed(2);
            document.getElementById("policyCopay1").value = amount;
        }
    });
    $(document).on('focusout','#policyCopay2',function() {
        if($(this).val() !=""){
            var amount = $(this).val();
            amount = parseInt(amount).toFixed(2);
            document.getElementById("policyCopay2").value = amount;
        }
    });
    $(document).on('focusout','#policyCopay3',function() {
        if($(this).val() !=""){
            var amount = $(this).val();
            amount = parseInt(amount).toFixed(2);
            document.getElementById("policyCopay3").value = amount;
        }
    });
    $(document).on('focusout','#policyDeduc1',function() {
        if($(this).val() !=""){
            var amount = $(this).val();
            amount = parseInt(amount).toFixed(2);
            document.getElementById("policyDeduc1").value = amount;
        }
    });
    $(document).on('focusout','#policyDeduc2',function() {
        if($(this).val() !=""){
            var amount = $(this).val();
            amount = parseInt(amount).toFixed(2);
            document.getElementById("policyDeduc2").value = amount;
        }
    });
    $(document).on('focusout','#policyDeduc3',function() {
        if($(this).val() !=""){
            var amount = $(this).val();
            amount = parseInt(amount).toFixed(2);
            document.getElementById("policyDeduc3").value = amount;
        }
    });


  $('#ssn').mask('000-00-0000');
  $('#phoneHome').mask('(000) 000-0000');
  $('#guarantorPhoneHome').mask('(000) 000-0000');
  $('#phoneEmerMobile').mask('(000) 000-0000');
  $('#phoneEmerHome').mask('(000) 000-0000');

            //$('#phoneWork').mask('(000) 000-0000');

            $('#phoneMobile').mask('(000) 000-0000');

        $('#policyDOB1').mask('00-00-0000');
        $('#policyDOB2').mask('00-00-0000');
        $('#policyDOB3').mask('00-00-0000');
        $('#dob').mask('00-00-0000');

        $('#getAddr').change(function(){
            var getAddr = document.getElementById("getAddr");
            if(getAddr.checked){
                document.getElementById("guarantorAddrStreet").value = document.getElementById("addrStreet1").value;
                document.getElementById("guarantorAddrCity").value = document.getElementById("addrCity").value;
                document.getElementById("guarantorAddrState").value = document.getElementById("addrState").value;
                document.getElementById("guarantorAddrZip").value = document.getElementById("addrZip").value;
                document.getElementById("guarantorPhoneHome").value = document.getElementById("phoneHome").value;
                document.getElementById("guarantorAddrStreet").disabled = true;
                document.getElementById("guarantorAddrCity").disabled = true;
                document.getElementById("guarantorAddrState").disabled = true;
                document.getElementById("guarantorAddrZip").disabled = true;
                document.getElementById("guarantorPhoneHome").disabled = true;

            }
            else{
                document.getElementById("guarantorAddrStreet").value = "";
                document.getElementById("guarantorAddrCity").value = "";
                document.getElementById("guarantorAddrState").value = "";
                document.getElementById("guarantorAddrZip").value = "";
                document.getElementById("guarantorPhoneHome").value = "";
                document.getElementById("guarantorAddrStreet").disabled = false;
                document.getElementById("guarantorAddrCity").disabled = false;
                document.getElementById("guarantorAddrState").disabled = false;
                document.getElementById("guarantorAddrZip").disabled = false;
                document.getElementById("guarantorPhoneHome").disabled = false;
            }
        });

   $('#ActMaxi1').change(function(){
        if($(this).val() == "Amount"){
            $('#ActPer1').html("");
            $('#ActPer1').append('<option value="Week">Week</option><option value="Month">Month</option><option value="Total Auth">Total Auth</option>');
        }
        else if($(this).val() == "Sessions"){
            $('#ActPer1').html("");
            $('#ActPer1').append('<option value="Total Auth">Total Auth</option>');
        }
        else{
            $('#ActPer1').html("");
            $('#ActPer1').append('<option value="Day">Day</option><option value="Week">Week</option><option value="Month">Month</option><option value="Total Auth">Total Auth</option>');
        }
    });
    $('#ActMaxi2').change(function(){
        if($(this).val() == "Amount"){
            $('#ActPer2').html("");
            $('#ActPer2').append('<option value="Week">Week</option><option value="Month">Month</option><option value="Total Auth">Total Auth</option>');
        }
        else if($(this).val() == "Sessions"){
            $('#ActPer2').html("");
            $('#ActPer2').append('<option value="Total Auth">Total Auth</option>');
        }
        else{
            $('#ActPer2').html("");
            $('#ActPer2').append('<option value="Day">Day</option><option value="Week">Week</option><option value="Month">Month</option><option value="Total Auth">Total Auth</option>');
        }
    });
    $('#ActMaxi3').change(function(){
        if($(this).val() == "Amount"){
            $('#ActPer3').html("");
            $('#ActPer3').append('<option value="Week">Week</option><option value="Month">Month</option><option value="Total Auth">Total Auth</option>');
        }
        else if($(this).val() == "Sessions"){
            $('#ActPer3').html("");
            $('#ActPer3').append('<option value="Total Auth">Total Auth</option>');
        }
        else{
            $('#ActPer3').html("");
            $('#ActPer3').append('<option value="Day">Day</option><option value="Week">Week</option><option value="Month">Month</option><option value="Total Auth">Total Auth</option>');
        }
    });
    // $("#btnAddAct").click(function() {
    //     var acti = $("#Activity").val();
    //     if(acti != ""){
    //         addAct();
    //     }
    // });

    $(document).on('click','.del',function() {
            var temp = $(this).attr('class').split(' ')[1];
            var newTemp = temp.replace('del','');
            var ActID = newTemp;
            $.ajax({
                type: "POST",
                url:"getActData.php",
                async : false,
                data:{
                  actID : ActID,
                },success:function(result){
                  var res = JSON.parse(result);
                  var totalAuth = res[0].totalAuth;
                  var remainAuth = res[0].remainAuth;
                  if(totalAuth == remainAuth){
                    $.ajax({
                        type: "POST",
                        url:"delActData.php",
                        async : false,
                        data:{
                          actID : ActID,
                        },success:function(result){
                            alertify.success(result);
                            getActivityData();
                        }
                    });
                  }
                  else{
                    alertify.error("Sorry, This Activity has been used. So unable to delete it!!")
                  }
              }
            });
        });

        $(document).on('click','.act',function() {
          var temp = $(this).attr('class').split(' ')[1];
          var newTemp = temp.replace('act','');
          var ActID = newTemp;
          $("#modalActivity").modal("show");
          $.ajax({
            type: "POST",
            url:"getActData.php",
            async : false,
            data:{
              actID : ActID,
            },success:function(result){
              var res = JSON.parse(result);
              var auth = $('#ddlAuth').val();
              $.ajax({
                type: "POST",
                url:"chkTOS.php",
                async : false,
                data:{
                  auth : auth
                },success:function(result){
                  if(result =="Behaviour Therapy"){
                    $("#Activity").html('');
                    $("#Activity").append('<option value=""></option><option value="Initial Assessment By BCBA">Initial Assessment By BCBA</option><option value="Follow up Assessment By BCBA">Follow up Assessment By BCBA</option><option value="Direct Behavior Therapy By BCBA">Direct Behavior Therapy By BCBA</option><option value="Direct Behavior Therapy By BcABA">Direct Behavior Therapy By BcABA</option><option value="Direct Behavior Therapy By Para">Direct Behavior Therapy By Para</option><option value="Supervision By BCBA">Supervision By BCBA</option><option value="Social Skills Group By Para">Social Skills Group By Para</option><option value="Social Skills Group By BCBA">Social Skills Group By BCBA</option><option value="Parent Training By BCBA">Parent Training By BCBA</option><option value="Family Group Training By BCBA">Family Group Training By BCBA</option>');
                    $("#ActCPT").html('');
                    $("#ActCPT").append('<option value=""></option><option value="0359T">0359T</option><option value="0360T">0360T</option><option value="0361T">0361T</option><option value="0360T-0361T">0360T-0361T</option><option value="0364T">0364T</option><option value="0365T">0365T</option><option value="0364T-0365T">0364T-0365T</option><option value="0368T">0368T</option><option value="0369T">0369T</option><option value="0368T-0369T">0368T-0369T</option><option value="0366T">0366T</option><option value="0367T">0367T</option><option value="0366T-0367T">0366T-0367T</option><option value="0370T">0370T</option><option value="0371T">0371T</option><option value="0372T">0372T</option>      <option value="0373T">0373T</option><option value="0374T">0374T</option><option value="0373T-0374T">0373T-0374T</option><option value="H0004">H0004</option><option value="H0031">H0031</option><option value="H0032">H0032</option><option value="H0046">H0046</option><option value="H2000">H2000</option><option value="H2012">H2012</option><option value="H2014">H2014</option><option value="H2016">H2016</option><option value="H2019">H2019</option><option value="H2020">H2020</option><option value="H2021">H2021</option><option value="H2027">H2027</option><option value="H2030">H2030</option><option value="H2031">H2031</option><option value="H2033">H2033</option><option value="S5102">S5102</option><option value="S5108">S5108</option><option value="S5110">S5110</option><option value="S5111">S5111</option><option value="S5130">S5130</option><option value="S5135">S5135</option><option value="S5151">S5151</option>')
                  }
                  else if(result =="Speech Therapy"){
                    $("#Activity").html('');
                    $("#Activity").append('<option value=""></option><option value="Evaluation By Speech Therapist">Evaluation By Speech Therapist</option><option value="Continued Services">Continued Services</option>');
                    $("#ActCPT").html('');
                    $("#ActCPT").append('<option value=""></option><option value="92513">92513</option><option value="92507">92507</option>')
                  }
                  else{
                    $("#Activity").html('');
                    $("#Activity").append('<option value=""></option><option value="Assessment">Assessment</option><option value="Direct Behavior Theraphy">Direct Behavior Theraphy</option><option value="Supervision">Supervision</option><option value="Supervision Parent Training">Supervision Parent Training</option><option value="Report Writing Send to Editor">Report Writing Send to Editor</option><option value="Report Writing Send to Insurance">Report Writing Send to Insurance</option><option value="Report Writing Progress Reports">Report Writing Progress Reports</option>');
                    $("#ActCPT").html('');
                    $("#ActCPT").append('<option value=""></option><option value="0359T">0359T</option><option value="0360T">0360T</option><option value="0361T">0361T</option><option value="0364T">0364T</option><option value="0365T">0365T</option><option value="0368T">0368T</option><option value="0369T">0369T</option><option value="0370T">0370T</option>')
                  }
                }
              });
              $("#ActTitleMod").html("Update Activity");
              $("#btnAddActi").val("Update Activity");
              document.getElementById("hdnFlagAct").value = "1";
              document.getElementById("Activity").value = res[0].activityName;
              document.getElementById("ActCPT").value = res[0].activityCPT;
              document.getElementById("ActMod1").value = res[0].activityMod1;
              document.getElementById("ActMod2").value = res[0].activityMod2;
              document.getElementById("ActMod3").value = res[0].activityMod3;
              document.getElementById("ActMod4").value = res[0].activityMod4;
              document.getElementById("ActPer").value = res[0].activityBilledPer;
              document.getElementById("ActRate").value = res[0].activityRates;
              document.getElementById("ActMaxi1").value = res[0].activityMax1;
              document.getElementById("ActMaxi2").value = res[0].activityMax2;
              document.getElementById("ActMaxi3").value = res[0].activityMax3;
              document.getElementById("ActPer1").value = res[0].activityPer1;
              document.getElementById("ActPer2").value = res[0].activityPer2;
              document.getElementById("ActPer3").value = res[0].activityPer3;
              document.getElementById("ActIs1").value = res[0].activityIs1;
              document.getElementById("ActIs2").value = res[0].activityIs2;
              document.getElementById("ActIs3").value = res[0].activityIs3;
            }
          });
        });

    $("#btnSaveCase").click(function() {
        //alert("Hit Case Save");
        addCase();
    });
    $(document).on('focusout','#guarantorAddrZip',function() {
        var currValue = $(this).val();
        $.ajax({
            type: "POST",
            url:"getZipInfo.php",
            async: false,
            data:{
                zip : currValue
            },
            success:function(result){
                var n = result.length;
                result = result.substr(0,n-1);
                if(result != "none"){
                    var res = JSON.parse(result);
                    document.getElementById("guarantorAddrState").value = res[0].state;
                    document.getElementById("guarantorAddrCity").value = res[0].city;
                }
                else{
                    alertify.error("Sorry! This Zip is not available in our Database.")
                }
            }
        });
    });
    
    $(document).on('focusout','#addrZip',function() {
        var currValue = $(this).val();
        $.ajax({
            type: "POST",
            url:"getZipInfo.php",
            async: false,
            data:{
                zip : currValue
            },
            success:function(result){
                var n = result.length;
                result = result.substr(0,n-1);
                if(result != "none"){
                    var res = JSON.parse(result);
                    document.getElementById("addrState").value = res[0].state;
                    document.getElementById("addrCity").value = res[0].city;
                }
                else{
                    alertify.error("Sorry! This Zip is not available in our Database.")
                }
            }
        });
    });
    // $("#btnNext1").click(function() {
    //     //if($('.nav-tabs .active').text() == "Client Info"){
    //         $(".validateGeneral").each(function () {
    //             //alert($(this).val());
    //             if ($(this).val() === "") {
    //                 var changeBorder = $(this).attr('id');
    //                 document.getElementById(changeBorder).style.border = "1px solid maroon";
    //                 document.getElementById(changeBorder).focus();
    //                 return false;
    //             }
    //             else {
    //                     $(".alert").remove();
    //                     $(this).removeClass('validateGeneral')
    //                     var changeBorder = $(this).attr('id');
    //                     document.getElementById(changeBorder).style.border = "1px solid #ccc";
    //                     if ($('.validateGeneral').length <= 0) {
    //                       $('.nav-tabs a[href="#tab_3_2"]').tab('show');
    //                       $('.nav-tabs a[href="#tab_3_2_1"]').addClass('active');
    //                       add_patient();
    //                     }
    //             }                
    //         });
    //     //}
    // });

})
function changeDateFormat(inputDate){  // expects Y-m-d
        var splitDate = inputDate.split('-');
        if(splitDate.count == 0){
            return null;
        }

        var year = splitDate[0];
        var month = splitDate[1];
        var day = splitDate[2].substr(0,2); 

        return month + '-' + day + '-' + year;
    }

    function addCase(){
        var PatID = document.getElementById('hdnPatientID').value;
        var description = "Primary";
        var authNo1 = $('#authNo1').val();
        var tos1 = $('#tos1').val();
        var AuthstartDt1 = convertDate($('#AuthstartDt1').val());
        var AuthendDt1 = convertDate($('#AuthendDt1').val());
        var authNo2 = $('#authNo2').val();
        var tos2 = $('#tos2').val();
        var AuthstartDt2 = convertDate($('#AuthstartDt2').val());
        var AuthendDt2 = convertDate($('#AuthendDt2').val());
        // var AuthtotalVisits = $('#AuthtotalVisits').val();
        // var totAuthType = $('#totAuthType').val();

        var lastVisit = convertDate($('#lastVisit').val());
        // if($('#totalVisits').val() == ""){
        //     var totalVisits = -1;
        // }
        // else{
        //     var totalVisits = $('#totalVisits').val();
        // }
        var principalDiag = $('#principalDiag').val();
        var defDiag2 = $('#defDiag2').val();
        var defDiag3 = $('#defDiag3').val();
        var defDiag4 = $('#defDiag4').val();
        
        var policyInsurance1 = $('#policyInsurance1').val();
        var policyHolder1 = getHolderID($('#policyHolder1').val());
        var policyRelationInsured1 = $('#policyRelationInsured1').val();
        var policyNo1 = $('#policyNo1').val();
        var policyStartDt1 = convertDate($('#policyStartDt1').val());
        var policyGroupNo1 = $('#policyGroupNo1').val();
        var policyGroupName1 = $('#policyGroupName1').val();
        var policyEndDt1 = convertDate($('#policyEndDt1').val());
        var policyClaimNo1 = $('#policyClaimNo1').val();
        var policyCopay1 = $('#policyCopay1').val();    
        var policyDeduc1 = $('#policyDeduc1').val();    

        var policyInsurance2 = $('#policyInsurance2').val();
        var policyHolder2 = getHolderID($('#policyHolder2').val());
        var policyRelationInsured2 = $('#policyRelationInsured2').val();
        var policyNo2 = $('#policyNo2').val();
        var policyStartDt2 = convertDate($('#policyStartDt2').val());
        var policyGroupNo2 = $('#policyGroupNo2').val();
        var policyGroupName2 = $('#policyGroupName2').val();
        var policyEndDt2 = convertDate($('#policyEndDt2').val());
        var policyClaimNo2 = $('#policyClaimNo2').val();        
        var policyCopay2 = $('#policyCopay2').val();
        var policyDeduc2 = $('#policyDeduc2').val();    

        var policyInsurance3 = $('#policyInsurance3').val();
        var policyHolder3 = getHolderID($('#policyHolder3').val());
        var policyRelationInsured3 = $('#policyRelationInsured3').val();
        var policyNo3 = $('#policyNo3').val();
        var policyStartDt3 = convertDate($('#policyStartDt3').val());
        var policyGroupNo3 = $('#policyGroupNo3').val();
        var policyGroupName3 = $('#policyGroupName3').val();
        var policyEndDt3 = convertDate($('#policyEndDt3').val());
        var policyClaimNo3 = $('#policyClaimNo3').val();        
        var policyCopay3 = $('#policyCopay3').val(); 
        var policyDeduc3 = $('#policyDeduc3').val();       
        
        if(policyInsurance1 != ""){
            $.ajax({
                type: "POST",
                url:"getInsuranceID.php",
                async: false,
                data:{
                    "payerName" : policyInsurance1
                    },success:function(result){
                        sessionStorage.setItem("insurance1",result);
                }
            });
        }
        if(policyInsurance2 != ""){
            $.ajax({
                type: "POST",
                url:"getInsuranceID.php",
                async: false,
                data:{
                    "payerName" : policyInsurance2
                    },success:function(result){
                        sessionStorage.setItem("insurance2",result);
                }
            });
        }
         if(policyInsurance3 != ""){
            $.ajax({
                type: "POST",
                url:"getInsuranceID.php",
                async: false,
                data:{
                    "payerName" : policyInsurance3
                    },success:function(result){
                        sessionStorage.setItem("insurance3",result);
                }
            });
        }
        var tempInsID = sessionStorage.getItem("insurance1");
        $.ajax({
            async : false,
            type: "POST",
            url:"https://curismed.com/medService/cases/create",
            data:{
                "patientID" : PatID,
                "description" : description,
                "insuranceID" : tempInsID,
                "authNo" : authNo1,
                "lastVisit" : lastVisit,
                //"noOfVisits" : totalVisits,
                //"totalVisits" : totalVisits,
                "principalDiag" : principalDiag,
                "defDiag2" : defDiag2,
                "defDiag3" : defDiag3,
                "defDiag4" : defDiag4,
            },success:function(result){
                document.getElementById("hdncaseID").value = result.CaseID;
                if(authNo1 != "") {
                    $.ajax({
                        async: false,
                        type: "POST",
                        url: "addAuth.php",
                        data: {
                            "caseID": result.CaseID,
                            "patientID": PatID,
                            "authNo": authNo1,
                            "tos": tos1,
                            "startDt": AuthstartDt1,
                            "endDt": AuthendDt1,
                            "policyEntity": "1"
                            // "totAuthType" : totAuthType,
                            // "totalVisits" : AuthtotalVisits,
                            // "totalRemaining" : AuthtotalVisits
                        }, success: function (result) {
                            console.log(result);
                        }
                    });
                }
                if(authNo2 != ""){
                    $.ajax({
                        async : false,
                        type: "POST",
                        url:"addAuth.php",
                        data:{
                            "caseID" : result.CaseID,
                            "patientID" : PatID,
                            "authNo" : authNo2,
                            "tos" : tos2,
                            "startDt" : AuthstartDt2,
                            "endDt" : AuthendDt2,
                            "policyEntity" : "2"
                            // "totAuthType" : totAuthType,
                            // "totalVisits" : AuthtotalVisits,
                            // "totalRemaining" : AuthtotalVisits
                            },success:function(result){
                                console.log(result);
                        }
                    });
                }
                alertify.success("Case successfully Created");
                document.getElementById('hdnChkCase').value = "1";
            }
        });

        $.ajax({
            type: "POST",
            url:"chartNoList.php",
            async: false,
            data:{
                "patientID" : PatID
            },success:function(result){
                var hdnCaseChartNo = result;
                resultNew = hdnCaseChartNo.split(',');
                sessionStorage.setItem("tempCaseChartNo", resultNew[0]);
                sessionStorage.setItem("tempClaimID", resultNew[1]);
            }
        });    
        var tempCaseChartNo = sessionStorage.getItem("tempCaseChartNo");
        var tempClaimID = sessionStorage.getItem("tempClaimID");
        var insurance1 = sessionStorage.getItem("insurance1");
        var insurance2 = sessionStorage.getItem("insurance2");
        var insurance3 = sessionStorage.getItem("insurance3");
        if(policyInsurance1 != "" && policyNo1 != ""){
            $.ajax({
                type: "POST",
                url:"https://curismed.com/medService/policies/create",
                async: false,
                data:{
                    "policyEntity" : "1",
                    "caseID" : tempClaimID,
                    "authNo" : authNo1,
                    "caseChartNo" : tempCaseChartNo,
                    "insuranceID" : insurance1,
                    "policyHolder" : policyHolder1,
                    "relationshipToInsured" : policyRelationInsured1,
                    "policyNo" : policyNo1,
                    "groupNo" : policyGroupNo1,
                    "groupName" : policyGroupName1,
                    "claimNo" : policyClaimNo1,
                    "startDt" : policyStartDt1,
                    "endDt" : policyEndDt1,
                    "coPayment" : policyCopay1,
                    "deductibleMet" : policyDeduc1,
                },success:function(result){
                    alertify.success("Primary Policy added successfully");
                    document.getElementById('hdnChkCase').value = "1";
                    //window.location.href = "ViewPatient.php";
                }
            });
        }
        if(policyInsurance2 != "" && policyNo2 != ""){
            $.ajax({
                type: "POST",
                url:"https://curismed.com/medService/policies/create",
                async: false,
                data:{
                    "policyEntity" : "2",
                    "caseID" : tempClaimID,
                    "authNo" : authNo2,
                    "caseChartNo" : tempCaseChartNo,
                    "insuranceID" : insurance2,
                    "policyHolder" : policyHolder2,
                    "relationshipToInsured" : policyRelationInsured2,
                    "policyNo" : policyNo2,
                    "groupNo" : policyGroupNo2,
                    "groupName" : policyGroupName2,
                    "claimNo" : policyClaimNo2,
                    "startDt" : policyStartDt2,
                    "endDt" : policyEndDt2,
                    "coPayment" : policyCopay2,
                    "deductibleMet" : policyDeduc2,
                },success:function(result){
                    alertify.success("Secondary Policy added successfully");
                    //window.location.href = "ViewPatient.php";
                    document.getElementById('hdnChkCase').value = "1";
                }
            });
        }
        if(policyInsurance3 != "" && policyNo3 != ""){
            $.ajax({
                type: "POST",
                url:"https://curismed.com/medService/policies/create",
                async: false,
                data:{
                    "policyEntity" : "3",
                    "caseID" : tempClaimID,
                    "caseChartNo" : tempCaseChartNo,
                    "insuranceID" : insurance3,
                    "policyHolder" : policyHolder3,
                    "relationshipToInsured" : policyRelationInsured3,
                    "policyNo" : policyNo3,
                    "groupNo" : policyGroupNo3,
                    "groupName" : policyGroupName3,
                    "claimNo" : policyClaimNo3,
                    "startDt" : policyStartDt3,
                    "endDt" : policyEndDt3,
                    "coPayment" : policyCopay3,
                    "deductibleMet" : policyDeduc3,
                },success:function(result){
                    alertify.success("Teritiary Policy added successfully");
                    document.getElementById('hdnChkCase').value = "1";
                    //window.location.href = "ViewPatient.php";
                }
            });
        }
        $("#step-2").css({"display":"none"});
        $("#step-3").css({"display":"flex"});
        $("#up2").removeClass("btn-primary");
        $("#up3").addClass("btn-primary");
        $("#up2").addClass("btn-default");
        $("#up3").removeClass("btn-default");
        var tempCaseI = document.getElementById('hdncaseID').value;
          $.ajax({
                type: "POST",
                url:"getAuths.php",
                async : false,
                data:{
                  caseID : tempCaseI
                },success:function(data){
                    $('#ddlAuth').html('');
                  //iterate over the data and append a select option
                  data = JSON.parse(data);
                  $.each(data, function(key, val){
                    $('#ddlAuth').append('<option value="' + val.key + '">' + val.label + '</option>');
                  });
              }
            });
          var auth = $('#ddlAuth').val();
              $.ajax({
                type: "POST",
                url:"chkTOS.php",
                async : false,
                data:{
                  auth : auth
                },success:function(result){
                  if(result =="Behaviour Therapy"){
                    $("#Activity").html('');
                    $("#Activity").append('<option value=""></option><option value="Initial Assessment By BCBA">Initial Assessment By BCBA</option><option value="Follow up Assessment By BCBA">Follow up Assessment By BCBA</option><option value="Direct Behavior Therapy By BCBA">Direct Behavior Therapy By BCBA</option><option value="Direct Behavior Therapy By BcABA">Direct Behavior Therapy By BcABA</option><option value="Direct Behavior Therapy By Para">Direct Behavior Therapy By Para</option><option value="Supervision By BCBA">Supervision By BCBA</option><option value="Social Skills Group By Para">Social Skills Group By Para</option><option value="Social Skills Group By BCBA">Social Skills Group By BCBA</option><option value="Parent Training By BCBA">Parent Training By BCBA</option><option value="Family Group Training By BCBA">Family Group Training By BCBA</option>');
                    $("#ActCPT").html('');
                      $("#ActCPT").append('<option value=""></option><option value="0359T">0359T</option><option value="0360T">0360T</option><option value="0361T">0361T</option><option value="0360T-0361T">0360T-0361T</option><option value="0364T">0364T</option><option value="0365T">0365T</option><option value="0364T-0365T">0364T-0365T</option><option value="0368T">0368T</option><option value="0369T">0369T</option><option value="0368T-0369T">0368T-0369T</option><option value="0366T">0366T</option><option value="0367T">0367T</option><option value="0366T-0367T">0366T-0367T</option><option value="0370T">0370T</option><option value="0371T">0371T</option><option value="0372T">0372T</option>      <option value="0373T">0373T</option><option value="0374T">0374T</option><option value="0373T-0374T">0373T-0374T</option><option value="H0004">H0004</option><option value="H0031">H0031</option><option value="H0032">H0032</option><option value="H0046">H0046</option><option value="H2000">H2000</option><option value="H2012">H2012</option><option value="H2014">H2014</option><option value="H2016">H2016</option><option value="H2019">H2019</option><option value="H2020">H2020</option><option value="H2021">H2021</option><option value="H2027">H2027</option><option value="H2030">H2030</option><option value="H2031">H2031</option><option value="H2033">H2033</option><option value="S5102">S5102</option><option value="S5108">S5108</option><option value="S5110">S5110</option><option value="S5111">S5111</option><option value="S5130">S5130</option><option value="S5135">S5135</option><option value="S5151">S5151</option>')
                  }
                  else if(result =="Speech Therapy"){
                    $("#Activity").html('');
                    $("#Activity").append('<option value=""></option><option value="Evaluation By Speech Therapist">Evaluation By Speech Therapist</option><option value="Continued Services">Continued Services</option>');
                    $("#ActCPT").html('');
                    $("#ActCPT").append('<option value=""></option><option value="92513">92513</option><option value="92507">92507</option>')
                  }
                  else{
                    $("#Activity").html('');
                    $("#Activity").append('<option value=""></option><option value="Assessment">Assessment</option><option value="Direct Behavior Theraphy">Direct Behavior Theraphy</option><option value="Supervision">Supervision</option><option value="Supervision Parent Training">Supervision Parent Training</option><option value="Report Writing Send to Editor">Report Writing Send to Editor</option><option value="Report Writing Send to Insurance">Report Writing Send to Insurance</option><option value="Report Writing Progress Reports">Report Writing Progress Reports</option>');
                    $("#ActCPT").html('');
                    $("#ActCPT").append('<option value=""></option><option value="0359T">0359T</option><option value="0360T">0360T</option><option value="0361T">0361T</option><option value="0364T">0364T</option><option value="0365T">0365T</option><option value="0368T">0368T</option><option value="0369T">0369T</option><option value="0370T">0370T</option>')
                  }
                }
              });
    }

    function addAct(){
        var authNo = $("#ddlAuth").val();
        var caseID = document.getElementById("hdncaseID").value;
        var activityName = $("#Activity").val();
        var activityCPT = $("#ActCPT").val();
        var activityMod1 = $("#ActMod1").val();
        var activityMod2 = $("#ActMod2").val();
        var activityMod3 = $("#ActMod3").val();
        var activityMod4 = $("#ActMod4").val();
        var activityBilledPer = $("#ActPer").val();
        var activityBilledPerTime = $("#ActPerTime").val();
        var activityRates = $("#ActRate").val();
        // var activityMax1 = $("#ActMaxi1").val();
        // var activityMax2 = $("#ActMaxi2").val();
        // var activityMax3 = $("#ActMaxi3").val();
        // var activityPer1 = $("#ActPer1").val();
        // var activityPer2 = $("#ActPer2").val();
        // var activityPer3 = $("#ActPer3").val();
        // var activityIs1 = $("#ActIs1").val();
        // var activityIs2 = $("#ActIs2").val();
        // var activityIs3 = $("#ActIs3").val();
        var totAuth = $("#totAuth").val();
        var aType = $("#ddlApprove").val();
        var authType="";
        if(aType=="Approved Units"){
            authType = "Total Auth";
        }
        else{
            authType = document.getElementById("ddlApproveHours").value;
        }
        $.ajax({
            type: "POST",
            url:"addActivity.php",
            async: false,
            data:{
                "authNo" : authNo,
                "caseID" : caseID,
                "activityName" : activityName,
                "activityCPT" : activityCPT,
                "activityMod1" : activityMod1,
                "activityMod2" : activityMod2,
                "activityMod3" : activityMod3,
                "activityMod4" : activityMod4,
                "activityBilledPer" : activityBilledPer,
                "activityBilledPerTime" : activityBilledPerTime,
                "authType" : authType,
                "totAuth" : totAuth,
                "remainAuth" : totAuth,
                "activityRates" : activityRates,
                // "activityMax1" : activityMax1,
                // "activityMax2" : activityMax2,
                // "activityMax3" : activityMax3,
                // "activityPer1" : activityPer1,
                // "activityPer2" : activityPer2,
                // "activityPer3" : activityPer3,
                // "activityIs1" : activityIs1,
                // "activityIs2" : activityIs2,
                // "activityIs3" : activityIs3,
            },success:function(result){
                if(result =="added"){
                    alertify.success("Activity added successfully");
                    $("#ActMod1").val("");
                    $("#ActMod2").val("");
                    $("#ActMod3").val("");
                    $("#ActMod4").val("");
                    $("#ActRate").val("");
                    $("#ActIs1").val("");
                    $("#calculate").val("");
                    $("#totAuth").val("");
                    $('#ddlApproveHours').prop('selectedIndex', -1);
                    $('#Activity').prop('selectedIndex', -1);
                    $('#ActPer').prop('selectedIndex', -1);
                    $('#ActCPT').prop('selectedIndex', -1);
                    $('#ddlApprove').prop('selectedIndex', -1);
                    getActivityData();
                }
                else{
                    alertify.error(result);
                }
                //window.location.href = "ViewPatient.php";
            }
        });
    }

    function getActivityData(){
        var ddlVal = $("#ddlAuth").val();
        $.ajax({
              async : false,
              type: "POST",
              url:"getActivity.php",
              data:{
                authNo: ddlVal
                },success:function(data1){
                    data1 = JSON.parse(data1);
                    var dt = [];
                    $.each(data1,function(i,v) {
                        var startDt = changeDateFormat(data1[i].startDt);
                        var endDt = changeDateFormat(data1[i].endDt);
                        dt.push([data1[i].activityName,startDt,endDt,data1[i].totalAuth,data1[i].remainAuth,data1[i].activityID]);
                    });
                    //alert(dt);
                    var table = $('#activityTab').DataTable({
                    destroy: true,
                    "bPaginate": false,
                    "bFilter": false,
                    "bInfo": false,
                    "data": dt,
                      columns: [
                       {"title": "Activity Name"},
                      {"title": "Start Date"},
                      {"title": "End Date"},
                      {"title": "Total Auth"},
                      {"title": "Remaining Auth"},
                      {"title": "Action",
                          "render": function ( data, type, full, meta ) {
                             return '<a href="javascript:void(0);" class="act act'+data+'"><span class="ti-pencil"></span></a> | <a href="javascript:void(0);" class="del del'+data+'"><span class="ti-trash"></span></a>';
                          }
                      }
                      ]
                    });
                }
            });
    }

    function getHolderID(name1){
        var name = name1;
            if(name != ""){
                var n=name.indexOf("-");
                var truncID = name.substr(0,n);
            }
        return truncID;
    }
    var convertDate = function(usDate) {
        if(usDate != "" && usDate != null && usDate != undefined){
            var dateParts = usDate.split(/(\d{1,2})-(\d{1,2})-(\d{4})/);
            return dateParts[3] + "-" + dateParts[1] + "-" + dateParts[2];
        }
    }
    var getPhyID = function(phyVal) {
      var n=phyVal.indexOf("-");
      var truncID = phyVal.substr(0,n)
      return truncID;
    }
    function add_patient(){
        //alert("Hi");
        var tempDOB = "";
        var first = document.getElementById('patFirstName').value;
        var middle = document.getElementById('patMiddleName').value;
        var last = document.getElementById('patLastName').value;

        if(middle == "" && last!=""){
            var resFullName = document.getElementById('patFirstName').value + ' ' +document.getElementById('patLastName').value;  
        }
        else if(last == "" && middle == ""){
            var resFullName = document.getElementById('patFirstName').value;
        }

        var resFullName = document.getElementById('patFirstName').value + ' ' + document.getElementById('patMiddleName').value + ' ' +document.getElementById('patLastName').value;
        var Name = resFullName;
        // var MartialStatus = $('#martialStatus').val();
        var SocialSecurity = $('#ssn').val();
        var EmploymentStatus = $('#employment').val();
        var tempDOB = $('#dob').val();
        var DOB = convertDate(tempDOB);
        //var Employer = $('#employer').val();
        var Gender = $('#gender').val();
        //var ReferralSource = $('#reference').val();
        //var medicalRecord = $('#medicalRecord').val();
        var addrStreet1 = $('#addrStreet1').val();
        var addrStreet2 = $('#addrStreet2').val();
        var addrState = $('#addrState').val();
        var addrCity = $('#addrCity').val();
        var addrZip = $('#addrZip').val();
        var HomePhone =  $('#phoneHome').val();
        //var WorkPhone = $('#phoneWork').val();
        var MobilePhone = $('#phoneMobile').val();
        // if($('#notifyEmail').prop('checked')==true){
        // var IsMail = "1";
        // }
        // else{
        // var IsMail = "0";
        // }
        // if($('#notifyPhone').prop('checked')==true){
        // var IsPhoneRemainder = "1";
        // }
        // else{
        // var IsPhoneRemainder = "0";
        // }
        if($('#IsFinancialResp').prop('checked')==true){
        var IsFinancialResp = "1";
        }
        else{
        var IsFinancialResp = "0";
        }
        var IsEmailAddr = $('#email').val();
        var IsPhoneRemainder = $('#notifyPhone').val();
        var PrimaryCarePhy =  getPhyID($('#primaryCarePhy').val());
        //var DServiceLoc = getPhyID($('#defaultServiceLoc').val());
        var practiceID = sessionStorage.getItem('practiceId');

        // var IsFinancialResp = $('#IsFinancialResp').val();
        // var DPayerScenario = $('#DPayerScenario').val();
        // var Notes =  $('#Notes').val();
        $('#loader').show();
        $.ajax({
            type: "POST",
            url:"https://curismed.com/medService/patients/create",
            data:{ 
                "practiceID": practiceID,
                "fullName" : Name,
                //"martialStatus" : MartialStatus,
                "ssn" : SocialSecurity,
                "employment" : EmploymentStatus,
                "dob" : DOB,
                //"employer" : Employer,
                "gender" : Gender,
                //"reference" : ReferralSource,
                //"medicalRecord" : medicalRecord,
                "addrStreet1" : addrStreet1,
                "addrStreet2" : addrStreet2,
                "addrState" : addrState,
                "addrCity" : addrCity,
                "addrZip" : addrZip,
                "phoneHome" : HomePhone,
                //"phoneWork" : WorkPhone,
                "phoneMobile" : MobilePhone,
                //"notifyEmail" : IsMail,
                "email" : IsEmailAddr,
                //"notifyPhone" : IsPhoneRemainder,
                "primaryCarePhy" : PrimaryCarePhy,
                "defaultServiceLoc" : "3",
                "IsFinancialResp" : IsFinancialResp
                // "DPayerScenario" : DPayerScenario,
                // "Notes" : Notes
               },success:function(result) {
                $('#loader').hide();
                if (result.message == "Patient already exists") {
                    alertify.error(result.message);
                } else {
                    alertify.success("New Patient added successfully");
                    $("#step-1").css({"display": "none"});
                    $("#step-2").css({"display": "flex"});
                    $("#up1").removeClass("btn-primary");
                    $("#up2").addClass("btn-primary");
                    $("#up1").addClass("btn-default");
                    $("#up2").removeClass("btn-default");
                    var FirstName = $('#patFirstName').val();
                    var FirstName3 = FirstName.substring(0, 3);
                    var LastName = $('#patLastName').val();
                    var LastName3 = LastName.substring(0, 3);
                    var patientAccNo = FirstName3 + LastName3;
                    var AccNo = patientAccNo.toUpperCase();
                    var home = "";
                    var office = "";
                    var school = "";
                    document.getElementById('hdnChkPat').value = "1";
                    document.getElementById('hdnPatientID').value = result.patientID;
                    var homeChk = document.getElementById('homeChk');
                    if (homeChk.checked) {
                        home = "1";
                    } else {
                        home = "0";
                    }
                    var officeChk = document.getElementById('officeChk');
                    if (officeChk.checked) {
                        office = "1";
                    } else {
                        office = "0";
                    }
                    var schoolChk = document.getElementById('schoolChk');
                    if (schoolChk.checked) {
                        school = "1";
                    } else {
                        school = "0";
                    }
                    $.post("setLocations.php",
                        {
                            patientID: result.patientID,
                            home: home,
                            school: school,
                            office: office
                        },
                        function (data, status) {
                            console.log("added")
                        });

                    $.post("https://curismed.com/medService/patients/chartnumber",
                        {
                            patientID: result.patientID,
                            prefix: AccNo
                        },
                        function (data, status) {
                            document.getElementById('lblCaseNo').value = data.ChartNumber + '_1';
                            document.getElementById('policyHolder1').value = document.getElementById('hdnPatientID').value + ' - ' + document.getElementById('patFirstName').value + ' ' + document.getElementById('patMiddleName').value + ' ' + document.getElementById('patLastName').value;
                            document.getElementById('policyHolder2').value = document.getElementById('hdnPatientID').value + ' - ' + document.getElementById('patFirstName').value + ' ' + document.getElementById('patMiddleName').value + ' ' + document.getElementById('patLastName').value;
                            document.getElementById('policyHolder3').value = document.getElementById('hdnPatientID').value + ' - ' + document.getElementById('patFirstName').value + ' ' + document.getElementById('patMiddleName').value + ' ' + document.getElementById('patLastName').value;
                            var patFullName = document.getElementById('hdnPatientID').value + ' - ' + document.getElementById('patFirstName').value + ' ' + document.getElementById('patMiddleName').value + ' ' + document.getElementById('patLastName').value;
                            var tempBirth = $('#dob').val();
                            $('#policyDOB1').val(tempBirth);
                            $('#policyDOB2').val(tempBirth);
                            $('#policyDOB3').val(tempBirth);
                            var patDOB = tempBirth;
                            if ($('#IsFinancialResp').is(':checked') == true) {
                                $.ajax({
                                    type: "POST",
                                    url: "getGuarantorInfo.php",
                                    async: false,
                                    data: {
                                        patientID: document.getElementById('hdnPatientID').value
                                    }, success: function (data) {
                                        var res = JSON.parse(data);
                                        document.getElementById("policyHolder1").value = res[0].guarantorID + ' - ' + res[0].guarantorName;
                                        document.getElementById("policyDOB1").value = changeDateFormat(res[0].guarantorDOB);
                                        $("#policyRelationInsured1").val(res[0].guarantorRelationship);
                                    }
                                });
                            }
                            $('#policyRelationInsured1').change(function () {
                                if ($(this).val() != 'Self') {
                                    $.ajax({
                                        type: "POST",
                                        url: "getGuarantorInfo.php",
                                        async: false,
                                        data: {
                                            patientID: document.getElementById('hdnPatientID').value
                                        }, success: function (data) {
                                            var res = JSON.parse(data);
                                            document.getElementById("policyHolder1").value = res[0].guarantorID + ' - ' + res[0].guarantorName;
                                            document.getElementById("policyDOB1").value = changeDateFormat(res[0].guarantorDOB);
                                        }
                                    });
                                    // $('#policyHolder1').val('');
                                    // $('#policyDOB1').val('');
                                }
                                else {
                                    $('#policyHolder1').val(patFullName);
                                    $('#policyDOB1').val(patDOB);
                                }
                            });
                            $('#policyRelationInsured2').change(function () {
                                if ($(this).val() != 'Self') {
                                    $('#policyHolder2').val('');
                                    $('#policyDOB2').val('');
                                }
                                else {
                                    $('#policyHolder2').val(patFullName);
                                    $('#policyDOB2').val(patDOB);
                                }
                            });
                            $('#policyRelationInsured3').change(function () {
                                if ($(this).val() != 'Self') {
                                    $('#policyHolder3').val('');
                                    $('#policyDOB3').val('');
                                }
                                else {
                                    $('#policyHolder3').val(patFullName);
                                    $('#policyDOB3').val(patDOB);
                                }
                            });
                        });
                    if (IsFinancialResp == "1") {
                        var guarantorRelation = $('#guarantorRelation').val();
                        var guarantorName = $('#guarantorFirstName').val() + ' ' + $('#guarantorMiddleName').val() + ' ' + $('#guarantorLastName').val();
                        var guarantorDOB = convertDate($('#guarantorDOB').val());
                        var guarantorAddrStreet = $('#guarantorAddrStreet').val();
                        var guarantorAddrCity = $('#guarantorAddrCity').val();
                        var guarantorAddrState = $('#guarantorAddrState').val();
                        var guarantorAddrZip = $('#guarantorAddrZip').val();
                        var guarantorPhoneHome = $('#guarantorPhoneHome').val();

                        $.ajax({
                            type: "POST",
                            url: "addGuarantor.php",
                            async: false,
                            data: {
                                guarantorName: guarantorName,
                                guarantorDOB: guarantorDOB,
                                guarantorRelationship: guarantorRelation,
                                guarantorAddrStreet: guarantorAddrStreet,
                                guarantorAddrCity: guarantorAddrCity,
                                guarantorAddrState: guarantorAddrState,
                                guarantorAddrZip: guarantorAddrZip,
                                guarantorPhoneHome: guarantorPhoneHome,
                                patientID: result.patientID
                            }, success: function (result) {

                            }
                        });
                    }
                }
            }
        });
    }
    function validateEmail($email) {
      var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
      return emailReg.test( $email );
    }
</script>
<?php
    include('footer.html');
 ?>