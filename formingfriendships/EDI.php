<?php
session_start(); 
$fullName = $_POST["fullName"];
$patientID = $_POST["patientID"];
$dob = $_POST["dob"];
$gender = $_POST["gender"];
if($gender == "M"){
  $Male = 'X';
  $Female = '';
}
else{
  $Male = '';
  $Female = 'X';
}
$invoiceNo = $_POST["invoiceNo"];
$relationship = $_POST["relationship"];
$ssn = $_POST["ssn"];
$martialStatus = $_POST["martialStatus"];
$medicalRecord = $_POST["medicalRecord"];
$employment = $_POST["employment"];
$employer = $_POST["employer"];
$reference = $_POST["reference"];
$addrStreet1 = $_POST["addrStreet1"];
$addrStreet2 = $_POST["addrStreet2"];
$addrCity = $_POST["addrCity"];
$addrState = $_POST["addrState"];
$addrCountry = $_POST["addrCountry"];
$addrZip = $_POST["addrZip"];
$email = $_POST["email"];
$phoneHome = $_POST["phoneHome"];
$phoneWork = $_POST["phoneWork"];
$phoneMobile = $_POST["phoneMobile"];
$notifyEmail = $_POST["notifyEmail"];
$notifyPhone = $_POST["notifyPhone"];
$primaryCarePhy = $_POST["primaryCarePhy"];
$referringPhy = $_POST["referringPhy"];
$defaultRenderPhy = $_POST["defaultRenderPhy"];
$defaultServiceLoc = $_POST["defaultServiceLoc"];
$chartNo = $_POST["chartNo"];
$emergencyContact = $_POST["emergencyContact"];
$emergencyPhone = $_POST["emergencyPhone"];
$tempName = explode(" ", $fullName);
if($tempName[0]!=""){
  $firstName = $tempName[0];
}
else{
  $firstName = "";
}
if($tempName[1]!=""){
  $middleName = $tempName[1];
}
else{
  $middleName = "";
}
if($tempName[2]!=""){
  $lastName = $tempName[2];
}
else{
  $lastName = "";
}
$insureID1 = $_POST["insureID"];
$insureID2 = $_POST["secinsureID"];
if($insureID2 != ""){
  $insureID2 = str_pad($insureID2, 29);
  $insuranceAddr2 = $_POST["secinsuranceAddr"];
  $insName2 = $_POST["secinsuranceID"];
  $insName2 = str_pad($insName2, 49);
  $seclastName = $lastName.',';
  $secfirstName = $firstName;
  $secmiddleName = $middleName;
  $IsOtherHealthYes = "X";
  $IsOtherHealthNo = "";
}
else{
  $insureID2 = str_pad($insureID2, 29);
  $insuranceAddr2 = "";
  $insName2 = "";
  $insName2 = str_pad($insName2, 49);
  $seclastName = "";
  $secfirstName = "";
  $secmiddleName = "";
  $IsOtherHealthYes = "";
  $IsOtherHealthNo = "X";
}
$payor = $_POST["payor"];
$insuranceAddr1 = $_POST["insuranceAddr"];
$insName1 = $_POST["insuranceID"];
$individualNPI = $_POST["individualNPI"];
$providerName= $_POST["providerName"];
$chartNo= $_POST["chartNo"];

$facilityName = $_POST["facilityName"];
$facilityAddr = $_POST["facilityAddr"];
$practiceEIN = $_POST["practiceEIN"];
$practiceName = $_POST["practiceName"];
$practiceAddr = $_POST["practiceAddr"];


$assignedProvider = $_POST["assignedProvider"];
$principalDiag = $_POST["principalDiag"];
$defDiag2 = $_POST["defDiag2"];
$defDiag3 = $_POST["defDiag3"];
$defDiag4 = $_POST["defDiag4"];
$fromDt = $_POST["fromDt"];
$toDt = $_POST["toDt"];
$PrimaryCarePhysician = $_POST["PrimaryCarePhysician"];
$placeOfService = $_POST["placeOfService"];
$proced = $_POST["proced"];
$mod1 = $_POST["mod1"];
$charge = $_POST["total"].' 00';
$units = $_POST["units"];

$insuranceAddr1 = explode(",", $insuranceAddr1);
$cou = count($insuranceAddr1);
if($cou == 3){
  $concatInsAddr1 = $insuranceAddr1[0].', '.$insuranceAddr1[1];
  $nextInsAddr1 = $insuranceAddr1[2];
}
else{
  $concatInsAddr1 = $insuranceAddr1[0];
  $nextInsAddr1 = $insuranceAddr1[1]; 
}
$practiceAddr = explode(",", $practiceAddr);
$facilityAddr = explode(",", $facilityAddr);


$tempDOB = explode("-", $dob);
if($tempDOB[0]!=""){
  $year = $tempDOB[0];
}
else{
  $year = "";
}
if($tempDOB[1]!=""){
  $month = $tempDOB[1];
}
else{
  $month = "";
}
if($tempDOB[2]!=""){
  $date = $tempDOB[2];
}
else{
  $date = "";
}

$tempfromDt = explode("-", $fromDt);
if($tempfromDt[0]!=""){
  $Smonth = $tempfromDt[0];
}
else{
  $Smonth = "";
}
if($tempfromDt[1]!=""){
  $Sdate = $tempfromDt[1];
}
else{
  $Sdate = "";
}
if($tempfromDt[2]!=""){
  $Syear = substr($tempfromDt[2],-2);
}
else{
  $Syear = "";
}

$temptoDt = explode("-", $toDt);
if($temptoDt[0]!=""){
  $Emonth = $temptoDt[0];
}
else{
  $Emonth = "";
}
if($temptoDt[1]!=""){
  $Edate = $temptoDt[1];
}
else{
  $Edate = "";
}
if($temptoDt[2]!=""){
  $Eyear = substr($temptoDt[2],-2);
}
else{
  $Eyear = "";
}
$Lname= strlen($lastName);
$Fname= strlen($firstName);
$Mname= strlen($middleName);
if($Mname !=""){
  $tot = 27 - ($Lname + $Fname + $Mname);
}
else{
  $tot = 26 - ($Lname + $Fname + $Mname);
}
$addrStreet = str_pad($addrStreet1, 31);
$addrCity = str_pad($addrCity, 25);
$addrState = str_pad($addrState, 20);
$addrZip = str_pad($addrZip, 13);
$phoneHome = str_pad($phoneHome, 35);
$facilityName = str_pad($facilityName, 27);

if($principalDiag != "" && $defDiag2 != "" && $defDiag3 != "" && $defDiag4 != "" ){
  $pointer = "ABCD";
}
else if($principalDiag != "" && $defDiag2 != "" && $defDiag3 != "" && $defDiag4 == "" ){
  $pointer = "ABC";
}
else if($principalDiag != "" && $defDiag2 != "" && $defDiag3 == "" && $defDiag4 == "" ){
  $pointer = "AB";
}
else if($principalDiag != "" && $defDiag2 == "" && $defDiag3 == "" && $defDiag4 == "" ){
  $pointer = "A";
}
else if($principalDiag == "" && $defDiag2 == "" && $defDiag3 == "" && $defDiag4 == "" ){
  $pointer = "";
}

$mod1 = str_pad($mod1, 12);
$pointer = str_pad($pointer, 7);
$principalDiag = str_pad($principalDiag, 13);
$defDiag2 = str_pad($defDiag2, 13);
$defDiag3 = str_pad($defDiag3, 13);
$defDiag4 = str_pad($defDiag4, 13);
$charge1  = str_pad($charge, 8);
$charge2  = str_pad($charge, 7);
$units  = str_pad($units, 4);
$dummy  = str_pad('', 39);


$providerName = str_pad($providerName, 22);
$facilityAddr[0] = str_pad($facilityAddr[0], 27);
$practiceAddr[0] = str_pad($practiceAddr[0], 30);

$middleName = str_pad($middleName, $tot-1);
$CurrNow = date('Y-m-d');
$txtFileName = $fullName.'_'.$CurrNow.".txt";
$CurrDate = date('Y-m-d');
$tempCurrDate = explode("-", $CurrDate);
if($tempCurrDate[0]!=""){
  $Curryear = $tempCurrDate[0];
}
else{
  $Curryear = "";
}
if($tempCurrDate[1]!=""){
  $Currmonth = $tempCurrDate[1];
}
else{
  $Currmonth = "";
}
if($tempCurrDate[2]!=""){
  $Currdate = $tempCurrDate[2];
}
else{
  $Currdate = "";
}

if($invoiceNo != ""){
  if (isset($_SESSION['invoice'])) {
    if($_SESSION['invoice'] != $invoiceNo){
  $fp = fopen('EDI/'.$invoiceNo, 'a');
fwrite($fp, '                                       '.$insName1.' 
                                       '.$concatInsAddr1.' 
                                      '.$nextInsAddr1.' 



 X                                               '.$insureID1.'                    
                                                                               
 '.$lastName.', '.$firstName.' '.$middleName.' '.$month.' '.$date.' '.$year.' '.$Male.'   '.$Female.'    '.$lastName.', '.$firstName.' '.$middleName.'            

 '.$addrStreet.'X                '.$addrStreet.'             

 '.$addrCity.''.$addrState.'   '.$addrCity.''.$addrState.'  

 '.$addrZip.''.$phoneHome.''.$addrZip.''.$phoneHome.'   

 '.$seclastName.' '.$secfirstName.' '.$secmiddleName.'                                                                            

 '.$insureID2.'          X           '.$month.' '.$date.' '.$year.'     '.$Male.'      '.$Female.'   

                                        X                                     

                                        X                                     

 '.$insName2.'   '.$IsOtherHealthYes.'     '.$IsOtherHealthNo.'                     



       SIGNATURE ON FILE             '.$Currmonth.''.$Currdate.''.$Curryear.'           SIGNATURE ON FILE       





                                                         X                     
  '.$dummy.' 0                                                                            
  '.$principalDiag.''.$defDiag2.''.$defDiag3.''.$defDiag4.'                                                                       





 '.$Smonth.' '.$Sdate.' '.$Syear.' '.$Emonth.' '.$Edate.' '.$Eyear.' '.$placeOfService.'     '.$proced.' '.$mod1.''.$pointer.' '.$charge1.''.$units.'   '.$individualNPI.'  











 '.$practiceEIN.'        X   '.$chartNo.'     X               '.$charge2.'            0 00 
                                                                               
                       '.$facilityName.''.$practiceName.'
 '.$providerName.''.$facilityAddr[0].''.$practiceAddr[0].' 
              '.$Currmonth.''.$Currdate.''.$Curryear.' '.$facilityAddr[1].''.$practiceAddr[1].'          
                                                  1376973651





');
$loc = 'EDI/'.$invoiceNo;

$con = mysql_connect("localhost:3306","curis_user","Curis@123");

if(!$con){
  die("Error : ".mysql_error());
}

mysql_select_db("curismed_aba",$con);

$date = new DateTime('now', new DateTimeZone('America/Chicago'));
$date1 = $date->format('Y-m-d H:i:s');

mysql_query("INSERT INTO `akrone18_ananth`.`m_edi` (`ediInvoiceNo`, `ediLocation`, `ediCreatedOn`) VALUES ('$invoiceNo', '$loc', '$date1')");
$_SESSION['invoice'] = $_POST["invoiceNo"];
//echo "INSERT INTO `akrone18_ananth`.`m_edi` (`ediInvoiceNo`, `ediLocation`, `ediCreatedOn`) VALUES ('$invoiceNo', '$loc', '$date1')";
}
else{
    $fp = fopen('EDI/'.$invoiceNo, 'a');
fwrite($fp, '                                       '.$insName1.' 
                                       '.$concatInsAddr1.' 
                                       '.$nextInsAddr1.' 



 X                                               '.$insureID1.'                    
                                                                               
 '.$lastName.', '.$firstName.' '.$middleName.' '.$month.' '.$date.' '.$year.' '.$Male.'   '.$Female.'    '.$lastName.', '.$firstName.' '.$middleName.'            

 '.$addrStreet.'X                '.$addrStreet.'             

 '.$addrCity.''.$addrState.'   '.$addrCity.''.$addrState.'  

 '.$addrZip.''.$phoneHome.''.$addrZip.''.$phoneHome.'   

 '.$seclastName.' '.$secfirstName.' '.$secmiddleName.'                                                                            

 '.$insureID2.'          X           '.$month.' '.$date.' '.$year.'     '.$Male.'      '.$Female.'   

                                        X                                     

                                        X                                     

 '.$insName2.'   '.$IsOtherHealthYes.'     '.$IsOtherHealthNo.'                     



       SIGNATURE ON FILE             '.$Currmonth.''.$Currdate.''.$Curryear.'           SIGNATURE ON FILE       





                                                         X                     
  '.$dummy.' 0                                                                            
  '.$principalDiag.''.$defDiag2.''.$defDiag3.''.$defDiag4.'                                                                       





 '.$Smonth.' '.$Sdate.' '.$Syear.' '.$Emonth.' '.$Edate.' '.$Eyear.' '.$placeOfService.'     '.$proced.' '.$mod1.''.$pointer.' '.$charge1.''.$units.'   '.$individualNPI.'  











 '.$practiceEIN.'        X   '.$chartNo.'     X               '.$charge2.'            0 00 
                                                                               
                       '.$facilityName.''.$practiceName.'
 '.$providerName.''.$facilityAddr[0].''.$practiceAddr[0].' 
              '.$Currmonth.''.$Currdate.''.$Curryear.' '.$facilityAddr[1].''.$practiceAddr[1].'          
                                                  1376973651





');
$loc = 'EDI/'.$invoiceNo;

$con = mysql_connect("localhost:3306","curis_user","Curis@123");

if(!$con){
  die("Error : ".mysql_error());
}

mysql_select_db("curismed_aba",$con);

$date = new DateTime('now', new DateTimeZone('America/Chicago'));
$date1 = $date->format('Y-m-d H:i:s');

mysql_query("INSERT INTO `akrone18_ananth`.`m_edi` (`ediInvoiceNo`, `ediLocation`, `ediCreatedOn`) VALUES ('$invoiceNo', '$loc', '$date1')");
$_SESSION['invoice'] = $_POST["invoiceNo"];
echo "INSERT INTO `akrone18_ananth`.`m_edi` (`ediInvoiceNo`, `ediLocation`, `ediCreatedOn`) VALUES ('$invoiceNo', '$loc', '$date1')";
}
fclose($fp);

}
else{
  $_SESSION['invoice'] = $_POST["invoiceNo"];
   $fp = fopen('EDI/'.$invoiceNo, 'a');
fwrite($fp, '                                       '.$insName1.' 
                                       '.$concatInsAddr1.' 
                                       '.$nextInsAddr1.' 



 X                                               '.$insureID1.'                    
                                                                               
 '.$lastName.', '.$firstName.' '.$middleName.' '.$month.' '.$date.' '.$year.' '.$Male.'   '.$Female.'    '.$lastName.', '.$firstName.' '.$middleName.'            

 '.$addrStreet.'X                '.$addrStreet.'             

 '.$addrCity.''.$addrState.'   '.$addrCity.''.$addrState.'  

 '.$addrZip.''.$phoneHome.''.$addrZip.''.$phoneHome.'   

 '.$seclastName.' '.$secfirstName.' '.$secmiddleName.'                                                                            

 '.$insureID2.'          X           '.$month.' '.$date.' '.$year.'     '.$Male.'      '.$Female.'   

                                        X                                     

                                        X                                     

 '.$insName2.'   '.$IsOtherHealthYes.'     '.$IsOtherHealthNo.'                     



       SIGNATURE ON FILE             '.$Currmonth.''.$Currdate.''.$Curryear.'           SIGNATURE ON FILE       





                                                         X                     
  '.$dummy.' 0                                                                            
  '.$principalDiag.''.$defDiag2.''.$defDiag3.''.$defDiag4.'                                                                       





 '.$Smonth.' '.$Sdate.' '.$Syear.' '.$Emonth.' '.$Edate.' '.$Eyear.' '.$placeOfService.'     '.$proced.' '.$mod1.''.$pointer.' '.$charge1.''.$units.'   '.$individualNPI.'  











 '.$practiceEIN.'        X   '.$chartNo.'     X               '.$charge2.'            0 00 
                                                                               
                       '.$facilityName.''.$practiceName.'
 '.$providerName.''.$facilityAddr[0].''.$practiceAddr[0].' 
              '.$Currmonth.''.$Currdate.''.$Curryear.' '.$facilityAddr[1].''.$practiceAddr[1].'          
                                                  1376973651





');

$loc = 'EDI/'.$invoiceNo;

$con = mysql_connect("localhost:3306","curis_user","Curis@123");

if(!$con){
  die("Error : ".mysql_error());
}

mysql_select_db("curismed_aba",$con);

$date = new DateTime('now', new DateTimeZone('America/Chicago'));
$date1 = $date->format('Y-m-d H:i:s');

mysql_query("INSERT INTO `akrone18_ananth`.`m_edi` (`ediInvoiceNo`, `ediLocation`, `ediCreatedOn`) VALUES ('$invoiceNo', '$loc', '$date1')");
fclose($fp);
//echo "INSERT INTO `akrone18_ananth`.`m_edi` (`ediInvoiceNo`, `ediLocation`, `ediCreatedOn`) VALUES ('$invoiceNo', '$loc', '$date1')";
}
}
else{


$fp = fopen('EDI/'.$txtFileName, 'w');
fwrite($fp, '                                       '.$insName1.' 
                                       '.$concatInsAddr1.' 
                                       '.$nextInsAddr1.' 



 X                                               '.$insureID1.'                    
                                                                               
 '.$lastName.', '.$firstName.' '.$middleName.' '.$month.' '.$date.' '.$year.' '.$Male.'   '.$Female.'    '.$lastName.', '.$firstName.' '.$middleName.'            

 '.$addrStreet.'X                '.$addrStreet.'             

 '.$addrCity.''.$addrState.'   '.$addrCity.''.$addrState.'  

 '.$addrZip.''.$phoneHome.''.$addrZip.''.$phoneHome.'   

 '.$seclastName.' '.$secfirstName.' '.$secmiddleName.'                                                                            

 '.$insureID2.'          X           '.$month.' '.$date.' '.$year.'     '.$Male.'      '.$Female.'   

                                        X                                     

                                        X                                     

 '.$insName2.'   '.$IsOtherHealthYes.'     '.$IsOtherHealthNo.'                     



       SIGNATURE ON FILE             '.$Currmonth.''.$Currdate.''.$Curryear.'           SIGNATURE ON FILE       





                                                         X                     
  '.$dummy.' 0                                                                            
  '.$principalDiag.''.$defDiag2.''.$defDiag3.''.$defDiag4.'                                                                       





 '.$Smonth.' '.$Sdate.' '.$Syear.' '.$Emonth.' '.$Edate.' '.$Eyear.' '.$placeOfService.'     '.$proced.' '.$mod1.''.$pointer.' '.$charge1.''.$units.'   '.$individualNPI.'  











 '.$practiceEIN.'        X   '.$chartNo.'     X               '.$charge2.'            0 00 
                                                                               
                       '.$facilityName.''.$practiceName.'
 '.$providerName.''.$facilityAddr[0].''.$practiceAddr[0].' 
              '.$Currmonth.''.$Currdate.''.$Curryear.' '.$facilityAddr[1].''.$practiceAddr[1].'          
                                                  1376973651');

$loc = 'EDI/'.$invoiceNo;

$con = mysql_connect("localhost:3306","curis_user","Curis@123");

if(!$con){
  die("Error : ".mysql_error());
}

mysql_select_db("curismed_aba",$con);

$date = new DateTime('now', new DateTimeZone('America/Chicago'));
$date1 = $date->format('Y-m-d H:i:s');

mysql_query("INSERT INTO `akrone18_ananth`.`m_edi` (`ediInvoiceNo`, `ediLocation`, `ediCreatedOn`) VALUES ('$invoiceNo', '$loc', '$date1')");
$_SESSION['invoice'] = $_POST["invoiceNo"];

fclose($fp);
}
//echo "INSERT INTO `akrone18_ananth`.`m_edi` (`ediInvoiceNo`, `ediLocation`, `ediCreatedOn`) VALUES ('$invoiceNo', '$loc', '$date1')";
?>