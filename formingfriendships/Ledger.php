<?php
 session_start();

 if (!isset($_SESSION['userID'])) {
  header("Location: index.html");
 }
 include('header.html');
 ?>


<div class="row">
	<!-- <img src="assets/beat.gif" id="loader" style="position:absolute; left:50%; z-index:99999; top:35%;" /> -->
	<h2 style="color: #251367; margin-left:20px;">Ledger</h2>
	<div class="col-md-12">
      <div class="widget box" >
          <div class="widget-content" style="min-height:100px">
            <div id="filter" class="col-md-12">
                <input type="hidden" id="_hdnClaimID"/>
                <div class="card-body">
                  <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group row">
                            <div class="col-5">
                                <label for="input-text" class="control-label float-right txt_media1">DOS</label>
                            </div>
                            <div class="col-7">
                                <input type="text" class="form-control required" id="dos" placeholder="Date Of Service" />
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group row">
                            <div class="col-5">
                                <label for="input-text" class="control-label float-right txt_media1">From Date</label>
                            </div>
                            <div class="col-7">
                                <input type="text" class="form-control required" id="fromDate" placeholder="From Date" />
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group row">
                            <div class="col-5">
                                <label for="input-text" class="control-label float-right txt_media1">To Date</label>
                            </div>
                            <div class="col-7">
                                <input type="text" class="form-control required" id="toDate" placeholder="To Date" />
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group row">
                            <div class="col-5">
                                <label for="input-text" class="control-label float-right txt_media1">Client Name</label>
                            </div>
                            <div class="col-7">
                                <input type="text" class="form-control required" id="clientName" placeholder="Client Name" />
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group row">
                            <div class="col-5">
                                <label for="input-text" class="control-label float-right txt_media1">Provider Name</label>
                            </div>
                            <div class="col-7">
                                <input type="text" class="form-control required" id="providerName" placeholder="Provider Name" />
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group row">
                            <div class="col-5">
                                <label for="input-text" class="control-label float-right txt_media1">Insurance Name</label>
                            </div>
                            <div class="col-7">
                                <input type="text" class="form-control required" id="insName" placeholder="Insurance Name" />
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group row">
                            <div class="col-5">
                                <label for="input-text" class="control-label float-right txt_media1">CPT</label>
                            </div>
                            <div class="col-7">
                                <input type="text" class="form-control required" id="cptVal" placeholder="Procedure Code" />
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group row">
                            <div class="col-5">
                                <label for="input-text" class="control-label float-right txt_media1"></label>
                            </div>
                            <div class="col-7">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group row">
                            <div class="col-12">
                                <input type="button" class="btn btn-primary float-right" id="btnGo" value="Get Details" />
                            </div>
                        </div>
                    </div>
                  </div>
                </div>
                <div class="col-12" id="bulkDiv">
                  <div class="card-body">
                    <div class="row">
                      <div class="col-sm-2">
                          <div class="form-group row">
                              <div class="col-12">
                                <input type="text" class="form-control required" id="seqCPT" placeholder="Procedure Code" />
                              </div>
                          </div>
                      </div>
                      <div class="col-sm-2">
                          <div class="form-group row">
                              <div class="col-12">
                                <input type="text" class="form-control required" id="seqProvider" placeholder="Provider Name" />
                              </div>
                          </div>
                      </div>
                      <div class="col-sm-2">
                          <div class="form-group row">
                              <div class="col-12">
                                <input type="text" class="form-control required" id="seqMOD" placeholder="Modifier" />
                              </div>
                          </div>
                      </div>
                      <div class="col-sm-3">
                          <div class="form-group row">
                              <div class="col-7">
                                <select id="seqStatus" class="form-control">
                                  <option value="">--Select Status--</option>
                                  <option value="Confirmed" selected>Confirmed</option>
                                <option value="Rendered" selected>Ready to Bill</option>
                                <option value="Ready" selected>Ready for EDI</option>
                                </select>
                              </div>
                          </div>
                      </div>
                      <div class="col-sm-2">
                          <div class="form-group row">
                              <div class="col-12">
                                <input type="button" class="btn btn-primary" id="btnFin" value="Update" />
                              </div>
                          </div>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
            <div class="widget-content">
                <div class="card-body">
                <div class="bs-example">
                    <ul class="nav nav-tabs" style="margin-bottom: 15px;">
                        <li class="nav-item">
                            <a href="#tab_3_1" style="color:#000;" data-toggle="tab" class="nav-link active">Still Confirmed</a>
                        </li>
                        <li class="nav-item">
                            <a href="#tab_3_2" style="color:#000;" data-toggle="tab" class="nav-link">Ready to Bill</a>
                        </li>
                        <li class="nav-item">
                            <a href="#tab_3_3" style="color:#000;" data-toggle="tab" class="nav-link">Ready for EDI</a>
                        </li>
                        <li class="nav-item">
                            <a href="#tab_3_4" style="color:#000;" data-toggle="tab" class="nav-link">Balance</a>
                        </li>
                        </li>
                    </ul>
                    <div id="myTabContent" class="tab-content">
                        <div class="tab-pane active" id="tab_3_1">
                          <div class="table-responsive">
                              <table id="test4" class="table table-bordered display"  cellspacing="0" width="100%">
                              </table>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="tab_3_2">
                          <div class="table-responsive">
                            <table id="test1" class="table table-bordered display"  cellspacing="0" width="100%">
                            </table>
                          </div>
                        </div>
                        <div class="tab-pane fade" id="tab_3_3">
                          <div class="table-responsive">
                            <table id="test2" class="table table-bordered display"  cellspacing="0" width="100%">
                            </table>
                          </div>
                        </div>
                        <div class="tab-pane fade" id="tab_3_4">
                          <div class="table-responsive">
                            <table id="test3" class="table table-bordered display"  cellspacing="0" width="100%">
                            </table>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
          	<!-- <div id="tableDiv">
          		<table id="testAll" class="table table-striped  table-hover table-checkable" data-horizontal-width="150%">
				</table>
                  <table id="testPatient" class="table table-striped  table-hover table-checkable" data-horizontal-width="150%">
				</table>
				<table id="testInsurance" class="table table-striped  table-hover table-checkable" data-horizontal-width="150%">
				</table>
			</div> -->
          </div>
      </div>

</div>
<div class="modal fade" id="swapModal" tabindex="-1">
    <div class="modal-dialog" style="width:50%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Swap Claim</h4>
            </div>
            <div class="modal-body" style="min-height:80px;">
              <div class="card-body">
                <div class="row">
                  <div class="col-sm-8">
                      <div class="form-group row">
                          <div class="col-5">
                              <label for="input-text" class="control-label float-right txt_media1">Move to </label>
                          </div>
                          <div class="col-7">
                              <select class="form-control" id="moveTo">
                                <option value="Confirmed" selected>Confirmed</option>
                                <option value="Rendered" selected>Ready to Bill</option>
                                <option value="Ready" selected>Ready for EDI</option>
                            </select>
                          </div>
                      </div>
                  </div>
                  <div class="col-sm-4">
                      <div class="form-group row">
                          <div class="col-12">
                              <input type="button" class="btn btn-primary pull-right" id="btnSwap" value="Swap Claim">
                          </div>
                      </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="modal-footer">
                <input type="button" class="btn btn-default" data-dismiss="modal" value="Exit" />
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<div class="modal fade" id="claimDetails" tabindex="-1">
    <div class="modal-dialog" style="width:75%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Claim Details</h4>
            </div>
            <div class="modal-body" style="min-height:120px;">
                <div class="form-group col-md-4" >
                    <label class="control-label col-md-6">Client Name </label>
                    <div class="col-md-6">
                        <input type="text" class="form-control required" id="modClientName" disabled />
                    </div>
                </div>
                <div class="form-group col-md-4" >
                    <label class="control-label col-md-6">Therapist Name </label>
                    <div class="col-md-6">
                        <input type="text" class="form-control required" id="modTherapist" disabled />
                    </div>
                </div>
                <div class="form-group col-md-4" >
                    <label class="control-label col-md-6">Activity </label>
                    <div class="col-md-6">
                        <input type="text" class="form-control required" id="modActivity" disabled />
                    </div>
                </div>
                <div class="form-group col-md-4" >
                    <label class="control-label col-md-6">DOS </label>
                    <div class="col-md-6">
                        <input type="text" class="form-control required" id="modDOS" disabled />
                    </div>
                </div>
                <div class="form-group col-md-4" >
                    <label class="control-label col-md-6">CPT </label>
                    <div class="col-md-6">
                        <input type="text" class="form-control required" id="modCPT"  />
                    </div>
                </div>
                <div class="form-group col-md-4" >
                    <label class="control-label col-md-6">Modifier </label>
                    <div class="col-md-6">
                        <input type="text" class="form-control required" id="modMOD"  />
                    </div>
                </div>
                <div class="form-group col-md-4" >
                    <label class="control-label col-md-6">POS </label>
                    <div class="col-md-6">
                        <select class="form-control" id="modPOS">
                            <option value="12">Home</option>
                            <option value="11">Office</option>
                        </select>
                    </div>
                </div>
                <div class="form-group col-md-4" >
                    <label class="control-label col-md-6">Units </label>
                    <div class="col-md-6">
                        <input type="text" class="form-control required" id="modUnits"  />
                    </div>
                </div>
                <div class="form-group col-md-4" >
                    <label class="control-label col-md-6">Charges </label>
                    <div class="col-md-6">
                        <input type="text" class="form-control required" id="modCharges"  />
                    </div>
                </div>
                <div class="form-group col-md-4" >
                    <label class="control-label col-md-6">Claim # </label>
                    <div class="col-md-6">
                        <input type="text" class="form-control required" id="modClaimNo" disabled />
                    </div>
                </div>
                <div class="form-group col-md-4" >
                    <label class="control-label col-md-6">Status </label>
                    <div class="col-md-6">
                        <select class="form-control" id="modStatus" disabled>
                            <option value=""></option>
                                <option value="Confirm">Confirmed</option>
                                <option value="UnConfirm">UnConfirmed</option>
                                <option value="Noshow">No Show</option>
                                <option value="Cancelled">Cancelled</option>
                                <option value="Hold">Hold</option>
                                <option value="Cancelled by Client">Cancelled by Client</option>
                                <option value="Cancelled by Provider">Cancelled by Provider</option>
                                <option value="Rendered">Rendered</option>
                        </select>
                    </div>
                </div>
                <div class="form-group col-md-4" >
                    <label class="control-label col-md-6">Move to </label>
                    <div class="col-md-6" id="ddlMove">
                        <select class="form-control">
                            <option value="Ready">Ready To Send</option>
                            <option value="Submit">Submitted</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <input type="button" class="btn btn-default" data-dismiss="modal" value="Exit" />
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<style type="text/css">
.form-group{
  margin-bottom: 0;
}
.table .form-control{
  padding:0.15rem 0;
}
</style>
<?php
  include('footer.html');
 ?>

<script>
  $(document).ready(function(){

    $(".navigation li").parent().find('li').removeClass("active");

    $(".ti-menu-alt").click();

    document.getElementById("lblUser").innerHTML = sessionStorage.getItem('userName');

    $("#navv5").addClass("active");
    
    $('#divDDL').hide();
    getAllClaimsInfo();
    var val = "";
    $(document).on('click','.redPat',function() {
      var temp = $(this).attr('class').split(' ')[0];
      var newTemp = temp.replace('redirectPatient','');
      sessionStorage.setItem("patientId", newTemp);
      window.location.href ="EditPatient.php";
    });
    document.getElementById('practiceName').innerHTML = sessionStorage.getItem('practiceName');
    $("#bulkDiv").hide();
        var patientList = <?php include('getPat1.php') ?>;
        var insurances = <?php include('insuranceList1.php') ?>;
        var PrimaryPhy = <?php include('primaryPhysician.php') ?>;
        
        $("#insName").autocomplete({
            source: insurances,
            autoFocus:true
        });
        $('#fromDate').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $("#providerName").autocomplete({
            source: PrimaryPhy,
            autoFocus:true
        });
        $("#seqProvider").autocomplete({
            source: PrimaryPhy,
            autoFocus:true
        });
        $("#clientName").autocomplete({
            source: patientList,
            autoFocus:true
        });
        $('#toDate').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#dos').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });

        $("#btnFin").click(function(){
          var searchIDs = $('input:checked').map(function(){
            return $(this).attr('id');
          });
          for(var i=0;i<searchIDs.length;i++){
            var temp = searchIDs[i];
            var newTemp = temp.replace('chk','');
            document.getElementById('_hdnClaimID').value = newTemp;
            var tempCPT = document.getElementById("seqCPT").value;
            var temp_Mod = document.getElementById("seqMOD").value;
            var tempMod = temp_Mod.split(" ");
            var tempMod1 = tempMod[0];
            var tempMod2 = tempMod[1];
            var tempMod3 = tempMod[2];
            var tempMod4 = tempMod[3];
            var tempProvider = document.getElementById("seqProvider").value;
            var tempStatus = document.getElementById("seqStatus").value;
            var n1=tempProvider.indexOf("-");
            tempProvider = tempProvider.substr(0,n1-1);
            $.ajax({
                type: "POST",
                url:"bulkUpdateLedger.php",
                asyn:false,
                data:{
                  claimID : newTemp,
                  tempCPT : tempCPT,
                  tempMod1 : tempMod1,
                  tempMod2 : tempMod2,
                  tempMod3 : tempMod3,
                  tempMod4 : tempMod4,
                  tempProvider : tempProvider,
                  tempStatus : tempStatus
                },success:function(result){
                  console.log(result);
                  window.location.reload();
                }
            });
          }
        });


        $(document).on('click','.xchange',function() {
          var temp = $(this).attr('class').split(' ')[0];
          var newTemp = temp.replace('xchange','');
          document.getElementById('_hdnClaimID').value = newTemp;
          $("#swapModal").modal("show");
        });
        $("#btnSwap").click(function(){
            var Status = document.getElementById("moveTo").value;
            var newTemp = document.getElementById('_hdnClaimID').value;
              $.ajax({
                  type: "POST",
                  url:"updateStatus.php",
                  data:{
                    claimID : newTemp,
                    status : Status
                  },success:function(result){
                    $("#swapModal").modal("hide");
                    alertify.success("Successfully Updated");
                    window.location.reload();
                  }
              });
        })
        $(document).on('click','.editLedger',function() {
          var temp = $(this).attr('class').split(' ')[0];
          var newTemp = temp.replace('edit','');
          document.getElementById('_hdnClaimID').value = newTemp;
          var txtCPT = 'txtCPT'+newTemp;
          var txtMod = 'txtMod'+newTemp;
          var txtPOS = 'txtPOS'+newTemp;
          var txtUnits = 'txtUnits'+newTemp;
          var txtCharges = 'txtCharges'+newTemp;
          var tempCPT = document.getElementById(txtCPT).value;
          var temp_Mod = document.getElementById(txtMod).value;
          var tempMod = temp_Mod.split(" ");
          var tempMod1 = tempMod[0];
          var tempMod2 = tempMod[1];
          var tempMod3 = tempMod[2];
          var tempMod4 = tempMod[3];
          var tempPOS = document.getElementById(txtPOS).value;
          var tempUnits = document.getElementById(txtUnits).value;
          var tempCharges = document.getElementById(txtCharges).value;
          $.ajax({
              type: "POST",
              url:"updateLedger.php",
              data:{
                claimID : newTemp,
                tempCPT : tempCPT,
                tempMod1 : tempMod1,
                tempMod2 : tempMod2,
                tempMod3 : tempMod3,
                tempMod4 : tempMod4,
                tempPOS : tempPOS,
                tempUnits : tempUnits,
                tempCharges : tempCharges
              },success:function(result){
                alertify.success("Successfully Updated");
              }
          });
        });
        $("#btnGo").click(function(){
            var clientName = $("#clientName").val();
            var nos=clientName.indexOf("- ");
            clientName = clientName.substr(0,nos-1);
            var insName = $("#insName").val();
            var providerNa = $("#providerName").val();
            var n=providerNa.indexOf("-");
            var providerName = providerNa.substr(0,n-1);
            var n1=insName.indexOf("-");
            insName = insName.substr(0,n1-1);
            var dos = convertDate($("#dos").val());
            var cpt = $("#cptVal").val();
            var fromDate = convertDate($("#fromDate").val());
            var toDate = convertDate($("#toDate").val());
            $.ajax({
              type: "POST",
              url:"getLedgerInfo.php",
              data:{
                clientName : clientName,
                providerName : providerName,
                insName : insName,
                cpt : cpt,
                dos : dos,
                fromDate : fromDate,
                toDate : toDate,
              },success:function(result){
                var data1 = JSON.parse(result);
                var dt = [];
                $.each(data1,function(i,v) {
                    dt.push([data1[i].claimID,data1[i].claimID,data1[i].fullName,data1[i].phyName,data1[i].activityName,data1[i].fromDt,data1[i].proced,data1[i].mod1,data1[i].appLocation,data1[i].units,data1[i].total,data1[i].claimNumber,data1[i].claimStatus,data1[i].claimID]);
                });
                $('#test1').DataTable({
                    "aaSorting": [[ 0, "desc" ]],
                    "destroy": true,
                    "data" : dt, 
                    
                    columns: [
                        {"title": "Claim ID","visible" : false},
                        {"title": "",
                            "render": function ( data, type, full, meta ) {
                              return '<input type="checkbox" class="chk chk'+full[0]+' form-control" id="chk'+full[0]+'" />';
                            }
                        },
                        {"title": "Client Name",
                            "render": function ( data, type, full, meta ) {
                              return data;
                            }
                        },
                        {"title": "Client Provider"},
                        {"title": "Activity"},
               //          {"title": "Bill Date",
               //           "render": function ( data, type, full, meta ) {
                        //       return changeDateFormat(data);
                        //     }
                        // },
                        {"title": "DOS",
                            "render": function ( data, type, full, meta ) {
                              return changeDateFormat(data);
                            }
                        },
                        {"title": "CPT      ",
                            "render": function ( data, type, full, meta ) {
                              return '<input type="text" class="txtCPT txtCPT'+full[0]+' form-control" style="padding-right:2px;" id="txtCPT'+full[0]+'" value="'+data+'" />';
                            }
                        },
                        {"title": "Modifier",
                            "render": function ( data, type, full, meta ) {
                              return '<input type="text" class="txtMod txtMod'+full[0]+' form-control" style="padding-right:2px;" id="txtMod'+full[0]+'" value="'+data+'" />';
                            }
                        },
                        {"title": "POS",
                            "render": function ( data, type, full, meta ) {
                              return '<input type="text" class="txtPOS txtPOS'+full[0]+' form-control" style="padding-right:2px;" id="txtPOS'+full[0]+'" value="'+data+'" />';
                            }
                        },
                        {"title": "Units",
                            "render": function ( data, type, full, meta ) {
                              return '<input type="text" class="txtUnits txtUnits'+full[0]+' form-control" style="padding-right:2px;" id="txtUnits'+full[0]+'" value="'+data+'" />';
                            }
                        },
                        {"title": "Charges",
                            "render": function ( data, type, full, meta ) {
                              return '<input type="text" class="txtCharges txtCharges'+full[0]+' form-control" style="padding-right:2px;" id="txtCharges'+full[0]+'" value="'+data+'" />';
                            }
                        },
                        {"title": "Claim #"},
                        {"title": "Status"},
                        {"title": "Actions",
                            "render": function ( data, type, full, meta ) {
                              return '<a href="javascript:void(0)" class="edit'+data+' editLedger" title="Update"><span class="ti-check" style="font-size:12px;"></span> </a>&nbsp;|&nbsp;<a href="javascript:void(0)" class="xchange'+data+' xchange"><span class="ti-arrows-horizontal" style="font-size:12px;"></span> </a>';
                            }
                        }
                        // {"title": "Patient Name",
                        //     "render": function ( data, type, full, meta ) {
                        //         var tempData=data.indexOf("-");
                        //         var truncID = data.substr(0,tempData);
                        //         var tempName = data.replace(truncID,"");
                        //         var tempDataName = tempName.replace("- ","");
                        //       return '<a href="#" class="redirectPatient'+truncID+' redPat">'+tempDataName+'</a>';
                        //     }
                        // }
                    ]
                });
              }
          });
        });
    $('.rdoLedger').change(function(){
      $('#divDDL').show();
      val = $(this).val();
      if(val == "patient"){
        document.getElementById("ddlVal").value = "";
        document.getElementById("lblVal").innerHTML = "Patient";
        var patientList = <?php include('patientList.php'); ?>;
              $("#ddlVal").autocomplete({
            source: patientList,
                  autoFocus:true
              });
      }
      else if(val == "insurance")
      {
        document.getElementById("ddlVal").value = "";
        $('#divDDL').show();
        document.getElementById("lblVal").innerHTML = "Insurance";
        var insuranceList = <?php include('insuranceLedgerList.php'); ?>;
              $("#ddlVal").autocomplete({
            source: insuranceList,
                  autoFocus:true
              });
      }
      else{
        $('#divDDL').hide();
        getAllClaimsInfo();
      }
    });
    $('#ddlVal').focusout(function(){
      //$('#loader').show();
      if(val == "patient"){
        var truncID = "";
        $('#tableAll').hide();
        $('#tablePat').show();
        $('#tableIns').hide();
        $('#testAll_wrapper').hide();
        var tot = 0;
        $('#testInsurance_wrapper').hide();
        $('#testPatient_wrapper').show();
        var ddlValue = $(this).val();
        var n=ddlValue.indexOf("-");
        var truncID = ddlValue.substr(0,n);
        var tempNam = ddlValue.replace(truncID,"");
        var tempNamFin = tempNam.replace("- ","");
        $.post("http://careaba.com/CareService/claims/ledgerpatientonly",
          {
              patientID: truncID
          },
          function(data1, status){
        //    $.ajax({
       //                type: "POST",
       //                url:"getZeroToThirtyByPat.php",
       //                async : false,
       //                data:{
       //                  patientID : truncID
       //                },success:function(result){
       //                      document.getElementById('spanPatZero').innerHTML = '$ '+result+'.00';
          //      tot += parseInt(result);
       //                   }
       //            });
          // $.ajax({
       //                type: "POST",
       //                url:"getThirtyToSixtyByPat.php",
       //                async : false,
       //                data:{
       //                  patientID : truncID
       //                },success:function(result){
       //                   document.getElementById('spanPatThirty').innerHTML = '$ '+result+'.00';
          //      tot += parseInt(result);
       //                   }
       //            });
          // $.ajax({
       //                type: "POST",
       //                url:"getSixtyToNinetyByPat.php",
       //                async : false,
       //                data:{
       //                  patientID : truncID
       //                },success:function(result){
       //                      document.getElementById('spanPatSixty').innerHTML = '$ '+result+'.00';
          //      tot += parseInt(result);
       //                   }
       //            });
          // $.ajax({
       //                type: "POST",
       //                url:"getNinetyToOnetwentyByPat.php",
       //                async : false,
       //                data:{
       //                  patientID : truncID
       //                },success:function(result){
       //                      document.getElementById('spanPatNinety').innerHTML = '$ '+result+'.00';
          //      tot += parseInt(result);
       //                   }
       //            });
          // $.ajax({
       //                type: "POST",
       //                url:"get120plusByPat.php",
       //                async : false,
       //                data:{
       //                  patientID : truncID
       //                },success:function(result){
       //                   document.getElementById('spanPat120').innerHTML = '$ '+result+'.00';
          //      tot += parseInt(result);
       //                   }
       //            });
          // document.getElementById('spanPatTotal').innerHTML = '$ '+parseInt(tot);
          var dt = [];
          $.each(data1,function(i,v) {
            if(data1[i].charge == ""){
                            data1[i].charge = 0.00;
                        }
                        else{
                            if(data1[i].charge%1 == 0){
                                //data1[i].charge = data1[i].charge+'.00';
                                data1[i].charge = data1[i].charge;
                            }
                            else{
                                data1[i].charge = data1[i].charge;
                            }
                        }
                        if(data1[i].allowed == ""){
                            //data1[i].allowed = 0+'.00';
                            data1[i].allowed = 0;
                        }
                        else{
                            if(data1[i].allowed%1 == 0){
                                //data1[i].allowed = data1[i].allowed+'.00';
                                data1[i].allowed = data1[i].allowed;
                            }
                            else{
                                data1[i].allowed = data1[i].allowed;
                            }
                        }
                        if(data1[i].claimBalance == ""){
                            if(data1[i].total%1 == 0){
                                //data1[i].claimBalance = data1[i].total - data1[i].copay+'.00';
                                data1[i].claimBalance = data1[i].total - data1[i].copay;
                            }
                            else{
                                data1[i].claimBalance = data1[i].total - data1[i].copay;
                            }
                        }
                        else{
                            if(data1[i].claimBalance%1 == 0){
                                //data1[i].claimBalance = data1[i].claimBalance+'.00';
                                data1[i].claimBalance = data1[i].claimBalance;
                            }
                            else{
                                data1[i].claimBalance = data1[i].claimBalance;
                            }
                        }
                        if(data1[i].paid == ""){
                            //data1[i].paid = 0+'.00';
                            data1[i].paid = 0;
                        }
                        else{
                            if(data1[i].paid%1 == 0){
                                //data1[i].paid = data1[i].paid+'.00';
                                data1[i].paid = data1[i].paid;
                            }
                            else{
                                data1[i].paid = data1[i].paid;
                            }
                        }
                        if(data1[i].adjustment == ""){
                            //data1[i].adjustment = 0+'.00';
                            data1[i].adjustment = 0;
                        }
                        else{
                            if(data1[i].adjustment%1 == 0){
                                //data1[i].adjustment = data1[i].adjustment+'.00';
                                data1[i].adjustment = data1[i].adjustment;
                            }
                            else{
                                data1[i].adjustment = data1[i].adjustment;
                            }
                        }
            $.ajax({
                        type: "POST",
                        url:"getPatientName.php",
                        async : false,
                        data:{
                          claimID : data1[i].claimID
                        },success:function(result){
                          if(result!=""){
                            fullName = result;
                          }
                          else{
                            fullName = "";
                          }
                        }
                    });
            $.ajax({
                              type: "POST",
                              url:"getInsuranceName.php",
                              async : false,
                              data:{
                                insuranceID : data1[i].insuranceID
                              },success:function(result){
                                var res = JSON.parse(result);
                                if(res.length != 0){
                            payorName = res[0].payerName;
                          }
                          else{
                            payorName = "SELF";
                          }
                            }
                        });
            dt.push([data1[i].claimID,data1[i].fromDt,data1[i].proced,data1[i].units,data1[i].toDt,data1[i].charge,data1[i].allowed,data1[i].paid,data1[i].adjustment,data1[i].claimBalance,'SELF',tempNamFin,fullName]);
          });
          $('#testPatient').DataTable({
            "aaSorting": [[ 0, "desc" ]],
                "destroy": true,
                "data" : dt, 
                
                columns: [
                  {"title": "Claim ID","visible" : false},
                    {"title": "DOS",
                    "render": function ( data, type, full, meta ) {
                    return changeDateFormat(data);
                  }
              },
                    {"title": "CPT"},
                    {"title": "UNITS"},
                    {"title": "Bill Date",
                    "render": function ( data, type, full, meta ) {
                    return changeDateFormat(data);
                  }
              },
                    {"title": "Billed Amount",
                    "render": function ( data, type, full, meta ) {
                    return '$'+data;
                  }
              },
                    {"title": "Allowed Amount",
                    "render": function ( data, type, full, meta ) {
                    return '$'+data;
                  }
              },
                    {"title": "Paid",
                    "render": function ( data, type, full, meta ) {
                    return '$'+data;
                  }
              },
                    {"title": "Adjustment",
                    "render": function ( data, type, full, meta ) {
                    return '$'+data;
                  }
              },
                    {"title": "Balance",
                    "render": function ( data, type, full, meta ) {
                    return '$'+data;
                  }
              },
                    {"title": "Billed To"},
                    {"title": "Patient Name",
                  "render": function ( data, type, full, meta ) {
                    var tempData=data.indexOf("-");
                  var truncID = data.substr(0,tempData);
                  var tempName = data.replace(truncID,"");
                  var tempDataName = tempName.replace("- ","");
                    return '<a href="#" class="redirectPatient'+truncID+' redPat">'+tempDataName+'</a>';
                  }
              }
                ]
          });
          //$('#testPatient').DataTable().destroy();
          });
        $('#loader').hide();
      }
      else{
        $('#tableAll').hide();
        $('#tablePat').hide();
        $('#tableIns').show();
        var tot = 0;
        $('#testAll_wrapper').hide();
        $('#testInsurance_wrapper').show();
        $('#testPatient_wrapper').hide();
        var ddlValue = $(this).val();
        var n=ddlValue.indexOf("-");
        var truncID = ddlValue.substr(0,n);
        
        $.post("http://careaba.com/CareService/claims/ledgerinsuranceonly",
          {
              insuranceID: truncID
          },
          function(data1, status){
        //    $.ajax({
       //                type: "POST",
       //                url:"getZeroToThirtyByIns.php",
       //                async : false,
       //                data:{
       //                  insuranceID : truncID
       //                },success:function(result){
       //                      document.getElementById('spanInsZero').innerHTML = '$ '+result+'.00';
          //      tot += parseInt(result);
       //                   }
       //            });
          // $.ajax({
       //                type: "POST",
       //                url:"getThirtyToSixtyByIns.php",
       //                async : false,
       //                data:{
       //                  insuranceID : truncID
       //                },success:function(result){
       //                   document.getElementById('spanInsThirty').innerHTML = '$ '+result+'.00';
          //      tot += parseInt(result);
       //                   }
       //            });
          // $.ajax({
       //                type: "POST",
       //                url:"getSixtyToNinetyByIns.php",
       //                async : false,
       //                data:{
       //                  insuranceID : truncID
       //                },success:function(result){
       //                      document.getElementById('spanInsSixty').innerHTML = '$ '+result+'.00';
          //      tot += parseInt(result);
       //                   }
       //            });
          // $.ajax({
       //                type: "POST",
       //                url:"getNinetyToOnetwentyByIns.php",
       //                async : false,
       //                data:{
       //                  insuranceID : truncID
       //                },success:function(result){
       //                      document.getElementById('spanInsNinety').innerHTML = '$ '+result+'.00';
          //      tot += parseInt(result);
       //                   }
       //            });
          // $.ajax({
       //                type: "POST",
       //                url:"get120plusByIns.php",
       //                async : false,
       //                data:{
       //                  insuranceID : truncID
       //                },success:function(result){
       //                   document.getElementById('spanIns120').innerHTML = '$ '+result+'.00';
          //      tot += parseInt(result);
       //                   }
       //            });
          // document.getElementById('spanInsTotal').innerHTML = '$ '+parseInt(tot)+'.00';
          var dt = [];
          $.each(data1,function(i,v) {
            if(data1[i].charge == ""){
                            data1[i].charge = 0.00;
                        }
                        else{
                            if(data1[i].charge%1 == 0){
                                //data1[i].charge = data1[i].charge+'.00';
                                data1[i].charge = data1[i].charge;
                            }
                            else{
                                data1[i].charge = data1[i].charge;
                            }
                        }
                        if(data1[i].allowed == ""){
                            //data1[i].allowed = 0+'.00';
                            data1[i].allowed = 0;
                        }
                        else{
                            if(data1[i].allowed%1 == 0){
                                //data1[i].allowed = data1[i].allowed+'.00';
                                data1[i].allowed = data1[i].allowed;
                            }
                            else{
                                data1[i].allowed = data1[i].allowed;
                            }
                        }
                        if(data1[i].claimBalance == ""){
                            if(data1[i].total%1 == 0){
                                //data1[i].claimBalance = data1[i].total - data1[i].copay+'.00';
                                data1[i].claimBalance = data1[i].total - data1[i].copay;
                            }
                            else{
                                data1[i].claimBalance = data1[i].total - data1[i].copay;
                            }
                        }
                        else{
                            if(data1[i].claimBalance%1 == 0){
                                //data1[i].claimBalance = data1[i].claimBalance+'.00';
                                data1[i].claimBalance = data1[i].claimBalance;
                            }
                            else{
                                data1[i].claimBalance = data1[i].claimBalance;
                            }
                        }
                        if(data1[i].paid == ""){
                            //data1[i].paid = 0+'.00';
                            data1[i].paid = 0;
                        }
                        else{
                            if(data1[i].paid%1 == 0){
                                //data1[i].paid = data1[i].paid+'.00';
                                data1[i].paid = data1[i].paid;
                            }
                            else{
                                data1[i].paid = data1[i].paid;
                            }
                        }
                        if(data1[i].adjustment == ""){
                            //data1[i].adjustment = 0+'.00';
                            data1[i].adjustment = 0;
                        }
                        else{
                            if(data1[i].adjustment%1 == 0){
                                //data1[i].adjustment = data1[i].adjustment+'.00';
                                data1[i].adjustment = data1[i].adjustment;
                            }
                            else{
                                data1[i].adjustment = data1[i].adjustment;
                            }
                        }
            $.ajax({
                        type: "POST",
                        url:"getPatientName.php",
                        async : false,
                        data:{
                          claimID : data1[i].claimID
                        },success:function(result){
                          if(result!=""){
                            fullName = result;
                          }
                          else{
                            fullName = "";
                          }
                        }
                    });
            $.ajax({
                              type: "POST",
                              url:"getInsuranceName.php",
                              async : false,
                              data:{
                                insuranceID : data1[i].insuranceID
                              },success:function(result){
                                var res = JSON.parse(result);
                                if(res.length != 0){
                            payorName = res[0].payerName;
                          }
                          else{
                            payorName = "SELF";
                          }
                            }
                        });
            dt.push([data1[i].claimID,data1[i].fromDt,data1[i].proced,data1[i].units,data1[i].toDt,data1[i].charge,data1[i].allowed,data1[i].paid,data1[i].adjustment,data1[i].claimBalance,payorName,fullName]);
          });
          $('#testInsurance').DataTable({
            "data" : dt, 
            
            "aaSorting": [[ 0, "desc" ]],
                        "destroy": true,
                columns: [
                  {"title": "Claim ID","visible" : false},
                    {"title": "DOS",
                    "render": function ( data, type, full, meta ) {
                    return changeDateFormat(data);
                  }
              },
                    {"title": "CPT"},
                    {"title": "UNITS"},
                    {"title": "Bill Date",
                    "render": function ( data, type, full, meta ) {
                    return changeDateFormat(data);
                  }
              },
                    {"title": "Billed Amount",
                    "render": function ( data, type, full, meta ) {
                    return '$'+data;
                  }
              },
                    {"title": "Allowed Amount",
                    "render": function ( data, type, full, meta ) {
                    return '$'+data;
                  }
              },
                    {"title": "Paid",
                    "render": function ( data, type, full, meta ) {
                    return '$'+data;
                  }
              },
                    {"title": "Adjustment",
                    "render": function ( data, type, full, meta ) {
                    return '$'+data;
                  }
              },
                    {"title": "Balance",
                    "render": function ( data, type, full, meta ) {
                    return '$'+data;
                  }
              },
                    {"title": "Billed To"},
                    {"title": "Patient Name",
                  "render": function ( data, type, full, meta ) {
                    var tempData=data.indexOf("-");
                  var truncID = data.substr(0,tempData);
                  var tempName = data.replace(truncID,"");
                  var tempDataName = tempName.replace("- ","");
                    return '<a href="#" class="redirectPatient'+truncID+' redPat">'+tempDataName+'</a>';
                  }
              }
                ]
          });
          //$('#testInsurance').DataTable().destroy();
          });
        $('#loader').hide();
      }

    });
      $(document).on('click','.chk',function() {
        if ($("input:checkbox:checked").length > 1) {
          $("#bulkDiv").show();
        }
        else{
          $("#bulkDiv").hide();
        }
      });

      $(document).on('click','.chkall1',function() {
        if($('.chkall1').is(':checked') == true){
          $(".chkall11").prop("checked",true);
          $("#bulkDiv").show();
        }
        else{
          $(".chkall11").prop("checked",false);
          $("#bulkDiv").hide();
        }
      });

      $(document).on('click','.chkall2',function() {
        if($('.chkall2').is(':checked') == true){
          $(".chkall22").prop("checked",true);
          $("#bulkDiv").show();
        }
        else{
          $(".chkall22").prop("checked",false);
          $("#bulkDiv").hide();
        }
      });


      $(document).on('click','.chkall3',function() {
        if($('.chkall3').is(':checked') == true){
          $(".chkall33").prop("checked",true);
          $("#bulkDiv").show();
        }
        else{
          $(".chkall33").prop("checked",false);
          $("#bulkDiv").hide();
        }
      });


      $(document).on('click','.chkall4',function() {
        if($('.chkall4').is(':checked') == true){
          $(".chkall44").prop("checked",true);
          $("#bulkDiv").show();
        }
        else{
          $(".chkall44").prop("checked",false);
          $("#bulkDiv").hide();
        }
      });
  
   });
  
  function Dec2(num) {
    num = String(num);
    if(num.indexOf('.') !== -1) {
      var numarr = num.split(".");
      if (numarr.length == 1) {
        return Number(num);
      }
      else {
        return Number(numarr[0]+"."+numarr[1].charAt(0)+numarr[1].charAt(1));
      }
    }
    else {
      return Number(num);
    }  
  }

  function getAllClaimsInfo(){
    var tot = 0;
    var AllPattot = 0;
    $('#tableAll').show();
    $('#tablePat').hide();
    $('#tableIns').hide();
    // $.get("getZeroToThirtyAllByIns.php",function(data1){
    //  document.getElementById('spanZero').innerHTML = '$ '+data1+'.00';
    //  tot += parseInt(data1);
    // });

    // $.get("getThirtyToSixtyAllByIns.php",function(data1){
    //  document.getElementById('spanThirty').innerHTML = '$ '+data1+'.00';
    //  tot += parseInt(data1);
    // });

    // $.get("getSixtyToNinetyAllByIns.php",function(data1){
    //  document.getElementById('spanSixty').innerHTML = '$ '+data1+'.00';
    //  tot += parseInt(data1);
    // });

    // $.get("getNinetyToOnetwentyAllByIns.php",function(data1){
    //  document.getElementById('spanNinety').innerHTML = '$ '+data1+'.00';
    //  tot += parseInt(data1);
    // });

    // $.get("get120plusAllByIns.php",function(data1){
    //  document.getElementById('span120').innerHTML = '$ '+data1+'.00';
    //  tot += parseInt(data1);
    // });

    // $.get("getZeroToThirtyAllByPat.php",function(data1){
    //  document.getElementById('spanAllPatZero').innerHTML = '$ '+data1+'.00';
    //  AllPattot += parseInt(data1);
    // });

    // $.get("getThirtyToSixtyAllByPat.php",function(data1){
    //  document.getElementById('spanAllPatThirty').innerHTML = '$ '+data1+'.00';
    //  AllPattot += parseInt(data1);
    // });

    // $.get("getSixtyToNinetyAllByPat.php",function(data1){
    //  document.getElementById('spanAllPatSixty').innerHTML = '$ '+data1+'.00';
    //  AllPattot += parseInt(data1);
    // });

    // $.get("getNinetyToOnetwentyAllByPat.php",function(data1){
    //  document.getElementById('spanAllPatNinety').innerHTML = '$ '+data1+'.00';
    //  AllPattot += parseInt(data1);
    // });

    // $.get("get120plusAllByPat.php",function(data1){
    //  document.getElementById('spanAllPat120').innerHTML = '$ '+data1+'.00';
    //  AllPattot += parseInt(data1);
    // });

    
    $.get("getAllClaims.php",function(data1){
      $('#loader').show();
      var data1 = JSON.parse(data1);
      var dt = [];
      $.each(data1,function(i,v) {
        dt.push([data1[i].claimID,data1[i].claimID,data1[i].fullName,data1[i].phyName,data1[i].rendPhy,data1[i].activityName,data1[i].fromDt,data1[i].proced,data1[i].mod1,data1[i].appLocation,data1[i].units,data1[i].total,data1[i].claimNumber,data1[i].claimStatus,data1[i].claimID]);
      });
      $('#test1').DataTable({
        "aaSorting": [[ 0, "desc" ]],
        "destroy": true,
        "data" : dt, 
            columns: [
              {"title": "Claim ID","visible" : false},
              {"title": "<input type='checkbox' class='chkall1'/> ",
                  "render": function ( data, type, full, meta ) {
                    return '<input type="checkbox" class="chkall11 chk chk'+full[0]+' form-control" id="chk'+full[0]+'" />';
                  }
              },
              {"title": "Client Name",
                  "render": function ( data, type, full, meta ) {
                    return data;
                  }
              },
              {"title": "Client Provider"},
              {"title": "Rendering Provider"},
              {"title": "Activity"},
     //          {"title": "Bill Date",
     //           "render": function ( data, type, full, meta ) {
              //       return changeDateFormat(data);
              //     }
              // },
              {"title": "DOS",
                  "render": function ( data, type, full, meta ) {
                    return changeDateFormat(data);
                  }
              },
              {"title": "CPT      ",
                  "render": function ( data, type, full, meta ) {
                    return '<input type="text" class="txtCPT txtCPT'+full[0]+' form-control" style="padding-right:2px;" id="txtCPT'+full[0]+'" value="'+data+'" />';
                  }
              },
              {"title": "Modifier",
                  "render": function ( data, type, full, meta ) {
                    return '<input type="text" class="txtMod txtMod'+full[0]+' form-control" style="padding-right:2px;" id="txtMod'+full[0]+'" value="'+data+'" />';
                  }
              },
              {"title": "POS",
                  "render": function ( data, type, full, meta ) {
                    return '<input type="text" class="txtPOS txtPOS'+full[0]+' form-control" style="padding-right:2px;" id="txtPOS'+full[0]+'" value="'+data+'" />';
                  }
              },
              {"title": "Units",
                  "render": function ( data, type, full, meta ) {
                    return '<input type="text" class="txtUnits txtUnits'+full[0]+' form-control" style="padding-right:2px;" id="txtUnits'+full[0]+'" value="'+data+'" />';
                  }
              },
              {"title": "Charges",
                  "render": function ( data, type, full, meta ) {
                    return '<input type="text" class="txtCharges txtCharges'+full[0]+' form-control" style="padding-right:2px;" id="txtCharges'+full[0]+'" value="'+data+'" />';
                  }
              },
              {"title": "Claim #"},
              {"title": "Status"},
              {"title": "Actions",
                  "render": function ( data, type, full, meta ) {
                    return '<a href="javascript:void(0)" class="edit'+data+' editLedger" title="Update"><span class="ti-check" style="font-size:12px;"></span> </a>&nbsp;|&nbsp;<a href="javascript:void(0)" class="xchange'+data+' xchange"><span class="ti-arrows-horizontal" style="font-size:12px;"></span> </a>';
                  }
              }
            ]
      });
      $('#loader').hide();
      //document.getElementById('spanTotal').innerHTML = '$ '+parseInt(tot)+'.00';
      //document.getElementById('spanAllPatTot').innerHTML = '$ '+AllPattot+'.00';
      $('#testAll').removeAttr('style');
      });


      $.get("getConfirmedClaims.php",function(data1){
      $('#loader').show();
      var data1 = JSON.parse(data1);
      var dt = [];
      $.each(data1,function(i,v) {
        dt.push([data1[i].claimID,data1[i].claimID,data1[i].fullName,data1[i].phyName,data1[i].rendPhy,data1[i].activityName,data1[i].fromDt,data1[i].proced,data1[i].mod1,data1[i].appLocation,data1[i].units,data1[i].total,data1[i].claimNumber,data1[i].claimStatus,data1[i].claimID]);
      });
      $('#test4').DataTable({
        "aaSorting": [[ 0, "desc" ]],
        "destroy": true,
        "data" : dt,
        
            columns: [
              {"title": "Claim ID","visible" : false},
              {"title": "<input type='checkbox' class='chkall2'/>",
                  "render": function ( data, type, full, meta ) {
                    return '<input type="checkbox" class="chkall22 chk chk'+full[0]+' form-control" id="chk'+full[0]+'" />';
                  }
              },
              {"title": "Client Name",
                  "render": function ( data, type, full, meta ) {
                    return data;
                  }
              },
              {"title": "Client Provider"},
              {"title": "Rendering Provider"},
              {"title": "Activity"},
     //          {"title": "Bill Date",
     //           "render": function ( data, type, full, meta ) {
              //       return changeDateFormat(data);
              //     }
              // },
              {"title": "DOS",
                  "render": function ( data, type, full, meta ) {
                    return changeDateFormat(data);
                  }
              },
              {"title": "CPT      ",
                  "render": function ( data, type, full, meta ) {
                    return '<input type="text" class="txtCPT txtCPT'+full[0]+' form-control" style="padding-right:2px;" id="txtCPT'+full[0]+'" value="'+data+'" />';
                  }
              },
              {"title": "Modifier",
                  "render": function ( data, type, full, meta ) {
                    return '<input type="text" class="txtMod txtMod'+full[0]+' form-control" style="padding-right:2px;" id="txtMod'+full[0]+'" value="'+data+'" />';
                  }
              },
              {"title": "POS",
                  "render": function ( data, type, full, meta ) {
                    return '<input type="text" class="txtPOS txtPOS'+full[0]+' form-control" style="padding-right:2px;" id="txtPOS'+full[0]+'" value="'+data+'" />';
                  }
              },
              {"title": "Units",
                  "render": function ( data, type, full, meta ) {
                    return '<input type="text" class="txtUnits txtUnits'+full[0]+' form-control" style="padding-right:2px;" id="txtUnits'+full[0]+'" value="'+data+'" />';
                  }
              },
              {"title": "Charges",
                  "render": function ( data, type, full, meta ) {
                    return '<input type="text" class="txtCharges txtCharges'+full[0]+' form-control" style="padding-right:2px;" id="txtCharges'+full[0]+'" value="'+data+'" />';
                  }
              },
              {"title": "Claim #"},
              {"title": "Status"},
              {"title": "Actions",
                  "render": function ( data, type, full, meta ) {
                    return '<a href="javascript:void(0)" class="edit'+data+' editLedger" title="Update"><span class="ti-check" style="font-size:12px;"></span> </a>&nbsp;|&nbsp;<a href="javascript:void(0)" class="xchange'+data+' xchange"><span class="ti-arrows-horizontal" style="font-size:12px;"></span> </a>';
                  }
              }
            ]
      });
      $('#loader').hide();
      //document.getElementById('spanTotal').innerHTML = '$ '+parseInt(tot)+'.00';
      //document.getElementById('spanAllPatTot').innerHTML = '$ '+AllPattot+'.00';
      $('#testAll').removeAttr('style');
      });
        $.get("getReadyClaims.php",function(data1){
            $('#loader').show();
            var data1 = JSON.parse(data1);
            var dt = [];
            $.each(data1,function(i,v) {
                dt.push([data1[i].claimID,data1[i].claimID,data1[i].fullName,data1[i].phyName,data1[i].rendPhy,data1[i].activityName,data1[i].fromDt,data1[i].proced,data1[i].mod1,data1[i].appLocation,data1[i].units,data1[i].total,data1[i].claimNumber,data1[i].claimStatus,data1[i].claimID]);
            });
            $('#test2').DataTable({
                "aaSorting": [[ 0, "desc" ]],
                "destroy": true,
                "data" : dt, 
                
                columns: [
                        {"title": "Claim ID","visible" : false},
                        {"title": "<input type='checkbox' class='chkall3'/>",
                            "render": function ( data, type, full, meta ) {
                              return '<input type="checkbox" class="chkall33 chk chk'+full[0]+' form-control" id="chk'+full[0]+'" />';
                            }
                        },
                        {"title": "Client Name",
                            "render": function ( data, type, full, meta ) {
                              return data;
                            }
                        },
                        {"title": "Client Provider"},
                        {"title": "Rendering Provider"},
                        {"title": "Activity"},
               //          {"title": "Bill Date",
               //           "render": function ( data, type, full, meta ) {
                        //       return changeDateFormat(data);
                        //     }
                        // },
                        {"title": "DOS",
                            "render": function ( data, type, full, meta ) {
                              return changeDateFormat(data);
                            }
                        },
                        {"title": "CPT"
                        },
                        {"title": "Modifier"
                        },
                        {"title": "POS"
                        },
                        {"title": "Units"
                        },
                        {"title": "Charges"
                        },
                        {"title": "Claim #"},
                        {"title": "Status"},
                        {"title": "Actions",
                            "render": function ( data, type, full, meta ) {
                              return '<a href="javascript:void(0)" class="edit'+data+' editLedger" title="Update"><span class="ti-check" style="font-size:12px;"></span> </a>&nbsp;|&nbsp;<a href="javascript:void(0)" class="xchange'+data+' xchange"><span class="ti-arrows-horizontal" style="font-size:12px;"></span> </a>';
                            }
                        }
                ]
            });
            $('#loader').hide();
            //document.getElementById('spanTotal').innerHTML = '$ '+parseInt(tot)+'.00';
            //document.getElementById('spanAllPatTot').innerHTML = '$ '+AllPattot+'.00';
            $('#testAll').removeAttr('style');
        });
        $.get("getSubmitClaims.php",function(data1){
            $('#loader').show();
            var data1 = JSON.parse(data1);
            var dt = [];
            $.each(data1,function(i,v) {
                dt.push([data1[i].claimID,data1[i].claimID,data1[i].fullName,data1[i].phyName,data1[i].rendPhy,data1[i].activityName,data1[i].fromDt,data1[i].proced,data1[i].mod1,data1[i].appLocation,data1[i].units,data1[i].total,data1[i].claimNumber,data1[i].claimStatus,data1[i].claimID]);
            });
            $('#test3').DataTable({
                "aaSorting": [[ 0, "desc" ]],
                "destroy": true,
                "data" : dt, 
                
                columns: [
                        {"title": "Claim ID","visible" : false},
                        {"title": "<input type='checkbox' class='chkall4'/>",
                            "render": function ( data, type, full, meta ) {
                              return '<input type="checkbox" class="chkall44 chk chk'+full[0]+' form-control" id="chk'+full[0]+'" />';
                            }
                        },
                        {"title": "Client Name",
                            "render": function ( data, type, full, meta ) {
                              return data;
                            }
                        },
                        {"title": "Client Provider"},
                        {"title": "Rendering Provider"},
                        {"title": "Activity"},
               //          {"title": "Bill Date",
               //           "render": function ( data, type, full, meta ) {
                        //       return changeDateFormat(data);
                        //     }
                        // },
                        {"title": "DOS",
                            "render": function ( data, type, full, meta ) {
                              return changeDateFormat(data);
                            }
                        },
                        {"title": "CPT"
                        },
                        {"title": "Modifier"
                        },
                        {"title": "POS"
                        },
                        {"title": "Units"
                        },
                        {"title": "Charges"
                        },
                        {"title": "Claim #"},
                        {"title": "Status"},
                        {"title": "Actions",
                            "render": function ( data, type, full, meta ) {
                              return '<a href="javascript:void(0)" class="edit'+data+' editLedger" title="Update"><span class="ti-check" style="font-size:12px;"></span> </a>';
                            }
                        }
                ]
            });
            $('#loader').hide();
            //document.getElementById('spanTotal').innerHTML = '$ '+parseInt(tot)+'.00';
            //document.getElementById('spanAllPatTot').innerHTML = '$ '+AllPattot+'.00';
            $('#testAll').removeAttr('style');
        });

  }
  function changeDateFormat(inputDate){  // expects Y-m-d
        var splitDate = (inputDate.split(" ")[0]).split('-');
        
        if(splitDate.count == 0){
            return null;
        }

        var year = splitDate[0];
        var month = splitDate[1];
        var day = splitDate[2]; 

        var consolidated = month + '-' + day + '-' + year;
        var fake = year+ '-' + month+ '-' + day;
        var timing = inputDate.replace(fake,"")
        return consolidated+ ' ' + timing ;
    }
    var convertDate = function(usDate) {
        if(usDate !=""){
            var dateParts = usDate.split(/(\d{1,2})-(\d{1,2})-(\d{4})/);
            return dateParts[3] + "-" + dateParts[1] + "-" + dateParts[2];
        }
    }
  </script>