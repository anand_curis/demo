<?php
 session_start();

 if (!isset($_SESSION['userID'])) {
 	header("Location: login.php");
 }
 if($_SESSION['uName'] == "chaks"){
    header("Location: index.html");
}
 include("header.html");
 ?>


<div class="row">
	<h2 style="color: #251367; margin-left:20px;">EDI List</h2>
	<div class="col-md-12" style="margin-top:20px;">
        <div class="form-group col-md-12" style="margin-bottom:50px;">
            <input type="button" class="btn btn-primary" value="Submit EDI" id="btnGenerate"/>
        </div>
        <div class="widget box" >
            <div class="widget-content">
            	<form action="EditPatient.php" method="post">
                <table id="test" class="display table-bordered" cellspacing="0" width="100%">
				</table>
				</form>
            </div>
        </div>
    </div>
</div>

<?php

 include("footer.html");
 ?>

	<link href="https://cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css" rel='stylesheet' type='text/css'>
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
  <link rel="stylesheet" href="vendors/pnotify/css/pnotify.css">
  <link rel="stylesheet" href="vendors/animate/animate.min.css"/>
  <link href="vendors/pnotify/css/pnotify.brighttheme.css" rel="stylesheet" type="text/css"/>
    <link href="vendors/pnotify/css/pnotify.buttons.css" rel="stylesheet" type="text/css"/>
    <link href="vendors/pnotify/css/pnotify.mobile.css" rel="stylesheet" type="text/css"/>
    <link href="vendors/pnotify/css/pnotify.history.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="css/custom_css/toastr_notificatons.css">
    <script type="text/javascript" src="vendors/pnotify/js/pnotify.js"></script>
<script type="text/javascript" src="vendors/pnotify/js/pnotify.animate.js"></script>
<script type="text/javascript" src="vendors/pnotify/js/pnotify.buttons.js"></script>
<script type="text/javascript" src="vendors/pnotify/js/pnotify.confirm.js"></script>
<script type="text/javascript" src="vendors/pnotify/js/pnotify.nonblock.js"></script>
<script type="text/javascript" src="vendors/pnotify/js/pnotify.mobile.js"></script>
<script type="text/javascript" src="vendors/pnotify/js/pnotify.desktop.js"></script>
<script type="text/javascript" src="vendors/pnotify/js/pnotify.history.js"></script>
<script type="text/javascript" src="vendors/pnotify/js/pnotify.callbacks.js"></script>
	<script>
	var data1 = <?php 
	$con = mysql_connect("localhost:3306","curis_user","Curis@123");

	if(!$con){
		die("Error : ".mysql_error());
	}

	mysql_select_db("curismed_aba",$con);

	$result = mysql_query("select * from m_edi GROUP BY ediInvoiceNo ORDER BY ediInvoiceNo DESC");
	
	$edi = array();

	while($row = mysql_fetch_array($result)){
		$row_array['ediID'] = $row['ediID'];
		$row_array['ediInvoiceNo'] = $row['ediInvoiceNo'];
		$row_array['ediLocation'] = $row['ediLocation'];
		$row_array['ediCreatedOn'] = $row['ediCreatedOn'];
	        array_push($edi,$row_array);
	}
	echo json_encode($edi);
	
	?>;

	$(document).ready(function(){
		document.getElementById("practiceName").innerHTML = sessionStorage.getItem("practiceName");
        $(".navigation li").parent().find('li').removeClass("active");

         var inv = "";
    $.ajax({
          type: "GET",
          url:"getLastEDI.php",
          async : false,
          data:{
          },success:function(result){
            inv = parseInt(result)+1;
            inv = pad(inv, 5);
         }
      });
    $('#btnGenerate').click(function(){
      $.ajax({
          type: "POST",
          url:"getClaimNoList.php",
          data:{
            practiceID : "1"
          },success:function(result){
            var n = result.length;
            //result = result.substr(0,n-1);
            var res = JSON.parse(result);
            if(res.length != 0){
              for(var i=0; i< res.length;i++){
                var chNo = res[i].chartNumber;
                var ClNo = res[i].ClaimNumber;
                $.ajax({
                  type: "POST",
                  url:"EDI_new.php",
                  async:false,
                  data:{
                    ClaimNumber : ClNo,
                    chartNumber : chNo
                  },success:function(result){
                      var percent = 0;
                      var notice = new PNotify({
                        text: "Please Wait",
                        type: 'info',
                        icon: 'fa fa-spinner fa-spin',
                        hide: false,
                        buttons: {
                          closer: false,
                          sticker: false
                        },
                        opacity: .75,
                        shadow: false,
                        width: "170px"
                      });

                      setTimeout(function () {
                        notice.update({
                          title: false
                        });
                        var interval = setInterval(function () {
                          percent += 2;
                          var options = {
                            text: percent + "% complete."
                          };
                          if (percent == 80) options.title = "Almost There";
                          if (percent >= 100) {
                            window.clearInterval(interval);
                            options.title = "Done!";
                            options.type = "success";
                            options.hide = true;
                            options.delay = 3000;
                            options.buttons = {
                              closer: true,
                              sticker: true
                            };
                            options.icon = 'fa fa-check';
                            options.opacity = 1;
                            options.shadow = true;
                            options.width = PNotify.prototype.options.width;
                          }
                          notice.update(options);
                        }, 120);
                      }, 1500);
                  }
                });
              }
              $.ajax({
                  type: "POST",
                  url:"unsetEDI.php",
                  data:{
                  },success:function(result){
                      window.location.reload();
                  }
                });
              $.ajax({
                  type: "POST",
                  url:"test.php",
                  data:{
                  },success:function(result){
                    window.location.reload();
                  }
                });
            }
            else{
              new PNotify({
                  title: 'Oh No!',
                  text: 'No Claims are to be generated.',
                  type: 'error'
              });
            }
         }
      });
    });

      $("#navv3").addClass("active");
		var dt = [];
		$.each(data1,function(i,v) {
			dt.push([data1[i].ediID,data1[i].ediInvoiceNo,'/'+data1[i].ediLocation,data1[i].ediCreatedOn,'']);
		});
		$('#test').DataTable({
        "data": dt,
        columns: [
        	{"title": "ID",visible:false},
            {"title": "Invoice No"},
            {"title": "Location"},
            {
            	"title":"Created On",
            	"mdata": "createdOn",
            	mRender: function (data, type, row) { 
            		return changeDateFormat1(data); 
            	}
        	},
            {
            	"title":"Actions",
            	"mdata": "Actions",
            	mRender: function (data, type, row) { return '<a href="EDI/outbound/'+row[1]+'.txt" download><span class="ti-download"></span></a>&nbsp;|&nbsp;<a href="EDI/outbound/'+row[1]+'.txt" target="_blank">View</a>&nbsp;|&nbsp;<a href="'+row[1]+'.pdf" target="_blank">HCFA 1500</a>'; }
              //mRender: function (data, type, row) { return '<a href="EDI/'+row[1]+'.txt" download><span class="ti-download"></span></a>&nbsp;|&nbsp;<a href="EDI/'+row[1]+'.txt" target="_blank">View</a>&nbsp;|&nbsp;<a href="'+row[1]+'.pdf" target="_blank">HCFA 1500</a>'; }
        	},
        ]
        });

	});
	function changeDateFormat1(inputDate){  // expects Y-m-d
        var splitDate = inputDate.split('-');
        if(splitDate.count == 0){
            return null;
        }

        var year = splitDate[0];
        var month = splitDate[1];
        var day = splitDate[2].slice(0,2); 
        var n = splitDate[2].length;
        var time = splitDate[2].slice(3,n);

        return month + '-' + day + '-' + year+ ' '+time;
    }
    function pad(number, length) {
   
    var str = '' + number;
    while (str.length < length) {
        str = '0' + str;
    }
   
    return str;

}
    function changeDateFormat(inputDate){  // expects Y-m-d
        var splitDate = inputDate.split('-');
        if(splitDate.count == 0){
            return null;
        }

        var year = splitDate[0];
        var month = splitDate[1];
        var day = splitDate[2]; 

        return month + '-' + day + '-' + year;
    }
	</script>
</body>
</html>