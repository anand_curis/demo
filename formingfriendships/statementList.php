<?php
 session_start();

 if (!isset($_SESSION['userID'])) {
    header("Location: index.html");
 }
 if($_SESSION['uName'] == "chaks"){
    header("Location: index.html");
}
 include('header.html');
 ?>
 
<div class="row">
	<h2 style="color: #251367; margin-left:20px;">Statement List</h2>
	<div class="col-md-12" style="margin-top:20px;">
    <div class="widget box" >
        <div class="widget-content">
        	<form action="EditPatient.php" method="post">
            <table id="test" class="display" cellspacing="0" width="100%">
			</table>
			</form>
        </div>
    </div>

</div>

	<link href="https://cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css" rel='stylesheet' type='text/css'>
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
	<script>
	var data1 = <?php 
	$con = mysql_connect("localhost:3306","curis_user","Curis@123");

    if(!$con){
      die("Error : ".mysql_error());
    }
    mysql_select_db("curismed_aba",$con);

	$result = mysql_query("select * from m_statements");
	
	$edi = array();

	while($row = mysql_fetch_array($result)){
		$row_array['statementID'] = $row['statementID'];
		$row_array['statementName'] = $row['statementName'];
		$row_array['location'] = $row['location'];
		$row_array['createdOn'] = $row['createdOn'];
	        array_push($edi,$row_array);
	}
	echo json_encode($edi);
	
	?>;

	$(document).ready(function(){
		document.getElementById("practiceName").innerHTML = sessionStorage.getItem("practiceName");
		var dt = [];
		$.each(data1,function(i,v) {
			dt.push([data1[i].statementID,data1[i].statementName,data1[i].location+".pdf",data1[i].createdOn]);
		});
		$('#test').DataTable({
        "data": dt,
        columns: [
        	{"title": "ID",visible:false},
            {"title": "Statement Name"},
            {"title": "Location"},
            {
            	"title":"Created On",
            	"mdata": "createdOn",
            	mRender: function (data, type, row) { 
            		return changeDateFormat(data); 
            	}
        	},
            {
            	"title":"Actions",
            	"mdata": "Actions",
            	mRender: function (data, type, row) { return '<a href="Documents/Statements/'+row[1]+'.pdf" download><i class="icon icon-download-alt"></i></a>&nbsp;|&nbsp;<a href="Documents/Statements/'+row[1]+'.pdf" target="_blank">View</a>'; }
        	},
        ]
        });

	});
	function changeDateFormat(inputDate){  // expects Y-m-d
        var splitDate = inputDate.split('-');
        if(splitDate.count == 0){
            return null;
        }

        var year = splitDate[0];
        var month = splitDate[1];
        var day = splitDate[2].slice(0,2); 
        var n = splitDate[2].length;
        var time = splitDate[2].slice(3,n);

        return month + '-' + day + '-' + year+ ' '+time;
    }
	</script>
<?php
    include('footer.html');
 ?>