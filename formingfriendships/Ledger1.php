<?php
 session_start();

 if (!isset($_SESSION['userID'])) {
  header("Location: index.html");
 }
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0" />
  <title>Ledger | AMROMED LLC</title>

  <!--=== CSS ===-->

  <!-- Bootstrap -->
  <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

  <!-- jQuery UI -->
  <!--<link href="plugins/jquery-ui/jquery-ui-1.10.2.custom.css" rel="stylesheet" type="text/css" />-->
  <!--[if lt IE 9]>
    <link rel="stylesheet" type="text/css" href="plugins/jquery-ui/jquery.ui.1.10.2.ie.css"/>
  <![endif]-->

  <!-- Theme -->
  <link href="assets/css/main.css" rel="stylesheet" type="text/css" />
  <link href="assets/css/plugins.css" rel="stylesheet" type="text/css" />
  <link href="assets/css/responsive.css" rel="stylesheet" type="text/css" />
  <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />

  <link rel="stylesheet" href="assets/css/fontawesome/font-awesome.min.css">
  <!--[if IE 7]>
    <link rel="stylesheet" href="assets/css/fontawesome/font-awesome-ie7.min.css">
  <![endif]-->

  <!--[if IE 8]>
    <link href="assets/css/ie8.css" rel="stylesheet" type="text/css" />
  <![endif]-->
  <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>

  <!--=== JavaScript ===-->

  <script type="text/javascript" src="assets/js/libs/jquery-1.10.2.min.js"></script>
  <script type="text/javascript" src="plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>

  <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="assets/js/libs/lodash.compat.min.js"></script>

  <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
  <!--[if lt IE 9]>
    <script src="assets/js/libs/html5shiv.js"></script>
  <![endif]-->

  <!-- Smartphone Touch Events -->
  <script type="text/javascript" src="plugins/touchpunch/jquery.ui.touch-punch.min.js"></script>
  <script type="text/javascript" src="plugins/event.swipe/jquery.event.move.js"></script>
  <script type="text/javascript" src="plugins/event.swipe/jquery.event.swipe.js"></script>

  <!-- General -->
  <script type="text/javascript" src="assets/js/libs/breakpoints.js"></script>
  <script type="text/javascript" src="plugins/respond/respond.min.js"></script> <!-- Polyfill for min/max-width CSS3 Media Queries (only for IE8) -->
  <script type="text/javascript" src="plugins/cookie/jquery.cookie.min.js"></script>
  <script type="text/javascript" src="plugins/slimscroll/jquery.slimscroll.min.js"></script>
  <script type="text/javascript" src="plugins/slimscroll/jquery.slimscroll.horizontal.min.js"></script>

  <!-- Page specific plugins -->
  <!-- Charts -->
  <script type="text/javascript" src="plugins/sparkline/jquery.sparkline.min.js"></script>

  <script type="text/javascript" src="plugins/daterangepicker/moment.min.js"></script>
  <script type="text/javascript" src="plugins/daterangepicker/daterangepicker.js"></script>
  <script type="text/javascript" src="plugins/blockui/jquery.blockUI.min.js"></script>

  <!-- Pickers -->
  <script type="text/javascript" src="plugins/pickadate/picker.js"></script>
  <script type="text/javascript" src="plugins/pickadate/picker.date.js"></script>
  <script type="text/javascript" src="plugins/pickadate/picker.time.js"></script>
  <script type="text/javascript" src="plugins/bootstrap-colorpicker/bootstrap-colorpicker.min.js"></script>

  <!-- Noty -->
  <script type="text/javascript" src="plugins/noty/jquery.noty.js"></script>
  <script type="text/javascript" src="plugins/noty/layouts/top.js"></script>
  <script type="text/javascript" src="plugins/noty/themes/default.js"></script>

  <!-- Slim Progress Bars -->
  <script type="text/javascript" src="plugins/nprogress/nprogress.js"></script>

  <!-- Bootbox -->
  <script type="text/javascript" src="plugins/bootbox/bootbox.min.js"></script>

  <!-- App -->
  <script type="text/javascript" src="assets/js/app.js"></script>
  <script type="text/javascript" src="assets/js/plugins.js"></script>
  <script type="text/javascript" src="assets/js/plugins.form-components.js"></script>
  <script src="//cdn.jsdelivr.net/jquery.ui-contextmenu/1.7.0/jquery.ui-contextmenu.min.js"></script>

  <script>
  $(document).ready(function(){
    "use strict";

    App.init(); // Init layout and core plugins
    Plugins.init(); // Init all plugins
    FormComponents.init(); // Init all form-specific plugins
    document.getElementById('practiceName').innerHTML = sessionStorage.getItem('practiceName');
    $("#bulkDiv").hide();
        var patientList = <?php include('getPat1.php') ?>;
        var insurances = <?php include('insuranceList1.php') ?>;
        var PrimaryPhy = <?php include('primaryPhysician.php') ?>;
        
        $("#insName").autocomplete({
            source: insurances,
            autoFocus:true
        });
        $('#fromDate').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $("#providerName").autocomplete({
            source: PrimaryPhy,
            autoFocus:true
        });
        $("#seqProvider").autocomplete({
            source: PrimaryPhy,
            autoFocus:true
        });
        $("#clientName").autocomplete({
            source: patientList,
            autoFocus:true
        });
        $('#toDate').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#dos').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });

        $("#btnFin").click(function(){
          var searchIDs = $('input:checked').map(function(){
            return $(this).attr('id');
          });
          for(var i=0;i<searchIDs.length;i++){
            var temp = searchIDs[i];
            var newTemp = temp.replace('chk','');
            document.getElementById('_hdnClaimID').value = newTemp;
            var tempCPT = document.getElementById("seqCPT").value;
            var temp_Mod = document.getElementById("seqMOD").value;
            var tempMod = temp_Mod.split(" ");
            var tempMod1 = tempMod[0];
            var tempMod2 = tempMod[1];
            var tempMod3 = tempMod[2];
            var tempMod4 = tempMod[3];
            var tempProvider = document.getElementById("seqProvider").value;
            var tempStatus = document.getElementById("seqStatus").value;
            var n1=tempProvider.indexOf("-");
            tempProvider = tempProvider.substr(0,n1-1);
            $.ajax({
                type: "POST",
                url:"bulkUpdateLedger.php",
                asyn:false,
                data:{
                  claimID : newTemp,
                  tempCPT : tempCPT,
                  tempMod1 : tempMod1,
                  tempMod2 : tempMod2,
                  tempMod3 : tempMod3,
                  tempMod4 : tempMod4,
                  tempProvider : tempProvider,
                  tempStatus : tempStatus
                },success:function(result){
                  console.log(result);
                }
            });
          }
        });

        $(document).on('click','.chk',function() {
          if ($("input:checkbox:checked").length > 1) {
            $("#bulkDiv").show();
          }
          else{
            $("#bulkDiv").hide();
          }
        });


        $(document).on('click','.xchange',function() {
          var temp = $(this).attr('class').split(' ')[0];
          var newTemp = temp.replace('xchange','');
          document.getElementById('_hdnClaimID').value = newTemp;
          $("#swapModal").modal("show");
        });
        $("#btnSwap").click(function(){
            var Status = document.getElementById("moveTo").value;
            var newTemp = document.getElementById('_hdnClaimID').value;
              $.ajax({
                  type: "POST",
                  url:"updateStatus.php",
                  data:{
                    claimID : newTemp,
                    status : Status
                  },success:function(result){
                    $("#swapModal").modal("hide");
                    alertify.success("Successfully Updated");
                    window.location.reload();
                  }
              });
        })
        $(document).on('click','.editLedger',function() {
          var temp = $(this).attr('class').split(' ')[0];
          var newTemp = temp.replace('edit','');
          document.getElementById('_hdnClaimID').value = newTemp;
          var txtCPT = 'txtCPT'+newTemp;
          var txtMod = 'txtMod'+newTemp;
          var txtPOS = 'txtPOS'+newTemp;
          var txtUnits = 'txtUnits'+newTemp;
          var txtCharges = 'txtCharges'+newTemp;
          var tempCPT = document.getElementById(txtCPT).value;
          var temp_Mod = document.getElementById(txtMod).value;
          var tempMod = temp_Mod.split(" ");
          var tempMod1 = tempMod[0];
          var tempMod2 = tempMod[1];
          var tempMod3 = tempMod[2];
          var tempMod4 = tempMod[3];
          var tempPOS = document.getElementById(txtPOS).value;
          var tempUnits = document.getElementById(txtUnits).value;
          var tempCharges = document.getElementById(txtCharges).value;
          $.ajax({
              type: "POST",
              url:"updateLedger.php",
              data:{
                claimID : newTemp,
                tempCPT : tempCPT,
                tempMod1 : tempMod1,
                tempMod2 : tempMod2,
                tempMod3 : tempMod3,
                tempMod4 : tempMod4,
                tempPOS : tempPOS,
                tempUnits : tempUnits,
                tempCharges : tempCharges
              },success:function(result){

              }
          });
        });
        $("#btnGo").click(function(){
            var clientName = $("#clientName").val();
            var nos=clientName.indexOf("- ");
            clientName = clientName.substr(0,nos-1);
            var insName = $("#insName").val();
            var providerNa = $("#providerName").val();
            var n=providerNa.indexOf("-");
            var providerName = providerNa.substr(0,n-1);
            var n1=insName.indexOf("-");
            insName = insName.substr(0,n1-1);
            var dos = convertDate($("#dos").val());
            var cpt = $("#cptVal").val();
            var fromDate = convertDate($("#fromDate").val());
            var toDate = convertDate($("#toDate").val());
            $.ajax({
              type: "POST",
              url:"getLedgerInfo.php",
              data:{
                clientName : clientName,
                providerName : providerName,
                insName : insName,
                cpt : cpt,
                dos : dos,
                fromDate : fromDate,
                toDate : toDate,
              },success:function(result){
                var data1 = JSON.parse(result);
                var dt = [];
                $.each(data1,function(i,v) {
                    dt.push([data1[i].claimID,data1[i].claimID,data1[i].fullName,data1[i].phyName,data1[i].activityName,data1[i].fromDt,data1[i].proced,data1[i].mod1,data1[i].appLocation,data1[i].units,data1[i].total,data1[i].claimNumber,data1[i].claimStatus,data1[i].claimID]);
                });
                $('#test1').DataTable({
                    "aaSorting": [[ 0, "desc" ]],
                    "destroy": true,
                    "data": dt,
                    columns: [
                        {"title": "Claim ID","visible" : false},
                        {"title": "",
                            "render": function ( data, type, full, meta ) {
                              return '<input type="checkbox" class="chk chk'+full[0]+' form-control" id="chk'+full[0]+'" />';
                            }
                        },
                        {"title": "Client Name",
                            "render": function ( data, type, full, meta ) {
                              return data;
                            }
                        },
                        {"title": "Client Provider"},
                        {"title": "Activity"},
               //          {"title": "Bill Date",
               //           "render": function ( data, type, full, meta ) {
                        //       return changeDateFormat(data);
                        //     }
                        // },
                        {"title": "DOS",
                            "render": function ( data, type, full, meta ) {
                              return changeDateFormat(data);
                            }
                        },
                        {"title": "CPT",
                            "render": function ( data, type, full, meta ) {
                              return '<input type="text" class="txtCPT txtCPT'+full[0]+' form-control" id="txtCPT'+full[0]+'" value="'+data+'" />';
                            }
                        },
                        {"title": "Modifier",
                            "render": function ( data, type, full, meta ) {
                              return '<input type="text" class="txtMod txtMod'+full[0]+' form-control" id="txtMod'+full[0]+'" value="'+data+'" />';
                            }
                        },
                        {"title": "POS",
                            "render": function ( data, type, full, meta ) {
                              return '<input type="text" class="txtPOS txtPOS'+full[0]+' form-control" id="txtPOS'+full[0]+'" value="'+data+'" />';
                            }
                        },
                        {"title": "Units",
                            "render": function ( data, type, full, meta ) {
                              return '<input type="text" class="txtUnits txtUnits'+full[0]+' form-control" id="txtUnits'+full[0]+'" value="'+data+'" />';
                            }
                        },
                        {"title": "Charges",
                            "render": function ( data, type, full, meta ) {
                              return '<input type="text" class="txtCharges txtCharges'+full[0]+' form-control" id="txtCharges'+full[0]+'" value="'+data+'" />';
                            }
                        },
                        {"title": "Claim #"},
                        {"title": "Status"},
                        {"title": "Actions",
                            "render": function ( data, type, full, meta ) {
                              return '<a href="javascript:void(0)" class="edit'+data+' editLedger" title="Update"><i class="icon icon-ok" style="font-size:12px;"></i> </a>&nbsp;&nbsp; |&nbsp;&nbsp;<a href="javascript:void(0)" class="xchange'+data+' xchange"><i class="icon icon-exchange" style="font-size:12px;"></i> </a>';
                            }
                        }
                        // {"title": "Patient Name",
                        //     "render": function ( data, type, full, meta ) {
                        //         var tempData=data.indexOf("-");
                        //         var truncID = data.substr(0,tempData);
                        //         var tempName = data.replace(truncID,"");
                        //         var tempDataName = tempName.replace("- ","");
                        //       return '<a href="#" class="redirectPatient'+truncID+' redPat">'+tempDataName+'</a>';
                        //     }
                        // }
                    ]
                });
              }
          });
        });
  });
    var convertDate = function(usDate) {
        if(usDate !=""){
            var dateParts = usDate.split(/(\d{1,2})-(\d{1,2})-(\d{4})/);
            return dateParts[3] + "-" + dateParts[1] + "-" + dateParts[2];
        }
    }
  </script>

  <!-- Demo JS -->
  <script type="text/javascript" src="assets/js/custom.js"></script>
  <script type="text/javascript" src="assets/js/demo/ui_general.js"></script>
</head>

  <!-- Demo JS -->
  <script type="text/javascript" src="assets/js/custom.js"></script>
  <script type="text/javascript" src="assets/js/demo/ui_general.js"></script>
</head>

<body oncontextmenu="return false;">

  <!-- Header -->
    <header class="header navbar navbar-fixed-top" role="banner" style="background-image: url('assets/bg.jpg'); background-repeat: repeat-x;">
        <!-- Top Navigation Bar -->
        <div class="container">

            <!-- Only visible on smartphones, menu toggle -->
            <ul class="nav navbar-nav">
                <li class="nav-toggle"><a href="javascript:void(0);" title=""><i class="icon-reorder"></i></a></li>
            </ul>

            <!-- Logo -->
            <a class="navbar-brand" style="text-align:center;padding-right:50px;" href="javascript:void(0);">
                <img src="assets/logo2.png"/>
                <strong>medABA</strong>
            </a>
            <!-- /logo -->

            <!-- Sidebar Toggler -->
            <a href="#" class="toggle-sidebar bs-tooltip" data-placement="bottom" data-original-title="Toggle navigation">
                <i class="icon-reorder"></i>
            </a>
            <!-- /Sidebar Toggler -->

            <!-- Top Left Menu -->
            <ul class="nav navbar-nav navbar-left hidden-xs hidden-sm" style="list-style:none;">
                <li>
                    <a href="ViewPatient.php" class="dropdown-toggle">
                        Clients
                    </a>
                </li>
                <li>
                    <a href="scheduler.php" class="dropdown-toggle">
                        Scheduler
                    </a>
                </li>
                <li>
                    <a href="Deposit.php" class="dropdown-toggle">
                        Deposit
                    </a>
                </li>
                <li>
                    <a href="Ledger.php" class="dropdown-toggle">
                        Ledger
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                        EDI
                        <i class="icon-caret-down small"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="generateAllEDI.php">
                            <i class="icon-angle-right"></i>
                            Generate EDI
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="EDIList.php">
                            <i class="icon-angle-right"></i>
                            EDI List
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="charts.html" class="dropdown-toggle" data-toggle="dropdown">
                        Settings
                        <i class="icon-caret-down small"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="ViewPractice.php">
                            <i class="icon-angle-right"></i>
                            Practices
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="ViewPhysician.php">
                            <i class="icon-angle-right"></i>
                            Physicians
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="ViewServiceLoc.php">
                            <i class="icon-angle-right"></i>
                            Locations &amp; Facilities
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="javascript:void(0);" style="font-weight:600;">
                            Codes
                            </a>
                            <li>
                                <a href="ViewCPT.php">
                                <i class="icon-angle-right"></i>
                                CPT/Procedure
                                </a>
                            </li>
                            <li>
                                <a href="ViewDX.php">
                                <i class="icon-angle-right"></i>
                                ICD 10/ICD 9 Library
                                </a>
                            </li>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="insurances.php">
                            <i class="icon-angle-right"></i>
                            Insurances
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- /Top Left Menu -->

            <!-- Top Right Menu -->
            <ul class="nav navbar-nav navbar-right">
                <!-- Notifications -->
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-warning-sign"></i>
                        <span class="badge">5</span>
                    </a>
                    <ul class="dropdown-menu extended notification">
                        <li class="title">
                            <p>You have 5 new notifications</p>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="label label-success"><i class="icon-plus"></i></span>
                                <span class="message">New user registration.</span>
                                <span class="time">1 mins</span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="label label-danger"><i class="icon-warning-sign"></i></span>
                                <span class="message">High CPU load on cluster #2.</span>
                                <span class="time">5 mins</span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="label label-success"><i class="icon-plus"></i></span>
                                <span class="message">New user registration.</span>
                                <span class="time">10 mins</span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="label label-info"><i class="icon-bullhorn"></i></span>
                                <span class="message">New items are in queue.</span>
                                <span class="time">25 mins</span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="label label-warning"><i class="icon-bolt"></i></span>
                                <span class="message">Disk space to 85% full.</span>
                                <span class="time">55 mins</span>
                            </a>
                        </li>
                        <li class="footer">
                            <a href="javascript:void(0);">View all notifications</a>
                        </li>
                    </ul>
                </li>

                <!-- Tasks -->
                <li class="dropdown hidden-xs hidden-sm">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-tasks"></i>
                        <span class="badge">7</span>
                    </a>
                    <ul class="dropdown-menu extended notification">
                        <li class="title">
                            <p>You have 7 pending tasks</p>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="task">
                                    <span class="desc">Preparing new release</span>
                                    <span class="percent">30%</span>
                                </span>
                                <div class="progress progress-small">
                                    <div style="width: 30%;" class="progress-bar progress-bar-info"></div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="task">
                                    <span class="desc">Change management</span>
                                    <span class="percent">80%</span>
                                </span>
                                <div class="progress progress-small progress-striped active">
                                    <div style="width: 80%;" class="progress-bar progress-bar-danger"></div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="task">
                                    <span class="desc">Mobile development</span>
                                    <span class="percent">60%</span>
                                </span>
                                <div class="progress progress-small">
                                    <div style="width: 60%;" class="progress-bar progress-bar-success"></div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="task">
                                    <span class="desc">Database migration</span>
                                    <span class="percent">20%</span>
                                </span>
                                <div class="progress progress-small">
                                    <div style="width: 20%;" class="progress-bar progress-bar-warning"></div>
                                </div>
                            </a>
                        </li>
                        <li class="footer">
                            <a href="javascript:void(0);">View all tasks</a>
                        </li>
                    </ul>
                </li>

                <!-- Messages -->
                <li class="dropdown hidden-xs hidden-sm">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-envelope"></i>
                        <span class="badge">1</span>
                    </a>
                    <ul class="dropdown-menu extended notification">
                        <li class="title">
                            <p>You have 3 new messages</p>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="photo"><img src="assets/img/demo/avatar-1.jpg" alt="" /></span>
                                <span class="subject">
                                    <span class="from">Bob Carter</span>
                                    <span class="time">Just Now</span>
                                </span>
                                <span class="text">
                                    Consetetur sadipscing elitr...
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="photo"><img src="assets/img/demo/avatar-2.jpg" alt="" /></span>
                                <span class="subject">
                                    <span class="from">Jane Doe</span>
                                    <span class="time">45 mins</span>
                                </span>
                                <span class="text">
                                    Sed diam nonumy...
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="photo"><img src="assets/img/demo/avatar-3.jpg" alt="" /></span>
                                <span class="subject">
                                    <span class="from">Patrick Nilson</span>
                                    <span class="time">6 hours</span>
                                </span>
                                <span class="text">
                                    No sea takimata sanctus...
                                </span>
                            </a>
                        </li>
                        <li class="footer">
                            <a href="javascript:void(0);">View all messages</a>
                        </li>
                    </ul>
                </li>

                <!-- .row .row-bg Toggler -->
                <li>
                    <a href="#" class="dropdown-toggle row-bg-toggle">
                        <i class="icon-resize-vertical"></i>
                    </a>
                </li>


                <!-- User Login Dropdown -->
                <li class="dropdown user">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <!--<img alt="" src="assets/img/avatar1_small.jpg" />-->
                        <i class="icon-male"></i>
                        <span class="username" id="practiceName"></span>
                        <i class="icon-caret-down small"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="javascript:void(0);"><i class="icon-user"></i> My Profile</a></li>
                        <li><a href="javascript:void(0);"><i class="icon-calendar"></i> My Calendar</a></li>
                        <li><a href="javascript:void(0);"><i class="icon-tasks"></i> My Tasks</a></li>
                        <li class="divider"></li>
                        <li><a href="login.php"><i class="icon-key"></i> Log Out</a></li>
                    </ul>
                </li>
                <!-- /user login dropdown -->
            </ul>
            <!-- /Top Right Menu -->
        </div>
        <!-- /top navigation bar -->

    </header> <!-- /.header -->


	<div id="container">

		<div id="content">
			<div class="container">
				<!-- Breadcrumbs line -->
				<div class="crumbs">
					<ul id="breadcrumbs" class="breadcrumb">
						<li class="current">
							<i class="icon-home"></i>
							<a href="index.html">Dashboard</a>
						</li>
						
					</ul>
					<a href="javascript:void(0);"><img src="assets/icons/Settings.png" title="Settings" style="float:right; padding-top:0.3em; padding-right:2em;"/></a>
					<a href="Ledger.php"><img src="assets/icons/Claim_search.png" title="Claim Search" style="float:right; padding-top:0.3em; padding-right:2em;"/></a>
					<a href="AddClaim.php"><img src="assets/icons/Claim_add.png" title="Add Claim" style="float:right; padding-top:0.3em; padding-right:2em;"/></a>
					<a href="ViewPatient.php"><img src="assets/icons/Patient_search.png" title="Patient Search" style="float:right; padding-top:0.3em; padding-right:2em;"/></a>
					<a href="AddPatient.php"><img src="assets/icons/Patient_add.png" title="Add Patient" style="float:right; padding-top:0.3em; padding-right:2em;"/></a>
					<a href="dashboard.html"><img src="assets/icons/Home.png" title="Home" style="float:right; padding-top:0.3em; padding-right:2em;"/></a>

				</div>
				<!-- /Breadcrumbs line -->


				<!--=== Page Content ===-->
				<!--=== Modals ===-->
				<div class="row">
					<!-- <img src="assets/beat.gif" id="loader" style="position:absolute; left:50%; z-index:99999; top:35%;" /> -->
					<h2 style="color: #251367; margin-left:20px;">Ledger</h2>
					<div class="col-md-12" style="margin-top:20px;">
			        <div class="widget box" >
			            <div class="widget-content" style="min-height:100px">
                    <div id="filter" class="col-md-12">
                        <input type="hidden" id="_hdnClaimID"/>
                        <div class="form-group col-md-4" >
                            <label class="control-label col-md-6">DOS </label>
                            <div class="col-md-6">
                                <input type="text" class="form-control required" id="dos" placeholder="Date Of Service" />
                            </div>
                        </div>
                        <div class="form-group col-md-4" >
                            <label class="control-label col-md-6">From Date </label>    
                            <div class="col-md-6">
                                <input type="text" class="form-control required" id="fromDate" placeholder="From Date" />
                            </div>
                        </div>
                        <div class="form-group col-md-4" >
                            <label class="control-label col-md-6">To Date </label>  
                            <div class="col-md-6">
                                <input type="text" class="form-control required" id="toDate" placeholder="To Date" />
                            </div>
                        </div>
                        <div class="form-group col-md-4" >
                            <label class="control-label col-md-6">Client Name </label>
                            <div class="col-md-6">
                                <input type="text" class="form-control required" id="clientName" placeholder="Client Name" />
                            </div>
                        </div>
                        <div class="form-group col-md-4" >
                            <label class="control-label col-md-6">Provider Name </label>    
                            <div class="col-md-6">
                                <input type="text" class="form-control required" id="providerName" placeholder="Provider Name" />
                            </div>
                        </div>
                        <div class="form-group col-md-4" >
                            <label class="control-label col-md-6">Insurance Name </label>
                            <div class="col-md-6">
                                <input type="text" class="form-control required" id="insName" placeholder="Insurance Name" />
                            </div>
                        </div>
                         <div class="form-group col-md-4" >
                            <label class="control-label col-md-6">CPT </label>
                            <div class="col-md-6">
                                <input type="text" class="form-control required" id="cptVal" placeholder="Procedure Code" />
                            </div>
                        </div>
                        <div class="form-group col-md-4" >
                            <label class="control-label col-md-6"> </label>
                            <div class="col-md-6">
                            </div>
                        </div>
                        <div class="form-group col-md-4" >
                            <div class="col-md-12">
                                <input type="button" class="btn btn-primary pull-right" id="btnGo" value="Get Details" />
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group col-md-12" style="padding-top:2%" >
                        </div>
                        <div class="form-group col-md-12" id="bulkDiv">
                          <div class="form-group col-md-2" >
                              <div class="col-md-12">
                                  <input type="text" class="form-control required" id="seqCPT" placeholder="Procedure Code" />
                              </div>
                          </div>
                          <div class="form-group col-md-2" >
                              <div class="col-md-12">
                                  <input type="text" class="form-control required" id="seqMOD" placeholder="Modifier" />
                              </div>
                          </div>
                          <div class="form-group col-md-2" >
                              <div class="col-md-12">
                                  <input type="text" class="form-control required" id="seqProvider" placeholder="Provider Name" />
                              </div>
                          </div>
                          <div class="form-group col-md-2" >
                              <div class="col-md-12">
                                  <select id="seqStatus" class="form-control">
                                    <option value="">--Select Status--</option>
                                    <option value="Ready">Ready to send</option>
                                  </select>
                              </div>
                          </div>
                          <div class="form-group col-md-2" >
                            <div class="col-md-12">
                                <input type="button" class="btn btn-primary" id="btnFin" value="Update" />
                            </div>
                          </div>
                        </div>
                    </div>
          	         <div class="clearfix"></div><br/><br/>
                    <div class="widget-content">
                        <div class="tabbable tabbable-custom">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#tab_1_1" data-toggle="tab">Still Confirmed</a></li>
                                <li><a href="#tab_1_2" data-toggle="tab">Ready to Bill</a></li>
                                <li><a href="#tab_1_3" data-toggle="tab">Ready for EDI</a></li>
                                <li><a href="#tab_1_4" data-toggle="tab">Balance</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_1_1">
                                    <table id="test4" class="display" cellspacing="0" width="100%">
                                    </table>
                                </div>
                                <div class="tab-pane" id="tab_1_2">
                                    <table id="test1" class="display" cellspacing="0" width="100%">
                                    </table>
                                </div>
                                <div class="tab-pane" id="tab_1_3">
                                    <table id="test2" class="display" cellspacing="0" width="100%">
                                    </table>
                                </div>
                                <div class="tab-pane" id="tab_1_4">
                                    <table id="test3" class="display" cellspacing="0" width="100%">
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
			            	<!-- <div id="tableDiv">
			            		<table id="testAll" class="table table-striped table-bordered table-hover table-checkable" data-horizontal-width="150%">
								</table>
			                    <table id="testPatient" class="table table-striped table-bordered table-hover table-checkable" data-horizontal-width="150%">
								</table>
								<table id="testInsurance" class="table table-striped table-bordered table-hover table-checkable" data-horizontal-width="150%">
								</table>
							</div> -->
			            </div>
			        </div>

				</div>

				</div>
				<!-- /Page Content -->
			</div>
			<!-- /.container -->

		</div>
	</div>
    <div class="modal fade" id="swapModal" tabindex="-1">
        <div class="modal-dialog" style="width:50%">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Swap Claim</h4>
                </div>
                <div class="modal-body" style="min-height:80px;">
                    <div class="form-group col-md-8" >
                        <label class="control-label col-md-6">Move to </label>
                        <div class="col-md-6">
                            <select class="form-control" id="moveTo">
                                <option value="Rendered" selected>Review</option>
                                <option value="Ready" selected>Ready To Send</option>
                                <option value="Submit">Submitted</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-md-4" >
                        <input type="button" class="btn btn-primary pull-right" id="btnSwap" value="Swap Claim">
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="button" class="btn btn-default" data-dismiss="modal" value="Exit" />
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
    <div class="modal fade" id="claimDetails" tabindex="-1">
        <div class="modal-dialog" style="width:75%">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Claim Details</h4>
                </div>
                <div class="modal-body" style="min-height:120px;">
                    <div class="form-group col-md-4" >
                        <label class="control-label col-md-6">Client Name </label>
                        <div class="col-md-6">
                            <input type="text" class="form-control required" id="modClientName" disabled />
                        </div>
                    </div>
                    <div class="form-group col-md-4" >
                        <label class="control-label col-md-6">Therapist Name </label>
                        <div class="col-md-6">
                            <input type="text" class="form-control required" id="modTherapist" disabled />
                        </div>
                    </div>
                    <div class="form-group col-md-4" >
                        <label class="control-label col-md-6">Activity </label>
                        <div class="col-md-6">
                            <input type="text" class="form-control required" id="modActivity" disabled />
                        </div>
                    </div>
                    <div class="form-group col-md-4" >
                        <label class="control-label col-md-6">DOS </label>
                        <div class="col-md-6">
                            <input type="text" class="form-control required" id="modDOS" disabled />
                        </div>
                    </div>
                    <div class="form-group col-md-4" >
                        <label class="control-label col-md-6">CPT </label>
                        <div class="col-md-6">
                            <input type="text" class="form-control required" id="modCPT"  />
                        </div>
                    </div>
                    <div class="form-group col-md-4" >
                        <label class="control-label col-md-6">Modifier </label>
                        <div class="col-md-6">
                            <input type="text" class="form-control required" id="modMOD"  />
                        </div>
                    </div>
                    <div class="form-group col-md-4" >
                        <label class="control-label col-md-6">POS </label>
                        <div class="col-md-6">
                            <select class="form-control" id="modPOS">
                                <option value="12">Home</option>
                                <option value="11">Office</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-md-4" >
                        <label class="control-label col-md-6">Units </label>
                        <div class="col-md-6">
                            <input type="text" class="form-control required" id="modUnits"  />
                        </div>
                    </div>
                    <div class="form-group col-md-4" >
                        <label class="control-label col-md-6">Charges </label>
                        <div class="col-md-6">
                            <input type="text" class="form-control required" id="modCharges"  />
                        </div>
                    </div>
                    <div class="form-group col-md-4" >
                        <label class="control-label col-md-6">Claim # </label>
                        <div class="col-md-6">
                            <input type="text" class="form-control required" id="modClaimNo" disabled />
                        </div>
                    </div>
                    <div class="form-group col-md-4" >
                        <label class="control-label col-md-6">Status </label>
                        <div class="col-md-6">
                            <select class="form-control" id="modStatus" disabled>
                                <option value=""></option>
                                    <option value="Confirm">Confirmed</option>
                                    <option value="UnConfirm">UnConfirmed</option>
                                    <option value="Noshow">No Show</option>
                                    <option value="Cancelled">Cancelled</option>
                                    <option value="Hold">Hold</option>
                                    <option value="Cancelled by Client">Cancelled by Client</option>
                                    <option value="Cancelled by Provider">Cancelled by Provider</option>
                                    <option value="Rendered">Rendered</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-md-4" >
                        <label class="control-label col-md-6">Move to </label>
                        <div class="col-md-6" id="ddlMove">
                            <select class="form-control">
                                <option value="Ready">Ready To Send</option>
                                <option value="Submit">Submitted</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="button" class="btn btn-default" data-dismiss="modal" value="Exit" />
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
	<link href="plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="assets/js/alertify.min.js"></script>
	<link href="https://cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css" rel='stylesheet' type='text/css'>
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
  <script src="assets/js/jquery.datetimepicker.js"></script>
    <link href="assets/css/jquery.datetimepicker.css" rel="stylesheet" />
	<script>
	$(document).ready(function(){
		$('#divDDL').hide();
		getAllClaimsInfo();
		var val = "";
		$(document).on('click','.redPat',function() {
			var temp = $(this).attr('class').split(' ')[0];
			var newTemp = temp.replace('redirectPatient','');
			sessionStorage.setItem("patientId", newTemp);
			window.location.href ="EditPatient.php";
		});
		$('.rdoLedger').change(function(){
			$('#divDDL').show();
			val = $(this).val();
			if(val == "patient"){
				document.getElementById("ddlVal").value = "";
				document.getElementById("lblVal").innerHTML = "Patient";
				var patientList = <?php include('patientList.php'); ?>;
	            $("#ddlVal").autocomplete({
	    			source: patientList,
	                autoFocus:true
	            });
			}
			else if(val == "insurance")
			{
				document.getElementById("ddlVal").value = "";
				$('#divDDL').show();
				document.getElementById("lblVal").innerHTML = "Insurance";
				var insuranceList = <?php include('insuranceLedgerList.php'); ?>;
	            $("#ddlVal").autocomplete({
	    			source: insuranceList,
	                autoFocus:true
	            });
			}
			else{
				$('#divDDL').hide();
				getAllClaimsInfo();
			}
		});
		$('#ddlVal').focusout(function(){
			//$('#loader').show();
			if(val == "patient"){
				var truncID = "";
				$('#tableAll').hide();
				$('#tablePat').show();
				$('#tableIns').hide();
				$('#testAll_wrapper').hide();
				var tot = 0;
				$('#testInsurance_wrapper').hide();
				$('#testPatient_wrapper').show();
				var ddlValue = $(this).val();
				var n=ddlValue.indexOf("-");
				var truncID = ddlValue.substr(0,n);
				var tempNam = ddlValue.replace(truncID,"");
				var tempNamFin = tempNam.replace("- ","");
				$.post("http://careaba.com/CareService/claims/ledgerpatientonly",
			    {
			        patientID: truncID
			    },
			    function(data1, status){
			  //   	$.ajax({
		   //                type: "POST",
		   //                url:"getZeroToThirtyByPat.php",
		   //                async : false,
		   //                data:{
		   //                  patientID : truncID
		   //                },success:function(result){
		   //                      document.getElementById('spanPatZero').innerHTML = '$ '+result+'.00';
					// 			tot += parseInt(result);
		   //                   }
		   //            });
					// $.ajax({
		   //                type: "POST",
		   //                url:"getThirtyToSixtyByPat.php",
		   //                async : false,
		   //                data:{
		   //                  patientID : truncID
		   //                },success:function(result){
		   //                  	document.getElementById('spanPatThirty').innerHTML = '$ '+result+'.00';
					// 			tot += parseInt(result);
		   //                   }
		   //            });
					// $.ajax({
		   //                type: "POST",
		   //                url:"getSixtyToNinetyByPat.php",
		   //                async : false,
		   //                data:{
		   //                  patientID : truncID
		   //                },success:function(result){
		   //                      document.getElementById('spanPatSixty').innerHTML = '$ '+result+'.00';
					// 			tot += parseInt(result);
		   //                   }
		   //            });
					// $.ajax({
		   //                type: "POST",
		   //                url:"getNinetyToOnetwentyByPat.php",
		   //                async : false,
		   //                data:{
		   //                  patientID : truncID
		   //                },success:function(result){
		   //                      document.getElementById('spanPatNinety').innerHTML = '$ '+result+'.00';
					// 			tot += parseInt(result);
		   //                   }
		   //            });
					// $.ajax({
		   //                type: "POST",
		   //                url:"get120plusByPat.php",
		   //                async : false,
		   //                data:{
		   //                  patientID : truncID
		   //                },success:function(result){
		   //                  	document.getElementById('spanPat120').innerHTML = '$ '+result+'.00';
					// 			tot += parseInt(result);
		   //                   }
		   //            });
					// document.getElementById('spanPatTotal').innerHTML = '$ '+parseInt(tot);
					var dt = [];
					$.each(data1,function(i,v) {
						if(data1[i].charge == ""){
                            data1[i].charge = 0.00;
                        }
                        else{
                            if(data1[i].charge%1 == 0){
                                //data1[i].charge = data1[i].charge+'.00';
                                data1[i].charge = data1[i].charge;
                            }
                            else{
                                data1[i].charge = data1[i].charge;
                            }
                        }
                        if(data1[i].allowed == ""){
                            //data1[i].allowed = 0+'.00';
                            data1[i].allowed = 0;
                        }
                        else{
                            if(data1[i].allowed%1 == 0){
                                //data1[i].allowed = data1[i].allowed+'.00';
                                data1[i].allowed = data1[i].allowed;
                            }
                            else{
                                data1[i].allowed = data1[i].allowed;
                            }
                        }
                        if(data1[i].claimBalance == ""){
                            if(data1[i].total%1 == 0){
                                //data1[i].claimBalance = data1[i].total - data1[i].copay+'.00';
                                data1[i].claimBalance = data1[i].total - data1[i].copay;
                            }
                            else{
                                data1[i].claimBalance = data1[i].total - data1[i].copay;
                            }
                        }
                        else{
                            if(data1[i].claimBalance%1 == 0){
                                //data1[i].claimBalance = data1[i].claimBalance+'.00';
                                data1[i].claimBalance = data1[i].claimBalance;
                            }
                            else{
                                data1[i].claimBalance = data1[i].claimBalance;
                            }
                        }
                        if(data1[i].paid == ""){
                            //data1[i].paid = 0+'.00';
                            data1[i].paid = 0;
                        }
                        else{
                            if(data1[i].paid%1 == 0){
                                //data1[i].paid = data1[i].paid+'.00';
                                data1[i].paid = data1[i].paid;
                            }
                            else{
                                data1[i].paid = data1[i].paid;
                            }
                        }
                        if(data1[i].adjustment == ""){
                            //data1[i].adjustment = 0+'.00';
                            data1[i].adjustment = 0;
                        }
                        else{
                            if(data1[i].adjustment%1 == 0){
                                //data1[i].adjustment = data1[i].adjustment+'.00';
                                data1[i].adjustment = data1[i].adjustment;
                            }
                            else{
                                data1[i].adjustment = data1[i].adjustment;
                            }
                        }
						$.ajax({
			                  type: "POST",
			                  url:"getPatientName.php",
			                  async : false,
			                  data:{
			                    claimID : data1[i].claimID
			                  },success:function(result){
			                  	if(result!=""){
			                  		fullName = result;
			                  	}
			                  	else{
			                  		fullName = "";
			                  	}
			                  }
			              });
						$.ajax({
                              type: "POST",
                              url:"getInsuranceName.php",
                              async : false,
                              data:{
                                insuranceID : data1[i].insuranceID
                              },success:function(result){
                              	var res = JSON.parse(result);
                              	if(res.length != 0){
			                  		payorName = res[0].payerName;
			                  	}
			                  	else{
			                  		payorName = "SELF";
			                  	}
	                          }
	                      });
						dt.push([data1[i].claimID,data1[i].fromDt,data1[i].proced,data1[i].units,data1[i].toDt,data1[i].charge,data1[i].allowed,data1[i].paid,data1[i].adjustment,data1[i].claimBalance,'SELF',tempNamFin,fullName]);
					});
					$('#testPatient').DataTable({
						"aaSorting": [[ 0, "desc" ]],
                        "destroy": true,
						"data": dt,
				        columns: [
				        	{"title": "Claim ID","visible" : false},
				            {"title": "DOS",
				        		"render": function ( data, type, full, meta ) {
							      return changeDateFormat(data);
							    }
							},
				            {"title": "CPT"},
				            {"title": "UNITS"},
				            {"title": "Bill Date",
				        		"render": function ( data, type, full, meta ) {
							      return changeDateFormat(data);
							    }
							},
				            {"title": "Billed Amount",
				        		"render": function ( data, type, full, meta ) {
							      return '$'+data;
							    }
							},
				            {"title": "Allowed Amount",
				        		"render": function ( data, type, full, meta ) {
							      return '$'+data;
							    }
							},
				            {"title": "Paid",
				        		"render": function ( data, type, full, meta ) {
							      return '$'+data;
							    }
							},
				            {"title": "Adjustment",
				        		"render": function ( data, type, full, meta ) {
							      return '$'+data;
							    }
							},
				            {"title": "Balance",
				        		"render": function ( data, type, full, meta ) {
							      return '$'+data;
							    }
							},
				            {"title": "Billed To"},
				            {"title": "Patient Name",
							    "render": function ( data, type, full, meta ) {
							    	var tempData=data.indexOf("-");
									var truncID = data.substr(0,tempData);
									var tempName = data.replace(truncID,"");
									var tempDataName = tempName.replace("- ","");
							      return '<a href="#" class="redirectPatient'+truncID+' redPat">'+tempDataName+'</a>';
							    }
							}
				        ]
					});
					//$('#testPatient').DataTable().destroy();
			    });
				$('#loader').hide();
			}
			else{
				$('#tableAll').hide();
				$('#tablePat').hide();
				$('#tableIns').show();
				var tot = 0;
				$('#testAll_wrapper').hide();
				$('#testInsurance_wrapper').show();
				$('#testPatient_wrapper').hide();
				var ddlValue = $(this).val();
				var n=ddlValue.indexOf("-");
				var truncID = ddlValue.substr(0,n);
				
				$.post("http://careaba.com/CareService/claims/ledgerinsuranceonly",
			    {
			        insuranceID: truncID
			    },
			    function(data1, status){
			  //   	$.ajax({
		   //                type: "POST",
		   //                url:"getZeroToThirtyByIns.php",
		   //                async : false,
		   //                data:{
		   //                  insuranceID : truncID
		   //                },success:function(result){
		   //                      document.getElementById('spanInsZero').innerHTML = '$ '+result+'.00';
					// 			tot += parseInt(result);
		   //                   }
		   //            });
					// $.ajax({
		   //                type: "POST",
		   //                url:"getThirtyToSixtyByIns.php",
		   //                async : false,
		   //                data:{
		   //                  insuranceID : truncID
		   //                },success:function(result){
		   //                  	document.getElementById('spanInsThirty').innerHTML = '$ '+result+'.00';
					// 			tot += parseInt(result);
		   //                   }
		   //            });
					// $.ajax({
		   //                type: "POST",
		   //                url:"getSixtyToNinetyByIns.php",
		   //                async : false,
		   //                data:{
		   //                  insuranceID : truncID
		   //                },success:function(result){
		   //                      document.getElementById('spanInsSixty').innerHTML = '$ '+result+'.00';
					// 			tot += parseInt(result);
		   //                   }
		   //            });
					// $.ajax({
		   //                type: "POST",
		   //                url:"getNinetyToOnetwentyByIns.php",
		   //                async : false,
		   //                data:{
		   //                  insuranceID : truncID
		   //                },success:function(result){
		   //                      document.getElementById('spanInsNinety').innerHTML = '$ '+result+'.00';
					// 			tot += parseInt(result);
		   //                   }
		   //            });
					// $.ajax({
		   //                type: "POST",
		   //                url:"get120plusByIns.php",
		   //                async : false,
		   //                data:{
		   //                  insuranceID : truncID
		   //                },success:function(result){
		   //                  	document.getElementById('spanIns120').innerHTML = '$ '+result+'.00';
					// 			tot += parseInt(result);
		   //                   }
		   //            });
					// document.getElementById('spanInsTotal').innerHTML = '$ '+parseInt(tot)+'.00';
					var dt = [];
					$.each(data1,function(i,v) {
						if(data1[i].charge == ""){
                            data1[i].charge = 0.00;
                        }
                        else{
                            if(data1[i].charge%1 == 0){
                                //data1[i].charge = data1[i].charge+'.00';
                                data1[i].charge = data1[i].charge;
                            }
                            else{
                                data1[i].charge = data1[i].charge;
                            }
                        }
                        if(data1[i].allowed == ""){
                            //data1[i].allowed = 0+'.00';
                            data1[i].allowed = 0;
                        }
                        else{
                            if(data1[i].allowed%1 == 0){
                                //data1[i].allowed = data1[i].allowed+'.00';
                                data1[i].allowed = data1[i].allowed;
                            }
                            else{
                                data1[i].allowed = data1[i].allowed;
                            }
                        }
                        if(data1[i].claimBalance == ""){
                            if(data1[i].total%1 == 0){
                                //data1[i].claimBalance = data1[i].total - data1[i].copay+'.00';
                                data1[i].claimBalance = data1[i].total - data1[i].copay;
                            }
                            else{
                                data1[i].claimBalance = data1[i].total - data1[i].copay;
                            }
                        }
                        else{
                            if(data1[i].claimBalance%1 == 0){
                                //data1[i].claimBalance = data1[i].claimBalance+'.00';
                                data1[i].claimBalance = data1[i].claimBalance;
                            }
                            else{
                                data1[i].claimBalance = data1[i].claimBalance;
                            }
                        }
                        if(data1[i].paid == ""){
                            //data1[i].paid = 0+'.00';
                            data1[i].paid = 0;
                        }
                        else{
                            if(data1[i].paid%1 == 0){
                                //data1[i].paid = data1[i].paid+'.00';
                                data1[i].paid = data1[i].paid;
                            }
                            else{
                                data1[i].paid = data1[i].paid;
                            }
                        }
                        if(data1[i].adjustment == ""){
                            //data1[i].adjustment = 0+'.00';
                            data1[i].adjustment = 0;
                        }
                        else{
                            if(data1[i].adjustment%1 == 0){
                                //data1[i].adjustment = data1[i].adjustment+'.00';
                                data1[i].adjustment = data1[i].adjustment;
                            }
                            else{
                                data1[i].adjustment = data1[i].adjustment;
                            }
                        }
						$.ajax({
			                  type: "POST",
			                  url:"getPatientName.php",
			                  async : false,
			                  data:{
			                    claimID : data1[i].claimID
			                  },success:function(result){
			                  	if(result!=""){
			                  		fullName = result;
			                  	}
			                  	else{
			                  		fullName = "";
			                  	}
			                  }
			              });
						$.ajax({
                              type: "POST",
                              url:"getInsuranceName.php",
                              async : false,
                              data:{
                                insuranceID : data1[i].insuranceID
                              },success:function(result){
                              	var res = JSON.parse(result);
                              	if(res.length != 0){
			                  		payorName = res[0].payerName;
			                  	}
			                  	else{
			                  		payorName = "SELF";
			                  	}
	                          }
	                      });
						dt.push([data1[i].claimID,data1[i].fromDt,data1[i].proced,data1[i].units,data1[i].toDt,data1[i].charge,data1[i].allowed,data1[i].paid,data1[i].adjustment,data1[i].claimBalance,payorName,fullName]);
					});
					$('#testInsurance').DataTable({
						"data": dt,
						"aaSorting": [[ 0, "desc" ]],
                        "destroy": true,
				        columns: [
				        	{"title": "Claim ID","visible" : false},
				            {"title": "DOS",
				        		"render": function ( data, type, full, meta ) {
							      return changeDateFormat(data);
							    }
							},
				            {"title": "CPT"},
				            {"title": "UNITS"},
				            {"title": "Bill Date",
				        		"render": function ( data, type, full, meta ) {
							      return changeDateFormat(data);
							    }
							},
				            {"title": "Billed Amount",
				        		"render": function ( data, type, full, meta ) {
							      return '$'+data;
							    }
							},
				            {"title": "Allowed Amount",
				        		"render": function ( data, type, full, meta ) {
							      return '$'+data;
							    }
							},
				            {"title": "Paid",
				        		"render": function ( data, type, full, meta ) {
							      return '$'+data;
							    }
							},
				            {"title": "Adjustment",
				        		"render": function ( data, type, full, meta ) {
							      return '$'+data;
							    }
							},
				            {"title": "Balance",
				        		"render": function ( data, type, full, meta ) {
							      return '$'+data;
							    }
							},
				            {"title": "Billed To"},
				            {"title": "Patient Name",
							    "render": function ( data, type, full, meta ) {
							    	var tempData=data.indexOf("-");
									var truncID = data.substr(0,tempData);
									var tempName = data.replace(truncID,"");
									var tempDataName = tempName.replace("- ","");
							      return '<a href="#" class="redirectPatient'+truncID+' redPat">'+tempDataName+'</a>';
							    }
							}
				        ]
					});
					//$('#testInsurance').DataTable().destroy();
			    });
				$('#loader').hide();
			}

		});
	
	 });
	
	function Dec2(num) {
	  num = String(num);
	  if(num.indexOf('.') !== -1) {
	    var numarr = num.split(".");
	    if (numarr.length == 1) {
	      return Number(num);
	    }
	    else {
	      return Number(numarr[0]+"."+numarr[1].charAt(0)+numarr[1].charAt(1));
	    }
	  }
	  else {
	    return Number(num);
	  }  
	}

	function getAllClaimsInfo(){
		var tot = 0;
		var AllPattot = 0;
		$('#tableAll').show();
		$('#tablePat').hide();
		$('#tableIns').hide();
		// $.get("getZeroToThirtyAllByIns.php",function(data1){
		// 	document.getElementById('spanZero').innerHTML = '$ '+data1+'.00';
		// 	tot += parseInt(data1);
		// });

		// $.get("getThirtyToSixtyAllByIns.php",function(data1){
		// 	document.getElementById('spanThirty').innerHTML = '$ '+data1+'.00';
		// 	tot += parseInt(data1);
		// });

		// $.get("getSixtyToNinetyAllByIns.php",function(data1){
		// 	document.getElementById('spanSixty').innerHTML = '$ '+data1+'.00';
		// 	tot += parseInt(data1);
		// });

		// $.get("getNinetyToOnetwentyAllByIns.php",function(data1){
		// 	document.getElementById('spanNinety').innerHTML = '$ '+data1+'.00';
		// 	tot += parseInt(data1);
		// });

		// $.get("get120plusAllByIns.php",function(data1){
		// 	document.getElementById('span120').innerHTML = '$ '+data1+'.00';
		// 	tot += parseInt(data1);
		// });

		// $.get("getZeroToThirtyAllByPat.php",function(data1){
		// 	document.getElementById('spanAllPatZero').innerHTML = '$ '+data1+'.00';
		// 	AllPattot += parseInt(data1);
		// });

		// $.get("getThirtyToSixtyAllByPat.php",function(data1){
		// 	document.getElementById('spanAllPatThirty').innerHTML = '$ '+data1+'.00';
		// 	AllPattot += parseInt(data1);
		// });

		// $.get("getSixtyToNinetyAllByPat.php",function(data1){
		// 	document.getElementById('spanAllPatSixty').innerHTML = '$ '+data1+'.00';
		// 	AllPattot += parseInt(data1);
		// });

		// $.get("getNinetyToOnetwentyAllByPat.php",function(data1){
		// 	document.getElementById('spanAllPatNinety').innerHTML = '$ '+data1+'.00';
		// 	AllPattot += parseInt(data1);
		// });

		// $.get("get120plusAllByPat.php",function(data1){
		// 	document.getElementById('spanAllPat120').innerHTML = '$ '+data1+'.00';
		// 	AllPattot += parseInt(data1);
		// });

		
		$.get("getAllClaims.php",function(data1){
	    $('#loader').show();
	    var data1 = JSON.parse(data1);
			var dt = [];
			$.each(data1,function(i,v) {
				dt.push([data1[i].claimID,data1[i].claimID,data1[i].fullName,data1[i].phyName,data1[i].rendPhy,data1[i].activityName,data1[i].fromDt,data1[i].proced,data1[i].mod1,data1[i].appLocation,data1[i].units,data1[i].total,data1[i].claimNumber,data1[i].claimStatus,data1[i].claimID]);
			});
			$('#test1').DataTable({
				"aaSorting": [[ 0, "desc" ]],
        "destroy": true,
				"data": dt,
		        columns: [
              {"title": "Claim ID","visible" : false},
              {"title": "",
                  "render": function ( data, type, full, meta ) {
                    return '<input type="checkbox" class="chk chk'+full[0]+' form-control" id="chk'+full[0]+'" />';
                  }
              },
              {"title": "Client Name",
                  "render": function ( data, type, full, meta ) {
                    return data;
                  }
              },
              {"title": "Client Provider"},
              {"title": "Rendering Provider"},
              {"title": "Activity"},
     //          {"title": "Bill Date",
     //           "render": function ( data, type, full, meta ) {
              //       return changeDateFormat(data);
              //     }
              // },
              {"title": "DOS",
                  "render": function ( data, type, full, meta ) {
                    return changeDateFormat(data);
                  }
              },
              {"title": "CPT",
                  "render": function ( data, type, full, meta ) {
                    return '<input type="text" class="txtCPT txtCPT'+full[0]+' form-control" id="txtCPT'+full[0]+'" value="'+data+'" />';
                  }
              },
              {"title": "Modifier",
                  "render": function ( data, type, full, meta ) {
                    return '<input type="text" class="txtMod txtMod'+full[0]+' form-control" id="txtMod'+full[0]+'" value="'+data+'" />';
                  }
              },
              {"title": "POS",
                  "render": function ( data, type, full, meta ) {
                    return '<input type="text" class="txtPOS txtPOS'+full[0]+' form-control" id="txtPOS'+full[0]+'" value="'+data+'" />';
                  }
              },
              {"title": "Units",
                  "render": function ( data, type, full, meta ) {
                    return '<input type="text" class="txtUnits txtUnits'+full[0]+' form-control" id="txtUnits'+full[0]+'" value="'+data+'" />';
                  }
              },
              {"title": "Charges",
                  "render": function ( data, type, full, meta ) {
                    return '<input type="text" class="txtCharges txtCharges'+full[0]+' form-control" id="txtCharges'+full[0]+'" value="'+data+'" />';
                  }
              },
              {"title": "Claim #"},
              {"title": "Status"},
              {"title": "Actions",
                  "render": function ( data, type, full, meta ) {
                    return '<a href="javascript:void(0)" class="edit'+data+' editLedger" title="Update"><i class="icon icon-ok" style="font-size:12px;"></i> </a>&nbsp;&nbsp; |&nbsp;&nbsp;<a href="javascript:void(0)" class="xchange'+data+' xchange"><i class="icon icon-exchange" style="font-size:12px;"></i> </a>';
                  }
              }
		        ]
			});
			$('#loader').hide();
			//document.getElementById('spanTotal').innerHTML = '$ '+parseInt(tot)+'.00';
			//document.getElementById('spanAllPatTot').innerHTML = '$ '+AllPattot+'.00';
			$('#testAll').removeAttr('style');
	    });


      $.get("getConfirmedClaims.php",function(data1){
      $('#loader').show();
      var data1 = JSON.parse(data1);
      var dt = [];
      $.each(data1,function(i,v) {
        dt.push([data1[i].claimID,data1[i].claimID,data1[i].fullName,data1[i].phyName,data1[i].rendPhy,data1[i].activityName,data1[i].fromDt,data1[i].proced,data1[i].mod1,data1[i].appLocation,data1[i].units,data1[i].total,data1[i].claimNumber,data1[i].claimStatus,data1[i].claimID]);
      });
      $('#test4').DataTable({
        "aaSorting": [[ 0, "desc" ]],
        "destroy": true,
        "data": dt,
            columns: [
              {"title": "Claim ID","visible" : false},
              {"title": "",
                  "render": function ( data, type, full, meta ) {
                    return '<input type="checkbox" class="chk chk'+full[0]+' form-control" id="chk'+full[0]+'" />';
                  }
              },
              {"title": "Client Name",
                  "render": function ( data, type, full, meta ) {
                    return data;
                  }
              },
              {"title": "Client Provider"},
              {"title": "Rendering Provider"},
              {"title": "Activity"},
     //          {"title": "Bill Date",
     //           "render": function ( data, type, full, meta ) {
              //       return changeDateFormat(data);
              //     }
              // },
              {"title": "DOS",
                  "render": function ( data, type, full, meta ) {
                    return changeDateFormat(data);
                  }
              },
              {"title": "CPT",
                  "render": function ( data, type, full, meta ) {
                    return '<input type="text" class="txtCPT txtCPT'+full[0]+' form-control" id="txtCPT'+full[0]+'" value="'+data+'" />';
                  }
              },
              {"title": "Modifier",
                  "render": function ( data, type, full, meta ) {
                    return '<input type="text" class="txtMod txtMod'+full[0]+' form-control" id="txtMod'+full[0]+'" value="'+data+'" />';
                  }
              },
              {"title": "POS",
                  "render": function ( data, type, full, meta ) {
                    return '<input type="text" class="txtPOS txtPOS'+full[0]+' form-control" id="txtPOS'+full[0]+'" value="'+data+'" />';
                  }
              },
              {"title": "Units",
                  "render": function ( data, type, full, meta ) {
                    return '<input type="text" class="txtUnits txtUnits'+full[0]+' form-control" id="txtUnits'+full[0]+'" value="'+data+'" />';
                  }
              },
              {"title": "Charges",
                  "render": function ( data, type, full, meta ) {
                    return '<input type="text" class="txtCharges txtCharges'+full[0]+' form-control" id="txtCharges'+full[0]+'" value="'+data+'" />';
                  }
              },
              {"title": "Claim #"},
              {"title": "Status"},
              {"title": "Actions",
                  "render": function ( data, type, full, meta ) {
                    return '<a href="javascript:void(0)" class="edit'+data+' editLedger" title="Update"><i class="icon icon-ok" style="font-size:12px;"></i> </a>&nbsp;&nbsp; |&nbsp;&nbsp;<a href="javascript:void(0)" class="xchange'+data+' xchange"><i class="icon icon-exchange" style="font-size:12px;"></i> </a>';
                  }
              }
            ]
      });
      $('#loader').hide();
      //document.getElementById('spanTotal').innerHTML = '$ '+parseInt(tot)+'.00';
      //document.getElementById('spanAllPatTot').innerHTML = '$ '+AllPattot+'.00';
      $('#testAll').removeAttr('style');
      });
        $.get("getReadyClaims.php",function(data1){
            $('#loader').show();
            var data1 = JSON.parse(data1);
            var dt = [];
            $.each(data1,function(i,v) {
                dt.push([data1[i].claimID,data1[i].claimID,data1[i].fullName,data1[i].phyName,data1[i].rendPhy,data1[i].activityName,data1[i].fromDt,data1[i].proced,data1[i].mod1,data1[i].appLocation,data1[i].units,data1[i].total,data1[i].claimNumber,data1[i].claimStatus,data1[i].claimID]);
            });
            $('#test2').DataTable({
                "aaSorting": [[ 0, "desc" ]],
                "destroy": true,
                "data": dt,
                columns: [
                        {"title": "Claim ID","visible" : false},
                        {"title": "",
                            "render": function ( data, type, full, meta ) {
                              return '<input type="checkbox" class="chk chk'+full[0]+' form-control" id="chk'+full[0]+'" />';
                            }
                        },
                        {"title": "Client Name",
                            "render": function ( data, type, full, meta ) {
                              return data;
                            }
                        },
                        {"title": "Client Provider"},
                        {"title": "Rendering Provider"},
                        {"title": "Activity"},
               //          {"title": "Bill Date",
               //           "render": function ( data, type, full, meta ) {
                        //       return changeDateFormat(data);
                        //     }
                        // },
                        {"title": "DOS",
                            "render": function ( data, type, full, meta ) {
                              return changeDateFormat(data);
                            }
                        },
                        {"title": "CPT"
                        },
                        {"title": "Modifier"
                        },
                        {"title": "POS"
                        },
                        {"title": "Units"
                        },
                        {"title": "Charges"
                        },
                        {"title": "Claim #"},
                        {"title": "Status"},
                        {"title": "Actions",
                            "render": function ( data, type, full, meta ) {
                              return '<a href="javascript:void(0)" class="edit'+data+' editLedger" title="Update"><i class="icon icon-ok" style="font-size:12px;"></i> </a>&nbsp;&nbsp; |&nbsp;&nbsp;<a href="javascript:void(0)" class="xchange'+data+' xchange"><i class="icon icon-exchange" style="font-size:12px;"></i> </a>';
                            }
                        }
                ]
            });
            $('#loader').hide();
            //document.getElementById('spanTotal').innerHTML = '$ '+parseInt(tot)+'.00';
            //document.getElementById('spanAllPatTot').innerHTML = '$ '+AllPattot+'.00';
            $('#testAll').removeAttr('style');
        });
        $.get("getSubmitClaims.php",function(data1){
            $('#loader').show();
            var data1 = JSON.parse(data1);
            var dt = [];
            $.each(data1,function(i,v) {
                dt.push([data1[i].claimID,data1[i].claimID,data1[i].fullName,data1[i].phyName,data1[i].rendPhy,data1[i].activityName,data1[i].fromDt,data1[i].proced,data1[i].mod1,data1[i].appLocation,data1[i].units,data1[i].total,data1[i].claimNumber,data1[i].claimStatus,data1[i].claimID]);
            });
            $('#test3').DataTable({
                "aaSorting": [[ 0, "desc" ]],
                "destroy": true,
                "data": dt,
                columns: [
                        {"title": "Claim ID","visible" : false},
                        {"title": "",
                            "render": function ( data, type, full, meta ) {
                              return '<input type="checkbox" class="chk chk'+full[0]+' form-control" id="chk'+full[0]+'" />';
                            }
                        },
                        {"title": "Client Name",
                            "render": function ( data, type, full, meta ) {
                              return data;
                            }
                        },
                        {"title": "Client Provider"},
                        {"title": "Rendering Provider"},
                        {"title": "Activity"},
               //          {"title": "Bill Date",
               //           "render": function ( data, type, full, meta ) {
                        //       return changeDateFormat(data);
                        //     }
                        // },
                        {"title": "DOS",
                            "render": function ( data, type, full, meta ) {
                              return changeDateFormat(data);
                            }
                        },
                        {"title": "CPT"
                        },
                        {"title": "Modifier"
                        },
                        {"title": "POS"
                        },
                        {"title": "Units"
                        },
                        {"title": "Charges"
                        },
                        {"title": "Claim #"},
                        {"title": "Status"},
                        {"title": "Actions",
                            "render": function ( data, type, full, meta ) {
                              return '<a href="javascript:void(0)" class="edit'+data+' editLedger" title="Update"><i class="icon icon-ok" style="font-size:12px;"></i> </a>';
                            }
                        }
                ]
            });
            $('#loader').hide();
            //document.getElementById('spanTotal').innerHTML = '$ '+parseInt(tot)+'.00';
            //document.getElementById('spanAllPatTot').innerHTML = '$ '+AllPattot+'.00';
            $('#testAll').removeAttr('style');
        });
	}
	function changeDateFormat(inputDate){  // expects Y-m-d
        var splitDate = inputDate.split('-');
        if(splitDate.count == 0){
            return null;
        }

        var year = splitDate[0];
        var month = splitDate[1];
        var day = splitDate[2]; 

        return month + '-' + day + '-' + year;
    }
	</script>
</body>
</html>