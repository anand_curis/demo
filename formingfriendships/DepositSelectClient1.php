<?php
 session_start();

 if (!isset($_SESSION['userID'])) {
    header("Location: login.php");
 }
 ?>
 <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <title>Deposit | AMROMED LLC</title>

    <!--=== CSS ===-->

    <!-- Bootstrap -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

    <!-- jQuery UI -->
    <!--<link href="plugins/jquery-ui/jquery-ui-1.10.2.custom.css" rel="stylesheet" type="text/css" />-->
    <!--[if lt IE 9]>
        <link rel="stylesheet" type="text/css" href="plugins/jquery-ui/jquery.ui.1.10.2.ie.css"/>
    <![endif]-->
    <!-- Theme -->
    <link href="assets/css/main.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/plugins.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/alertify.css" rel='stylesheet' type='text/css'>
    <link href="assets/css/themes/default.css" rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="assets/css/fontawesome/font-awesome.min.css">
    <!--[if IE 7]>
        <link rel="stylesheet" href="assets/css/fontawesome/font-awesome-ie7.min.css">
    <![endif]-->

    <!--[if IE 8]>
        <link href="assets/css/ie8.css" rel="stylesheet" type="text/css" />
    <![endif]-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>

    <!--=== JavaScript ===-->

    <script type="text/javascript" src="assets/js/libs/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>
    <script type="text/javascript" src="plugins/bootstrap-switch/bootstrap-switch.min.js"></script>

    <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/js/libs/lodash.compat.min.js"></script>

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
        <script src="assets/js/libs/html5shiv.js"></script>
    <![endif]-->

    <!-- Smartphone Touch Events -->
    <script type="text/javascript" src="plugins/touchpunch/jquery.ui.touch-punch.min.js"></script>
    <script type="text/javascript" src="plugins/event.swipe/jquery.event.move.js"></script>
    <script type="text/javascript" src="plugins/event.swipe/jquery.event.swipe.js"></script>

    <!-- General -->
    <script type="text/javascript" src="assets/js/libs/breakpoints.js"></script>
    <script type="text/javascript" src="plugins/respond/respond.min.js"></script> <!-- Polyfill for min/max-width CSS3 Media Queries (only for IE8) -->
    <script type="text/javascript" src="plugins/cookie/jquery.cookie.min.js"></script>
    <script type="text/javascript" src="plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script type="text/javascript" src="plugins/slimscroll/jquery.slimscroll.horizontal.min.js"></script>

    <!-- Page specific plugins -->
    <!-- Charts -->
    <script type="text/javascript" src="plugins/sparkline/jquery.sparkline.min.js"></script>

    <script type="text/javascript" src="plugins/daterangepicker/moment.min.js"></script>
    <script type="text/javascript" src="plugins/daterangepicker/daterangepicker.js"></script>
    <script type="text/javascript" src="plugins/blockui/jquery.blockUI.min.js"></script>

    <!-- Pickers -->
    <script type="text/javascript" src="plugins/pickadate/picker.js"></script>
    <script type="text/javascript" src="plugins/pickadate/picker.date.js"></script>
    <script type="text/javascript" src="plugins/pickadate/picker.time.js"></script>
    <script type="text/javascript" src="plugins/bootstrap-colorpicker/bootstrap-colorpicker.min.js"></script>

    <!-- Noty -->
    <script type="text/javascript" src="plugins/noty/jquery.noty.js"></script>
    <script type="text/javascript" src="plugins/noty/layouts/top.js"></script>
    <script type="text/javascript" src="plugins/noty/themes/default.js"></script>

    <!-- Slim Progress Bars -->
    <script type="text/javascript" src="plugins/nprogress/nprogress.js"></script>

    <!-- Bootbox -->
    <script type="text/javascript" src="plugins/bootbox/bootbox.min.js"></script>

    <!-- App -->
    <script type="text/javascript" src="assets/js/app.js"></script>
    <script type="text/javascript" src="assets/js/plugins.js"></script>
    <script type="text/javascript" src="assets/js/plugins.form-components.js"></script>

    <script>
    $(document).ready(function(){
        "use strict";

        App.init(); // Init layout and core plugins
        Plugins.init(); // Init all plugins
        FormComponents.init(); // Init all form-specific plugins
        document.getElementById('practiceName').innerHTML = sessionStorage.getItem('practiceName');
        var Id = getParameterByName('id');
    });
    function getParameterByName(name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    }
    </script>
    <style>
    .icon:hover{
        text-decoration: none;
    }
    a:hover{
        text-decoration: none;
    }
    </style>
    
    <!-- Demo JS -->
    <script type="text/javascript" src="assets/js/custom.js"></script>
    <script type="text/javascript" src="assets/js/demo/ui_general.js"></script>
</head>

<body>
    <!-- Header -->
    <header class="header navbar navbar-fixed-top" role="banner" style="background-image: url('assets/bg.jpg'); background-repeat: repeat-x;">
        <!-- Top Navigation Bar -->
        <div class="container">

            <!-- Only visible on smartphones, menu toggle -->
            <ul class="nav navbar-nav">
                <li class="nav-toggle"><a href="javascript:void(0);" title=""><i class="icon-reorder"></i></a></li>
            </ul>

            <!-- Logo -->
            <a class="navbar-brand" style="text-align:center;padding-right:50px;" href="javascript:void(0);">
                <img src="assets/logo2.png"/>
                <strong>medABA</strong>
            </a>
            <!-- /logo -->

            <!-- Sidebar Toggler -->
            <a href="#" class="toggle-sidebar bs-tooltip" data-placement="bottom" data-original-title="Toggle navigation">
                <i class="icon-reorder"></i>
            </a>
            <!-- /Sidebar Toggler -->

            <!-- Top Left Menu -->
            <ul class="nav navbar-nav navbar-left hidden-xs hidden-sm" style="list-style:none;">
                <li>
                    <a href="ViewPatient.php" class="dropdown-toggle">
                        Clients
                    </a>
                </li>
                <li>
                    <a href="scheduler.php" class="dropdown-toggle">
                        Scheduler
                    </a>
                </li>
                <li>
                    <a href="Deposit.php" class="dropdown-toggle">
                        Deposit
                    </a>
                </li>
                <li>
                    <a href="Ledger.php" class="dropdown-toggle">
                        Ledger
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                        EDI
                        <i class="icon-caret-down small"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="generateAllEDI.php">
                            <i class="icon-angle-right"></i>
                            Generate EDI
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="EDIList.php">
                            <i class="icon-angle-right"></i>
                            EDI List
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="charts.html" class="dropdown-toggle" data-toggle="dropdown">
                        Settings
                        <i class="icon-caret-down small"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="ViewPractice.php">
                            <i class="icon-angle-right"></i>
                            Practices
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="ViewPhysician.php">
                            <i class="icon-angle-right"></i>
                            Physicians
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="ViewServiceLoc.php">
                            <i class="icon-angle-right"></i>
                            Locations &amp; Facilities
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="javascript:void(0);" style="font-weight:600;">
                            Codes
                            </a>
                            <li>
                                <a href="ViewCPT.php">
                                <i class="icon-angle-right"></i>
                                CPT/Procedure
                                </a>
                            </li>
                            <li>
                                <a href="ViewDX.php">
                                <i class="icon-angle-right"></i>
                                ICD 10/ICD 9 Library
                                </a>
                            </li>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="insurances.php">
                            <i class="icon-angle-right"></i>
                            Insurances
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- /Top Left Menu -->

            <!-- Top Right Menu -->
            <ul class="nav navbar-nav navbar-right">
                <!-- Notifications -->
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-warning-sign"></i>
                        <span class="badge">5</span>
                    </a>
                    <ul class="dropdown-menu extended notification">
                        <li class="title">
                            <p>You have 5 new notifications</p>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="label label-success"><i class="icon-plus"></i></span>
                                <span class="message">New user registration.</span>
                                <span class="time">1 mins</span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="label label-danger"><i class="icon-warning-sign"></i></span>
                                <span class="message">High CPU load on cluster #2.</span>
                                <span class="time">5 mins</span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="label label-success"><i class="icon-plus"></i></span>
                                <span class="message">New user registration.</span>
                                <span class="time">10 mins</span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="label label-info"><i class="icon-bullhorn"></i></span>
                                <span class="message">New items are in queue.</span>
                                <span class="time">25 mins</span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="label label-warning"><i class="icon-bolt"></i></span>
                                <span class="message">Disk space to 85% full.</span>
                                <span class="time">55 mins</span>
                            </a>
                        </li>
                        <li class="footer">
                            <a href="javascript:void(0);">View all notifications</a>
                        </li>
                    </ul>
                </li>

                <!-- Tasks -->
                <li class="dropdown hidden-xs hidden-sm">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-tasks"></i>
                        <span class="badge">7</span>
                    </a>
                    <ul class="dropdown-menu extended notification">
                        <li class="title">
                            <p>You have 7 pending tasks</p>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="task">
                                    <span class="desc">Preparing new release</span>
                                    <span class="percent">30%</span>
                                </span>
                                <div class="progress progress-small">
                                    <div style="width: 30%;" class="progress-bar progress-bar-info"></div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="task">
                                    <span class="desc">Change management</span>
                                    <span class="percent">80%</span>
                                </span>
                                <div class="progress progress-small progress-striped active">
                                    <div style="width: 80%;" class="progress-bar progress-bar-danger"></div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="task">
                                    <span class="desc">Mobile development</span>
                                    <span class="percent">60%</span>
                                </span>
                                <div class="progress progress-small">
                                    <div style="width: 60%;" class="progress-bar progress-bar-success"></div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="task">
                                    <span class="desc">Database migration</span>
                                    <span class="percent">20%</span>
                                </span>
                                <div class="progress progress-small">
                                    <div style="width: 20%;" class="progress-bar progress-bar-warning"></div>
                                </div>
                            </a>
                        </li>
                        <li class="footer">
                            <a href="javascript:void(0);">View all tasks</a>
                        </li>
                    </ul>
                </li>

                <!-- Messages -->
                <li class="dropdown hidden-xs hidden-sm">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-envelope"></i>
                        <span class="badge">1</span>
                    </a>
                    <ul class="dropdown-menu extended notification">
                        <li class="title">
                            <p>You have 3 new messages</p>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="photo"><img src="assets/img/demo/avatar-1.jpg" alt="" /></span>
                                <span class="subject">
                                    <span class="from">Bob Carter</span>
                                    <span class="time">Just Now</span>
                                </span>
                                <span class="text">
                                    Consetetur sadipscing elitr...
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="photo"><img src="assets/img/demo/avatar-2.jpg" alt="" /></span>
                                <span class="subject">
                                    <span class="from">Jane Doe</span>
                                    <span class="time">45 mins</span>
                                </span>
                                <span class="text">
                                    Sed diam nonumy...
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="photo"><img src="assets/img/demo/avatar-3.jpg" alt="" /></span>
                                <span class="subject">
                                    <span class="from">Patrick Nilson</span>
                                    <span class="time">6 hours</span>
                                </span>
                                <span class="text">
                                    No sea takimata sanctus...
                                </span>
                            </a>
                        </li>
                        <li class="footer">
                            <a href="javascript:void(0);">View all messages</a>
                        </li>
                    </ul>
                </li>

                <!-- .row .row-bg Toggler -->
                <li>
                    <a href="#" class="dropdown-toggle row-bg-toggle">
                        <i class="icon-resize-vertical"></i>
                    </a>
                </li>


                <!-- User Login Dropdown -->
                <li class="dropdown user">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <!--<img alt="" src="assets/img/avatar1_small.jpg" />-->
                        <i class="icon-male"></i>
                        <span class="username" id="practiceName"></span>
                        <i class="icon-caret-down small"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="javascript:void(0);"><i class="icon-user"></i> My Profile</a></li>
                        <li><a href="javascript:void(0);"><i class="icon-calendar"></i> My Calendar</a></li>
                        <li><a href="javascript:void(0);"><i class="icon-tasks"></i> My Tasks</a></li>
                        <li class="divider"></li>
                        <li><a href="login.php"><i class="icon-key"></i> Log Out</a></li>
                    </ul>
                </li>
                <!-- /user login dropdown -->
            </ul>
            <!-- /Top Right Menu -->
        </div>
        <!-- /top navigation bar -->

    </header> <!-- /.header -->

    <div id="container">

        <!-- Breadcrumbs line -->
        <div class="crumbs">
            <ul id="breadcrumbs" class="breadcrumb">
                <li class="current">
                    <i class="icon-home"></i>
                    <a href="index.html">Dashboard</a>
                </li>
                
            </ul>
            <a href="javascript:void(0);"><img src="assets/icons/Settings.png" title="Settings" style="float:right; padding-top:0.3em; padding-right:2em;"/></a>
                    <a href="Ledger.php"><img src="assets/icons/Claim_search.png" title="Claim Search" style="float:right; padding-top:0.3em; padding-right:2em;"/></a>
                    <a href="AddClaim.php"><img src="assets/icons/Claim_add.png" title="Add Claim" style="float:right; padding-top:0.3em; padding-right:2em;"/></a>
                    <a href="ViewPatient.php"><img src="assets/icons/Patient_search.png" title="Patient Search" style="float:right; padding-top:0.3em; padding-right:2em;"/></a>
                    <a href="AddPatient.php"><img src="assets/icons/Patient_add.png" title="Add Patient" style="float:right; padding-top:0.3em; padding-right:2em;"/></a>
                    <a href="dashboard.html"><img src="assets/icons/Home.png" title="Home" style="float:right; padding-top:0.3em; padding-right:2em;"/></a>

        </div>
        <div class="clearfix"></div>
        <div class="alert alert-warning fade in" style="margin-top:10px;" id="divAlert">
            <i class="icon-remove close" data-dismiss="alert"></i>
            <strong>Warning!</strong> <span id="alertTop">Your Payment Due is on 05/22/2016</span>
        </div>
        <!-- /Breadcrumbs line -->

        <div id="content">
            
            <h3 style="margin:20px 20px 20px 20px;">Payment</h3>
            <div class="form-group col-md-4" style="margin-bottom:30px;">
                <label class="control-label col-md-12">Select Client</label>
                <div class="col-md-10">
                    <select id="patient" class="form-control">
                        <option value="0"></option>
                        <option value="1">Niyas Khan</option>
                        <option value="3">Jai</option>
                    </select>
                </div>
            </div>
            <div class="form-group col-md-4" style="margin-bottom:50px;">
                <label class="control-label col-md-2"></label>
                <div class="col-md-6">
                    <input type="checkbox" class="checkbox"/> All Clients
                </div>
            </div>
            <div class="form-group col-md-4" style="margin-bottom:50px;">
                <label class="control-label col-md-2"></label>
                <div class="col-md-6">
                    <input type="checkbox" class="checkbox"/> Include Closed
                </div>
            </div>
            <div class="clearfix"></div>

            <div class="form-group col-md-4">
                <span class="col-md-6"><strong>Total Amount</strong></span>
                <span class="col-md-6" id="spanTotAmt"></span>
            </div>

            <div class="form-group col-md-4">
                <span class="col-md-6"><strong>Amount Applied</strong></span>
                <span class="col-md-6" id="spanAmtApp"></span>
            </div>

            <div class="form-group col-md-4">
                <span class="col-md-6"><strong>Amount Remaining</strong></span>
                <span class="col-md-6" id="spanAmtRem"></span>
            </div>
            
            <table class="table table-bordered table table-hover table-striped" style="margin-top:70px;">
                <thead>
                    <th style="background-color:#4583e7; color:#fff; width:3%;">Select</th>
                    <th style="background-color:#4583e7; color:#fff; width:3%;">ID</th>
                    <th style="background-color:#4583e7; color:#fff; width:9%;">DOS</th>
                    <th style="background-color:#4583e7; color:#fff; width:7%;">Code</th>
                    <th style="background-color:#4583e7; color:#fff; width:7%;">Modifier</th>
                    <th style="background-color:#4583e7; color:#fff; width:7%;">Billed</th>
                    <th style="background-color:#4583e7; color:#fff; width:9%;">Allowed</th>
                    <th style="background-color:#4583e7; color:#fff; width:9%;">Paid</th>
                    <th style="background-color:#4583e7; color:#fff; width:9%;">Adjustment</th>
                    <th style="background-color:#4583e7; color:#fff; width:7%;">Balance</th>
                    <th style="background-color:#4583e7; color:#fff; width:9%;">Reason</th>
                    <th style="background-color:#4583e7; color:#fff; width:15%;">Status</th>

                </thead>
                <tbody id="depBody">
                </tbody>
            </table>
            <a href="Deposit.php"><button class="btn btn-primary pull-right">Back to Deposit</button></a>
        </div>
    </div>
<div class="modal fade" id="Status" tabindex="-1">
        <div class="modal-dialog" style="width:30%">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Add New Deposit</h4>
                </div>
                <div class="modal-body" style="min-height:180px;">
                    <div class="form-group col-md-12">
                        <label class="control-label col-md-12">Copay :</label>
                        <div class="col-md-10" id="copay">
                            <input type="text" class="form-control copay" />
                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <label class="control-label col-md-12">Co-Ins :</label>
                        <div class="col-md-10" id="coins">
                            <input type="text" class="form-control coins" />
                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <label class="control-label col-md-12">Deductible :</label>
                        <div class="col-md-10" id="deduc">
                            <input type="text" class="form-control deduc" />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="button" class="btn btn-default" data-dismiss="modal" value="Exit" />
                    <input type="button" id="btnSaveStatus" class="btn btn-primary" data-dismiss="modal" value="Save" />
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
    <link href="plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css" rel='stylesheet' type='text/css'>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="assets/js/alertify.js"></script>

    <script>
    $(document).ready(function(){
        var depID = sessionStorage.getItem("depID");
        var depAmt = sessionStorage.getItem("depAmt");
        var depPayor = sessionStorage.getItem("depPayor");
        $.post("http://curismed.com/medService/insurances/listpatients",
            {
                insuranceID: depPayor,
            },
            function(data, status){
                var arrDep = [];
                    $('#patient').html('');
                    for(var x in data){
                      arrDep.push(data[x]);
                    }
                    //alert(arrCase);
                    $.each(arrDep,function(i,v) {
                        $('#patient').html('');
                        $('#patient').append('<option value=""></option>');
                        arrDep.forEach(function(t) { 
                            $('#patient').append('<option value="'+t.patientID+'">'+t.fullName+'</option>');
                        });
                    });
            });
        $('#divAlert').hide();
        var temp = sessionStorage.getItem("patientId");
        document.getElementById('spanTotAmt').innerHTML = depAmt+'.00';
        $.post("http://curismed.com/medService/deposits/balance",
            {
                depositID: depID,
            },
            function(data, status){
                document.getElementById('spanAmtRem').innerHTML = data+'.00';
                document.getElementById('spanAmtApp').innerHTML = document.getElementById('spanTotAmt').innerHTML - document.getElementById('spanAmtRem').innerHTML+'.00';
            });
        $('#patient').change(function(){
            var currVal = $(this).val();
            $.ajax({
                  type: "POST",
                  url:"getAlerts.php",
                  async : false,
                  data:{
                    alert_patientID : currVal
                  },success:function(result){
                    var res = JSON.parse(result);
                    if(res!=""){
                      if(res[0].alerts_Deposits == "1"){
                          $('#divAlert').show();
                          document.getElementById("alertTop").innerHTML = res[0].alert_desc;
                      }
                      else{
                        $('#divAlert').hide();
                      }
                    }
                }
              });
            $.post("http://curismed.com/medService/claims/ledger",
            {
                patientID: currVal,
            },
            function(data, status){
                var arrDep = [];
                    $('#depBody').html('');
                    for(var x in data){
                      arrDep.push(data[x]);
                    }
                    //alert(arrCase);
                    $.each(arrDep,function(i,v) {
                        $('#depBody').html('');
                        arrDep.forEach(function(t) { 
                             if(t.isPosted == '1'){
                                clsName = "background-color:red";
                             }
                             else{
                                clsName = "background-color:white";
                             }
                             if(t.claimBalance == ""){
                                balanceValue = parseInt(t.total).toFixed(2);
                             }
                             else{
                                balanceValue = parseInt(t.claimBalance).toFixed(2);
                             }

                             var billed = parseInt(t.total);


                             if((t.allowed == "" && t.adjustment == "" && t.paid == "") ||( t.allowed == "0.00" && t.adjustment == "0.00" && t.paid == "0.00")){
                                $('#depBody').append('<tr><td><input type="checkbox" class="uniform" value=""></td><td style="'+clsName+'"></td><td>'+changeDateFormat(t.fromDt)+'</td><td>'+t.proced+'</td><td>'+t.mod1+'</td><td id="txtTot'+t.claimID+'">'+billed.toFixed(2)+'</td><td><input type="text" class="form-control textbox2"  id="txtDepAllowed'+t.claimID+'" style="width:70px; border-radius:3px;" value="'+t.allowed+'"/></td><td><input type="text" class="form-control textbox1"  id="txtDepPayment'+t.claimID+'" style="width:70px; border-radius:3px;" value="'+t.paid+'"/></td><td><input type="text" class="form-control textbox3"  id="txtDepAdjust'+t.claimID+'" style="width:70px; border-radius:3px;" value="'+t.adjustment+'"/></td><td id="txtBalance'+t.claimID+'">'+balanceValue+'</td><td><select id="ddlreason'+t.claimID+'" class="form-control" style="width:100px; border-radius:3px;"><option>Contractual Adj</option><option>Exceeded Authorized Units</option><option>Paid by RC</option><option>Billed in Error</option><option>Rebilled Corrected Claim</option><option>Can not bill separately</option><option>Duplicate claim</option></select></td><td><select id="ddlStatus'+t.claimID+'" class="form-control" style="width:100px; border-radius:3px; float:left;"><option></option><option value="1">Patient Responsibility</option><option value="2">Bill to Next Responsibility</option><option value="3">Closed</option></select>&nbsp;<a href="javascript:void(0);" id="statusID'+t.claimID+'" class="statusLink" style="float:left; margin:5px 0 0 5px;"><i class="icon icon-plus"></i></a><input type="button" class="btn btn-xs btn-primary btnSave" style="margin:0px 0 0 10px;" id="btnSaveDep'+t.claimID+'" value="Save" /></td></tr>');
                             }
                             else{
                                $('#depBody').append('<tr><td><input type="checkbox" class="uniform" value=""></td><td style="'+clsName+'"></td><td>'+changeDateFormat(t.fromDt)+'</td><td>'+t.proced+'</td><td>'+t.mod1+'</td><td id="txtTot'+t.claimID+'">'+billed.toFixed(2)+'</td><td><input type="text" class="form-control textbox2" disabled id="txtDepAllowed'+t.claimID+'" style="width:70px; border-radius:3px;" value="'+t.allowed+'"/></td><td><input type="text" class="form-control textbox1" disabled id="txtDepPayment'+t.claimID+'" style="width:70px; border-radius:3px;" value="'+t.paid+'"/></td><td><input type="text" class="form-control textbox3" disabled id="txtDepAdjust'+t.claimID+'" style="width:70px; border-radius:3px;" value="'+t.adjustment+'"/></td><td id="txtBalance'+t.claimID+'">'+balanceValue+'</td><td><select id="ddlreason'+t.claimID+'" class="form-control" style="width:100px; border-radius:3px;"><option>Contractual Adj</option><option>Exceeded Authorized Units</option><option>Paid by RC</option><option>Billed in Error</option><option>Rebilled Corrected Claim</option><option>Can not bill separately</option><option>Duplicate claim</option></select></td><td><select id="ddlStatus'+t.claimID+'" class="form-control" style="width:100px; border-radius:3px; float:left;"><option></option><option value="1">Patient Responsibility</option><option value="2">Bill to Next Responsibility</option><option value="3">Closed</option></select>&nbsp;<a href="javascript:void(0);" id="statusID'+t.claimID+'" class="statusLink" style="float:left; margin:5px 0 0 5px;"><i class="icon icon-plus"></i></a><input type="button" class="btn btn-xs btn-primary btnSave" style="margin:0px 0 0 10px;" id="btnSaveDep'+t.claimID+'" value="Save" /></td></tr>');
                            }

                            var status = "ddlStatus"+t.claimID;
                            if(t.status != ""){
                                document.getElementById(status).value = t.status;
                            }
                            $.ajax({
                                  type: "POST",
                                  url:"checkSecPolicy.php",
                                  async : false,
                                  data:{
                                    caseID : t.caseID
                                  },success:function(result){
                                    var status = "#ddlStatus"+t.claimID;
                                    if(result == "Secondary"){
                                        $(status).val("2");
                                    }
                                    else
                                    {
                                        $(status).val("1");
                                    }
                                  }
                              });
                            
                        });
                    });
            });
        });
    });

        $(document).on('click','.btnSave',function() {
            var depID = sessionStorage.getItem("depID");
            var id = $(this).attr('id');
            var final = id.replace("btnSaveDep","");
            var copayVal = "txtcopay"+final;
            var deducVal = "txtdeduc"+final;
            var coinsVal = "txtcoins"+final;
            var allowed = "txtDepAllowed"+final;
            var paid = "txtDepPayment"+final;
            var adjust = "txtDepAdjust"+final;
            var bal = "txtBalance"+final;
            var reason = "ddlreason"+final;
            var status = "ddlStatus"+final;
            var copay = document.getElementById(copayVal).value;
            var deduc = document.getElementById(deducVal).value;
            var coins = document.getElementById(coinsVal).value;
            var allowedVal = document.getElementById(allowed).value;
            var paidVal = document.getElementById(paid).value;
            var adjustVal = document.getElementById(adjust).value;
            var balVal = document.getElementById(bal).innerHTML;
            var reasonVal = document.getElementById(reason).value;
            var statusVal = document.getElementById(status).value;
            if(statusVal =="1"){
                $.ajax({
                    type: "POST",
                    url:"http://curismed.com/medService/deposits/adjust",
                    data:{ 
                        "isPosted" : "1",
                        "depositID" : depID,
                        "claimID" : final,
                        "patientID" : "-1",
                        "insuranceID" : "-100",
                        "amountAdjusted" : paidVal,
                        "copay" : copay,
                        "deductible" : deduc,
                        "coins" : coins,
                        "allowed" : allowedVal,
                        "paid" : paidVal,
                        "adjustment" : adjustVal,
                        "claimBalance" : balVal,
                        "reason" : reasonVal,
                        "status" : statusVal
                        },success:function(result){
                            window.location.href = "Deposit.php";
                         }

                });
            }
        });
      
        $(document).on('click','.statusLink',function() {
             $("#Status").modal('show');
             var ID = $(this).attr('id');
             StatusModal(ID);
        });
    $(document).on('focusout','.textbox1',function() {
        var tempValue1 = $(this).val();
        var newVal = parseInt(tempValue1).toFixed(2);
        $(this).val(newVal);
        var tempID = $(this).attr('id');
        sessionStorage.setItem("txtboxPaid",tempValue1)
        FocOut(tempValue1,tempID);
    });
    $(document).on('focusout','.textbox2',function() {
        var tempValue1 = $(this).val();
        var newVal = parseInt(tempValue1).toFixed(2);
        $(this).val(newVal);
    });
    $(document).on('focusin','.textbox1',function() {
        var tempValue1 = $(this).val();
        var tempID = $(this).attr('id');
        var resStr = tempID.replace('txtDepPayment','');
    var tempid = 'txtBalance'+resStr;
        var tempTot = 'txtTot'+resStr;
        if(tempValue1 != ""){
            var tempBal = document.getElementById(tempid).innerHTML;
            var tempTotal = document.getElementById(tempTot).innerHTML;
            var befAmtApp = document.getElementById('spanAmtApp').innerHTML;
            var befAmtRem = document.getElementById('spanAmtRem').innerHTML;
            var AmtApplied = parseFloat(befAmtApp);
            var AmtRemain = parseFloat(befAmtRem);
            // var interApplied = AmtApplied - tempValue1;
            // var interRemain = AmtRemain + tempValue1;
            var FinalBal = tempTotal ;
            var FinalApp = AmtApplied - parseFloat(tempValue1);
            var FinalRem = AmtRemain + parseFloat(tempValue1);
            if(FinalApp % 1 != 0){
                document.getElementById('spanAmtApp').innerHTML = FinalApp;
                document.getElementById(tempid).innerHTML = "";
            }
            else{
                document.getElementById('spanAmtApp').innerHTML = FinalApp+'.00';
                document.getElementById(tempid).innerHTML = "";
            }
            if(FinalRem % 1 != 0){
                document.getElementById('spanAmtRem').innerHTML = FinalRem;
                document.getElementById(tempid).innerHTML = "";
            }
            else{
                document.getElementById('spanAmtRem').innerHTML = FinalRem+'.00';
                document.getElementById(tempid).innerHTML = "";
            }
        }

    });

    function StatusModal(ID){
        var fin = ID.replace("statusID","");
        $("#copay").html('<input type="text" id="txtcopay'+fin+'" class="form-control" value="" />');
        $("#coins").html('<input type="text" id="txtcoins'+fin+'" class="form-control" value="" />');
        $("#deduc").html('<input type="text" id="txtdeduc'+fin+'" class="form-control" value="" />');
        $.ajax({
          type: "POST",
          url:"generateStatus.php",
          async : false,
          data:{
            claimID : fin
          },success:function(result){
            var t = JSON.parse(result);
            var copayVal = "#txtcopay"+t[0].claimID;
            var deducVal = "#txtdeduc"+t[0].claimID;
            var coinsVal = "#txtcoins"+t[0].claimID;

            if(t.copay != ""){
                $(copayVal).val(t[0].copay);
            }
            else
            {
                $(copayVal).val(t[0].copay);
            }
            if(t.deductible != ""){
                $(deducVal).val(t[0].deductible);
            }
            else{
                $(deducVal).val(t[0].deductible);
            }
            if(t.coins != ""){
                $(coinsVal).val(t[0].coins);
            }
            else{
                $(coinsVal).val(t[0].coins);
            }
          }
        });
    }
    function FocOut(a,b){
            var tempVal = a;
            var resStr = b.replace('txtDepPayment','');
            var tempid = 'txtBalance'+resStr;
            var tempStatus = '#ddlStatus'+resStr;
            var tempTot = 'txtTot'+resStr;
            var tempAll = 'txtDepAllowed'+resStr;
            var tempAdj = 'txtDepAdjust'+resStr;
            var tempCopay = 'txtcopay'+resStr;
            var tempCoins = 'txtcoins'+resStr;
            var tempCaseID = "";
            var tempSessiom = sessionStorage.getItem("txtboxPaid");
            $.ajax({
                  type: "POST",
                  url:"getCaseID.php",
                  async : false,
                  data:{
                    claimID : resStr
                  },success:function(result){
                    tempCaseID = result;
                  }
              });
            if(a != tempSessiom){
                var IsDec = tempVal.indexOf(".");
                if(tempVal != ""){
                    if(IsDec == '0' || IsDec == '-1'){
                        var decVal = tempVal+'.00';
                        $(this).val(decVal);
                        var tempBal = document.getElementById(tempTot).innerHTML;
                        var tempAll = document.getElementById(tempAll).value;
                        var befAmtApp = document.getElementById('spanAmtApp').innerHTML;
                        var befAmtRem = document.getElementById('spanAmtRem').innerHTML;
                        var AmtBal = parseFloat(tempBal);
                        var AmtApplied = parseFloat(befAmtApp);
                        var AmtRemain = parseFloat(befAmtRem);
                        var Adjust = AmtBal - tempAll;
                        var Balance = tempAll - tempVal;
                        var interApplied = AmtApplied - a;
                        var interRemain = AmtRemain - a;
                        var FinalApp = interApplied + parseFloat(decVal);
                        var FinalRem = interRemain - parseFloat(decVal);
                        //alert(tempBal);
                        if(FinalApp % 1 != 0){
                            document.getElementById('spanAmtApp').innerHTML = FinalApp;
                            document.getElementById(tempid).innerHTML = Balance;
                            document.getElementById(tempAdj).value = Adjust.toFixed(2);
                        }
                        else{
                            document.getElementById('spanAmtApp').innerHTML = FinalApp+'.00';
                            document.getElementById(tempid).innerHTML = Balance+'.00';
                            document.getElementById(tempAdj).value = Adjust.toFixed(2);
                        }
                        if(FinalRem % 1 != 0){
                            document.getElementById('spanAmtRem').innerHTML = FinalRem.toFixed(2);
                            document.getElementById(tempid).innerHTML = Balance;
                            document.getElementById(tempAdj).value = Adjust.toFixed(2);
                        }
                        else{
                            document.getElementById('spanAmtRem').innerHTML = FinalRem+'.00';
                            document.getElementById(tempid).innerHTML = Balance+'.00';
                            document.getElementById(tempAdj).value = Adjust.toFixed(2);
                        }
                    }
                    else{
                        $(this).val(tempVal);
                    }
                }
            }
            else{
                var IsDec = tempVal.indexOf(".");
                if(tempVal != ""){
                    if(IsDec == '0' || IsDec == '-1'){
                        var decVal = tempVal+'.00';
                        $(this).val(decVal);
                        var tempBal = document.getElementById(tempTot).innerHTML;
                        var befAmtApp = document.getElementById('spanAmtApp').innerHTML;
                        var befAmtRem = document.getElementById('spanAmtRem').innerHTML;
                        var AmtBal = parseFloat(tempBal);
                        var tempAll = document.getElementById(tempAll).value;
                        var Adjust = AmtBal - tempAll;
                        var Balance = tempAll - tempVal;
                        var AmtApplied = parseFloat(befAmtApp);
                        var AmtRemain = parseFloat(befAmtRem);
                        var FinalApp = AmtApplied + parseFloat(decVal);
                        var FinalRem = AmtRemain - parseFloat(decVal);
                        //alert(Balance);
                        if(Balance == 0){
                            $(tempStatus).val("3");
                        }
                        else{
                            $.ajax({
                                  type: "POST",
                                  url:"checkSecPolicy.php",
                                  async : false,
                                  data:{
                                    caseID : tempCaseID
                                  },success:function(result){
                                    if(result == "Secondary"){
                                        $(tempStatus).val("2");
                                    }
                                    else
                                    {
                                        $(tempStatus).val("1");
                                    }
                                  }
                              });
                        }
                        if(FinalApp % 1 != 0){
                            document.getElementById('spanAmtApp').innerHTML = FinalApp;
                            document.getElementById(tempid).innerHTML = Balance;
                            document.getElementById(tempAdj).value = Adjust.toFixed(2);
                        }
                        else{
                            document.getElementById('spanAmtApp').innerHTML = FinalApp+'.00';
                            document.getElementById(tempid).innerHTML = Balance+'.00';
                            document.getElementById(tempAdj).value = Adjust.toFixed(2);
                        }
                        if(FinalRem % 1 != 0){
                            document.getElementById('spanAmtRem').innerHTML = FinalRem.toFixed(2);
                            document.getElementById(tempid).innerHTML = Balance;
                            document.getElementById(tempAdj).value = Adjust.toFixed(2);
                        }
                        else{
                            document.getElementById('spanAmtRem').innerHTML = FinalRem +'.00';
                            document.getElementById(tempid).innerHTML = Balance+'.00';
                            document.getElementById(tempAdj).value = Adjust.toFixed(2);
                        }
                        $("#Status").modal('show');
                        StatusModal(resStr);
                        if(Balance % 1 != 0){
                            document.getElementById(tempCoins).value = Balance;
                        }
                        else{
                            console.log(tempCopay+".value = "+Balance+"");
                            document.getElementById(tempCopay).value = Balance;
                        }
                    }
                    else{
                        $(this).val(tempVal);
                        var tempBal = document.getElementById(tempTot).innerHTML;
                        var befAmtApp = document.getElementById('spanAmtApp').innerHTML;
                        var befAmtRem = document.getElementById('spanAmtRem').innerHTML;
                        var AmtBal = parseFloat(tempBal);
                        var tempAll = document.getElementById(tempAll).value;
                        var Adjust = AmtBal - tempAll;
                        var Balance = tempAll - tempVal;
                        var AmtApplied = parseFloat(befAmtApp);
                        var AmtRemain = parseFloat(befAmtRem);
                        var FinalApp = AmtApplied + parseFloat(tempVal);
                        var FinalRem = AmtRemain - parseFloat(tempVal);
                        //alert(Balance);
                        if(Balance == 0){
                            $(tempStatus).val("3");
                        }
                        else{
                            $.ajax({
                                  type: "POST",
                                  url:"checkSecPolicy.php",
                                  async : false,
                                  data:{
                                    caseID : tempCaseID
                                  },success:function(result){
                                    if(result == "Secondary"){
                                        $(tempStatus).val("2");
                                        $.ajax({
                                              type: "POST",
                                              url:"updateInsurance.php",
                                              async : false,
                                              data:{
                                                caseID : tempCaseID
                                              },success:function(result){
                                              }
                                          });
                                    }
                                    else
                                    {
                                        $(tempStatus).val("1");

                                    }
                                  }
                              });
                        }
                        if(FinalApp % 1 != 0){
                            document.getElementById('spanAmtApp').innerHTML = FinalApp;
                            document.getElementById(tempid).innerHTML = Balance;
                            document.getElementById(tempAdj).value = Adjust.toFixed(2);
                        }
                        else{
                            document.getElementById('spanAmtApp').innerHTML = FinalApp+'.00';
                            document.getElementById(tempid).innerHTML = Balance+'.00';
                            document.getElementById(tempAdj).value = Adjust.toFixed(2);
                        }
                        if(FinalRem % 1 != 0){
                            document.getElementById('spanAmtRem').innerHTML = FinalRem.toFixed(2);
                            document.getElementById(tempid).innerHTML = Balance;
                            document.getElementById(tempAdj).value = Adjust.toFixed(2);
                        }
                        else{
                            document.getElementById('spanAmtRem').innerHTML = FinalRem +'.00';
                            document.getElementById(tempid).innerHTML = Balance+'.00';
                            document.getElementById(tempAdj).value = Adjust.toFixed(2);
                        }
                        $("#Status").modal('show');
                        StatusModal(resStr);
                        if(Balance % 1 != 0){
                            document.getElementById(tempCoins).value = Balance;
                        }
                        else{
                            console.log(tempCopay+".value = "+Balance+"");
                            document.getElementById(tempCopay).value = Balance;
                        }
                    }
                }
            }
            
    }
    function changeDateFormat(inputDate){  // expects Y-m-d
        var splitDate = inputDate.split('-');
        if(splitDate.count == 0){
            return null;
        }

        var year = splitDate[0];
        var month = splitDate[1];
        var day = splitDate[2]; 

        return month + '-' + day + '-' + year;
    }
    </script>
</body>
</html>