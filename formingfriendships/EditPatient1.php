<?php
 session_start();

 if (!isset($_SESSION['userID'])) {
    header("Location: index.html");
 }
 ?>
 <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <title>Edit Client | AMROMED LLC</title>

    <!--=== CSS ===-->

    <!-- Bootstrap -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

    <!-- jQuery UI -->
    <!--<link href="plugins/jquery-ui/jquery-ui-1.10.2.custom.css" rel="stylesheet" type="text/css" />-->
    <!--[if lt IE 9]>
        <link rel="stylesheet" type="text/css" href="plugins/jquery-ui/jquery.ui.1.10.2.ie.css"/>
    <![endif]-->

    <!-- Theme -->
    <link href="assets/css/main.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/plugins.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/alertify.css" rel='stylesheet' type='text/css'>
    <link href="assets/css/themes/default.css" rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="assets/css/fontawesome/font-awesome.min.css">
    <!--[if IE 7]>
        <link rel="stylesheet" href="assets/css/fontawesome/font-awesome-ie7.min.css">
    <![endif]-->

    <!--[if IE 8]>
        <link href="assets/css/ie8.css" rel="stylesheet" type="text/css" />
    <![endif]-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>

    <!--=== JavaScript ===-->

    <script type="text/javascript" src="assets/js/libs/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>
    <script type="text/javascript" src="plugins/bootstrap-switch/bootstrap-switch.min.js"></script>

    <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/js/libs/lodash.compat.min.js"></script>

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
        <script src="assets/js/libs/html5shiv.js"></script>
    <![endif]-->

    <!-- Smartphone Touch Events -->
    <script type="text/javascript" src="plugins/touchpunch/jquery.ui.touch-punch.min.js"></script>
    <script type="text/javascript" src="plugins/event.swipe/jquery.event.move.js"></script>
    <script type="text/javascript" src="plugins/event.swipe/jquery.event.swipe.js"></script>

    <!-- General -->
    <script type="text/javascript" src="assets/js/libs/breakpoints.js"></script>
    <script type="text/javascript" src="plugins/respond/respond.min.js"></script> <!-- Polyfill for min/max-width CSS3 Media Queries (only for IE8) -->
    <script type="text/javascript" src="plugins/cookie/jquery.cookie.min.js"></script>
    <script type="text/javascript" src="plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script type="text/javascript" src="plugins/slimscroll/jquery.slimscroll.horizontal.min.js"></script>

    <!-- Page specific plugins -->
    <!-- Charts -->
    <!--[if lt IE 9]>
        <script type="text/javascript" src="plugins/flot/excanvas.min.js"></script>
    <![endif]-->
    <script type="text/javascript" src="plugins/sparkline/jquery.sparkline.min.js"></script>
    <script type="text/javascript" src="plugins/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>

    <script type="text/javascript" src="plugins/daterangepicker/moment.min.js"></script>
    <script type="text/javascript" src="plugins/daterangepicker/daterangepicker.js"></script>
    <script type="text/javascript" src="plugins/blockui/jquery.blockUI.min.js"></script>

    <script type="text/javascript" src="plugins/fullcalendar/fullcalendar.min.js"></script>

    <!-- Noty -->
    <script type="text/javascript" src="plugins/noty/jquery.noty.js"></script>
    <script type="text/javascript" src="plugins/noty/layouts/top.js"></script>
    <script type="text/javascript" src="plugins/noty/themes/default.js"></script>

    <!-- Forms -->
    <script type="text/javascript" src="assets/js/jquery.mask.min.js"></script>
    <script type="text/javascript" src="plugins/uniform/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="plugins/select2/select2.min.js"></script>

    <!-- App -->
    <script type="text/javascript" src="assets/js/app.js"></script>
    <script type="text/javascript" src="assets/js/plugins.js"></script>
    <script type="text/javascript" src="assets/js/plugins.form-components.js"></script>
    <link rel="stylesheet" href="assets/css/select2.css">
    <script type="text/javascript" src="assets/js/select2.js"></script>

    <script type="text/javascript" src="assets/js/alertify.min.js"></script>
    <script src="assets/js/jquery.datetimepicker.js"></script>
    <link href="assets/css/jquery.datetimepicker.css" rel="stylesheet" />
    <link href="plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css" rel='stylesheet' type='text/css'>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
    
    <script>
    function changeDateFormat(inputDate){  // expects Y-m-d
        var splitDate = inputDate.split('-');
        if(splitDate.count == 0){
            return null;
        }

        var year = splitDate[0];
        var month = splitDate[1];
        var day = splitDate[2].substr(0,2); 

        return month + '-' + day + '-' + year;
    }
    $(document).ready(function(){
        "use strict";

        App.init(); // Init layout and core plugins
        Plugins.init(); // Init all plugins
        FormComponents.init(); // Init all form-specific plugins
        //alert(sessionStorage.getItem("patientId"));
        document.getElementById('hdntoggleDX').value = "false";
        document.getElementById('practiceName').innerHTML = sessionStorage.getItem('practiceName');
        var temp = sessionStorage.getItem("patientId");

        // $('#icdtoggle').on('change', function(event, state) {
        //   document.getElementById('hdntoggleDX').value = event.target.checked; // jQuery event
        // });
        $("#divPerTime").hide();

        $("#linkUpload").click(function(){
          window.location.href="upload.php";
        });

        $("#createClaim").click(function(){
          sessionStorage.setItem(" PatID",temp);
          window.location.href ="AddClaim.php";
        });

        $("#linkEditAuth").click(function(){
          $('#EditAuthModal').modal('show');
          var tempAuth = $("#EditauthNo").val();
          document.getElementById("_hdnAuthID").value = tempAuth;
          $.ajax({
              async : false,
              type: "POST",
              url:"getAuthinfo.php",
              data:{
                  "authNo" : tempAuth
                  },success:function(result){
                    
                  }
              });
        });

        $("#linkAddAuth").click(function(){
          $('#AddAuthModal').modal('show');
        });

        $('#ActPer').change(function(){
            var val = $(this).val();
            if(val == "Unit"){
                $("#divPerTime").show();
            }
            else{
                $("#divPerTime").hide();
            }
        });

        $('#ActMaxi1').change(function(){
                if($(this).val() == "Amount"){
                    $('#ActPer1').html("");
                    $('#ActPer1').append('<option value="Week">Week</option><option value="Month">Month</option><option value="Total Auth">Total Auth</option>');
                }
                else if($(this).val() == "Sessions"){
                    $('#ActPer1').html("");
                    $('#ActPer1').append('<option value="Total Auth">Total Auth</option>');
                }
                else{
                    $('#ActPer1').html("");
                    $('#ActPer1').append('<option value="Day">Day</option><option value="Week">Week</option><option value="Month">Month</option><option value="Total Auth">Total Auth</option>');
                }
            });
            $('#ActMaxi2').change(function(){
                if($(this).val() == "Amount"){
                    $('#ActPer2').html("");
                    $('#ActPer2').append('<option value="Week">Week</option><option value="Month">Month</option><option value="Total Auth">Total Auth</option>');
                }
                else if($(this).val() == "Sessions"){
                    $('#ActPer2').html("");
                    $('#ActPer2').append('<option value="Total Auth">Total Auth</option>');
                }
                else{
                    $('#ActPer2').html("");
                    $('#ActPer2').append('<option value="Day">Day</option><option value="Week">Week</option><option value="Month">Month</option><option value="Total Auth">Total Auth</option>');
                }
            });
            $('#ActMaxi3').change(function(){
                if($(this).val() == "Amount"){
                    $('#ActPer3').html("");
                    $('#ActPer3').append('<option value="Week">Week</option><option value="Month">Month</option><option value="Total Auth">Total Auth</option>');
                }
                else if($(this).val() == "Sessions"){
                    $('#ActPer3').html("");
                    $('#ActPer3').append('<option value="Total Auth">Total Auth</option>');
                }
                else{
                    $('#ActPer3').html("");
                    $('#ActPer3').append('<option value="Day">Day</option><option value="Week">Week</option><option value="Month">Month</option><option value="Total Auth">Total Auth</option>');
                }
            });


        $("#btntempAuth").click(function(){
          var caseChartNo = document.getElementById('ddlcaseList').value;
          $.ajax({
              async : false,
              type: "POST",
              url:"getCaseIDbyChart.php",
              data:{
                  "caseChartNo" : caseChartNo
                  },success:function(result){
                    var caseID = result;
                    var patientID = temp;
                    var authNo = document.getElementById('tempAuth').value;
                    var newtos = document.getElementById('newtos').value;
                    var startDt = convertDate(document.getElementById('tempAuthstartDt').value);
                    var endDt = convertDate(document.getElementById('tempAuthendDt').value);
                    var totalVisits = document.getElementById('temptotalVisits').value;
                    var totAuthType = document.getElementById('temptotAuthType').value;
                    $.ajax({
                        async : false,
                        type: "POST",
                        url:"addAuth.php",
                        data:{
                            "caseID" : caseID,
                            "patientID" : patientID,
                            "authNo" : authNo,
                            "tos" : newtos,
                            "startDt" : startDt,
                            "endDt" : endDt,
                            "totAuthType" : totAuthType,
                            "totalVisits" : totalVisits,
                            "totalRemaining" : totalVisits
                            },success:function(result){
                              alertify.success("New Auth # added successfully");
                              window.location.reload();
                        }
                    });
              }
          });
        });

        $('#IsFinancialResp').change(function(){
            if($('#IsFinancialResp').is(':checked') == true){
                $('#guarantorModal').modal('show');
            }
            else{
                $('#guarantorModal').modal('hide');
            }
        })

        $('#EditauthNo').change(function(){
          authDetails();
        });

        $('#ddlAuth').change(function(){
          getActivityData();
        });

        $('#btnOpenAct').hide();

        $('#btnOpenAct').click(function(){
          $("#modalActivity").modal("show");
          document.getElementById("hdnFlagAct").value = "0";
          $("#ActTitleMod").html("Create Activity");
          $("#btnAddActi").val("Add Activity");
          $('#Activity').prop('selectedIndex', -1);
          var auth = $('#ddlAuth').val();
          $.ajax({
            type: "POST",
            url:"chkTOS.php",
            async : false,
            data:{
              auth : auth
            },success:function(result){
              if(result =="Behaviour Therapy"){
                $("#Activity").html('');
                $("#Activity").append('<option value=""></option><option value="Initial Assessment By BCBA">Initial Assessment By BCBA</option><option value="Follow up Assessment By BCBA">Follow up Assessment By BCBA</option><option value="Direct Behavior Therapy By BCBA">Direct Behavior Therapy By BCBA</option><option value="Direct Behavior Therapy By BcABA">Direct Behavior Therapy By BcABA</option><option value="Direct Behavior Therapy By Para">Direct Behavior Therapy By Para</option><option value="Supervision By BCBA">Supervision By BCBA</option><option value="Social Skills Group By Para">Social Skills Group By Para</option><option value="Social Skills Group By BCBA">Social Skills Group By BCBA</option><option value="Parent Training By BCBA">Parent Training By BCBA</option><option value="Family Group Training By BCBA">Family Group Training By BCBA</option>');
                $("#ActCPT").html('');
                $("#ActCPT").append('<option value=""></option><option value="0359T">0359T</option><option value="0360T">0360T</option><option value="0361T">0361T</option><option value="0360T-0361T">0360T-0361T</option><option value="0364T">0364T</option><option value="0365T">0365T</option><option value="0364T-0365T">0364T-0365T</option><option value="0368T">0368T</option><option value="0369T">0369T</option><option value="0368T-0369T">0368T-0369T</option><option value="0366T">0366T</option><option value="0367T">0367T</option><option value="0366T-0367T">0366T-0367T</option><option value="0370T">0370T</option><option value="0371T">0371T</option><option value="0372T">0372T</option>')
              }
              else if(result =="Speech Therapy"){
                $("#Activity").html('');
                $("#Activity").append('<option value=""></option><option value="Evaluation By Speech Therapist">Evaluation By Speech Therapist</option><option value="Continued Services">Continued Services</option>');
                $("#ActCPT").html('');
                $("#ActCPT").append('<option value=""></option><option value="92513">92513</option><option value="92507">92507</option>')
              }
              else{
                $("#Activity").html('');
                $("#Activity").append('<option value=""></option><option value="Assessment">Assessment</option><option value="Direct Behavior Theraphy">Direct Behavior Theraphy</option><option value="Supervision">Supervision</option><option value="Supervision Parent Training">Supervision Parent Training</option><option value="Report Writing Send to Editor">Report Writing Send to Editor</option><option value="Report Writing Send to Insurance">Report Writing Send to Insurance</option><option value="Report Writing Progress Reports">Report Writing Progress Reports</option>');
                $("#ActCPT").html('');
                $("#ActCPT").append('<option value=""></option><option value="0359T">0359T</option><option value="0360T">0360T</option><option value="0361T">0361T</option><option value="0364T">0364T</option><option value="0365T">0365T</option><option value="0368T">0368T</option><option value="0369T">0369T</option><option value="0370T">0370T</option>')
              }
            }
          });
          $("#ActCPT").val("");
          $("#ActMod1").val("");
          $("#ActMod2").val("");
          $("#ActMod3").val("");
          $("#ActMod4").val("");
          $("#ActPer").val("");
          $("#ActPerTime").val("");
          $("#ActRate").val("");
          $("#ActMaxi1").val("");
          $("#ActMaxi2").val("");
          $("#ActMaxi3").val("");
          $("#ActPer1").val("");
          $("#ActPer2").val("");
          $("#ActPer3").val("");
          $("#ActIs1").val("");
          $("#ActIs2").val("");
          $("#ActIs3").val("");
        });
        

        $(document).on('click','.docSave',function() {
          var temp = $(this).attr('class').split(' ')[0];
          var newTemp = temp.replace('saveDoc','');
          var DocID = newTemp;
          var statusVal = "ddlDocStatus"+newTemp;
          var status = document.getElementById(statusVal).value;
          $.ajax({
            type: "POST",
            url:"updateDoc.php",
            async : false,
            data:{
              docID : DocID,
              status : status
            },success:function(result){
              window.location.reload();
            }
          });
        });

        $('#btnAddActi').click(function(){
          var flag = document.getElementById("hdnFlagAct").value;
          if(flag == "0"){
            var authNo = $("#ddlAuth").val();
            var caseVal = $("#ddlActcaseList").val();
            $.ajax({
                type: "POST",
                url:"getCaseIDbyChart.php",
                async: false,
                data:{
                    "caseChartNo" : caseVal,
                },success:function(result){
                  document.getElementById("hdncaseID").value = result;
                }
            });
            var caseID = document.getElementById("hdncaseID").value;
            var activityName = $("#Activity").val();
            var activityCPT = $("#ActCPT").val();
            var activityMod1 = $("#ActMod1").val();
            var activityMod2 = $("#ActMod2").val();
            var activityMod3 = $("#ActMod3").val();
            var activityMod4 = $("#ActMod4").val();
            var activityBilledPer = $("#ActPer").val();
            var activityBilledPerTime = $("#ActPerTime").val();
            var activityRates = $("#ActRate").val();
            var activityMax1 = $("#ActMaxi1").val();
            var activityMax2 = $("#ActMaxi2").val();
            var activityMax3 = $("#ActMaxi3").val();
            var activityPer1 = $("#ActPer1").val();
            var activityPer2 = $("#ActPer2").val();
            var activityPer3 = $("#ActPer3").val();
            var activityIs1 = $("#ActIs1").val();
            var activityIs2 = $("#ActIs2").val();
            var activityIs3 = $("#ActIs3").val();
            $.ajax({
                type: "POST",
                url:"addActivity.php",
                async: false,
                data:{
                    "authNo" : authNo,
                    "caseID" : caseID,
                    "activityName" : activityName,
                    "activityCPT" : activityCPT,
                    "activityMod1" : activityMod1,
                    "activityMod2" : activityMod2,
                    "activityMod3" : activityMod3,
                    "activityMod4" : activityMod4,
                    "activityBilledPer" : activityBilledPer,
                    "activityBilledPerTime" : activityBilledPerTime,
                    "activityRates" : activityRates,
                    "activityMax1" : activityMax1,
                    "activityMax2" : activityMax2,
                    "activityMax3" : activityMax3,
                    "activityPer1" : activityPer1,
                    "activityPer2" : activityPer2,
                    "activityPer3" : activityPer3,
                    "activityIs1" : activityIs1,
                    "activityIs2" : activityIs2,
                    "activityIs3" : activityIs3,
                },success:function(result){
                    alertify.success("Activity added successfully");
                    $("#ActMod1").val("");
                    $("#ActRate").val("");
                    $("#ActIs1").val("");
                    getActivityData();
                    $("#modalActivity").modal("hide")
                    //window.location.href = "ViewPatient.php";
                }
            });
          }
          else{

          }
        });

        $(document).on('click','.act',function() {
          var temp = $(this).attr('class').split(' ')[1];
          var newTemp = temp.replace('act','');
          var ActID = newTemp;
          $("#modalActivity").modal("show");
          $.ajax({
            type: "POST",
            url:"getActData.php",
            async : false,
            data:{
              actID : ActID,
            },success:function(result){
              var res = JSON.parse(result);
              var auth = $('#ddlAuth').val();
              $.ajax({
                type: "POST",
                url:"chkTOS.php",
                async : false,
                data:{
                  auth : auth
                },success:function(result){
                  if(result =="Behaviour Therapy"){
                    $("#Activity").html('');
                    $("#Activity").append('<option value=""></option><option value="Initial Assessment By BCBA">Initial Assessment By BCBA</option><option value="Follow up Assessment By BCBA">Follow up Assessment By BCBA</option><option value="Direct Behavior Therapy By BCBA">Direct Behavior Therapy By BCBA</option><option value="Direct Behavior Therapy By BcABA">Direct Behavior Therapy By BcABA</option><option value="Direct Behavior Therapy By Para">Direct Behavior Therapy By Para</option><option value="Supervision By BCBA">Supervision By BCBA</option><option value="Social Skills Group By Para">Social Skills Group By Para</option><option value="Social Skills Group By BCBA">Social Skills Group By BCBA</option><option value="Parent Training By BCBA">Parent Training By BCBA</option><option value="Family Group Training By BCBA">Family Group Training By BCBA</option>');
                    $("#ActCPT").html('');
                    $("#ActCPT").append('<option value=""></option><option value="0359T">0359T</option><option value="0360T">0360T</option><option value="0361T">0361T</option><option value="0360T-0361T">0360T-0361T</option><option value="0364T">0364T</option><option value="0365T">0365T</option><option value="0364T-0365T">0364T-0365T</option><option value="0368T">0368T</option><option value="0369T">0369T</option><option value="0368T-0369T">0368T-0369T</option><option value="0366T">0366T</option><option value="0367T">0367T</option><option value="0366T-0367T">0366T-0367T</option><option value="0370T">0370T</option><option value="0371T">0371T</option><option value="0372T">0372T</option>')
                  }
                  else if(result =="Speech Therapy"){
                    $("#Activity").html('');
                    $("#Activity").append('<option value=""></option><option value="Evaluation By Speech Therapist">Evaluation By Speech Therapist</option><option value="Continued Services">Continued Services</option>');
                    $("#ActCPT").html('');
                    $("#ActCPT").append('<option value=""></option><option value="92513">92513</option><option value="92507">92507</option>')
                  }
                  else{
                    $("#Activity").html('');
                    $("#Activity").append('<option value=""></option><option value="Assessment">Assessment</option><option value="Direct Behavior Theraphy">Direct Behavior Theraphy</option><option value="Supervision">Supervision</option><option value="Supervision Parent Training">Supervision Parent Training</option><option value="Report Writing Send to Editor">Report Writing Send to Editor</option><option value="Report Writing Send to Insurance">Report Writing Send to Insurance</option><option value="Report Writing Progress Reports">Report Writing Progress Reports</option>');
                    $("#ActCPT").html('');
                    $("#ActCPT").append('<option value=""></option><option value="0359T">0359T</option><option value="0360T">0360T</option><option value="0361T">0361T</option><option value="0364T">0364T</option><option value="0365T">0365T</option><option value="0368T">0368T</option><option value="0369T">0369T</option><option value="0370T">0370T</option>')
                  }
                }
              });
              $("#ActTitleMod").html("Update Activity");
              $("#btnAddActi").val("Update Activity");
              document.getElementById("hdnFlagAct").value = "1";
              document.getElementById("Activity").value = res[0].activityName;
              document.getElementById("ActCPT").value = res[0].activityCPT;
              document.getElementById("ActMod1").value = res[0].activityMod1;
              document.getElementById("ActMod2").value = res[0].activityMod2;
              document.getElementById("ActMod3").value = res[0].activityMod3;
              document.getElementById("ActMod4").value = res[0].activityMod4;
              document.getElementById("ActPer").value = res[0].activityBilledPer;
              document.getElementById("ActRate").value = res[0].activityRates;
              document.getElementById("ActMaxi1").value = res[0].activityMax1;
              document.getElementById("ActMaxi2").value = res[0].activityMax2;
              document.getElementById("ActMaxi3").value = res[0].activityMax3;
              document.getElementById("ActPer1").value = res[0].activityPer1;
              document.getElementById("ActPer2").value = res[0].activityPer2;
              document.getElementById("ActPer3").value = res[0].activityPer3;
              document.getElementById("ActIs1").value = res[0].activityIs1;
              document.getElementById("ActIs2").value = res[0].activityIs2;
              document.getElementById("ActIs3").value = res[0].activityIs3;
            }
          });
        });

        $(document).on('click','.noteEdit',function() {
          var temp = $(this).attr('class').split(' ')[0];
          var newTemp = temp.replace('editNote','');
          document.getElementById('hdnDocID').value = newTemp;
          $('#DocumentNotes').modal("show");
        });

        $('#btnDocNotes').click(function(){
          var notes = document.getElementById("Documentnotes").value;
          var DocID = document.getElementById("hdnDocID").value;
          $.ajax({
            type: "POST",
            url:"updateDocNotes.php",
            async : false,
            data:{
              docID : DocID,
              notes : notes
            },success:function(result){
              $('#DocumentNotes').modal("hide");
              window.location.reload();
            }
          });
        })

        $("#formUpload").submit(function(){
          alert("Submitted");
        });

        $("#btnSub").submit(function(){
          window.location.href = "UploadFile.php";
        });

        $("#addAuthModal").click(function(){
            $('#authModal').modal('show');
        });
        
        $('#divAlert').hide();
        $.ajax({
            type: "POST",
            url:"getDocumentsByID.php",
            async : false,
            data:{
              patientID : temp
            },success:function(result){
              var dt = [];
              var n = result.length;
              var extName = "";
              var sel1 = "";
              var sel2 = "";
              var sel3 = "";
              var sel4 = "";
              var sel5 = "";
              result = result.substr(0,n-1);
            var data1 = JSON.parse(result);
            $.each(data1,function(i,v) {
                if(data1[i].docType == "DOC" || data1[i].docType == "DOCX"|| data1[i].docType == "doc" || data1[i].docType == "docx"){
                    extName = 'assets/img/DOC.png';
                  }
                  else if(data1[i].docType == "XLS" || data1[i].docType == "XLSX" || data1[i].docType == "xls" || data1[i].docType == "xlsx"){
                    extName = 'assets/img/EXCEL.png';
                  }
                  else if(data1[i].docType == "PDF" || data1[i].docType == "pdf"){
                    extName = 'assets/img/PDF.png';
                  }
                  else if(data1[i].docType == "TIFF" || data1[i].docType == "tiff"){
                    extName = 'assets/img/TIFF.png';
                  }
                  else if(data1[i].docType == "html" || data1[i].docType == "HTML"){
                    extName = 'assets/img/HTML.png';
                  }
                  else{
                    extName = data1[i].docType;
                  }
                  if(data1[i].status == "New"){
                    var sel1 = "selected";
                    var sel2 = "";
                    var sel3 = "";
                    var sel4 = "";
                    var sel5 = "";
                  }
                  else if(data1[i].status == "In Process"){
                    var sel1 = "";
                    var sel2 = "selected";
                    var sel3 = "";
                    var sel4 = "";
                    var sel5 = "";
                  }
                  else if(data1[i].status == "Ready for Entry"){
                    var sel1 = "";
                    var sel2 = "";
                    var sel3 = "selected";
                    var sel4 = "";
                    var sel5 = "";
                  }
                  else if(data1[i].status == "Error"){
                    var sel1 = "";
                    var sel2 = "";
                    var sel3 = "";
                    var sel4 = "selected";
                    var sel5 = "";
                  }
                  else if(data1[i].status == "Processed"){
                    var sel1 = "";
                    var sel2 = "";
                    var sel3 = "";
                    var sel4 = "";
                    var sel5 = "selected";
                  }
                  var tot=data1[i].docPath.length;
                  var n=data1[i].docPath.indexOf("/");
                  var file = data1[i].docPath.substring(n+1,tot);

                dt.push([data1[i].docID,data1[i].docName,extName,data1[i].docPath,'<select class="form-control" id="ddlDocStatus'+data1[i].docID+'"><option value="New" '+sel1+'>New</option><option value="In Process" '+sel2+'>In Process</option><option value="Ready for Entry" '+sel3+'>Ready for Entry</option><option value="Error" '+sel4+'>Error</option><option value="Processed" '+sel5+'>Processed</option></select>',data1[i].notes,data1[i].CreatedOn,file,data1[i].docID]);
            });
                $('#documentTable').DataTable({
                  "data": dt,
                  "autoWidth": false,
                  columns: [
                      {"title": "docID","visible" : false},
                      {"title": "DOC NAME", "width": "60px",
                          "render": function ( data, type, full, meta ) {
                            return data;
                          }
                      },
                      {"title": "DOC TYPE", "width": "60px",
                          "render": function ( data, type, full, meta ) {
                            return '<img src="'+data+'"/>';
                          }
                      },
                      {"title": "DOCUMENT PATH",
                          "render": function ( data, type, full, meta ) {
                            return data;
                          }
                      },
                      {"title": "STATUS",
                          "render": function ( data, type, full, meta ) {
                            return data;
                          }
                      },
                      {"title": "NOTES",
                          "render": function ( data, type, full, meta ) {
                            return data;
                          }
                      },
                      {"title": "CREATED ON",
                          "render": function ( data, type, full, meta ) {
                            return data;
                          }
                      },
                      {"title": "DOWNLOAD",
                          "render": function ( data, type, full, meta ) {
                            return '<a href="Documents/'+data+'" download><i class="icon icon-download-alt" style="font-size:22px;text-align:center;"></i></a>';
                          }
                      },
                      {"title": "ACTION",
                          "render": function ( data, type, full, meta ) {
                            return '<a href="javascript:void(0)" class="editNote'+data+' noteEdit"><i class="icon icon-pencil" style="font-size:22px;"></i></a>&nbsp; | &nbsp;<a href="javascript:void(0)" class="saveDoc'+data+' docSave"><i class="icon icon-save" style="font-size:22px;"></i></a>';
                          }
                      }
                  ]
              });
          }
        });


        $.ajax({
              type: "POST",
              url:"getAlerts.php",
              async : false,
              data:{
                alert_patientID : temp
              },success:function(result){
                var res = JSON.parse(result);
                if(res!=""){
                  if(res[0].alerts_EditPatient == "1"){
                      $('#divAlert').show();
                      document.getElementById("alertTop").innerHTML = res[0].alert_desc;
                  }
                  else{
                    $('#divAlert').hide();
                  }
                }
            }
          });

        $("#saveAlert").click(function(){
          var alert_patientID = temp;
          var alert_desc = $('#alertMsg').val();
          var alerts_Charge = 0;
          var alerts_Deposits = 0;
          var alerts_Statements = 0;
          var alerts_EditPatient = 0;
          var alerts_ScheduleApp = 0;
          var alerts_ViewClaims = 0;
          if($('#chkalerts_Charge').is(':checked') == true){
            alerts_Charge = "1";
          }
          else{
            alerts_Charge = "0";
          }
          if($('#chkalerts_Deposits').is(':checked') == true){
            alerts_Deposits = "1";
          }
          else{
            alerts_Deposits = "0";
          }
          if($('#chkalerts_Statements').is(':checked') == true){
            alerts_Statements = "1";
          }
          else{
            alerts_Statements = "0";
          }
          if($('#chkalerts_EditPatient').is(':checked') == true){
            alerts_EditPatient = "1";
          }
          else{
            alerts_EditPatient = "0";
          }
          if($('#chkalerts_ScheduleApp').is(':checked') == true){
            alerts_ScheduleApp = "1";
          }
          else{
            alerts_ScheduleApp = "0";
          }
          if($('#chkalerts_ViewClaims').is(':checked') == true){
            alerts_ViewClaims = "1";
          }
          else{
            alerts_ViewClaims = "0";
          }
          $.ajax({
              type: "POST",
              url:"alerts.php",
              async : false,
              data:{
                alert_patientID : alert_patientID,
                alert_desc : alert_desc,
                alerts_Statements : alerts_Statements,
                alerts_Deposits : alerts_Deposits,
                alerts_Charge : alerts_Charge,
                alerts_ViewClaims : alerts_ViewClaims,
                alerts_EditPatient : alerts_EditPatient,
                alerts_ScheduleApp : alerts_ScheduleApp
              },success:function(result){
                if(result == "add"){
                  alertify.success("Alert added successfully");
                  window.location.reload();
                }
                else{
                  alertify.success("Alert updated successfully"); 
                  window.location.reload();
                }
            }
          });
        });

        $(document).on('focusout','#addrZip',function() {
            var currValue = $(this).val();
            $.ajax({
                type: "POST",
                url:"getZipInfo.php",
                async: false,
                data:{
                    zip : currValue
                },
                success:function(result){
                    var n = result.length;
                    result = result.substr(0,n-1);
                    if(result != "none"){
                        var res = JSON.parse(result);
                        document.getElementById("addrState").value = res[0].state;
                        document.getElementById("addrCity").value = res[0].city;
                    }
                    else{
                        alertify.error("Sorry! This Zip is not available in our Database.")
                    }
                }
            });
        });

        var inv = "";
        $.ajax({
              type: "GET",
              url:"getLastEDI.php",
              async : false,
              data:{
              },success:function(result){
                inv = parseInt(result)+1;
                inv = pad(inv, 5);
             }
          });

        $("#btnPrint").click(function(){
          var claimID = document.getElementById('hdnClaimID').value;
          var ClaimNumber = document.getElementById('hdnClaimNo').value;
          if($("input[name='BG']:checked").val() == "Without BG"){
              $.ajax({
                type: "POST",
                url:"HCFA-500.php",
                async : false,
                data:{
                  ClaimNumber : ClaimNumber
                },success:function(result){
                  alertify.success("EDI generated successfully");
                  window.open('HCFA-500.pdf','_blank');
                }
            });
          }
          else{
              $.ajax({
                type: "POST",
                url:"HCFA-500-BG.php",
                async : false,
                data:{
                  ClaimNumber : ClaimNumber
                },success:function(result){
                  alertify.success("EDI generated successfully");
                  window.open('HCFA-500-BG.pdf','_blank');
                }
            });
          }
        });

        $(document).on('click','.Editclaim',function() {
            var temp = $(this).attr('class').split(' ')[0];
            var newTemp = temp.replace('edit','');
            $.ajax({
              type: "POST",
              url:"getClaiminfo.php",
              async : false,
              data:{
                claimID : newTemp
              },success:function(result){
                $("#claimDetails").modal("hide");
                $('#EditClaim').modal('show');
                var n = result.length;
                result = result.substr(0,n-1);
                var res = JSON.parse(result);
                document.getElementById('fromDt').value = changeDateFormat(res[0].fromDt);
                document.getElementById('hdnClaimID').value = res[0].claimID;
                document.getElementById('hdnClaimNo').value = res[0].ClaimNumber;
                document.getElementById('toDt').value = changeDateFormat(res[0].toDt);
                document.getElementById('proced').value = res[0].proced;
                document.getElementById('billDt').value = changeDateFormat(res[0].toDt);
                document.getElementById('units').value = res[0].units;
                document.getElementById('total').value = res[0].total;
                document.getElementById('allowed').value = res[0].allowed;
                document.getElementById('paid').value = res[0].paid;
                document.getElementById('adjustment').value = res[0].adjustment;
                document.getElementById('claimBalance').value = res[0].claimBalance;
                document.getElementById('diag1').value = res[0].diag1;
                document.getElementById('diag2').value = res[0].diag2;
                document.getElementById('diag3').value = res[0].diag3;
                document.getElementById('diag4').value = res[0].diag4;
                document.getElementById('hdnCaseChart').value = res[0].caseChartNo;
                document.getElementById('hdnBal').value = res[0].claimBalance;
                document.getElementById('hdnCurrInsurance').value = res[0].insuranceID;
                $("#claimStatus").val(res[0].claimStatus);
             }
          });
        });

        $(document).on('click','.actionbtn',function() {
            if($(this).val() == "Transfer Balance"){
                $('.lblTitle').html('Transfer Balance :');
                $('.lblSub1').html('Policy :');
                $('.lblSub2').html('Amount :');
                var claimBal = document.getElementById('hdnBal').value;
                var currInsurance = document.getElementById('hdnCurrInsurance').value;
                $('.txtSub1').html('<select class="form-control" id="ddlPolicy"><option value="-100">SELF</option></select>');
                var tempcaseText = document.getElementById('hdnCaseChart').value;
                for(var i=1; i<4;i++){
                    $.post("http://curismed.com/medService/policies/load",
                    {
                        policyEntity: i,
                        caseChartNo : tempcaseText
                    },
                    function(data1, status){
                        if(data1.length != 0){
                            var insName1 = data1[0].InsuranceName;
                            var insuranceID1 = data1[0].insuranceID;
                            if(insuranceID1 != currInsurance){
                                $('#ddlPolicy').append('<option value="'+insuranceID1+'">'+insName1+'</option>');
                            }
                        }
                    });
                }
                $('.txtSub2').html('<input type="text" id="txtAmount" value="'+claimBal+'" disabled class="form-control"/>');
                $('.lblSub3').html('');
                $('.txtSub3').html('');
                $('.btnSection').html('<input type="button" id="btnTransfer" value="Transfer" class="btn btn-primary pull-right"/>');
            }
            else if($(this).val() == "Adjustment"){
                $('.lblTitle').html('Adjustment :');
                $('.lblSub1').html('Adjust Amount :');
                $('.lblSub2').html('Adjustment Code :');
                $('.txtSub1').html('<input type="text" id="txtAdjust" class="form-control"/>');
                $('.txtSub2').html('<select class="form-control" id="ddlAdjustmentCd"><option value="0">Default</option><option value="0">General Adjustment</option><option value="02">Contractural Adjustment 1</option><option value="03">Contractural Adjustment 2</option><option value="04">Contractural Adjustment 3</option><option value="05">Capitation Adjustment</option><option value="06">Out Of Network Adjustment</option><option value="07">Small Balance Adjustment</option><option value="08">Courtesy/Charity Adjustment</option><option value="09">Workers Comp Adjustment</option><option value="10">Payment Correction Adjustment</option><option value="11">IRS Tax Deduction</option><option value="12">Patient Refund</option><option value="13">Insurance Refund</option><option value="14">Other Refund</option><option value="15">Small Balance Write-Off</option><option value="16">Collection Account Write-Off</option><option value="17">Settlement Write-Off</option><option value="18">Bad Debt Write-Off</option><option value="19">Debit Adjustment</option><option value="20">Credit Adjustment</option></select>');
                $('.btnSection').html('<input type="button" id="btnAdjust" value="Adjust" class="btn btn-primary pull-right"/>');
            }
            else if($(this).val() == "Note"){
                $('.lblTitle').html('Notes :');
                $('.txtSub1').html('');
                $('.txtSub2').html('');
                $('.lblSub1').html('<textarea class="form-control" style="width:400px; height:80px;" rows="3" cols="6" id="areaNotes"></textarea>');
                $('.lblSub2').html('');
                $('.btnSection').html('<input type="button" id="btnAddNotes" value="Add Note" class="btn btn-primary pull-right"/>');
            }
            else if($(this).val() == "Void"){
                $('.lblTitle').html('');
                $('.lblSub1').html('');
                $('.lblSub2').html('');
                $('.txtSub1').html('');
                $('.txtSub2').html('');
                $('.lblSub3').html('');
                $('.txtSub3').html('');
                $('.btnSection').html('');
            }
            else if($(this).val() == "Settle"){
                $('.lblTitle').html('');
                $('.lblSub1').html('');
                $('.lblSub2').html('');
                $('.txtSub1').html('');
                $('.txtSub2').html('');
                $('.lblSub3').html('');
                $('.txtSub3').html('');
                $('.btnSection').html('');
            }
        });

        $(document).on('click','#btnTransfer',function() {
            var claimID = document.getElementById('hdnClaimID').value;
            var policyID = $('#ddlPolicy').val();
            var amount = $('#txtAmount').val();
            $.ajax({
              type: "POST",
              url:"updateTransfer.php",
              async : false,
              data:{
                claimID : claimID,
                insuranceID : policyID,
                claimBalance : amount
              },success:function(result){
                //console.log(result);
                $('#EditClaim').modal('hide');
                window.location.reload();
              }
          });
        });

        $(document).on('click','#btnAdjust',function() {
            var claimID = document.getElementById('hdnClaimID').value;
            var policyID = $('#ddlPolicy').val();
            var amount = $('#txtAdjust').val();
            var adjustCode = $('#ddlAdjustmentCd').val();
            $.ajax({
              type: "POST",
              url:"updateAdjust.php",
              async : false,
              data:{
                claimID : claimID,
                insuranceID : policyID,
                claimAdjust : amount,
                adjustCode : adjustCode
              },success:function(result){
                $('#EditClaim').modal('hide');
                window.location.reload();
              }
          });
        });

        $(document).on('click','#btnSettle',function() {
          if (confirm("Do you want to Settle the Charges?")) {
              var claimID = document.getElementById('hdnClaimID').value;
              $.ajax({
                type: "POST",
                url:"updateSettle.php",
                async : false,
                data:{
                  claimID : claimID,
                },success:function(result){
                  alertify.success("Claim Balance reset and added with adjustment successfully.");
                  $('#EditClaim').modal('hide');
                  window.location.reload();
                }
            });
          }
          return false;
        });

        $(document).on('click','#btnVoid',function() {
          if (confirm("Do you want to Void the Charges?")) {
              var claimID = document.getElementById('hdnClaimID').value;
              $.ajax({
                type: "POST",
                url:"updateVoid.php",
                async : false,
                data:{
                  claimID : claimID,
                },success:function(result){
                  alertify.success("Claim has been deleted.");
                  $('#EditClaim').modal('hide');
                  window.location.reload();
                }
            });
          }
          return false;
        });

        $(document).on('click','#btnAddNotes',function() {
            var claimID = document.getElementById('hdnClaimID').value;
            var notes = $('#areaNotes').val();
            $.ajax({
              type: "POST",
              url:"updateNotes.php",
              async : false,
              data:{
                claimID : claimID,
                notes : notes,
              },success:function(result){
                $('#EditClaim').modal('hide');
                window.location.reload();
              }
          });
        });

        $('#btnPaperClaim').click(function(){
            $('#paperClaimModal').modal('show');
        })

        $.post("getAccountClaims.php",
        {
            patientID: temp
        },
        function(data, status){
              var data1 = JSON.parse(data);

              var dt = [];
              $.each(data1,function(i,v) {
                 dt.push([data1[i].ClaimNumber,data1[i].fromDt,data1[i].TotalCharges,data1[i].Balance,data1[i].payerName,data1[i].fullName]);
              });
              $('#accountTable').DataTable({
               "aaSorting": [[ 0, "desc" ]],
               "data": dt,
                  columns: [
                   {"title": "Claim #",
                      "render": function ( data, type, full, meta ) {
                         return '<a href="javascript:void(0);" class="cNo cNo'+data+'">'+data+'</a>';
                      }
                   },
                  {"title": "DOS",
                      "render": function ( data, type, full, meta ) {
                        return changeDateFormat(data);
                      }
                  },
                  {"title": "Total Charges",
                      "render": function ( data, type, full, meta ) {
                         return '$'+data;
                      }
                  },
                  {"title": "Balance",
                      "render": function ( data, type, full, meta ) {
                         return '$'+data;
                      }
                  },
                  {"title": "Payer Name"},
                  {"title": "Patient Name"}
                  ]
           });
          $("#accountTable").removeAttr("style");
        });
        $(document).on('click','.cNo',function() {
            //var value = $(this).val();
            var temp = $(this).attr('class').split(' ')[1];
            var newTemp = temp.replace('cNo','');
            sessionStorage.setItem("claimNo", newTemp);
            $("#claimDetails").modal("show");
            $.ajax({
              type: "POST",
              url:"http://curismed.com/medService/claims/ledgerdetail",
              data:{
                ClaimNumber : newTemp
              },success:function(data1){
                
                var dt = [];
                $.each(data1,function(i,v) {
                    if(data1[i].allowed == ""){
                        data1[i].allowed = "0.00";
                    }
                    if(data1[i].paid == ""){
                        data1[i].paid = "0.00";
                    }
                    if(data1[i].adjustment == ""){
                        data1[i].adjustment = "0.00";
                    }

                    if(data1[i].insuranceName == null){
                        data1[i].insuranceName = "SELF";
                    }
                   dt.push([data1[i].fromDt,data1[i].toDt,data1[i].proced,data1[i].units,data1[i].total,data1[i].allowed,data1[i].paid,data1[i].adjustment,data1[i].claimBalance,data1[i].insuranceName,data1[i].fullName,data1[i].claimID]);
                });


                $('#details').DataTable({
                 destroy: true,
                 searching: false,
                 "aaSorting": [[ 4, "desc" ]],
                 "data": dt,
                    columns: [
                    {"title": "From",
                        "render": function ( data, type, full, meta ) {
                          return changeDateFormat(data);
                        }
                    },
                    {"title": "To",
                        "render": function ( data, type, full, meta ) {
                          return changeDateFormat(data);
                        }
                    },
                    {"title": "CPT"},
                    {"title": "Units"},
                    {"title":"Total Charges","mdata": "total",
                        "render": function ( data, type, full, meta ) {
                          return '$'+data;
                        }
                    },
                    {"title":"Allowed","mdata": "allowed",
                        "render": function ( data, type, full, meta ) {
                          return '$'+data;
                        }
                    },
                    {"title":"Paid","mdata": "paid",
                        "render": function ( data, type, full, meta ) {
                          return '$'+data;
                        }
                    },
                    {"title":"Adjustment","mdata": "adjustment",
                        "render": function ( data, type, full, meta ) {
                          return '$'+data;
                        }
                    },
                    {"title":"Balance","mdata": "claimBalance",
                        "render": function ( data, type, full, meta ) {
                          return '$'+data;
                        }
                    },
                    {"title":"Payer Name",
                        "render": function ( data, type, full, meta ) {
                          return data;
                        }
                    },
                    {"title":"Patient Name",
                        "render": function ( data, type, full, meta ) {
                          return data;
                        }
                    },
                    {"title": "Action",
                        "render": function ( data, type, full, meta ) {
                          return '<a href="javascript:void(0);" class="edit'+data+' Editclaim"><i class="icon icon-pencil"></i></a>';
                        }
                    }
                    ]
             });
            }
        });
        });
        $('#btnUpdateClaim').click(function(){
            var claimID = document.getElementById('hdnClaimID').value;
            var fromDt = convertDate(document.getElementById('fromDt').value);
            var toDt = convertDate(document.getElementById('toDt').value);
            var proced = document.getElementById('proced').value;
            var billDt = convertDate(document.getElementById('billDt').value);
            var units = document.getElementById('units').value;
            var total = document.getElementById('total').value;
            var allowed = document.getElementById('allowed').value;
            var paid = document.getElementById('paid').value;
            var adjustment = document.getElementById('adjustment').value;
            var claimBalance = document.getElementById('claimBalance').value;
            var claimStatus = document.getElementById('claimStatus').value;
            var isPosted = document.getElementById('IsPosted').value;
            var isSent = document.getElementById('IsSent').value;
            var diag1 = document.getElementById('diag1').value;
            var diag2 = document.getElementById('diag2').value;
            var diag3 = document.getElementById('diag3').value;
            var diag4 = document.getElementById('diag4').value;
        
            $.ajax({
              type: "POST",
              url:"updateClaim.php",
              data:{
                claimID : claimID,
                fromDt : fromDt,
                toDt : toDt,
                proced : proced,
                billDt : billDt,
                units : units,
                total : total,
                allowed : allowed,
                paid : paid,
                adjustment : adjustment,
                claimBalance : claimBalance,
                claimStatus : claimStatus,
                isPosted : isPosted,
                isSent : isSent,
                diag1 : diag1,
                diag2 : diag2,
                diag3 : diag3,
                diag4 : diag4
              },success:function(result){
                $('#EditClaim').modal('hide');
              }
            });
        });
        $('#btnRebill').click(function(){
            var claimID = document.getElementById("hdnClaimID").value;
            //var rebillVal = document.getElementById("IsSent").value;
            //if(rebillVal != 0){
                $.ajax({
                    type: "POST",
                    url:"rebill.php",
                    data:{
                        claimID : claimID
                    },
                    success:function(result){
                        alertify.success("This claim is added to EDI Queue again");
                    }
                });
            //}
            // else{
            //     alertify.error("This claims is not sent already.");
            // }
        });

        var patientList = <?php include('patientList.php'); ?>;
            $("#policyHolder1").autocomplete({
                source: patientList,
                autoFocus:true
            });
            $("#policyHolder2").autocomplete({
                source: patientList,
                autoFocus:true
            });
            $("#policyHolder3").autocomplete({
                source: patientList,
                autoFocus:true
            });
            $("#EditpolicyHolder2").autocomplete({
                source: patientList,
                autoFocus:true
            });
            $("#EditpolicyHolder1").autocomplete({
                source: patientList,
                autoFocus:true
            });
            $("#EditpolicyHolder3").autocomplete({
                source: patientList,
                autoFocus:true
            });

        $('#chkAuth').change(function(){
            if($('#chkAuth').is(':checked') == true){
                $('#authNo').removeAttr('disabled');
                //$('#noOfVisits').removeAttr('disabled');
                $('#totalVisits').removeAttr('disabled');
                $('#lastVisit').removeAttr('disabled');
                $('#principalDiag').removeAttr('disabled');
                $('#defDiag2').removeAttr('disabled');
                $('#defDiag3').removeAttr('disabled');
                $('#defDiag4').removeAttr('disabled');
            }
            else{
                $('#authNo').attr('disabled',true);
                $('#noOfVisits').attr('disabled',true);
                $('#totalVisits').attr('disabled',true);
                $('#lastVisit').attr('disabled',true);
                $('#principalDiag').attr('disabled',true);
                $('#defDiag2').attr('disabled',true);
                $('#defDiag3').attr('disabled',true);
                $('#defDiag4').attr('disabled',true);
            }
        });

        $('#EditchkAuth').change(function(){
            checkAuth();
        });

        var insurances = <?php include('insuranceList.php'); ?>;
        $("#policyInsurance1").autocomplete({
                source: function(request, response) {
                    var results = $.ui.autocomplete.filter(insurances, request.term);

                    if (!results.length) {
                        results = ["No matches found"];
                    }

                    response(results);
                },
                autoFocus:true,
                focus: function (event, ui) {
                       var val = ui.item.id;
                       if(val != undefined){
                           $.ajax({
                                type: "POST",
                                url:"getInsuranceAddrbyID.php",
                                async: false,
                                data:{
                                    insuranceID : val
                                },
                                success:function(result){
                                    $("#NewspanAddress1").html(result);
                                }
                            });
                           event.preventDefault(); // Prevent the default focus behavior.
                       }
                       else{
                            $("#NewspanAddress1").html("<a href='javascript:void(0);' id='linkIns'><i class='icon icon-plus'></i> Add New Insurance</a>");
                       }
                },
                select: function (event, ui) {
                    var label = ui.item.id;
                    var value = ui.item.value;
                    var currValue = label;
                    $.ajax({
                        type: "POST",
                        url:"getInsuranceAddrbyID.php",
                        async: false,
                        data:{
                            insuranceID : label
                        },
                        success:function(result){
                            $("#NewspanAddress1").html(result);
                        }
                    });
                }
            }); 
            $("#policyInsurance2").autocomplete({
                source: insurances,
                autoFocus:true,
                focus: function (event, ui) {
                       var val = ui.item.id;
                       $.ajax({
                            type: "POST",
                            url:"getInsuranceAddrbyID.php",
                            async: false,
                            data:{
                                insuranceID : val
                            },
                            success:function(result){
                                $("#NewspanAddress2").html(result);
                            }
                        });
                       event.preventDefault(); // Prevent the default focus behavior.
                },
                select: function (event, ui) {
                    var label = ui.item.id;
                    var value = ui.item.value;
                    var currValue = label;
                    $.ajax({
                        type: "POST",
                        url:"getInsuranceAddrbyID.php",
                        async: false,
                        data:{
                            insuranceID : label
                        },
                        success:function(result){
                            $("#NewspanAddress2").html(result);
                        }
                    });
                }
            }); 
            $("#policyInsurance3").autocomplete({
                source: insurances,
                autoFocus:true,
                focus: function (event, ui) {
                       var val = ui.item.id;
                       $.ajax({
                            type: "POST",
                            url:"getInsuranceAddrbyID.php",
                            async: false,
                            data:{
                                insuranceID : val
                            },
                            success:function(result){
                                $("#NewspanAddress3").html(result);
                            }
                        });
                       event.preventDefault(); // Prevent the default focus behavior.
                },
                select: function (event, ui) {
                    var label = ui.item.id;
                    var value = ui.item.value;
                    var currValue = label;
                    $.ajax({
                        type: "POST",
                        url:"getInsuranceAddrbyID.php",
                        async: false,
                        data:{
                            insuranceID : label
                        },
                        success:function(result){
                            $("#NewspanAddress3").html(result);
                        }
                    });
                }
            }); 

            $("#EditpolicyInsurance1").autocomplete({
                source: function(request, response) {
                    var results = $.ui.autocomplete.filter(insurances, request.term);

                    if (!results.length) {
                        results = ["No matches found"];
                    }

                    response(results);
                },
                autoFocus:true,
                focus: function (event, ui) {
                       var val = ui.item.id;
                       if(val != undefined){
                           $.ajax({
                                type: "POST",
                                url:"getInsuranceAddrbyID.php",
                                async: false,
                                data:{
                                    insuranceID : val
                                },
                                success:function(result){
                                    $("#spanAddress1").html(result);
                                }
                            });
                           event.preventDefault(); // Prevent the default focus behavior.
                       }
                       else{
                            $("#spanAddress1").html("<a href='javascript:void(0);' id='linkIns'><i class='icon icon-plus'></i> Add New Insurance</a>");
                       }
                },
                select: function (event, ui) {
                    var label = ui.item.id;
                    var value = ui.item.value;
                    var currValue = label;
                    $.ajax({
                        type: "POST",
                        url:"getInsuranceAddrbyID.php",
                        async: false,
                        data:{
                            insuranceID : label
                        },
                        success:function(result){
                            $("#NewspanAddress1").html(result);
                        }
                    });
                }
            }); 
            $("#EditpolicyInsurance2").autocomplete({
                source: insurances,
                autoFocus:true,
                focus: function (event, ui) {
                       var val = ui.item.id;
                       $.ajax({
                            type: "POST",
                            url:"getInsuranceAddrbyID.php",
                            async: false,
                            data:{
                                insuranceID : val
                            },
                            success:function(result){
                                $("#spanAddress2").html(result);
                            }
                        });
                       event.preventDefault(); // Prevent the default focus behavior.
                },
                select: function (event, ui) {
                    var label = ui.item.id;
                    var value = ui.item.value;
                    var currValue = label;
                    $.ajax({
                        type: "POST",
                        url:"getInsuranceAddrbyID.php",
                        async: false,
                        data:{
                            insuranceID : label
                        },
                        success:function(result){
                            $("#spanAddress2").html(result);
                        }
                    });
                }
            }); 
            $("#EditpolicyInsurance3").autocomplete({
                source: insurances,
                autoFocus:true,
                focus: function (event, ui) {
                       var val = ui.item.id;
                       $.ajax({
                            type: "POST",
                            url:"getInsuranceAddrbyID.php",
                            async: false,
                            data:{
                                insuranceID : val
                            },
                            success:function(result){
                                $("#spanAddress3").html(result);
                            }
                        });
                       event.preventDefault(); // Prevent the default focus behavior.
                },
                select: function (event, ui) {
                    var label = ui.item.id;
                    var value = ui.item.value;
                    var currValue = label;
                    $.ajax({
                        type: "POST",
                        url:"getInsuranceAddrbyID.php",
                        async: false,
                        data:{
                            insuranceID : label
                        },
                        success:function(result){
                            $("#spanAddress3").html(result);
                        }
                    });
                }
            }); 
        
        $(document).on('focus','#principalDiag',function() {
            var toggleDX = document.getElementById('hdntoggleDX').value;
            if(toggleDX != "false"){
                $("#principalDiag").autocomplete({
                    minlength : 3,
                    source: function(request, response) { 
                        $.ajax({
                            url: "dxSearch9.php",
                            data: { term: request.term },
                            type: "POST", 
                            success: function(data) { 
                                response(JSON.parse(data)); 
                            }
                        });
                    },
                    autoFocus:true
                });
            }
            else{
                $("#principalDiag").autocomplete({
                    minlength : 3,
                    source: function(request, response) { 
                        $.ajax({
                            url: "dxSearch10.php",
                            data: { term: request.term },
                            type: "POST", 
                            success: function(data) { 
                                response(JSON.parse(data)); 
                            }
                        });
                    },
                    autoFocus:true
                });
            }
        });

        $(document).on('focus','#defDiag2',function() {
            var toggleDX = document.getElementById('hdntoggleDX').value;
            if(toggleDX != "false"){
                $("#defDiag2").autocomplete({
                    minlength : 3,
                    source: function(request, response) { 
                        $.ajax({
                            url: "dxSearch9.php",
                            data: { term: request.term },
                            type: "POST", 
                            success: function(data) { 
                                response(JSON.parse(data)); 
                            }
                        });
                    },
                    autoFocus:true
                });
            }
            else{
                $("#defDiag2").autocomplete({
                    minlength : 3,
                    source: function(request, response) { 
                        $.ajax({
                            url: "dxSearch10.php",
                            data: { term: request.term },
                            type: "POST", 
                            success: function(data) { 
                                response(JSON.parse(data)); 
                            }
                        });
                    },
                    autoFocus:true
                });
            }
        });

        $(document).on('focus','#defDiag3',function() {
            var toggleDX = document.getElementById('hdntoggleDX').value;
            if(toggleDX != "false"){
                $("#defDiag3").autocomplete({
                    minlength : 3,
                    source: function(request, response) { 
                        $.ajax({
                            url: "dxSearch9.php",
                            data: { term: request.term },
                            type: "POST", 
                            success: function(data) { 
                                response(JSON.parse(data)); 
                            }
                        });
                    },
                    autoFocus:true
                });
            }
            else{
                $("#defDiag3").autocomplete({
                    minlength : 3,
                    source: function(request, response) { 
                        $.ajax({
                            url: "dxSearch10.php",
                            data: { term: request.term },
                            type: "POST", 
                            success: function(data) { 
                                response(JSON.parse(data)); 
                            }
                        });
                    },
                    autoFocus:true
                });
            }
        });

        $(document).on('focus','#defDiag4',function() {
            var toggleDX = document.getElementById('hdntoggleDX').value;
            if(toggleDX != "false"){
                $("#defDiag4").autocomplete({
                    minlength : 3,
                    source: function(request, response) { 
                        $.ajax({
                            url: "dxSearch9.php",
                            data: { term: request.term },
                            type: "POST", 
                            success: function(data) { 
                                response(JSON.parse(data)); 
                            }
                        });
                    },
                    autoFocus:true
                });
            }
            else{
                $("#defDiag4").autocomplete({
                    minlength : 3,
                    source: function(request, response) { 
                        $.ajax({
                            url: "dxSearch10.php",
                            data: { term: request.term },
                            type: "POST", 
                            success: function(data) { 
                                response(JSON.parse(data)); 
                            }
                        });
                    },
                    autoFocus:true
                });
            }
        });

        document.getElementById('hdnDX').value = "true";

        $('#icd').on('change', function(event, state) {
          document.getElementById('hdnDX').value = event.target.checked; // jQuery event
        });


        $(document).on('focus','#diag1',function() {
            var toggleDX = document.getElementById('hdnDX').value;
            if(toggleDX != "false"){
                $("#diag1").autocomplete({
                    minlength : 3,
                    source: function(request, response) { 
                        $.ajax({
                            url: "dxSearch9.php",
                            data: { term: request.term },
                            type: "POST", 
                            success: function(data) { 
                                response(JSON.parse(data)); 
                            }
                        });
                    },
                    autoFocus:true
                });
            }
            else{
                $("#diag1").autocomplete({
                    minlength : 3,
                    source: function(request, response) { 
                        $.ajax({
                            url: "dxSearch10.php",
                            data: { term: request.term },
                            type: "POST", 
                            success: function(data) { 
                                response(JSON.parse(data)); 
                            }
                        });
                    },
                    autoFocus:true
                });
            }
        });

        $(document).on('focus','#diag2',function() {
            var toggleDX = document.getElementById('hdnDX').value;
            if(toggleDX != "false"){
                $("#diag2").autocomplete({
                    minlength : 3,
                    source: function(request, response) { 
                        $.ajax({
                            url: "dxSearch9.php",
                            data: { term: request.term },
                            type: "POST", 
                            success: function(data) { 
                                response(JSON.parse(data)); 
                            }
                        });
                    },
                    autoFocus:true
                });
            }
            else{
                $("#diag2").autocomplete({
                    minlength : 3,
                    source: function(request, response) { 
                        $.ajax({
                            url: "dxSearch10.php",
                            data: { term: request.term },
                            type: "POST", 
                            success: function(data) { 
                                response(JSON.parse(data)); 
                            }
                        });
                    },
                    autoFocus:true
                });
            }
        });

        $(document).on('focus','#diag3',function() {
            var toggleDX = document.getElementById('hdnDX').value;
            if(toggleDX != "false"){
                $("#diag3").autocomplete({
                    minlength : 3,
                    source: function(request, response) { 
                        $.ajax({
                            url: "dxSearch9.php",
                            data: { term: request.term },
                            type: "POST", 
                            success: function(data) { 
                                response(JSON.parse(data)); 
                            }
                        });
                    },
                    autoFocus:true
                });
            }
            else{
                $("#diag3").autocomplete({
                    minlength : 3,
                    source: function(request, response) { 
                        $.ajax({
                            url: "dxSearch10.php",
                            data: { term: request.term },
                            type: "POST", 
                            success: function(data) { 
                                response(JSON.parse(data)); 
                            }
                        });
                    },
                    autoFocus:true
                });
            }
        });

        $(document).on('focus','#diag4',function() {
            var toggleDX = document.getElementById('hdnDX').value;
            if(toggleDX != "false"){
                $("#diag4").autocomplete({
                    minlength : 3,
                    source: function(request, response) { 
                        $.ajax({
                            url: "dxSearch9.php",
                            data: { term: request.term },
                            type: "POST", 
                            success: function(data) { 
                                response(JSON.parse(data)); 
                            }
                        });
                    },
                    autoFocus:true
                });
            }
            else{
                $("#diag4").autocomplete({
                    minlength : 3,
                    source: function(request, response) { 
                        $.ajax({
                            url: "dxSearch10.php",
                            data: { term: request.term },
                            type: "POST", 
                            success: function(data) { 
                                response(JSON.parse(data)); 
                            }
                        });
                    },
                    autoFocus:true
                });
            }
        });

        $('#Editicdtoggle').on('change', function(event, state) {
          document.getElementById('hdnEdittoggleDX').value = event.target.checked; // jQuery event
        });

        $(document).on('focus','#EditprincipalDiag',function() {
            var toggleDX = document.getElementById('hdnEdittoggleDX').value;
            if(toggleDX != "false"){
                $("#EditprincipalDiag").autocomplete({
                    minlength : 3,
                    source: function(request, response) { 
                        $.ajax({
                            url: "dxSearch9.php",
                            data: { term: request.term },
                            type: "POST", 
                            success: function(data) { 
                                response(JSON.parse(data)); 
                            }
                        });
                    },
                    autoFocus:true
                });
            }
            else{
                $("#EditprincipalDiag").autocomplete({
                    minlength : 3,
                    source: function(request, response) { 
                        $.ajax({
                            url: "dxSearch10.php",
                            data: { term: request.term },
                            type: "POST", 
                            success: function(data) { 
                                response(JSON.parse(data)); 
                            }
                        });
                    },
                    autoFocus:true
                });
            }
        });

        $(document).on('focus','#EditdefDiag2',function() {
            var toggleDX = document.getElementById('hdnEdittoggleDX').value;
            if(toggleDX != "false"){
                $("#EditdefDiag2").autocomplete({
                    minlength : 3,
                    source: function(request, response) { 
                        $.ajax({
                            url: "dxSearch9.php",
                            data: { term: request.term },
                            type: "POST", 
                            success: function(data) { 
                                response(JSON.parse(data)); 
                            }
                        });
                    },
                    autoFocus:true
                });
            }
            else{
                $("#EditdefDiag2").autocomplete({
                    minlength : 3,
                    source: function(request, response) { 
                        $.ajax({
                            url: "dxSearch10.php",
                            data: { term: request.term },
                            type: "POST", 
                            success: function(data) { 
                                response(JSON.parse(data)); 
                            }
                        });
                    },
                    autoFocus:true
                });
            }
        });

        $(document).on('focus','#EditdefDiag3',function() {
            var toggleDX = document.getElementById('hdnEdittoggleDX').value;
            if(toggleDX != "false"){
                $("#EditdefDiag3").autocomplete({
                    minlength : 3,
                    source: function(request, response) { 
                        $.ajax({
                            url: "dxSearch9.php",
                            data: { term: request.term },
                            type: "POST", 
                            success: function(data) { 
                                response(JSON.parse(data)); 
                            }
                        });
                    },
                    autoFocus:true
                });
            }
            else{
                $("#EditdefDiag3").autocomplete({
                    minlength : 3,
                    source: function(request, response) { 
                        $.ajax({
                            url: "dxSearch10.php",
                            data: { term: request.term },
                            type: "POST", 
                            success: function(data) { 
                                response(JSON.parse(data)); 
                            }
                        });
                    },
                    autoFocus:true
                });
            }
        });

        $(document).on('focus','#EditdefDiag4',function() {
            var toggleDX = document.getElementById('hdnEdittoggleDX').value;
            if(toggleDX != "false"){
                $("#EditdefDiag4").autocomplete({
                    minlength : 3,
                    source: function(request, response) { 
                        $.ajax({
                            url: "dxSearch9.php",
                            data: { term: request.term },
                            type: "POST", 
                            success: function(data) { 
                                response(JSON.parse(data)); 
                            }
                        });
                    },
                    autoFocus:true
                });
            }
            else{
                $("#EditdefDiag4").autocomplete({
                    minlength : 3,
                    source: function(request, response) { 
                        $.ajax({
                            url: "dxSearch10.php",
                            data: { term: request.term },
                            type: "POST", 
                            success: function(data) { 
                                response(JSON.parse(data)); 
                            }
                        });
                    },
                    autoFocus:true
                });
            }
        });
        
        
        $('#ssn').mask('000-00-0000');
        $('#dob').mask('00/00/0000');
        $('#firstConsultDate').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#firstConsultDate').mask('00-00-0000');

        $('#tempAuthstartDt').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#tempAuthstartDt').mask('00-00-0000');
        $('#AuthstartDt').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#AuthstartDt').mask('00-00-0000');
        $('#AuthendDt').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#AuthendDt').mask('00-00-0000');

        $('#tempAuthendDt').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#tempAuthendDt').mask('00-00-0000');
        $('#effStartDt').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#effStartDt').mask('00-00-0000');
        $('#effEndDt').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#effEndDt').mask('00-00-0000');
        $('#dateLastSeen').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#dateLastSeen').mask('00-00-0000');
        $('#refDate').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#refDate').mask('00-00-0000');
        $('#prescripDate').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#prescripDate').mask('00-00-0000');
        $('#lastXRay').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#lastXRay').mask('00-00-0000');
        $('#estimatedDOB').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#estimatedDOB').mask('00-00-0000');
        $('#dateAssumedCare').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#dateAssumedCare').mask('00-00-0000');
        $('#dateRelinquishedCare').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#dateRelinquishedCare').mask('00-00-0000');
        $('#lastWorkDate').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#lastWorkDate').mask('000-00-0000');
        $('#dob').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#globalCoverageUntil').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#globalCoverageUntil').mask('000-00-0000');
        $('#fromDt').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#fromDt').mask('00-00-0000');
        $('#billDt').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#billDt').mask('00-00-0000');
        $('#toDt').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#toDt').mask('00-00-0000');
        $('#policyStartDt1').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#policyStartDt1').mask('00-00-0000');
        $('#policyEndDt1').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#policyEndDt1').mask('00-00-0000');
        $('#policyStartDt2').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#policyStartDt2').mask('00-00-0000');
        $('#policyEndDt2').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#policyEndDt2').mask('00-00-0000');
        $('#policyStartDt3').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#policyStartDt3').mask('00-00-0000');
        $('#policyEndDt3').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#policyEndDt3').mask('00-00-0000');
        $('#EditpolicyStartDt1').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#EditpolicyStartDt1').mask('00-00-0000');
        $('#EditpolicyEndDt1').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#EditpolicyEndDt1').mask('00-00-0000');
        $('#EditpolicyStartDt2').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#EditpolicyStartDt2').mask('00-00-0000');
        $('#EditpolicyEndDt2').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#EditpolicyEndDt2').mask('00-00-0000');
        $('#EditpolicyStartDt3').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#EditpolicyStartDt3').mask('00-00-0000');
        $('#EditpolicyEndDt3').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#EditpolicyEndDt3').mask('00-00-0000');
        $('#retirementDate').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#retirementDate').mask('00-00-0000');
        $('#lastVisit').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#lastVisit').mask('00-00-0000');
        $('#authorizedThrough').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#authorizedThrough').mask('00-00-0000');
            $('#phoneHome').mask('(000)000-0000');
            $('#phoneWork').mask('(000)000-0000');
            $('#phoneMobile').mask('(000)000-0000');

        $("#btnAddPriPhy").click(function() {
            var primaryPhysiName = $('#PrimaryPhysiName').val();
            var phyDOB = convertDate($('#phyDOB').val());
            var phyDegree = $('#phyDegree').val();
            var phyNPI= $('#phyNPI').val();
            var phyType = $('#phyType').val();
            var phySSN = $('#phySSN').val();
            var phyAddr = $('#phyAddr').val();
            var phyHomePhone = $('#phyHomePhone').val();
            var phyWorkPhone = $('#phyWorkPhone').val();
            var phyMobilePhone = $('#phyMobilePhone').val();
            var phyEmail = $('#phyEmail').val();
            var phyFax = $('#phyFax').val();
            var phyNotes = $('#phyNotes').val();

            $.ajax({
                type: "POST",
                url:"backend/add_primaryPhysi.php",
                data:{ 
                    "PrimaryPhysiName" : primaryPhysiName,
                    "phyDOB" : phyDOB,
                    "phyDegree" : phyDegree,
                    "phyNPI" : phyNPI,
                    "phyType" : phyType,
                    "phySSN" : phySSN,
                    "phyAddr" : phyAddr,
                    "phyHomePhone" : phyHomePhone,
                    "phyWorkPhone" : phyWorkPhone,
                    "phyMobilePhone" : phyMobilePhone,
                    "phyEmail" : phyEmail,
                    "phyFax" : phyFax,
                    "phyNotes" : phyNotes
                    },success:function(result){
                        alertify.success('PrimaryPhysiName updated successfully');
                        $('#PrimaryPhysi').modal('hide');
                        //window.location.reload();
                     }

            });
        });

        $.get("facilityList.php", function(data, status){
            var data = JSON.parse(data);
            var arrFacility = [];
            $('#Editfacility').html('');
            for(var x in data){
              arrFacility.push(data[x]);
            }
            //alert(arrCase);
            $.each(arrFacility,function(i,v) {
                $('#Editfacility').html('');
                arrFacility.forEach(function(t) { 
                    $('#Editfacility').append('<option value="'+t.serviceLocID+'">'+t.internalName+'</option>');
                });
            });
        });

        $.get("facilityList.php", function(data, status){
            var data = JSON.parse(data);
            var arrFacility = [];
            $('#facility').html('');
            for(var x in data){
              arrFacility.push(data[x]);
            }
            //alert(arrCase);
            $.each(arrFacility,function(i,v) {
                $('#facility').html('');
                arrFacility.forEach(function(t) { 
                    $('#facility').append('<option value="'+t.serviceLocID+'">'+t.internalName+'</option>');
                });
            });
        });

        var loc = <?php include('getServiceLoc.php'); ?>;
            $("#defaultServiceLoc").autocomplete({
                source: loc,
                autoFocus:true
            });

        var PrimaryPhy = <?php include('primaryPhysician.php'); ?>;
            $("#primaryCarePhy").autocomplete({
                source: PrimaryPhy,
                autoFocus:true
            });
            $("#defaultRenderPhy").autocomplete({
                source: PrimaryPhy,
                autoFocus:true
            });
            $("#assignedProvider").autocomplete({
                source: PrimaryPhy,
                autoFocus:true
            });
            $("#supervisingProvider").autocomplete({
                source: PrimaryPhy,
                autoFocus:true
            });
            $("#operatingProvider").autocomplete({
                source: PrimaryPhy,
                autoFocus:true
            });
            $("#otherProvider").autocomplete({
                source: PrimaryPhy,
                autoFocus:true
            });
            $("#outsideProvider").autocomplete({
                source: PrimaryPhy,
                autoFocus:true
            });

            $("#EditprimaryCarePhy").autocomplete({
                source: PrimaryPhy,
                autoFocus:true
            });
            $("#EditassignedProvider").autocomplete({
                source: PrimaryPhy,
                autoFocus:true
            });
            $("#EditsupervisingProvider").autocomplete({
                source: PrimaryPhy,
                autoFocus:true
            });
            $("#EditoperatingProvider").autocomplete({
                source: PrimaryPhy,
                autoFocus:true
            });
            $("#EditotherProvider").autocomplete({
                source: PrimaryPhy,
                autoFocus:true
            });
            $("#EditoutsideProvider").autocomplete({
                source: PrimaryPhy,
                autoFocus:true
            });
            

            var billinCod = <?php include('billingCode.php'); ?>;
            $("#EditcaseBillingCode").autocomplete({
                source: billinCod,
                autoFocus:true
            });
            $("#caseBillingCode").autocomplete({
                source: billinCod,
                autoFocus:true
            });

        $.post("http://curismed.com/medService/cases",
        {
            patientID: temp,
        },
        function(data, status){
            $('#ddlcaseList').html('');
          //iterate over the data and append a select option
          $('#ddlcaseList').append('<option>-- Select Cases --</option>');
          $.each(data, function(key, val){
            $.ajax({
              async : false,
              type: "POST",
              url:"caseGroup.php",
              data:{
                  "caseID" : val.caseID
                  },success:function(result){
                        var res = JSON.parse(result);
                        $('#ddlcaseList').append('<option id="' + val.caseChartNo + '" value="' + val.caseChartNo + '">' + res[0].caseChartNo + ' - ('+res[0].payerName+') - '+res[0].description+' - (Dt : '+changeDateFormat(res[0].startDt)+' & '+changeDateFormat(res[0].endDt)+')</option>');
                    }
                });
          });
          $('#ddlActcaseList').html('');
          $('#ddlActcaseList').append('<option>-- Select Cases --</option>');
          $.each(data, function(key, val){
            $.ajax({
              async : false,
              type: "POST",
              url:"caseGroup.php",
              data:{
                  "caseID" : val.caseID
                  },success:function(result){
                        var res = JSON.parse(result);
                        $('#ddlActcaseList').append('<option id="' + val.caseChartNo + '" value="' + val.caseChartNo + '">' + res[0].caseChartNo + ' - ('+res[0].payerName+') - '+res[0].description+' - (Dt : '+changeDateFormat(res[0].startDt)+' & '+changeDateFormat(res[0].endDt)+')</option>');
                    }
                });
          });
          var ddlVal = $("#ddlActcaseList").val();
            $.ajax({
              async : false,
              type: "POST",
              url:"getCaseIDbyChart.php",
              data:{
                  "caseChartNo" : ddlVal
                  },success:function(result){
                    var caseID = result;
                    $.post("getAuths.php",
                    {
                        caseID: caseID,
                    },
                    function(data, status){
                        $('#ddlAuth').html('');
                      //iterate over the data and append a select option
                      data = JSON.parse(data);
                      $('#ddlAuth').append('<option>-- Select Auth --</option>');
                      $.each(data, function(key, val){
                        $('#ddlAuth').append('<option value="' + val.key + '">' + val.label + '</option>');
                      });
                    });
              }
          });
        });

        $("#btnUpdateIns").click(function(){
          updatePolicy();
        });

        var patFullName = "";
        var patDOB = "";
        $.ajax({
            type: "POST",
            url:"getLocations.php",
            async: false,
            data:{
                patientID: temp
            },
            success:function(data){
                var res = JSON.parse(data);
                if(res[0].home == "1"){
                    document.getElementById('homeChk').checked = true;
                }
                else{
                    document.getElementById('homeChk').checked = false;
                }
                if(res[0].office == "1"){
                    document.getElementById('officeChk').checked = true;
                }
                else{
                    document.getElementById('officeChk').checked = false;
                }
                if(res[0].school == "1"){
                    document.getElementById('schoolChk').checked = true;
                }
                else{
                    document.getElementById('schoolChk').checked = false;
                }
            }
        });
        $.ajax({
            type: "POST",
            url:"http://curismed.com/medService/patients/load",
            async: false,
            data:{
                patientID: temp
            },
            success:function(data){
                var newDate = changeDateFormat(data[0].dob);
                document.title = data[0].fullName + ' | AMROMED Billing Service';
                document.getElementById('fullName').value = data[0].fullName;
                patFullName = data[0].fullName;
                document.getElementById("hdnPatFullName").value = patFullName;
                document.getElementById('accNo').innerHTML = data[0].chartNo;
                document.getElementById('DOBspan').innerHTML = changeDateFormat(data[0].dob);
                patDOB = changeDateFormat(data[0].dob);
                document.getElementById("hdnPatDOB").value = patDOB;
                if(data[0].PrimaryCarePhysician !=null){document.getElementById('rendered').innerHTML = data[0].PrimaryCarePhysician;}else{document.getElementById('rendered').innerHTML = "Not yet Selected";}
                if(data[0].ReferringPhysician != null){document.getElementById('referred').innerHTML = data[0].ReferringPhysician;}else{document.getElementById('referred').innerHTML = "Not yet Selected";}
                //$("#martialStatus").val(data[0].martialStatus);
                $("#employment").val(data[0].employment);
                //$("#reference").val(data[0].reference);
                //$("#employer").val(data[0].employer);
                document.getElementById('dob').value = newDate;
                //document.getElementById('medicalRecord').value = data[0].medicalRecord;
                if(data[0].gender =='M'){
                $("#gender").val("Male");
                }
                else{
                $("#gender").val("Female");
                }
                if(data[0].notifyEmail=='1'){
                $('#notifyEmail').prop('checked',true);
                }
                else{
                $('#notifyEmail').prop('checked',false);
                }
                if(data[0].notifyPhone=='1'){
                $('#notifyPhone').prop('checked',true);
                }
                else{
                $('#notifyPhone').prop('checked',false);
                }
                document.getElementById('ssn').value = data[0].ssn;
                document.getElementById('patientName_top').innerHTML = data[0].fullName;
                document.getElementById('addrStreet1').value = data[0].addrStreet1;
                document.getElementById('addrStreet2').value = data[0].addrStreet2;
                document.getElementById('phoneHome').value = data[0].phoneHome;
                document.getElementById('addrCity').value = data[0].addrCity;
                document.getElementById('phoneWork').value = data[0].phoneWork;
                document.getElementById('addrState').value = data[0].addrState;
                document.getElementById('email').value = data[0].email;
                document.getElementById('phoneMobile').value = data[0].phoneMobile;
                document.getElementById('addrZip').value = data[0].addrZip;
                if(data[0].primaryCarePhy == undefined || data[0].primaryCarePhy == 0){
                    document.getElementById('primaryCarePhy').value = "";
                }
                else{
                    document.getElementById('primaryCarePhy').value = data[0].primaryCarePhy + ' - ' +data[0].PrimaryCarePhysician;
                }
                document.getElementById('defaultServiceLoc').value = data[0].defaultServiceLoc;
                if(data[0].IsFinancialResp == "1"){
                  $('#IsFinancialResp').prop('checked',true);
                  $.ajax({
                    type: "POST",
                    url:"getGuarantorInfo.php",
                    async: false,
                    data:{
                        patientID: temp
                    },
                    success:function(data){
                      data = JSON.parse(data);
                      document.getElementById('guarantorName').value = data[0].guarantorName;
                      document.getElementById('guarantorAddrStreet').value = data[0].guarantorStreet;
                      document.getElementById('guarantorAddrCity').value = data[0].guarantorCity;
                      document.getElementById('guarantorAddrState').value = data[0].guarantorState;
                      document.getElementById('guarantorAddrZip').value = data[0].guarantorZip;
                      $('#guarantorRelation').val(data[0].guarantorRelationship);
                    }
                  });
                }
            }
        });
        $("#divInsured").hide();
        $("#IsInsured").click(function(){
            if($('#IsInsured').prop('checked')){
                $("#divInsured").show();
            }
            else{
                $("#divInsured").hide();
            }
        });

        $("#fullName").click(function(){
            var data = $("#fullName").val();
            var arr = data.split(' ');
            $('#NameModal').modal('show');
            document.getElementById('FirstName').value = arr[0];
            if(arr[1] == undefined){
            document.getElementById('MiddleName').value = '';
        }
        else{
            document.getElementById('MiddleName').value = arr[1];   
        }

            if(arr[2] == undefined){
            document.getElementById('LastName').value = '';
        }
        else{
            document.getElementById('LastName').value = arr[2]; 
        }

        });
        $("#Address").click(function(){
            $('#AddrModal').modal('show');
        });
        $("#linkattachatt").click(function(){
            $('#attach1').modal('show');
        });

         $("#linkPrimaryPhysi").click(function(){
            $('#PrimaryPhysi').modal('show');
        });
        $("#linkRenderPhysi").click(function(){
            $('#PrimaryPhysi').modal('show');
        });
        $("#linkReferPhysi").click(function(){
            $('#PrimaryPhysi').modal('show');
        });

        $("#btnSaveCase").click(function() {
        addCase();
        });

        $("#btnInsurance").click(function(){
            $('#insurance').modal('show');
        });

        $('#ddlcaseList').change(function(){
            var patFullName = document.getElementById("hdnPatFullName").value;
            var patDOB = document.getElementById("hdnPatDOB").value;
            editCase($(this).val(),temp,patFullName,patDOB);
            var ddlVal = $(this).val();
            $.ajax({
              type: "POST",
              url:"getCaseIDbyChart.php",
              data:{
                  "caseChartNo" : ddlVal
                  },success:function(result){
                    var caseID = result;
                    $.post("getAuths.php",
                    {
                        caseID: caseID,
                    },
                    function(data, status){
                      data = JSON.parse(data);
                      //$('#EditauthNo').append('<option>-- Select Auth --</option>');
                      $.each(data, function(key, val){
                        $('#EditauthNo').append('<option value="' + val.key + '">' + val.label + '</option>');
                      });
                    });
              }
          });
        })

        $('#ddlActcaseList').change(function(){
            var ddlVal = $(this).val();
            $.ajax({
              async : false,
              type: "POST",
              url:"getCaseIDbyChart.php",
              data:{
                  "caseChartNo" : ddlVal
                  },success:function(result){
                    var caseID = result;
                    $.post("getAuths.php",
                    {
                        caseID: caseID,
                    },
                    function(data, status){
                        $('#ddlAuth').html('');
                      //iterate over the data and append a select option
                      data = JSON.parse(data);
                      $('#ddlAuth').append('<option>-- Select Auth --</option>');
                      $.each(data, function(key, val){
                        $('#ddlAuth').append('<option value="' + val.key + '">' + val.label + '</option>');
                      });
                    });
              }
          });
            getActivityData();
        });

        // $("#btnEditinsurance").click(function(){
        //     var ddlValue = $('#ddlcaseList').val();
        //     $.post("http://curismed.com/medService/cases/load",
        //     {
        //         caseChartNo: ddlValue,
        //     },
        //     function(data, status){
        //         if(data[0].description != ""){document.getElementById('Editdescription').value = data[0].description;}
        //         if(data[0].authNo != ""){document.getElementById('EditauthNo').value = data[0].authNo;}
        //         if(data[0].lastVisit != ""){if(data[0].lastVisit != '0000-00-00 00:00:00'){document.getElementById('EditlastVisit').value = changeDateFormat(data[0].lastVisit);} else{document.getElementById('EditlastVisit').value = '';}}
        //         if(data[0].noOfVisits != ""){document.getElementById('EditnoOfVisits').value = data[0].noOfVisits;}
        //         if(data[0].principalDiag != ""){document.getElementById('EditprincipalDiag').value = data[0].principalDiag;}
        //         if(data[0].defDiag2 != ""){document.getElementById('EditdefDiag2').value = data[0].defDiag2;}
        //         if(data[0].defDiag3 != ""){document.getElementById('EditdefDiag3').value = data[0].defDiag3;}
        //         if(data[0].defDiag4 != ""){document.getElementById('EditdefDiag4').value = data[0].defDiag4;}
        //     });
        //     $.post("http://curismed.com/medService/policies/load",
        //     {
        //         caseChartNo: ddlValue,
        //         policyEntity : "1"
        //     },
        //     function(data, status){
        //         if(data.length!=0){
        //             var patVal1 = data[0].policyHolder;
        //             $.ajax({
        //                 type: "POST",
        //                 url:"getPatName.php",
        //                 async: false,
        //                 data:{
        //                     "patientID" : patVal1
        //                     },success:function(result){
        //                         var res = JSON.parse(result);
        //                         sessionStorage.setItem("patName1",res[0].fullName);
        //                 }
        //             });
        //             var patName1 = sessionStorage.getItem("patName1");
        //             if(data[0].InsuranceName != ""){document.getElementById('EditpolicyInsurance1').value = data[0].InsuranceName;}
        //             if(data[0].policyHolder != ""){document.getElementById('EditpolicyHolder1').value = temp + ' - ' +patName1;}
        //             if(data[0].relationshipToInsured != ""){document.getElementById('EditpolicyRelationInsured1').value = data[0].relationshipToInsured;}
        //             if(data[0].policyNo != ""){document.getElementById('EditpolicyNo1').value = data[0].policyNo;}
        //             if(data[0].startDt != ""){if(data[0].startDt != '0000-00-00'){document.getElementById('EditpolicyStartDt1').value = changeDateFormat(data[0].startDt);} else{document.getElementById('EditpolicyStartDt1').value = '';}}
        //             if(data[0].groupNo != ""){document.getElementById('EditpolicyGroupNo1').value = data[0].groupNo;}
        //             if(data[0].endDt != ""){if(data[0].endDt != '0000-00-00'){document.getElementById('EditpolicyEndDt1').value = changeDateFormat(data[0].endDt);} else{document.getElementById('EditpolicyEndDt1').value = '';}}
        //             if(data[0].coPayment != ""){document.getElementById('EditpolicyCopay1').value = data[0].coPayment;}    
        //             if(patDOB != ""){document.getElementById('EditpolicyDOB1').value = patDOB;}
                    
        //         }
        //     });

        //     $.post("http://curismed.com/medService/policies/load",
        //     {
        //         caseChartNo: ddlValue,
        //         policyEntity : "2"
        //     },
        //     function(data, status){
        //         if(data.length!=0){
        //             var patVal2 = data[0].policyHolder;
        //             $.ajax({
        //                 type: "POST",
        //                 url:"getPatName.php",
        //                 async: false,
        //                 data:{
        //                     "patientID" : patVal2
        //                     },success:function(result){
        //                         var res = JSON.parse(result);
        //                         sessionStorage.setItem("patName2",res[0].fullName);
        //                 }
        //             });
        //             var patName2 = sessionStorage.getItem("patName1");
        //             if(data[0].InsuranceName != ""){document.getElementById('EditpolicyInsurance2').value = data[0].InsuranceName;}
        //             if(data[0].policyHolder != ""){document.getElementById('EditpolicyHolder2').value = temp + ' - ' +patName2;}
        //             if(data[0].relationshipToInsured != ""){document.getElementById('EditpolicyRelationInsured2').value = data[0].relationshipToInsured;}
        //             if(data[0].policyNo != ""){document.getElementById('EditpolicyNo2').value = data[0].policyNo;}
        //             if(data[0].startDt != ""){if(data[0].startDt != '0000-00-00'){document.getElementById('EditpolicyStartDt2').value = changeDateFormat(data[0].startDt);} else{document.getElementById('EditpolicyStartDt2').value = '';}}
        //             if(data[0].groupNo != ""){document.getElementById('EditpolicyGroupNo2').value = data[0].groupNo;}
        //             if(data[0].endDt != ""){if(data[0].endDt != '0000-00-00'){document.getElementById('EditpolicyEndDt2').value = changeDateFormat(data[0].endDt);} else{document.getElementById('EditpolicyEndDt2').value = '';}}
        //             if(data[0].coPayment != ""){document.getElementById('EditpolicyCopay2').value = data[0].coPayment;}  
        //             if(patDOB != ""){document.getElementById('EditpolicyDOB2').value = patDOB;}  
        //         }
        //     });
        //     $.post("http://curismed.com/medService/policies/load",
        //     {
        //         caseChartNo: ddlValue,
        //         policyEntity : "3"
        //     },
        //     function(data, status){
        //         if(data.length!=0){
        //             var patVal3 = data[0].policyHolder;
        //             $.ajax({
        //                 type: "POST",
        //                 url:"getPatName.php",
        //                 async: false,
        //                 data:{
        //                     "patientID" : patVal3
        //                     },success:function(result){
        //                         var res = JSON.parse(result);
        //                         sessionStorage.setItem("patName3",res[0].fullName);
        //                 }
        //             });
        //             var patName3 = sessionStorage.getItem("patName3");
        //             if(data[0].InsuranceName != ""){document.getElementById('EditpolicyInsurance3').value = data[0].InsuranceName;}
        //             if(data[0].policyHolder != ""){document.getElementById('EditpolicyHolder3').value = temp + ' - ' +patName3;}
        //             if(data[0].relationshipToInsured != ""){document.getElementById('EditpolicyRelationInsured3').value = data[0].relationshipToInsured;}
        //             if(data[0].policyNo != ""){document.getElementById('EditpolicyNo3').value = data[0].policyNo;}
        //             if(data[0].startDt != ""){if(data[0].startDt != '0000-00-00'){document.getElementById('EditpolicyStartDt3').value = changeDateFormat(data[0].startDt);} else{document.getElementById('EditpolicyStartDt2').value = '';}}
        //             if(data[0].groupNo != ""){document.getElementById('EditpolicyGroupNo3').value = data[0].groupNo;}
        //             if(data[0].endDt != ""){if(data[0].endDt != '0000-00-00'){document.getElementById('EditpolicyEndDt3').value = changeDateFormat(data[0].endDt);} else{document.getElementById('EditpolicyEndDt2').value = '';}}
        //             if(data[0].coPayment != ""){document.getElementById('EditpolicyCopay3').value = data[0].coPayment;}    
        //             if(patDOB != ""){document.getElementById('EditpolicyDOB3').value = patDOB;}
        //         }
        //     });
        //     $('#policyHolder1').val(temp + ' - ' +patFullName);
        //     $('#policyDOB1').val(patDOB);
        //     $('#policyHolder2').val(temp + ' - ' +patFullName);
        //     $('#policyDOB2').val(patDOB);
        //     $('#policyHolder3').val(temp + ' - ' +patFullName);
        //     $('#policyDOB3').val(patDOB);
        // });

        $('#EditpolicyRelationInsured1').change(function(){
            if($(this).val() != 'Self'){
                $('#EditpolicyHolder1').val('');
                $('#EditpolicyDOB1').val('');
            }
            else{
                $('#EditpolicyHolder1').val(temp + ' - ' +patFullName);
                $('#EditpolicyDOB1').val(patDOB);
            }
        });
        $('#EditpolicyRelationInsured2').change(function(){
            if($(this).val() != 'Self'){
                $('#EditpolicyHolder2').val('');
                $('#EditpolicyDOB2').val('');
            }
            else{
                $('#EditpolicyHolder2').val(temp + ' - ' +patFullName);
                $('#EditpolicyDOB2').val(patDOB);
            }
        });
        $('#EditpolicyRelationInsured3').change(function(){
            if($(this).val() != 'Self'){
                $('#EditpolicyHolder3').val('');
                $('#EditpolicyDOB3').val('');
            }
            else{
                $('#EditpolicyHolder3').val(temp + ' - ' +patFullName);
                $('#EditpolicyDOB3').val(patDOB);
            }
        });

        $('#policyHolder1').val(temp + ' - ' +patFullName);
        $('#policyDOB1').val(patDOB);
        $('#policyHolder2').val(temp + ' - ' +patFullName);
        $('#policyDOB2').val(patDOB);
        $('#policyHolder3').val(temp + ' - ' +patFullName);
        $('#policyDOB3').val(patDOB);


        $('#policyRelationInsured1').change(function(){
            if($(this).val() != 'Self'){
                $('#policyHolder1').val('');
                $('#policyDOB1').val('');
            }
            else{
                $('#policyHolder1').val(temp + ' - ' +patFullName);
                $('#policyDOB1').val(patDOB);
            }
        });
        $('#policyRelationInsured2').change(function(){
            if($(this).val() != 'Self'){
                $('#policyHolder2').val('');
                $('#policyDOB2').val('');
            }
            else{
                $('#policyHolder2').val(temp + ' - ' +patFullName);
                $('#policyDOB2').val(patDOB);
            }
        });
        $('#policyRelationInsured3').change(function(){
            if($(this).val() != 'Self'){
                $('#policyHolder3').val('');
                $('#policyDOB3').val('');
            }
            else{
                $('#policyHolder3').val(temp + ' - ' +patFullName);
                $('#policyDOB3').val(patDOB);
            }
        });


        $("#btnUpdatePatient").click(function() {
            var fullName = $.trim($("#fullName").val());
            //var martialStatus = $.trim($('#martialStatus').val());
            var ssn = $.trim($('#ssn').val());
            var employment = $.trim($('#employment').val());
            var dob =  convertDate($.trim($('#dob').val()));
            //var employer = $.trim($('#employer').val());
            var gender = $.trim($('#gender').val());
            if(gender == "Male"){
                gender="M";
            }
            else{
                gender = "F";
            }
            //var reference = $.trim($('#reference').val());
            //var medicalRecord = $.trim($('#medicalRecord').val());
            var addrStreet1 = $.trim($('#addrStreet1').val());
            var addrStreet2 = $.trim($('#addrStreet2').val());
            var addrCity = $.trim($('#addrCity').val());
            var addrState = $.trim($('#addrState').val());
            var addrCountry = $.trim($('#addrCountry').val());
            var addrZip = $.trim($('#addrZip').val());
            var phoneHome =  $.trim($('#phoneHome').val());
            var phoneWork = $.trim($('#phoneWork').val());
            var phoneMobile = $.trim($('#phoneMobile').val());
            var email = $.trim($('#email').val());
            var notifyEmail = $.trim($('#notifyEmail').val());
            var notifyPhone = $.trim($('#notifyPhone').val());
            var primaryCarePhy =  $.trim($('#primaryCarePhy').val());
            var n1=primaryCarePhy.indexOf("-");
            if(n1 != -1){
                var PrimaryPhyID = primaryCarePhy.substr(0,n1-1);
            }
            else{
                var PrimaryPhyID = primaryCarePhy;
            }
            var defaultServiceLoc = $.trim($('#defaultServiceLoc').val());
            var ID = temp;

            var homeChk = document.getElementById('homeChk');
            if (homeChk.checked){
             var home="1";
            }else{
             var home="0";
            }

            var officeChk = document.getElementById('officeChk');
            if (officeChk.checked){
             var office="1";
            }else{
             var office="0";
            }

            var schoolChk = document.getElementById('schoolChk');
            if (schoolChk.checked){
             var school="1";
            }else{
             var school="0";
            }

            $.ajax({
                type: "POST",
                url:"updatePatLoc.php",
                data:{ 
                    "patientID" : ID,
                    "home" : home,
                    "office" : office,
                    "school" : school
                },success:function(result){
                 }

            });


            $.ajax({
                type: "POST",
                url:"backend/update_patient.php",
                data:{ 
                    "patientID" : ID,
                    "fullName" : fullName,
                    //"martialStatus" : martialStatus,
                    "ssn" : ssn,
                    "employment" : employment,
                    "dob" : dob,
                    //"employer" : employer,
                    "gender" : gender,
                    //"reference" : reference,
                    //"medicalRecord" : medicalRecord,
                    "addrStreet1" : addrStreet1,
                    "addrStreet2" : addrStreet2,
                    "addrCity" : addrCity,
                    "addrState" : addrState,
                    "addrCountry" : addrCountry,
                    "addrZip" : addrZip,
                    "phoneHome" : phoneHome,
                    "phoneWork" : phoneWork,
                    "phoneMobile" : phoneMobile,
                    "email" : email,
                    "notifyEmail" : notifyEmail,
                    "notifyPhone" : notifyPhone,
                    "primaryCarePhy" : PrimaryPhyID,
                    "defaultServiceLoc" : defaultServiceLoc,
                    },success:function(result){
                        alertify.success('Patient updated successfully');
                        window.location.reload();
                     }

            });
        });
    });

    function getActivityData(){
        var ddlVal = $("#ddlAuth").val();
        $('#btnOpenAct').show();
         $.ajax({
              async : false,
              type: "POST",
              url:"getActivity.php",
              data:{
                authNo: ddlVal
                },success:function(data1){
                    data1 = JSON.parse(data1);
                    var dt = [];
                    $.each(data1,function(i,v) {
                        var startDt = changeDateFormat(data1[i].startDt);
                        var endDt = changeDateFormat(data1[i].endDt);
                        dt.push([data1[i].activityName,startDt,endDt,data1[i].activityID]);
                    });
                    //alert(dt);
                    var table = $('#activityTab').DataTable({
                    destroy: true,
                    "data": dt,
                      columns: [
                       {"title": "Activity Name"},
                      {"title": "Start Date"},
                      {"title": "End Date"},
                      {"title": "Action",
                          "render": function ( data, type, full, meta ) {
                             return '<a href="javascript:void(0);" class="act act'+data+'"><i class="icon icon-pencil"></i></a>';
                          }
                      }
                      ]
                    });
                }
            });
    }

    function generateActivity(){
      var ddlVal = $("#ddlActcaseList").val();
        $.ajax({
          async : false,
          type: "POST",
          url:"getCaseIDbyChart.php",
          data:{
              "caseChartNo" : ddlVal
              },success:function(result){
                var caseID = result;
                $.post("getAuths.php",
                {
                    caseID: caseID,
                },
                function(data, status){
                    $('#ddlAuth').html('');
                  //iterate over the data and append a select option
                  data = JSON.parse(data);
                  $.each(data, function(key, val){
                    $('#ddlAuth').append('<option value="' + val.authNo + '">' + val.authNo + '</option>');
                  });
                });
          }
      });
        getActivityData();
    }

    function editCase(temp1,patientID,patFullName,patDOB){
        var ddlValue = temp1;
        $.post("http://curismed.com/medService/cases/load",
        {
            caseChartNo: ddlValue,
        },
        function(data, status){
            if(data[0].description != ""){$("#Editdescription").val(data[0].description);}
            if(data[0].authNo != ""){
                document.getElementById('EditauthNo').value = data[0].authNo;
                $.post("getAuthinfo.php",
                {
                    authNo: data[0].authNo,
                },
                function(data, status){
                  var n = data.length;
                  data = data.substr(0,n-1);
                  var res = JSON.parse(data);
                  if(res[0].visitsRemaining != ""){document.getElementById('EditnoOfVisits').value = res[0].visitsRemaining;}
                  if(res[0].totalAuthVisits != ""){document.getElementById('EdittotalVisits').value = res[0].totalAuthVisits;}
                  if(res[0].authNo != ""){document.getElementById('EditauthNo').value = res[0].authNo;}
                  if(res[0].tos != ""){document.getElementById('Edittos').value = res[0].tos;}
                  if(res[0].totAuthType != ""){document.getElementById('EdittotAuthType').value = res[0].totAuthType;}
                  if(res[0].startDt != ""){document.getElementById('EditstartDt').value = changeDateFormat(res[0].startDt);}
                  if(res[0].endDt != ""){document.getElementById('EditendDt').value = changeDateFormat(res[0].endDt);}
                });
                $('#EditauthNo').removeAttr('disabled');
                //$('#EditnoOfVisits').removeAttr('disabled');
                $('#EditlastVisit').removeAttr('disabled');
                $('#EditprincipalDiag').removeAttr('disabled');
                $('#EditdefDiag2').removeAttr('disabled');
                $('#EditdefDiag3').removeAttr('disabled');
                $('#EditdefDiag4').removeAttr('disabled');
                $('#uniform-EditchkAuth span').addClass('checked');
            }
            //if(data[0].lastVisit != ""){if(data[0].lastVisit != '0000-00-00 00:00:00'){document.getElementById('EditlastVisit').value = changeDateFormat(data[0].lastVisit);} else{document.getElementById('EditlastVisit').value = '';}}
            //if(data[0].noOfVisits != ""){document.getElementById('EditnoOfVisits').value = data[0].noOfVisits;}
            //if(data[0].totalVisits != ""){document.getElementById('EdittotalVisits').value = data[0].totalVisits;}else if(data[0].totalVisits == -1){document.getElementById('EdittotalVisits').value = "-";}
            if(data[0].principalDiag != ""){
                document.getElementById('EditprincipalDiag').value = data[0].principalDiag;
            }
            if(data[0].defDiag2 != ""){document.getElementById('EditdefDiag2').value = data[0].defDiag2;}
            if(data[0].defDiag3 != ""){document.getElementById('EditdefDiag3').value = data[0].defDiag3;}
            if(data[0].defDiag4 != ""){document.getElementById('EditdefDiag4').value = data[0].defDiag4;}
        });
        $.post("http://curismed.com/medService/policies/load",
        {
            caseChartNo: ddlValue,
            policyEntity : "1"
        },
        function(data, status){
            if(data.length!=0){
                var patVal1 = data[0].policyHolder;
                $.ajax({
                    type: "POST",
                    url:"getPatName.php",
                    async: false,
                    data:{
                        "patientID" : patVal1
                        },success:function(result){
                            var res = JSON.parse(result);
                            sessionStorage.setItem("patName1",res[0].fullName);
                    }
                });
                $.ajax({
                    type: "POST",
                    url:"getInsuranceAddrbyID.php",
                    data:{
                        "insuranceID" : data[0].insuranceID
                        },success:function(result){
                            document.getElementById('spanAddress1').innerHTML = result;
                    }
                });
                var patName1 = sessionStorage.getItem("patName1");
                if(data[0].InsuranceName != ""){document.getElementById('EditpolicyInsurance1').value = data[0].InsuranceName;}
                if(data[0].policyHolder != ""){document.getElementById('EditpolicyHolder1').value = patientID + ' - ' +patName1;}
                if(data[0].relationshipToInsured != ""){document.getElementById('EditpolicyRelationInsured1').value = data[0].relationshipToInsured;}
                if(data[0].policyNo != ""){document.getElementById('EditpolicyNo1').value = data[0].policyNo;}
                if(data[0].startDt != ""){if(data[0].startDt != '0000-00-00'){document.getElementById('EditpolicyStartDt1').value = changeDateFormat(data[0].startDt);} else{document.getElementById('EditpolicyStartDt1').value = '';}}
                if(data[0].groupNo != ""){document.getElementById('EditpolicyGroupNo1').value = data[0].groupNo;}
                if(data[0].endDt != ""){if(data[0].endDt != '0000-00-00'){document.getElementById('EditpolicyEndDt1').value = changeDateFormat(data[0].endDt);} else{document.getElementById('EditpolicyEndDt1').value = '';}}
                if(data[0].coPayment != ""){document.getElementById('EditpolicyCopay1').value = data[0].coPayment;}    
                if(patDOB != ""){document.getElementById('EditpolicyDOB1').value = patDOB;}
                
            }
        });

        $.post("http://curismed.com/medService/policies/load",
        {
            caseChartNo: ddlValue,
            policyEntity : "2"
        },
        function(data, status){
            if(data.length!=0){
                var patVal2 = data[0].policyHolder;
                $.ajax({
                    type: "POST",
                    url:"getPatName.php",
                    async: false,
                    data:{
                        "patientID" : patVal2
                        },success:function(result){
                            var res = JSON.parse(result);
                            sessionStorage.setItem("patName2",res[0].fullName);
                    }
                });
                $.ajax({
                    type: "POST",
                    url:"getInsuranceAddrbyID.php",
                    data:{
                        "insuranceID" : data[0].insuranceID
                        },success:function(result){
                            document.getElementById('spanAddress2').innerHTML = result;
                    }
                });
                var patName2 = sessionStorage.getItem("patName1");
                if(data[0].InsuranceName != ""){document.getElementById('EditpolicyInsurance2').value = data[0].InsuranceName;}
                if(data[0].policyHolder != ""){document.getElementById('EditpolicyHolder2').value = patientID + ' - ' +patName2;}
                if(data[0].relationshipToInsured != ""){document.getElementById('EditpolicyRelationInsured2').value = data[0].relationshipToInsured;}
                if(data[0].policyNo != ""){document.getElementById('EditpolicyNo2').value = data[0].policyNo;}
                if(data[0].startDt != ""){if(data[0].startDt != '0000-00-00'){document.getElementById('EditpolicyStartDt2').value = changeDateFormat(data[0].startDt);} else{document.getElementById('EditpolicyStartDt2').value = '';}}
                if(data[0].groupNo != ""){document.getElementById('EditpolicyGroupNo2').value = data[0].groupNo;}
                if(data[0].endDt != ""){if(data[0].endDt != '0000-00-00'){document.getElementById('EditpolicyEndDt2').value = changeDateFormat(data[0].endDt);} else{document.getElementById('EditpolicyEndDt2').value = '';}}
                if(data[0].coPayment != ""){document.getElementById('EditpolicyCopay2').value = data[0].coPayment;}  
                if(patDOB != ""){document.getElementById('EditpolicyDOB2').value = patDOB;}  
            }
        });
        $.post("http://curismed.com/medService/policies/load",
        {
            caseChartNo: ddlValue,
            policyEntity : "3"
        },
        function(data, status){
            if(data.length!=0){
                var patVal3 = data[0].policyHolder;
                $.ajax({
                    type: "POST",
                    url:"getPatName.php",
                    async: false,
                    data:{
                        "patientID" : patVal3
                        },success:function(result){
                            var res = JSON.parse(result);
                            sessionStorage.setItem("patName3",res[0].fullName);
                    }
                });
                $.ajax({
                    type: "POST",
                    url:"getInsuranceAddrbyID.php",
                    data:{
                        "insuranceID" : data[0].insuranceID
                        },success:function(result){
                            document.getElementById('spanAddress3').innerHTML = result;
                    }
                });
                var patName3 = sessionStorage.getItem("patName3");
                if(data[0].InsuranceName != ""){document.getElementById('EditpolicyInsurance3').value = data[0].InsuranceName;}
                if(data[0].policyHolder != ""){document.getElementById('EditpolicyHolder3').value = patientID + ' - ' +patName3;}
                if(data[0].relationshipToInsured != ""){document.getElementById('EditpolicyRelationInsured3').value = data[0].relationshipToInsured;}
                if(data[0].policyNo != ""){document.getElementById('EditpolicyNo3').value = data[0].policyNo;}
                if(data[0].startDt != ""){if(data[0].startDt != '0000-00-00'){document.getElementById('EditpolicyStartDt3').value = changeDateFormat(data[0].startDt);} else{document.getElementById('EditpolicyStartDt2').value = '';}}
                if(data[0].groupNo != ""){document.getElementById('EditpolicyGroupNo3').value = data[0].groupNo;}
                if(data[0].endDt != ""){if(data[0].endDt != '0000-00-00'){document.getElementById('EditpolicyEndDt3').value = changeDateFormat(data[0].endDt);} else{document.getElementById('EditpolicyEndDt2').value = '';}}
                if(data[0].coPayment != ""){document.getElementById('EditpolicyCopay3').value = data[0].coPayment;}    
                if(patDOB != ""){document.getElementById('EditpolicyDOB3').value = patDOB;}
            }
        });
        $('#policyHolder1').val(patientID + ' - ' +patFullName);
        $('#policyDOB1').val(patDOB);
        $('#policyHolder2').val(patientID + ' - ' +patFullName);
        $('#policyDOB2').val(patDOB);
        $('#policyHolder3').val(patientID + ' - ' +patFullName);
        $('#policyDOB3').val(patDOB);
    }

    function reloadCases(temp){
        $.post("http://curismed.com/medService/cases",
        {
            patientID: temp,
        },
        function(data, status){
            $('#ddlcaseList').html('');
          //iterate over the data and append a select option
          $('#ddlcaseList').append('<option>-- Select Cases --</option>');
          $.each(data, function(key, val){
            $.ajax({
              async : false,
              type: "POST",
              url:"caseGroup.php",
              data:{
                  "caseID" : val.caseID
                  },success:function(result){
                        var res = JSON.parse(result);
                        $('#ddlcaseList').append('<option id="' + val.caseChartNo + '" value="' + val.caseChartNo + '">' + res[0].caseChartNo + ' - (Insurance : '+res[0].payerName+') - '+res[0].description+' - (Auth : '+res[0].authNo+') - (Dated : '+res[0].startDt+' & '+res[0].endDt+')</option>');
                    }
                });
          });
          $('#ddlActcaseList').html('');
          //iterate over the data and append a select option
          //$('#ddlActcaseList').append('<option>-- Select Cases --</option>');
          $.each(data, function(key, val){
            $('#ddlActcaseList').append('<option value="' + val.caseChartNo + '" id="' + val.caseChartNo + '">' + val.caseChartNo + '</option>');
          });
          var ddlVal = $("#ddlActcaseList").val();
            $.ajax({
              async : false,
              type: "POST",
              url:"getCaseIDbyChart.php",
              data:{
                  "caseChartNo" : ddlVal
                  },success:function(result){
                    var caseID = result;
                    $.post("getAuths.php",
                    {
                        caseID: caseID,
                    },
                    function(data, status){
                        $('#ddlAuth').html('');
                      //iterate over the data and append a select option
                      data = JSON.parse(data);
                      $('#ddlAuth').append('<option>-- Select Auth --</option>');
                      $.each(data, function(key, val){
                        $('#ddlAuth').append('<option value="' + val.authNo + '">' + val.authNo + '</option>');
                      });
                    });
              }
          });
        });
    }

    var convertDate = function(usDate) {
      var dateParts = usDate.split(/(\d{1,2})-(\d{1,2})-(\d{4})/);
      return dateParts[3] + "-" + dateParts[1] + "-" + dateParts[2];
    }

    function authDetails(){
      var authNo = document.getElementById('EditauthNo').value;
      $.post("getAuthinfo.php",
      {
          authNo: authNo,
      },
      function(data, status){
        var n = data.length;
        data = data.substr(0,n-1);
        var res = JSON.parse(data);
        if(res[0].visitsRemaining != ""){document.getElementById('EditnoOfVisits').value = res[0].visitsRemaining;}
        if(res[0].totalAuthVisits != ""){document.getElementById('EdittotalVisits').value = res[0].totalAuthVisits;}
        if(res[0].authNo != ""){document.getElementById('EditauthNo').value = res[0].authNo;}
        if(res[0].tos != ""){document.getElementById('Edittos').value = res[0].tos;}
        if(res[0].totAuthType != ""){document.getElementById('EdittotAuthType').value = res[0].totAuthType;}
        if(res[0].startDt != ""){document.getElementById('EditstartDt').value = changeDateFormat(res[0].startDt);}
        if(res[0].endDt != ""){document.getElementById('EditendDt').value = changeDateFormat(res[0].endDt);}
      });
    }

    function addCase(){
        var PatID = sessionStorage.getItem("patientId");
        var description = $('#description').val();
        var authNo = $('#authNo').val();
        var tos = $('#tos').val();
        var AuthstartDt = convertDate($('#AuthstartDt').val());
        var AuthendDt = convertDate($('#AuthendDt').val());
        var AuthtotalVisits = $('#AuthtotalVisits').val();
        var totAuthType = $('#totAuthType').val();

        var lastVisit = convertDate($('#lastVisit').val());
        // if($('#totalVisits').val() == ""){
        //     var totalVisits = -1;
        // }
        // else{
        //     var totalVisits = $('#totalVisits').val();
        // }
        var principalDiag = $('#principalDiag').val();
        var defDiag2 = $('#defDiag2').val();
        var defDiag3 = $('#defDiag3').val();
        var defDiag4 = $('#defDiag4').val();
        
        var policyInsurance1 = $('#policyInsurance1').val();
        var policyHolder1 = getHolderID($('#policyHolder1').val());
        var policyRelationInsured1 = $('#policyRelationInsured1').val();
        var policyNo1 = $('#policyNo1').val();
        var policyStartDt1 = convertDate($('#policyStartDt1').val());
        var policyGroupNo1 = $('#policyGroupNo1').val();
        var policyEndDt1 = convertDate($('#policyEndDt1').val());
        var policyClaimNo1 = $('#policyClaimNo1').val();
        var policyCopay1 = $('#policyCopay1').val();    

        var policyInsurance2 = $('#policyInsurance2').val();
        var policyHolder2 = getHolderID($('#policyHolder2').val());
        var policyRelationInsured2 = $('#policyRelationInsured2').val();
        var policyNo2 = $('#policyNo2').val();
        var policyStartDt2 = convertDate($('#policyStartDt2').val());
        var policyGroupNo2 = $('#policyGroupNo2').val();
        var policyEndDt2 = convertDate($('#policyEndDt2').val());
        var policyClaimNo2 = $('#policyClaimNo2').val();        
        var policyCopay2 = $('#policyCopay2').val();

        var policyInsurance3 = $('#policyInsurance3').val();
        var policyHolder3 = getHolderID($('#policyHolder3').val());
        var policyRelationInsured3 = $('#policyRelationInsured3').val();
        var policyNo3 = $('#policyNo3').val();
        var policyStartDt3 = convertDate($('#policyStartDt3').val());
        var policyGroupNo3 = $('#policyGroupNo3').val();
        var policyEndDt3 = convertDate($('#policyEndDt3').val());
        var policyClaimNo3 = $('#policyClaimNo3').val();        
        var policyCopay3 = $('#policyCopay3').val();    
        
        if(policyInsurance1 != ""){
            $.ajax({
                type: "POST",
                url:"getInsuranceID.php",
                async: false,
                data:{
                    "payerName" : policyInsurance1
                    },success:function(result){
                        sessionStorage.setItem("insurance1",result);
                }
            });
        }
        if(policyInsurance2 != ""){
            $.ajax({
                type: "POST",
                url:"getInsuranceID.php",
                async: false,
                data:{
                    "payerName" : policyInsurance2
                    },success:function(result){
                        sessionStorage.setItem("insurance2",result);
                }
            });
        }
         if(policyInsurance3 != ""){
            $.ajax({
                type: "POST",
                url:"getInsuranceID.php",
                async: false,
                data:{
                    "payerName" : policyInsurance3
                    },success:function(result){
                        sessionStorage.setItem("insurance3",result);
                }
            });
        }

        $.ajax({
            async : false,
            type: "POST",
            url:"http://curismed.com/medService/cases/create",
            data:{
                "patientID" : PatID,
                "description" : description,
                "authNo" : authNo,
                "lastVisit" : lastVisit,
                //"noOfVisits" : totalVisits,
                //"totalVisits" : totalVisits,
                "principalDiag" : principalDiag,
                "defDiag2" : defDiag2,
                "defDiag3" : defDiag3,
                "defDiag4" : defDiag4,
            },success:function(result){
                $.ajax({
                    async : false,
                    type: "POST",
                    url:"addAuth.php",
                    data:{
                        "caseID" : result.CaseID,
                        "patientID" : PatID,
                        "authNo" : authNo,
                        "tos" : tos,
                        "startDt" : AuthstartDt,
                        "endDt" : AuthendDt,
                        "totAuthType" : totAuthType,
                        "totalVisits" : AuthtotalVisits,
                        "totalRemaining" : AuthtotalVisits
                        },success:function(result){
                            console.log(result);
                    }
                });
                alertify.success("Case successfully Created");
            }
        });

        $.ajax({
            type: "POST",
            url:"chartNoList.php",
            async: false,
            data:{
                "patientID" : PatID
            },success:function(result){
                var hdnCaseChartNo = result;
                resultNew = hdnCaseChartNo.split(',');
                sessionStorage.setItem("tempCaseChartNo", resultNew[0]);
                sessionStorage.setItem("tempClaimID", resultNew[1]);
            }
        });    
        var tempCaseChartNo = sessionStorage.getItem("tempCaseChartNo");
        var tempClaimID = sessionStorage.getItem("tempClaimID");
        var insurance1 = sessionStorage.getItem("insurance1");
        var insurance2 = sessionStorage.getItem("insurance2");
        var insurance3 = sessionStorage.getItem("insurance3");
        if(policyInsurance1 != "" && policyNo1 != ""){
            $.ajax({
                type: "POST",
                url:"http://curismed.com/medService/policies/create",
                async: false,
                data:{
                    "policyEntity" : "1",
                    "caseID" : tempClaimID,
                    "caseChartNo" : tempCaseChartNo,
                    "insuranceID" : insurance1,
                    "policyHolder" : policyHolder1,
                    "relationshipToInsured" : policyRelationInsured1,
                    "policyNo" : policyNo1,
                    "groupNo" : policyGroupNo1,
                    "claimNo" : policyClaimNo1,
                    "startDt" : policyStartDt1,
                    "endDt" : policyEndDt1,
                    "coPayment" : policyCopay1,
                },success:function(result){
                    alertify.success("Primary Policy added successfully");
                    $('#tab2').removeClass('active');
                    $('#tab3').addClass('active');
                    $('#tab_3_2').removeClass('active');
                    $('#tab_3_3').addClass('active');
                    reloadCases(PatID);
                }
            });
        }
        if(policyInsurance2 != "" && policyNo2 != ""){
            $.ajax({
                type: "POST",
                url:"http://curismed.com/medService/policies/create",
                async: false,
                data:{
                    "policyEntity" : "2",
                    "caseID" : tempClaimID,
                    "caseChartNo" : tempCaseChartNo,
                    "insuranceID" : insurance2,
                    "policyHolder" : policyHolder2,
                    "relationshipToInsured" : policyRelationInsured2,
                    "policyNo" : policyNo2,
                    "groupNo" : policyGroupNo2,
                    "claimNo" : policyClaimNo2,
                    "startDt" : policyStartDt2,
                    "endDt" : policyEndDt2,
                    "coPayment" : policyCopay2,
                },success:function(result){
                    alertify.success("Secondary Policy added successfully");
                    $('#tab2').removeClass('active');
                    $('#tab3').addClass('active');
                    $('#tab_3_2').removeClass('active');
                    $('#tab_3_3').addClass('active');
                    reloadCases(PatID);
                }
            });
        }
        if(policyInsurance3 != "" && policyNo3 != ""){
            $.ajax({
                type: "POST",
                url:"http://curismed.com/medService/policies/create",
                async: false,
                data:{
                    "policyEntity" : "3",
                    "caseID" : tempClaimID,
                    "caseChartNo" : tempCaseChartNo,
                    "insuranceID" : insurance3,
                    "policyHolder" : policyHolder3,
                    "relationshipToInsured" : policyRelationInsured3,
                    "policyNo" : policyNo3,
                    "groupNo" : policyGroupNo3,
                    "claimNo" : policyClaimNo3,
                    "startDt" : policyStartDt3,
                    "endDt" : policyEndDt3,
                    "coPayment" : policyCopay3,
                },success:function(result){
                    alertify.success("Teritiary Policy added successfully");
                    $('#tab2').removeClass('active');
                    $('#tab3').addClass('active');
                    $('#tab_3_2').removeClass('active');
                    $('#tab_3_3').addClass('active');
                    reloadCases(PatID);
                }
            });
        }
    }

    function updatePolicy(){
      var tempCaseChartNo = $("#ddlcaseList").val();
      var policyInsurance1 = $('#EditpolicyInsurance1').val();
      var policyHolder1 = getHolderID($('#EditpolicyHolder1').val());
      var policyRelationInsured1 = $('#EditpolicyRelationInsured1').val();
      var policyNo1 = $('#EditpolicyNo1').val();
      var policyStartDt1 = convertDate($('#EditpolicyStartDt1').val());
      var policyGroupNo1 = $('#EditpolicyGroupNo1').val();
      var policyEndDt1 = convertDate($('#EditpolicyEndDt1').val());
      var policyClaimNo1 = $('#EditpolicyClaimNo1').val();
      var policyCopay1 = $('#EditpolicyCopay1').val();    

      var policyInsurance2 = $('#EditpolicyInsurance2').val();
      var policyHolder2 = getHolderID($('#EditpolicyHolder2').val());
      var policyRelationInsured2 = $('#EditpolicyRelationInsured2').val();
      var policyNo2 = $('#EditpolicyNo2').val();
      var policyStartDt2 = convertDate($('#EditpolicyStartDt2').val());
      var policyGroupNo2 = $('#EditpolicyGroupNo2').val();
      var policyEndDt2 = convertDate($('#EditpolicyEndDt2').val());
      var policyClaimNo2 = $('#EditpolicyClaimNo2').val();        
      var policyCopay2 = $('#EditpolicyCopay2').val();

      var policyInsurance3 = $('#EditpolicyInsurance3').val();
      var policyHolder3 = getHolderID($('#EditpolicyHolder3').val());
      var policyRelationInsured3 = $('#EditpolicyRelationInsured3').val();
      var policyNo3 = $('#EditpolicyNo3').val();
      var policyStartDt3 = convertDate($('#EditpolicyStartDt3').val());
      var policyGroupNo3 = $('#EditpolicyGroupNo3').val();
      var policyEndDt3 = convertDate($('#EditpolicyEndDt3').val());
      var policyClaimNo3 = $('#EditpolicyClaimNo3').val();        
      var policyCopay3 = $('#EditpolicyCopay3').val();    
      if(policyInsurance1 != ""){
            $.ajax({
                type: "POST",
                url:"getInsuranceID.php",
                async: false,
                data:{
                    "payerName" : policyInsurance1
                    },success:function(result){
                        sessionStorage.setItem("insurance1",result);
                }
            });
        }
        if(policyInsurance2 != ""){
            $.ajax({
                type: "POST",
                url:"getInsuranceID.php",
                async: false,
                data:{
                    "payerName" : policyInsurance2
                    },success:function(result){
                        sessionStorage.setItem("insurance2",result);
                }
            });
        }
         if(policyInsurance3 != ""){
            $.ajax({
                type: "POST",
                url:"getInsuranceID.php",
                async: false,
                data:{
                    "payerName" : policyInsurance3
                    },success:function(result){
                        sessionStorage.setItem("insurance3",result);
                }
            });
        }
        var insurance1 = sessionStorage.getItem("insurance1");
        var insurance2 = sessionStorage.getItem("insurance2");
        var insurance3 = sessionStorage.getItem("insurance3");
      if(policyInsurance1 != "" && policyNo1 != ""){
          $.ajax({
              type: "POST",
              url:"updatePolicy.php",
              async: false,
              data:{
                  "policyEntity" : "1",
                  "caseChartNo" : tempCaseChartNo,
                  "insuranceID" : insurance1,
                  "policyHolder" : policyHolder1,
                  "relationshipToInsured" : policyRelationInsured1,
                  "policyNo" : policyNo1,
                  "groupNo" : policyGroupNo1,
                  "claimNo" : policyClaimNo1,
                  "startDt" : policyStartDt1,
                  "endDt" : policyEndDt1,
                  "coPayment" : policyCopay1,
              },success:function(result){
                  alertify.success("Primary Policy Updated successfully");
                  $('#tab2').removeClass('active');
                    $('#tab3').addClass('active');
                    $('#tab_3_2').removeClass('active');
                    $('#tab_3_3').addClass('active');
                    reloadCases(PatID);
              }
          });
      }
      if(policyInsurance2 != "" && policyNo2 != ""){
          $.ajax({
              type: "POST",
              url:"updatePolicy.php",
              async: false,
              data:{
                  "policyEntity" : "2",
                  "caseChartNo" : tempCaseChartNo,
                  "insuranceID" : insurance2,
                  "policyHolder" : policyHolder2,
                  "relationshipToInsured" : policyRelationInsured2,
                  "policyNo" : policyNo2,
                  "groupNo" : policyGroupNo2,
                  "claimNo" : policyClaimNo2,
                  "startDt" : policyStartDt2,
                  "endDt" : policyEndDt2,
                  "coPayment" : policyCopay2,
              },success:function(result){
                  alertify.success("Secondary Policy Updated successfully");
                  $('#tab2').removeClass('active');
                    $('#tab3').addClass('active');
                    $('#tab_3_2').removeClass('active');
                    $('#tab_3_3').addClass('active');
                    reloadCases(PatID);
              }
          });
      }
      if(policyInsurance3 != "" && policyNo3 != ""){
          $.ajax({
              type: "POST",
              url:"updatePolicy.php",
              async: false,
              data:{
                  "policyEntity" : "3",
                  "caseChartNo" : tempCaseChartNo,
                  "insuranceID" : insurance3,
                  "policyHolder" : policyHolder3,
                  "relationshipToInsured" : policyRelationInsured3,
                  "policyNo" : policyNo3,
                  "groupNo" : policyGroupNo3,
                  "claimNo" : policyClaimNo3,
                  "startDt" : policyStartDt3,
                  "endDt" : policyEndDt3,
                  "coPayment" : policyCopay3,
              },success:function(result){
                  alertify.success("Teritiary Policy Updated successfully");
                  $('#tab2').removeClass('active');
                    $('#tab3').addClass('active');
                    $('#tab_3_2').removeClass('active');
                    $('#tab_3_3').addClass('active');
                    reloadCases(PatID);
              }
          });
      }
    }

    function pad(number, length) {
   
    var str = '' + number;
    while (str.length < length) {
        str = '0' + str;
    }
   
    return str;

}

    function checkAuth(){
        if($('#EditchkAuth').is(':checked') == true){
            $('#EditauthNo').removeAttr('disabled');
            //$('#EditnoOfVisits').removeAttr('disabled');
            //$('#EdittotalVisits').removeAttr('disabled');
            $('#EditlastVisit').removeAttr('disabled');
            $('#EditprincipalDiag').removeAttr('disabled');
            $('#EditdefDiag2').removeAttr('disabled');
            $('#EditdefDiag3').removeAttr('disabled');
            $('#EditdefDiag4').removeAttr('disabled');
        }
        else{
            $('#EditauthNo').attr('disabled',true);
            $('#EditnoOfVisits').attr('disabled',true);
            $('#EdittotalVisits').attr('disabled',true);
            $('#EditlastVisit').attr('disabled',true);
            $('#EditprincipalDiag').attr('disabled',true);
            $('#EditdefDiag2').attr('disabled',true);
            $('#EditdefDiag3').attr('disabled',true);
            $('#EditdefDiag4').attr('disabled',true);
        }
    }

    function getHolderID(name1){
        var name = name1;
            if(name != ""){
                var n=name.indexOf("-");
                var truncID = name.substr(0,n);
            }
        return truncID;
    }

    var convertDate = function(usDate) {
        if(usDate != "" && usDate != null && usDate != undefined){
            var dateParts = usDate.split(/(\d{1,2})-(\d{1,2})-(\d{4})/);
            return dateParts[3] + "-" + dateParts[1] + "-" + dateParts[2];
        }
    }

    </script>
    <style>
    .ui-autocomplete-input {
  border: none; 
  font-size: 14px;

  height: 24px;
  margin-bottom: 5px;
  padding-top: 2px;
  border: 1px solid #DDD !important;
  padding-top: 0px !important;
  position: relative;
}
.ui-menu .ui-menu-item a {
  font-size: 12px;
  color:#fff;
}
.ui-autocomplete {
  position: absolute;
  top: 0;
  left: 0;
  z-index: 1510 !important;
  float: left;
  display: none;
  min-width: 160px;
  width: 160px;
  padding: 4px 0;
  margin: 2px 0 0 0;
  list-style: none;
  background-color: #ffffff;
  border-color: #ccc;
  border-color: rgba(0, 0, 0, 0.2);
  border-style: solid;
  border-width: 1px;
  -webkit-border-radius: 2px;
  -moz-border-radius: 2px;
  border-radius: 2px;
  -webkit-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
  -moz-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
  box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
  -webkit-background-clip: padding-box;
  -moz-background-clip: padding;
  background-clip: padding-box;
  *border-right-width: 2px;
  *border-bottom-width: 2px;
}
.ui-menu-item > a.ui-corner-all {
    display: block;
    padding: 3px 15px;
    clear: both;
    font-weight: normal;
    line-height: 18px;
    color:#000;
    white-space: nowrap;
    text-decoration: none;
}
.ui-state-hover {
      color: #fff;
      text-decoration: none;
      background-color: #0088cc;
      border-radius: 0px;
      -webkit-border-radius: 0px;
      -moz-border-radius: 0px;
      background-image: none;
}
.ui-state-active {
      color: #fff;
      text-decoration: none;
      background-color: #0088cc;
      border-radius: 0px;
      -webkit-border-radius: 0px;
      -moz-border-radius: 0px;
      background-image: none;
}

    .icon:hover{
        text-decoration: none;
    }
    a:hover{
        text-decoration: none;
    }
    </style>
    

</head>

<body>

    <!-- Header -->
    <header class="header navbar navbar-fixed-top" role="banner" style="background-image: url('assets/bg.jpg'); background-repeat: repeat-x;">
        <!-- Top Navigation Bar -->
        <div class="container">

            <!-- Only visible on smartphones, menu toggle -->
            <ul class="nav navbar-nav">
                <li class="nav-toggle"><a href="javascript:void(0);" title=""><i class="icon-reorder"></i></a></li>
            </ul>

            <!-- Logo -->
            <a class="navbar-brand" style="text-align:center;padding-right:50px;" href="index.html">
                <img src="assets/logo2.png"/>
                <strong>medABA</strong>
            </a>
            <!-- /logo -->

            <!-- Sidebar Toggler -->
            <a href="#" class="toggle-sidebar bs-tooltip" data-placement="bottom" data-original-title="Toggle navigation">
                <i class="icon-reorder"></i>
            </a>
            <!-- /Sidebar Toggler -->

            <!-- Top Left Menu -->
            <ul class="nav navbar-nav navbar-left hidden-xs hidden-sm" style="list-style:none;">
                <li>
                    <a href="ViewPatient.php" class="dropdown-toggle">
                        Clients
                    </a>
                </li>
                <li>
                    <a href="scheduler.php" class="dropdown-toggle">
                        Scheduler
                    </a>
                </li>
                <li>
                    <a href="Deposit.php" class="dropdown-toggle">
                        Deposit
                    </a>
                </li>
                <li>
                    <a href="Ledger.php" class="dropdown-toggle">
                        Ledger
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                        EDI
                        <i class="icon-caret-down small"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="generateAllEDI.php">
                            <i class="icon-angle-right"></i>
                            Generate EDI
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="EDIList.php">
                            <i class="icon-angle-right"></i>
                            EDI List
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="charts.html" class="dropdown-toggle" data-toggle="dropdown">
                        Settings
                        <i class="icon-caret-down small"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="ViewPractice.php">
                            <i class="icon-angle-right"></i>
                            Practices
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="ViewPhysician.php">
                            <i class="icon-angle-right"></i>
                            Physicians
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="ViewServiceLoc.php">
                            <i class="icon-angle-right"></i>
                            Locations &amp; Facilities
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="javascript:void(0);" style="font-weight:600;">
                            Codes
                            </a>
                            <li>
                                <a href="ViewCPT.php">
                                <i class="icon-angle-right"></i>
                                CPT/Procedure
                                </a>
                            </li>
                            <li>
                                <a href="ViewDX.php">
                                <i class="icon-angle-right"></i>
                                ICD 10/ICD 9 Library
                                </a>
                            </li>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="insurances.php">
                            <i class="icon-angle-right"></i>
                            Insurances
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- /Top Left Menu -->

            <!-- Top Right Menu -->
            <ul class="nav navbar-nav navbar-right">
                <!-- Notifications -->
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-warning-sign"></i>
                        <span class="badge">5</span>
                    </a>
                    <ul class="dropdown-menu extended notification">
                        <li class="title">
                            <p>You have 5 new notifications</p>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="label label-success"><i class="icon-plus"></i></span>
                                <span class="message">New user registration.</span>
                                <span class="time">1 mins</span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="label label-danger"><i class="icon-warning-sign"></i></span>
                                <span class="message">High CPU load on cluster #2.</span>
                                <span class="time">5 mins</span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="label label-success"><i class="icon-plus"></i></span>
                                <span class="message">New user registration.</span>
                                <span class="time">10 mins</span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="label label-info"><i class="icon-bullhorn"></i></span>
                                <span class="message">New items are in queue.</span>
                                <span class="time">25 mins</span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="label label-warning"><i class="icon-bolt"></i></span>
                                <span class="message">Disk space to 85% full.</span>
                                <span class="time">55 mins</span>
                            </a>
                        </li>
                        <li class="footer">
                            <a href="javascript:void(0);">View all notifications</a>
                        </li>
                    </ul>
                </li>

                <!-- Tasks -->
                <li class="dropdown hidden-xs hidden-sm">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-tasks"></i>
                        <span class="badge">7</span>
                    </a>
                    <ul class="dropdown-menu extended notification">
                        <li class="title">
                            <p>You have 7 pending tasks</p>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="task">
                                    <span class="desc">Preparing new release</span>
                                    <span class="percent">30%</span>
                                </span>
                                <div class="progress progress-small">
                                    <div style="width: 30%;" class="progress-bar progress-bar-info"></div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="task">
                                    <span class="desc">Change management</span>
                                    <span class="percent">80%</span>
                                </span>
                                <div class="progress progress-small progress-striped active">
                                    <div style="width: 80%;" class="progress-bar progress-bar-danger"></div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="task">
                                    <span class="desc">Mobile development</span>
                                    <span class="percent">60%</span>
                                </span>
                                <div class="progress progress-small">
                                    <div style="width: 60%;" class="progress-bar progress-bar-success"></div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="task">
                                    <span class="desc">Database migration</span>
                                    <span class="percent">20%</span>
                                </span>
                                <div class="progress progress-small">
                                    <div style="width: 20%;" class="progress-bar progress-bar-warning"></div>
                                </div>
                            </a>
                        </li>
                        <li class="footer">
                            <a href="javascript:void(0);">View all tasks</a>
                        </li>
                    </ul>
                </li>

                <!-- Messages -->
                <li class="dropdown hidden-xs hidden-sm">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-envelope"></i>
                        <span class="badge">1</span>
                    </a>
                    <ul class="dropdown-menu extended notification">
                        <li class="title">
                            <p>You have 3 new messages</p>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="photo"><img src="assets/img/demo/avatar-1.jpg" alt="" /></span>
                                <span class="subject">
                                    <span class="from">Bob Carter</span>
                                    <span class="time">Just Now</span>
                                </span>
                                <span class="text">
                                    Consetetur sadipscing elitr...
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="photo"><img src="assets/img/demo/avatar-2.jpg" alt="" /></span>
                                <span class="subject">
                                    <span class="from">Jane Doe</span>
                                    <span class="time">45 mins</span>
                                </span>
                                <span class="text">
                                    Sed diam nonumy...
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="photo"><img src="assets/img/demo/avatar-3.jpg" alt="" /></span>
                                <span class="subject">
                                    <span class="from">Patrick Nilson</span>
                                    <span class="time">6 hours</span>
                                </span>
                                <span class="text">
                                    No sea takimata sanctus...
                                </span>
                            </a>
                        </li>
                        <li class="footer">
                            <a href="javascript:void(0);">View all messages</a>
                        </li>
                    </ul>
                </li>

                <!-- .row .row-bg Toggler -->
                <li>
                    <a href="#" class="dropdown-toggle row-bg-toggle">
                        <i class="icon-resize-vertical"></i>
                    </a>
                </li>


                <!-- User Login Dropdown -->
                <li class="dropdown user">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <!--<img alt="" src="assets/img/avatar1_small.jpg" />-->
                        <i class="icon-male"></i>
                        <span class="username" id="practiceName"></span>
                        <i class="icon-caret-down small"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="javascript:void(0);"><i class="icon-user"></i> My Profile</a></li>
                        <li><a href="javascript:void(0);"><i class="icon-calendar"></i> My Calendar</a></li>
                        <li><a href="javascript:void(0);"><i class="icon-tasks"></i> My Tasks</a></li>
                        <li class="divider"></li>
                        <li><a href="login.php"><i class="icon-key"></i> Log Out</a></li>
                    </ul>
                </li>
                <!-- /user login dropdown -->
            </ul>
            <!-- /Top Right Menu -->
        </div>
        <!-- /top navigation bar -->

    </header> <!-- /.header -->

  <div id="container">
    <!-- Breadcrumbs line -->
    <div class="crumbs">
      <ul id="breadcrumbs" class="breadcrumb">
        <li class="current">
          <i class="icon-home"></i>
          <a href="index.html">Dashboard</a>
        </li>
        
      </ul>
      <a href="javascript:void(0);"><img src="assets/icons/Settings.png" title="Settings" style="float:right; padding-top:0.3em; padding-right:2em;"/></a>
          <a href="Ledger.php"><img src="assets/icons/Claim_search.png" title="Claim Search" style="float:right; padding-top:0.3em; padding-right:2em;"/></a>
          <a href="AddClaim.php"><img src="assets/icons/Claim_add.png" title="Add Claim" style="float:right; padding-top:0.3em; padding-right:2em;"/></a>
          <a href="ViewPatient.php"><img src="assets/icons/Patient_search.png" title="Patient Search" style="float:right; padding-top:0.3em; padding-right:2em;"/></a>
          <a href="AddPatient.php"><img src="assets/icons/Patient_add.png" title="Add Patient" style="float:right; padding-top:0.3em; padding-right:2em;"/></a>
          <a href="dashboard.html"><img src="assets/icons/Home.png" title="Home" style="float:right; padding-top:0.3em; padding-right:2em;"/></a>

    </div>
    <div class="clearfix"></div>
                <!-- /Breadcrumbs line -->
                <!--=== Page Content ===-->
                <!--=== Modals ===-->

                <div class="row">
                    <div class="row col-md-9" style="border:1px solid #ddd; margin-left:3px; color:#45aed6; font-weight:600;">
                        <div class="col-md-4" style="margin-top:10px;">
                            <span style="font-size:24px; color: #45aed6;" id="patientName_top"></span><br/>
                            <span>Date of Birth : <span id="DOBspan"></span></span> <br/> 
                            <span>Insurance : SELF</span> 
                        </div>
                        <div class="col-md-4" style="margin-top:15px;">
                            <span>Account # &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <span id="accNo"></span></span> <br/>
                            <span>Rendered By &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <span id="rendered"> </span></span> <br/>
                            <span>Referred By &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <span id="referred"> </span></span> <br/>
                        </div>
                        <div class="col-md-4" style="margin-top:15px;">
                            <span>Last Statement : 2015-10-12</span> <br/>
                            <span>Last Payment &nbsp;&nbsp; : 2015-11-12</span> <br/>
                            <span>Last Charge &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: 2015-12-12</span> <br/>
                        </div>
                    </div>
                    <div class="row col-md-3" style="border-top:1px solid #ddd;border-bottom:1px solid #ddd;border-right:1px solid #ddd; margin-left:3px; padding-top:5px; color:#45aed6; font-weight:600;">
                        <div class="col-md-12">
                            <h3 style="margin-top:10px;padding:0">Total Outstanding</h3>
                            <span style="color:maroon">Insurance : $1000.00</span> <br/>
                            <span style="color:green">Patient : $869.00</span> <br/>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="alert alert-warning fade in" style="margin-top:10px;" id="divAlert">
                        <i class="icon-remove close" data-dismiss="alert"></i>
                        <strong>Warning!</strong> <span id="alertTop">Your Payment Due is on 05/22/2016</span>
                    </div>
                    <div class="col-md-12" style="margin-top:20px;">
                    <div class="widget box" >
                        <div class="widget-content">
                            <div class="tabbable tabbable-custom tabs-left">
                                <!-- Only required for left/right tabs -->
                                <ul class="nav nav-tabs tabs-left">
                                    <li id="tab1" class="active"><a href="#tab_3_1" data-toggle="tab">Client Info</a></li>
                                    <li id="tab2"><a href="#tab_3_2" data-toggle="tab">Insurance</a></li>
                                    <li id="tab3"><a href="#tab_3_3" data-toggle="tab">Activity</a></li>
                                    <li id="tab4"><a href="#tab_3_4" data-toggle="tab">Accounts</a></li>
                                    <li id="tab5"><a href="#tab_3_5" data-toggle="tab">Alerts</a></li>
                                    <li id="tab6"><a href="#tab_3_6" data-toggle="tab">Documents</a></li>
                                    <li id="tab7"><a href="#tab_3_7" data-toggle="tab">Log</a></li>
                                </ul>
                                <div class="tab-content" style="background-color:aliceblue;">
                                    <div class="tab-pane active" id="tab_3_1">
                                        <div class="modal fade" id="NameModal">
                                                <div class="modal-dialog" style="width:40%">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                            <h4 class="modal-title">Check Full Name</h4>
                                                        </div>
                                                        <div class="modal-body" style="min-height:150px;">
                                                            <div class="form-group col-md-12" style="padding-bottom:5%;">
                                                        <label class="control-label col-md-6">Title </label>
                                                        <div class="col-md-6">
                                                            <select class="form-control">
                                                                <option></option>
                                                                <option>Mr.</option>
                                                                <option>Ms.</option>
                                                                <option>Mrs.</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-12" style="padding-bottom:5%;">
                                                        <label class="control-label col-md-6">First Name </label>
                                                        <div class="col-md-6">
                                                            <input type="text" class="form-control required" id="FirstName" name="txtFirstName" placeholder="Enter the First Name" />
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-12" style="padding-bottom:5%;">
                                                        <label class="control-label col-md-6">Middle Name </label>
                                                        <div class="col-md-6">
                                                            <input type="text" class="form-control required" id="MiddleName" name="txtMiddleName" placeholder="Enter the Middle Name" />
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-12" style="padding-bottom:5%;">
                                                        <label class="control-label col-md-6">Last Name </label>
                                                        <div class="col-md-6">
                                                            <input type="text" class="form-control required" id="LastName" name="txtLastName" placeholder="Enter the Last Name" />
                                                        </div>
                                                    </div>
                                                    </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                            <button type="button" class="btn btn-primary" data-dismiss="modal">Save changes</button>
                                                        </div>
                                                    </div><!-- /.modal-content -->
                                                </div><!-- /.modal-dialog -->
                                            </div>

                                            <div class="modal fade" id="PrimaryPhysi" tab-index="-1">
                                                <div class="modal-dialog" style="width:70%">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                            <h4 class="modal-title">Add Physician</h4>
                                                        </div>
                                                        <div class="modal-body" style="min-height:150px; padding-bottom:110px;">
                                                            <div class="form-group col-md-6">
                                                                <label class="control-label col-md-4">Full Name </label>
                                                                <div class="col-md-8">
                                                                    <input type="text" class="form-control required" id="PrimaryPhysiName" name="PrimaryPhysiName" placeholder="Name of the Physiciam" value="" />
                                                                </div>
                                                            </div>

                                                            <div class="form-group col-md-6">
                                                                <label class="control-label col-md-4">Speciality </label>
                                                                <div class="col-md-8">
                                                                    <select class="form-control" id="phyEmploymentStatus">
                                                                        <option></option>
                                                                        <option>Psychologist</option>
                                                                        <option>Counselor</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="clearfix"></div>

                                                            <div class="form-group col-md-6">
                                                                <label class="control-label col-md-4">Date Of Birth </label>
                                                                <div class="col-md-8">
                                                                    <input type="text" class="form-control required" id="phyDOB" name="txtPTNumber" placeholder="Date Of Birth of the Physiciam" value="" />
                                                                </div>
                                                            </div>

                                                            <div class="form-group col-md-6">
                                                                <label class="control-label col-md-4">Degree </label>
                                                                <div class="col-md-8">
                                                                    <input type="text" class="form-control required" id="phyDegree" name="txtPTNumber" placeholder="Degree of the Physiciam" value="" />
                                                                </div>
                                                            </div>

                                                            <div class="form-group col-md-6">
                                                                <label class="control-label col-md-4">Individual NPI </label>
                                                                <div class="col-md-8">
                                                                    <input type="text" class="form-control required" id="phyNPI" name="txtPTNumber" placeholder="Individual NPI of the Physiciam" value="" />
                                                                </div>
                                                            </div>

                                                            <div class="form-group col-md-6">
                                                                <label class="control-label col-md-4">Type </label>
                                                                <div class="col-md-8">
                                                                    <input type="text" class="form-control required" id="phyType" name="txtPTNumber" placeholder="Type of the Physiciam" disabled value="" />
                                                                </div>
                                                            </div>

                                                            <div class="form-group col-md-6">
                                                                <label class="control-label col-md-4">Social Security # </label>
                                                                <div class="col-md-8">
                                                                    <input type="text" class="form-control required" id="phySSN" name="txtPTNumber" placeholder="SSN of the Physiciam" value="" />
                                                                </div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <hr/>
                                                            <div class="form-group col-md-6">
                                                                <label class="control-label col-md-4">Address </label>
                                                                <div class="col-md-8">
                                                                    <textarea class="form-control" cols="5" rows="4" id="phyAddr"></textarea>
                                                                </div>
                                                            </div>

                                                            <div class="form-group col-md-6" >
                                                                <label class="control-label col-md-4">Home</label>
                                                                <div class="col-md-8">
                                                                     <input type="text" class="form-control required" id="phyHomePhone" name="txtPTNumber" placeholder="Enter the Home Phone" value="" />
                                                                </div>
                                                            </div>

                                                             <div class="form-group col-md-6">
                                                                <label class="control-label col-md-4">Work </label>
                                                                <div class="col-md-8">
                                                                    <input type="text" class="form-control required" id="phyWorkPhone" name="txtPTNumber" placeholder="Enter the Work Phone" value="" />
                                                                </div>
                                                            </div>

                                                            <div class="form-group col-md-6" >
                                                                <label class="control-label col-md-4">Mobile </label>
                                                                <div class="col-md-8">
                                                                    <input type="text" class="form-control required" id="phyMobilePhone" name="txtPTNumber" placeholder="Enter the Mobile Phone" value="" />
                                                                </div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <div class="form-group col-md-6">
                                                                <label class="control-label col-md-4">Email </label>
                                                                <div class="col-md-8">
                                                                    <input type="text" class="form-control required" id="phyEmail" name="txtPTNumber" placeholder="Enter the Email" value="" />
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <label class="control-label col-md-4">Fax </label>
                                                                <div class="col-md-8">
                                                                    <input type="text" class="form-control required" id="phyFax" name="txtPTNumber" placeholder="Enter the Fax" value="" />
                                                                </div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <hr/>
                                                            <div class="form-group col-md-12">
                                                                <label class="control-label col-md-2">Notes </label>
                                                                <div class="col-md-8">
                                                                    <textarea class="form-control" cols="20" rows="6" id="phyNotes"></textarea>
                                                                </div>
                                                            </div>

                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                            <input type="button" value="Add Physician" class="btn btn-primary" id="btnAddPriPhy"/>
                                                        </div>
                                                    </div><!-- /.modal-content -->
                                                </div><!-- /.modal-dialog -->
                                            </div>

                                            <div class="modal fade" id="AddrModal">
                                                <div class="modal-dialog" style="width:40%">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                            <h4 class="modal-title">Check Address</h4>
                                                        </div>
                                                        <div class="modal-body" style="min-height:150px;">
                                                            <div class="form-group col-md-12" style="padding-bottom:5%;">
                                                                <label class="control-label col-md-6">First Name </label>
                                                                <div class="col-md-6">
                                                                    <input type="text" class="form-control required" id="FirstName" name="txtFirstName" placeholder="Enter the First Name" />
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-md-12" style="padding-bottom:5%;">
                                                                <label class="control-label col-md-6">Middle Name </label>
                                                                <div class="col-md-6">
                                                                    <input type="text" class="form-control required" id="MiddleName" name="txtMiddleName" placeholder="Enter the Middle Name" />
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-md-12" style="padding-bottom:5%;">
                                                                <label class="control-label col-md-6">Last Name </label>
                                                                <div class="col-md-6">
                                                                    <input type="text" class="form-control required" id="LastName" name="txtLastName" placeholder="Enter the Last Name" />
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-md-12" style="padding-bottom:5%;">
                                                                <label class="control-label col-md-6"></label>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                            <button type="button" class="btn btn-primary" data-dismiss="modal">Save changes</button>
                                                        </div>
                                                    </div><!-- /.modal-content -->
                                                </div><!-- /.modal-dialog -->
                                            </div>
                                        <form class="form-horizontal" id="UpdatePatientValues" method="post" >
                                            <input type="hidden" id="_hdnID" />
                                            <input type="hidden" id="hdnPatFullName" />
                                            <input type="hidden" id="hdnPatDOB" />
                                            <div class="tab-content">
                                                    <div class="tab-pane active" id="tab_1_1">
                                                        <div class="form-group col-md-6">
                                                <label class="control-label col-md-4">Full Name </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control required" id="fullName" name="txtPTNumber" placeholder="Enter the Name of the Patient" value="<?php echo $fullName; ?>" />
                                                </div>
                                            </div>

                                            <div class="form-group col-md-6">
                                                <div class="col-md-2"></div>
                                                <div class="make-switch" data-on="success" data-on-label="Active" data-off-label="Inactive" data-off="warning">
                                                    <input type="checkbox" checked class="toggle"/>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>

                                            <div class="form-group col-md-6">
                                                <label class="control-label col-md-4">Social Security # </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" id="ssn" name="txtPTNumber" data-mask="999-99-9999" placeholder="Enter the Social Security #" value="<?php echo $ssn; ?>" />
                                                </div>
                                            </div>

                                            <!-- <div class="form-group col-md-6">
                                                <label class="control-label col-md-4">Martial Status </label>
                                                <div class="col-md-8">
                                                    <select class="form-control" id="martialStatus">
                                                        <option value=""></option>
                                                        <option value="Annulled">Annulled</option>
                                                        <option value="Divorced">Divorced</option>
                                                        <option value="Interlocutory">Interlocutory</option>
                                                        <option value="LegallySeparated">LegallySeparated</option>
                                                        <option value="Married">Married</option>
                                                        <option value="Polygamous">Polygamous</option>
                                                        <option value="NeverMarried">NeverMarried</option>
                                                        <option value="DomesticPartner">DomesticPartner</option>
                                                        <option value="Widowed">Widowed</option>
                                                    </select>
                                                </div>
                                            </div> -->
                                            <div class="form-group col-md-6">
                                                <label class="control-label col-md-4">Date Of Birth </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control required" id="dob" name="txtPTNumber" placeholder="Enter the Date Of Birth" value="<?php echo $dob;?>" />
                                                </div>
                                            </div>

                                            <div class="form-group col-md-6">
                                                <label class="control-label col-md-4">Employment Status </label>
                                                <div class="col-md-8">
                                                    <select class="form-control" id="employment">
                                                        <option></option>
                                                        <option>Employed</option>
                                                        <option>Self-Employed</option>
                                                        <option>Retired</option>
                                                        <option>Student. Full-time</option>
                                                        <option>Student. Part-time</option>
                                                        <option>Unknown</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group col-md-6">
                                                <label class="control-label col-md-4">Gender </label>
                                                <div class="col-md-8">
                                                    <select class="form-control" id="gender">
                                                        <option value=""></option>
                                                        <option value="Male">Male</option>
                                                        <option value="Female">Female</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <!-- <div class="form-group col-md-6">
                                                <label class="control-label col-md-4">Employer </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control required" id="employer" name="employer" />
                                                </div>
                                            </div> -->

                                            <!-- <div class="form-group col-md-6">
                                                <label class="control-label col-md-4">Medical Record # </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control required" id="medicalRecord" name="txtPTNumber" placeholder="Enter the Client Code" value="" />
                                                </div>
                                            </div> -->

                                            <!-- <div class="form-group col-md-6">
                                                <label class="control-label col-md-4">Referral Source </label>
                                                <div class="col-md-8">
                                                    <select class="form-control" id="reference">
                                                        <option value="0">Not Specified</option>
                                                        <option value="1">Attorney</option>
                                                        <option value="2">Brouchure</option>
                                                        <option value="3">Case Manager</option>
                                                        <option value="4">Chiropractor</option>
                                                        <option value="5">Emergency room</option>
                                                        <option value="6">Friend / Family</option>
                                                        <option value="7">Insurance</option>
                                                        <option value="8">Nurse</option>
                                                        <option value="9">Online Ad</option>
                                                        <option value="10">Other Source #1</option>
                                                        <option value="11">Other Source #2</option>
                                                        <option value="12">Other Source #3</option>
                                                        <option value="13">Other Source #4</option>
                                                        <option value="13">Other Source #5</option>
                                                        <option value="14">Patient Seminar</option>
                                                        <option value="15">Physical Therapist</option>
                                                        <option value="16">Physician</option>
                                                        <option value="17">Physician's Assistant</option>
                                                        <option value="18">Previous Patient</option>
                                                        <option value="19">Print Ad</option>
                                                        <option value="20">Radio Ad</option>
                                                        <option value="21">Search Engine</option>
                                                        <option value="22">Website</option>
                                                        <option value="23">Worker's Comp</option>
                                                        <option value="24">Yellow Pages</option>
                                                    </select>
                                                </div>
                                            </div> -->

                                        <div class="clearfix"></div>

                                                        <div class="form-group col-md-6">
                                            <label class="control-label col-md-6" style="text-align:left;"><span style="color:blue">Contact Information</span></label><hr>
                                        </div><hr>
                                        <div class="clearfix"></div>

                                        <div class="form-group col-md-6">
                                            <label class="control-label col-md-4">Street 1</label>
                                            <div class="col-md-8">
                                                 <input type="text" class="form-control required" id="addrStreet1" name="txtPTNumber" placeholder="Enter the Street address" value="" />
                                            </div>
                                        </div>

                                        <div class="form-group col-md-6">
                                            <label class="control-label col-md-4">Street 2</label>
                                            <div class="col-md-8">
                                                 <input type="text" class="form-control required" id="addrStreet2" name="txtPTNumber" placeholder="Enter the Street address" value="" />
                                            </div>
                                        </div>



                                        <div class="form-group col-md-6">
                                            <label class="control-label col-md-4">City </label>
                                            <div class="col-md-8">
                                                 <input type="text" class="form-control required" id="addrCity" disabled name="txtPTNumber" placeholder="City" value="" />
                                            </div>
                                        </div>

                                        
                                        <div class="form-group col-md-6">
                                            <label class="control-label col-md-4">State </label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control required" id="addrState" disabled name="txtPTNumber" placeholder="State" value="" />
                                            </div>
                                        </div>

                                        <div class="form-group col-md-6">
                                            <label class="control-label col-md-4">Zipcode </label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control required" id="addrZip" name="txtPTNumber" placeholder="Enter the Zipcode" value="" />
                                            </div>
                                        </div>
                                        
                                        <div class="form-group col-md-6">
                                            <label class="control-label col-md-4">Home #</label>
                                            <div class="col-md-8">
                                                 <input type="text" class="form-control required" id="phoneHome" name="txtPTNumber" placeholder="Enter the Home Phone" value="<?php echo $phoneHome;?>" />
                                            </div>
                                        </div>

                                        <div class="form-group col-md-6">
                                            <label class="control-label col-md-4">Work # </label>
                                            <div class="col-md-8">
                                                 <input type="text" class="form-control required" id="phoneWork" name="txtPTNumber" placeholder="Enter the Work Phone" value="<?php echo $phoneWork;?>" />
                                            </div>
                                        </div>

                                        <div class="form-group col-md-6">
                                            <label class="control-label col-md-4">Cell # </label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control required" id="phoneMobile" name="txtPTNumber" placeholder="Enter the Mobile Phone" value="<?php echo $phoneMobile;?>" />
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>

                                        <div class="form-group col-md-6">

                                            <label class="checkbox-inline col-md-5">
                                                <input type="checkbox" class="uniform" style="margin-left:5px;" value="" id="notifyEmail"> &nbsp;&nbsp;Send Email Notification
                                            </label>
                                            <div class="col-md-7">
                                                <input type="text" class="form-control required" id="email" name="txtPTNumber" placeholder="Enter the Email Address" value="<?php echo $email;?>" />
                                            </div>

                                        </div>

                                        <div class="form-group col-md-6">
                                            <div class="col-md-1"></div>
                                            <label class="checkbox-inline col-md-8">
                                                <input type="checkbox" class="uniform" value="" id="notifyPhone"> Enable Auto Phone call Remainders
                                            </label>
                                        </div>

                                        <div class="clearfix"></div>
                                                    
                                                        <div class="form-group col-md-6">
                                            <label class="control-label col-md-6" style="text-align:left;"><span style="color:blue">Providers Information</span></label><hr>
                                        </div><hr>
                                        <div class="clearfix"></div>

                                        <div class="form-group col-md-6">
                                            <label class="control-label col-md-5">Default Billing Provider</label>
                                            <div class="col-md-6">
                                                 <input type="text" class="form-control required" id="primaryCarePhy" name="DRenderProvider" placeholder="Enter the Rendering Provider" />
                                            </div>

                                                <!-- <a href="#" id="linkRenderPhysi"><i class="icon icon-plus" style="font-size:20px; color:maroon"></i></a> -->

                                        </div>
                                        
                                        <div class="form-group col-md-6">
                                            <label class="control-label col-md-5"> Service Locations </label>
                                            <div class="col-md-6">
                                                <input type="text" class="form-control required" id="defaultServiceLoc" name="defaultServiceLoc" placeholder="Enter the Service Location" />
                                            </div>
                                            <!-- <a href="#" id="linkRenderPhysi"><i class="icon icon-plus" style="font-size:20px; color:maroon"></i></a> -->
                                            
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-group col-md-6">
                                            <label class="control-label col-md-5"></label>
                                            <div class="col-md-6">
                                            </div>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label class="control-label col-md-2"></label>
                                            <label class="checkbox-inline">
                                                <input type="checkbox" class="uniform" id="homeChk" value="11"> Home
                                            </label>
                                            <label class="checkbox-inline">
                                                <input type="checkbox" class="uniform" id="officeChk" value="12"> Office
                                            </label>
                                            <label class="checkbox-inline">
                                                <input type="checkbox" class="uniform" id="schoolChk" value="3"> School
                                            </label>
                                        </div>
                                        <div class="clearfix"></div>
                                                <div class="form-group col-md-6">
                                            <label class="control-label col-md-6" style="text-align:left;"><span style="color:blue">Guarantor<span></label>
                                        </div>
                                        <div class="clearfix"></div>

                                        <div class="form-group col-md-12">
                                            <div class="col-md-1"></div>
                                            <label class="checkbox-inline col-md-8">
                                                <input type="checkbox" class="uniform" value="" id="IsFinancialResp"> Personally Financially responsible(a.k.a. Guarantor) is different than Patient
                                            </label>
                                        </div>
                                        <div class="clearfix"></div><br/>
                                        
                                                </div>
                                            </div>
                                            <div style="text-align: right">
                                                <input class="btn btn-primary" value="Update Patient" id="btnUpdatePatient" type="button">
                                                <a href="ViewPatient.php"><input type="button" class="btn btn-default" value="Cancel" /></a>
                                            </div>

                                    </div>
                                    <div class="tab-pane " id="tab_3_2">
                                        <div class="col-md-10">
                                            <div class="col-md-6">
                                                <select id="ddlcaseList" style="height:32px;" class="col-md-6 form-control"></select>
                                            </div>
                                            <div class="col-md-4">
                                                <!-- <input type="button" id="btnEditinsurance" class="btn btn-primary" value="Edit Case" />&nbsp;&nbsp; -->
                                                <input type="button" id="btnInsurance" class="btn btn-primary" value="Create New Case" />
                                            </div>
                                            <div class="clearfix"></div>
                                            <br/>
                                            <div class="panel-group" id="Editaccordion">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h3 class="panel-title">
                                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#Editaccordion" href="#EditcollapseOne">
                                                        Primary Policy Info </a>
                                                        </h3>
                                                    </div>
                                                    <div id="EditcollapseOne" class="panel-collapse collapse in">
                                                        <div class="panel-body">
                                                            <input type="hidden" id="hdnEdittoggleDX" />
                                                            <div class="form-group col-md-6">
                                                                <label class="control-label col-md-6">Insurance </label>
                                                                <div class="col-md-6">
                                                                    <input type="text" id="EditpolicyInsurance1" class="form-control"/>
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-md-4">
                                                                <span id="spanAddress1" style="font-weight:600;"></span>
                                                            </div>
                                                            <div class="form-group col-md-2">
                                                                <!-- <div class="col-md-12" style="padding-left:0">
                                                                    <label class="checkbox-inline">
                                                                        <input type="checkbox" class="uniform" id="EditchkAuth" value=""> Authorization
                                                                    </label>
                                                                </div> -->
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <div class="form-group col-md-6">
                                                                <label class="control-label col-md-6">Patient's Relationship </label>
                                                                <div class="col-md-6">
                                                                    <select class="form-control" id="EditpolicyRelationInsured1">
                                                                        <option>Self</option>
                                                                        <option>Spouse</option>
                                                                        <option>Other</option>
                                                                        <option>Child</option>
                                                                        <option>Grandfather Or Grandmother</option>
                                                                        <option>Grandson Or Granddaughter</option>
                                                                        <option>Nephew Or Niece</option>
                                                                        <option>Adopted Child</option>
                                                                        <option>Foster Child</option>
                                                                        <option>Stepson</option>
                                                                        <option>Ward</option>
                                                                        <option>Stepdaughter</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <label class="control-label col-md-6">Description </label>
                                                                <div class="col-md-6">
                                                                    <input type="text" id="Editdescription" class="form-control"/>
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <label class="control-label col-md-6">Policy Holder </label>
                                                                <div class="col-md-6">
                                                                    <input type="text" id="EditpolicyHolder1" class="form-control"/>
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <label class="control-label col-md-6">Auth # </label>
                                                                <div class="col-md-5">
                                                                    <select class="form-control" disabled id="EditauthNo"></select>
                                                                </div>
                                                                <div class="col-md-1" style="padding:0px;">
                                                                    <a href="javascript:void(0);" title="Edit Existing Auth#" disabled style="float:left; padding-right:3px;" id="linkEditAuth"><i class="icon icon-pencil"></i></a>
                                                                  <a href="javascript:void(0);" title="Add New Auth#" disabled style="float:left;" id="linkAddAuth"><i class="icon icon-plus"></i></a>
                                                                </div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <div class="form-group col-md-6">
                                                                <label class="control-label col-md-6">DOB </label>
                                                                <div class="col-md-6">
                                                                    <input type="text" id="EditpolicyDOB1" class="form-control"/>
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <label class="control-label col-md-6">Type of Service </label>
                                                                <div class="col-md-6">
                                                                    <select id="Edittos" class="form-control">
                                                                        <option></option>
                                                                        <option value="Behaviour Therapy">Behaviour Therapy</option>
                                                                        <option value="Physical Therapy">Physical Therapy</option>
                                                                        <option value="Occupational Therapy">Occupational Therapy</option>
                                                                        <option value="Speech Therapy">Speech Therapy</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <label class="control-label col-md-6">Policy ID </label>
                                                                <div class="col-md-6">
                                                                    <input type="text" id="EditpolicyNo1" class="form-control"/>
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <label class="control-label col-md-6">Total Auth Type </label>
                                                                <div class="col-md-6">
                                                                    <select class="form-control" id="EdittotAuthType">
                                                                    <option value=""></option>
                                                                    <option value="Week">Week</option>
                                                                    <option value="Total Auth">Total Auth</option>
                                                                    <option value="Session">Session</option>
                                                                </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <label class="control-label col-md-6">Policy Start </label>
                                                                <div class="col-md-6">
                                                                    <input type="text" id="EditpolicyStartDt1" class="form-control"/>
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                               <label class="control-label col-md-4">Total Auth Visits </label>
                                                                <div class="col-md-2">
                                                                    <input type="text" id="EdittotalVisits" class="form-control" disabled/>
                                                                </div>
                                                                <label class="control-label col-md-4">Visits Remaining </label>
                                                                <div class="col-md-2">
                                                                    <input type="text" id="EditnoOfVisits" class="form-control" disabled/>
                                                                </div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <div class="form-group col-md-6">
                                                                <label class="control-label col-md-6">Policy End </label>
                                                                <div class="col-md-6">
                                                                    <input type="text" id="EditpolicyEndDt1" class="form-control"/>
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                 <label class="control-label col-md-3">Start Date </label>
                                                                <div class="col-md-3">
                                                                    <input type="text" id="EditstartDt" class="form-control" disabled/>
                                                                </div>
                                                                <label class="control-label col-md-3">End Date </label>
                                                                <div class="col-md-3">
                                                                    <input type="text" id="EditendDt" class="form-control" disabled/>
                                                                </div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <div class="form-group col-md-6">
                                                                <label class="control-label col-md-6">Group ID </label>
                                                                <div class="col-md-6">
                                                                    <input type="text" id="EditpolicyGroupNo1" class="form-control"/>
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-md-3">
                                                                <div class="col-md-12">
                                                                    <input type="text" id="EditprincipalDiag" placeholder="Principal Diag" class="form-control" disabled/>
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-md-3">
                                                                <div class="col-md-12">
                                                                    <input type="text" id="EditdefDiag2" placeholder="Default Diagnosis 2" class="form-control" disabled/>
                                                                </div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <div class="form-group col-md-6">
                                                                <label class="control-label col-md-6">Group Name </label>
                                                                <div class="col-md-6">
                                                                    <input type="text" id="EditpolicyGroupName1" class="form-control"/>
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-md-3">
                                                                <div class="col-md-12">
                                                                    <input type="text" id="EditdefDiag3" placeholder="Default Diagnosis 3" class="form-control" disabled/>
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-md-3">
                                                                <div class="col-md-12">
                                                                    <input type="text" id="EditdefDiag4" placeholder="Default Diagnosis 4" class="form-control" disabled/>
                                                                </div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <div class="form-group col-md-6">
                                                                <label class="control-label col-md-6">Copay </label>
                                                                <div class="col-md-6">
                                                                    <input type="text" id="EditpolicyCopay1" class="form-control"/>
                                                                </div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h3 class="panel-title">
                                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#Editaccordion" href="#EditcollapseTwo">
                                                        Secondary Policy Info </a>
                                                        </h3>
                                                    </div>
                                                    <div id="EditcollapseTwo" class="panel-collapse collapse">
                                                        <div class="panel-body">
                                                            <div class="form-group col-md-6">
                                                                <label class="control-label col-md-6">Insurance </label>
                                                                <div class="col-md-6">
                                                                    <input type="text" id="EditpolicyInsurance2" class="form-control"/>
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <span id="spanAddress2" style="font-weight:600;"></span>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <div class="form-group col-md-6">
                                                                <label class="control-label col-md-6">Patient's Relationship </label>
                                                                <div class="col-md-6">
                                                                    <select class="form-control" id="EditpolicyRelationInsured2">
                                                                        <option>Self</option>
                                                                        <option>Spouse</option>
                                                                        <option>Other</option>
                                                                        <option>Child</option>
                                                                        <option>Grandfather Or Grandmother</option>
                                                                        <option>Grandson Or Granddaughter</option>
                                                                        <option>Nephew Or Niece</option>
                                                                        <option>Adopted Child</option>
                                                                        <option>Foster Child</option>
                                                                        <option>Stepson</option>
                                                                        <option>Ward</option>
                                                                        <option>Stepdaughter</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <label class="control-label col-md-6">Policy Holder </label>
                                                                <div class="col-md-6">
                                                                    <input type="text" id="EditpolicyHolder2" class="form-control"/>
                                                                </div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <div class="form-group col-md-6">
                                                                <label class="control-label col-md-6">DOB </label>
                                                                <div class="col-md-6">
                                                                    <input type="text" id="EditpolicyDOB2" class="form-control"/>
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <label class="control-label col-md-6">Policy ID </label>
                                                                <div class="col-md-6">
                                                                    <input type="text" id="EditpolicyNo2" class="form-control"/>
                                                                </div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <div class="form-group col-md-6">
                                                                <label class="control-label col-md-6">Policy Start </label>
                                                                <div class="col-md-6">
                                                                    <input type="text" id="EditpolicyStartDt2" class="form-control"/>
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <label class="control-label col-md-6">Policy End </label>
                                                                <div class="col-md-6">
                                                                    <input type="text" id="EditpolicyEndDt2" class="form-control"/>
                                                                </div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <div class="form-group col-md-6">
                                                                <label class="control-label col-md-6">Group ID </label>
                                                                <div class="col-md-6">
                                                                    <input type="text" id="EditpolicyGroupNo2" class="form-control"/>
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <label class="control-label col-md-6">Group Name </label>
                                                                <div class="col-md-6">
                                                                    <input type="text" id="EditpolicyGroupName2" class="form-control"/>
                                                                </div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <div class="form-group col-md-6">
                                                                <label class="control-label col-md-6">Copay </label>
                                                                <div class="col-md-6">
                                                                    <input type="text" id="EditpolicyCopay2" class="form-control"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h3 class="panel-title">
                                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#Editaccordion" href="#EditcollapseThree">
                                                        Teritiary Policy Info </a>
                                                        </h3>
                                                    </div>
                                                    <div id="EditcollapseThree" class="panel-collapse collapse">
                                                        <div class="panel-body">
                                                            <div class="form-group col-md-6">
                                                                <label class="control-label col-md-6">Insurance </label>
                                                                <div class="col-md-6">
                                                                    <input type="text" id="EditpolicyInsurance3" class="form-control"/>
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <span id="spanAddress3" style="font-weight:600;"></span>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <div class="form-group col-md-6">
                                                                <label class="control-label col-md-6">Patient's Relationship </label>
                                                                <div class="col-md-6">
                                                                    <select class="form-control" id="EditpolicyRelationInsured3">
                                                                        <option>Self</option>
                                                                        <option>Spouse</option>
                                                                        <option>Other</option>
                                                                        <option>Child</option>
                                                                        <option>Grandfather Or Grandmother</option>
                                                                        <option>Grandson Or Granddaughter</option>
                                                                        <option>Nephew Or Niece</option>
                                                                        <option>Adopted Child</option>
                                                                        <option>Foster Child</option>
                                                                        <option>Stepson</option>
                                                                        <option>Ward</option>
                                                                        <option>Stepdaughter</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <label class="control-label col-md-6">Policy Holder </label>
                                                                <div class="col-md-6">
                                                                    <input type="text" id="EditpolicyHolder3" class="form-control"/>
                                                                </div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <div class="form-group col-md-6">
                                                                <label class="control-label col-md-6">DOB </label>
                                                                <div class="col-md-6">
                                                                    <input type="text" id="EditpolicyDOB3" class="form-control"/>
                                                                </div>
                                                            </div>
                                                            
                                                            <div class="form-group col-md-6">
                                                                <label class="control-label col-md-6">Policy ID </label>
                                                                <div class="col-md-6">
                                                                    <input type="text" id="EditpolicyNo3" class="form-control"/>
                                                                </div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <div class="form-group col-md-6">
                                                                <label class="control-label col-md-6">Policy Start </label>
                                                                <div class="col-md-6">
                                                                    <input type="text" id="EditpolicyStartDt3" class="form-control"/>
                                                                </div>
                                                            </div>
                                                            
                                                            <div class="form-group col-md-6">
                                                                <label class="control-label col-md-6">Policy End </label>
                                                                <div class="col-md-6">
                                                                    <input type="text" id="EditpolicyEndDt3" class="form-control"/>
                                                                </div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <div class="form-group col-md-6">
                                                                <label class="control-label col-md-6">Group ID </label>
                                                                <div class="col-md-6">
                                                                    <input type="text" id="EditpolicyGroupNo3" class="form-control"/>
                                                                </div>
                                                            </div>
                                                            
                                                            <div class="form-group col-md-6">
                                                                <label class="control-label col-md-6">Group Name </label>
                                                                <div class="col-md-6">
                                                                    <input type="text" id="EditpolicyGroupName3" class="form-control"/>
                                                                </div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <div class="form-group col-md-6">
                                                                <label class="control-label col-md-6">Copay </label>
                                                                <div class="col-md-6">
                                                                    <input type="text" id="EditpolicyCopay3" class="form-control"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div><br/>
                                        <div class="col-md-10">
                                          <input type="button" class="btn btn-primary pull-right" id="btnUpdateIns" value="Update Insurance">
                                        </div>
                                        <div class="clearfix"></div>
                                        <br/>
                                    </div>
                                    <div class="tab-pane" id="tab_3_3">
                                        <input type="hidden" id="hdnAuth" />
                                        <input type="hidden" id="hdncaseID" />
                                        <div class="form-group col-md-6">
                                          <label class="control-label col-md-6">Insurance </label>
                                            <div class="col-md-6">
                                              <select id="ddlActcaseList" class="form-control"></select>
                                            </div>
                                        </div>
                                         <div class="form-group col-md-6">
                                            <input type="button" id="btnOpenAct" value="Create New Activity" class="btn btn-primary">
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-group col-md-6">
                                            <label class="control-label col-md-6">Authorization </label>
                                            <div class="col-md-6">
                                                <select class="form-control" id="ddlAuth">
                                                </select>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="col-md-12">
                                            <table id="activityTab" class="table-bordered" style="width:100%">
                                            </table>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tab_3_4">
                                        <table id="accountTable" class="table-bordered"></table><br/><br/>
                                        <input type="button" id="createClaim" class="btn btn-primary pull-right" value="Create Claim">
                                    </div>
                                    <div class="tab-pane" id="tab_3_5">
                                        <div class="form-group col-md-6">
                                            <label class="control-label col-md-6" style="text-align:left;">Alert Message :</label>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-group col-md-12">
                                            <div class="col-md-12">
                                                <textarea class="form-control" id="alertMsg" rows="5" cols="5"></textarea>
                                            </div>
                                            <br/>
                                            <div class="col-md-12">
                                                <input type="button" id="saveAlert" class="btn btn-primary pull-right" value="Create Alert">
                                            </div>
                                        </div>
                                        <div class="form-group col-md-12">
                                            <u>Show Alert Message when :</u>
                                        </div>
                                        <div class="col-md-10">
                                            <label class="checkbox">
                                                <input type="checkbox" class="uniform" id="chkalerts_EditPatient" value=""> Displaying Patient Details
                                            </label>
                                            <label class="checkbox">
                                                <input type="checkbox" class="uniform" id="chkalerts_ScheduleApp" value=""> Scheduling Appointments
                                            </label>
                                            <label class="checkbox">
                                                <input type="checkbox" class="uniform" id="chkalerts_Charge" value=""> Entering Encounters
                                            </label>
                                            <label class="checkbox">
                                                <input type="checkbox" class="uniform" id="chkalerts_ViewClaims" value=""> Viewing Claim Details
                                            </label>
                                            <label class="checkbox">
                                                <input type="checkbox" class="uniform" id="chkalerts_Deposits" value=""> Posting Payments
                                            </label>
                                            <label class="checkbox">
                                                <input type="checkbox" class="uniform" id="chkalerts_Statements" value=""> Preparing Patient Statements
                                            </label>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tab_3_6">
                                        <div class="form-group col-md-9">
                                        <label class="checkbox">
                                                <input type="checkbox" class="uniform" value=""> Show documents attached to all records associated with this patient
                                        </label>
                                        </div>
                                        <div class="form-group col-md-3">
                                          <a href="#" id="linkUpload" class="btn btn-primary" style="text-decoration:none"><i class="icon icon-upload-alt"></i>&nbsp;&nbsp;Upload Document</a>
                                        </div>
                                            <table id="documentTable" class="table table-striped table-bordered table-hover table-checkable">
                                            </table>
                                    </div>
                                    <div class="tab-pane" id="tab_3_7">
        
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>

                </div>
                <!-- /Page Content -->
            </div>
            <!-- /.container -->

        </div>
    <div class="modal fade" id="insurance" tabindex="-1">
        <div class="modal-dialog" style="width:75%;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Add New Case</h4>
                </div>
                <div class="modal-body">
                    <div class="panel-group" id="accordion">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                Primary Policy Info </a>
                                </h3>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <input type="hidden" id="hdntoggleDX" />
                                    <div class="form-group col-md-6">
                                        <label class="control-label col-md-6">Insurance </label>
                                        <div class="col-md-6">
                                            <input type="text" id="policyInsurance1" class="form-control"/>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <span id="NewspanAddress1" style="font-weight:600;"></span>
                                    </div>
                                    <div class="form-group col-md-2">
                                        <!-- <div class="col-md-12" style="padding-left:0">
                                            <label class="checkbox-inline">
                                                <input type="checkbox" class="uniform" id="chkAuth" value=""> Authorization
                                            </label>
                                        </div> -->
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group col-md-6">
                                        <label class="control-label col-md-6">Patient's Relationship </label>
                                        <div class="col-md-6">
                                            <select class="form-control" id="policyRelationInsured1">
                                                <option>Self</option>
                                                <option>Spouse</option>
                                                <option>Other</option>
                                                <option>Child</option>
                                                <option>Grandfather Or Grandmother</option>
                                                <option>Grandson Or Granddaughter</option>
                                                <option>Nephew Or Niece</option>
                                                <option>Adopted Child</option>
                                                <option>Foster Child</option>
                                                <option>Stepson</option>
                                                <option>Ward</option>
                                                <option>Stepdaughter</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group col-md-6">
                                        <label class="control-label col-md-6">Policy Holder </label>
                                        <div class="col-md-6">
                                            <input type="text" id="policyHolder1" class="form-control"/>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="control-label col-md-6">Policy Holder </label>
                                        <div class="col-md-6">
                                            <input type="text" id="description" class="form-control"/>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="control-label col-md-6">Type of Payer <span class="required">*</span></label>
                                        <div class="col-md-6">
                                            <select id="tos" class="form-control">
                                                <option></option>
                                                <option value="Behaviour Therapy">Behaviour Therapy</option>
                                                <option value="Physical Therapy">Physical Therapy</option>
                                                <option value="Occupational Therapy">Occupational Therapy</option>
                                                <option value="Speech Therapy">Speech Therapy</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group col-md-6">
                                        <label class="control-label col-md-6">DOB </label>
                                        <div class="col-md-6">
                                            <input type="text" id="policyDOB1" class="form-control"/>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="control-label col-md-6">Auth # </label>
                                        <div class="col-md-6">
                                            <a href="javascript:void(0)" id="addAuthModal">Add Auth</a>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group col-md-6">
                                        <label class="control-label col-md-6">Policy ID </label>
                                        <div class="col-md-6">
                                            <input type="text" id="policyNo1" class="form-control"/>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group col-md-6">
                                        <label class="control-label col-md-6">Policy Start </label>
                                        <div class="col-md-6">
                                            <input type="text" id="policyStartDt1" class="form-control"/>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <div class="col-md-1"></div>
                                        <!-- <div class="make-switch" data-on="success" data-on-label="ICD-9" data-off-label="ICD-10" data-off="warning">
                                            <input type="checkbox" checked class="toggle" id="icdtoggle" />
                                        </div> -->
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group col-md-6">
                                        <label class="control-label col-md-6">Policy End </label>
                                        <div class="col-md-6">
                                            <input type="text" id="policyEndDt1" class="form-control"/>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="control-label col-md-6">Principal Diag </label>
                                        <div class="col-md-6">
                                            <input type="text" id="principalDiag" class="form-control" disabled/>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group col-md-6">
                                        <label class="control-label col-md-6">Group ID </label>
                                        <div class="col-md-6">
                                            <input type="text" id="policyGroupNo1" class="form-control"/>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="control-label col-md-6">Default Diagnosis 2 </label>
                                        <div class="col-md-6">
                                            <input type="text" id="defDiag2" class="form-control" disabled/>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group col-md-6">
                                        <label class="control-label col-md-6">Group Name </label>
                                        <div class="col-md-6">
                                            <input type="text" id="policyGroupName1" class="form-control"/>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="control-label col-md-6">Default Diagnosis 3 </label>
                                        <div class="col-md-6">
                                            <input type="text" id="defDiag3" class="form-control" disabled/>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group col-md-6">
                                        <label class="control-label col-md-6">Copay </label>
                                        <div class="col-md-6">
                                            <input type="text" id="policyCopay1" class="form-control"/>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="control-label col-md-6">Default Diagnosis 4 </label>
                                        <div class="col-md-6">
                                            <input type="text" id="defDiag4" class="form-control" disabled/>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                Secondary Policy Info </a>
                                </h3>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="form-group col-md-6">
                                        <label class="control-label col-md-6">Insurance </label>
                                        <div class="col-md-6">
                                            <input type="text" id="policyInsurance2" class="form-control"/>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <span id="NewspanAddress2" style="font-weight:600;"></span>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group col-md-6">
                                        <label class="control-label col-md-6">Patient's Relationship </label>
                                        <div class="col-md-6">
                                            <select class="form-control" id="policyRelationInsured2">
                                                <option>Self</option>
                                                <option>Spouse</option>
                                                <option>Other</option>
                                                <option>Child</option>
                                                <option>Grandfather Or Grandmother</option>
                                                <option>Grandson Or Granddaughter</option>
                                                <option>Nephew Or Niece</option>
                                                <option>Adopted Child</option>
                                                <option>Foster Child</option>
                                                <option>Stepson</option>
                                                <option>Ward</option>
                                                <option>Stepdaughter</option>
                                            </select>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group col-md-6">
                                        <label class="control-label col-md-6">Policy Holder </label>
                                        <div class="col-md-6">
                                            <input type="text" id="policyHolder2" class="form-control"/>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group col-md-6">
                                        <label class="control-label col-md-6">DOB </label>
                                        <div class="col-md-6">
                                            <input type="text" id="policyDOB2" class="form-control"/>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group col-md-6">
                                        <label class="control-label col-md-6">Policy ID </label>
                                        <div class="col-md-6">
                                            <input type="text" id="policyNo2" class="form-control"/>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group col-md-6">
                                        <label class="control-label col-md-6">Policy Start </label>
                                        <div class="col-md-6">
                                            <input type="text" id="policyStartDt2" class="form-control"/>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group col-md-6">
                                        <label class="control-label col-md-6">Policy End </label>
                                        <div class="col-md-6">
                                            <input type="text" id="policyEndDt2" class="form-control"/>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group col-md-6">
                                        <label class="control-label col-md-6">Group ID </label>
                                        <div class="col-md-6">
                                            <input type="text" id="policyGroupNo2" class="form-control"/>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group col-md-6">
                                        <label class="control-label col-md-6">Group Name </label>
                                        <div class="col-md-6">
                                            <input type="text" id="policyGroupName2" class="form-control"/>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group col-md-6">
                                        <label class="control-label col-md-6">Copay </label>
                                        <div class="col-md-6">
                                            <input type="text" id="policyCopay2" class="form-control"/>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                Teritiary Policy Info </a>
                                </h3>
                            </div>
                            <div id="collapseThree" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="form-group col-md-6">
                                        <label class="control-label col-md-6">Insurance </label>
                                        <div class="col-md-6">
                                            <input type="text" id="policyInsurance3" class="form-control"/>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <span id="NewspanAddress3" style="font-weight:600;"></span>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group col-md-6">
                                        <label class="control-label col-md-6">Patient's Relationship </label>
                                        <div class="col-md-6">
                                            <select class="form-control" id="policyRelationInsured3">
                                                <option>Self</option>
                                                <option>Spouse</option>
                                                <option>Other</option>
                                                <option>Child</option>
                                                <option>Grandfather Or Grandmother</option>
                                                <option>Grandson Or Granddaughter</option>
                                                <option>Nephew Or Niece</option>
                                                <option>Adopted Child</option>
                                                <option>Foster Child</option>
                                                <option>Stepson</option>
                                                <option>Ward</option>
                                                <option>Stepdaughter</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="control-label col-md-6">Policy Holder </label>
                                        <div class="col-md-6">
                                            <input type="text" id="policyHolder3" class="form-control"/>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group col-md-6">
                                        <label class="control-label col-md-6">DOB </label>
                                        <div class="col-md-6">
                                            <input type="text" id="policyDOB3" class="form-control"/>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group col-md-6">
                                        <label class="control-label col-md-6">Policy ID </label>
                                        <div class="col-md-6">
                                            <input type="text" id="policyNo3" class="form-control"/>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group col-md-6">
                                        <label class="control-label col-md-6">Policy Start </label>
                                        <div class="col-md-6">
                                            <input type="text" id="policyStartDt3" class="form-control"/>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group col-md-6">
                                        <label class="control-label col-md-6">Policy End </label>
                                        <div class="col-md-6">
                                            <input type="text" id="policyEndDt3" class="form-control"/>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group col-md-6">
                                        <label class="control-label col-md-6">Group ID </label>
                                        <div class="col-md-6">
                                            <input type="text" id="policyGroupNo3" class="form-control"/>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group col-md-6">
                                        <label class="control-label col-md-6">Group Name </label>
                                        <div class="col-md-6">
                                            <input type="text" id="policyGroupName3" class="form-control"/>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group col-md-6">
                                        <label class="control-label col-md-6">Copay </label>
                                        <div class="col-md-6">
                                            <input type="text" id="policyCopay3" class="form-control"/>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="button" class="btn btn-default" data-dismiss="modal" value="Exit" />
                    <input type="button" id="btnSaveCase" class="btn btn-primary" data-dismiss="modal" value="Save" />
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
    <div class="modal fade" id="authModal" tabindex="-1">
        <div class="modal-dialog" style="width:40%">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Add Auth #</h4>
                </div>
                <div class="modal-body" style="min-height:150px;">
                    <div class="form-group col-md-12" style="padding-bottom:5%;">
                        <label class="control-label col-md-6">Type of Service</label>
                        <div class="col-md-6">
                            <select id="newtos" class="form-control">
                                <option></option>
                                <option value="Behaviour Therapy">Behaviour Therapy</option>
                                <option value="Physical Therapy">Physical Therapy</option>
                                <option value="Occupational Therapy">Occupational Therapy</option>
                                <option value="Speech Therapy">Speech Therapy</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-md-12" style="padding-bottom:5%;">
                        <label class="control-label col-md-6">Auth No</label>
                        <div class="col-md-6">
                            <input type="text" id="authNo" class="form-control"/>
                        </div>
                    </div>
                    <div class="form-group col-md-12" style="padding-bottom:5%;">
                        <label class="control-label col-md-6">Effective Start Date</label>
                        <div class="col-md-6">
                            <input type="text" id="AuthstartDt" class="form-control"/>
                        </div>
                    </div>
                    <div class="form-group col-md-12" style="padding-bottom:5%;">
                        <label class="control-label col-md-6">Effective End Date</label>
                        <div class="col-md-6">
                            <input type="text" id="AuthendDt" class="form-control"/>
                        </div>
                    </div>
                    <div class="form-group col-md-12" style="padding-bottom:5%;">
                        <label class="control-label col-md-4">Total Auth Visits </label>
                        <div class="col-md-4">
                            <select class="form-control" id="totAuthType">
                                <option value=""></option>
                                <option value="Week">Week</option>
                                <option value="Total Auth">Total Auth</option>
                                <option value="Session">Session</option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <input type="text" id="AuthtotalVisits" class="form-control"/>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="button" class="btn btn-default" data-dismiss="modal" value="Exit" />
                    <input type="button" id="btnAddAuth" class="btn btn-primary" data-dismiss="modal" value="Save" />
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
    <div class="modal fade" id="EditClaim" tabindex="-1">
        <div class="modal-dialog" style="width:90%;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Edit Claim</h4>
                </div>
                <div class="modal-body" style="min-height:500px;">
                    <input type="hidden" id="hdnClaimID" />
                    <input type="hidden" id="hdnClaimNo" />
                    <input type="hidden" id="hdnDX" />
                    <input type="hidden" id="hdnBal" />
                    <input type="hidden" id="hdnCaseChart" />
                    <input type="hidden" id="hdnCurrInsurance" />
                    <div class="form-group col-md-4" style="padding:0;">
                        <div class="form-group col-md-12" style="float:left;">
                            <label class="control-label col-md-6">From </label>
                            <div class="col-md-6">
                                <input type="text" id="fromDt" class="form-control" disabled />
                            </div>
                        </div>
                        <div class="form-group col-md-12" style="float:left;">
                            <label class="control-label col-md-6">To </label>
                            <div class="col-md-6">
                                <input type="text" id="toDt" class="form-control" disabled />
                            </div>
                        </div>
                        <div class="form-group col-md-12" style="float:left;">
                            <label class="control-label col-md-6">CPT </label>
                            <div class="col-md-6">
                                <input type="text" id="proced" class="form-control" disabled />
                            </div>
                        </div>
                        <div class="form-group col-md-12" style="float:left;">
                            <label class="control-label col-md-6">Units </label>
                            <div class="col-md-6">
                                <input type="text" id="units" class="form-control" disabled />
                            </div>
                        </div>
                        <div class="form-group col-md-12" style="float:left;">
                            <label class="control-label col-md-6">Bill Date </label>
                            <div class="col-md-6">
                                <input type="text" id="billDt" class="form-control" disabled />
                            </div>
                        </div>
                        <div class="form-group col-md-12" style="float:left;">
                            <label class="control-label col-md-6">Billed Amount </label>
                            <div class="col-md-6">
                                <input type="text" id="total" class="form-control" disabled />
                            </div>
                        </div>
                        <div class="form-group col-md-12" style="float:left;">
                            <label class="control-label col-md-6">Allowed Amonut </label>
                            <div class="col-md-6">
                                <input type="text" id="allowed" class="form-control" disabled />
                            </div>
                        </div>
                        <div class="form-group col-md-12" style="float:left;">
                            <label class="control-label col-md-6">Paid </label>
                            <div class="col-md-6">
                                <input type="text" id="paid" class="form-control" disabled />
                            </div>
                        </div>
                        <div class="form-group col-md-12" style="float:left;">
                            <label class="control-label col-md-6">Adjustment </label>
                            <div class="col-md-6">
                                <input type="text" id="adjustment" class="form-control" disabled />
                            </div>
                        </div>
                        <div class="form-group col-md-12" style="float:left;">
                            <label class="control-label col-md-6">Balance </label>
                            <div class="col-md-6">
                                <input type="text" id="claimBalance" class="form-control" disabled />
                            </div>
                        </div>
                        <div class="form-group col-md-12" style="float:left;">
                            <label class="control-label col-md-6">Claim Status </label>
                            <div class="col-md-6">
                                <select class="form-control" id="claimStatus" disabled>
                                    <option value="DRAFT">DRAFT</option>
                                    <option value="REVIEW">REVIEW</option>
                                    <option value="COMPLETE">COMPLETE</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group col-md-12" style="float:left;">
                            <label class="control-label col-md-6">Diag 1 </label>
                            <div class="col-md-6">
                                <input type="text" id="diag1" class="form-control" disabled />
                            </div>
                        </div>
                        <div class="form-group col-md-12" style="float:left;">
                            <label class="control-label col-md-6">Diag 2 </label>
                            <div class="col-md-6">
                                <input type="text" id="diag2" class="form-control" disabled />
                            </div>
                        </div>
                        <div class="form-group col-md-12" style="float:left;">
                            <label class="control-label col-md-6">Diag 3 </label>
                            <div class="col-md-6">
                                <input type="text" id="diag3" class="form-control" disabled />
                            </div>
                        </div>
                        <div class="form-group col-md-12" style="float:left;">
                            <label class="control-label col-md-6">Diag 4 </label>
                            <div class="col-md-6">
                                <input type="text" id="diag4" class="form-control" disabled />
                            </div>
                        </div>
                </div>
                <div class="form-group col-md-2" style="padding:0;">
                    <div class="make-switch" data-on="success" data-on-label="ICD-9" data-off-label="ICD-10" data-off="warning">
                        <input type="checkbox" checked class="toggle" id="icd" />
                    </div>
                    <input type="button" id="btnPaperClaim" style="margin:30px 0 2px 0px; width:162px;" class="btn btn-primary actionbtn" value="Print Paper Claim" />
                    <input type="button" id="btnRebill" style="margin:0 0 2px 0px; width:162px;" class="btn btn-primary" value="Rebill" />
                    <input type="button" id="btnTransBal" style="margin:0 0 2px 0px; width:162px;" class="btn btn-primary actionbtn" value="Transfer Balance" />
                    <input type="button" id="btnNote" style="margin:0 0 2px 0px; width:162px;" class="btn btn-primary actionbtn" value="Note" />
                    <input type="button" id="btnSettle" style="margin:0 0 2px 0px; width:162px;" class="btn btn-primary actionbtn" value="Settle" />
                    <input type="button" id="btnVoid" style="margin:0 0 2px 0px; width:162px;" class="btn btn-primary actionbtn" value="Void" />
                    <input type="button" id="btnAdjust1" style="margin:0 0 2px 0px; width:162px;" class="btn btn-primary actionbtn" value="Adjustment" />
                </div>
                <div class="form-group col-md-5" style="padding:0 0 0 20px;">
                    <div class="tabbable tabbable-custom">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab_1_1" data-toggle="tab" style="border-bottom:none">History</a></li>
                        </ul>
                        <div class="tab-content" style="min-height:300px; overflow:scroll;">
                            <div class="tab-pane active" id="tab_1_1">
                                <p>
                                    05/06/2016 : Claim has been Created
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-2"><label class="control-label col-md-12 lblTitle" style="padding-top:20px;"></label></div>
                <div class="col-md-6">
                        <div class="form-group col-md-12" style="float:left;">
                            <label class="control-label col-md-4 lblSub1"> </label>
                            <div class="col-md-6 txtSub1">
                            </div>
                        </div>
                        <div class="form-group col-md-12" style="float:left;">
                            <label class="control-label col-md-4 lblSub2"> </label>
                            <div class="col-md-6 txtSub2">
                            </div>
                        </div>
                        <div class="form-group col-md-12" style="float:left;">
                            <label class="control-label col-md-4 lblSub3"> </label>
                            <div class="col-md-6 txtSub3">
                            </div>
                        </div>
                        <div class="form-group col-md-9 btnSection">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="button" class="btn btn-default" data-dismiss="modal" value="Exit" />
                    <input type="button" id="btnUpdateClaim" class="btn btn-primary" data-dismiss="modal" value="Save" />
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
    <div class="modal fade" id="upload" tabindex="-1">
      <div class="modal-dialog" style="width:40%">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h4 class="modal-title">Upload File</h4>
              </div>
              <div class="modal-body" style="min-height:150px;">
                  <form action="UploadFile.php" method="post" enctype="multipart/form-data">
                      <h2>Upload File</h2>
                      <label for="fileSelect">Filename:</label>
                      <input type="file" name="photo" id="fileSelect"><br>
                      <input type="submit" name="submit" value="Upload">
                  </form>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
          </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
  </div>
    <div class="modal fade" id="paperClaimModal" tabindex="-1">
        <div class="modal-dialog" style="width:40%">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Select Paper Claim Style</h4>
                </div>
                <div class="modal-body" style="min-height:120px;">
                    <div class="form-group col-md-12" style="padding-bottom:5%;">
                        <div class="col-md-6">
                            <input type="radio" class="btn btn-primary" id="withBG" name="BG" value="With BG" checked /> With Background
                        </div>
                    </div>
                    <div class="form-group col-md-12" style="padding-bottom:5%;">
                        <div class="col-md-6">
                            <input type="radio" class="btn btn-primary" id="withoutBG" name="BG" value="Without BG" /> Without Background
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="button" class="btn btn-default" data-dismiss="modal" value="Exit" />
                    <input type="button" id="btnPrint" class="btn btn-primary" data-dismiss="modal" value="Save" />
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
    <div class="modal fade" id="DocumentNotes" tabindex="-1">
        <div class="modal-dialog" style="width:40%">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Add Notes</h4>
                </div>
                <div class="modal-body" style="min-height:120px;">
                    <div class="form-group col-md-12" style="padding-bottom:5%;">
                      <input type="hidden" id="hdnDocID"/>
                      <label>Notes : </label><br/><textarea class="form-control" rows="7" cols="10" id="Documentnotes"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="button" class="btn btn-default" data-dismiss="modal" value="Exit" />
                    <input type="button" id="btnDocNotes" class="btn btn-primary" data-dismiss="modal" value="Save" />
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
    <div class="modal fade" id="AddAuthModal" tabindex="-1">
        <div class="modal-dialog" style="width:40%">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Add Auth #</h4>
                </div>
                <div class="modal-body" style="min-height:120px;">
                    <div class="form-group col-md-12" style="padding-bottom:5%;">
                      <label>Auth # : </label><br/><input type="text" id="tempAuth" class="form-control"/>
                    </div>
                    <div class="form-group col-md-12" style="padding-bottom:5%;">
                      <label>Effective Start Date : </label><br/><input type="text" id="tempAuthstartDt" class="form-control"/>
                    </div>
                    <div class="form-group col-md-12" style="padding-bottom:5%;">
                      <label>Effective End Date : </label><br/><input type="text" id="tempAuthendDt" class="form-control"/>
                    </div>
                    <div class="form-group col-md-12" style="padding-bottom:5%;">
                      <label>Total Auth Type : </label><br/><input type="text" id="temptotAuthType" class="form-control"/>
                    </div>
                    <div class="form-group col-md-12" style="padding-bottom:5%;">
                      <label>Total Auth Visits : </label><br/><input type="text" id="temptotalVisits" class="form-control"/>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="button" class="btn btn-default" data-dismiss="modal" value="Exit" />
                    <input type="button" id="btntempAuth" class="btn btn-primary" data-dismiss="modal" value="Save" />
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
     <div class="modal fade" id="EditAuthModal" tabindex="-1">
        <div class="modal-dialog" style="width:40%">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Edit Auth #</h4>
                </div>
                <div class="modal-body" style="min-height:120px;">
                    <input type="hidden" id="_hdnAuthID" />
                    <div class="form-group col-md-12" style="padding-bottom:5%;">
                      <label>Auth # : </label><br/><input type="text" id="EdittempAuth" class="form-control"/>
                    </div>
                    <select id="Edittemptos" class="form-control">
                        <option></option>
                        <option value="Behaviour Therapy">Behaviour Therapy</option>
                        <option value="Physical Therapy">Physical Therapy</option>
                        <option value="Occupational Therapy">Occupational Therapy</option>
                        <option value="Speech Therapy">Speech Therapy</option>
                    </select>
                    <div class="form-group col-md-12" style="padding-bottom:5%;">
                      <label>Effective Start Date : </label><br/><input type="text" id="EdittempAuthstartDt" class="form-control"/>
                    </div>
                    <div class="form-group col-md-12" style="padding-bottom:5%;">
                      <label>Effective End Date : </label><br/><input type="text" id="EdittempAuthendDt" class="form-control"/>
                    </div>
                    <div class="form-group col-md-12" style="padding-bottom:5%;">
                      <label>Total Auth Type : </label><br/>
                      <select class="form-control" id="EdittempAuthType">
                            <option value=""></option>
                            <option value="Week">Week</option>
                            <option value="Total Auth">Total Auth</option>
                            <option value="Session">Session</option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="button" class="btn btn-default" data-dismiss="modal" value="Exit" />
                    <input type="button" id="btntempAuth" class="btn btn-primary" data-dismiss="modal" value="Save" />
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
    <div class="modal fade" id="guarantorModal" tabindex="-1">
        <div class="modal-dialog" style="width:40%">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Add Guarantor</h4>
                </div>
                <div class="modal-body" style="min-height:150px;">
                    <div class="form-group col-md-12" style="padding-bottom:5%;">
                        <label class="control-label col-md-6">Guarantor Name</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control required" id="guarantorName" name="txtFirstName" placeholder="Enter the First Name" />
                        </div>
                    </div>
                    <div class="form-group col-md-12" style="padding-bottom:5%;">
                        <label class="control-label col-md-6">Street</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control required" id="guarantorAddrStreet" name="txtFirstName" placeholder="Enter the First Name" />
                        </div>
                    </div>
                    <div class="form-group col-md-12" style="padding-bottom:5%;">
                        <label class="control-label col-md-6">City</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control required" id="guarantorAddrCity" name="txtFirstName" placeholder="Enter the First Name" />
                        </div>
                    </div>
                    <div class="form-group col-md-12" style="padding-bottom:5%;">
                        <label class="control-label col-md-6">State </label>
                        <div class="col-md-6">
                            <input type="text" class="form-control required" id="guarantorAddrState" name="txtMiddleName" placeholder="Enter the Middle Name" />
                        </div>
                    </div>
                    <div class="form-group col-md-12" style="padding-bottom:5%;">
                        <label class="control-label col-md-6">Zip code </label>
                        <div class="col-md-6">
                            <input type="text" class="form-control required" id="guarantorAddrZip" placeholder="Enter the Last Name" />
                        </div>
                    </div>
                    <div class="form-group col-md-12" style="padding-bottom:5%;">
                        <label class="control-label col-md-6">Relationship to Guarantor </label>
                        <div class="col-md-6">
                            <select class="form-control" id="guarantorRelation">
                                <option></option>
                                <option>Child</option>
                                <option>Other</option>
                                <option>Spouse</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="button" class="btn btn-default" data-dismiss="modal" value="Exit" />
                    <input type="button" id="btnSaveFullname" class="btn btn-primary" data-dismiss="modal" value="Save" />
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
    <div class="modal fade" id="claimDetails" tabindex="-1">
        <div class="modal-dialog" style="width:75%">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Claim Details</h4>
                </div>
                <div class="modal-body" style="min-height:120px;">
                    <table id="details" class="table table-striped table-bordered table-hover table-checkable">
                    </table>
                </div>
                <div class="modal-footer">
                    <input type="button" class="btn btn-default" data-dismiss="modal" value="Exit" />
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
    <div class="modal fade" id="modalActivity" tabindex="-1">
        <div class="modal-dialog" style="width:75%;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="ActTitleMod"></h4>
                </div>
                <div class="modal-body" style="min-height:350px;">
                  <input type="hidden" id="hdnActID" />
                  <input type="hidden" id="hdnFlagAct" />
                  
                  <div class="form-group col-md-4">
                      <label class="control-label col-md-6">Activity </label>
                      <div class="col-md-6">
                          <select class="form-control" id="Activity">
                          </select>
                      </div>
                  </div>
                  <div class="form-group col-md-3">
                      <label class="control-label col-md-6">CPT Code </label>
                      <div class="col-md-6">
                          <select class="form-control" id="ActCPT">
                          </select>
                      </div>
                  </div>
                  <div class="form-group col-md-5">
                      <label class="control-label col-md-2">Modifiers </label>
                      <div class="col-md-10">
                          <div class="col-md-3">
                              <input type="text" id="ActMod1" class="form-control"/> 
                          </div>
                          <div class="col-md-3">
                              <input type="text" id="ActMod2" class="form-control"/> 
                          </div>
                          <div class="col-md-3">
                              <input type="text" id="ActMod3" class="form-control"/> 
                          </div>
                          <div class="col-md-3">
                              <input type="text" id="ActMod4" class="form-control"/>
                          </div>
                      </div>
                  </div>
                  <div class="clearfix"></div>
                  <div class="form-group col-md-4">
                      <label class="control-label col-md-6">Billed per </label>
                      <div class="col-md-6">
                          <select class="form-control" id="ActPer">
                            <option value="15 mins">15 mins</option>
                            <option value="30 mins">30 mins</option>
                            <option value="Hour">Hour</option>
                            <option value="Session">Session</option>
                          </select>
                      </div>
                  </div>
                  <div class="form-group col-md-4" id="divPerTime">
                      <div class="col-md-6">
                          <select class="form-control" id="ActPerTime">
                            <option value="15 mins">15 mins</option>
                            <option value="30 mins">30 mins</option>
                            <option value="1 Hour">1 Hour</option>
                          </select>
                      </div>
                  </div>
                  <div class="form-group col-md-4" style="margin:10px 0;">
                      <label class="control-label col-md-4">Rate (s) </label>
                      <div class="col-md-6">
                          <input type="text" id="ActRate" class="form-control"/>
                      </div>
                      <label class="control-label col-md-2"></label>
                  </div>
                  <div class="form-group col-md-6">
                      <label class="control-label col-md-12">Maximum Frequency Allowed </label>
                  </div>
                  <div class="form-group col-md-6" style="min-height:30px">
                  </div>
                  <div class="form-group col-md-12">
                    <label class="control-label col-md-2">Maximum </label>
                    <div class="col-md-2">
                        <select class="form-control" id="ActMaxi1">
                            <option value="Unit">Unit</option>
                            <option value="Amount">Amount</option>
                            <option value="Sessions">Sessions</option>
                        </select>
                    </div>
                    <label class="control-label col-md-2">per </label>
                    <div class="col-md-2">
                        <select class="form-control" id="ActPer1">
                            <option value="Day">Day</option>
                            <option value="Week">Week</option>
                            <option value="Month">Month</option>
                            <option value="Total Auth">Total Auth</option>
                        </select>
                    </div>
                    <label class="control-label col-md-2">Is </label>
                    <div class="col-md-2">
                        <input type="text" id="ActIs1" class="form-control"/> And
                    </div>
                </div>
                <div class="form-group col-md-12">
                    <label class="control-label col-md-2">Maximum </label>
                    <div class="col-md-2">
                        <select class="form-control" id="ActMaxi2">
                            <option value="Unit">Unit</option>
                            <option value="Amount">Amount</option>
                            <option value="Sessions">Sessions</option>
                        </select>
                    </div>
                    <label class="control-label col-md-2">per </label>
                    <div class="col-md-2">
                        <select class="form-control" id="ActPer2">
                            <option value="Day">Day</option>
                            <option value="Week">Week</option>
                            <option value="Month">Month</option>
                            <option value="Total Auth">Total Auth</option>
                        </select>
                    </div>
                    <label class="control-label col-md-2">Is </label>
                    <div class="col-md-2">
                        <input type="text" id="ActIs2" class="form-control"/> And
                    </div>
                </div>
                <div class="form-group col-md-12" style="padding-bottom:3%;">
                    <label class="control-label col-md-2">Maximum </label>
                    <div class="col-md-2">
                        <select class="form-control" id="ActMaxi3">
                            <option value="Unit">Unit</option>
                            <option value="Amount">Amount</option>
                            <option value="Sessions">Sessions</option>
                        </select>
                    </div>
                    <label class="control-label col-md-2">per </label>
                    <div class="col-md-2">
                        <select class="form-control" id="ActPer3">
                            <option value="Day">Day</option>
                            <option value="Week">Week</option>
                            <option value="Month">Month</option>
                            <option value="Total Auth">Total Auth</option>
                        </select>
                    </div>
                    <label class="control-label col-md-2">Is </label>
                    <div class="col-md-2">
                        <input type="text" id="ActIs3" class="form-control"/>
                    </div>
                </div>
                </div>
                <div class="modal-footer">
                    <input type="button" class="btn btn-primary" id="btnAddActi" value="Update Activity" />
                    <input type="button" class="btn btn-default" data-dismiss="modal" value="Exit" />
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>

</body>
</html>