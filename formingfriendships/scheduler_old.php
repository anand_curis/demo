<?php
 session_start();

 if (!isset($_SESSION['userID'])) {
    header("Location: index.html");
 }
 ?>
<!doctype html>
<head>
	<meta http-equiv="Content-type" content="text/html; charset=utf-8">
	<title>Scheduler | AMROMED LLC</title>

	<script src="Scheduler/dhtmlxscheduler.js" type="text/javascript" charset="utf-8"></script>
	<script src="Scheduler/dhtmlxscheduler_serialize.js" type="text/javascript" charset="utf-8"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.devbridge-autocomplete/1.2.27/jquery.autocomplete.min.js"></script>
	<link rel="stylesheet" href="Scheduler/dhtmlxscheduler_glossy.css" type="text/css" media="screen" title="no title" charset="utf-8">
	<link rel="stylesheet" href="Scheduler/dhtmlxCombo/dhtmlxcombo.css" type="text/css" media="screen" title="no title" charset="utf-8">
	<script src="Scheduler/dhtmlxCombo/dhtmlxcombo.js" type="text/javascript" charset="utf-8"></script>
	<script src="Scheduler/sources/ext/dhtmlxscheduler_editors.js" type="text/javascript" charset="utf-8"></script>
	<script src="Scheduler/sources/ext/dhtmlxscheduler_limit.js" type="text/javascript" charset="utf-8"></script>
	<script src="Scheduler/sources/ext/dhtmlxscheduler_collision.js"></script>

  <!--=== CSS ===-->

  <!-- Bootstrap -->
  <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

  <!-- jQuery UI -->
  <!--<link href="plugins/jquery-ui/jquery-ui-1.10.2.custom.css" rel="stylesheet" type="text/css" />-->
  <!--[if lt IE 9]>
    <link rel="stylesheet" type="text/css" href="plugins/jquery-ui/jquery.ui.1.10.2.ie.css"/>
  <![endif]-->
  <!-- Theme -->
  <link href="assets/css/main.css" rel="stylesheet" type="text/css" />
  <link href="assets/css/plugins.css" rel="stylesheet" type="text/css" />
  <link href="assets/css/responsive.css" rel="stylesheet" type="text/css" />
  <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
  <link href="assets/css/alertify.css" rel='stylesheet' type='text/css'>
  <link href="assets/css/themes/default.css" rel='stylesheet' type='text/css'>

  <link rel="stylesheet" href="assets/css/fontawesome/font-awesome.min.css">
  <link href="plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
  <!--[if IE 7]>
    <link rel="stylesheet" href="assets/css/fontawesome/font-awesome-ie7.min.css">
  <![endif]-->

  <!--[if IE 8]>
    <link href="assets/css/ie8.css" rel="stylesheet" type="text/css" />
  <![endif]-->
  <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>

  <!--=== JavaScript ===-->

  <script type="text/javascript" src="assets/js/libs/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>
  <script type="text/javascript" src="plugins/bootstrap-switch/bootstrap-switch.min.js"></script>

  <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="assets/js/libs/lodash.compat.min.js"></script>

  <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
  <!--[if lt IE 9]>
    <script src="assets/js/libs/html5shiv.js"></script>
  <![endif]-->

  <!-- Smartphone Touch Events -->
  <script type="text/javascript" src="plugins/touchpunch/jquery.ui.touch-punch.min.js"></script>
  <script type="text/javascript" src="plugins/event.swipe/jquery.event.move.js"></script>
  <script type="text/javascript" src="plugins/event.swipe/jquery.event.swipe.js"></script>

  <!-- General -->
  <script type="text/javascript" src="assets/js/libs/breakpoints.js"></script>
  <script type="text/javascript" src="plugins/respond/respond.min.js"></script> <!-- Polyfill for min/max-width CSS3 Media Queries (only for IE8) -->
  <script type="text/javascript" src="plugins/cookie/jquery.cookie.min.js"></script>
  <script type="text/javascript" src="plugins/slimscroll/jquery.slimscroll.min.js"></script>
  <script type="text/javascript" src="plugins/slimscroll/jquery.slimscroll.horizontal.min.js"></script>

  <!-- Page specific plugins -->
  <!-- Charts -->
  <script type="text/javascript" src="plugins/sparkline/jquery.sparkline.min.js"></script>

  <script type="text/javascript" src="plugins/daterangepicker/moment.min.js"></script>
  <script type="text/javascript" src="plugins/daterangepicker/daterangepicker.js"></script>
  <script type="text/javascript" src="plugins/blockui/jquery.blockUI.min.js"></script>

  <!-- Pickers -->
  <script type="text/javascript" src="plugins/pickadate/picker.js"></script>
  <script type="text/javascript" src="plugins/pickadate/picker.date.js"></script>
  <script type="text/javascript" src="plugins/pickadate/picker.time.js"></script>
  <script type="text/javascript" src="plugins/bootstrap-colorpicker/bootstrap-colorpicker.min.js"></script>

  <!-- Noty -->
  <script type="text/javascript" src="plugins/noty/jquery.noty.js"></script>
  <script type="text/javascript" src="plugins/noty/layouts/top.js"></script>
  <script type="text/javascript" src="plugins/noty/themes/default.js"></script>

  <!-- Slim Progress Bars -->
  <script type="text/javascript" src="plugins/nprogress/nprogress.js"></script>
  <script type="text/javascript" src="assets/js/jquery.mask.min.js"></script>

  <!-- Bootbox -->
  <script type="text/javascript" src="plugins/bootbox/bootbox.min.js"></script>

  <!-- App -->
  <script type="text/javascript" src="assets/js/app.js"></script>
  <script type="text/javascript" src="assets/js/plugins.js"></script>
  <script type="text/javascript" src="assets/js/plugins.form-components.js"></script>
  <script src="assets/js/jquery.datetimepicker.js"></script>
    <link href="assets/css/jquery.datetimepicker.css" rel="stylesheet" />


	<style type="text/css" media="screen">
		html, body {
			margin: 0px;
			padding: 0px;
			height: 100%;
			overflow: hidden;
		}

		#my_form {
			position: absolute;
			top: 100px;
			left: 200px;
			z-index: 10001;
			display: none;
			background-color: white;
			border: 2px outset gray;
			padding: 20px;
			font-family: Tahoma;
			font-size: 10pt;
		}

		#my_form label {
			width: 200px;
		}

		.dhx_cal_event div.dhx_footer,
		.dhx_cal_event.past_event div.dhx_footer,
		.dhx_cal_event.event_UnConfirmed div.dhx_footer,
		.dhx_cal_event.event_math div.dhx_footer,
		.dhx_cal_event.event_Confirmed div.dhx_footer{
			background-color: transparent !important;
		}
		.dhx_cal_event .dhx_body{
			-webkit-transition: opacity 0.1s;
			transition: opacity 0.1s;
			opacity: 0.7;
		}
		.dhx_cal_event .dhx_title{
			line-height: 12px;
		}
		.dhx_cal_event_line:hover,
		.dhx_cal_event:hover .dhx_body,
		.dhx_cal_event.selected .dhx_body,
		.dhx_cal_event.dhx_cal_select_menu .dhx_body{
			opacity: 1;
		}

		.dhx_cal_event.event_NoShow div, .dhx_cal_event_line.event_NoShow{
			background-color: orange !important;
			border-color: #a36800 !important;
		}
		.dhx_cal_event_clear.event_NoShow{
			color:orange !important;
		}

		.dhx_cal_event.event_Confirm div, .dhx_cal_event_line.event_Confirm{
			background-color: #36BD14 !important;
			border-color: #698490 !important;
		}
		.dhx_cal_event_clear.event_Confirm{
			color:#36BD14 !important;
		}

		.dhx_cal_event.event_UnConfirm div, .dhx_cal_event_line.event_UnConfirm{
			background-color: #FC5BD5 !important;
			border-color: #839595 !important;
		}
		.dhx_cal_event_clear.event_UnConfirm{
			color:#B82594 !important;
		}
		.highlighted_timespan {
			background-color: #87cefa;
			opacity:0.5;
			filter:alpha(opacity=50);
			cursor: pointer;
			z-index: 0;
		}
		.dhx_menu_icon.icon_location{
		  background-image: url('location_icon.png');  
		}
		.ui-autocomplete-input {
		  border: none; 
		  font-size: 14px;

		  height: 24px;
		  
		  border: 1px solid #DDD !important;
		  position: relative;
		}
		.ui-menu .ui-menu-item a {
		  font-size: 12px;
		  color:#fff;
		}
		.ui-autocomplete {
		  position: absolute;
		  top: 0;
		  left: 0;
		  z-index: 1510 !important;
		  float: left;
		  display: none;
		  min-width: 160px;
		  width: 160px;
		  list-style: none;
		  background-color: #ffffff;
		  border-color: #ccc;
		  border-color: rgba(0, 0, 0, 0.2);
		  border-style: solid;
		  border-width: 1px;
		  -webkit-border-radius: 2px;
		  -moz-border-radius: 2px;
		  border-radius: 2px;
		  -webkit-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
		  -moz-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
		  box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
		  -webkit-background-clip: padding-box;
		  -moz-background-clip: padding;
		  background-clip: padding-box;
		  *border-right-width: 2px;
		  *border-bottom-width: 2px;
		}
		.ui-menu-item > a.ui-corner-all {
		    display: block;
		    padding: 3px 15px;
		    clear: both;
		    font-weight: normal;
		    line-height: 18px;
		    color:#000;
		    white-space: nowrap;
		    text-decoration: none;
		}
		.ui-state-hover {
		      color: #fff;
		      text-decoration: none;
		      background-color: #0088cc;
		      border-radius: 0px;
		      -webkit-border-radius: 0px;
		      -moz-border-radius: 0px;
		      background-image: none;
		}
		.ui-state-active {
		      color: #fff;
		      text-decoration: none;
		      background-color: #0088cc;
		      border-radius: 0px;
		      -webkit-border-radius: 0px;
		      -moz-border-radius: 0px;
		      background-image: none;
		}
	</style>

	<script type="text/javascript" charset="utf-8">
	var convertDate = function(usDate) {
		if(usDate!=""){
			var dateParts = usDate.split(/(\d{1,2})-(\d{1,2})-(\d{4})/);
	      return dateParts[3] + "-" + dateParts[1] + "-" + dateParts[2];
		}
    }
		$(document).ready(function(){
			document.getElementById('practiceName').innerHTML = sessionStorage.getItem('practiceName');
			$("#rightDiv").css("border-left","1px solid #000");
			$('#AppFromTime').datetimepicker({
	            // dayOfWeekStart: 1,
	            // lang: 'en',
	            format: 'm-d-Y H:i',
	            timepicker: true,
	            //minDate: '-2013/01/02',
	            //maxDate: '+2014/12/31',
	            formatDate: 'm-d-Y HH:mm',
	            hours12:false,
	            closeOnTimeSelect: true,
	            
	        });
	        $("#divLoc").hide();
	        $('#EditAppFromTime').datetimepicker({
	            // dayOfWeekStart: 1,
	            // lang: 'en',
	            format: 'm-d-Y H:i',
	            timepicker: true,
	            //minDate: '-2013/01/02',
	            //maxDate: '+2014/12/31',
	            formatDate: 'm-d-Y HH:mm',
	            hours12:false,
	            closeOnTimeSelect: true,
	            
	        });

	   //      $(document).on('change','#chkLoc',function() {
	   //      	var remember = document.getElementById('chkLoc');
				// if (remember.checked){
				// 	$("#divLoc").show();
				// }else{
				// 	$("#divLoc").hide();
				// }
	   //      });

	        $(document).on('focusout','#AppFromTime',function() {
	            var sVal = $(this).val();
	            var fromDt = new Date(sVal);
	            var mins = fromDt.getMinutes();
	            var step = document.getElementById("AppStepTime").value;
	            var toDt = new Date(fromDt);
				toDt.setMinutes(parseInt(toDt.getMinutes()) + parseInt(step));
				document.getElementById("AppToTime").value = nonConvertDt(toDt);
	        })
	        $(document).on('focusout','#EditAppFromTime',function() {
	            var sVal = $(this).val();
	            var fromDt = new Date(sVal);
	            var mins = fromDt.getMinutes();
	            var step = document.getElementById("EditAppStepTime").value;
	            var toDt = new Date(fromDt);
				toDt.setMinutes(parseInt(toDt.getMinutes()) + parseInt(step));
				document.getElementById("EditAppToTime").value = nonConvertDt(toDt);
	        })
	        $(document).on('focusout','#AppClientName',function() {
	        	var sVal = $(this).val();
	        	var n=sVal.indexOf("- ");
				var patientID = sVal.substr(0,n-1);
				$.ajax({
		            type: "POST",
		            url:"getLocations.php",
		            data:{
		              patientID : patientID
		            },success:function(data){
		            	$('#ddlLoca').append( '<option value="">-- Select Locations --</option>' );
		            	var res = JSON.parse(data);
		            	if(res[0].home=="1"){
		            		$('#ddlLoca').append( '<option value="Home">Home</option>' );
		            	}
		            	if(res[0].office=="1"){
		            		$('#ddlLoca').append( '<option value="Office">Office</option>' );
		            	}
		            	if(res[0].school=="1"){
		            		$('#ddlLoca').append( '<option value="School">School</option>' );
		            	}
		            }
		        });
				$.ajax({
		            type: "POST",
		            url:"getAuthbyPatient.php",
		            data:{
		              patientID : patientID
		            },success:function(data){
		            	data = JSON.parse(data);
		            	$("#AppProvider").val("");
		          		$("#AppFromTime").val("");
		          		$("#AppToTime").val("");
		          		$("#AppDesc").val("");
		          		//document.getElementById("chkLoc").checked = false;
		          		//$("#divLoc").hide();
		          		$('#AppActivity').prop('selectedIndex',0);
		          		$('#AppStatus').prop('selectedIndex',0);
		          		$('#AppStepTime').prop('selectedIndex',0);
		            	var arrCase = [];
				        $('#AppAuth').html('');
				        for(var x in data){
						  arrCase.push(data[x]);
						}
						//alert(arrCase);
						$.each(arrCase,function(i,v) {
			            	$('#AppAuth').html('<option value="">-- Select Auth --</option>');
							arrCase.forEach(function(t) { 
					            $('#AppAuth').append('<option value="'+t.key+'">'+t.label+'</option>');
					        });
						});
		            }
		          });
			});
			$(document).on('focusout','#EditAppClientName',function() {
	        	var sVal = $(this).val();
	        	var n=sVal.indexOf("- ");
				var patientID = sVal.substr(0,n-1);
				$.ajax({
		            type: "POST",
		            url:"getAuthbyPatient.php",
		            data:{
		              patientID : patientID
		            },success:function(data){
		            	data = JSON.parse(data);
		            	var arrCase = [];
				        $('#EditAppAuth').html('');
				        for(var x in data){
						  arrCase.push(data[x]);
						}
						//alert(arrCase);
						$.each(arrCase,function(i,v) {
			            	$('#EditAppAuth').html('<option value="">-- Select Auth --</option>');
							arrCase.forEach(function(t) { 
					            $('#EditAppAuth').append('<option value="'+t.key+'">'+t.label+'</option>');
					        });
						});
		            }
		          });
			});
			$('#AppAuth').change(function(){
				var authNo = $(this).val();
				$.ajax({
		            type: "POST",
		            url:"getActivitySch.php",
		            data:{
		              authNo : authNo
		            },success:function(data){
		            	data = JSON.parse(data);
		            	var arrCase = [];
				        $('#AppActivity').html('');
				        for(var x in data){
						  arrCase.push(data[x]);
						}
						//alert(arrCase);
						$.each(arrCase,function(i,v) {
			            	$('#AppActivity').html('<option value="">-- Select Activity --</option>');
							arrCase.forEach(function(t) { 
					            $('#AppActivity').append('<option value="'+t.key+'">'+t.label+'</option>');
					        });
						});
		            }
		          });
			})
			$('#EditAppAuth').change(function(){
				var authNo = $(this).val();
				$.ajax({
		            type: "POST",
		            url:"getActivitySch.php",
		            data:{
		              authNo : authNo
		            },success:function(data){
		            	data = JSON.parse(data);
		            	var arrCase = [];
				        $('#EditAppActivity').html('');
				        for(var x in data){
						  arrCase.push(data[x]);
						}
						//alert(arrCase);
						$.each(arrCase,function(i,v) {
			            	$('#EditAppActivity').html('<option value="">-- Select Activity --</option>');
							arrCase.forEach(function(t) { 
					            $('#EditAppActivity').append('<option value="'+t.key+'">'+t.label+'</option>');
					        });
						});
		            }
		          });
			})
			$("#AppStatus option[value='Rendered']").hide();
			$("#AppStatus option[value='Noshow']").hide();
			$("#AppStatus option[value='Hold']").hide();
			$("#AppStatus option[value='Cancelled']").hide();
			$("#AppStatus option[value='Cancelled by Client']").hide();
			$("#AppStatus option[value='Cancelled by Provider']").hide();
			var patientList = <?php include('getPat.php'); ?>;
			var patientList1 = <?php include('patientList.php'); ?>;
            $("#clientName").autocomplete({
                source: patientList,
                autoFocus:true
            });
            $("#AppClientName").autocomplete({
                source: patientList1,
                autoFocus:true
            });
            $("#EditAppClientName").autocomplete({
                source: patientList1,
                autoFocus:true
            });
            $("#btnGo").click(function(){
            	$("#schedulerTable").show();
            	$("#DivAddApp").hide();
            })
            $("#schedulerTable").hide();
            $("#tblSch1").hide();
            var PrimaryPhy = <?php include('primaryPhysician.php'); ?>;
            $("#providerName").autocomplete({
                source: PrimaryPhy,
                autoFocus:true
            });
            $("#AppProvider").autocomplete({
                source: PrimaryPhy,
                autoFocus:true
            });
            $("#EditAppProvider").autocomplete({
                source: PrimaryPhy,
                autoFocus:true
            });
            $("#btnAddApp").click(function(){
    //         	var appID = sessionStorage.getItem("appID");
				// if(appID == "" || appID == null){
					var truncID = "";
					var truncID1 = "";
					var patientName = $("#AppClientName").val();
					var n1=patientName.indexOf(" -");
			        var remStr = patientName.substr(0,n1+3);
			        var patName = patientName.replace(remStr,"");
					var patID = patientName;

					var providerName = $("#AppProvider").val();
					var n11=providerName.indexOf(" - ");
			        var remStr1 = providerName.substr(0,n11+3);
			        var patName1 = providerName.replace(remStr1,"");
					var patID1 = providerName;
					var authNo = $("#AppAuth").val();
					var activity = $("#AppActivity").val();
					var POS = $("#ddlLoca").val();

		            if(patID != ""){
			            var n=patID.indexOf(" -");
			            truncID = patID.substr(0,n);
			        }
			        if(patID1 != ""){
			            var n=patID1.indexOf(" -");
			            truncID1 = patID1.substr(0,n);
			        }
					var description = $("#AppDesc").val();
					//var statusApp = $("#AppStatus").val();
					var start_date = convertDt($("#AppFromTime").val());
					var end_date = convertDt($("#AppToTime").val());
					$.ajax({
			          type: "POST",
			          url:"addAppointment.php",
			          data:{
			            patientName : patName,
			            description : description,
			            statusApp : "Confirmed",
			            start_date : start_date,
			            end_date : end_date,
			            patientID : truncID,
			            providerName : patName1,
			            providerID : truncID1,
			            authNo : authNo,
			            activity : activity,
			            POS : POS
			          },success:function(result){
			          	if(result == "not-allow"|| result == "no-auth"){
			          		alert("Auth Expired or You have exceeded the Total Auth Limit");
			          		sessionStorage.removeItem("appID");
			          	}else{
			          		alert("Appointment Added successfully");
			          		sessionStorage.removeItem("appID");
			          		//window.location.reload();
			          		$("#AppProvider").val("");
			          		$("#AppFromTime").val("");
			          		$("#AppToTime").val("");
			          		$("#AppDesc").val("");
			          		//document.getElementById("chkLoc").checked = false;
			          		//$("#divLoc").hide();
			          		$('#AppActivity').prop('selectedIndex',0);
			          		$('#AppStatus').prop('selectedIndex',0);
			          		$('#AppStepTime').prop('selectedIndex',0);
			          		getAppointData();
			          	}
			          }
			      	});
				//}
            })
			$("#btnUpdateApp").click(function(){
				var appID = document.getElementById("_hdnAppID").value;
				if(appID != "" && appID != null){
					var statusApp = $("#EditAppStatus").val();
					$.ajax({
			          type: "POST",
			          url:"updAppointment.php",
			          data:{
			          	appID : appID,
			          	statusApp : statusApp
			          },success:function(result){
			          	alert("Appointment Updated successfully");
			          	sessionStorage.removeItem("appID");
			          	window.location.reload();
			          }
			      	});
				}
				else{
					alert("Error on selecting Appointment. Contact with Administrator");
				}
			})
            $("#btnGo").click(function(){
            	var clientName = $("#clientName").val();
            	var providerNa = $("#providerName").val();
            	var len = providerNa.length;
            	var n=providerNa.indexOf("-");
                var providerName = providerNa.substr(n+2,len);
            	var statusName = $("#statusName").val();
            	var locationName = $("#locationName").val();
            	var fromDate = convertDate($("#fromDate").val());
            	var toDate = convertDate($("#toDate").val());
            	$.ajax({
		          type: "POST",
		          url:"getAppoints.php",
		          data:{
		            clientName : clientName,
		            providerName : providerName,
		            statusName : statusName,
		            locationName : locationName,
		            fromDate : fromDate,
		            toDate : toDate,
		          },success:function(result){
		          	var data1 = JSON.parse(result);

		              var dt = [];
		              $.each(data1,function(i,v) {
		                 dt.push([data1[i].clientName,data1[i].authNo,data1[i].activity,data1[i].fromDate,data1[i].fromTime,data1[i].toTime,data1[i].loc,data1[i].appStatus,data1[i].providerName,data1[i].appID]);
		              });
			          $('#tblSch').DataTable({
			            destroy: true,
			            "pageLength": 5,
			           "data": dt,
			              columns: [
			               {"title": "Client Name"},
			               {"title": "Auth #"},
			               {"title": "ActivityName"},
			               {"title": "Scheduled Date"},
			               {"title": "From Time"},
			               {"title": "To Time"},
			               {"title": "Location"},
			               {"title": "Status"},
			              {"title": "Provider Name"},
			              {"title": "Action",
			              	"render": function ( data, type, full, meta ) {
			                   return '<a href="javascript:void(0);" class="editSch editSch'+data+'"><i class="icon icon-pencil"></i></a>';
			                }
			          	  }
			              ]
			            });
		          }
		      	});
            })
			$(document).on('click','.editSch',function() {
				var temp = $(this).attr('class').split(' ')[1];
				var newTemp = temp.replace('editSch','');
				var appID = newTemp;
				document.getElementById("_hdnAppID").value = appID;
				sessionStorage.setItem("appID",appID);
				$("#EditAppoint").modal("show");
				$("#EditAppClientName").attr("disabled",true);
				$("#EditAppProvider").attr("disabled",true);
				$("#EditAppAuth").attr("disabled",true);
				$("#EditAppActivity").attr("disabled",true);
				$("#AppStatus option[value='Rendered']").show();
				$("#AppStatus option[value='Noshow']").show();
				$("#AppStatus option[value='Hold']").show();
				$("#AppStatus option[value='Cancelled']").show();
				$("#AppStatus option[value='Cancelled by Client']").show();
				$("#AppStatus option[value='Cancelled by Provider']").show();
				$("#EditAppDesc").attr("disabled",true);
				$("#divDuration").hide();
				$("#EditAppFromTime").attr("disabled",true);
				$("#EditAppToTime").attr("disabled",true);
				$.ajax({
		            type: "POST",
		            url:"getAppInfo.php",
		            data:{
		              appID : appID
		            },success:function(data){
		            	data = JSON.parse(data);
		            	$.ajax({
				            type: "POST",
				            async: false,
				            url:"getAuthbyPatient.php",
				            data:{
				              patientID : data[0].AppPatientID
				            },success:function(res){
				            	res = JSON.parse(res);
				            	var arrCase = [];
						        $('#EditAppAuth').html('');
						        for(var x in res){
								  arrCase.push(res[x]);
								}
								//alert(arrCase);
								$.each(arrCase,function(i,v) {
					            	$('#EditAppAuth').html('-- Select Auth --');
									arrCase.forEach(function(t) { 
							            $('#EditAppAuth').append('<option value="'+t.key+'">'+t.label+'</option>');
							        });
								});
				            }
				          });
		            	document.getElementById("EditAppAuth").value = data[0].authNo;
		            	$.ajax({
				            type: "POST",
				            url:"getActivitySch.php",
				            async:false,
				            data:{
				              authNo : data[0].authNo
				            },success:function(data){
				            	data = JSON.parse(data);
				            	var arrCase = [];
						        $('#EditAppActivity').html('');
						        for(var x in data){
								  arrCase.push(data[x]);
								}
								//alert(arrCase);
								$.each(arrCase,function(i,v) {
					            	$('#EditAppActivity').html('-- Select Activity --');
									arrCase.forEach(function(t) { 
							            $('#EditAppActivity').append('<option value="'+t.key+'">'+t.label+'</option>');
							        });
								});
				            }
				          });
		            	document.getElementById("EditAppClientName").value = data[0].clientName;
		            	document.getElementById("EditAppActivity").value = data[0].activity;
		            	document.getElementById("EditAppProvider").value = data[0].providerName;
		            	document.getElementById("EditAppStatus").value = data[0].appStatus;
		            	//document.getElementById("EditAppStepTime").value = data[0].;
		            	document.getElementById("EditAppFromTime").value = data[0].fromTime;
		            	document.getElementById("EditAppToTime").value = data[0].toTime;
		            	 //$("#EditAppStatus option[value='Rendered']").show();
		            	document.getElementById("EditAppDesc").value = data[0].appDesc;

		            }
		          });
			});
            $('#fromDate').datetimepicker({
	            dayOfWeekStart: 1,
	            lang: 'en',
	            format: 'm-d-Y',
	            timepicker: false,
	            //minDate: '-2013/01/02',
	            //maxDate: '+2014/12/31',
	            formatDate: 'm-d-Y',
	            closeOnDateSelect: true
	        });
	        $('#toDate').datetimepicker({
	            dayOfWeekStart: 1,
	            lang: 'en',
	            format: 'm-d-Y',
	            timepicker: false,
	            //minDate: '-2013/01/02',
	            //maxDate: '+2014/12/31',
	            formatDate: 'm-d-Y',
	            closeOnDateSelect: true
	        });
	        var dt = [];
	          $('#tblSch').DataTable({
	            destroy: true,
	           "aaSorting": [[ 0, "desc" ]],
	           "data": dt,
	              columns: [
	               {"title": "Client Name"},
	               {"title": "Auth #"},
	               {"title": "ActivityName"},
	               {"title": "Scheduled Date"},
	               {"title": "From Time"},
	               {"title": "To Time"},
	               {"title": "Location"},
	               {"title": "Status"},
	              {"title": "Provider Name"},
	              {"title": "Action",
	              	"render": function ( data, type, full, meta ) {
	                   return '<a href="javascript:void(0);" class="editSch editSch'+data+'"><i class="icon icon-pencil"></i></a>';
	                }
	          	  }
	              ]
	            });
	          $("#AddAppoint").click(function(){
	          	window.location.reload();
	          })
		});
		
		function convertDt(i){
			i = new Date(i);
		 var res = i.toLocaleDateString();
		 var yy = i.getFullYear();
		 var mm = pad(i.getMonth()+1);
		 var dd = pad(i.getDate());
		 var hours = pad(i.getHours());
		 var min = pad(i.getMinutes());
		 return yy+"-"+mm+"-"+dd+" "+hours+":"+min;
		}
		function nonConvertDt(i){
		 var res = i.toLocaleDateString();
		 var yy = i.getFullYear();
		 var mm = pad(i.getMonth()+1);
		 var dd = pad(i.getDate());
		 var hours = pad(i.getHours());
		 var min = pad(i.getMinutes());
		 return mm+"-"+dd+"-"+yy+" "+hours+":"+min;
		}
		function getAppointData(){
			var patientName = $("#AppClientName").val();
			var n1=patientName.indexOf(" -");
	        var remStr = patientName.substr(0,n1+3);
	        var patName = patientName.replace(remStr,"");
			var patID = patientName;
			var truncID = "";
			if(patID != ""){
	            var n=patID.indexOf(" -");
	            truncID = patID.substr(0,n);
	        }
			var authNo = $("#AppAuth").val();
        $.post("getRecentAppoints.php",
            {
                patientID: truncID,
                authNo : authNo
            },
            function(data1, status){
              data1 = JSON.parse(data1);
              $("#tblSch1").show();
              var dt = [];
              $.each(data1,function(i,v) {
                 dt.push([data1[i].clientName,data1[i].authNo,data1[i].activity,data1[i].fromDate,data1[i].fromTime,data1[i].toTime,data1[i].loc,data1[i].appStatus,data1[i].providerName]);
              });
	          $('#tblSch1').DataTable({
	            destroy: true,
	            "pageLength": 5,
	           "data": dt,
	              columns: [
	               {"title": "Client Name"},
	               {"title": "Auth #"},
	               {"title": "ActivityName"},
	               {"title": "Scheduled Date"},
	               {"title": "From Time"},
	               {"title": "To Time"},
	               {"title": "Location"},
	               {"title": "Status"},
	              {"title": "Provider Name"}
	              ]
	            });
            });
		}
		function pad(n){return n<10 ? '0'+n : n}
	</script>
</head>
<!-- <body onload="init();"> -->
<body>
	<!-- Header -->
    <header class="header navbar navbar-fixed-top" role="banner" style="background-image: url('assets/bg.jpg'); background-repeat: repeat-x;">
        <!-- Top Navigation Bar -->
        <div class="container">

            <!-- Only visible on smartphones, menu toggle -->
            <ul class="nav navbar-nav">
                <li class="nav-toggle"><a href="javascript:void(0);" title=""><i class="icon-reorder"></i></a></li>
            </ul>

            <!-- Logo -->
            <a class="navbar-brand" style="text-align:center;padding-right:50px;" href="javascript:void(0);">
                <img src="assets/logo2.png"/>
                <strong>medABA</strong>
            </a>
            <!-- /logo -->

            <!-- Sidebar Toggler -->
            <a href="#" class="toggle-sidebar bs-tooltip" data-placement="bottom" data-original-title="Toggle navigation">
                <i class="icon-reorder"></i>
            </a>
            <!-- /Sidebar Toggler -->

            <!-- Top Left Menu -->
            <ul class="nav navbar-nav navbar-left hidden-xs hidden-sm" style="list-style:none;">
                <li>
                    <a href="ViewPatient.php" class="dropdown-toggle">
                        Clients
                    </a>
                </li>
                <li>
                    <a href="scheduler.php" class="dropdown-toggle">
                        Scheduler
                    </a>
                </li>
                <li>
                    <a href="Deposit.php" class="dropdown-toggle">
                        Deposit
                    </a>
                </li>
                <li>
                    <a href="Ledger.php" class="dropdown-toggle">
                        Ledger
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                        EDI
                        <i class="icon-caret-down small"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="generateAllEDI.php">
                            <i class="icon-angle-right"></i>
                            Generate EDI
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="EDIList.php">
                            <i class="icon-angle-right"></i>
                            EDI List
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="charts.html" class="dropdown-toggle" data-toggle="dropdown">
                        Settings
                        <i class="icon-caret-down small"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="ViewPractice.php">
                            <i class="icon-angle-right"></i>
                            Practices
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="ViewPhysician.php">
                            <i class="icon-angle-right"></i>
                            Physicians
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="ViewServiceLoc.php">
                            <i class="icon-angle-right"></i>
                            Locations &amp; Facilities
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="javascript:void(0);" style="font-weight:600;">
                            Codes
                            </a>
                            <li>
                                <a href="ViewCPT.php">
                                <i class="icon-angle-right"></i>
                                CPT/Procedure
                                </a>
                            </li>
                            <li>
                                <a href="ViewDX.php">
                                <i class="icon-angle-right"></i>
                                ICD 10/ICD 9 Library
                                </a>
                            </li>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="insurances.php">
                            <i class="icon-angle-right"></i>
                            Insurances
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- /Top Left Menu -->

            <!-- Top Right Menu -->
            <ul class="nav navbar-nav navbar-right">
                <!-- Notifications -->
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-warning-sign"></i>
                        <span class="badge">5</span>
                    </a>
                    <ul class="dropdown-menu extended notification">
                        <li class="title">
                            <p>You have 5 new notifications</p>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="label label-success"><i class="icon-plus"></i></span>
                                <span class="message">New user registration.</span>
                                <span class="time">1 mins</span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="label label-danger"><i class="icon-warning-sign"></i></span>
                                <span class="message">High CPU load on cluster #2.</span>
                                <span class="time">5 mins</span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="label label-success"><i class="icon-plus"></i></span>
                                <span class="message">New user registration.</span>
                                <span class="time">10 mins</span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="label label-info"><i class="icon-bullhorn"></i></span>
                                <span class="message">New items are in queue.</span>
                                <span class="time">25 mins</span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="label label-warning"><i class="icon-bolt"></i></span>
                                <span class="message">Disk space to 85% full.</span>
                                <span class="time">55 mins</span>
                            </a>
                        </li>
                        <li class="footer">
                            <a href="javascript:void(0);">View all notifications</a>
                        </li>
                    </ul>
                </li>

                <!-- Tasks -->
                <li class="dropdown hidden-xs hidden-sm">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-tasks"></i>
                        <span class="badge">7</span>
                    </a>
                    <ul class="dropdown-menu extended notification">
                        <li class="title">
                            <p>You have 7 pending tasks</p>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="task">
                                    <span class="desc">Preparing new release</span>
                                    <span class="percent">30%</span>
                                </span>
                                <div class="progress progress-small">
                                    <div style="width: 30%;" class="progress-bar progress-bar-info"></div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="task">
                                    <span class="desc">Change management</span>
                                    <span class="percent">80%</span>
                                </span>
                                <div class="progress progress-small progress-striped active">
                                    <div style="width: 80%;" class="progress-bar progress-bar-danger"></div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="task">
                                    <span class="desc">Mobile development</span>
                                    <span class="percent">60%</span>
                                </span>
                                <div class="progress progress-small">
                                    <div style="width: 60%;" class="progress-bar progress-bar-success"></div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="task">
                                    <span class="desc">Database migration</span>
                                    <span class="percent">20%</span>
                                </span>
                                <div class="progress progress-small">
                                    <div style="width: 20%;" class="progress-bar progress-bar-warning"></div>
                                </div>
                            </a>
                        </li>
                        <li class="footer">
                            <a href="javascript:void(0);">View all tasks</a>
                        </li>
                    </ul>
                </li>

                <!-- Messages -->
                <li class="dropdown hidden-xs hidden-sm">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-envelope"></i>
                        <span class="badge">1</span>
                    </a>
                    <ul class="dropdown-menu extended notification">
                        <li class="title">
                            <p>You have 3 new messages</p>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="photo"><img src="assets/img/demo/avatar-1.jpg" alt="" /></span>
                                <span class="subject">
                                    <span class="from">Bob Carter</span>
                                    <span class="time">Just Now</span>
                                </span>
                                <span class="text">
                                    Consetetur sadipscing elitr...
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="photo"><img src="assets/img/demo/avatar-2.jpg" alt="" /></span>
                                <span class="subject">
                                    <span class="from">Jane Doe</span>
                                    <span class="time">45 mins</span>
                                </span>
                                <span class="text">
                                    Sed diam nonumy...
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="photo"><img src="assets/img/demo/avatar-3.jpg" alt="" /></span>
                                <span class="subject">
                                    <span class="from">Patrick Nilson</span>
                                    <span class="time">6 hours</span>
                                </span>
                                <span class="text">
                                    No sea takimata sanctus...
                                </span>
                            </a>
                        </li>
                        <li class="footer">
                            <a href="javascript:void(0);">View all messages</a>
                        </li>
                    </ul>
                </li>

                <!-- .row .row-bg Toggler -->
                <li>
                    <a href="#" class="dropdown-toggle row-bg-toggle">
                        <i class="icon-resize-vertical"></i>
                    </a>
                </li>


                <!-- User Login Dropdown -->
                <li class="dropdown user">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <!--<img alt="" src="assets/img/avatar1_small.jpg" />-->
                        <i class="icon-male"></i>
                        <span class="username" id="practiceName"></span>
                        <i class="icon-caret-down small"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="javascript:void(0);"><i class="icon-user"></i> My Profile</a></li>
                        <li><a href="javascript:void(0);"><i class="icon-calendar"></i> My Calendar</a></li>
                        <li><a href="javascript:void(0);"><i class="icon-tasks"></i> My Tasks</a></li>
                        <li class="divider"></li>
                        <li><a href="login.php"><i class="icon-key"></i> Log Out</a></li>
                    </ul>
                </li>
                <!-- /user login dropdown -->
            </ul>
            <!-- /Top Right Menu -->
        </div>
        <!-- /top navigation bar -->

    </header> <!-- /.header -->

  <div id="container">
    <!-- Breadcrumbs line -->
    <div class="crumbs">
      <ul id="breadcrumbs" class="breadcrumb">
        <li class="current">
          <i class="icon-home"></i>
          <a href="index.html">Dashboard</a>
        </li>
        
      </ul>
      <a href="javascript:void(0);"><img src="assets/icons/Settings.png" title="Settings" style="float:right; padding-top:0.3em; padding-right:2em;"/></a>
          <a href="Ledger.php"><img src="assets/icons/Claim_search.png" title="Claim Search" style="float:right; padding-top:0.3em; padding-right:2em;"/></a>
          <a href="AddClaim.php"><img src="assets/icons/Claim_add.png" title="Add Claim" style="float:right; padding-top:0.3em; padding-right:2em;"/></a>
          <a href="ViewPatient.php"><img src="assets/icons/Patient_search.png" title="Patient Search" style="float:right; padding-top:0.3em; padding-right:2em;"/></a>
          <a href="AddPatient.php"><img src="assets/icons/Patient_add.png" title="Add Patient" style="float:right; padding-top:0.3em; padding-right:2em;"/></a>
          <a href="dashboard.html"><img src="assets/icons/Home.png" title="Home" style="float:right; padding-top:0.3em; padding-right:2em;"/></a>

    </div>
    <div class="clearfix"></div>
<!-- <div style='height:20px; padding:5px 10px;'>
	<input type="button" name="show" value="Show" onclick="show()" style="right:400px;" />
</div> -->
<div class="form-group col-md-3" style="margin-top:60px; height:100%;">
	<div class="form-group col-md-12" style="padding-bottom:30px;">
	    <label class="control-label col-md-6">Client Name </label>
	    <div class="col-md-6">
	        <input type="text" class="form-control required" id="clientName" placeholder="Client Name" />
	    </div>
	</div>
	<div class="form-group col-md-12" style="padding-bottom:30px;">
	    <label class="control-label col-md-6">Provider Name </label>	
	    <div class="col-md-6">
	        <input type="text" class="form-control required" id="providerName" placeholder="Provider Name" />
	    </div>
	</div>
	<div class="form-group col-md-12" style="padding-bottom:30px;">
	    <label class="control-label col-md-6">Location Name </label>
	    <div class="col-md-6">
	        <select class="form-control" id="locationName">
	        	<option value=""></option>
	        	<option value="12">Home</option>
	        	<option value="11">Office</option>
	        </select>
	    </div>
	</div>
	<div class="form-group col-md-12" style="padding-bottom:30px;">
	    <label class="control-label col-md-6">Status </label>
	    <div class="col-md-6">
	        <select class="form-control" id="statusName">
	        	<option value=""></option>
		        	<option value="Confirm">Confirmed</option>
		        	<option value="UnConfirm">UnConfirmed</option>
		        	<option value="Noshow">No Show</option>
		        	<option value="Cancelled">Cancelled</option>
		        	<option value="Hold">Hold</option>
		        	<option value="Cancelled by Client">Cancelled by Client</option>
		        	<option value="Cancelled by Provider">Cancelled by Provider</option>
		        	<option value="Rendered">Rendered</option>
	        </select>
	    </div>
	</div>
	<div class="form-group col-md-12" style="padding-bottom:30px;">
	    <label class="control-label col-md-6">From Date </label>	
	    <div class="col-md-6">
	        <input type="text" class="form-control required" id="fromDate" placeholder="From Date" />
	    </div>
	</div>
	<div class="form-group col-md-12" style="padding-bottom:30px;">
	    <label class="control-label col-md-6">To Date </label>	
	    <div class="col-md-6">
	        <input type="text" class="form-control required" id="toDate" placeholder="To Date" />
	    </div>
	</div>
	<div class="form-group col-md-12" style="padding-bottom:30px;">
		<label class="control-label col-md-4"></label>
	    <input type="button" class="btn btn-primary" id="btnGo" value="Get Details" />
	</div>
</div>
<div class="col-md-9" id="rightDiv" style=" height:100%; overflow:scroll">
	<div id="schedulerTable" style='width:75%; height:100%; margin-top:100px; float:left;'>
		<table id="tblSch" class="table-bordered" >
	    </table>
	    <input type="button" id="AddAppoint" value="Go to Add Appointment" class="btn btn-primary pull-right" style="margin:20px 20px 0 0;">
	</div>
	<div id="DivAddApp">
		<h2 style="text-align:center">Add Appointment</h2>
		<div class="form-group col-md-6">
			<label class="control-label col-md-4">Client Name : </label>
		    <div class="col-md-6">
		        <input type="text" class="form-control required" id="AppClientName" placeholder="Client Name" />
		    </div>
		</div>
		<div class="form-group col-md-6" style="padding-bottom:30px;">
			<label class="control-label col-md-4">Auth # : </label>
		    <div class="col-md-6">
		        <select class="form-control" id="AppAuth"></select>
		    </div>
		</div>
		<div class="form-group col-md-6" style="padding-bottom:30px;">
			<label class="control-label col-md-4">Activity : </label>
		    <div class="col-md-6">
		        <select class="form-control" id="AppActivity"></select>
		    </div>
		</div>
		<div class="form-group col-md-6" style="padding-bottom:30px;">
			<label class="control-label col-md-4">Provider Name : </label>
		    <div class="col-md-6">
		        <input type="text" class="form-control required" id="AppProvider" placeholder="Provider Name" />
		    </div>
		</div>
		<!-- <div class="form-group col-md-6" style="padding-bottom:30px;">
			<label class="control-label col-md-4">Status</label>
		    <div class="col-md-6">
		        <select class="form-control" id="AppStatus">
		        	<option value=""></option>
		        	<option value="Confirm">Confirmed</option>
		        	<option value="UnConfirm">UnConfirmed</option>
		        	<option value="Noshow">No Show</option>
		        	<option value="Cancelled">Cancelled</option>
		        	<option value="Hold">Hold</option>
		        	<option value="Cancelled by Client">Cancelled by Client</option>
		        	<option value="Cancelled by Provider">Cancelled by Provider</option>
		        	<option value="Rendered">Rendered</option>
		        </select>
		    </div>
		</div> -->
		<div class="form-group col-md-6" style="padding-bottom:30px;">
			<label class="control-label col-md-4">Time Duration</label>
		    <div class="col-md-6">
		        <select class="form-control" id="AppStepTime">
		        	<option value="0">-- Select Duration --</option>
		        	<option value="15">15 mins</option>
		        	<option value="30">30 mins</option>
		        	<option value="45">45 mins</option>
		        	<option value="60">1 Hour</option>

		        	<option value="75">1 Hour 15 mins</option>
		        	<option value="90">1 Hour 30 mins</option>
		        	<option value="105">1 Hour 45 mins</option>
		        	<option value="120">2 Hour</option>

		        	<option value="135">2 Hour 15 mins</option>
		        	<option value="150">2 Hour 30 mins</option>
		        	<option value="165">2 Hour 45 mins</option>
		        	<option value="180">3 Hour</option>

		        	<option value="195">3 Hour 15 mins</option>
		        	<option value="210">3 Hour 30 mins</option>
		        	<option value="225">3 Hour 45 mins</option>
		        	<option value="240">4 Hour</option>

		        	<option value="255">4 Hour 15 mins</option>
		        	<option value="270">4 Hour 30 mins</option>
		        	<option value="285">4 Hour 45 mins</option>
		        	<option value="300">5 Hour</option>

		        	<option value="315">5 Hour 15 mins</option>
		        	<option value="330">5 Hour 30 mins</option>
		        	<option value="345">5 Hour 45 mins</option>
		        	<option value="360">6 Hour</option>

		        	<option value="375">6 Hour 15 mins</option>
		        	<option value="390">6 Hour 30 mins</option>
		        	<option value="405">6 Hour 45 mins</option>
		        	<option value="420">7 Hour</option>

		        	<option value="435">7 Hour 15 mins</option>
		        	<option value="450">7 Hour 30 mins</option>
		        	<option value="465">7 Hour 45 mins</option>
		        	<option value="480">8 Hour</option>

		        	<option value="495">8 Hour 15 mins</option>
		        	<option value="510">8 Hour 30 mins</option>
		        	<option value="525">8 Hour 45 mins</option>
		        	<option value="540">9 Hour</option>

		        	<option value="555">9 Hour 15 mins</option>
		        	<option value="570">9 Hour 30 mins</option>
		        	<option value="585">9 Hour 45 mins</option>
		        	<option value="600">10 Hour</option>
		        </select>
		    </div>
		</div>
		<div class="form-group col-md-6" style="padding-bottom:30px;">
			<label class="control-label col-md-4">From Time : </label>
		    <div class="col-md-6">
		        <input type="text" class="form-control required" id="AppFromTime" />
		    </div>
		</div>
		<div class="form-group col-md-6" style="padding-bottom:30px;">
			<label class="control-label col-md-4">To Time : </label>
		    <div class="col-md-6">
		        <input type="text" class="form-control required" id="AppToTime" />
		    </div>
		</div>
		<div class="col-md-6" style="padding-top:10px;">
			<label class="control-label col-md-4">Location : </label>
			<div class="col-md-6">
				<select class="form-control" id="ddlLoca">
				</select>
			</div>
		</div>
		<div class="clearfix"></div>
		<!-- <div class="form-group col-md-2" style="padding-bottom:30px;">
			<label class="checkbox">
				<input type="checkbox" class="uniform" id="chkLoc" value=""> Choose Location
			</label>
		</div>
		<div class="col-md-4" id="divLoc" style="padding-top:10px;">
			<label class="control-label col-md-4">Location : </label>
			<div class="col-md-7">
				<select class="form-control" id="ddlLoca">
					<option value=""> Select Location </option>
					<option value="Home">Home</option>
					<option value="Office">Office</option>
					<option value="School">School</option>
				</select>
			</div>
		</div> -->
		<div class="form-group col-md-3" style="padding-bottom:30px;">
			<label class="checkbox">
				<input type="checkbox" class="uniform" id="chkRpt" value=""> Repeat
			</label>
		</div>
		<div class="clearfix"></div>
		<div class="form-group col-md-12" style="padding-bottom:30px;">
			<label class="control-label col-md-2">Description : </label>
		    <div class="col-md-9">
		        <textarea class="form-control" rows="5" cols="3" id="AppDesc"></textarea>
		    </div>
		</div>
		<div class="clearfix"></div>
		<div class="form-group col-md-3 pull-right" style="padding-top:10px;">
		    <input type="button" class="btn btn-primary" id="btnAddApp" value="Add Appointment"/>
		</div>
		<table id="tblSch1" class="table-bordered" >
	    </table>
	</div>
</div>
<div class="modal fade" id="EditAppoint" tabindex="-1">
    <div class="modal-dialog" style="width:80%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Edit Appointment</h4>
            </div>
            <div class="modal-body" style="min-height:420px;">
            	<input id="_hdnAppID" type="hidden">
                <div class="form-group col-md-6">
					<label class="control-label col-md-4">Client Name : </label>
				    <div class="col-md-6">
				        <input type="text" class="form-control required" id="EditAppClientName" placeholder="Client Name" />
				    </div>
				</div>
				<div class="form-group col-md-6" style="padding-bottom:30px;">
					<label class="control-label col-md-4">Auth # : </label>
				    <div class="col-md-6">
				        <select class="form-control" id="EditAppAuth"><option></option></select>
				    </div>
				</div>
				<div class="form-group col-md-6" style="padding-bottom:30px;">
					<label class="control-label col-md-4">Activity : </label>
				    <div class="col-md-6">
				        <select class="form-control" id="EditAppActivity"></select>
				    </div>
				</div>
				<div class="form-group col-md-6" style="padding-bottom:30px;">
					<label class="control-label col-md-4">Provider Name : </label>
				    <div class="col-md-6">
				        <input type="text" class="form-control required" id="EditAppProvider" placeholder="Provider Name" />
				    </div>
				</div>
				<div class="form-group col-md-6" style="padding-bottom:30px;">
					<label class="control-label col-md-4">Status</label>
				    <div class="col-md-6">
				        <select class="form-control" id="EditAppStatus">
				        	<option value=""></option>
				        	<option value="Confirm">Confirmed</option>
				        	<option value="UnConfirm">UnConfirmed</option>
				        	<option value="Noshow">No Show</option>
				        	<option value="Cancelled">Cancelled</option>
				        	<option value="Hold">Hold</option>
				        	<option value="Cancelled by Client">Cancelled by Client</option>
				        	<option value="Cancelled by Provider">Cancelled by Provider</option>
				        	<option value="Rendered">Rendered</option>
				        </select>
				    </div>
				</div>
				<div class="form-group col-md-6" style="padding-bottom:30px;" id="divDuration">
					<label class="control-label col-md-4">Time Duration</label>
				    <div class="col-md-6">
				        <select class="form-control" id="EditAppStepTime">
				        	<option value="0">-- Select Duration --</option>
				        	<option value="15">15 mins</option>
				        	<option value="30">30 mins</option>
				        	<option value="45">45 mins</option>
				        	<option value="60">60 mins</option>
				        </select>
				    </div>
				</div>
				<div class="form-group col-md-6" style="padding-bottom:30px;">
					<label class="control-label col-md-4">From Time : </label>
				    <div class="col-md-6">
				        <input type="text" class="form-control required" id="EditAppFromTime" />
				    </div>
				</div>
				<div class="form-group col-md-6" style="padding-bottom:30px;">
					<label class="control-label col-md-4">To Time : </label>
				    <div class="col-md-6">
				        <input type="text" class="form-control required" id="EditAppToTime" />
				    </div>
				</div>
				<div class="form-group col-md-6" style="padding-bottom:30px;">
					<label class="control-label col-md-4">Description : </label>
				    <div class="col-md-6">
				        <textarea class="form-control" rows="5" cols="5" id="EditAppDesc"></textarea>
				    </div>
				</div>
            </div>
            <div class="modal-footer">
                <input type="button" class="btn btn-default" data-dismiss="modal" value="Exit" />
                <input type="button" class="btn btn-primary" id="btnUpdateApp" value="Update Appointment"/>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<link href="plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css" rel='stylesheet' type='text/css'>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
    <script src="assets/js/jquery.datetimepicker.js"></script>
    <link href="assets/css/jquery.datetimepicker.css" rel="stylesheet" />
    <link href="plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
</body>