<?php

$con = mysql_connect("localhost:3306","curis_user","Curis@123");

if(!$con){
  die("Error : ".mysql_error());
}
mysql_select_db("curismed_aba",$con);

$info = array();

// $from = date("Y-m-d",strtotime("-4 month"));
// $to = date("Y-m-d",strtotime("-10 year"));
$patientID = $_POST['patientID'];
$filterFrom = $_POST['filterFrom'];
$filterTo = $_POST['filterTo'];
//$patientID = $_POST['patientID'];

if($patientID != ""){
  $result = mysql_query("SELECT m_serviceLocations.billingName, m_serviceLocations.address, m_patients.fullName,m_patients.patientID, m_patients.addrStreet1, m_patients.addrCity, m_patients.addrState, m_patients.addrZip, m_patients.chartNo, m_claims.fromDt, m_claims.total, m_claims.claimBalance,m_claims.paid, m_claims.adjustment, m_claims.claimBalance FROM m_claims inner join m_cases on m_cases.caseID = m_claims.caseID inner join m_patients on m_patients.patientID = m_cases.patientID inner join m_serviceLocations on m_serviceLocations.serviceLocID = m_patients.defaultServiceLoc where m_patients.patientID = '$patientID' AND m_claims.insuranceID = '-100' AND m_claims.fromDt between '$filterFrom' AND '$filterTo'");
  if(mysql_num_rows($result) > 0){
    while($row = mysql_fetch_array($result)){
      $billingName = $row['billingName'];
      $address = explode(" ",$row['address']);
      $billingAddr = $address[0]; // piece1
      $billingTotAddr = $address[1];
      $addrStreet1 = $row['addrStreet1'];
      $patientID = $row['patientID'];
      $totAddr = $row['addrCity'].' '.$row['addrState'].' '.$row['addrZip'];
      $chartNo = $row['chartNo'];

      $fromDt = $row['fromDt'];
      $fullName = $row['fullName'];
      $total = $row['total'];
      $claimBalance = $row['claimBalance'];
      $paid = $row['paid'];
      $adjustment = $row['adjustment'];
    }
    require_once('SearchPT/tcpdf_include.php');

  //create new PDF document
  $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
  //set document information
  $pdf->SetCreator(PDF_CREATOR);
  $pdf->SetAuthor('Nicola Asuni');
  $pdf->SetTitle('INVOICE # :'.$fullName);
  $pdf->SetSubject('TCPDF Tutorial');
  $pdf->SetKeywords('TCPDF, PDF, example, test, guide');

  //set default header data

  //set header and footer fonts
  $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
  $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

  //set default monospaced font
  $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

  //set margins
  $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
  $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
  $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

  //set auto page breaks
  $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

  //set image scale factor
  $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

  //set some language-dependent strings (optional)
  if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
      require_once(dirname(__FILE__).'/lang/eng.php');
      $pdf->setLanguageArray($l);
  }


  //add a page
  $pdf->AddPage('L', 'A4');

  $pdf->SetFont('helvetica', '', 2);

  $CurrPage = $pdf->getAliasNumPage();

  $temp = '';
  $temp .='<style type="text/css">
  .tg  {border-collapse:collapse;border-spacing:0; border-left:1px solid #000;border-right:1px solid #000;}
  .tg td{font-family:Arial, sans-serif;font-size:10px;border-left:1px solid #000;border-right:1px solid #000;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
  .tg th{font-family:Arial, sans-serif;font-size:10px;border-left:1px solid #000;border-right:1px solid #000;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
  .tg .tg-baqh{text-align:center;vertical-align:top;border-left:1px solid #000;border-right:1px solid #000;}
  .tg .tg-yw4l{vertical-align:top;border-bottom:1px solid #000;}
  </style>
  <table class="tg">
    <tr>
      <th class="tg-yw4l" colspan="5">'.$billingName.'</th>
      <th class="tg-yw4l" colspan="7"></th>
    </tr>
    <tr>
      <td class="tg-yw4l" colspan="5">'.$billingAddr.'</td>
      <td class="tg-baqh" colspan="7">IF PAYING BY CREDIT / DEBIT CARD, FILL OUT BELOW</td>
    </tr>
    <tr>
      <td class="tg-yw4l" colspan="5">'.$billingTotAddr.'</td>
      <td class="tg-baqh" colspan="7">CHECK CARD USING FOR PAYMENT</td>
    </tr>
    <tr>
      <td class="tg-yw4l" colspan="5" rowspan="3"></td>
      <td class="tg-baqh" style="background-color:#ccc"><input type="checkbox" name="box" value="1" readonly="true" /> MASTER CARD</td>
      <td class="tg-baqh" style="background-color:#ccc"><input type="checkbox" name="box" value="2" readonly="true" /> DISCOVER</td>
      <td class="tg-baqh" style="background-color:#ccc"><input type="checkbox" name="box" value="3" readonly="true" /> VISA</td>
      <td class="tg-baqh" style="background-color:#ccc"><input type="checkbox" name="box" value="4" readonly="true" /> AMERICAN EXPRESS</td>
      <td class="tg-baqh" style="background-color:#ccc"><input type="checkbox" name="box" value="5" readonly="true" /> DINERS</td>
      <td class="tg-baqh" colspan="2" style="background-color:#ccc"><input type="checkbox" name="box" value="6" readonly="true" /> DEBIT CARD</td>
    </tr>
    <tr>
      <td class="tg-baqh" style="background-color:#ccc">CARD NUMBER : </td>
      <td class="tg-baqh" colspan="3"></td>
      <td class="tg-baqh" style="background-color:#ccc">SECURITY CODE : </td>
      <td class="tg-baqh" colspan="2"></td>
    </tr>
    <tr>
      <td class="tg-baqh" style="background-color:#ccc">SIGNATURE :</td>
      <td class="tg-baqh" colspan="3"></td>
      <td class="tg-baqh" style="background-color:#ccc">EXP. DATE :</td>
      <td class="tg-baqh" colspan="2"></td>
    </tr>
    <tr>
      <td class="tg-yw4l" colspan="6">'.$fullName.'</td>
      <td class="tg-yw4l" colspan="6"></td>
    </tr>
    <tr>
      <td class="tg-yw4l" colspan="6">'.$addrStreet1.'</td>
      <td class="tg-yw4l" colspan="2" style="background-color:#ccc">STATEMENT DATE</td>
      <td class="tg-yw4l" colspan="2" style="background-color:#ccc">PAY THIS AMOUNT</td>
      <td class="tg-yw4l" colspan="2" style="background-color:#ccc">ACCT #</td>
    </tr>
    <tr>
      <td class="tg-yw4l" colspan="6">'.$totAddr.'</td>
      <td class="tg-yw4l" colspan="2">'.$fromDt.'</td>
      <td class="tg-yw4l" colspan="2"> $ '.$claimBalance.'</td>
      <td class="tg-yw4l" colspan="2">'.$chartNo.'</td>
    </tr>
    <tr>
      <td class="tg-yw4l" colspan="6"></td>
      <td class="tg-yw4l" colspan="2">PAGE : 1 OF 1</td>
      <td class="tg-yw4l" colspan="4">AMOUNT PAID : $ '.$claimBalance.'</td>
    </tr>
    <tr>
      <td class="tg-yw4l" colspan="3" style="background-color:#ccc">Last Payment Amount : </td>
      <td class="tg-yw4l" colspan="2"> $ '.$claimBalance.'</td>
      <td class="tg-yw4l" colspan="4"></td>
      <td class="tg-yw4l" colspan="3">'.$billingName.'</td>
    </tr>
    <tr>
      <td class="tg-yw4l" colspan="3" style="background-color:#ccc">Last Payment Date : </td>
      <td class="tg-yw4l" colspan="2">12/12/2012</td>
      <td class="tg-yw4l" colspan="4"></td>
      <td class="tg-yw4l" colspan="3">'.$billingAddr.'</td>
    </tr>
    <tr>
      <td class="tg-yw4l" colspan="9"></td>
      <td class="tg-yw4l" colspan="3">'.$billingTotAddr.'</td>
    </tr>
    <tr>
      <td class="tg-yw4l" colspan="12"></td>
    </tr>
    <tr>
      <td class="tg-yw4l" colspan="12">PLEASE DETACH AND RETURN TOP PORTION WITH YOUR PATIENT STATEMENT</td>
    </tr>
    <tr>
      <td class="tg-yw4l" style="background-color:#ccc">DATE</td>
      <td class="tg-yw4l" colspan="2" style="background-color:#ccc">PATIENT NAME</td>
      <td class="tg-yw4l" colspan="3" style="background-color:#ccc">DESCRIPTION</td>
      <td class="tg-yw4l" style="background-color:#ccc">CHARGES</td>
      <td class="tg-yw4l" style="background-color:#ccc">PATIENT RESP.</td>
      <td class="tg-yw4l" style="background-color:#ccc">INSURANCE RECEIPTS</td>
      <td class="tg-yw4l" style="background-color:#ccc">PATIENT RECEIPTS</td>
      <td class="tg-yw4l" style="background-color:#ccc">ADJUSTMENT</td>
      <td class="tg-yw4l" style="background-color:#ccc">PATIENT BALANCE</td>
    </tr>
    <tr>
      <td class="tg-yw4l">'.$fromDt.'</td>
      <td class="tg-yw4l" colspan="2">'.$fullName.'</td>
      <td class="tg-yw4l" colspan="3"></td>
      <td class="tg-yw4l">'.$total.'</td>
      <td class="tg-yw4l">'.$claimBalance.'</td>
      <td class="tg-yw4l">'.$paid.'</td>
      <td class="tg-yw4l">'.$claimBalance.'</td>
      <td class="tg-yw4l">'.$adjustment.'</td>
      <td class="tg-yw4l">'.$claimBalance.'</td>
    </tr>
  </table>';
    
  $tbl = $temp;

  $pdf->writeHTML($tbl, true, false, false, false, '');


  $pdf->Output($_SERVER['DOCUMENT_ROOT'].'formingfriendships/Documents/Statements/'.$chartNo.'_'.$fromDt.'.pdf','FI');

  $statementName = $chartNo.'_'.$fromDt;
  $location = 'formingfriendships/Documents/Statements/'.$chartNo.'_'.$fromDt;

  mysql_query("INSERT INTO m_statements(statementName, patientID, location, createdOn) VALUES ('$statementName','$patientID','$location',now())");

  }
  else{
    echo "no";
  }
}
else{
  $result = mysql_query("SELECT m_serviceLocations.billingName, m_serviceLocations.address, m_patients.fullName,m_patients.patientID, m_patients.addrStreet1, m_patients.addrCity, m_patients.addrState, m_patients.addrZip, m_patients.chartNo, m_claims.fromDt, m_claims.total, m_claims.claimBalance,m_claims.paid, m_claims.adjustment, m_claims.claimBalance FROM m_claims inner join m_cases on m_cases.caseID = m_claims.caseID inner join m_patients on m_patients.patientID = m_cases.patientID inner join m_serviceLocations on m_serviceLocations.serviceLocID = m_patients.defaultServiceLoc where m_claims.insuranceID = '-100' AND m_claims.fromDt between '$filterFrom' AND '$filterTo'");
  if(mysql_num_rows($result) > 0){
    while($row = mysql_fetch_array($result)){
      $billingName = $row['billingName'];
      $address = explode(" ",$row['address']);
      $billingAddr = $address[0]; // piece1
      $billingTotAddr = $address[1];
      $addrStreet1 = $row['addrStreet1'];
      $patientID = $row['patientID'];
      $totAddr = $row['addrCity'].' '.$row['addrState'].' '.$row['addrZip'];
      $chartNo = $row['chartNo'];

      $fromDt = $row['fromDt'];
      $fullName = $row['fullName'];
      $total = $row['total'];
      $claimBalance = $row['claimBalance'];
      $paid = $row['paid'];
      $adjustment = $row['adjustment'];

      require_once('SearchPT/tcpdf_include.php');

  //create new PDF document
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    //set document information
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('Nicola Asuni');
    $pdf->SetTitle('INVOICE # :'.$fullName);
    $pdf->SetSubject('TCPDF Tutorial');
    $pdf->SetKeywords('TCPDF, PDF, example, test, guide');

    //set default header data

    //set header and footer fonts
    $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

    //set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

    //set margins
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

    //set auto page breaks
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

    //set image scale factor
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

    //set some language-dependent strings (optional)
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
        require_once(dirname(__FILE__).'/lang/eng.php');
        $pdf->setLanguageArray($l);
    }


    //add a page
    $pdf->AddPage('L', 'A4');

    $pdf->SetFont('helvetica', '', 2);

    $CurrPage = $pdf->getAliasNumPage();

    $temp = '';
    $temp .='<style type="text/css">
    .tg  {border-collapse:collapse;border-spacing:0; border-left:1px solid #000;border-right:1px solid #000;}
    .tg td{font-family:Arial, sans-serif;font-size:10px;border-left:1px solid #000;border-right:1px solid #000;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
    .tg th{font-family:Arial, sans-serif;font-size:10px;border-left:1px solid #000;border-right:1px solid #000;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
    .tg .tg-baqh{text-align:center;vertical-align:top;border-left:1px solid #000;border-right:1px solid #000;}
    .tg .tg-yw4l{vertical-align:top;border-bottom:1px solid #000;}
    </style>
    <table class="tg">
      <tr>
        <th class="tg-yw4l" colspan="5">'.$billingName.'</th>
        <th class="tg-yw4l" colspan="7"></th>
      </tr>
      <tr>
        <td class="tg-yw4l" colspan="5">'.$billingAddr.'</td>
        <td class="tg-baqh" colspan="7">IF PAYING BY CREDIT / DEBIT CARD, FILL OUT BELOW</td>
      </tr>
      <tr>
        <td class="tg-yw4l" colspan="5">'.$billingTotAddr.'</td>
        <td class="tg-baqh" colspan="7">CHECK CARD USING FOR PAYMENT</td>
      </tr>
      <tr>
        <td class="tg-yw4l" colspan="5" rowspan="3"></td>
        <td class="tg-baqh" style="background-color:#ccc"><input type="checkbox" name="box" value="1" readonly="true" /> MASTER CARD</td>
        <td class="tg-baqh" style="background-color:#ccc"><input type="checkbox" name="box" value="2" readonly="true" /> DISCOVER</td>
        <td class="tg-baqh" style="background-color:#ccc"><input type="checkbox" name="box" value="3" readonly="true" /> VISA</td>
        <td class="tg-baqh" style="background-color:#ccc"><input type="checkbox" name="box" value="4" readonly="true" /> AMERICAN EXPRESS</td>
        <td class="tg-baqh" style="background-color:#ccc"><input type="checkbox" name="box" value="5" readonly="true" /> DINERS</td>
        <td class="tg-baqh" colspan="2" style="background-color:#ccc"><input type="checkbox" name="box" value="6" readonly="true" /> DEBIT CARD</td>
      </tr>
      <tr>
        <td class="tg-baqh" style="background-color:#ccc">CARD NUMBER : </td>
        <td class="tg-baqh" colspan="3"></td>
        <td class="tg-baqh" style="background-color:#ccc">SECURITY CODE : </td>
        <td class="tg-baqh" colspan="2"></td>
      </tr>
      <tr>
        <td class="tg-baqh" style="background-color:#ccc">SIGNATURE :</td>
        <td class="tg-baqh" colspan="3"></td>
        <td class="tg-baqh" style="background-color:#ccc">EXP. DATE :</td>
        <td class="tg-baqh" colspan="2"></td>
      </tr>
      <tr>
        <td class="tg-yw4l" colspan="6">'.$fullName.'</td>
        <td class="tg-yw4l" colspan="6"></td>
      </tr>
      <tr>
        <td class="tg-yw4l" colspan="6">'.$addrStreet1.'</td>
        <td class="tg-yw4l" colspan="2" style="background-color:#ccc">STATEMENT DATE</td>
        <td class="tg-yw4l" colspan="2" style="background-color:#ccc">PAY THIS AMOUNT</td>
        <td class="tg-yw4l" colspan="2" style="background-color:#ccc">ACCT #</td>
      </tr>
      <tr>
        <td class="tg-yw4l" colspan="6">'.$totAddr.'</td>
        <td class="tg-yw4l" colspan="2">'.$fromDt.'</td>
        <td class="tg-yw4l" colspan="2"> $ '.$claimBalance.'</td>
        <td class="tg-yw4l" colspan="2">'.$chartNo.'</td>
      </tr>
      <tr>
        <td class="tg-yw4l" colspan="6"></td>
        <td class="tg-yw4l" colspan="2">PAGE : 1 OF 1</td>
        <td class="tg-yw4l" colspan="4">AMOUNT PAID : $ '.$claimBalance.'</td>
      </tr>
      <tr>
        <td class="tg-yw4l" colspan="3" style="background-color:#ccc">Last Payment Amount : </td>
        <td class="tg-yw4l" colspan="2"> $ '.$claimBalance.'</td>
        <td class="tg-yw4l" colspan="4"></td>
        <td class="tg-yw4l" colspan="3">'.$billingName.'</td>
      </tr>
      <tr>
        <td class="tg-yw4l" colspan="3" style="background-color:#ccc">Last Payment Date : </td>
        <td class="tg-yw4l" colspan="2">12/12/2012</td>
        <td class="tg-yw4l" colspan="4"></td>
        <td class="tg-yw4l" colspan="3">'.$billingAddr.'</td>
      </tr>
      <tr>
        <td class="tg-yw4l" colspan="9"></td>
        <td class="tg-yw4l" colspan="3">'.$billingTotAddr.'</td>
      </tr>
      <tr>
        <td class="tg-yw4l" colspan="12"></td>
      </tr>
      <tr>
        <td class="tg-yw4l" colspan="12">PLEASE DETACH AND RETURN TOP PORTION WITH YOUR PATIENT STATEMENT</td>
      </tr>
      <tr>
        <td class="tg-yw4l" style="background-color:#ccc">DATE</td>
        <td class="tg-yw4l" colspan="2" style="background-color:#ccc">PATIENT NAME</td>
        <td class="tg-yw4l" colspan="3" style="background-color:#ccc">DESCRIPTION</td>
        <td class="tg-yw4l" style="background-color:#ccc">CHARGES</td>
        <td class="tg-yw4l" style="background-color:#ccc">PATIENT RESP.</td>
        <td class="tg-yw4l" style="background-color:#ccc">INSURANCE RECEIPTS</td>
        <td class="tg-yw4l" style="background-color:#ccc">PATIENT RECEIPTS</td>
        <td class="tg-yw4l" style="background-color:#ccc">ADJUSTMENT</td>
        <td class="tg-yw4l" style="background-color:#ccc">PATIENT BALANCE</td>
      </tr>
      <tr>
        <td class="tg-yw4l">'.$fromDt.'</td>
        <td class="tg-yw4l" colspan="2">'.$fullName.'</td>
        <td class="tg-yw4l" colspan="3"></td>
        <td class="tg-yw4l">'.$total.'</td>
        <td class="tg-yw4l">'.$claimBalance.'</td>
        <td class="tg-yw4l">'.$paid.'</td>
        <td class="tg-yw4l">'.$claimBalance.'</td>
        <td class="tg-yw4l">'.$adjustment.'</td>
        <td class="tg-yw4l">'.$claimBalance.'</td>
      </tr>
    </table>';
      
    $tbl = $temp;
    $pdf->writeHTML($tbl, true, false, false, false, '');
    $pdf->Output($_SERVER['DOCUMENT_ROOT'].'formingfriendships//Documents/Statements/'.$chartNo.'_'.$fromDt.'.pdf','FI');
    }


  $statementName = $chartNo.'_'.$fromDt;
  $location = 'formingfriendships//Documents/Statements/'.$chartNo.'_'.$fromDt;

  mysql_query("INSERT INTO m_statements(statementName, patientID, location, createdOn) VALUES ('$statementName','$patientID','$location',now())");

  }
  else{
    echo "no";
  }
}
  

?>