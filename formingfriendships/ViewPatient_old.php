<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0" />
	<title>Find Patient | AMROMED LLC</title>

	<!--=== CSS ===-->

	<!-- Bootstrap -->
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

	<!-- jQuery UI -->
	<!--<link href="plugins/jquery-ui/jquery-ui-1.10.2.custom.css" rel="stylesheet" type="text/css" />-->
	<!--[if lt IE 9]>
		<link rel="stylesheet" type="text/css" href="plugins/jquery-ui/jquery.ui.1.10.2.ie.css"/>
	<![endif]-->

	<!-- Theme -->
	<link href="assets/css/main.css" rel="stylesheet" type="text/css" />
	<link href="assets/css/plugins.css" rel="stylesheet" type="text/css" />
	<link href="assets/css/responsive.css" rel="stylesheet" type="text/css" />
	<link href="assets/css/icons.css" rel="stylesheet" type="text/css" />

	<link rel="stylesheet" href="assets/css/fontawesome/font-awesome.min.css">
	<!--[if IE 7]>
		<link rel="stylesheet" href="assets/css/fontawesome/font-awesome-ie7.min.css">
	<![endif]-->

	<!--[if IE 8]>
		<link href="assets/css/ie8.css" rel="stylesheet" type="text/css" />
	<![endif]-->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>

	<!--=== JavaScript ===-->

	<script type="text/javascript" src="assets/js/libs/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" src="plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>

	<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="assets/js/libs/lodash.compat.min.js"></script>

	<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
		<script src="assets/js/libs/html5shiv.js"></script>
	<![endif]-->

	<!-- Smartphone Touch Events -->
	<script type="text/javascript" src="plugins/touchpunch/jquery.ui.touch-punch.min.js"></script>
	<script type="text/javascript" src="plugins/event.swipe/jquery.event.move.js"></script>
	<script type="text/javascript" src="plugins/event.swipe/jquery.event.swipe.js"></script>

	<!-- General -->
	<script type="text/javascript" src="assets/js/libs/breakpoints.js"></script>
	<script type="text/javascript" src="plugins/respond/respond.min.js"></script> <!-- Polyfill for min/max-width CSS3 Media Queries (only for IE8) -->
	<script type="text/javascript" src="plugins/cookie/jquery.cookie.min.js"></script>
	<script type="text/javascript" src="plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script type="text/javascript" src="plugins/slimscroll/jquery.slimscroll.horizontal.min.js"></script>

	<!-- Page specific plugins -->
	<!-- Charts -->
	<script type="text/javascript" src="plugins/sparkline/jquery.sparkline.min.js"></script>

	<script type="text/javascript" src="plugins/daterangepicker/moment.min.js"></script>
	<script type="text/javascript" src="plugins/daterangepicker/daterangepicker.js"></script>
	<script type="text/javascript" src="plugins/blockui/jquery.blockUI.min.js"></script>

	<!-- Pickers -->
	<script type="text/javascript" src="plugins/pickadate/picker.js"></script>
	<script type="text/javascript" src="plugins/pickadate/picker.date.js"></script>
	<script type="text/javascript" src="plugins/pickadate/picker.time.js"></script>
	<script type="text/javascript" src="plugins/bootstrap-colorpicker/bootstrap-colorpicker.min.js"></script>

	<!-- Noty -->
	<script type="text/javascript" src="plugins/noty/jquery.noty.js"></script>
	<script type="text/javascript" src="plugins/noty/layouts/top.js"></script>
	<script type="text/javascript" src="plugins/noty/themes/default.js"></script>

	<!-- Slim Progress Bars -->
	<script type="text/javascript" src="plugins/nprogress/nprogress.js"></script>

	<!-- Bootbox -->
	<script type="text/javascript" src="plugins/bootbox/bootbox.min.js"></script>

	<!-- App -->
	<script type="text/javascript" src="assets/js/app.js"></script>
	<script type="text/javascript" src="assets/js/plugins.js"></script>
	<script type="text/javascript" src="assets/js/plugins.form-components.js"></script>

	<script>
	$(document).ready(function(){
		"use strict";

		App.init(); // Init layout and core plugins
		Plugins.init(); // Init all plugins
		FormComponents.init(); // Init all form-specific plugins
	});
	</script>

	<!-- Demo JS -->
	<script type="text/javascript" src="assets/js/custom.js"></script>
	<script type="text/javascript" src="assets/js/demo/ui_general.js"></script>
</head>

<body>

	<!-- Header -->
	<header class="header navbar navbar-fixed-top" role="banner">
		<!-- Top Navigation Bar -->
		<div class="container">

			<!-- Only visible on smartphones, menu toggle -->
			<ul class="nav navbar-nav">
				<li class="nav-toggle"><a href="javascript:void(0);" title=""><i class="icon-reorder"></i></a></li>
			</ul>

			<!-- Logo -->
			<a class="navbar-brand" href="index.html">
				
				<strong>AKRON MED BILLING</strong>
			</a>
			<!-- /logo -->

			<!-- Sidebar Toggler -->
			<a href="#" class="toggle-sidebar bs-tooltip" data-placement="bottom" data-original-title="Toggle navigation">
				<i class="icon-reorder"></i>
			</a>
			<!-- /Sidebar Toggler -->

			<!-- Top Left Menu -->
			<ul class="nav navbar-nav navbar-left hidden-xs hidden-sm">
				<li>
					<a href="#">
						Dashboard
					</a>
				</li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						Dropdown
						<i class="icon-caret-down small"></i>
					</a>
					<ul class="dropdown-menu">
						<li><a href="#"><i class="icon-user"></i> Example #1</a></li>
						<li><a href="#"><i class="icon-calendar"></i> Example #2</a></li>
						<li class="divider"></li>
						<li><a href="#"><i class="icon-tasks"></i> Example #3</a></li>
					</ul>
				</li>
			</ul>
			<!-- /Top Left Menu -->

			<!-- Top Right Menu -->
			<ul class="nav navbar-nav navbar-right">
				<!-- Notifications -->
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="icon-warning-sign"></i>
						<span class="badge">5</span>
					</a>
					<ul class="dropdown-menu extended notification">
						<li class="title">
							<p>You have 5 new notifications</p>
						</li>
						<li>
							<a href="javascript:void(0);">
								<span class="label label-success"><i class="icon-plus"></i></span>
								<span class="message">New user registration.</span>
								<span class="time">1 mins</span>
							</a>
						</li>
						<li>
							<a href="javascript:void(0);">
								<span class="label label-danger"><i class="icon-warning-sign"></i></span>
								<span class="message">High CPU load on cluster #2.</span>
								<span class="time">5 mins</span>
							</a>
						</li>
						<li>
							<a href="javascript:void(0);">
								<span class="label label-success"><i class="icon-plus"></i></span>
								<span class="message">New user registration.</span>
								<span class="time">10 mins</span>
							</a>
						</li>
						<li>
							<a href="javascript:void(0);">
								<span class="label label-info"><i class="icon-bullhorn"></i></span>
								<span class="message">New items are in queue.</span>
								<span class="time">25 mins</span>
							</a>
						</li>
						<li>
							<a href="javascript:void(0);">
								<span class="label label-warning"><i class="icon-bolt"></i></span>
								<span class="message">Disk space to 85% full.</span>
								<span class="time">55 mins</span>
							</a>
						</li>
						<li class="footer">
							<a href="javascript:void(0);">View all notifications</a>
						</li>
					</ul>
				</li>

				<!-- Tasks -->
				<li class="dropdown hidden-xs hidden-sm">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="icon-tasks"></i>
						<span class="badge">7</span>
					</a>
					<ul class="dropdown-menu extended notification">
						<li class="title">
							<p>You have 7 pending tasks</p>
						</li>
						<li>
							<a href="javascript:void(0);">
								<span class="task">
									<span class="desc">Preparing new release</span>
									<span class="percent">30%</span>
								</span>
								<div class="progress progress-small">
									<div style="width: 30%;" class="progress-bar progress-bar-info"></div>
								</div>
							</a>
						</li>
						<li>
							<a href="javascript:void(0);">
								<span class="task">
									<span class="desc">Change management</span>
									<span class="percent">80%</span>
								</span>
								<div class="progress progress-small progress-striped active">
									<div style="width: 80%;" class="progress-bar progress-bar-danger"></div>
								</div>
							</a>
						</li>
						<li>
							<a href="javascript:void(0);">
								<span class="task">
									<span class="desc">Mobile development</span>
									<span class="percent">60%</span>
								</span>
								<div class="progress progress-small">
									<div style="width: 60%;" class="progress-bar progress-bar-success"></div>
								</div>
							</a>
						</li>
						<li>
							<a href="javascript:void(0);">
								<span class="task">
									<span class="desc">Database migration</span>
									<span class="percent">20%</span>
								</span>
								<div class="progress progress-small">
									<div style="width: 20%;" class="progress-bar progress-bar-warning"></div>
								</div>
							</a>
						</li>
						<li class="footer">
							<a href="javascript:void(0);">View all tasks</a>
						</li>
					</ul>
				</li>

				<!-- Messages -->
				<li class="dropdown hidden-xs hidden-sm">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="icon-envelope"></i>
						<span class="badge">1</span>
					</a>
					<ul class="dropdown-menu extended notification">
						<li class="title">
							<p>You have 3 new messages</p>
						</li>
						<li>
							<a href="javascript:void(0);">
								<span class="photo"><img src="assets/img/demo/avatar-1.jpg" alt="" /></span>
								<span class="subject">
									<span class="from">Bob Carter</span>
									<span class="time">Just Now</span>
								</span>
								<span class="text">
									Consetetur sadipscing elitr...
								</span>
							</a>
						</li>
						<li>
							<a href="javascript:void(0);">
								<span class="photo"><img src="assets/img/demo/avatar-2.jpg" alt="" /></span>
								<span class="subject">
									<span class="from">Jane Doe</span>
									<span class="time">45 mins</span>
								</span>
								<span class="text">
									Sed diam nonumy...
								</span>
							</a>
						</li>
						<li>
							<a href="javascript:void(0);">
								<span class="photo"><img src="assets/img/demo/avatar-3.jpg" alt="" /></span>
								<span class="subject">
									<span class="from">Patrick Nilson</span>
									<span class="time">6 hours</span>
								</span>
								<span class="text">
									No sea takimata sanctus...
								</span>
							</a>
						</li>
						<li class="footer">
							<a href="javascript:void(0);">View all messages</a>
						</li>
					</ul>
				</li>

				<!-- .row .row-bg Toggler -->
				<li>
					<a href="#" class="dropdown-toggle row-bg-toggle">
						<i class="icon-resize-vertical"></i>
					</a>
				</li>


				<!-- User Login Dropdown -->
				<li class="dropdown user">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<!--<img alt="" src="assets/img/avatar1_small.jpg" />-->
						<i class="icon-male"></i>
						<span class="username">Peter Browne</span>
						<i class="icon-caret-down small"></i>
					</a>
					<ul class="dropdown-menu">
						<li><a href="pages_user_profile.html"><i class="icon-user"></i> My Profile</a></li>
						<li><a href="pages_calendar.html"><i class="icon-calendar"></i> My Calendar</a></li>
						<li><a href="#"><i class="icon-tasks"></i> My Tasks</a></li>
						<li class="divider"></li>
						<li><a href="login.html"><i class="icon-key"></i> Log Out</a></li>
					</ul>
				</li>
				<!-- /user login dropdown -->
			</ul>
			<!-- /Top Right Menu -->
		</div>
		<!-- /top navigation bar -->

	</header> <!-- /.header -->

	<div id="container">
		<div id="sidebar" class="sidebar-fixed">
			<div id="sidebar-content">

				<!-- Search Input -->
				<form class="sidebar-search">
					<div class="input-box">
						<button type="submit" class="submit">
							<i class="icon-search"></i>
						</button>
						<span>
							<input type="text" placeholder="Search...">
						</span>
					</div>
				</form>

				<!-- Search Results -->
				<div class="sidebar-search-results">

					<i class="icon-remove close"></i>
					<!-- Documents -->
					<div class="title">
						Documents
					</div>
					<ul class="notifications">
						<li>
							<a href="javascript:void(0);">
								<div class="col-left">
									<span class="label label-info"><i class="icon-file-text"></i></span>
								</div>
								<div class="col-right with-margin">
									<span class="message"><strong>Peter Browne</strong> received $1.527,32</span>
									<span class="time">finances.xls</span>
								</div>
							</a>
						</li>
						<li>
							<a href="javascript:void(0);">
								<div class="col-left">
									<span class="label label-success"><i class="icon-file-text"></i></span>
								</div>
								<div class="col-right with-margin">
									<span class="message">My name is <strong>Peter Browne</strong> ...</span>
									<span class="time">briefing.docx</span>
								</div>
							</a>
						</li>
					</ul>
					<!-- /Documents -->
					<!-- Persons -->
					<div class="title">
						Persons
					</div>
					<ul class="notifications">
						<li>
							<a href="javascript:void(0);">
								<div class="col-left">
									<span class="label label-danger"><i class="icon-female"></i></span>
								</div>
								<div class="col-right with-margin">
									<span class="message">Peter <strong>Browne</strong></span>
									<span class="time">21 years old</span>
								</div>
							</a>
						</li>
					</ul>
				</div> <!-- /.sidebar-search-results -->

				<!--=== Navigation ===-->
				<ul id="nav">
					<li class="current">
						<a href="index.html">
							<i class="icon-dashboard"></i>
							Dashboard
						</a>
					</li>
					<li>
						<a href="javascript:void(0);">
							<i class="icon-desktop"></i>
							Appointment
							<span class="label label-info pull-right">6</span>
						</a>
					</li>
					<li>
						<a href="javascript:void(0);">
							<i class="icon-edit"></i>
							Patients
						</a>
						<ul class="sub-menu">
							<li>
								<a href="AddPatient.html">
								<i class="icon-angle-right"></i>
								Add Patients
								</a>
							</li>
							<li>
								<a href="ViewPatient.php">
								<i class="icon-angle-right"></i>
								Find Patients
								</a>
							</li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0);">
							<i class="icon-table"></i>
							Claims
						</a>
						<ul class="sub-menu">
							<li>
								<a href="tables_static.html">
								<i class="icon-angle-right"></i>
								Static Tables
								</a>
							</li>
							<li>
								<a href="tables_dynamic.html">
								<i class="icon-angle-right"></i>
								Dynamic Tables (DataTables)
								</a>
							</li>
							<li>
								<a href="tables_responsive.html">
								<i class="icon-angle-right"></i>
								Responsive Tables
								</a>
							</li>
						</ul>
					</li>
					<li>
						<a href="charts.html">
							<i class="icon-bar-chart"></i>
							Financial
						</a>
					</li>
					<li>
						<a href="charts.html">
							<i class="icon-bar-chart"></i>
							Reports
						</a>
					</li>
					<li>
						<a href="charts.html">
							<i class="icon-bar-chart"></i>
							Settings
						</a>
					</li>
					<li>
						<a href="javascript:void(0);">
							<i class="icon-folder-open-alt"></i>
							Help
						</a>
						
					</li>
				</ul>
				
				<!-- /Navigation -->
				<div class="sidebar-title">
					<span>Notifications</span>
				</div>
				<ul class="notifications demo-slide-in"> <!-- .demo-slide-in is just for demonstration purposes. You can remove this. -->
					
					<li style="display: none;"> <!-- style-attr is here only for fading in this notification after a specific time. Remove this. -->
						<div class="col-left">
							<span class="label label-info"><i class="icon-envelope"></i></span>
						</div>
						<div class="col-right with-margin">
							<span class="message"><strong>Vivek</strong> sent you a message</span>
							<span class="time">few second ago</span>
						</div>
					</li>
					<li>
						<div class="col-left">
							<span class="label label-success"><i class="icon-plus"></i></span>
						</div>
						<div class="col-right with-margin">
							<span class="message"><strong>Peter Browne</strong>'s account was created</span>
							<span class="time">4 hours ago</span>
						</div>
					</li>
				</ul>

				<div class="sidebar-widget align-center">
					<div class="btn-group" data-toggle="buttons" id="theme-switcher">
						<label class="btn active">
							<input type="radio" name="theme-switcher" data-theme="bright"><i class="icon-sun"></i> Bright
						</label>
						<label class="btn">
							<input type="radio" name="theme-switcher" data-theme="dark"><i class="icon-moon"></i> Dark
						</label>
					</div>
				</div>

			</div>
			<div id="divider" class="resizeable"></div>
		</div>
		<!-- /Sidebar -->

		<div id="content">
			<div class="container">
				<!-- Breadcrumbs line -->
				<div class="crumbs">
					<ul id="breadcrumbs" class="breadcrumb">
						<li class="current">
							<i class="icon-home"></i>
							<a href="index.html">Dashboard</a>
						</li>
						
					</ul>

				</div>
				<!-- /Breadcrumbs line -->


				<!--=== Page Content ===-->
				<!--=== Modals ===-->
				<div class="row">
					<h2 style="color: #251367; margin-left:20px;">Find Patient</h2>
					<div class="col-md-12" style="margin-top:20px;">
			        <div class="widget box" >
			            <div class="widget-content">
			            	<form action="EditPatient.php" method="post">
		                    <table id="test" class="display" cellspacing="0" width="100%">
							</table>
							</form>
			            </div>
			        </div>

				</div>

				</div>
				<!-- /Page Content -->
			</div>
			<!-- /.container -->

		</div>
	</div>
	<link href="https://cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css" rel='stylesheet' type='text/css'>
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
	<script>
	var data1 = [{
	"patientID": "1",
	"fullName": "Emmanuel",
	"dob": "0000-00-00",
	"gender": "M",
	"relationship": "",
	"ssn": "",
	"medicalRecord": "0",
	"employment": "",
	"employer": "",
	"reference": "",
	"isActive": "1",
	"isDeleted": "0",
	"addrStreet1": "",
	"addrStreet2": "",
	"addrCity": "",
	"addrState": "",
	"addrCountry": "",
	"addrZip": "",
	"email": "email@domain.com",
	"phoneHome": "",
	"phoneWork": "",
	"phoneMobile": "",
	"notifyEmail": "0",
	"notifyPhone": "0",
	"primaryCarePhy": "0",
	"referringPhy": "0",
	"defaultPhy": "0",
	"defaultServiceLoc": "",
	"chartNo": "",
	"emergencyContact": "",
	"emergencyPhone": "",
	"createdBy": "0",
	"lastEditedBy": "0",
	"conduct": "",
	"practiceID": "0",
	"updated_at": "2016-01-23 10:19:32",
	"created_at": "2016-01-23 10:19:32"
}, {
	"patientID": "2",
	"fullName": "Ashish",
	"dob": "0000-00-00",
	"gender": "M",
	"relationship": "",
	"ssn": "",
	"medicalRecord": "0",
	"employment": "",
	"employer": "",
	"reference": "",
	"isActive": "1",
	"isDeleted": "0",
	"addrStreet1": "",
	"addrStreet2": "",
	"addrCity": "",
	"addrState": "",
	"addrCountry": "",
	"addrZip": "",
	"email": "abc@akron.in",
	"phoneHome": "",
	"phoneWork": "",
	"phoneMobile": "",
	"notifyEmail": "0",
	"notifyPhone": "0",
	"primaryCarePhy": "0",
	"referringPhy": "0",
	"defaultPhy": "0",
	"defaultServiceLoc": "",
	"chartNo": "",
	"emergencyContact": "",
	"emergencyPhone": "",
	"createdBy": "0",
	"lastEditedBy": "0",
	"conduct": "",
	"practiceID": "0",
	"updated_at": "2016-01-23 10:23:41",
	"created_at": "2016-01-23 10:23:41"
}, {
	"patientID": "3",
	"fullName": "Ananth",
	"dob": "0000-00-00",
	"gender": "M",
	"relationship": "",
	"ssn": "",
	"medicalRecord": "0",
	"employment": "",
	"employer": "",
	"reference": "",
	"isActive": "1",
	"isDeleted": "0",
	"addrStreet1": "",
	"addrStreet2": "",
	"addrCity": "",
	"addrState": "",
	"addrCountry": "",
	"addrZip": "",
	"email": "",
	"phoneHome": "",
	"phoneWork": "",
	"phoneMobile": "",
	"notifyEmail": "0",
	"notifyPhone": "0",
	"primaryCarePhy": "0",
	"referringPhy": "0",
	"defaultPhy": "0",
	"defaultServiceLoc": "",
	"chartNo": "",
	"emergencyContact": "",
	"emergencyPhone": "",
	"createdBy": "0",
	"lastEditedBy": "0",
	"conduct": "",
	"practiceID": "0",
	"updated_at": "2016-01-23 15:38:31",
	"created_at": "2016-01-23 15:38:31"
}, {
	"patientID": "4",
	"fullName": "Niyaz",
	"dob": "0000-00-00",
	"gender": "F",
	"relationship": "",
	"ssn": "",
	"medicalRecord": "0",
	"employment": "",
	"employer": "",
	"reference": "",
	"isActive": "1",
	"isDeleted": "0",
	"addrStreet1": "",
	"addrStreet2": "",
	"addrCity": "",
	"addrState": "",
	"addrCountry": "",
	"addrZip": "",
	"email": "",
	"phoneHome": "",
	"phoneWork": "",
	"phoneMobile": "",
	"notifyEmail": "0",
	"notifyPhone": "0",
	"primaryCarePhy": "0",
	"referringPhy": "0",
	"defaultPhy": "0",
	"defaultServiceLoc": "",
	"chartNo": "",
	"emergencyContact": "",
	"emergencyPhone": "",
	"createdBy": "0",
	"lastEditedBy": "0",
	"conduct": "",
	"practiceID": "0",
	"updated_at": "2016-01-23 18:16:57",
	"created_at": "2016-01-23 18:16:57"
}];
	$(document).ready(function(){
		//  console.log(data1.fullName);
		// var dt = [];
		// $.each(data1,function(i,v) {
		// 	dt.push([data1[i].fullName,data1[i].dob,data1[i].gender,data1[i].relationship,data1[i].ssn,data1[i].medicalRecord]);
		// });
		$('#test').DataTable({
        "serverSide": true,
        "ajax": "http://www.akronbilling.com/CareService/patients",
        columns: [
            {"title": "fullName"},
            {"title": "dob"},
            {"title": "gender"},
            {"title": "relationship"},
            {"title": "ssn"},
            {"title": "medicalRecord"}
        ]
        });

		// var table = $('#test').DataTable({
  //   	"columns":[
  //       	{
  //           	"title":"FirstName"
  //           },
  //           {
  //           	"title":"LastName"
  //           },
  //           {
  //           	"title":"Job"
  //           },
  //           {
  //           	"title":"Place"
  //           },
  //           {
  //           	"title":"DOJ"
  //           },
  //           {
  //           	"title":"Salary"
  //           }
  //       ]
  //   });
		// $.each(data1.data, function(k, v){
  //   	console.log(data1.data[0][0]);
  //   	// table.row.add([
  //    //    	data1.data[0][0],
  //    //        data1.data[0][1],
  //    //        data1.data[0][2],
  //    //        data1.data[0][3],
  //    //        data1.data[0][4],
  //    //        data1.data[0][5],
  //    //    ]).draw();
    
  //   })
		var table = $('#test').DataTable();
	    $('#test tbody').on('click', 'tr', function () {
	    	$('#MedCoding').modal('show');
	         var data = table.row( this ).data();
	         var id = data[0];           
	         window.location.href='EditPatient.php?id='+id;
	    } );
	});
	</script>
</body>
</html>