<?php
 session_start();

 if (!isset($_SESSION['userID'])) {
    header("Location: login.php");
 }
 ?>
 <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <title>Add Patient | AMROMED LLC</title>

    <!--=== CSS ===-->

    <!-- Bootstrap -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

    <!-- jQuery UI -->
    <!--<link href="plugins/jquery-ui/jquery-ui-1.10.2.custom.css" rel="stylesheet" type="text/css" />-->
    <!--[if lt IE 9]>
        <link rel="stylesheet" type="text/css" href="plugins/jquery-ui/jquery.ui.1.10.2.ie.css"/>
    <![endif]-->

    <!-- Theme -->
    <link href="assets/css/main.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/plugins.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/alertify.css" rel='stylesheet' type='text/css'>
    <link href="assets/css/themes/default.css" rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="assets/css/fontawesome/font-awesome.min.css">
    <!--[if IE 7]>
        <link rel="stylesheet" href="assets/css/fontawesome/font-awesome-ie7.min.css">
    <![endif]-->

    <!--[if IE 8]>
        <link href="assets/css/ie8.css" rel="stylesheet" type="text/css" />
    <![endif]-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>

    <!--=== JavaScript ===-->

    <script type="text/javascript" src="assets/js/libs/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>
    <script type="text/javascript" src="plugins/bootstrap-switch/bootstrap-switch.min.js"></script>

    <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/js/libs/lodash.compat.min.js"></script>

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
        <script src="assets/js/libs/html5shiv.js"></script>
    <![endif]-->

    <!-- Smartphone Touch Events -->
    <script type="text/javascript" src="plugins/touchpunch/jquery.ui.touch-punch.min.js"></script>
    <script type="text/javascript" src="plugins/event.swipe/jquery.event.move.js"></script>
    <script type="text/javascript" src="plugins/event.swipe/jquery.event.swipe.js"></script>

    <!-- General -->
    <script type="text/javascript" src="assets/js/libs/breakpoints.js"></script>
    <script type="text/javascript" src="plugins/respond/respond.min.js"></script> <!-- Polyfill for min/max-width CSS3 Media Queries (only for IE8) -->
    <script type="text/javascript" src="plugins/cookie/jquery.cookie.min.js"></script>
    <script type="text/javascript" src="plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script type="text/javascript" src="plugins/slimscroll/jquery.slimscroll.horizontal.min.js"></script>

    <!-- Page specific plugins -->
    <!-- Charts -->
    <!--[if lt IE 9]>
        <script type="text/javascript" src="plugins/flot/excanvas.min.js"></script>
    <![endif]-->
    <script type="text/javascript" src="plugins/sparkline/jquery.sparkline.min.js"></script>
    <script type="text/javascript" src="plugins/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>

    <script type="text/javascript" src="plugins/daterangepicker/moment.min.js"></script>
    <script type="text/javascript" src="plugins/daterangepicker/daterangepicker.js"></script>
    <script type="text/javascript" src="plugins/blockui/jquery.blockUI.min.js"></script>

    <script type="text/javascript" src="plugins/fullcalendar/fullcalendar.min.js"></script>

    <!-- Noty -->
    <script type="text/javascript" src="plugins/noty/jquery.noty.js"></script>
    <script type="text/javascript" src="plugins/noty/layouts/top.js"></script>
    <script type="text/javascript" src="plugins/noty/themes/default.js"></script>

    <!-- Forms -->
    <script type="text/javascript" src="assets/js/jquery.mask.min.js"></script>
    <script type="text/javascript" src="plugins/uniform/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="plugins/select2/select2.min.js"></script>

    <!-- App -->
    <script type="text/javascript" src="assets/js/app.js"></script>
    <script type="text/javascript" src="assets/js/plugins.js"></script>
    <script type="text/javascript" src="assets/js/plugins.form-components.js"></script>
    <link rel="stylesheet" href="assets/css/select2.css">
    <script type="text/javascript" src="assets/js/select2.js"></script>

    <script type="text/javascript" src="assets/js/alertify.min.js"></script>
    <script src="assets/js/jquery.datetimepicker.js"></script>
    <link href="assets/css/jquery.datetimepicker.css" rel="stylesheet" />
    <link href="plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />

    <script>
    $(document).ready(function(){
        "use strict";
        App.init(); // Init layout and core plugins
        Plugins.init(); // Init all plugins
        FormComponents.init(); // Init all form-specific plugins
        document.getElementById('practiceName').innerHTML = sessionStorage.getItem('practiceName');

        $('#insuredDiv').hide();
        $('#ssn').mask('000-00-0000');
        $('#dob').mask('00-00-0000');

        document.getElementById('hdntoggleDX').value = "true";

        $('#icdtoggle').on('change', function(event, state) {
          document.getElementById('hdntoggleDX').value = event.target.checked; // jQuery event
        });

        $(document).on('focus','#defDiag2',function() {
            var toggleDX = document.getElementById('hdntoggleDX').value;
            $('#defDiag2').keypress(function() {
                var num = $(this).val().length;
                if(num > 1){
                    if(toggleDX != "false"){
                        var DXCds = "";
                        $.ajax({
                            type: "GET",
                            url:"dx9List.php",
                            async: false,
                            data:{},
                            success:function(result){
                                DXCds = JSON.parse(result);
                            }
                        });
                        $("#defDiag2").autocomplete({
                            minlength : 3,
                            source: DXCds,
                            autoFocus:true
                        });
                    }
                else{
                        var DXCds = "";
                        $.ajax({
                            type: "GET",
                            url:"dx10List.php",
                            async: false,
                            data:{},
                            success:function(result){
                                DXCds = JSON.parse(result);
                            }
                        });
                        $("#defDiag2").autocomplete({
                            minlength : 3,
                            source: DXCds,
                            autoFocus:true
                        });
                    }
                }
            });
        });

        $(document).on('focus','#defDiag3',function() {
            var toggleDX = document.getElementById('hdntoggleDX').value;
            $('#defDiag3').keypress(function() {
                var num = $(this).val().length;
                if(num > 1){
                    if(toggleDX != "false"){
                        var DXCds = "";
                        $.ajax({
                            type: "GET",
                            url:"dx9List.php",
                            async: false,
                            data:{},
                            success:function(result){
                                DXCds = JSON.parse(result);
                            }
                        });
                        $("#defDiag3").autocomplete({
                            minlength : 3,
                            source: DXCds,
                            autoFocus:true
                        });
                    }
                else{
                        var DXCds = "";
                        $.ajax({
                            type: "GET",
                            url:"dx10List.php",
                            async: false,
                            data:{},
                            success:function(result){
                                DXCds = JSON.parse(result);
                            }
                        });
                        $("#defDiag3").autocomplete({
                            minlength : 3,
                            source: DXCds,
                            autoFocus:true
                        });
                    }
                }
            });
        });

        $(document).on('focus','#defDiag4',function() {
            var toggleDX = document.getElementById('hdntoggleDX').value;
            $('#defDiag4').keypress(function() {
                var num = $(this).val().length;
                if(num > 1){
                    if(toggleDX != "false"){
                        var DXCds = "";
                        $.ajax({
                            type: "GET",
                            url:"dx9List.php",
                            async: false,
                            data:{},
                            success:function(result){
                                DXCds = JSON.parse(result);
                            }
                        });
                        $("#defDiag4").autocomplete({
                            minlength : 3,
                            source: DXCds,
                            autoFocus:true
                        });
                    }
                else{
                        var DXCds = "";
                        $.ajax({
                            type: "GET",
                            url:"dx10List.php",
                            async: false,
                            data:{},
                            success:function(result){
                                DXCds = JSON.parse(result);
                            }
                        });
                        $("#defDiag4").autocomplete({
                            minlength : 3,
                            source: DXCds,
                            autoFocus:true
                        });
                    }
                }
            });
        });


        $(document).on('focus','#principalDiag',function() {
            var toggleDX = document.getElementById('hdntoggleDX').value;
            $('#principalDiag').keypress(function() {
                var num = $(this).val().length;
                var val1 = $(this).val();
                if(num > 1){
                    if(toggleDX != "false"){
                        var DXCds = "";
                        $.ajax({
                            type: "POST",
                            url:"dx9List.php",
                            async: false,
                            data:{
                                value1 : val1
                            },
                            success:function(result){
                                DXCds = JSON.parse(result);
                            }
                        });
                        $("#principalDiag").autocomplete({
                            minlength : 3,
                            source: DXCds,
                            autoFocus:true,
                            width:500
                        });
                    }
                else{
                        var DXCds = "";
                        $.ajax({
                            type: "POST",
                            url:"dx10List.php",
                            async: false,
                            data:{
                                value1 : val1
                            },
                            success:function(result){
                                DXCds = JSON.parse(result);
                            }
                        });
                        $("#principalDiag").autocomplete({
                            minlength : 3,
                            source: DXCds,
                            autoFocus:true
                        });
                    }
                }
            });
        });

        $('#unableToWorkFrm').mask('00-00-0000');
        $('#unableToWorkTo').mask('00-00-0000');
        $('#injuryDt').mask('00-00-0000');
        $('#totalDisabFrm').mask('00-00-0000');
        $('#totDisabTo').mask('00-00-0000');
        $('#partDisabFrm').mask('00-00-0000');
        $('#partDisabTo').mask('00-00-0000');
        $('#hospFrm').mask('00-00-0000');
        $('#hospTo').mask('00-00-0000');
        $('#resubmissionDt').mask('00-00-0000');

        $('#phoneHome').mask('(000)000-0000');
            $('#phoneWork').mask('(000)000-0000');
            $('#phoneMobile').mask('(000)000-0000');


        $('#loader').hide();
        $('#firstConsultDate').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#firstConsultDate').mask('00-00-0000');
        $('#effStartDt').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#effStartDt').mask('00-00-0000');
        $('#effEndDt').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#effEndDt').mask('00-00-0000');
        $('#dateLastSeen').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#dateLastSeen').mask('00-00-0000');
        $('#refDate').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#refDate').mask('00-00-0000');
        $('#prescripDate').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#prescripDate').mask('00-00-0000');
        $('#lastXRay').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#lastXRay').mask('00-00-0000');
        $('#estimatedDOB').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#estimatedDOB').mask('00-00-0000');
        $('#dateAssumedCare').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#dateAssumedCare').mask('00-00-0000');
        $('#dateRelinquishedCare').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#dateRelinquishedCare').mask('00-00-0000');
        $('#lastWorkDate').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#lastWorkDate').mask('00-00-0000');
        $('#dob').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#globalCoverageUntil').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#globalCoverageUntil').mask('00-00-0000');
        $('#policyStartDt1').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#policyStartDt1').mask('00-00-0000');
        $('#policyEndDt1').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#policyEndDt1').mask('00-00-0000');
        $('#policyStartDt2').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#policyStartDt2').mask('00-00-0000');
        $('#policyEndDt2').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#policyEndDt2').mask('00-00-0000');
        $('#retirementDate').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#retirementDate').mask('00-00-0000');
        $('#lastVisit').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#lastVisit').mask('00-00-0000');
        $('#authorizedThrough').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            format: 'm-d-Y',
            timepicker: false,
            //minDate: '-2013/01/02',
            //maxDate: '+2014/12/31',
            formatDate: 'm-d-Y',
            closeOnDateSelect: true
        });
        $('#authorizedThrough').mask('00-00-0000');
        var employer = <?php include('employerList.php') ?>;
            $("#employer").autocomplete({
                source: employer,
                autoFocus:true
            });
            $("#employerCase").autocomplete({
                source: employer,
                autoFocus:true
            });
            


        $("#btnAddPriPhy").click(function() {
            var primaryPhysiName = $('#PrimaryPhysiName').val();
            var phyDOB = $('#phyDOB').val();
            var phyDegree = $('#phyDegree').val();
            var phyNPI= $('#phyNPI').val();
            var phyType = $('#phyType').val();
            var phySSN = $('#phySSN').val();
            var phyAddr = $('#phyAddr').val();
            var phyHomePhone = $('#phyHomePhone').val();
            var phyWorkPhone = $('#phyWorkPhone').val();
            var phyMobilePhone = $('#phyMobilePhone').val();
            var phyEmail = $('#phyEmail').val();
            var phyFax = $('#phyFax').val();
            var phyNotes = $('#phyNotes').val();

            $.ajax({
                type: "POST",
                url:"backend/add_primaryPhysi.php",
                data:{ 
                    "PrimaryPhysiName" : primaryPhysiName,
                    "phyDOB" : phyDOB,
                    "phyDegree" : phyDegree,
                    "phyNPI" : phyNPI,
                    "phyType" : phyType,
                    "phySSN" : phySSN,
                    "phyAddr" : phyAddr,
                    "phyHomePhone" : phyHomePhone,
                    "phyWorkPhone" : phyWorkPhone,
                    "phyMobilePhone" : phyMobilePhone,
                    "phyEmail" : phyEmail,
                    "phyFax" : phyFax,
                    "phyNotes" : phyNotes
                    },success:function(result){
                        alertify.success('PrimaryPhysiName updated successfully');
                        $('#PrimaryPhysi').modal('hide');
                        //window.location.reload();
                     }

            });
        });

        $.get("facilityList.php", function(data, status){
            var data = JSON.parse(data);
            var arrFacility = [];
            $('#facility').html('');
            for(var x in data){
              arrFacility.push(data[x]);
            }
            //alert(arrCase);
            $.each(arrFacility,function(i,v) {
                $('#facility').html('');
                arrFacility.forEach(function(t) { 
                    $('#facility').append('<option value="'+t.serviceLocID+'">'+t.internalName+'</option>');
                });
            });
        });
        

        $("#fullName").click(function(){
            var data = $("#fullName").val();
            var arr = data.split(' ');
            $('#NameModal').modal('show');
            document.getElementById('FirstName').value = arr[0];

            if(arr[1] == undefined){
            document.getElementById('MiddleName').value = '';
            }
            else{
                document.getElementById('MiddleName').value = arr[1];   
            }

            if(arr[2] == undefined){
                document.getElementById('LastName').value = '';
            }
            else{
                document.getElementById('LastName').value = arr[2]; 
            }
            document.getElementById('FirstName').focus();
        });

        $("#btnSaveFullname").click(function(){
            var first = document.getElementById('FirstName').value;
            var middle = document.getElementById('MiddleName').value;
            var last = document.getElementById('LastName').value;

            if(middle == "" && last!=""){
                var resFullName = document.getElementById('FirstName').value + ' ' +document.getElementById('LastName').value;  
            }
            else if(last == "" && middle == ""){
                var resFullName = document.getElementById('FirstName').value
            }
            var resFullName = document.getElementById('FirstName').value + ' ' + document.getElementById('MiddleName').value + ' ' +document.getElementById('LastName').value;
            $('#NameModal').modal('hide');
            document.getElementById('fullName').value = resFullName;
        });

        $("#Address").click(function(){
            $('#AddrModal').modal('show');
        });
        $("#linkUpload").click(function(){
            $('#upload').modal('show');
        });

        $("#linkattachatt").click(function(){
            $('#attach1').modal('show');
        });

        $("#linkPrimaryPhysi").click(function(){
            $('#PrimaryPhysi').modal('show');
        });
        $("#linkRenderPhysi").click(function(){
            $('#PrimaryPhysi').modal('show');
        });
        $("#linkReferPhysi").click(function(){
            $('#PrimaryPhysi').modal('show');
        });

        $("#linkinsurance").click(function(){
            $('#insurance').modal('show');
        });

        var PrimaryPhy = <?php include('primaryPhysician.php'); ?>;
            $("#primaryCarePhy").autocomplete({
                source: PrimaryPhy,
                autoFocus:true
            });
            $("#defaultRenderPhy").autocomplete({
                source: PrimaryPhy,
                autoFocus:true
            });
            $("#referringPhy").autocomplete({
                source: PrimaryPhy,
                autoFocus:true
            });
            $("#assignedProvider").autocomplete({
                source: PrimaryPhy,
                autoFocus:true
            });
            $("#referringProvider").autocomplete({
                source: PrimaryPhy,
                autoFocus:true
            });
            $("#supervisingProvider").autocomplete({
                source: PrimaryPhy,
                autoFocus:true
            });
            $("#operatingProvider").autocomplete({
                source: PrimaryPhy,
                autoFocus:true
            });
            $("#otherProvider").autocomplete({
                source: PrimaryPhy,
                autoFocus:true
            });
            $("#outsideProvider").autocomplete({
                source: PrimaryPhy,
                autoFocus:true
            });
            

            var billinCod = <?php include('billingCode.php'); ?>;
            $("#caseBillingCode").autocomplete({
                source: billinCod,
                autoFocus:true
            });

            var insurances = <?php include('insuranceList.php'); ?>;
            $("#policyInsurance1").autocomplete({
                source: insurances,
                autoFocus:true
            }); 
            $("#policyInsurance2").autocomplete({
                source: insurances,
                autoFocus:true
            }); 

        $('#policyRelationInsured1').change(function(){
            //alert($(this).val());
            if($(this).val() != "Self"){
                $('#insuredDiv').show();
            }
            else{
                $('#insuredDiv').hide();
            }
        })

        $("#btnSaveCase").click(function() {
            alert("Hit Case Save");
            addCase();
        });

        $("#btnNext").click(function() {
            if($('.nav-tabs .active').text() == "Patient Info"){
                $(".validateGeneral").each(function () {
                    //alert($(this).val());
                    if ($(this).val() === "") {
                        var changeBorder = $(this).attr('id');
                        document.getElementById(changeBorder).style.border = "1px solid maroon";
                        document.getElementById(changeBorder).focus();
                        return false;
                    }
                    else {
                            $(".alert").remove();
                            $(this).removeClass('validateGeneral')
                            var changeBorder = $(this).attr('id');
                            document.getElementById(changeBorder).style.border = "1px solid #ccc";
                            if ($('.validateGeneral').length <= 0) {
                              $('.nav-tabs a[href="#tab_3_2"]').tab('show');
                              $('.nav-tabs a[href="#tab_3_2_1"]').addClass('active');
                              add_patient();
                            }
                    }                
                });
            }
        });
    });

    function addCase(){
        alert("Hit Case Save function");
        var PatID = document.getElementById('hdnPatientID').value;
        var description = $('#description').val();
        // var allowPrint = $('#allowPrint').val();
        // var isDeleted = $('#isDeleted').val();
        // var caseDt = $('#caseDt').val();
        // var caseChartNo = $('#caseChartNo').val();
        var globalCoverageUntil = '';
        var employer = $('#employer').val();
        var empstatus = $('#empstatus').val();
        var retirementDate = convertDate($('#retirementDate').val());
        var location = $('#location').val();
        //var workPhone = $('#workPhone').val();
        var extension = $('#extension').val();
        var studStatus = $('#studStatus').val();
        var guarantor = $('#guarantor').val();
        var maritalStatus = $('#caseMartialStatus').val();
        var assignedProvider = $('#assignedProvider').val();
        var referringProvider = $('#referringProvider').val();
        var supervisingProvider = $('#supervisingProvider').val();
        var operatingProvider = $('#operatingProvider').val();
        var otherProvider = $('#otherProvider').val();
        var referralSource = $('#referralSource').val();
        var attorney = $('#attorney').val();
        var facility = $('#facility').val();
        var caseBillingCode = $('#caseBillingCode').val();
        var priceCode = $('#priceCode').val();
        var otherArrangements = $('#otherArrangements').val();
        var authorizedThrough = $('#authorizedThrough').val();
        var authNo = $('#authNo').val();
        var lastVisit = convertDate($('#lastVisit').val());
        var noOfVisits = $('#noOfVisits').val();
        var authID = $('#authID').val();
        var lastVisitNo = $('#lastVisitNo').val();
        var principalDiag = $('#principalDiag').val();
        var defDiag2 = $('#defDiag2').val();
        var defDiag3 = $('#defDiag3').val();
        var defDiag4 = $('#defDiag4').val();
        var poa1 = $('#poa1').val();
        var poa2 = $('#poa2').val();
        var poa3 = $('#poa3').val();
        var poa4 = $('#poa4').val();
        var allergiesNotes = $('#allergiesNotes').val();
        var ediNotes = $('#ediNotes').val();
        var repTypeCode = $('#repTypeCode').val();
        var attachCtrlNo = $('#attachCtrlNo').val();
        var repTransCode = $('#repTransCode').val();
        var injuryDt = convertDate($('#injuryDt').val());
        var illnessIndicator = $('#illnessIndicator').val();
        var firstConsultDate = convertDate($('#firstConsultDate').val());
        var similarSymptoms = $('#similarSymptoms').val();
        var sameSymptom = $('#sameSymptom').val();
        var empRelated = $('#empRelated').val();
        var emergency = $('#emergency').val();
        var relatedTo = $('#relatedTo').val();
        var relatedState = $('#relatedState').val();
        var nature = $('#nature').val();
        var lastXRay = convertDate($('#lastXRay').val());
        var deathStat = $('#deathStat').val();
        var unableToWorkFrm = convertDate($('#unableToWorkFrm').val());
        var unableToWorkTo = convertDate($('#unableToWorkTo').val());
        var totalDisabFrm = convertDate($('#totalDisabFrm').val());
        var totDisabTo = convertDate($('#totDisabTo').val());
        var partDisabFrm = convertDate($('#partDisabFrm').val());
        var partDisabTo = convertDate($('#partDisabTo').val());
        var hospFrm = convertDate($('#hospFrm').val());
        var hospTo = convertDate($('#hospTo').val());
        var returnWorkIndicator = $('#returnWorkIndicator').val();
        var disabilityPercent = $('#disabilityPercent').val();
        var lastWorkDate = convertDate($('#lastWorkDate').val());
        var pregnant = $('#pregnant').val();
        var estimatedDOB = convertDate($('#estimatedDOB').val());
        var dateAssumedCare = convertDate($('#dateAssumedCare').val());
        var dateRelinquishedCare = convertDate($('#dateRelinquishedCare').val());
        var outsideLab = $('#outsideLab').val();
        var labCharges = $('#labCharges').val();
        var localUseA = $('#localUseA').val();
        var localUseB = $('#localUseB').val();
        var indicator = $('#indicator').val();
        var refDate = convertDate($('#refDate').val());
        var prescripDate = convertDate($('#prescripDate').val());
        var priorAuthNo = $('#priorAuthNo').val();
        var extra1 = $('#extra1').val();
        var extra2 = $('#extra2').val();
        var extra3 = $('#extra3').val();
        var extra4 = $('#extra4').val();
        var outsideProvider = $('#outsideProvider').val();
        var dateLastSeen = convertDate($('#dateLastSeen').val());
        var epsdt = $('#epsdt').val();
        var resubmissionDt = convertDate($('#resubmissionDt').val());
        var familyPlanning = $('#familyPlanning').val();
        var originalRefNo = $('#originalRefNo').val();
        var serviceAuth = $('#serviceAuth').val();
        var nonAvailIndicator = $('#nonAvailIndicator').val();
        var branchService = $('#branchService').val();
        var sponsorStat = $('#sponsorStat').val();
        var splProgram = $('#splProgram').val();
        var sponsorGuide = $('#sponsorGuide').val();
        var effStartDt = convertDate($('#effStartDt').val());
        var effEndDt = convertDate($('#effEndDt').val());
        var carePlanOversight = $('#carePlanOversight').val();
        var hospiceNo = $('#hospiceNo').val();
        var CLIANo = $('#CLIANo').val();
        var mamoCertification = $('#mamoCertification').val();
        var refAccessNo = $('#refAccessNo').val();
        var demoCode = $('#demoCode').val();
        var assignIndicator = $('#assignIndicator').val();
        var insuranceTypeCode = $('#insuranceTypeCode').val();
        var timelyFillingIndicator = $('#timelyFillingIndicator').val();
        var EPSDTRefCode1 = $('#EPSDTRefCode1').val();
        var EPSDTRefCode2 = $('#EPSDTRefCode2').val();
        var EPSDTRefCode3 = $('#EPSDTRefCode3').val();
        var homebound = $('#homebound').val();
        var IDENo = $('#IDENo').val();
        var conditionIndicator = $('#conditionIndicator').val();
        var certCodeApplies = $('#certCodeApplies').val();
        var codeCategory = $('#codeCategory').val();
        var totalVisitRendered = $('#totalVisitRendered').val();
        var totalVisitProjected = $('#totalVisitProjected').val();
        var noOfVisitsHomeHealth = $('#noOfVisitsHomeHealth').val();
        var noOfUnits = $('#noOfUnits').val();
        var duration = $('#duration').val();
        var disciplineTypeCode = $('#disciplineTypeCode').val();
        var deliveryPatternCode = $('#deliveryPatternCode').val();
        var deliveryTimeCode = $('#deliveryTimeCode').val();
        var freqPeriod = $('#freqPeriod').val();
        var freqCount = $('#freqCount').val();
        //var isCash = $('#isCash').val();

        var policyInsurance1 = $('#policyInsurance1').val();
        var policyHolder1 = $('#policyHolder1').val();
        var policyRelationInsured1 = $('#policyRelationInsured1').val();
        var policyNo1 = $('#policyNo1').val();
        var policyStartDt1 = $('#policyStartDt1').val();
        var policyGroupNo1 = $('#policyGroupNo1').val();
        var policyEndDt1 = $('#policyEndDt1').val();
        var policyClaimNo1 = $('#policyClaimNo1').val();
        var policyAssignBenefits1 = $('#policyAssignBenefits1').val();
        var policyDeduc1 = $('#policyDeduc1').val();
        var policyCapitatedPlan1 = $('#policyCapitatedPlan1').val();
        var policyAnnualDedu1 = $('#policyAnnualDedu1').val();
        var policyCopay1 = $('#policyCopay1').val();    
        var policyTreatAuth1 = $('#policyTreatAuth1').val();
        var policyDocControl1 = $('#policyDocControl1').val();
        var policyA1 = $('#policyA1').val();
        var policyB1 = $('#policyB1').val();    
        var policyC1 = $('#policyC1').val();
        var policyD1 = $('#policyD1').val();
        var policyE1 = $('#policyE1').val();
        var policyF1 = $('#policyF1').val();
        var policyG1 = $('#policyG1').val();
        var policyH1 = $('#policyH1').val();

        var policyInsurance2 = $('#policyInsurance2').val();
        var policyHolder2 = $('#policyHolder2').val();
        var policyRelationInsured2 = $('#policyRelationInsured2').val();
        var policyNo2 = $('#policyNo2').val();
        var policyStartDt2 = $('#policyStartDt2').val();
        var policyGroupNo2 = $('#policyGroupNo2').val();
        var policyEndDt2 = $('#policyEndDt2').val();
        var policyClaimNo2 = $('#policyClaimNo2').val();
        var policyAssignBenefits2 = $('#policyAssignBenefits2').val();
        var policyDeduc2 = $('#policyDeduc2').val();
        var policyCapitatedPlan2 = $('#policyCapitatedPlan2').val();
        var policyAnnualDedu2 = $('#policyAnnualDedu2').val();
        var policyCopay2 = $('#policyCopay2').val();    
        var policyTreatAuth2 = $('#policyTreatAuth2').val();
        var policyDocControl2 = $('#policyDocControl2').val();
        var policyA2 = $('#policyA2').val();
        var policyB2 = $('#policyB2').val();    
        var policyC2 = $('#policyC2').val();
        var policyD2 = $('#policyD2').val();
        var policyE2 = $('#policyE2').val();
        var policyF2 = $('#policyF2').val();
        var policyG2 = $('#policyG2').val();
        var policyH2 = $('#policyH2').val();
        var caseChartNoVal = document.getElementById('lblCaseNo').innerHTML;

       
        $.ajax({
            async : false,
            type: "POST",
            url:"http://curismed.com/medService/cases/create",
            data:{
                "patientID" : PatID,
                "description" : description,
                // "allowPrint" : allowPrint,
                // "isDeleted" : isDeleted,
                // "caseDt" : caseDt,
                // "caseChartNo" : caseChartNo,
                "globalCoverageUntil" : globalCoverageUntil,
                "employer" : employer,
                "empstatus" : empstatus,
                "retirementDate" : retirementDate,
                "location" : location,
                //"workPhone" : workPhone,
                "extension" : extension,
                "studStatus" : studStatus,
                "guarantor" : guarantor,
                "maritalStatus" : maritalStatus,
                "assignedProvider" : assignedProvider,
                "referringProvider" : referringProvider,
                "supervisingProvider" : supervisingProvider,
                "operatingProvider" : operatingProvider,
                "otherProvider" : otherProvider,
                "referralSource" : referralSource,
                "attorney" : attorney,
                "facility" : facility,
                "caseBillingCode" : caseBillingCode,
                "priceCode" : priceCode,
                "otherArrangements" : otherArrangements,
                "authorizedThrough" : authorizedThrough,
                "authNo" : authNo,
                "lastVisit" : lastVisit,
                "noOfVisits" : noOfVisits,
                "authID" : authID,
                "lastVisitNo" : lastVisitNo,
                "principalDiag" : principalDiag,
                "defDiag2" : defDiag2,
                "defDiag3" : defDiag3,
                "defDiag4" : defDiag4,
                "poa1" : poa1,
                "poa2" : poa2,
                "poa3" : poa3,
                "poa4" : poa4,
                "allergiesNotes" : allergiesNotes,
                "ediNotes" : ediNotes,
                "repTypeCode" : repTypeCode,
                "attachCtrlNo" : attachCtrlNo,
                "repTransCode" : repTransCode,
                "injuryDt" : injuryDt,
                "illnessIndicator" : illnessIndicator,
                "firstConsultDate" : firstConsultDate,
                "similarSymptoms" : similarSymptoms,
                "sameSymptom" : sameSymptom,
                "empRelated" : empRelated,
                "emergency" : emergency,
                "relatedTo" : relatedTo,
                "relatedState" : relatedState,
                "nature" : nature,
                "lastXRay" : lastXRay,
                "deathStat" : deathStat,
                "unableToWorkFrm" : unableToWorkFrm,
                "unableToWorkTo" : unableToWorkTo,
                "totalDisabFrm" : totalDisabFrm,
                "totDisabTo" : totDisabTo,
                "partDisabFrm" : partDisabFrm,
                "partDisabTo" : partDisabTo,
                "hospFrm" : hospFrm,
                "hospTo" : hospTo,
                "returnWorkIndicator" : returnWorkIndicator,
                "disabilityPercent" : disabilityPercent,
                "lastWorkDate" : lastWorkDate,
                "pregnant" : pregnant,
                "estimatedDOB" : estimatedDOB,
                "dateAssumedCare" : dateAssumedCare,
                "dateRelinquishedCare" : dateRelinquishedCare,
                "outsideLab" : outsideLab,
                "labCharges" : labCharges,
                "localUseA" : localUseA,
                "localUseB" : localUseB,
                "indicator" : indicator,
                "refDate" : refDate,
                "prescripDate" : prescripDate,
                "priorAuthNo" : priorAuthNo,
                "extra1" : extra1,
                "extra2" : extra2,
                "extra3" : extra3,
                "extra4" : extra4,
                "outsideProvider" : outsideProvider,
                "dateLastSeen" : dateLastSeen,
                "epsdt" : epsdt,
                "resubmissionDt" : resubmissionDt,
                "familyPlanning" : familyPlanning,
                "originalRefNo" : originalRefNo,
                "serviceAuth" : serviceAuth,
                "nonAvailIndicator" : nonAvailIndicator,
                "branchService" : branchService,
                "sponsorStat" : sponsorStat,
                "splProgram" : splProgram,
                "sponsorGuide" : sponsorGuide,
                "effStartDt" : effStartDt,
                "effEndDt" : effEndDt,
                "carePlanOversight" : carePlanOversight,
                "hospiceNo" : hospiceNo,
                "CLIANo" : CLIANo,
                "mamoCertification" : mamoCertification,
                "refAccessNo" : refAccessNo,
                "demoCode" : demoCode,
                "assignIndicator" : assignIndicator,
                "insuranceTypeCode" : insuranceTypeCode,
                "timelyFillingIndicator" : timelyFillingIndicator,
                "EPSDTRefCode1" : EPSDTRefCode1,
                "EPSDTRefCode2" : EPSDTRefCode2,
                "EPSDTRefCode3" : EPSDTRefCode3,
                "homebound" : homebound,
                "IDENo" : IDENo,
                "conditionIndicator" : conditionIndicator,
                "certCodeApplies" : certCodeApplies,
                "codeCategory" : codeCategory,
                "totalVisitRendered" : totalVisitRendered,
                "totalVisitProjected" : totalVisitProjected,
                "noOfVisitsHomeHealth" : noOfVisitsHomeHealth,
                "noOfUnits" : noOfUnits,
                "duration" : duration,
                "disciplineTypeCode" : disciplineTypeCode,
                "deliveryPatternCode" : deliveryPatternCode,
                "deliveryTimeCode" : deliveryTimeCode,
                "freqPeriod" : freqPeriod,
                "freqCount" : freqCount,
                // "isCash" : isCash,

            },success:function(result){
                alert("Hit Case Save finished");   
            }
        });
        if(policyInsurance1 != ""){
             $.ajax({
                type: "POST",
                url:"getInsuranceID.php",
                async: false,
                data:{
                    "payerName" : policyInsurance1
                    },success:function(result){
                        sessionStorage.setItem("insurance1",result);
                }
            });
        }
        if(policyInsurance2 != ""){
            $.ajax({
                type: "POST",
                url:"getInsuranceID.php",
                async: false,
                data:{
                    "payerName" : policyInsurance2
                    },success:function(result){
                        sessionStorage.setItem("insurance2",result);
                }
            });
        }

        $.ajax({
            type: "POST",
            url:"chartNoList.php",
            async: false,
            data:{
                "patientID" : PatID
            },success:function(result){
                var hdnCaseChartNo = result;
                resultNew = hdnCaseChartNo.split(',');
                sessionStorage.setItem("tempCaseChartNo", resultNew[0]);
                sessionStorage.setItem("tempClaimID", resultNew[1]);
            }
        });    
        var tempCaseChartNo = sessionStorage.getItem("tempCaseChartNo");
        var tempClaimID = sessionStorage.getItem("tempClaimID");
        var insurance1 = sessionStorage.getItem("insurance1");
        var insurance2 = sessionStorage.getItem("insurance2");
        if(policyInsurance1 != "" && policyNo1 != ""){
            $.ajax({
                async : false,
                type: "POST",
                url:"http://curismed.com/medService/policies/create",
                data:{
                    "policyEntity" : "1",
                    "caseID" : tempClaimID,
                    "caseChartNo" : caseChartNoVal,
                    "insuranceID" : insurance1,
                    "policyHolder" : policyHolder1,
                    "relationshipToInsured" : policyRelationInsured1,
                    "policyNo" : policyNo1,
                    "groupNo" : policyGroupNo1,
                    "claimNo" : policyClaimNo1,
                    "startDt" : policyStartDt1,
                    "endDt" : policyEndDt1,
                    "assigned" : policyAssignBenefits1,
                    "crossoverClaim" : policyCapitatedPlan1,
                    "deductibleMet" : policyDeduc1,
                    "annualDeductible" : policyAnnualDedu1,
                    "coPayment" : policyCopay1,
                    "treatmentAuth" : policyTreatAuth1,
                    "docCtrlNo" : policyDocControl1,
                    "insClassA" : policyA1,
                    "insClassB" : policyB1,
                    "insClassC" : policyC1,
                    "insClassD" : policyD1,
                    "insClassE" : policyE1,
                    "insClassF" : policyF1,
                    "insClassG" : policyG1,
                    "insClassH" : policyH1
                },success:function(result){
                    alertify.success("Primary Policy added successfully");
                }
            });
        }
        if(policyInsurance2 != "" && policyNo2 != ""){
            $.ajax({
                async : false,
                type: "POST",
                url:"http://curismed.com/medService/policies/create",
                data:{
                    "policyEntity" : "2",
                    "caseID" : tempClaimID,
                    "caseChartNo" : caseChartNoVal,
                    "insuranceID" : insurance2,
                    "policyHolder" : policyHolder2,
                    "relationshipToInsured" : policyRelationInsured2,
                    "policyNo" : policyNo2,
                    "groupNo" : policyGroupNo2,
                    "claimNo" : policyClaimNo2,
                    "startDt" : policyStartDt2,
                    "endDt" : policyEndDt2,
                    "assigned" : policyAssignBenefits2,
                    "crossoverClaim" : policyCapitatedPlan2,
                    "deductibleMet" : policyDeduc2,
                    "annualDeductible" : policyAnnualDedu2,
                    "coPayment" : policyCopay2,
                    "treatmentAuth" : policyTreatAuth2,
                    "docCtrlNo" : policyDocControl2,
                    "insClassA" : policyA2,
                    "insClassB" : policyB2,
                    "insClassC" : policyC2,
                    "insClassD" : policyD2,
                    "insClassE" : policyE2,
                    "insClassF" : policyF2,
                    "insClassG" : policyG2,
                    "insClassH" : policyH2

                },success:function(result){
                    alertify.success("Secondary Policy added successfully");
                }
            });
        }
alertify.success("Activity successfully added");
        window.location.reload();
    }
    var convertDate = function(usDate) {
      var dateParts = usDate.split(/(\d{1,2})-(\d{1,2})-(\d{4})/);
      return dateParts[3] + "-" + dateParts[1] + "-" + dateParts[2];
    }
    var getPhyID = function(phyVal) {
      var n=phyVal.indexOf("-");
      var truncID = phyVal.substr(0,n)
      return truncID;
    }
    function add_patient(){
        //alert("Hi");
        var Name = $("#fullName").val();
        var MartialStatus = $('#martialStatus').val();
        var SocialSecurity = $('#ssn').val();
        var EmploymentStatus = $('#employment').val();
        var tempDOB = $('#dob').val();
        var DOB = convertDate(tempDOB);
        var Employer = $('#employer').val();
        var Gender = $('#gender').val();
        var ReferralSource = $('#Reference').val();
        var medicalRecord = $('#medicalRecord').val();
        var addrStreet1 = $('#addrStreet1').val();
        var addrStreet2 = $('#addrStreet2').val();
        var addrState = $('#addrState').val();
        var addrCity = $('#addrCity').val();
        var addrZip = $('#addrZip').val();
        var HomePhone =  $('#phoneHome').val();
        var WorkPhone = $('#phoneWork').val();
        var MobilePhone = $('#phoneMobile').val();
        if($('#notifyEmail').prop('checked')=="true"){
        var IsMail = "1";
        }
        else{
        var IsMail = "0";
        }
        if($('#notifyPhone').prop('checked')=="true"){
        var IsPhoneRemainder = "1";
        }
        else{
        var IsPhoneRemainder = "0";
        }
        var IsEmailAddr = $('#email').val();
        var IsPhoneRemainder = $('#notifyPhone').val();
        var PrimaryCarePhy =  getPhyID($('#primaryCarePhy').val());
        var DRenderProvider = getPhyID($('#defaultRenderPhy').val());
        var ReferPhy = getPhyID($('#referringPhy').val());
        var DServiceLoc = $('#defaultServiceLoc').val();
        var practiceID = sessionStorage.getItem('practiceId');

        // var IsFinancialResp = $('#IsFinancialResp').val();
        // var DPayerScenario = $('#DPayerScenario').val();
        // var Notes =  $('#Notes').val();
        $('#loader').show();
        $.ajax({
            type: "POST",
            url:"http://curismed.com/medService/patients/create",
            data:{ 
                "practiceID": practiceID,
                "fullName" : Name,
                "martialStatus" : MartialStatus,
                "ssn" : SocialSecurity,
                "employment" : EmploymentStatus,
                "dob" : DOB,
                "employer" : Employer,
                "gender" : Gender,
                "reference" : ReferralSource,
                "medicalRecord" : medicalRecord,
                "addrStreet1" : addrStreet1,
                "addrStreet2" : addrStreet2,
                "addrState" : addrState,
                "addrCity" : addrCity,
                "addrZip" : addrZip,
                "phoneHome" : HomePhone,
                "phoneWork" : WorkPhone,
                "phoneMobile" : MobilePhone,
                "notifyEmail" : IsMail,
                "email" : IsEmailAddr,
                "notifyPhone" : IsPhoneRemainder,
                "primaryCarePhy" : PrimaryCarePhy,
                "defaultRenderPhy" : DRenderProvider,
                "referringPhy" : ReferPhy,
                "defaultServiceLoc" : DServiceLoc,
                // "IsFinancialResp" : IsFinancialResp,
                // "DPayerScenario" : DPayerScenario,
                // "Notes" : Notes
               },success:function(result){
                    $('#loader').hide();
                    alertify.success("New Patient added successfully");
                    var FirstName = $('#FirstName').val();
                    var FirstName3 = FirstName.substring(0, 3);
                    var LastName = $('#LastName').val();
                    var LastName3 = LastName.substring(0, 3);
                    var patientAccNo = FirstName3 + LastName3;
                    var AccNo = patientAccNo.toUpperCase();
                    document.getElementById('hdnPatientID').value = result.patientID;
                    $.post("http://curismed.com/medService/patients/chartnumber",
                    {
                        patientID : result.patientID,
                        prefix : AccNo
                    },
                    function(data, status){
                        $('#tab1').removeAttr('class','active');
                        $('#tab2').attr('class','active');
                        $('#main_tab a[href="#tab_3_2_1"]').attr('class','active');
                        document.getElementById('lblCaseNo').innerHTML = data.ChartNumber+'_1';
                });
                }

        });
    }

    </script>

    <!-- Demo JS -->
    <script type="text/javascript" src="assets/js/custom.js"></script>
    <script type="text/javascript" src="assets/js/demo/pages_calendar.js"></script>
    <style>
    #principalDiag {
  border: none; 
  font-size: 14px;

  height: 24px;
  margin-bottom: 5px;
  padding-top: 2px;
  border: 1px solid #DDD !important;
  padding-top: 0px !important;
  z-index: 1511;
  position: relative;
}
#defDiag2 {
  border: none; 
  font-size: 14px;

  height: 24px;
  margin-bottom: 5px;
  padding-top: 2px;
  border: 1px solid #DDD !important;
  padding-top: 0px !important;
  z-index: 1511;
  position: relative;
}
#defDiag3 {
  border: none; 
  font-size: 14px;

  height: 24px;
  margin-bottom: 5px;
  padding-top: 2px;
  border: 1px solid #DDD !important;
  padding-top: 0px !important;
  z-index: 1511;
  position: relative;
}
#defDiag4 {
  border: none; 
  font-size: 14px;

  height: 24px;
  margin-bottom: 5px;
  padding-top: 2px;
  border: 1px solid #DDD !important;
  padding-top: 0px !important;
  z-index: 1511;
  position: relative;
}
.ui-menu .ui-menu-item a {
  font-size: 12px;
  color:#fff;
}
.ui-autocomplete {
  position: absolute;
  top: 0;
  left: 0;
  z-index: 1510 !important;
  float: left;
  display: none;
  min-width: 160px;
  width: 160px;
  padding: 4px 0;
  margin: 2px 0 0 0;
  list-style: none;
  background-color: #ffffff;
  border-color: #ccc;
  border-color: rgba(0, 0, 0, 0.2);
  border-style: solid;
  border-width: 1px;
  -webkit-border-radius: 2px;
  -moz-border-radius: 2px;
  border-radius: 2px;
  -webkit-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
  -moz-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
  box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
  -webkit-background-clip: padding-box;
  -moz-background-clip: padding;
  background-clip: padding-box;
  *border-right-width: 2px;
  *border-bottom-width: 2px;
}
.ui-menu-item > a.ui-corner-all {
    display: block;
    padding: 3px 15px;
    clear: both;
    font-weight: normal;
    line-height: 18px;
    color:#000;
    white-space: nowrap;
    text-decoration: none;
}
.ui-state-hover {
      color: #fff;
      text-decoration: none;
      background-color: #0088cc;
      border-radius: 0px;
      -webkit-border-radius: 0px;
      -moz-border-radius: 0px;
      background-image: none;
}
.ui-state-active {
      color: #fff;
      text-decoration: none;
      background-color: #0088cc;
      border-radius: 0px;
      -webkit-border-radius: 0px;
      -moz-border-radius: 0px;
      background-image: none;
}

    </style>
</head>

<body>

    <!-- Header -->
    <header class="header navbar navbar-fixed-top" role="banner" style="background-image: url('assets/bg.jpg'); background-repeat: repeat-x;">
        <!-- Top Navigation Bar -->
        <div class="container">

            <!-- Only visible on smartphones, menu toggle -->
            <ul class="nav navbar-nav">
                <li class="nav-toggle"><a href="javascript:void(0);" title=""><i class="icon-reorder"></i></a></li>
            </ul>

            <!-- Logo -->
            <a class="navbar-brand" style="text-align:center;padding-right:50px;" href="index.html">
                <img src="assets/logo2.png"/>
                <strong>medABA</strong>
            </a>
            <!-- /logo -->

            <!-- Sidebar Toggler -->
            <a href="#" class="toggle-sidebar bs-tooltip" data-placement="bottom" data-original-title="Toggle navigation">
                <i class="icon-reorder"></i>
            </a>
            <!-- /Sidebar Toggler -->

            <!-- Top Left Menu -->
            <ul class="nav navbar-nav navbar-left hidden-xs hidden-sm">
                <li>
                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                        Appointment
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                        Patients
                        <i class="icon-caret-down small"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="AddPatient.php">
                            <i class="icon-angle-right"></i>
                            Add Patients
                            </a>
                        </li>
                        <li>
                            <a href="ViewPatient.php">
                            <i class="icon-angle-right"></i>
                            Find Patients
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                        Claims
                        <i class="icon-caret-down small"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="AddClaim.php">
                            <i class="icon-angle-right"></i>
                            Add Claims
                            </a>
                        </li>
                        <li>
                            <a href="ViewClaims.php">
                            <i class="icon-angle-right"></i>
                            View Claims
                            </a>
                        </li>
                        <li>
                            <a href="Ledger.php">
                            <i class="icon-angle-right"></i>
                            Ledger
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                        Financial
                        <i class="icon-caret-down small"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="Deposit.php">
                            <i class="icon-angle-right"></i>
                            View Deposits
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="charts.html" class="dropdown-toggle" data-toggle="dropdown">
                        Reports
                        <i class="icon-caret-down small"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="javascript:voic(0);">
                            <i class="icon-angle-right"></i>
                             Accounts Receivable
                            </a>
                        </li>
                        <li>
                            <a href="javascript:voic(0);">
                            <i class="icon-angle-right"></i>
                            Productivity
                            </a>
                        </li>
                        <li>
                            <a href="javascript:voic(0);">
                            <i class="icon-angle-right"></i>
                            Patients
                            </a>
                        </li>
                        <li>
                            <a href="javascript:voic(0);">
                            <i class="icon-angle-right"></i>
                            Appointments
                            </a>
                        </li>
                        <li>
                            <a href="javascript:voic(0);">
                            <i class="icon-angle-right"></i>
                            Claims
                            </a>
                        </li>
                        <li>
                            <a href="javascript:voic(0);">
                            <i class="icon-angle-right"></i>
                            Payments
                            </a>
                        </li>
                        <li>
                            <a href="javascript:voic(0);">
                            <i class="icon-angle-right"></i>
                            Refunds
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="charts.html" class="dropdown-toggle" data-toggle="dropdown">
                        Settings
                        <i class="icon-caret-down small"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="ViewPhysician.php">
                            <i class="icon-angle-right"></i>
                            Physicians
                            </a>
                        </li>
                        <li>
                            <a href="ViewPractice.php">
                            <i class="icon-angle-right"></i>
                            Practices
                            </a>
                        </li>
                        <li>
                            <a href="ViewServiceLoc.php">
                            <i class="icon-angle-right"></i>
                            Locations
                            </a>
                        </li>
                        <li>
                            <a href="ViewFacility.php">
                            <i class="icon-angle-right"></i>
                            Facilities
                            </a>
                        </li>
                        <li>
                            <a href="ViewCPT.php">
                            <i class="icon-angle-right"></i>
                            CPTs List
                            </a>
                        </li>
                        <li>
                            <a href="EDIList.php">
                            <i class="icon-angle-right"></i>
                            EDI List
                            </a>
                        </li>
                        <li>
                            <a href="generateEDI.php">
                            <i class="icon-angle-right"></i>
                            Generate EDI
                            </a>
                        </li>
                        <li>
                            <a href="generateAllEDI.php">
                            <i class="icon-angle-right"></i>
                            Generate Bulk EDI
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- /Top Left Menu -->

            <!-- Top Right Menu -->
            <ul class="nav navbar-nav navbar-right">
                <!-- Notifications -->
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-warning-sign"></i>
                        <span class="badge">5</span>
                    </a>
                    <ul class="dropdown-menu extended notification">
                        <li class="title">
                            <p>You have 5 new notifications</p>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="label label-success"><i class="icon-plus"></i></span>
                                <span class="message">New user registration.</span>
                                <span class="time">1 mins</span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="label label-danger"><i class="icon-warning-sign"></i></span>
                                <span class="message">High CPU load on cluster #2.</span>
                                <span class="time">5 mins</span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="label label-success"><i class="icon-plus"></i></span>
                                <span class="message">New user registration.</span>
                                <span class="time">10 mins</span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="label label-info"><i class="icon-bullhorn"></i></span>
                                <span class="message">New items are in queue.</span>
                                <span class="time">25 mins</span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="label label-warning"><i class="icon-bolt"></i></span>
                                <span class="message">Disk space to 85% full.</span>
                                <span class="time">55 mins</span>
                            </a>
                        </li>
                        <li class="footer">
                            <a href="javascript:void(0);">View all notifications</a>
                        </li>
                    </ul>
                </li>

                <!-- Tasks -->
                <li class="dropdown hidden-xs hidden-sm">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-tasks"></i>
                        <span class="badge">7</span>
                    </a>
                    <ul class="dropdown-menu extended notification">
                        <li class="title">
                            <p>You have 7 pending tasks</p>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="task">
                                    <span class="desc">Preparing new release</span>
                                    <span class="percent">30%</span>
                                </span>
                                <div class="progress progress-small">
                                    <div style="width: 30%;" class="progress-bar progress-bar-info"></div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="task">
                                    <span class="desc">Change management</span>
                                    <span class="percent">80%</span>
                                </span>
                                <div class="progress progress-small progress-striped active">
                                    <div style="width: 80%;" class="progress-bar progress-bar-danger"></div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="task">
                                    <span class="desc">Mobile development</span>
                                    <span class="percent">60%</span>
                                </span>
                                <div class="progress progress-small">
                                    <div style="width: 60%;" class="progress-bar progress-bar-success"></div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="task">
                                    <span class="desc">Database migration</span>
                                    <span class="percent">20%</span>
                                </span>
                                <div class="progress progress-small">
                                    <div style="width: 20%;" class="progress-bar progress-bar-warning"></div>
                                </div>
                            </a>
                        </li>
                        <li class="footer">
                            <a href="javascript:void(0);">View all tasks</a>
                        </li>
                    </ul>
                </li>

                <!-- Messages -->
                <li class="dropdown hidden-xs hidden-sm">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-envelope"></i>
                        <span class="badge">1</span>
                    </a>
                    <ul class="dropdown-menu extended notification">
                        <li class="title">
                            <p>You have 3 new messages</p>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="photo"><img src="assets/img/demo/avatar-1.jpg" alt="" /></span>
                                <span class="subject">
                                    <span class="from">Bob Carter</span>
                                    <span class="time">Just Now</span>
                                </span>
                                <span class="text">
                                    Consetetur sadipscing elitr...
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="photo"><img src="assets/img/demo/avatar-2.jpg" alt="" /></span>
                                <span class="subject">
                                    <span class="from">Jane Doe</span>
                                    <span class="time">45 mins</span>
                                </span>
                                <span class="text">
                                    Sed diam nonumy...
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="photo"><img src="assets/img/demo/avatar-3.jpg" alt="" /></span>
                                <span class="subject">
                                    <span class="from">Patrick Nilson</span>
                                    <span class="time">6 hours</span>
                                </span>
                                <span class="text">
                                    No sea takimata sanctus...
                                </span>
                            </a>
                        </li>
                        <li class="footer">
                            <a href="javascript:void(0);">View all messages</a>
                        </li>
                    </ul>
                </li>

                <!-- .row .row-bg Toggler -->
                <li>
                    <a href="#" class="dropdown-toggle row-bg-toggle">
                        <i class="icon-resize-vertical"></i>
                    </a>
                </li>


                <!-- User Login Dropdown -->
                <li class="dropdown user">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <!--<img alt="" src="assets/img/avatar1_small.jpg" />-->
                        <i class="icon-male"></i>
                        <span class="username" id="practiceName"></span>
                        <i class="icon-caret-down small"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="javascript:void(0);"><i class="icon-user"></i> My Profile</a></li>
                        <li><a href="javascript:void(0);"><i class="icon-calendar"></i> My Calendar</a></li>
                        <li><a href="javascript:void(0);"><i class="icon-tasks"></i> My Tasks</a></li>
                        <li class="divider"></li>
                        <li><a href="login.php"><i class="icon-key"></i> Log Out</a></li>
                    </ul>
                </li>
                <!-- /user login dropdown -->
            </ul>
            <!-- /Top Right Menu -->
        </div>
        <!-- /top navigation bar -->

    </header> <!-- /.header -->

    <div id="container">

        <div id="content">
            <div class="container">
                <!-- Breadcrumbs line -->
        <div class="crumbs">
            <ul id="breadcrumbs" class="breadcrumb">
                <li class="current">
                    <i class="icon-home"></i>
                    <a href="index.html">Dashboard</a>
                </li>
                
            </ul>
            <a href="javascript:void(0);"><img src="assets/icons/Settings.png" title="Settings" style="float:right; padding-top:0.3em; padding-right:2em;"/></a>
                    <a href="Ledger.php"><img src="assets/icons/Claim_search.png" title="Claim Search" style="float:right; padding-top:0.3em; padding-right:2em;"/></a>
                    <a href="AddClaim.php"><img src="assets/icons/Claim_add.png" title="Add Claim" style="float:right; padding-top:0.3em; padding-right:2em;"/></a>
                    <a href="ViewPatient.php"><img src="assets/icons/Patient_search.png" title="Patient Search" style="float:right; padding-top:0.3em; padding-right:2em;"/></a>
                    <a href="AddPatient.php"><img src="assets/icons/Patient_add.png" title="Add Patient" style="float:right; padding-top:0.3em; padding-right:2em;"/></a>
                    <a href="dashboard.html"><img src="assets/icons/Home.png" title="Home" style="float:right; padding-top:0.3em; padding-right:2em;"/></a>

        </div>
        <!-- /Breadcrumbs line -->

                <div class="row">
                    <img src="assets/patient.gif" id="loader" style="position:absolute; left:50%; z-index:99999; top:35%; width:100px; height:100px;" />
                    <h2 style="color: #251367; margin-left:20px;">Add Patient</h2>
                    <div class="col-md-12" style="margin-top:20px;">
                    <div class="widget box" >
                        <div class="widget-content">
                            <div class="tabbable tabbable-custom tabs-left">
                                <!-- Only required for left/right tabs -->
                                <ul class="nav nav-tabs tabs-left">
                                    <li id="tab1" class="active"><a href="#tab_3_1" data-toggle="tab">Patient Info</a></li>
                                    <li id="tab2"><a href="#tab_3_2" data-toggle="tab">Insurance</a></li>
                                    <li id="tab3"><a href="#tab_3_3" data-toggle="tab">Accounts</a></li>
                                    <li id="tab4"><a href="#tab_3_4" data-toggle="tab">Alerts</a></li>
                                    <li id="tab5"><a href="#tab_3_5" data-toggle="tab">Documents</a></li>
                                    <li id="tab6"><a href="#tab_3_6" data-toggle="tab">Log</a></li>
                                    <li id="tab7"><a href="#tab_3_7" data-toggle="tab">Others</a></li>
                                </ul>
                                <div class="tab-content" style="background-color:aliceblue;">
                                    <div class="tab-pane active" id="tab_3_1">

                                        <div class="modal fade" id="NameModal" tabindex="-1">
                                                <div class="modal-dialog" style="width:40%">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                            <h4 class="modal-title">Check Full Name</h4>
                                                        </div>
                                                        <div class="modal-body" style="min-height:150px;">
                                                            <div class="form-group col-md-12" style="padding-bottom:5%;">
                                                        <label class="control-label col-md-6">Title </label>
                                                        <div class="col-md-6">
                                                            <select class="form-control">
                                                                <option></option>
                                                                <option>Mr.</option>
                                                                <option>Ms.</option>
                                                                <option>Mrs.</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-12" style="padding-bottom:5%;">
                                                        <label class="control-label col-md-6">First Name </label>
                                                        <div class="col-md-6">
                                                            <input type="text" class="form-control required" id="FirstName" name="txtFirstName" placeholder="Enter the First Name" />
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-12" style="padding-bottom:5%;">
                                                        <label class="control-label col-md-6">Middle Name </label>
                                                        <div class="col-md-6">
                                                            <input type="text" class="form-control required" id="MiddleName" name="txtMiddleName" placeholder="Enter the Middle Name" />
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-12" style="padding-bottom:5%;">
                                                        <label class="control-label col-md-6">Last Name </label>
                                                        <div class="col-md-6">
                                                            <input type="text" class="form-control required" id="LastName" name="txtLastName" placeholder="Enter the Last Name" />
                                                        </div>
                                                    </div>
                                                    </div>
                                                        <div class="modal-footer">
                                                            <input type="button" class="btn btn-default" data-dismiss="modal" value="Exit" />
                                                            <input type="button" id="btnSaveFullname" class="btn btn-primary" data-dismiss="modal" value="Save" />
                                                        </div>
                                                    </div><!-- /.modal-content -->
                                                </div><!-- /.modal-dialog -->
                                            </div>

                                            <div class="modal fade" id="upload" tabindex="-1">
                                                <div class="modal-dialog" style="width:40%">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                            <h4 class="modal-title">Upload File</h4>
                                                        </div>
                                                        <div class="modal-body" style="min-height:150px;">
                                                            <form action="UploadFile.php" method="post" enctype="multipart/form-data">
                                                                <div class="form-group col-md-12" style="padding-bottom:5%;">
                                                                    <input type="text" id="hdnValID" name="modalID" value="<?php echo $ID ?>" />
                                                                    <label class="control-label col-md-3">Upload File </label>
                                                                    <div class="col-md-8">
                                                                        <input type="file" class="form-control required" id="uploadFile" name="uploadFile"  />
                                                                        <input type="submit" value="Upload Image" name="submit">
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                        </div>
                                                    </div><!-- /.modal-content -->
                                                </div><!-- /.modal-dialog -->
                                            </div>

                                            <div class="modal fade" id="PrimaryPhysi" tab-index="-1">
                                                <div class="modal-dialog" style="width:70%">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                            <h4 class="modal-title">Add Primary Physician</h4>
                                                        </div>
                                                        <div class="modal-body" style="min-height:150px; padding-bottom:110px;">
                                                            <div class="form-group col-md-6">
                                                                <label class="control-label col-md-4">Full Name </label>
                                                                <div class="col-md-8">
                                                                    <input type="text" class="form-control required" id="PrimaryPhysiName" name="PrimaryPhysiName" placeholder="Name of the Primary Physiciam" value="" />
                                                                </div>
                                                            </div>

                                                            <div class="form-group col-md-6">
                                                                <label class="control-label col-md-4">Speciality </label>
                                                                <div class="col-md-8">
                                                                    <select class="form-control" id="phyEmploymentStatus">
                                                                        <option></option>
                                                                        <option>Psychologist</option>
                                                                        <option>Counselor</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="clearfix"></div>

                                                            <div class="form-group col-md-6">
                                                                <label class="control-label col-md-4">Date Of Birth </label>
                                                                <div class="col-md-8">
                                                                    <input type="text" class="form-control required" id="phyDOB" name="txtPTNumber" placeholder="Date Of Birth of the Primary Physiciam" value="" />
                                                                </div>
                                                            </div>

                                                            <div class="form-group col-md-6">
                                                                <label class="control-label col-md-4">Degree </label>
                                                                <div class="col-md-8">
                                                                    <input type="text" class="form-control required" id="phyDegree" name="txtPTNumber" placeholder="Degree of the Primary Physiciam" value="" />
                                                                </div>
                                                            </div>

                                                            <div class="form-group col-md-6">
                                                                <label class="control-label col-md-4">Individual NPI </label>
                                                                <div class="col-md-8">
                                                                    <input type="text" class="form-control required" id="phyNPI" name="txtPTNumber" placeholder="Individual NPI of the Primary Physiciam" value="" />
                                                                </div>
                                                            </div>

                                                            <div class="form-group col-md-6">
                                                                <label class="control-label col-md-4">Type </label>
                                                                <div class="col-md-8">
                                                                    <input type="text" class="form-control required" id="phyType" name="txtPTNumber" placeholder="Type of the Primary Physiciam" disabled value="" />
                                                                </div>
                                                            </div>

                                                            <div class="form-group col-md-6">
                                                                <label class="control-label col-md-4">Social Security # </label>
                                                                <div class="col-md-8">
                                                                    <input type="text" class="form-control required" id="phySSN" name="txtPTNumber" placeholder="SSN of the Primary Physiciam" value="" />
                                                                </div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <hr/>
                                                            <div class="form-group col-md-6">
                                                                <label class="control-label col-md-4">Address </label>
                                                                <div class="col-md-8">
                                                                    <textarea class="form-control" cols="5" rows="4" id="phyAddr"></textarea>
                                                                </div>
                                                            </div>

                                                            <div class="form-group col-md-6" >
                                                                <label class="control-label col-md-4">Home</label>
                                                                <div class="col-md-8">
                                                                     <input type="text" class="form-control required" id="phyHomePhone" name="txtPTNumber" placeholder="Enter the Home Phone" value="" />
                                                                </div>
                                                            </div>

                                                             <div class="form-group col-md-6">
                                                                <label class="control-label col-md-4">Work </label>
                                                                <div class="col-md-8">
                                                                    <input type="text" class="form-control required" id="phyWorkPhone" name="txtPTNumber" placeholder="Enter the Work Phone" value="" />
                                                                </div>
                                                            </div>

                                                            <div class="form-group col-md-6" >
                                                                <label class="control-label col-md-4">Mobile </label>
                                                                <div class="col-md-8">
                                                                    <input type="text" class="form-control required" id="phyMobilePhone" name="txtPTNumber" placeholder="Enter the Mobile Phone" value="" />
                                                                </div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <div class="form-group col-md-6">
                                                                <label class="control-label col-md-4">Email </label>
                                                                <div class="col-md-8">
                                                                    <input type="text" class="form-control required" id="phyEmail" name="txtPTNumber" placeholder="Enter the Email" value="" />
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <label class="control-label col-md-4">Fax </label>
                                                                <div class="col-md-8">
                                                                    <input type="text" class="form-control required" id="phyFax" name="txtPTNumber" placeholder="Enter the Fax" value="" />
                                                                </div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <hr/>
                                                            <div class="form-group col-md-12">
                                                                <label class="control-label col-md-2">Notes </label>
                                                                <div class="col-md-8">
                                                                    <textarea class="form-control" cols="20" rows="6" id="phyNotes"></textarea>
                                                                </div>
                                                            </div>

                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                            <input type="button" value="Add Primary Physician" class="btn btn-primary" id="btnAddPriPhy"/>
                                                        </div>
                                                    </div><!-- /.modal-content -->
                                                </div><!-- /.modal-dialog -->
                                            </div>

                                            <div class="modal fade" id="AddrModal">
                                                <div class="modal-dialog" style="width:40%">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                            <h4 class="modal-title">Check Address</h4>
                                                        </div>
                                                        <div class="modal-body" style="min-height:150px;">
                                                            <div class="form-group col-md-12" style="padding-bottom:5%;">
                                                                <label class="control-label col-md-6">First Name </label>
                                                                <div class="col-md-6">
                                                                    <input type="text" class="form-control required" id="FirstName" name="txtFirstName" placeholder="Enter the First Name" />
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-md-12" style="padding-bottom:5%;">
                                                                <label class="control-label col-md-6">Middle Name </label>
                                                                <div class="col-md-6">
                                                                    <input type="text" class="form-control required" id="MiddleName" name="txtMiddleName" placeholder="Enter the Middle Name" />
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-md-12" style="padding-bottom:5%;">
                                                                <label class="control-label col-md-6">Last Name </label>
                                                                <div class="col-md-6">
                                                                    <input type="text" class="form-control required" id="LastName" name="txtLastName" placeholder="Enter the Last Name" />
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-md-12" style="padding-bottom:5%;">
                                                                <label class="control-label col-md-6"></label>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                            <button type="button" class="btn btn-primary" data-dismiss="modal">Save changes</button>
                                                        </div>
                                                    </div><!-- /.modal-content -->
                                                </div><!-- /.modal-dialog -->
                                            </div>
                                        <form class="form-horizontal" id="UpdatePatientValues" method="post" >
                                            <input type="hidden" id="_hdnID" value="<?php echo $ID ?>" />
                                            <div class="tab-content">
                                                    <div class="tab-pane active" id="tab_1_1">
                                                        <div class="form-group col-md-6">
                                                <label class="control-label col-md-4">Full Name </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control validateGeneral" id="fullName" name="txtPTNumber" placeholder="Enter the Name of the Patient" />
                                                </div>
                                            </div>

                                            <div class="form-group col-md-6">
                                                <div class="col-md-1"></div>
                                                <div class="make-switch" data-on="success" data-on-label="Active" data-off-label="Inactive" data-off="warning">
                                                    <input type="checkbox" checked class="toggle"/>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>

                                            <div class="form-group col-md-6">
                                                <label class="control-label col-md-4">Social Security # </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control validateGeneral" id="ssn" name="txtPTNumber" data-mask="999-99-9999" placeholder="Enter the Social Security #" />
                                                </div>
                                            </div>

                                            <div class="form-group col-md-6">
                                                <label class="control-label col-md-4">Martial Status </label>
                                                <div class="col-md-8">
                                                    <select class="form-control" id="martialStatus">
                                                        <option value=""></option>
                                                        <option value="Annulled">Annulled</option>
                                                        <option value="Divorced">Divorced</option>
                                                        <option value="Interlocutory">Interlocutory</option>
                                                        <option value="LegallySeparated">LegallySeparated</option>
                                                        <option value="Married">Married</option>
                                                        <option value="Polygamous">Polygamous</option>
                                                        <option value="NeverMarried">NeverMarried</option>
                                                        <option value="DomesticPartner">DomesticPartner</option>
                                                        <option value="Widowed">Widowed</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group col-md-6">
                                                <label class="control-label col-md-4">Date Of Birth </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control validateGeneral" id="dob" name="txtPTNumber" placeholder="Enter the Date Of Birth" />
                                                </div>
                                            </div>

                                            <div class="form-group col-md-6">
                                                <label class="control-label col-md-4">Employment Status </label>
                                                <div class="col-md-8">
                                                    <select class="form-control" id="employment">
                                                        <option></option>
                                                        <option>Employed</option>
                                                        <option>Retired</option>
                                                        <option>Student. Full-time</option>
                                                        <option>Student. Part-time</option>
                                                        <option>Unknown</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group col-md-6">
                                                <label class="control-label col-md-4">Gender </label>
                                                <div class="col-md-8">
                                                    <select class="form-control" id="gender">
                                                        <option value="Male">Male</option>
                                                        <option value="Female">Female</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group col-md-6">
                                                <label class="control-label col-md-4">Employer </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" id="employer" name="employer" />
                                                </div>
                                            </div>

                                            <div class="form-group col-md-6">
                                                <label class="control-label col-md-4">Medical Record # </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" id="medicalRecord" name="txtPTNumber" placeholder="Enter the Client Code" />
                                                </div>
                                            </div>

                                            <div class="form-group col-md-6">
                                                <label class="control-label col-md-4">Referral Source </label>
                                                <div class="col-md-8">
                                                    <select class="form-control" id="reference">
                                                        <option value="0">Not Specified</option>
                                                        <option value="1">Attorney</option>
                                                        <option value="2">Brouchure</option>
                                                        <option value="3">Case Manager</option>
                                                        <option value="4">Chiropractor</option>
                                                        <option value="5">Emergency room</option>
                                                        <option value="6">Friend / Family</option>
                                                        <option value="7">Insurance</option>
                                                        <option value="8">Nurse</option>
                                                        <option value="9">Online Ad</option>
                                                        <option value="10">Other Source #1</option>
                                                        <option value="11">Other Source #2</option>
                                                        <option value="12">Other Source #3</option>
                                                        <option value="13">Other Source #4</option>
                                                        <option value="13">Other Source #5</option>
                                                        <option value="14">Patient Seminar</option>
                                                        <option value="15">Physical Therapist</option>
                                                        <option value="16">Physician</option>
                                                        <option value="17">Physician's Assistant</option>
                                                        <option value="18">Previous Patient</option>
                                                        <option value="19">Print Ad</option>
                                                        <option value="20">Radio Ad</option>
                                                        <option value="21">Search Engine</option>
                                                        <option value="22">Website</option>
                                                        <option value="23">Worker's Comp</option>
                                                        <option value="24">Yellow Pages</option>
                                                    </select>
                                                </div>
                                            </div>

                                        <div class="clearfix"></div>

                                                        <div class="form-group col-md-6">
                                            <label class="control-label col-md-6" style="text-align:left;"><span style="color:blue">Contact Information</span></label><hr>
                                        </div><hr>
                                        <div class="clearfix"></div>

                                        <div class="form-group col-md-6">
                                            <label class="control-label col-md-4">Street 1</label>
                                            <div class="col-md-8">
                                                 <input type="text" class="form-control required" id="addrStreet1" name="txtPTNumber" placeholder="Enter the Street address" value="" />
                                            </div>
                                        </div>

                                        <div class="form-group col-md-6">
                                            <label class="control-label col-md-4">Street 2</label>
                                            <div class="col-md-8">
                                                 <input type="text" class="form-control required" id="addrStreet2" name="txtPTNumber" placeholder="Enter the Street address" value="" />
                                            </div>
                                        </div>



                                        <div class="form-group col-md-6">
                                            <label class="control-label col-md-4">City </label>
                                            <div class="col-md-8">
                                                 <input type="text" class="form-control required" id="addrCity" name="txtPTNumber" placeholder="Enter the City" value="" />
                                            </div>
                                        </div>

                                        
                                        <div class="form-group col-md-6">
                                            <label class="control-label col-md-4">State </label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control required" id="addrState" name="txtPTNumber" placeholder="Enter the State" value="" />
                                            </div>
                                        </div>

                                        <div class="form-group col-md-6">
                                            <label class="control-label col-md-4">Zipcode </label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control required" id="addrZip" name="txtPTNumber" placeholder="Enter the Zipcode" value="" />
                                            </div>
                                        </div>
                                        
                                        <div class="form-group col-md-6">
                                            <label class="control-label col-md-4">Home Phone</label>
                                            <div class="col-md-8">
                                                 <input type="text" class="form-control required" id="phoneHome" name="txtPTNumber" placeholder="Enter the Home Phone" value="<?php echo $phoneHome;?>" />
                                            </div>
                                        </div>

                                        <div class="form-group col-md-6">
                                            <label class="control-label col-md-4">Work Phone </label>
                                            <div class="col-md-8">
                                                 <input type="text" class="form-control required" id="phoneWork" name="txtPTNumber" placeholder="Enter the Work Phone" value="<?php echo $phoneWork;?>" />
                                            </div>
                                        </div>

                                        <div class="form-group col-md-6">
                                            <label class="control-label col-md-4">Mobile Phone </label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control required" id="phoneMobile" name="txtPTNumber" placeholder="Enter the Mobile Phone" value="<?php echo $phoneMobile;?>" />
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>

                                        <div class="form-group col-md-6">

                                            <label class="checkbox-inline col-md-5">
                                                <input type="checkbox" class="uniform" style="margin-left:5px;" value="" id="notifyEmail"> &nbsp;&nbsp;Send Email Notification
                                            </label>
                                            <div class="col-md-7">
                                                <input type="text" class="form-control required" id="email" name="txtPTNumber" placeholder="Enter the Email Address" value="" />
                                            </div>

                                        </div>

                                        <div class="form-group col-md-6">
                                            <div class="col-md-1"></div>
                                            <label class="checkbox-inline col-md-8">
                                                <input type="checkbox" class="uniform" value="" id="notifyPhone"> Enable Auto Phone call Remainders
                                            </label>
                                        </div>

                                        <div class="clearfix"></div>
                                                    
                                                        <div class="form-group col-md-6">
                                            <label class="control-label col-md-6" style="text-align:left;"><span style="color:blue">Providers Information</span></label><hr>
                                        </div><hr>
                                        <div class="clearfix"></div>

                                        <div class="form-group col-md-6">
                                            <label class="control-label col-md-5">Primary Care Physician </label>
                                            <div class="col-md-6">
                                                <input type="text" class="form-control required" id="primaryCarePhy" name="PrimaryCarePhy" placeholder="Enter the Primary Care Physician" value="" />
                                            </div>
                                            
                                                <a href="#" id="linkPrimaryPhysi"><i class="icon icon-plus" style="font-size:20px; color:maroon"></i></a>
                                            
                                        </div>

                                        <div class="form-group col-md-6">
                                            <label class="control-label col-md-5">Default Rendering Provider</label>
                                            <div class="col-md-6">
                                                 <input type="text" class="form-control required" id="defaultRenderPhy" name="DRenderProvider" placeholder="Enter the Rendering Provider" value="" />
                                            </div>

                                                <a href="#" id="linkRenderPhysi"><i class="icon icon-plus" style="font-size:20px; color:maroon"></i></a>

                                        </div>


                                        <div class="form-group col-md-6">
                                            <label class="control-label col-md-5">Referring Physician </label>
                                            <div class="col-md-6">
                                                 <input type="text" class="form-control required" id="referringPhy" name="ReferPhy" placeholder="Enter the Referring Physician" value="" />
                                            </div>
                                            
                                                <a href="#" id="linkReferPhysi"><i class="icon icon-plus" style="font-size:20px; color:maroon"></i></a>
                                            
                                        </div>
                                        
                                        <div class="form-group col-md-6">
                                            <label class="control-label col-md-5">Default Service Location </label>
                                            <div class="col-md-6">
                                                <input type="text" class="form-control required" id="defaultServiceLoc" name="DServiceLoc" placeholder="Enter the Service Location" value="" />
                                            </div>
                                            
                                        </div>
                                        <div class="clearfix"></div>
                                                </div>
                                            </div>
                                            <div style="text-align: right">
                                                <input class="btn btn-primary" value="Add Patient" id="btnNext" type="button">
                                                <a href="#"><input type="button" class="btn btn-default" value="Cancel" /></a>
                                            </div>

                                    </div>
                                    <div class="tab-pane " id="tab_3_2" style="min-height:300px;">
                                        <a href="#" id="linkinsurance" class="btn btn-primary" style="text-decoration:none"><i class="icon icon-plus"></i>&nbsp;&nbsp;Create New Case</a>
                                    </div>
                                    <div class="tab-pane" id="tab_3_3">
                                        <table id="acc" class="table-bordered" style="width:100%">
                                    
                                </table>
                                    </div>
                                    <div class="tab-pane" id="tab_3_4">
                                        <div class="form-group col-md-6">
                                            <label class="control-label col-md-6" style="text-align:left;">Alert Message :</label>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-group col-md-12">
                                            <div class="col-md-12">
                                                <textarea class="form-control" id="alertMsg" rows="5" cols="5"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-12">
                                            <u>Show Alert Message when :</u>
                                        </div>
                                        <div class="col-md-10">
                                            <label class="checkbox">
                                                <input type="checkbox" class="uniform" value=""> Displaying Patient Details
                                            </label>
                                            <label class="checkbox">
                                                <input type="checkbox" class="uniform" value=""> Scheduling Appointments
                                            </label>
                                            <label class="checkbox">
                                                <input type="checkbox" class="uniform" value=""> Entering Encounters
                                            </label>
                                            <label class="checkbox">
                                                <input type="checkbox" class="uniform" value=""> Viewing Claim Details
                                            </label>
                                            <label class="checkbox">
                                                <input type="checkbox" class="uniform" value=""> Posting Payments
                                            </label>
                                            <label class="checkbox">
                                                <input type="checkbox" class="uniform" value=""> Preparing Patient Statements
                                            </label>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tab_3_5">
                                        <div class="form-group col-md-12">
                                        <label class="checkbox">
                                                <input type="checkbox" class="uniform" value=""> Show documents attached to all records associated with this patient
                                        </label>
                                        </div>
                                            <table id="test" class="table table-striped table-bordered table-hover table-checkable datatable">
                                            <thead>
                                                <tr>
                                                    <th style="text-align:center; background-color:#4583e7; color:white; width:50px;">ID #</th>
                                                    <th style="text-align:center; background-color:#4583e7; color:white;">Created</th>
                                                    <th style="text-align:center; background-color:#4583e7; color:white;">Label</th>
                                                    <th style="text-align:center; background-color:#4583e7; color:white;">Name</th>
                                                    <th style="text-align:center; background-color:#4583e7; color:white;" >Type</th>
                                                    <th style="text-align:center; background-color:#4583e7; color:white;">Active</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td><span class="label label-success">Active</span></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <a href="#" id="linkattachatt">Attach</a>
                                    </div>
                                    <div class="tab-pane" id="tab_3_6">
        
                                    </div>
                                    <div class="tab-pane" id="tab_3_7">
                                        <div class="clearfix"></div>
                                        <div class="form-group col-md-6">
                                            <label class="control-label col-md-6" style="text-align:left;"><span style="color:blue">Guarantor<span></label>
                                        </div>
                                        <div class="clearfix"></div>

                                        <div class="form-group col-md-12">
                                            <div class="col-md-1"></div>
                                            <label class="checkbox-inline col-md-8">
                                                <input type="checkbox" class="uniform" value="" id="IsFinancialResp"> Personally Financially responsible(a.k.a. Guarantor) is different than Patient
                                            </label>
                                        </div>
                                        <div class="clearfix"></div><br/>
                                        <div class="form-group col-md-6">
                                            <label class="control-label col-md-6" style="text-align:left;"><span style="color:blue">Insurance</span></label>
                                        </div>
                                        <div class="clearfix"></div>

                                        <div class="form-group col-md-10">
                                            <label class="control-label col-md-3">Default Payer Scenario </label>
                                            <div class="col-md-8">
                                                <select class="form-control" id="DPayerScenario">
                                                    <option></option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div><br/>
                                                        <div class="form-group col-md-6">
                                            <label class="control-label col-md-6" style="text-align:left;"><span style="color:blue">Notes</span></label>
                                        </div>
                                        <div class="clearfix"></div>

                                        <div class="form-group col-md-12">
                                            <div class="col-md-12">
                                                <textarea class="form-control" id="Notes"><?php echo $Notes;?></textarea>
                                            </div>
                                        </div>
                                                </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
                
            </div>
            <!-- /.container -->

        </div>
    </div>
    <div class="modal fade" id="insurance" tabindex="-1">
        <div class="modal-dialog" style="width:75%">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Add New Case</h4>
                </div>
                <div class="modal-body">
                    <div class="tabbable tabbable-custom tabs-left">
                        <!-- Only required for left/right tabs -->
                        <ul class="nav nav-tabs tabs-left">
                            <li><a href="#tab_3_2_1" data-toggle="tab">Personal</a></li>
                            <li><a href="#tab_3_2_2" data-toggle="tab">Account</a></li>
                            <li><a href="#tab_3_2_3" data-toggle="tab">Diagnosis</a></li>
                            <li><a href="#tab_3_2_4" data-toggle="tab">Policy 1</a></li>
                            <li><a href="#tab_3_2_5" data-toggle="tab">Policy 2</a></li>
                            <li><a href="#tab_3_2_6" data-toggle="tab">Condition</a></li>
                            <li><a href="#tab_3_2_7" data-toggle="tab">Misc</a></li>
                            <li><a href="#tab_3_2_8" data-toggle="tab">Medicaid &amp;<br/> Tricare</a></li>
                            <!--<li><a href="#tab_3_2_9" data-toggle="tab">Multimedia</a></li>
                            <li><a href="#tab_3_2_10" data-toggle="tab">Comment</a></li>-->
                            <li><a href="#tab_3_2_11" data-toggle="tab">EDI</a></li>
                        </ul>
                        <div class="tab-content" style="background-color:aliceblue;">
                            <div class="tab-pane active" id="tab_3_2_1">
                                <input type="hidden" id="hdnPatientID" />
                                <input type="hidden" id="hdntoggleDX" />
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-4">Case # </label>
                                    <div class="col-md-8">
                                        <label class="col-md-12" id="lblCaseNo"></label>
                                    </div>
                                </div>
                                
                                <div class="form-group col-md-6">
                                   <label class="checkbox-inline col-md-5">
                                    <input type="checkbox" class="uniform" style="margin-left:5px;" value="" id="IsClosed"> &nbsp;&nbsp;Case Closed
                                </label>
                                </div>
                                <div class="clearfix"></div>
                                <hr>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-4">Description </label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control required" id="description" />
                                    </div>
                                </div>
                                
                                <div class="form-group col-md-6">
                                   <label class="checkbox-inline col-md-12">
                                    <input type="checkbox" class="uniform" style="margin-left:5px;" value="" id="IsCash"> &nbsp;&nbsp;Cash Case
                                </label>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-4">Global Coverage Until </label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control " id="globalCoverageUntil" />
                                    </div>
                                </div>
                                
                                <div class="form-group col-md-6">
                                   <label class="checkbox-inline col-md-12">
                                    <input type="checkbox" class="uniform" style="margin-left:5px;" value="" id="allowPrint"> &nbsp;&nbsp;Print Patient Statement
                                </label>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-4">Gurantor </label>
                                    <div class="col-md-8">
                                        <select class="form-control" id="guarantor">
                                            <option></option>
                                            <option>Niyaz</option>
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-4">Martial Status </label>
                                    <div class="col-md-8">
                                        <select class="form-control" id="caseMartialStatus">
                                            <option value=""></option>
                                            <option value="Annulled">Annulled</option>
                                            <option value="Divorced">Divorced</option>
                                            <option value="Interlocutory">Interlocutory</option>
                                            <option value="LegallySeparated">LegallySeparated</option>
                                            <option value="Married">Married</option>
                                            <option value="Polygamous">Polygamous</option>
                                            <option value="NeverMarried">NeverMarried</option>
                                            <option value="DomesticPartner">DomesticPartner</option>
                                            <option value="Widowed">Widowed</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-4">Student Status </label>
                                    <div class="col-md-8">
                                        <select class="form-control" id="studStatus">
                                            <option></option>
                                            <option>Non-Student</option>
                                            <option>Full-time</option>
                                            <option>Part-time</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                                
                                <hr>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-4">Employer </label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control " id="employerCase" name="employerCase" />
                                    </div>
                                </div>
                                <!-- <div class="form-group col-md-6">
                                    
                                    <label class="col-md-12">Vivek</label>
                                </div> -->
                                <div class="clearfix"></div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-4">Status </label>
                                    <div class="col-md-8">
                                        <select class="form-control" id="empstatus">
                                            <option></option>
                                            <option>Employed</option>
                                            <option>Retired</option>
                                            <option>Student. Full-time</option>
                                            <option>Student. Part-time</option>
                                            <option>Unknown</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-4">Retirement Date </label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control " id="retirementDate" />
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-4">Work Phone </label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" id="workPhoneCase"/>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-4">Location </label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" id="location"/>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-4">Extension </label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" id="extension"/>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane " id="tab_3_2_2">
                                <div class="form-group col-md-8">
                                    <label class="control-label col-md-6">Assigned Provider </label>
                                    <div class="col-md-6">
                                       <input type="text" class="form-control" id="assignedProvider" name="assignedProvider" />
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group col-md-8">
                                    <label class="control-label col-md-6">Referring Provider </label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="referringProvider" name="referringProvider" />
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group col-md-8">
                                    <label class="control-label col-md-6">Supervising Provider </label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="supervisingProvider" name="supervisingProvider" />
                                    </div>
                                </div>
                                
                                <div class="clearfix"></div>
                                <div class="form-group col-md-8">
                                    <label class="control-label col-md-6">Operating Provider </label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="operatingProvider" name="operatingProvider" />
                                    </div>
                                </div>
                               
                                <div class="clearfix"></div>
                                <div class="form-group col-md-8">
                                    <label class="control-label col-md-6">Other Provider </label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="otherProvider" name="otherProvider" />
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                                <div class="form-group col-md-8">
                                    <label class="control-label col-md-6">Referral Source </label>
                                    <div class="col-md-6">
                                         <select class="form-control" id="referralSource">
                                            <option value="0">Not Specified</option>
                                            <option value="1">Attorney</option>
                                            <option value="2">Brouchure</option>
                                            <option value="3">Case Manager</option>
                                            <option value="4">Chiropractor</option>
                                            <option value="5">Emergency room</option>
                                            <option value="6">Friend / Family</option>
                                            <option value="7">Insurance</option>
                                            <option value="8">Nurse</option>
                                            <option value="9">Online Ad</option>
                                            <option value="10">Other Source #1</option>
                                            <option value="11">Other Source #2</option>
                                            <option value="12">Other Source #3</option>
                                            <option value="13">Other Source #4</option>
                                            <option value="13">Other Source #5</option>
                                            <option value="14">Patient Seminar</option>
                                            <option value="15">Physical Therapist</option>
                                            <option value="16">Physician</option>
                                            <option value="17">Physician's Assistant</option>
                                            <option value="18">Previous Patient</option>
                                            <option value="19">Print Ad</option>
                                            <option value="20">Radio Ad</option>
                                            <option value="21">Search Engine</option>
                                            <option value="22">Website</option>
                                            <option value="23">Worker's Comp</option>
                                            <option value="24">Yellow Pages</option>
                                        </select>
                                    </div>
                                </div>
                               
                                <div class="clearfix"></div>
                                <div class="form-group col-md-8">
                                    <label class="control-label col-md-6">Attorney </label>
                                    <div class="col-md-6">
                                        <select class="form-control" id="attorney">
                                            <option></option>
                                            <option>Niyaz</option>
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="clearfix"></div>
                                <div class="form-group col-md-8">
                                    <label class="control-label col-md-6">Facility </label>
                                    <div class="col-md-6">
                                        <select class="form-control" id="facility">
                                            <option></option>
                                            <option>Niyaz</option>
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <hr>
                                <div class="form-group col-md-8">
                                    <label class="control-label col-md-6">Case Billing Code </label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="caseBillingCode" />
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group col-md-8">
                                    <label class="control-label col-md-6">Price Code </label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control required" id="priceCode" />
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group col-md-8">
                                    <label class="control-label col-md-6">Other Arrangements </label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control required" id="otherArrangements" name="txtPTNumber" placeholder="Enter the Mobile Phone" value="<?php echo $MobilePhone;?>" />
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <hr>
                                <div class="form-group col-md-8">
                                    <label class="control-label col-md-6">Treatment Authorized through </label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="authorizedThrough" />
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <hr>
                                <div class="form-group col-md-8">
                                    <label class="control-label col-md-6">Authorization # </label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control required" id="authNo" />
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group col-md-8">
                                    <label class="control-label col-md-6">Last Visit Date</label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="lastVisit" />
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group col-md-8">
                                    <label class="control-label col-md-6">Authorization # of Visits </label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control required" id="noOfVisits" />
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group col-md-8">
                                    <label class="control-label col-md-6">ID </label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control required" id="authID" />
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group col-md-8">
                                    <label class="control-label col-md-6">Last Visit # </label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control required" id="lastVisitNo" />
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="tab-pane" id="tab_3_2_3">
                                <div class="form-group col-md-6">
                                    <div class="col-md-1"></div>
                                    <div class="make-switch" data-on="success" data-on-label="ICD-9" data-off-label="ICD-10" data-off="warning">
                                        <input type="checkbox" checked class="toggle" id="icdtoggle" />
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-6">Principal Diagnosis </label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control icdDX" id="principalDiag" name="principalDiag" />
                                    </div>
                                </div>
                                <div class="form-group col-md-3">
                                </div>
                                <div class="form-group col-md-3">
                                    <label class="control-label col-md-4">POA</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control required" id="poa1" />
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-6">Default Diagnosis 2</label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control icdDX" id="defDiag2" name="defDiag2" />
                                    </div>
                                </div>
                                <div class="form-group col-md-3">
                                </div>
                                <div class="form-group col-md-3">
                                    <label class="control-label col-md-4">POA</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control required" id="poa2" />
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-6">Default Diagnosis 3</label>
                                    <div class="col-md-6">
                                       <input type="text" class="form-control icdDX" id="defDiag3" name="defDiag3" />
                                    </div>
                                </div>
                                <div class="form-group col-md-3">
                                </div>
                                <div class="form-group col-md-3">
                                    <label class="control-label col-md-4">POA</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control required" id="poa3" />
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-6">Default Diagnosis 4</label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control icdDX" id="defDiag4" name="defDiag4" />
                                    </div>
                                </div>
                                <div class="form-group col-md-3">
                                   
                                </div>
                                <div class="form-group col-md-3">
                                    <label class="control-label col-md-4">POA</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control required" id="poa4" />
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                                <hr>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-6">Allergies &amp; Notes 4</label>
                                    <textarea class="form-control" rows="5" id="allergiesNotes"></textarea>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-6">EDI Notes</label>
                                    <textarea class="form-control" rows="5" id="ediNotes"></textarea>
                                </div>
                                <div class="clearfix"></div>
                                <hr>
                                <h5>EDI Report</h5>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-6">Report Type Code </label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="repTypeCode" />
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-6">Report Transmission Code </label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="repTransCode" />
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-6">Attachment Control # </label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="attachCtrlNo" />
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="tab_3_2_4">
                                <!--<div class="tabbable tabbable-custom">
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a href="#tab_1_1" data-toggle="tab">Case 1</a></li>
                                        <li><a href="#tab_1_2" data-toggle="tab">Case 2</a></li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="tab_1_1">
                                            <div class="form-group col-md-6">
                                            <div class="panel-group" id="accordion">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h3 class="panel-title">
                                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                                        MEDICARE - Policy # : 12354856 </a>
                                                        </h3>
                                                    </div>
                                                    <div id="collapseOne" class="panel-collapse collapse in">
                                                        <div class="panel-body">
                                                            <div class="form-group col-md-12" style="margin-bottom:30px;">
                                                                <label class="control-label col-md-4">Payer Name </label>
                                                                <div class="col-md-8">
                                                                    <input type="text" class="form-control required" id="Name" name="txtPTNumber" placeholder="Enter the Name of the Patient" disabled value="<?php echo $Name; ?>" />
                                                                </div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                             <div class="form-group col-md-12" style="margin-bottom:60px;">
                                                                <label class="control-label col-md-4">Address</label>
                                                                <div class="col-md-8">
                                                                     <textarea class="form-control"></textarea>
                                                                </div>
                                                            </div>

                                                             <div class="form-group col-md-12" style="margin-bottom:30px;">
                                                                <label class="control-label col-md-4">Phone</label>
                                                                <div class="col-md-8">
                                                                     <input type="text" class="form-control required" id="HomePhone" name="txtPTNumber" placeholder="Enter the Home Phone" value="<?php echo $HomePhone;?>" />
                                                                </div>
                                                            </div>

                                                             <div class="form-group col-md-12" style="margin-bottom:30px;">
                                                                <label class="control-label col-md-4">Fax </label>
                                                                <div class="col-md-8">
                                                                    <input type="text" class="form-control required" id="MobilePhone" name="txtPTNumber" placeholder="Enter the Mobile Phone" value="<?php echo $MobilePhone;?>" />
                                                                </div>
                                                            </div>

                                                            <div class="form-group col-md-12" style="margin-bottom:30px;">
                                                                <label class="control-label col-md-4">Copay </label>
                                                                <div class="col-md-8">
                                                                    <input type="text" class="form-control required" id="DOB" name="txtPTNumber" placeholder="Enter the Date Of Birth" value="<?php echo $DOB;?>" />
                                                                </div>
                                                            </div>

                                                            <div class="form-group col-md-12" style="margin-bottom:30px;">
                                                                <label class="control-label col-md-4">Deductible </label>
                                                                <div class="col-md-8">
                                                                    <input type="text" class="form-control required" id="DOB" name="txtPTNumber" placeholder="Enter the Date Of Birth" value="<?php echo $DOB;?>" />
                                                                </div>
                                                            </div>

                                                             <div class="form-group col-md-12" style="margin-bottom:30px;">
                                                                <label class="control-label col-md-4">Effective Start </label>
                                                                <div class="col-md-8">
                                                                    <input type="text" class="form-control required" id="DOB" name="txtPTNumber" placeholder="Enter the Date Of Birth" value="<?php echo $DOB;?>" />
                                                                </div>
                                                            </div>

                                                            <div class="form-group col-md-12" style="margin-bottom:30px;">
                                                                <label class="control-label col-md-4">Effective End </label>
                                                                <div class="col-md-8">
                                                                    <input type="text" class="form-control required" id="DOB" name="txtPTNumber" placeholder="Enter the Date Of Birth" value="<?php echo $DOB;?>" />
                                                                </div>
                                                            </div>

                                                             <div class="form-group col-md-12" style="margin-bottom:30px;">
                                                                <label class="control-label col-md-4">Policy # </label>
                                                                <div class="col-md-8">
                                                                    <input type="text" class="form-control required" id="DOB" name="txtPTNumber" placeholder="Enter the Date Of Birth" value="<?php echo $DOB;?>" />
                                                                </div>
                                                            </div>

                                                             <div class="form-group col-md-12" style="margin-bottom:30px;">
                                                                <label class="control-label col-md-4">Group # </label>
                                                                <div class="col-md-8">
                                                                    <input type="text" class="form-control required" id="DOB" name="txtPTNumber" placeholder="Enter the Date Of Birth" value="<?php echo $DOB;?>" />
                                                                </div>
                                                            </div>

                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h3 class="panel-title">
                                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                                        Secondary Insurance </a>
                                                        </h3>
                                                    </div>
                                                    <div id="collapseTwo" class="panel-collapse collapse">
                                                        <div class="panel-body">
                                                             Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h3 class="panel-title">
                                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                                        Worker Group / No Fault / Automobile </a>
                                                        </h3>
                                                    </div>
                                                    <div id="collapseThree" class="panel-collapse collapse">
                                                        <div class="panel-body">
                                                              <div class="form-group col-md-12" style="margin-bottom:30px;">
                                                                <label class="control-label col-md-4">Payer Name </label>
                                                                <div class="col-md-8">
                                                                    <input type="text" class="form-control required" id="Name" name="txtPTNumber" placeholder="Enter the Name of the Patient" disabled value="<?php echo $Name; ?>" />
                                                                </div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                             <div class="form-group col-md-12" style="margin-bottom:60px;">
                                                                <label class="control-label col-md-4">Address</label>
                                                                <div class="col-md-8">
                                                                     <textarea class="form-control"></textarea>
                                                                </div>
                                                            </div>

                                                             <div class="form-group col-md-12" style="margin-bottom:30px;">
                                                                <label class="control-label col-md-4">Phone</label>
                                                                <div class="col-md-8">
                                                                     <input type="text" class="form-control required" id="HomePhone" name="txtPTNumber" placeholder="Enter the Home Phone" value="<?php echo $HomePhone;?>" />
                                                                </div>
                                                            </div>

                                                             <div class="form-group col-md-12" style="margin-bottom:30px;">
                                                                <label class="control-label col-md-4">Fax </label>
                                                                <div class="col-md-8">
                                                                    <input type="text" class="form-control required" id="MobilePhone" name="txtPTNumber" placeholder="Enter the Mobile Phone" value="<?php echo $MobilePhone;?>" />
                                                                </div>
                                                            </div>

                                                            <div class="form-group col-md-12" style="margin-bottom:30px;">
                                                                <label class="control-label col-md-4">Copay </label>
                                                                <div class="col-md-8">
                                                                    <input type="text" class="form-control required" id="DOB" name="txtPTNumber" placeholder="Enter the Date Of Birth" value="<?php echo $DOB;?>" />
                                                                </div>
                                                            </div>

                                                            <div class="form-group col-md-12" style="margin-bottom:30px;">
                                                                <label class="control-label col-md-4">Deductible </label>
                                                                <div class="col-md-8">
                                                                    <input type="text" class="form-control required" id="DOB" name="txtPTNumber" placeholder="Enter the Date Of Birth" value="<?php echo $DOB;?>" />
                                                                </div>
                                                            </div>

                                                             <div class="form-group col-md-12" style="margin-bottom:30px;">
                                                                <label class="control-label col-md-4">Effective Start </label>
                                                                <div class="col-md-8">
                                                                    <input type="text" class="form-control required" id="DOB" name="txtPTNumber" placeholder="Enter the Date Of Birth" value="<?php echo $DOB;?>" />
                                                                </div>
                                                            </div>

                                                            <div class="form-group col-md-12" style="margin-bottom:30px;">
                                                                <label class="control-label col-md-4">Effective End </label>
                                                                <div class="col-md-8">
                                                                    <input type="text" class="form-control required" id="DOB" name="txtPTNumber" placeholder="Enter the Date Of Birth" value="<?php echo $DOB;?>" />
                                                                </div>
                                                            </div>

                                                             <div class="form-group col-md-12" style="margin-bottom:30px;">
                                                                <label class="control-label col-md-4">Policy # </label>
                                                                <div class="col-md-8">
                                                                    <input type="text" class="form-control required" id="DOB" name="txtPTNumber" placeholder="Enter the Date Of Birth" value="<?php echo $DOB;?>" />
                                                                </div>
                                                            </div>

                                                             <div class="form-group col-md-12" style="margin-bottom:30px;">
                                                                <label class="control-label col-md-4">Group # </label>
                                                                <div class="col-md-8">
                                                                    <input type="text" class="form-control required" id="DOB" name="txtPTNumber" placeholder="Enter the Date Of Birth" value="<?php echo $DOB;?>" />
                                                                </div>
                                                            </div>

                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                            <div class="form-group col-md-6">
                                                <div class="form-group col-md-12">
                                                    <label class="checkbox-inline col-md-12">
                                                        <input type="checkbox" class="uniform" value="" id="IsInsured"> Insured Person available
                                                    </label>
                                                </div>
                                                <div class="form-group col-md-12" id="divInsured">
                                                    <div class="form-group col-md-12" style="margin-bottom:30px;">
                                                                    <label class="control-label col-md-4">Relationship </label>
                                                                    <div class="col-md-8">
                                                                        <select class="form-control"><option>Spouse</option><option>Child</option><option>Other</option></select>
                                                                    </div>
                                                                </div>
                                                    <div class="form-group col-md-12" style="margin-bottom:30px;">
                                                                    <label class="control-label col-md-4">Full Name </label>
                                                                    <div class="col-md-8">
                                                                        <input type="text" class="form-control required" id="Name" name="txtPTNumber" placeholder="Enter the Name of the Patient" disabled value="<?php echo $Name; ?>" />
                                                                    </div>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                                 <div class="form-group col-md-12" style="margin-bottom:60px;">
                                                                    <label class="control-label col-md-4">Address</label>
                                                                    <div class="col-md-8">
                                                                         <textarea class="form-control"></textarea>
                                                                    </div>
                                                                </div>

                                                                 <div class="form-group col-md-12" style="margin-bottom:30px;">
                                                                    <label class="control-label col-md-4">Insured ID No</label>
                                                                    <div class="col-md-8">
                                                                         <input type="text" class="form-control required" id="HomePhone" name="txtPTNumber" placeholder="Enter the Home Phone" value="<?php echo $HomePhone;?>" />
                                                                    </div>
                                                                </div>

                                                                 <div class="form-group col-md-12" style="margin-bottom:30px;">
                                                                    <label class="control-label col-md-4">SSN </label>
                                                                    <div class="col-md-8">
                                                                        <input type="text" class="form-control required" id="MobilePhone" name="txtPTNumber" placeholder="Enter the Mobile Phone" value="<?php echo $MobilePhone;?>" />
                                                                    </div>
                                                                </div>

                                                                 <div class="form-group col-md-12" style="margin-bottom:30px;">
                                                                    <label class="control-label col-md-4">Date Of Birth </label>
                                                                    <div class="col-md-8">
                                                                        <input type="text" class="form-control required" id="DOB" name="txtPTNumber" placeholder="Enter the Date Of Birth" value="<?php echo $DOB;?>" />
                                                                    </div>
                                                                </div>

                                                                 <div class="form-group col-md-12" style="margin-bottom:30px;">
                                                                    <label class="control-label col-md-4">Gender </label>
                                                                    <div class="col-md-8">
                                                                        <select class="form-control"><option>Male</option><option>Female</option></select>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group col-md-12" style="margin-bottom:30px;">
                                                                    <label class="control-label col-md-4">Active </label>
                                                                    <div class="col-md-8">
                                                                        <div class="make-switch" data-on="success" data-on-label="Active" data-off-label="Inactive" data-off="warning">
                                                                            <input type="checkbox" checked class="toggle"/>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane active" id="tab_1_2">
                                        </div>
                                    </div>
                                </div>-->
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-6">Insurance 1 </label>
                                    <div class="col-md-6">
                                        <input type="text" id="policyInsurance1" class="form-control"/>
                                    </div>
                                </div>
                                <!-- <div class="form-group col-md-6">
                                    
                                    <label class="col-md-12">Diagnosis 1</label>
                                </div> -->
                                <div class="clearfix"></div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-6">Policy Holder 1 </label>
                                    <div class="col-md-6">
                                        <input type="text" id="policyHolder1" class="form-control"/>
                                    </div>
                                </div>
                                <!-- <div class="form-group col-md-6">
                                    
                                    <label class="col-md-12">Diagnosis 1</label>
                                </div> -->
                                <div class="clearfix"></div>
                                <div class="form-group col-md-12" style="padding-bottom:25px;">
                                    <label class="control-label col-md-3">Relationship to Insured </label>
                                    <div class="col-md-3">
                                        <select class="form-control" id="policyRelationInsured1">
                                            <option>Self</option>
                                            <option>Spouse</option>
                                            <option>Other</option>
                                            <option>Child</option>
                                            <option>Grandfather Or Grandmother</option>
                                            <option>Grandson Or Granddaughter</option>
                                            <option>Nephew Or Niece</option>
                                            <option>Adopted Child</option>
                                            <option>Foster Child</option>
                                            <option>Stepson</option>
                                            <option>Ward</option>
                                            <option>Stepdaughter</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-6">Policy #</label>
                                    <div class="col-md-6">
                                         <input type="text" class="form-control" id="policyNo1" />
                                    </div>
                                </div>


                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-6">Policy Start Date </label>
                                    <div class="col-md-6">
                                         <input type="text" class="form-control" id="policyStartDt1" />
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-6">Group #</label>
                                    <div class="col-md-6">
                                         <input type="text" class="form-control" id="policyGroupNo1" />
                                    </div>
                                </div>


                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-6">Policy End Date </label>
                                    <div class="col-md-6">
                                         <input type="text" class="form-control" id="policyEndDt1" />
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-6">Claim # </label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="policyClaimNo1" />
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div id="insuredDiv">
                                    <div class="form-group col-md-6">
                                            <label class="control-label col-md-6" style="text-align:left;"><span style="color:blue">Insurer Information</span></label>
                                        </div>
                                        <div class="clearfix"></div>
                                        <hr style="margin-top:0;">
                                    <div class="form-group col-md-6">
                                                <label class="control-label col-md-4">Full Name </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" id="INSfullName" name="txtPTNumber" placeholder="Enter the Name of the Patient" />
                                                </div>
                                            </div>

                                            <div class="form-group col-md-6">
                                                <label class="control-label col-md-4">Social Security # </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" id="INSssn" name="txtPTNumber" data-mask="999-99-9999" placeholder="Enter the Social Security #" />
                                                </div>
                                            </div>

                                            
                                            <div class="form-group col-md-6">
                                                <label class="control-label col-md-4">Date Of Birth </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" id="INSdob" name="txtPTNumber" placeholder="Enter the Date Of Birth" />
                                                </div>
                                            </div>

                                           

                                            <div class="form-group col-md-6">
                                                <label class="control-label col-md-4">Gender </label>
                                                <div class="col-md-8">
                                                    <select class="form-control" id="INSgender">
                                                        <option value="Male">Male</option>
                                                        <option value="Female">Female</option>
                                                    </select>
                                                </div>
                                            </div>

                                        <div class="clearfix"></div>

                                        <div class="form-group col-md-6">
                                            <label class="control-label col-md-4">Street 1</label>
                                            <div class="col-md-8">
                                                 <input type="text" class="form-control required" id="INSaddrStreet1" name="txtPTNumber" placeholder="Enter the Street address" value="" />
                                            </div>
                                        </div>

                                        <div class="form-group col-md-6">
                                            <label class="control-label col-md-4">Street 2</label>
                                            <div class="col-md-8">
                                                 <input type="text" class="form-control required" id="INSaddrStreet2" name="txtPTNumber" placeholder="Enter the Street address" value="" />
                                            </div>
                                        </div>



                                        <div class="form-group col-md-6">
                                            <label class="control-label col-md-4">City </label>
                                            <div class="col-md-8">
                                                 <input type="text" class="form-control required" id="INSaddrCity" name="txtPTNumber" placeholder="Enter the City" value="" />
                                            </div>
                                        </div>

                                        
                                        <div class="form-group col-md-6">
                                            <label class="control-label col-md-4">State </label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control required" id="INSaddrState" name="txtPTNumber" placeholder="Enter the State" value="" />
                                            </div>
                                        </div>

                                        <div class="form-group col-md-6">
                                            <label class="control-label col-md-4">Zipcode </label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control required" id="INSaddrZip" name="txtPTNumber" placeholder="Enter the Zipcode" value="" />
                                            </div>
                                        </div>
                                        
                                        <div class="form-group col-md-6">
                                            <label class="control-label col-md-4">Home Phone</label>
                                            <div class="col-md-8">
                                                 <input type="text" class="form-control required" id="INSphoneHome" name="txtPTNumber" placeholder="Enter the Home Phone" />
                                            </div>
                                        </div>

                                        <div class="form-group col-md-6">
                                            <label class="control-label col-md-4">Work Phone </label>
                                            <div class="col-md-8">
                                                 <input type="text" class="form-control required" id="INSphoneWork" name="txtPTNumber" placeholder="Enter the Work Phone" />
                                            </div>
                                        </div>

                                        <div class="form-group col-md-6">
                                            <label class="control-label col-md-4">Mobile Phone </label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control required" id="INSphoneMobile" name="txtPTNumber" placeholder="Enter the Mobile Phone" />
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                </div>
                                <hr>
                                <div class="form-group col-md-6">
                                    <label class="checkbox col-md-8">
                                        <input type="checkbox" class="uniform" value="" id="policyAssignBenefits1"> Assignment of Benefits/Accept Assignment
                                    </label>

                                    <label class="checkbox col-md-8">
                                        <input type="checkbox" class="uniform" value="" id="policyDeduc1"> Deductible Met
                                    </label>

                                     <label class="checkbox col-md-8">
                                        <input type="checkbox" class="uniform" value="" id="policyCapitatedPlan1"> 
                                        Capitated Plan
                                    </label>
                                </div>
                                
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-6">Annual Deductible </label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="policyAnnualDedu1" value="0.00" />
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-6">Copayment Amount </label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="policyCopay1" value="0.00" />
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <hr>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-6">Treatment Authorization </label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="policyTreatAuth1" />
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-6">Document Control # </label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="policyDocControl1" />
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <hr>
                                <div class="form-group col-md-4">
                                    <label class="control-label col-md-8">Insurance Coverage Percents by Service Classification</label>
                                </div>
                                <div class="form-group col-md-4">
                                    <label class="control-label col-md-2">A </label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" id="policyA1" />
                                    </div>
                                    <label class="control-label col-md-2">C </label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" id="policyC1" />
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <label class="control-label col-md-2">E </label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" id="policyE1" />
                                    </div>
                                    <label class="control-label col-md-2">G </label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" id="policyG1" />
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <label class="control-label col-md-2">B </label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" id="policyB1" />
                                    </div>
                                    <label class="control-label col-md-2">D </label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" id="policyD1" />
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <label class="control-label col-md-2">F </label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" id="policyF1" />
                                    </div>
                                    <label class="control-label col-md-2">H </label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" id="policyH1" />
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="tab_3_2_5">
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-6">Insurance 2 </label>
                                    <div class="col-md-6">
                                        <input type="text" id="policyInsurance2" class="form-control"/>
                                    </div>
                                </div>
                                <!-- <div class="form-group col-md-6">
                                    
                                    <label class="col-md-12">Diagnosis 1</label>
                                </div> -->
                                <div class="clearfix"></div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-6">Policy Holder 2 </label>
                                    <div class="col-md-6">
                                        <input type="text" id="policyHolder2" class="form-control"/>
                                    </div>
                                </div>
                                <!-- <div class="form-group col-md-6">
                                    
                                    <label class="col-md-12">Diagnosis 1</label>
                                </div> -->
                                <div class="clearfix"></div>
                                <div class="form-group col-md-12" style="padding-bottom:25px;">
                                    <label class="control-label col-md-3">Relationship to Insured </label>
                                    <div class="col-md-3">
                                        <select class="form-control" id="policyRelationInsured2">
                                            <option>Self</option>
                                            <option>Spouse</option>
                                            <option>Other</option>
                                            <option>Child</option>
                                            <option>Grandfather Or Grandmother</option>
                                            <option>Grandson Or Granddaughter</option>
                                            <option>Nephew Or Niece</option>
                                            <option>Adopted Child</option>
                                            <option>Foster Child</option>
                                            <option>Stepson</option>
                                            <option>Ward</option>
                                            <option>Stepdaughter</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-6">Policy #</label>
                                    <div class="col-md-6">
                                         <input type="text" class="form-control" id="policyNo2" />
                                    </div>
                                </div>


                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-6">Policy Start Date </label>
                                    <div class="col-md-6">
                                         <input type="text" class="form-control" id="policyStartDt2" />
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-6">Group #</label>
                                    <div class="col-md-6">
                                         <input type="text" class="form-control" id="policyGroupNo2" />
                                    </div>
                                </div>


                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-6">Policy End Date </label>
                                    <div class="col-md-6">
                                         <input type="text" class="form-control" id="policyEndDt2" />
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-6">Claim # </label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="policyClaimNo2" />
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <hr>
                                <div class="form-group col-md-6">
                                    <label class="checkbox col-md-8">
                                        <input type="checkbox" class="uniform" value="" id="policyAssignBenefits2"> Assignment of Benefits/Accept Assignment
                                    </label>

                                    <label class="checkbox col-md-8">
                                        <input type="checkbox" class="uniform" value="" id="policyDeduc2"> Deductible Met
                                    </label>

                                     <label class="checkbox col-md-8">
                                        <input type="checkbox" class="uniform" value="" id="policyCapitatedPlan2"> 
                                        Capitated Plan
                                    </label>
                                </div>
                                
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-6">Annual Deductible </label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="policyAnnualDedu2" value="0.00" />
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-6">Copayment Amount </label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="policyCopay2" value="0.00" />
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <hr>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-6">Treatment Authorization </label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="policyTreatAuth2" />
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-6">Document Control # </label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="policyDocControl2" />
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <hr>
                                <div class="form-group col-md-4">
                                    <label class="control-label col-md-8">Insurance Coverage Percents by Service Classification</label>
                                </div>
                                <div class="form-group col-md-4">
                                    <label class="control-label col-md-2">A </label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" id="policyA2" />
                                    </div>
                                    <label class="control-label col-md-2">C </label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" id="policyC2" />
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <label class="control-label col-md-2">E </label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" id="policyE2" />
                                    </div>
                                    <label class="control-label col-md-2">G </label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" id="policyG2" />
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <label class="control-label col-md-2">B </label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" id="policyB2" />
                                    </div>
                                    <label class="control-label col-md-2">D </label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" id="policyD2" />
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <label class="control-label col-md-2">F </label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" id="policyF2" />
                                    </div>
                                    <label class="control-label col-md-2">H </label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" id="policyH2" />
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="tab_3_2_6">
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-6">Injury/Illness/LMP Date </label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="injuryDt" />
                                    </div>
                                </div>
                                
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-4">Date similar Symptoms </label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" id="similarSymptoms" />
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-6">Illness Indicator </label>
                                    <div class="col-md-6">
                                        <select class="form-control" id="illnessIndicator">
                                            <option></option>
                                            <option>Injury</option>
                                            <option>Illness</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <div class="col-md-1"></div>
                                    <label class="checkbox-inline col-md-8">
                                        <input type="checkbox" class="uniform" value="" id="sameSymptom"> Same/Similar Symptoms
                                    </label>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-6">First Consultation Date </label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="firstConsultDate" />
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <div class="col-md-1"></div>
                                    <label class="checkbox-inline col-md-8">
                                        <input type="checkbox" class="uniform" value="" id="empRelated"> Employment Related 
                                    </label>
                                </div>
                                
                                <div class="form-group col-md-6">
                                    <div class="col-md-1"></div>
                                    <label class="checkbox-inline col-md-8">
                                        <input type="checkbox" class="uniform" value="" id="emergency"> Emergency
                                    </label>
                                </div>
                                <div class="clearfix"></div>
                                <hr>
                                <h5>Accident</h5>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-3">Related To </label>
                                    <div class="col-md-3">
                                        <select class="form-control" id="relatedTo">
                                            <option></option>
                                            <option>Yes</option>
                                            <option>No</option>
                                            <option>Auto</option>
                                        </select>
                                    </div>
                                    <label class="control-label col-md-3">State </label>
                                    <div class="col-md-3">
                                        <input type="text" class="form-control" id="relatedState" />
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-4">Last X-Ray Date </label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" id="lastXRay" />
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-3">Nature Of </label>
                                    <div class="col-md-9">
                                        <select class="form-control" id="nature">
                                            <option></option>
                                            <option>Injured at School</option>
                                            <option>Injured during Recreation</option>
                                            <option>Work Injury/Self Employee</option>
                                            <option>Work Injury/Non Collision</option>
                                            <option>Work Injury/Collision</option>
                                            <option>Motocycle Injury</option>
                                            <option>Injury at Home</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-4">Death/Status </label>
                                    <div class="col-md-8">
                                        <select class="form-control" id="deathStat">
                                            <option></option>
                                            <option>Moribund</option>
                                            <option>Very Sick</option>
                                            <option>Severely Disabled</option>
                                            <option>Disabled</option>
                                            <option>Requires considerable assistance</option>
                                            <option>Requires occasional assistance</option>
                                            <option>Cares for self</option>
                                            <option>Normal activity with effort</option>
                                            <option>Able to carry on normal activity</option>
                                            <option>Dead</option>
                                            <option>Normal</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <hr>
                                <div class="form-group col-md-6">
                                    <div class="col-md-4">
                                        <h5><u>Dates</u></h5>
                                    </div>
                                    <div class="col-md-4">
                                        <h5><u>From</u></h5>
                                    </div>
                                    <div class="col-md-4">
                                        <h5><u>To</u></h5>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <div class="col-md-12">
                                        <h5><u>Worker's Compensation</u></h5>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-4">Unable To Work </label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" id="unableToWorkFrm" />
                                    </div>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" id="unableToWorkTo" />
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-6">Return to Work Indicator </label>
                                    <div class="col-md-6">
                                        <select class="form-control" id="returnWorkIndicator">
                                            <option></option>
                                            <option>Limited</option>
                                            <option>Normal</option>
                                            <option>Conditional</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-4">Total Disability </label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" id="totalDisabFrm" />
                                    </div>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" id="totDisabTo" />
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-6">Percent of Disability </label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="disabilityPercent" />
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-4">Partial Disability </label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" id="partDisabFrm" style="margin-top:7px;" />
                                    </div>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" id="partDisabTo" style="margin-top:7px;" />
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-6">Last Worked Date</label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="lastWorkDate" />
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-4">Hospitalization </label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" id="hospFrm" />
                                    </div>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" id="hospTo" />
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                </div>
                                <div class="clearfix"></div>
                                <hr>
                                <div class="form-group col-md-6">
                                    <div class="col-md-8"></div>
                                    <label class="checkbox-inline col-md-4">
                                        <input type="checkbox" class="uniform" style="float:right" id="pregnant"> Pregnant
                                    </label>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-6">Date Assumed Care</label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="dateAssumedCare" />
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-6">Estimated DOB</label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="estimatedDOB" />
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-6">Date Relinquished Care</label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="dateRelinquishedCare" />
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="tab_3_2_7">
                                <div class="form-group col-md-3">
                                    <div class="col-md-6"></div>
                                    <label class="checkbox-inline col-md-12">
                                        <input type="checkbox" class="uniform" value="" id="outsideLab"> Outside Lab Work
                                    </label>
                                </div>
                                <div class="form-group col-md-8">
                                    <label class="control-label col-md-6">Lab Charges </label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="labCharges" value="0.00" />
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <hr>
                                <div class="form-group col-md-12" style="padding-bottom:25px;">
                                    <label class="control-label col-md-6">Local Use A </label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="localUseA" />
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group col-md-12" style="padding-bottom:25px;">
                                    <label class="control-label col-md-6">Local Use B </label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="localUseB" />
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group col-md-12" style="padding-bottom:25px;">
                                    <label class="control-label col-md-6">Indicator </label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="indicator" />
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group col-md-12" style="padding-bottom:25px;">
                                    <label class="control-label col-md-6">Referral Date </label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="refDate" />
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group col-md-12" style="padding-bottom:25px;">
                                    <label class="control-label col-md-6">Prescription Date </label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="prescripDate" />
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group col-md-12" style="padding-bottom:25px;">
                                    <label class="control-label col-md-6">Prior Authorization # </label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="priorAuthNo" />
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <hr>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-4">Extra 1</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" id="extra1" />
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-4">Extra 3 </label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" id="extra3" />
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-4">Extra 2 </label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" id="extra2" />
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-4">Extra 4 </label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" id="extra4" />
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <hr>
                                <div class="form-group col-md-12">
                                    <h5><u>Primary Care Providers Outside of this Practice</u></h5>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-6">Outside Primary Care Provider </label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="outsideProvider" />
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-6">Date Last Seen </label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="dateLastSeen" />
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="tab_3_2_8">
                                <div class="form-group col-md-12">
                                    <h5><u>Medicad</u></h5>
                                </div>
                                <div class="form-group col-md-3">
                                    <div class="col-md-6"></div>
                                    <label class="checkbox-inline col-md-12">
                                        <input type="checkbox" class="uniform" id="epsdt"> EPSDT
                                    </label>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-6">Resubmission #</label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="resubmissionDt" />
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group col-md-3">
                                    <div class="col-md-6"></div>
                                    <label class="checkbox-inline col-md-12">
                                        <input type="checkbox" class="uniform" id="familyPlanning"> Family Planning
                                    </label>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-6">Original Reference</label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="originalRefNo" />
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group col-md-10">
                                    <label class="control-label col-md-6">Service Authorization Exception Code</label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="serviceAuth" />
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <hr>
                                <div class="form-group col-md-12">
                                    <h5><u>Tricare/Champus</u></h5>
                                </div>
                                <div class="form-group col-md-8">
                                    <label class="control-label col-md-6">Non-Availability Indicator</label>
                                    <div class="col-md-6">
                                        <select class="form-control" id="nonAvailIndicator">
                                            <option></option>
                                            <option>NA statement not needed</option>
                                            <option>NA statement obtained</option>
                                            <option>Other carrier paid at least 75%</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group col-md-8">
                                    <label class="control-label col-md-6">Branch Of Service</label>
                                    <div class="col-md-6">
                                        <select class="form-control" id="branchService">
                                            <option></option>
                                            <option>Army</option>
                                            <option>Air Force</option>
                                            <option>Marines</option>
                                            <option>Navy</option>
                                            <option>Coast Guard</option>
                                            <option>Public Health Service</option>
                                            <option>NOAA</option>
                                            <option>Champ VA</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group col-md-8">
                                    <label class="control-label col-md-6">Sponsor Status</label>
                                    <div class="col-md-6">
                                        <select class="form-control" id="sponsorStat">
                                            <option></option>
                                            <option>Active</option>
                                            <option>Recalled to active duty</option>
                                            <option>Civilian</option>
                                            <option>Deceased</option>
                                            <option>Former member</option>
                                            <option>Medal of Honor</option>
                                            <option>Permanently disabled</option>
                                            <option>Academy student/Navy OCS</option>
                                            <option>100% disabled</option>
                                            <option>National Guard</option>
                                            <option>Temporary disabled</option>
                                            <option>Retired</option>
                                            <option>Foreign military</option>
                                            <option>Reserves</option>
                                            <option>Other</option>
                                            <option>Unknown</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group col-md-8">
                                    <label class="control-label col-md-6">Special Program</label>
                                    <div class="col-md-6">
                                        <select class="form-control" id="splProgram">
                                            <option></option>
                                            <option>03: Special Federal funding</option>
                                            <option>05: Disability</option>
                                            <option>06: PPV/Medicare 100% payment</option>
                                            <option>07: Induced abortion - Danger to women's life</option>
                                            <option>08: Induced abortion - Victim of rape/incest</option>
                                            <option>09: Second opinion/surgery</option>
                                            <option>30: Medicare demo proj. for lung surgery study</option>
                                            <option>A: Patient is sponsor</option>
                                            <option>B: Patient is spouse</option>
                                            <option>C1: Patient is child 1</option>
                                            <option>C2: Patient is child 2</option>
                                            <option>C3: Patient is child 3</option>
                                            <option>C4: Patient is child 4</option>
                                            <option>C5: Patient is child 5</option>
                                            <option>C6: Patient is child 6</option>
                                            <option>C7: Patient is child 7</option>
                                            <option>C8: Patient is child 8</option>
                                            <option>C9: Patient is child 9</option>
                                            <option>D: Patient is widow of sponsor</option>
                                            <option>70: Local use</option>
                                            <option>71: Local use</option>
                                            <option>72: Local use</option>
                                            <option>73: Local use</option>
                                            <option>74: Local use</option>
                                            <option>75: Local use</option>
                                            <option>76: Local use</option>
                                            <option>77: Local use</option>
                                            <option>78: Local use</option>
                                            <option>79: Local use</option>
                                            <option>80: Local use</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group col-md-8">
                                    <label class="control-label col-md-6">Sponsor Grade</label>
                                    <div class="col-md-6">
                                        <select class="form-control" id="sponsorGuide">
                                            <option>W1</option>
                                            <option>W2</option>
                                            <option>W3</option>
                                            <option>W4</option>
                                            <option>E1</option>
                                            <option>E2</option>
                                            <option>E3</option>
                                            <option>E4</option>
                                            <option>E5</option>
                                            <option>E6</option>
                                            <option>E7</option>
                                            <option>E8</option>
                                            <option>E9</option>
                                            <option>G1</option>
                                            <option>S1</option>
                                            <option>VA</option>
                                            <option>01</option>
                                            <option>02</option>
                                            <option>03</option>
                                            <option>04</option>
                                            <option>05</option>
                                            <option>06</option>
                                            <option>07</option>
                                            <option>08</option>
                                            <option>09</option>
                                            <option>10</option>
                                            <option>11</option>
                                            <option>19</option>
                                            <option>41</option>
                                            <option>42</option>
                                            <option>43</option>
                                            <option>44</option>
                                            <option>45</option>
                                            <option>46</option>
                                            <option>47</option>
                                            <option>48</option>
                                            <option>49</option>
                                            <option>50</option>
                                            <option>51</option>
                                            <option>52</option>
                                            <option>53</option>
                                            <option>54</option>
                                            <option>55</option>
                                            <option>56</option>
                                            <option>57</option>
                                            <option>58</option>
                                            <option>90</option>
                                            <option>99</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group col-md-12">
                                    <h5><u>Effective Dates</u></h5>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-4">Start</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" id="effStartDt" />
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-4">End</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" id="effEndDt" />
                                    </div>
                                </div>
                            </div>
                            <!--<div class="tab-pane" id="tab_3_2_9">
                                <p>I'm in Section 3.</p>
                            </div>
                            <div class="tab-pane" id="tab_3_2_10">
                                <div class="form-group col-md-12">
                                    <textarea class="form-control" rows="10"></textarea>
                                </div>
                            </div>-->
                            <div class="tab-pane" id="tab_3_2_11">
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-6">Care Plan Oversight # </label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="carePlanOversight" />
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-6">Assignment Indicator </label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="assignIndicator" />
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-6">Hospice Number </label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="hospiceNo" />
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-6">Insurance Type Code </label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="insuranceTypeCode" />
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-6">CLIA Number </label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="CLIANo" />
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-6">Timely Filling Indicator </label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="timelyFillingIndicator" />
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-6">Mammography Certification </label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" style="margin-top:10px;" id="mamoCertification" />
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-3">EPSDT Referral Code </label>
                                    <div class="col-md-3">
                                        <input type="text" class="form-control" style="margin-top:10px;" id="EPSDTRefCode1" />
                                    </div>
                                    <div class="col-md-3">
                                        <input type="text" class="form-control" style="margin-top:10px;" id="EPSDTRefCode2" />
                                    </div>
                                    <div class="col-md-3">
                                        <input type="text" class="form-control" style="margin-top:10px;" id="EPSDTRefCode3" />
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-6">Medicaid Referral <br/>Access # </label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="refAccessNo" />
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-6">Homebound </label>
                                    <div class="col-md-6">
                                        <input type="checkbox" class="uniform" id="homebound">
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-6">Demo Code </label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="demoCode" />
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-6">IDE Number </label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="IDENo" />
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <hr>
                                <div class="form-group col-md-6">
                                    <h5><u>Vision Claims</u></h5>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-6">Condition Indicator </label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="conditionIndicator" />
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-6">Code Category </label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="codeCategory" />
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group col-md-6">
                                    <div class="col-md-12">
                                        <input type="checkbox" class="uniform" id="certCodeApplies" /> Certification Code Applies
                                    </div>
                                </div>
                                <div class="clearfix"></div><hr>
                                <div class="form-group col-md-6">
                                    <h5><u>Home Health Claims</u></h5>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-6">Total Visit Rendered </label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="totalVisitRendered" />
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-6">Discipline Type Code </label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="disciplineTypeCode" />
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-6">Total Visits Projected </label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" style="margin-top:6px" id="totalVisitProjected" />
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-6">Ship/Delivery Pattern Code </label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" style="margin-top:6px" id="deliveryPatternCode" />
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-6">Number of Visits </label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="noOfVisitsHomeHealth" />
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-6">Ship/Delivery Time Code  </label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="deliveryTimeCode" />
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-6">Duration </label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="duration" />
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-6">Frequency Period </label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="freqPeriod" />
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-6">Number of Units </label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="noOfUnits" />
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-6">Frequency Count </label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="freqCount" />
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="button" class="btn btn-default" data-dismiss="modal" value="Exit" />
                    <input type="button" id="btnSaveCase" class="btn btn-primary" data-dismiss="modal" value="Save" />
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>

</body>
</html>