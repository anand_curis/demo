
<?php

ini_set("display_errors", "1");
error_reporting(E_ALL);


require_once('fpdi/fpdf.php');
require_once('fpdi/fpdi.php');
$pdf = new FPDI();
$fullPathToPDF = '00041_06082018.pdf';
$pageCount = $pdf->setSourceFile($fullPathToPDF);
for ($i = 1; $i <= $pageCount; $i++) {
    $pdf->importPage($i);
    $pdf->AddPage();
    $pdf->useTemplate($i);
}
$pdf->SetFont('Helvetica');
$pdf->SetXY(110, 225);
$pdf->Write(8, 'A complete document imported with FPDI');
$pdf->Output($fullPathToPDF);

?>