<?php
 session_start();

 if (!isset($_SESSION['userID'])) {
 	header("Location: login.php");
 }
 ?>
 <!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0" />
	<title>Claim | AMROMED LLC</title>

	<!--=== CSS ===-->

	<!-- Bootstrap -->
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

	<!-- jQuery UI -->
	<!--<link href="plugins/jquery-ui/jquery-ui-1.10.2.custom.css" rel="stylesheet" type="text/css" />-->
	<!--[if lt IE 9]>
		<link rel="stylesheet" type="text/css" href="plugins/jquery-ui/jquery.ui.1.10.2.ie.css"/>
	<![endif]-->
	<!-- Theme -->
	<link href="assets/css/main.css" rel="stylesheet" type="text/css" />
	<link href="assets/css/plugins.css" rel="stylesheet" type="text/css" />
	<link href="assets/css/responsive.css" rel="stylesheet" type="text/css" />
	<link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
	<link href="assets/css/alertify.css" rel='stylesheet' type='text/css'>
	<link href="assets/css/themes/default.css" rel='stylesheet' type='text/css'>

	<link rel="stylesheet" href="assets/css/fontawesome/font-awesome.min.css">
	<!--[if IE 7]>
		<link rel="stylesheet" href="assets/css/fontawesome/font-awesome-ie7.min.css">
	<![endif]-->

	<!--[if IE 8]>
		<link href="assets/css/ie8.css" rel="stylesheet" type="text/css" />
	<![endif]-->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>

	<!--=== JavaScript ===-->

	<script type="text/javascript" src="assets/js/libs/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>
	<script type="text/javascript" src="plugins/bootstrap-switch/bootstrap-switch.min.js"></script>

	<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="assets/js/libs/lodash.compat.min.js"></script>

	<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
		<script src="assets/js/libs/html5shiv.js"></script>
	<![endif]-->

	<!-- Smartphone Touch Events -->
	<script type="text/javascript" src="plugins/touchpunch/jquery.ui.touch-punch.min.js"></script>
	<script type="text/javascript" src="plugins/event.swipe/jquery.event.move.js"></script>
	<script type="text/javascript" src="plugins/event.swipe/jquery.event.swipe.js"></script>

	<!-- General -->
	<script type="text/javascript" src="assets/js/libs/breakpoints.js"></script>
	<script type="text/javascript" src="plugins/respond/respond.min.js"></script> <!-- Polyfill for min/max-width CSS3 Media Queries (only for IE8) -->
	<script type="text/javascript" src="plugins/cookie/jquery.cookie.min.js"></script>
	<script type="text/javascript" src="plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script type="text/javascript" src="plugins/slimscroll/jquery.slimscroll.horizontal.min.js"></script>

	<!-- Page specific plugins -->
	<!-- Charts -->
	<script type="text/javascript" src="plugins/sparkline/jquery.sparkline.min.js"></script>

	<script type="text/javascript" src="plugins/daterangepicker/moment.min.js"></script>
	<script type="text/javascript" src="plugins/daterangepicker/daterangepicker.js"></script>
	<script type="text/javascript" src="plugins/blockui/jquery.blockUI.min.js"></script>

	<!-- Pickers -->
	<script type="text/javascript" src="plugins/pickadate/picker.js"></script>
	<script type="text/javascript" src="plugins/pickadate/picker.date.js"></script>
	<script type="text/javascript" src="plugins/pickadate/picker.time.js"></script>
	<script type="text/javascript" src="plugins/bootstrap-colorpicker/bootstrap-colorpicker.min.js"></script>

	<!-- Noty -->
	<script type="text/javascript" src="plugins/noty/jquery.noty.js"></script>
	<script type="text/javascript" src="plugins/noty/layouts/top.js"></script>
	<script type="text/javascript" src="plugins/noty/themes/default.js"></script>

	<!-- Slim Progress Bars -->
	<script type="text/javascript" src="plugins/nprogress/nprogress.js"></script>
	<script type="text/javascript" src="assets/js/jquery.mask.min.js"></script>

	<!-- Bootbox -->
	<script type="text/javascript" src="plugins/bootbox/bootbox.min.js"></script>

	<!-- App -->
	<script type="text/javascript" src="assets/js/app.js"></script>
	<script type="text/javascript" src="assets/js/plugins.js"></script>
	<script type="text/javascript" src="assets/js/plugins.form-components.js"></script>

	<script src="assets/js/jquery.datetimepicker.js"></script>
	<link href="assets/css/jquery.datetimepicker.css" rel="stylesheet" />
	<style type="text/css">
	.nav-tabs>li{
		float: left;
		margin-bottom: -2px;
	}
	.tabbable-custom > .nav-tabs > li > a : active{
		border-bottom:0;
	}
	.ui-menu .ui-menu-item a {
  font-size: 12px;
  color:#fff;
}
.ui-autocomplete {
  position: absolute;
  top: 0;
  left: 0;
  z-index: 1510 !important;
  float: left;
  display: none;
  min-width: 160px;
  width: 160px;
  padding: 4px 0;
  margin: 2px 0 0 0;
  list-style: none;
  background-color: #ffffff;
  border-color: #ccc;
  border-color: rgba(0, 0, 0, 0.2);
  border-style: solid;
  border-width: 1px;
  -webkit-border-radius: 2px;
  -moz-border-radius: 2px;
  border-radius: 2px;
  -webkit-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
  -moz-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
  box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
  -webkit-background-clip: padding-box;
  -moz-background-clip: padding;
  background-clip: padding-box;
  *border-right-width: 2px;
  *border-bottom-width: 2px;
}
.ui-menu-item > a.ui-corner-all {
    display: block;
    padding: 3px 15px;
    clear: both;
    font-weight: normal;
    line-height: 18px;
    color:#000;
    white-space: nowrap;
    text-decoration: none;
}
.ui-state-hover {
      color: #fff;
      text-decoration: none;
      background-color: #0088cc;
      border-radius: 0px;
      -webkit-border-radius: 0px;
      -moz-border-radius: 0px;
      background-image: none;
}
.ui-state-active {
      color: #fff;
      text-decoration: none;
      background-color: #0088cc;
      border-radius: 0px;
      -webkit-border-radius: 0px;
      -moz-border-radius: 0px;
      background-image: none;
}
#FacPOS {
  border: none; 
  font-size: 14px;

  height: 24px;
  margin-bottom: 5px;
  padding-top: 2px;
  border: 1px solid #DDD !important;
  padding-top: 0px !important;
  z-index: 1511;
  position: relative;
}
	</style>

	<script>
	$(document).ready(function(){
		"use strict";

		App.init(); // Init layout and core plugins
		Plugins.init(); // Init all plugins
		FormComponents.init(); // Init all form-specific plugins
		document.getElementById('practiceName').innerHTML = sessionStorage.getItem('practiceName');
		var tabVal = "";

		$('#loader').hide();
		$('#divAlert').hide();

		if(sessionStorage.getItem("editPatID") != null){
			var name = "";
			$.ajax({
                type: "POST",
                url:"getPatName.php",
                async: false,
                data:{
                    "patientID" : sessionStorage.getItem("editPatID")
                    },success:function(result){
                        var res = JSON.parse(result);
                        name = res[0].fullName;
                }
            });
			document.getElementById("patient").value = sessionStorage.getItem("editPatID")+" - "+name;
			$("#patient").focus();
		}
		else{
			var patientList = <?php include('patientList.php'); ?>;
            $("#patient").autocomplete({
    			source: patientList,
                autoFocus:true
            });
		}

		var temp = sessionStorage.getItem("patientId");


		var patientList = <?php include('patientList.php'); ?>;
            $("#patient").autocomplete({
    			source: patientList,
                autoFocus:true
            });

            document.getElementById('hdnDX').value = "true";

        $('#icdtoggle').on('change', function(event, state) {
          document.getElementById('hdnDX').value = event.target.checked; // jQuery event
        });

        $('#showAll').change(function(){
            if($('#showAll').is(':checked') == true){
                getClaims();
            }
            else{
                $("#body").html('');
                $('#body').append('<tr><td><input type="text" class="form-control date" style="width:70px;" id="fromDt"/></td><td><input type="text" class="form-control date" style="width:70px;" id="toDt"/></td><td><input type="text" class="form-control cpt" style="width:60px;float:left;display:inline-flex;" id="proced" title="" /><a href="#" id="linkCPT" style="padding:5px 0 0 5px;"><i class="icon icon-plus" style="font-size:10px; color:maroon"></i></a></td><td><input type="text" class="form-control mod" style="width:30px;" id="mod1" /></td><td><input type="text" class="form-control clsUnits" style="width:30px;" id="units" value="1" /></td><td><input type="text" class="form-control clsCharge" style="width:50px;" id="charge" /></td><td><input type="text" class="form-control tot" style="width:50px;" id="total" /></td><td><input type="text" class="form-control icd" style="width:50px;float:left;display:inline-flex;" id="diag1" /><a href="#" class="linkICD" style="padding:5px 0 0 5px;"><i class="icon icon-plus" style="font-size:10px; color:maroon"></i></a></td><td><input type="text" class="form-control icd" style="width:50px;float:left;display:inline-flex;" id="diag2" /><a href="#"  class="linkICD" style="padding:5px 0 0 5px;"><i class="icon icon-plus" style="font-size:10px; color:maroon"></i></a></td><td><input type="text" class="form-control icd" style="width:50px;float:left;display:inline-flex;" id="diag3" /><a href="#" class="linkICD" style="padding:5px 0 0 5px;"><i class="icon icon-plus" style="font-size:10px; color:maroon"></i></a></td><td><input type="text" class="form-control icd" style="width:50px;float:left;display:inline-flex;" id="diag4" /><a href="#" class="linkICD" style="padding:5px 0 0 5px;"><i class="icon icon-plus" style="font-size:10px; color:maroon"></i></a></td><td><input type="text" class="form-control phy" style="width:50px;float:left;display:inline-flex;" id="provider" /><a href="#" id="linkPrimaryPhysi" style="padding:5px 0 0 5px;"><i class="icon icon-plus" style="font-size:10px; color:maroon"></i></a></td><td><input type="text" class="form-control loc" style="width:50px;float:left;display:inline-flex;" id="location" /><a href="#" id="linkLOC" style="padding:5px 0 0 5px;"><i class="icon icon-plus" style="font-size:10px; color:maroon"></i></a></td></td><td><select id="ddlStatus" class="form-control status"><option value="COMPLETE">Complete</option><option value="DRAFT">Draft</option><option value="REVIEW">Review</option></select></td></tr>');
            }
        });

        $(document).on('focus','#diag1',function() {
            var toggleDX = document.getElementById('hdnDX').value;
            if(toggleDX != "false"){
                $("#diag1").autocomplete({
                    minlength : 3,
                    source: function(request, response) { 
                        $.ajax({
                            url: "dxSearch9.php",
                            data: { term: request.term },
                            type: "POST", 
                            success: function(data) { 
                                response(JSON.parse(data)); 
                            }
                        });
                    },
                    autoFocus:true
                });
            }
            else{
                $("#diag1").autocomplete({
                    minlength : 3,
                    source: function(request, response) { 
                        $.ajax({
                            url: "dxSearch10.php",
                            data: { term: request.term },
                            type: "POST", 
                            success: function(data) { 
                                response(JSON.parse(data)); 
                            }
                        });
                    },
                    autoFocus:true
                });
            }
        });

        $(document).on('focus','#diag2',function() {
            var toggleDX = document.getElementById('hdnDX').value;
            if(toggleDX != "false"){
                $("#diag2").autocomplete({
                    minlength : 3,
                    source: function(request, response) { 
                        $.ajax({
                            url: "dxSearch9.php",
                            data: { term: request.term },
                            type: "POST", 
                            success: function(data) { 
                                response(JSON.parse(data)); 
                            }
                        });
                    },
                    autoFocus:true
                });
            }
            else{
                $("#diag2").autocomplete({
                    minlength : 3,
                    source: function(request, response) { 
                        $.ajax({
                            url: "dxSearch10.php",
                            data: { term: request.term },
                            type: "POST", 
                            success: function(data) { 
                                response(JSON.parse(data)); 
                            }
                        });
                    },
                    autoFocus:true
                });
            }
        });

        $(document).on('focus','#diag3',function() {
            var toggleDX = document.getElementById('hdnDX').value;
            if(toggleDX != "false"){
                $("#diag3").autocomplete({
                    minlength : 3,
                    source: function(request, response) { 
                        $.ajax({
                            url: "dxSearch9.php",
                            data: { term: request.term },
                            type: "POST", 
                            success: function(data) { 
                                response(JSON.parse(data)); 
                            }
                        });
                    },
                    autoFocus:true
                });
            }
            else{
                $("#diag3").autocomplete({
                    minlength : 3,
                    source: function(request, response) { 
                        $.ajax({
                            url: "dxSearch10.php",
                            data: { term: request.term },
                            type: "POST", 
                            success: function(data) { 
                                response(JSON.parse(data)); 
                            }
                        });
                    },
                    autoFocus:true
                });
            }
        });

        $(document).on('focus','#diag4',function() {
            var toggleDX = document.getElementById('hdnDX').value;
            if(toggleDX != "false"){
                $("#diag4").autocomplete({
                    minlength : 3,
                    source: function(request, response) { 
                        $.ajax({
                            url: "dxSearch9.php",
                            data: { term: request.term },
                            type: "POST", 
                            success: function(data) { 
                                response(JSON.parse(data)); 
                            }
                        });
                    },
                    autoFocus:true
                });
            }
            else{
                $("#diag4").autocomplete({
                    minlength : 3,
                    source: function(request, response) { 
                        $.ajax({
                            url: "dxSearch10.php",
                            data: { term: request.term },
                            type: "POST", 
                            success: function(data) { 
                                response(JSON.parse(data)); 
                            }
                        });
                    },
                    autoFocus:true
                });
            }
        });

        $('#patient').focusout(function(){
        	sessionStorage.removeItem("editPatID");
			var patientId_claim = $(this).val();
			if(patientId_claim != ""){
				var n=patientId_claim.indexOf("-");
				var truncID = patientId_claim.substr(0,n);
				$.ajax({
	              type: "POST",
	              url:"getAlerts.php",
	              async : false,
	              data:{
	                alert_patientID : truncID
	              },success:function(result){
	                var res = JSON.parse(result);
	                if(res!=""){
	                  if(res[0].alerts_Charge == "1"){
	                      $('#divAlert').show();
	                      document.getElementById("alertTop").innerHTML = res[0].alert_desc;
	                  }
	                  else{
	                    $('#divAlert').hide();
	                  }
	                }
	            }
	          });
	        	$.post("http://curismed.com/medService/cases",
			    {
			        patientID: truncID,
			    },
			    function(data, status){
			        var arrCase = [];
			        $('#case').html('');
			        for(var x in data){
					  arrCase.push(data[x]);
					}
					//alert(arrCase);
					$.each(arrCase,function(i,v) {
						$('#case').html('');
						arrCase.forEach(function(t) { 
				            $('#case').append('<option value="'+t.caseID+'">'+t.caseChartNo+'</option>');
				        });
				        var tempCase = document.getElementById('case').value;
						var tempcaseText = document.getElementById('case').options[document.getElementById('case').selectedIndex].text;
						$.post("http://curismed.com/medService/cases/load",
					    {
					        caseChartNo: tempcaseText,
					    },
					    function(data, status){
					    	if(data.length != 0){
					    		if(data[0].principalDiag != ""){
							    	document.getElementById("diag1").value = data[0].principalDiag;
							    }
							    if(data[0].defDiag2 != ""){
							    	document.getElementById("diag2").value = data[0].defDiag2;
							    }
							    if(data[0].defDiag3 != ""){
							    	document.getElementById("diag3").value = data[0].defDiag3;
							    }
							    if(data[0].defDiag4 != ""){
							    	document.getElementById("diag4").value = data[0].defDiag4;
							    }
						    }
					    });
						//Cases(tempCase);
					});
					var tempCase = document.getElementById('case').value;
				    var tempcaseText = document.getElementById('case').options[document.getElementById('case').selectedIndex].text;
					tabList(tempcaseText);
			    });
	        }
	        else{
	        	$('#case').html('');
	        	$('#body').html('');
	        }
	        
    	});
		
    	$('#case').change(function(){
    		if($('#showAll').is(':checked') == true){
                getClaims();
            }
            else{
                $("#body").html('');
                $('#body').append('<tr><td><input type="text" class="form-control date" style="width:70px;" id="fromDt"/></td><td><input type="text" class="form-control date" style="width:70px;" id="toDt"/></td><td><input type="text" class="form-control cpt" style="width:60px;float:left;display:inline-flex;" id="proced" title="" /><a href="#" id="linkCPT" style="padding:5px 0 0 5px;"><i class="icon icon-plus" style="font-size:10px; color:maroon"></i></a></td><td><input type="text" class="form-control mod" style="width:30px;" id="mod1" /></td><td><input type="text" class="form-control clsUnits" style="width:30px;" id="units" value="1" /></td><td><input type="text" class="form-control clsCharge" style="width:50px;" id="charge" /></td><td><input type="text" class="form-control tot" style="width:50px;" id="total" /></td><td><input type="text" class="form-control icd" style="width:50px;float:left;display:inline-flex;" id="diag1" /><a href="#" class="linkICD" style="padding:5px 0 0 5px;"><i class="icon icon-plus" style="font-size:10px; color:maroon"></i></a></td><td><input type="text" class="form-control icd" style="width:50px;float:left;display:inline-flex;" id="diag2" /><a href="#"  class="linkICD" style="padding:5px 0 0 5px;"><i class="icon icon-plus" style="font-size:10px; color:maroon"></i></a></td><td><input type="text" class="form-control icd" style="width:50px;float:left;display:inline-flex;" id="diag3" /><a href="#" class="linkICD" style="padding:5px 0 0 5px;"><i class="icon icon-plus" style="font-size:10px; color:maroon"></i></a></td><td><input type="text" class="form-control icd" style="width:50px;float:left;display:inline-flex;" id="diag4" /><a href="#" class="linkICD" style="padding:5px 0 0 5px;"><i class="icon icon-plus" style="font-size:10px; color:maroon"></i></a></td><td><input type="text" class="form-control phy" style="width:50px;float:left;display:inline-flex;" id="provider" /><a href="#" id="linkPrimaryPhysi" style="padding:5px 0 0 5px;"><i class="icon icon-plus" style="font-size:10px; color:maroon"></i></a></td><td><input type="text" class="form-control loc" style="width:50px;float:left;display:inline-flex;" id="location" /><a href="#" id="linkLOC" style="padding:5px 0 0 5px;"><i class="icon icon-plus" style="font-size:10px; color:maroon"></i></a></td></td><td><select id="ddlStatus" class="form-control status"><option value="COMPLETE">Complete</option><option value="DRAFT">Draft</option><option value="REVIEW">Review</option></select></td></tr>');
            }
    		var tempCase = document.getElementById('case').value;
			var tempcaseText = document.getElementById('case').options[document.getElementById('case').selectedIndex].text;
			$.post("http://curismed.com/medService/cases/load",
		    {
		        caseChartNo: tempcaseText,
		    },
		    function(data, status){
		    	if(data.length != 0){
		    		if(data[0].principalDiag != ""){
				    	document.getElementById("diag1").value = data[0].principalDiag;
				    }
				    if(data[0].defDiag2 != ""){
				    	document.getElementById("diag2").value = data[0].defDiag2;
				    }
				    if(data[0].defDiag3 != ""){
				    	document.getElementById("diag3").value = data[0].defDiag3;
				    }
				    if(data[0].defDiag4 != ""){
				    	document.getElementById("diag4").value = data[0].defDiag4;
				    }
			    }
		    });
		    tabList(tempcaseText);
		});
	});
	
	$(window).load(function() {
		
		$(document).on('click','#linkCPT',function() {
			$('#CPTModal').modal('show');
		});
		$(document).on('click','.linkICD',function() {
			$('#ICDModal').modal('show');
		});
		$(document).on('click','#linkLOC',function() {
			$('#AddServLoc').modal('show');
		});
		$(document).on('click','#linkPrimaryPhysi',function() {
			$('#PrimaryPhysi').modal('show');
		});
        
		$(document).on('focus','.date',function() {
			$('.date').mask('00-00-0000');
		});
		$(document).on('focusout','#fromDt',function() {
			var dt = $("#fromDt").val();
			var res = dt.split("-");
			var mon = res[0];
			var date1 = res[1];
			var len = res[2].length;
			if( len != 4){
				var yr = "20"+res[2];
			}
			else{
				var yr = res[2];
			}
			document.getElementById("fromDt").value = mon+"-"+date1+"-"+yr;
		});

		$(document).on('focusout','#toDt',function() {
			var dt = $("#toDt").val();
			var res = dt.split("-");
			var mon = res[0];
			var date1 = res[1];
			var len = res[2].length;
			if( len != 4){
				var yr = "20"+res[2];
			}
			else{
				var yr = res[2];
			}
			document.getElementById("toDt").value = mon+"-"+date1+"-"+yr;
		});

		$(document).on('focus','.cpt',function() {
         var procedure = <?php include('procedureList.php'); ?>;
            $("#proced").autocomplete({
    			source: procedure,
                autoFocus:true
            });

		});

		$(document).on('focus','.mod',function() {
         var procedure = <?php include('modifiersList.php'); ?>;
            $("#mod1").autocomplete({
    			source: procedure,
                autoFocus:true
            });

		});
		$(document).on('focus','#FacPOS',function() {
         var procedure = <?php include('POSList.php'); ?>;
            $("#FacPOS").autocomplete({
    			source: procedure,
                autoFocus:true
            });

		});

		$(document).on('focus','.phy',function() {
			var PrimaryPhy = <?php include('primaryPhysician.php'); ?>;
            $("#provider").autocomplete({
                source: PrimaryPhy,
                autoFocus:true
            });
	    });

		$(document).on('focus','.loc',function() {
         var loc = <?php include('getServiceLoc.php'); ?>;
            $("#location").autocomplete({
    			source: loc,
                autoFocus:true
            });

		});
		
		$(document).on('focusout','.clsCharge',function() {
			var unit = $(".clsUnits").val();
			var charge = $(".clsCharge").val();
			charge = parseInt(charge).toFixed(2);
			$(this).val(charge);
			var tot = unit * charge;

			document.getElementById("total").value = tot.toFixed(2);;
		});
		$(document).on('focusout','.cpt',function() {
			var cpt = $("#proced").val();
			var n = cpt.indexOf("-");
			var truncCPT = cpt.substr(0,n);
			$.ajax({
                type: "POST",
                url:"getCPTCharge.php",
                async:false,
                data:{ 
			        cptCd : truncCPT
			    },success:function(data2){
					document.getElementById("proced").value = truncCPT;
			    	document.getElementById("charge").value = data2;
			    	var tit = cpt.replace(truncCPT+"- ","");
			    	$('#proced').attr('title', tit);
			    }
			});
		});
		
		$("#btnAddPriPhy").click(function() {
            var phyName = document.getElementById("PrimaryPhysiName").value;
			var speciality = document.getElementById("phySpeciality").value;
			var dob = document.getElementById("phyDOB").value;
			var individualNPI = document.getElementById("phyNPI").value;
			var ssno = document.getElementById("phySSN").value;
			var address = document.getElementById("phyAddr").value;
			var phoneHome = document.getElementById("phyHomePhone").value;
			var phoneWork = document.getElementById("phyWorkPhone").value;
			var phoneMobile = document.getElementById("phyMobilePhone").value;
			var phyEmail = document.getElementById("phyEmail").value;
			var fax = document.getElementById("phyFax").value;
			var notes = document.getElementById("phyNotes").value;
			var practiceID = sessionStorage.getItem('practiceId');

            $.ajax({
                type: "POST",
                url:"backend/add_primaryPhysi.php",
                data:{ 
                    "PrimaryPhysiName" : phyName,
                    "phyDOB" : dob,
                    "phyNPI" : individualNPI,
                    "phySSN" : ssno,
                    "phyAddr" : address,
                    "phyHomePhone" : phoneHome,
                    "phyWorkPhone" : phoneWork,
                    "phyMobilePhone" : phoneMobile,
                    "phyEmail" : phyEmail,
                    "phyFax" : fax,
                    "phyNotes" : notes,
                    "speciality" : speciality,
                    "practiceID" : practiceID
                    },success:function(result){
                        //alertify.success('PrimaryPhysiName updated successfully');
                        $('#PrimaryPhysi').modal('hide');
                        //window.location.reload();
                     }
            });
        });

		$("#btnCPT").click(function() {
            var cptCode = $('#cptCode').val();
            var description = $('#description').val();
            var charge = $('#charge').val();
            
            $.ajax({
                type: "POST",
                url:"http://curismed.com/medService/codes/cptcreate",
                data:{ 
                    "cptCode" : cptCode,
                    "description" : description,
                    "charge" : charge,
                    },success:function(result){
                        alertify.success('CPT Code created successfully');
                        $('#CPTModal').modal('hide');
                        window.location.reload();
                     }

            });
        });

        $("#btnICD").click(function() {
            var icdCode = $('#icdCode').val();
            var dxCode = $('#dxCode').val();
            var longDescription = $('#longDescription').val();
            var shortDescription = $('#shortDescription').val();
            var codeType = $('#ddlICDType').val();
            
            $.ajax({
                type: "POST",
                url:"http://curismed.com/medService/icdcreate",
                data:{ 
                    "icdCode" : icdCode,
                    "dxCode" : dxCode,
                    "longDescription" : longDescription,
                    "shortDescription" : shortDescription,
                    "codeType" : codeType,
                    },success:function(result){
                        alertify.success('ICD Code created successfully');
                        $('#ICDModal').modal('hide');
                        window.location.reload();
                     }

            });
        });

        $("#btnSaveServLoc").click(function() {
            var FacInternalName = $('#FacInternalName').val();
            var FacTimeZone = $('#FacTimeZone').val();
            var FacNPI = $('#FacNPI').val();
            var FacLegNoType = $('#FacLegNoType').val();
            var FacLegNo = $('#FacLegNo').val();
            var FacBillingName = $('#FacBillingName').val();
            var FacPOS = $('#FacPOS').val();
            var FacPhone = $('#FacPhone').val();
            var FacAddr = $('#FacAddr').val();
            var FacFax = $('#FacFax').val();

            var n1=FacPOS.indexOf("-");
			var POStruncID = FacPOS.substr(0,n1);
			var practiceID = sessionStorage.getItem('practiceId');
            
            $.ajax({
                type: "POST",
                url:"http://curismed.com/medService/serviceloc/create",
                data:{ 
                	"practiceID" : practiceID,
                    "internalName" : FacInternalName,
		            "timeZone" : FacTimeZone,
		            "NPI" : FacNPI,
		            "legacyNoType" : FacLegNoType,
		            "legacyNo" : FacLegNo,
		            "billingName" : FacBillingName,
		            "placeOfService" : POStruncID,
		            "phone" : FacPhone,
		            "address" : FacAddr,
		            "fax" : FacFax,
                    },success:function(result){
                        alertify.success('Facility added successfully');
                        $('#AddServLoc').modal('hide');
                        window.location.reload();
                     }

            });
        });

		$(document).on('focusout','.icd',function() {
			var icd = $(this).attr('id');
			var icdVal = document.getElementById(icd).value;
			var n = icdVal.indexOf("-");
			var truncCPT = icdVal.substr(0,n-1);
			document.getElementById(icd).value = truncCPT;
	    	var tit = icdVal.replace(truncCPT+"- ","");
	    	$('#'+icd).attr('title', tit);
		});
	    $(document).on('focusout','.status',function() {
	    	$(".tabID").each(function () {
				var IsTF = $(this).parents().hasClass('active');
				var IDVal = $(this).attr('id');
				if(IsTF == true){
					tabVal = IDVal;
				}
			});
	        var fromDt = convertDate(document.getElementById('fromDt').value);
	        var toDt = convertDate(document.getElementById('toDt').value);
	        var proced = document.getElementById('proced').value;
	        var mod1 = document.getElementById('mod1').value;
	        var units = document.getElementById('units').value;
	        var charge = document.getElementById('charge').value;
	        var total = document.getElementById('total').value;
	        var diag1 = document.getElementById('diag1').value;
	        var diag2 = document.getElementById('diag2').value;
	        var diag3 = document.getElementById('diag3').value;
	        var diag4 = document.getElementById('diag4').value;
	        var provider = document.getElementById('provider').value;
	        var location = document.getElementById('location').value;
	        // if(coPay != ""){
	        // 	var claimBal = 
	        // }
	        // else{

	        // }
	        var caseid = document.getElementById('case').value;
	        var status = document.getElementById('ddlStatus').value;
	        var insuranceID = tabVal;
	        
	        var n1=provider.indexOf("-");
			var truncID1 = provider.substr(0,n1);
	        
	        var n2=location.indexOf("-");
			var truncID2 = location.substr(0,n2);
			var POS = "";
			$.ajax({
                type: "POST",
                url:"getPlaceOfService.php",
                async:false,
                data:{ 
			        serviceLocID : location
			    },success:function(data2){
			    	POS = data2;
			    }
			});
	        $('#loader').show();
	        $.ajax({
                type: "POST",
                url:"checkVisit.php",
                async:false,
                data:{ 
			        caseID : caseid
			    },success:function(data2){
			    	if(data2=="allow" || data2 == "no-auth"){
			    		$.post("http://curismed.com/medService/claims/create",
					    {
					    	caseID: caseid,
					        fromDt: fromDt,
					        toDt: toDt,
					        proced: proced,
					        mod1: mod1,
					        units: units,
					        charge: charge,
					        claimBalance:charge,
					        total: total,
					        diag1: diag1,
					        diag2: diag2,
					        diag3: diag3,
					        diag4: diag4,
					        diag4: diag4,
					        primaryCarePhy: truncID1,
					        serviceLocID: truncID2,
					        placeOfService : POS,
					        claimStatus : status,
					        insuranceID : insuranceID,
					    },
					    function(data, status){
					    	alertify.success("New Claim has been added successfully");
					    	var valueCase = document.getElementById('case').value;
					    	$('#loader').hide();
					    	CasesCon(valueCase);
					    	$('html, body').animate({
							    scrollTop: 0,
							    scrollLeft: 0
							}, 1000);
					    	
				    });
			    	}
			    	else{
			    		alertify.error("The Maximum Auth # of Visits has been reached.");
			    	}
			    }
			});
	});
});

	function getClaims(){
		var caseId = $('#case').val();
		$.post("http://curismed.com/medService/claims",
		    {
		        caseID: caseId,
		    },
		    function(data, status){
		    	//alert(JSON.stringify(data));
		    	var tbody = $('#body');
			    $('#body').html('');
			    //var tr = $('<tr/>').appendTo(tbody);
			    for (var i = 0; i < data.length; i++) {
			    	var locName = "";
			    	$.ajax({
		                type: "POST",
		                url:"getLocName.php",
		                async : false,
		                data:{
		                	serviceLocID : data[i].serviceLocID
	                	},success:function(result){
	                        locName = result;
	                     }
		            });

		            if(data[i].charge == ""){
	                    if(data[i].charge.indexOf(".")==-1){
	                        data[i].charge = data[i].charge+'.00';
	                    }
	                    else{
	                        data[i].charge = data[i].charge;
	                    }
	                }
	                if(data[i].total == ""){
	                    if(data[i].total.indexOf(".")==-1){
	                        data[i].total = data[i].total+'.00';
	                    }
	                    else{
	                        data[i].total = data[i].total;
	                    }
	                }
			    	
					var fromDt = changeDateFormat(data[i].fromDt);
					var toDt = changeDateFormat(data[i].toDt);
			            $('#body').append('<tr><td>' + fromDt + '</td><td>' + toDt + '</td><td>' + data[i].proced + '</td><td>' + data[i].mod1 + '</td><td>' + data[i].units + '</td><td>' + data[i].charge + '</td><td>' + data[i].total + '</td><td>' + data[i].diag1 + '</td><td>' + data[i].diag2 + '</td><td>' + data[i].diag3 + '</td><td>' + data[i].diag4 + '</td><td>' + data[i].PrimaryCarePhysician + '</td><td>' + locName + '</td><td>' + data[i].claimStatus + '</td></tr>');
			    }
			    $('#body').append('<tr><td><input type="text" class="form-control date" style="width:70px;" id="fromDt"/></td><td><input type="text" class="form-control date" style="width:70px;" id="toDt"/></td><td><input type="text" class="form-control cpt" style="width:60px;float:left;display:inline-flex;" id="proced" title="" /><a href="#" id="linkCPT" style="padding:5px 0 0 5px;"><i class="icon icon-plus" style="font-size:10px; color:maroon"></i></a></td><td><input type="text" class="form-control mod" style="width:30px;" id="mod1" /></td><td><input type="text" class="form-control clsUnits" style="width:30px;" id="units" value="1" /></td><td><input type="text" class="form-control clsCharge" style="width:50px;" id="charge" /></td><td><input type="text" class="form-control tot" style="width:50px;" id="total" /></td><td><input type="text" class="form-control icd" style="width:50px;float:left;display:inline-flex;" id="diag1" /><a href="#" class="linkICD" style="padding:5px 0 0 5px;"><i class="icon icon-plus" style="font-size:10px; color:maroon"></i></a></td><td><input type="text" class="form-control icd" style="width:50px;float:left;display:inline-flex;" id="diag2" /><a href="#"  class="linkICD" style="padding:5px 0 0 5px;"><i class="icon icon-plus" style="font-size:10px; color:maroon"></i></a></td><td><input type="text" class="form-control icd" style="width:50px;float:left;display:inline-flex;" id="diag3" /><a href="#" class="linkICD" style="padding:5px 0 0 5px;"><i class="icon icon-plus" style="font-size:10px; color:maroon"></i></a></td><td><input type="text" class="form-control icd" style="width:50px;float:left;display:inline-flex;" id="diag4" /><a href="#" class="linkICD" style="padding:5px 0 0 5px;"><i class="icon icon-plus" style="font-size:10px; color:maroon"></i></a></td><td><input type="text" class="form-control phy" style="width:50px;float:left;display:inline-flex;" id="provider" /><a href="#" id="linkPrimaryPhysi" style="padding:5px 0 0 5px;"><i class="icon icon-plus" style="font-size:10px; color:maroon"></i></a></td><td><input type="text" class="form-control loc" style="width:50px;float:left;display:inline-flex;" id="location" /><a href="#" id="linkLOC" style="padding:5px 0 0 5px;"><i class="icon icon-plus" style="font-size:10px; color:maroon"></i></a></td></td><td><select id="ddlStatus" class="form-control status"><option value="COMPLETE">Complete</option><option value="DRAFT">Draft</option><option value="REVIEW">Review</option></select></td></tr>');
		    });
			var tempCase = document.getElementById('case').value;
			var tempcaseText = document.getElementById('case').options[document.getElementById('case').selectedIndex].text;
			$.post("http://curismed.com/medService/cases/load",
		    {
		        caseChartNo: tempcaseText,
		    },
		    function(data, status){
		    	if(data.length != 0){
		    		if(data[0].principalDiag != ""){
				    	document.getElementById("diag1").value = data[0].principalDiag;
				    }
				    if(data[0].defDiag2 != ""){
				    	document.getElementById("diag2").value = data[0].defDiag2;
				    }
				    if(data[0].defDiag3 != ""){
				    	document.getElementById("diag3").value = data[0].defDiag3;
				    }
				    if(data[0].defDiag4 != ""){
				    	document.getElementById("diag4").value = data[0].defDiag4;
				    }
			    }
		    });	
	}

	function tabList(tempcaseText){
		var temp="";
		$.post("http://curismed.com/medService/policies/load",
		    {
		        policyEntity: '1',
		        caseChartNo : tempcaseText
		    },
		    function(data1, status){
		    	if(data1.length != 0){
		    		var insName1 = data1[0].InsuranceName;
			    	temp += "<div class=\"tabbable tabbable-custom\"><ul class=\"nav nav-tabs\"><li class=\"active\"><a href=\"#tab_1_1\" id=\""+data1[0].insuranceID+"\" class=\"tabID\" data-toggle=\"tab\">"+insName1+"<\/a><\/li>";
				    $.ajax({
		                type: "POST",
		                url:"http://curismed.com/medService/policies/load",
		                async:false,
		                data:{ 
					        policyEntity: '2',
					        caseChartNo : tempcaseText
					    },success:function(data2){
					    	if(data2.length != 0){
					    	  	var insName2 = data2[0].InsuranceName;
						    	temp += "<li><a href=\"#tab_1_2\" id=\""+data2[0].insuranceID+"\" class=\"tabID\" data-toggle=\"tab\">"+insName2+"<\/a><\/li><li><a href=\"#tab_1_3\" data-toggle=\"tab\">Patient<\/a><\/li><\/ul><div class=\"tab-content\" style=\"min-height:100px; min-width:250px;\"><div class=\"tab-pane active\" id=\"tab_1_1\"><div class=\"col-md-6\"><label class=\"control-label col-md-4\">Patient Relationship</label><div class=\"col-md-8\"><input type=\"text\" class=\"form-control\" disabled id=\"tab1Relationship\"/><\/div><\/div><div class=\"col-md-6\"><label class=\"control-label col-md-4\">Insured #</label><div class=\"col-md-8\"><input type=\"text\" class=\"form-control\" disabled id=\"tab1InsuredNo\"/><\/div><\/div><div class=\"clearfix\"><\/div><div class=\"col-md-6\"><label class=\"control-label col-md-4\">Insured Name</label><div class=\"col-md-8\"><input type=\"text\" class=\"form-control\" disabled id=\"tab1InsuredName\"/><\/div><\/div><div class=\"col-md-6\"><label class=\"control-label col-md-4\">Effective Date</label><div class=\"col-md-8\"><input type=\"text\" class=\"form-control\" disabled id=\"tab1EffDate\"/><\/div><\/div><div class=\"clearfix\"><\/div><div class=\"col-md-6\"><label class=\"control-label col-md-4\">Copay</label><div class=\"col-md-8\"><input type=\"text\" class=\"form-control\" disabled id=\"tab1Copay\"/><\/div><\/div><div class=\"col-md-6\"><label class=\"control-label col-md-4\">Patient DOB</label><div class=\"col-md-8\"><input type=\"text\" class=\"form-control\" disabled id=\"tab1patDOB\"/><\/div><\/div><div class=\"clearfix\"><\/div><hr><p style=\"color:green\">Previous DOS 'MM-DD-YYYY' was created on 'MM-DD-YYYY' and the Status is 'Paid'</p><\/div><div class=\"tab-pane\" id=\"tab_1_2\"><div class=\"col-md-6\"><label class=\"control-label col-md-4\">Patient Relationship</label><div class=\"col-md-8\"><input type=\"text\" class=\"form-control\" disabled id=\"tab2Relationship\"/><\/div><\/div><div class=\"col-md-6\"><label class=\"control-label col-md-4\">Insured #</label><div class=\"col-md-8\"><input type=\"text\" class=\"form-control\" disabled id=\"tab2InsuredNo\"/><\/div><\/div><div class=\"clearfix\"><\/div><div class=\"col-md-6\"><label class=\"control-label col-md-4\">Insured Name</label><div class=\"col-md-8\"><input type=\"text\" class=\"form-control\" disabled id=\"tab2InsuredName\"/><\/div><\/div><div class=\"col-md-6\"><label class=\"control-label col-md-4\">Effective Date</label><div class=\"col-md-8\"><input type=\"text\" class=\"form-control\" disabled id=\"tab2EffDate\"/><\/div><\/div><div class=\"clearfix\"><\/div><div class=\"col-md-6\"><label class=\"control-label col-md-4\">Copay</label><div class=\"col-md-8\"><input type=\"text\" class=\"form-control\" disabled id=\"tab2Copay\"/><\/div><\/div><div class=\"col-md-6\"><label class=\"control-label col-md-4\">Patient DOB</label><div class=\"col-md-8\"><input type=\"text\" class=\"form-control\" disabled id=\"tab2patDOB\"/><\/div><\/div><div class=\"clearfix\"><\/div><hr><p style=\"color:green\">Previous DOS 'MM-DD-YYYY' was created on 'MM-DD-YYYY' and the Status is 'Paid'</p><\/div><div class=\"tab-pane\" id=\"tab_1_3\">Bill Send to <select style=\"form-control\"><option>Self</option><option>Guarantor</option><\/select><div class=\"clearfix\"><\/div><hr><p style=\"color:green\">Previous Statement for DOS 'MM-DD-YYYY' was sent on 'MM-DD-YYYY' and the Status is 'Paid'</p><\/div><\/div><\/div>";
						    }
						    else{
						    	temp += "<li><a href=\"#tab_1_2\"id=\"-100\" class=\"tabID\" data-toggle=\"tab\">Patient<\/a><\/li><\/ul><div class=\"tab-content\" style=\"min-height:100px; min-width:250px;\"><div class=\"tab-pane active\" id=\"tab_1_1\"><div class=\"col-md-6\"><label class=\"control-label col-md-4\">Patient Relationship</label><div class=\"col-md-8\"><input type=\"text\" class=\"form-control\" disabled id=\"tab1Relationship\"/><\/div><\/div><div class=\"col-md-6\"><label class=\"control-label col-md-4\">Insured #</label><div class=\"col-md-8\"><input type=\"text\" class=\"form-control\" disabled id=\"tab1InsuredNo\"/><\/div><\/div><div class=\"clearfix\"><\/div><div class=\"col-md-6\"><label class=\"control-label col-md-4\">Insured Name</label><div class=\"col-md-8\"><input type=\"text\" class=\"form-control\" disabled id=\"tab1InsuredName\"/><\/div><\/div><div class=\"col-md-6\"><label class=\"control-label col-md-4\">Effective Date</label><div class=\"col-md-8\"><input type=\"text\" class=\"form-control\" disabled id=\"tab1EffDate\"/><\/div><\/div><div class=\"clearfix\"><\/div><div class=\"col-md-6\"><label class=\"control-label col-md-4\">Copay</label><div class=\"col-md-8\"><input type=\"text\" class=\"form-control\" disabled id=\"tab1Copay\"/><\/div><\/div><div class=\"col-md-6\"><label class=\"control-label col-md-4\">Patient DOB</label><div class=\"col-md-8\"><input type=\"text\" class=\"form-control\" disabled id=\"tab1patDOB\"/><\/div><\/div><div class=\"clearfix\"><\/div><hr><p style=\"color:green\">Previous DOS 'MM-DD-YYYY' was created on 'MM-DD-YYYY' and the Status is 'Paid'</p><\/div><div class=\"tab-pane\" id=\"tab_1_2\">Bill Send to <select style=\"form-control\"><option>Self</option><option>Guarantor</option><\/select><div class=\"clearfix\"><\/div><hr><p style=\"color:green\">Previous Statement for DOS 'MM-DD-YYYY' was sent on 'MM-DD-YYYY' and the Status is 'Paid'</p><\/div><\/div><\/div>";
						    }
						}
				    });
				    	
		    	}
		    	else{
		    		$.ajax({
		                type: "POST",
		                url:"http://curismed.com/medService/policies/load",
		                async:false,
		                data:{ 
					        policyEntity: '2',
					        caseChartNo : tempcaseText
					    },success:function(data2){
					    	if(data2.length != 0){
					    	  	var insName2 = data2[0].InsuranceName;
						    	temp += "<div class=\"tabbable tabbable-custom\"><ul class=\"nav nav-tabs\"><li class=\"active\"><a href=\"#tab_1_1\" id=\""+data2[0].insuranceID+"\" class=\"tabID\" data-toggle=\"tab\">"+insName2+"<\/a><\/li><li><a href=\"#tab_1_2\" data-toggle=\"tab\">Patient<\/a><\/li><\/ul><div class=\"tab-content\" style=\"min-height:100px; min-width:250px;\"><div class=\"tab-pane active\" id=\"tab_1_1\"><div class=\"col-md-6\"><label class=\"control-label col-md-4\">Patient Relationship</label><div class=\"col-md-8\"><input type=\"text\" class=\"form-control\" disabled id=\"tab1Relationship\"/><\/div><\/div><div class=\"col-md-6\"><label class=\"control-label col-md-4\">Insured #</label><div class=\"col-md-8\"><input type=\"text\" class=\"form-control\" disabled id=\"tab1InsuredNo\"/><\/div><\/div><div class=\"clearfix\"><\/div><div class=\"col-md-6\"><label class=\"control-label col-md-4\">Insured Name</label><div class=\"col-md-8\"><input type=\"text\" class=\"form-control\" disabled id=\"tab1InsuredName\"/><\/div><\/div><div class=\"col-md-6\"><label class=\"control-label col-md-4\">Effective Date</label><div class=\"col-md-8\"><input type=\"text\" class=\"form-control\" disabled id=\"tab1EffDate\"/><\/div><\/div><div class=\"clearfix\"><\/div><div class=\"col-md-6\"><label class=\"control-label col-md-4\">Copay</label><div class=\"col-md-8\"><input type=\"text\" class=\"form-control\" disabled id=\"tab1Copay\"/><\/div><\/div><div class=\"col-md-6\"><label class=\"control-label col-md-4\">Patient DOB</label><div class=\"col-md-8\"><input type=\"text\" class=\"form-control\" disabled id=\"tab1patDOB\"/><\/div><\/div><div class=\"clearfix\"><\/div><hr><p style=\"color:green\">Previous DOS 'MM-DD-YYYY' was created on 'MM-DD-YYYY' and the Status is 'Paid'</p><\/div><div class=\"tab-pane\" id=\"tab_1_2\">Bill Send to <select style=\"form-control\"><option>Self</option><option>Guarantor</option><\/select><div class=\"clearfix\"><\/div><hr><p style=\"color:green\">Previous Statement for DOS 'MM-DD-YYYY' was sent on 'MM-DD-YYYY' and the Status is 'Paid'</p><\/div><\/div><\/div>";
						    }
						    else{	
						    	temp += "<div class=\"tabbable tabbable-custom\"><ul class=\"nav nav-tabs\"><li class=\"active\"><a href=\"#tab_1_1\"id=\"-100\" class=\"tabID\" data-toggle=\"tab\">Patient<\/a><\/li><\/ul><div class=\"tab-content\" style=\"min-height:100px; min-width:250px;\"><div class=\"tab-pane active\" id=\"tab_1_1\">Bill Send to <select style=\"form-control\"><option>Self</option><option>Guarantor</option><\/select><div class=\"clearfix\"><\/div><hr><p style=\"color:green\">Previous Statement for DOS 'MM-DD-YYYY' was sent on 'MM-DD-YYYY' and the Status is 'Paid'</p><\/div><\/div><\/div>";
						    }
						}
				    });
		    	}
		    	document.getElementById("insuranceTabList").innerHTML = temp;
		    	$.post("http://curismed.com/medService/cases/casechart",
			    {
			        caseChartNo : tempcaseText
			    },
			    function(data1, status){
			    	if(data1.length != 0){
			    		if(data1.length == 2){
				    		document.getElementById('tab1Relationship').value = data1[0].relationshipToInsured;
				    		document.getElementById('tab1InsuredNo').value = data1[0].policyNo;
				    		$.ajax({
	                              type: "POST",
	                              url:"getPatientInfo.php",
	                              async : false,
	                              data:{
	                                patientID : data1[0].patientID
	                              },success:function(result){
	                                    document.getElementById('tab1patDOB').value = changeDateFormat(result);
	                                    document.getElementById('tab2patDOB').value = changeDateFormat(result);
	                             }
	                          });
				    		$.ajax({
	                              type: "POST",
	                              url:"getInsuranceName.php",
	                              async : false,
	                              data:{
	                                insuranceID : data1[0].insuranceID
	                              },success:function(result){
	                                var res11 = JSON.parse(result);
	                                if(res11.length!= ""){
	                                    document.getElementById('tab1InsuredName').value = res11[0].payerName;
	                                 }
	                             }
	                          });
				    		document.getElementById('tab1EffDate').value = changeDateFormat(data1[0].effStartDt);
				    		document.getElementById('tab1Copay').value = data1[0].coPayment;
				    		document.getElementById('tab2Relationship').value = data1[1].relationshipToInsured;
				    		document.getElementById('tab2InsuredNo').value = data1[1].policyNo;
				    		$.ajax({
	                              type: "POST",
	                              url:"getInsuranceName.php",
	                              async : false,
	                              data:{
	                                insuranceID : data1[1].insuranceID
	                              },success:function(result){
	                                var res11 = JSON.parse(result);
	                                if(res11.length!= ""){
	                                    document.getElementById('tab2InsuredName').value = res11[0].payerName;
	                                 }
	                             }
	                          });
				    		document.getElementById('tab2EffDate').value = changeDateFormat(data1[1].effStartDt);
				    		document.getElementById('tab2Copay').value = data1[1].coPayment;
				    	}
				    	else if(data1.length == 1){
				    		document.getElementById('tab1Relationship').value = data1[0].relationshipToInsured;
				    		document.getElementById('tab1InsuredNo').value = data1[0].policyNo;
				    		$.ajax({
	                              type: "POST",
	                              url:"getPatientInfo.php",
	                              async : false,
	                              data:{
	                                patientID : data1[0].patientID
	                              },success:function(result){
	                                    document.getElementById('tab1patDOB').value = changeDateFormat(result);
	                                    document.getElementById('tab2patDOB').value = changeDateFormat(result);
	                             }
	                          });
				    		$.ajax({
	                              type: "POST",
	                              url:"getInsuranceName.php",
	                              async : false,
	                              data:{
	                                insuranceID : data1[0].insuranceID
	                              },success:function(result){
	                                var res11 = JSON.parse(result);
	                                if(res11.length!= ""){
	                                    document.getElementById('tab1InsuredName').value = res11[0].payerName;
	                                 }
	                             }
	                          });
				    		document.getElementById('tab1EffDate').value = changeDateFormat(data1[0].effStartDt);
				    		document.getElementById('tab1Copay').value = data1[0].coPayment;
				    	}
			    	}
			    });
		    });	
	}
	function changeDateFormat(inputDate){  // expects Y-m-d
        var splitDate = inputDate.split('-');
        if(splitDate.count == 0){
            return null;
        }

        var year = splitDate[0];
        var month = splitDate[1];
        var day = splitDate[2]; 

        return month + '-' + day + '-' + year;
    }
	function Cases(caseVal){
		$.post("http://curismed.com/medService/claims",
		    {
		        caseID: caseVal,
		    },
		    function(data, status){
		    	var tbody = $('#body');
			    $('#body').html('');
			    //var tr = $('<tr/>').appendTo(tbody);
			    for (var i = 0; i < data.length; i++) {
			    	var locName = "";
			    	$.ajax({
		                type: "POST",
		                url:"getLocName.php",
		                async : false,
		                data:{
		                	serviceLocID : data[i].serviceLocID
	                	},success:function(result){
	                        locName = result;
	                     }
		            });
			    	
					var fromDt = changeDateFormat(data[i].fromDt);
					var toDt = changeDateFormat(data[i].toDt);
			            $('#body').append('<tr><td>' + fromDt + '</td><td>' + toDt + '</td><td>' + data[i].proced + '</td><td>' + data[i].mod1 + '</td><td>' + data[i].units + '</td><td>' + data[i].charge + '</td><td>' + data[i].total + '</td><td>' + data[i].diag1 + '</td><td>' + data[i].diag2 + '</td><td>' + data[i].diag3 + '</td><td>' + data[i].diag4 + '</td><td>' + data[i].PrimaryCarePhysician + '</td><td>' + locName + '</td><td>' + data[i].claimStatus + '</td></tr>');
			    }
			    $('#body').append('<tr><td><input type="text" class="form-control date" style="width:70px;" id="fromDt"/></td><td><input type="text" class="form-control date" style="width:70px;" id="toDt"/></td><td><input type="text" class="form-control cpt" style="width:60px;float:left;display:inline-flex;" id="proced" title="" /><a href="#" id="linkCPT" style="padding:5px 0 0 5px;"><i class="icon icon-plus" style="font-size:10px; color:maroon"></i></a></td><td><input type="text" class="form-control mod" style="width:30px;" id="mod1" /></td><td><input type="text" class="form-control clsUnits" style="width:30px;" id="units" value="1" /></td><td><input type="text" class="form-control clsCharge" style="width:50px;" id="charge" /></td><td><input type="text" class="form-control tot" style="width:50px;" id="total" /></td><td><input type="text" class="form-control icd" style="width:50px;float:left;display:inline-flex;" id="diag1" /><a href="#" class="linkICD" style="padding:5px 0 0 5px;"><i class="icon icon-plus" style="font-size:10px; color:maroon"></i></a></td><td><input type="text" class="form-control icd" style="width:50px;float:left;display:inline-flex;" id="diag2" /><a href="#"  class="linkICD" style="padding:5px 0 0 5px;"><i class="icon icon-plus" style="font-size:10px; color:maroon"></i></a></td><td><input type="text" class="form-control icd" style="width:50px;float:left;display:inline-flex;" id="diag3" /><a href="#" class="linkICD" style="padding:5px 0 0 5px;"><i class="icon icon-plus" style="font-size:10px; color:maroon"></i></a></td><td><input type="text" class="form-control icd" style="width:50px;float:left;display:inline-flex;" id="diag4" /><a href="#" class="linkICD" style="padding:5px 0 0 5px;"><i class="icon icon-plus" style="font-size:10px; color:maroon"></i></a></td><td><input type="text" class="form-control phy" style="width:50px;float:left;display:inline-flex;" id="provider" /><a href="#" id="linkPrimaryPhysi" style="padding:5px 0 0 5px;"><i class="icon icon-plus" style="font-size:10px; color:maroon"></i></a></td><td><input type="text" class="form-control loc" style="width:50px;float:left;display:inline-flex;" id="location" /><a href="#" id="linkLOC" style="padding:5px 0 0 5px;"><i class="icon icon-plus" style="font-size:10px; color:maroon"></i></a></td></td><td><select id="ddlStatus" class="form-control status"><option value="COMPLETE">Complete</option><option value="DRAFT">Draft</option><option value="REVIEW">Review</option></select></td></tr>');
			});
			var tempCase = document.getElementById('case').value;
			var tempcaseText = document.getElementById('case').options[document.getElementById('case').selectedIndex].text;
			$.post("http://curismed.com/medService/cases/load",
		    {
		        caseChartNo: tempcaseText,
		    },
		    function(data, status){
		    	if(data.length != 0){
			    	document.getElementById("diag1").value = data[0].principalDiag;
			    	document.getElementById("diag2").value = data[0].defDiag2;
			    	document.getElementById("diag3").value = data[0].defDiag3;
			    	document.getElementById("diag4").value = data[0].defDiag4;
			    }
		    });
	}
	function CasesCon(caseVal){
		$.post("http://curismed.com/medService/claims",
		    {
		        caseID: caseVal,
		    },
		    function(data, status){
		    	var tbody = $('#body');
			    $('#body').html('');
			    //var tr = $('<tr/>').appendTo(tbody);
			    for (var i = 0; i < data.length; i++) {
			    	var locName = "";
			    	$.ajax({
		                type: "POST",
		                url:"getLocName.php",
		                async : false,
		                data:{
		                	serviceLocID : data[i].serviceLocID
	                	},success:function(result){
	                        locName = result;
	                     }
		            });
			    	
					var fromDt = changeDateFormat(data[i].fromDt);
					var toDt = changeDateFormat(data[i].toDt);
			            $('#body').append('<tr><td>' + fromDt + '</td><td>' + toDt + '</td><td>' + data[i].proced + '</td><td>' + data[i].mod1 + '</td><td>' + data[i].units + '</td><td>' + data[i].charge + '</td><td>' + data[i].total + '</td><td>' + data[i].diag1 + '</td><td>' + data[i].diag2 + '</td><td>' + data[i].diag3 + '</td><td>' + data[i].diag4 + '</td><td>' + data[i].PrimaryCarePhysician + '</td><td>' + locName + '</td><td>' + data[i].claimStatus + '</td></tr>');
			    }
			    $('#body').append('<tr><td><input type="text" class="form-control date" style="width:70px;" id="fromDt"/></td><td><input type="text" class="form-control date" style="width:70px;" id="toDt"/></td><td><input type="text" class="form-control cpt" style="width:60px;float:left;display:inline-flex;" id="proced" title="" /><a href="#" id="linkCPT" style="padding:5px 0 0 5px;"><i class="icon icon-plus" style="font-size:10px; color:maroon"></i></a></td><td><input type="text" class="form-control mod" style="width:30px;" id="mod1" /></td><td><input type="text" class="form-control clsUnits" style="width:30px;" id="units" value="1" /></td><td><input type="text" class="form-control clsCharge" style="width:50px;" id="charge" /></td><td><input type="text" class="form-control tot" style="width:50px;" id="total" /></td><td><input type="text" class="form-control icd" style="width:50px;float:left;display:inline-flex;" id="diag1" /><a href="#" class="linkICD" style="padding:5px 0 0 5px;"><i class="icon icon-plus" style="font-size:10px; color:maroon"></i></a></td><td><input type="text" class="form-control icd" style="width:50px;float:left;display:inline-flex;" id="diag2" /><a href="#"  class="linkICD" style="padding:5px 0 0 5px;"><i class="icon icon-plus" style="font-size:10px; color:maroon"></i></a></td><td><input type="text" class="form-control icd" style="width:50px;float:left;display:inline-flex;" id="diag3" /><a href="#" class="linkICD" style="padding:5px 0 0 5px;"><i class="icon icon-plus" style="font-size:10px; color:maroon"></i></a></td><td><input type="text" class="form-control icd" style="width:50px;float:left;display:inline-flex;" id="diag4" /><a href="#" class="linkICD" style="padding:5px 0 0 5px;"><i class="icon icon-plus" style="font-size:10px; color:maroon"></i></a></td><td><input type="text" class="form-control phy" style="width:50px;float:left;display:inline-flex;" id="provider" /><a href="#" id="linkPrimaryPhysi" style="padding:5px 0 0 5px;"><i class="icon icon-plus" style="font-size:10px; color:maroon"></i></a></td><td><input type="text" class="form-control loc" style="width:50px;float:left;display:inline-flex;" id="location" /><a href="#" id="linkLOC" style="padding:5px 0 0 5px;"><i class="icon icon-plus" style="font-size:10px; color:maroon"></i></a></td></td><td><select id="ddlStatus" class="form-control status"><option value="COMPLETE">Complete</option><option value="DRAFT">Draft</option><option value="REVIEW">Review</option></select></td></tr>');
			    $('.newclass').focus();
		    });
	}
    var convertDate = function(usDate) {
      var dateParts = usDate.split(/(\d{1,2})-(\d{1,2})-(\d{4})/);
      return dateParts[3] + "-" + dateParts[1] + "-" + dateParts[2];
    }

	function changeDateFormat(inputDate){  // expects Y-m-d
        var splitDate = inputDate.split('-');
        if(splitDate.count == 0){
            return null;
        }

        var year = splitDate[0];
        var month = splitDate[1];
        var day = splitDate[2]; 

        return month + '-' + day + '-' + year;
    }
	
	</script>
	<style>
	.icon:hover{
		text-decoration: none;
	}
	a:hover{
		text-decoration: none;
	}
	</style>
	
	<!-- Demo JS -->
	<script type="text/javascript" src="assets/js/custom.js"></script>
	<script type="text/javascript" src="assets/js/demo/ui_general.js"></script>
</head>

<body>

	<!-- Header -->
	<header class="header navbar navbar-fixed-top" role="banner" style="background-image: url('assets/bg.jpg'); background-repeat: repeat-x;">
		<!-- Top Navigation Bar -->
		<div class="container">

			<!-- Only visible on smartphones, menu toggle -->
			<ul class="nav navbar-nav">
				<li class="nav-toggle"><a href="javascript:void(0);" title=""><i class="icon-reorder"></i></a></li>
			</ul>

			<!-- Logo -->
			<a class="navbar-brand" style="text-align:center;padding-right:50px;" href="index.html">
				<img src="assets/logo2.png"/>
				<strong>medABA</strong>
			</a>
			<!-- /logo -->

			<!-- Sidebar Toggler -->
			<a href="#" class="toggle-sidebar bs-tooltip" data-placement="bottom" data-original-title="Toggle navigation">
				<i class="icon-reorder"></i>
			</a>
			<!-- /Sidebar Toggler -->

			<!-- Top Left Menu -->
            <ul class="nav navbar-nav navbar-left hidden-xs hidden-sm" style="list-style:none;">
                <li>
                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                        Appointment
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                        Patients
                        <i class="icon-caret-down small"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="AddPatient.php">
                            <i class="icon-angle-right"></i>
                            Add Patients
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="ViewPatient.php">
                            <i class="icon-angle-right"></i>
                            Find Patients
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                        Claims
                        <i class="icon-caret-down small"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="AddClaim.php">
                            <i class="icon-angle-right"></i>
                            Add Claims
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="ViewClaims.php">
                            <i class="icon-angle-right"></i>
                            View Claims
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="Ledger.php">
                            <i class="icon-angle-right"></i>
                            Ledger
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="javascript:void(0);" style="font-weight:600;">
                            EDI
                            </a>
                            <li>
                                <a href="EDIList.php">
                                <i class="icon-angle-right"></i>
                                View Claims batch
                                </a>
                            </li>
                            <li>
                                <a href="generateEDI.php">
                                <i class="icon-angle-right"></i>
                                Generate EDI
                                </a>
                            </li>
                            <li>
                                <a href="generateAllEDI.php">
                                <i class="icon-angle-right"></i>
                                Generate EDI Batch
                                </a>
                            </li>
                            <li>
                                <a href="generatePaperClaim.php">
                                <i class="icon-angle-right"></i>
                                Print Paper Claims
                                </a>
                            </li>
                            <li>
                                <a href="generateEOB.php">
                                <i class="icon-angle-right"></i>
                                Patient Statements
                                </a>
                            </li>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                        Financial
                        <i class="icon-caret-down small"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="Deposit.php">
                            <i class="icon-angle-right"></i>
                            View Deposits
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="charts.html" class="dropdown-toggle" data-toggle="dropdown">
                        Reports
                        <i class="icon-caret-down small"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="javascript:voic(0);">
                            <i class="icon-angle-right"></i>
                             Accounts Receivable
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="javascript:voic(0);">
                            <i class="icon-angle-right"></i>
                            Productivity
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="javascript:voic(0);">
                            <i class="icon-angle-right"></i>
                            Patients
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="javascript:voic(0);">
                            <i class="icon-angle-right"></i>
                            Appointments
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="javascript:voic(0);">
                            <i class="icon-angle-right"></i>
                            Claims
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="javascript:voic(0);">
                            <i class="icon-angle-right"></i>
                            Payments
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="javascript:voic(0);">
                            <i class="icon-angle-right"></i>
                            Refunds
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="charts.html" class="dropdown-toggle" data-toggle="dropdown">
                        Settings
                        <i class="icon-caret-down small"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="ViewPractice.php">
                            <i class="icon-angle-right"></i>
                            Practices
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="ViewPhysician.php">
                            <i class="icon-angle-right"></i>
                            Physicians
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="ViewServiceLoc.php">
                            <i class="icon-angle-right"></i>
                            Locations &amp; Facilities
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="javascript:void(0);" style="font-weight:600;">
                            Codes
                            </a>
                            <li>
                                <a href="ViewCPT.php">
                                <i class="icon-angle-right"></i>
                                CPT/Procedure
                                </a>
                            </li>
                            <li>
                                <a href="ViewDX.php">
                                <i class="icon-angle-right"></i>
                                ICD 10/ICD 9 Library
                                </a>
                            </li>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="insurances.php">
                            <i class="icon-angle-right"></i>
                            Insurances
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="selectThemes.php">
                            <i class="icon-angle-right"></i>
                            Themes
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="CRM.php">
                            <i class="icon-angle-right"></i>
                            CRM - AR
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- /Top Left Menu -->

			<!-- Top Right Menu -->
			<ul class="nav navbar-nav navbar-right">
				<!-- Notifications -->
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="icon-warning-sign"></i>
						<span class="badge">5</span>
					</a>
					<ul class="dropdown-menu extended notification">
						<li class="title">
							<p>You have 5 new notifications</p>
						</li>
						<li>
							<a href="javascript:void(0);">
								<span class="label label-success"><i class="icon-plus"></i></span>
								<span class="message">New user registration.</span>
								<span class="time">1 mins</span>
							</a>
						</li>
						<li>
							<a href="javascript:void(0);">
								<span class="label label-danger"><i class="icon-warning-sign"></i></span>
								<span class="message">High CPU load on cluster #2.</span>
								<span class="time">5 mins</span>
							</a>
						</li>
						<li>
							<a href="javascript:void(0);">
								<span class="label label-success"><i class="icon-plus"></i></span>
								<span class="message">New user registration.</span>
								<span class="time">10 mins</span>
							</a>
						</li>
						<li>
							<a href="javascript:void(0);">
								<span class="label label-info"><i class="icon-bullhorn"></i></span>
								<span class="message">New items are in queue.</span>
								<span class="time">25 mins</span>
							</a>
						</li>
						<li>
							<a href="javascript:void(0);">
								<span class="label label-warning"><i class="icon-bolt"></i></span>
								<span class="message">Disk space to 85% full.</span>
								<span class="time">55 mins</span>
							</a>
						</li>
						<li class="footer">
							<a href="javascript:void(0);">View all notifications</a>
						</li>
					</ul>
				</li>

				<!-- Tasks -->
				<li class="dropdown hidden-xs hidden-sm">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="icon-tasks"></i>
						<span class="badge">7</span>
					</a>
					<ul class="dropdown-menu extended notification">
						<li class="title">
							<p>You have 7 pending tasks</p>
						</li>
						<li>
							<a href="javascript:void(0);">
								<span class="task">
									<span class="desc">Preparing new release</span>
									<span class="percent">30%</span>
								</span>
								<div class="progress progress-small">
									<div style="width: 30%;" class="progress-bar progress-bar-info"></div>
								</div>
							</a>
						</li>
						<li>
							<a href="javascript:void(0);">
								<span class="task">
									<span class="desc">Change management</span>
									<span class="percent">80%</span>
								</span>
								<div class="progress progress-small progress-striped active">
									<div style="width: 80%;" class="progress-bar progress-bar-danger"></div>
								</div>
							</a>
						</li>
						<li>
							<a href="javascript:void(0);">
								<span class="task">
									<span class="desc">Mobile development</span>
									<span class="percent">60%</span>
								</span>
								<div class="progress progress-small">
									<div style="width: 60%;" class="progress-bar progress-bar-success"></div>
								</div>
							</a>
						</li>
						<li>
							<a href="javascript:void(0);">
								<span class="task">
									<span class="desc">Database migration</span>
									<span class="percent">20%</span>
								</span>
								<div class="progress progress-small">
									<div style="width: 20%;" class="progress-bar progress-bar-warning"></div>
								</div>
							</a>
						</li>
						<li class="footer">
							<a href="javascript:void(0);">View all tasks</a>
						</li>
					</ul>
				</li>

				<!-- Messages -->
				<li class="dropdown hidden-xs hidden-sm">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="icon-envelope"></i>
						<span class="badge">1</span>
					</a>
					<ul class="dropdown-menu extended notification">
						<li class="title">
							<p>You have 3 new messages</p>
						</li>
						<li>
							<a href="javascript:void(0);">
								<span class="photo"><img src="assets/img/demo/avatar-1.jpg" alt="" /></span>
								<span class="subject">
									<span class="from">Bob Carter</span>
									<span class="time">Just Now</span>
								</span>
								<span class="text">
									Consetetur sadipscing elitr...
								</span>
							</a>
						</li>
						<li>
							<a href="javascript:void(0);">
								<span class="photo"><img src="assets/img/demo/avatar-2.jpg" alt="" /></span>
								<span class="subject">
									<span class="from">Jane Doe</span>
									<span class="time">45 mins</span>
								</span>
								<span class="text">
									Sed diam nonumy...
								</span>
							</a>
						</li>
						<li>
							<a href="javascript:void(0);">
								<span class="photo"><img src="assets/img/demo/avatar-3.jpg" alt="" /></span>
								<span class="subject">
									<span class="from">Patrick Nilson</span>
									<span class="time">6 hours</span>
								</span>
								<span class="text">
									No sea takimata sanctus...
								</span>
							</a>
						</li>
						<li class="footer">
							<a href="javascript:void(0);">View all messages</a>
						</li>
					</ul>
				</li>

				<!-- .row .row-bg Toggler -->
				<li>
					<a href="#" class="dropdown-toggle row-bg-toggle">
						<i class="icon-resize-vertical"></i>
					</a>
				</li>


				<!-- User Login Dropdown -->
				<li class="dropdown user">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<!--<img alt="" src="assets/img/avatar1_small.jpg" />-->
						<i class="icon-male"></i>
						<span class="username" id="practiceName"></span>
						<i class="icon-caret-down small"></i>
					</a>
					<ul class="dropdown-menu">
						<li><a href="javascript:void(0);"><i class="icon-user"></i> My Profile</a></li>
						<li><a href="javascript:void(0);"><i class="icon-calendar"></i> My Calendar</a></li>
						<li><a href="javascript:void(0);"><i class="icon-tasks"></i> My Tasks</a></li>
						<li class="divider"></li>
						<li><a href="login.php"><i class="icon-key"></i> Log Out</a></li>
					</ul>
				</li>
				<!-- /user login dropdown -->
			</ul>
			<!-- /Top Right Menu -->
		</div>
		<!-- /top navigation bar -->

	</header> <!-- /.header -->

	<div id="container">
		<!-- Breadcrumbs line -->
		<div class="crumbs">
			<ul id="breadcrumbs" class="breadcrumb">
				<li class="current">
					<i class="icon-home"></i>
					<a href="index.html">Dashboard</a>
				</li>
				
			</ul>
			<a href="javascript:void(0);"><img src="assets/icons/Settings.png" title="Settings" style="float:right; padding-top:0.3em; padding-right:2em;"/></a>
					<a href="Ledger.php"><img src="assets/icons/Claim_search.png" title="Claim Search" style="float:right; padding-top:0.3em; padding-right:2em;"/></a>
					<a href="AddClaim.php"><img src="assets/icons/Claim_add.png" title="Add Claim" style="float:right; padding-top:0.3em; padding-right:2em;"/></a>
					<a href="ViewPatient.php"><img src="assets/icons/Patient_search.png" title="Patient Search" style="float:right; padding-top:0.3em; padding-right:2em;"/></a>
					<a href="AddPatient.php"><img src="assets/icons/Patient_add.png" title="Add Patient" style="float:right; padding-top:0.3em; padding-right:2em;"/></a>
					<a href="dashboard.html"><img src="assets/icons/Home.png" title="Home" style="float:right; padding-top:0.3em; padding-right:2em;"/></a>

		</div>
		<div class="clearfix"></div>
	    <div class="alert alert-warning fade in" style="margin-top:10px;" id="divAlert">
	        <i class="icon-remove close" data-dismiss="alert"></i>
	        <strong>Warning!</strong> <span id="alertTop">Your Payment Due is on 05/22/2016</span>
	    </div>
		<!-- /Breadcrumbs line -->
		<div id="content">
			<img src="assets/beat.gif" id="loader" style="position:absolute; left:45%; top:50%; " />
			<h3 style="margin:20px 20px 20px 20px;">Add Claim</h3>
			<div class="form-group col-md-6" style="margin-bottom:30px;">
                <label class="control-label col-md-4" style="margin-bottom:10px;">Patient</label>
                <div class="col-md-8" style="margin-bottom:10px;">
                    <input type="text" class="form-control required" id="patient" name="patient" />
                </div>

                <label class="control-label col-md-4">Case</label>
                <div class="col-md-8">
                    <select id="case" class="form-control"></select>
                </div>
            </div>
            <div class="form-group col-md-6" id="insuranceTabList">

            </div>
            <div class="clearfix"></div>
            <div class="form-group col-md-4">
            	<label class="checkbox-inline">
                    <input type="checkbox" class="uniform" id="showAll" value=""> Show All Claims
                </label>
            </div>
            <div class="form-group col-md-8 pull-right">
            	<input type="hidden" id="hdnDX" />
            	<div class="form-group col-md-6">
                    <div class="col-md-1"></div>
                    <div class="make-switch" data-on="success" data-on-label="ICD-9" data-off-label="ICD-10" data-off="warning">
                        <input type="checkbox" checked class="toggle" id="icdtoggle" />
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <table class="table table-bordered" style="margin-top:30px;">
            	<thead>
	            	<th style="background-color:#4583e7; color:#fff;">From</th>
	            	<th style="background-color:#4583e7; color:#fff;">To</th>
	            	<th style="background-color:#4583e7; color:#fff;">CPT</th>
	            	<th style="background-color:#4583e7; color:#fff;">Mod 1</th>
	            	<th style="background-color:#4583e7; color:#fff;">Units</th>
	            	<th style="background-color:#4583e7; color:#fff;">Charge</th>
	            	<th style="background-color:#4583e7; color:#fff;">Total</th>
	            	<th style="background-color:#4583e7; color:#fff;">Diag 1</th>
	            	<th style="background-color:#4583e7; color:#fff;">Diag 2</th>
	            	<th style="background-color:#4583e7; color:#fff;">Diag 3</th>
	            	<th style="background-color:#4583e7; color:#fff;">Diag 4</th>
	            	<th style="background-color:#4583e7; color:#fff;">Provider</th>
	            	<th style="background-color:#4583e7; color:#fff;">Location</th>
	            	<th style="background-color:#4583e7; color:#fff;">Status</th>
	            </thead>
	            <tbody id="body">
	            	<tr>
	            		<td><input type="text" class="form-control date" style="width:70px;" id="fromDt"/></td>
	            		<td><input type="text" class="form-control date" style="width:70px;" id="toDt"/></td>
	            		<td><input type="text" class="form-control cpt" style="width:60px;float:left;display:inline-flex;" id="proced" title="" /><a href="#" id="linkCPT" style="padding:5px 0 0 5px;"><i class="icon icon-plus" style="font-size:10px; color:maroon"></i></a></td>
	            		<td><input type="text" class="form-control mod" style="width:30px;" id="mod1" /></td>
	            		<td><input type="text" class="form-control clsUnits" style="width:30px;" id="units" value="1" /></td>
	            		<td><input type="text" class="form-control clsCharge" style="width:50px;" id="charge" /></td>
	            		<td><input type="text" class="form-control tot" style="width:50px;" id="total" /></td>
	            		<td><input type="text" class="form-control icd" style="width:50px;float:left;display:inline-flex;" id="diag1" /><a href="#" class="linkICD" style="padding:5px 0 0 5px;"><i class="icon icon-plus" style="font-size:10px; color:maroon"></i></a></td>
	            		<td><input type="text" class="form-control icd" style="width:50px;float:left;display:inline-flex;" id="diag2" /><a href="#"  class="linkICD" style="padding:5px 0 0 5px;"><i class="icon icon-plus" style="font-size:10px; color:maroon"></i></a></td>
	            		<td><input type="text" class="form-control icd" style="width:50px;float:left;display:inline-flex;" id="diag3" /><a href="#" class="linkICD" style="padding:5px 0 0 5px;"><i class="icon icon-plus" style="font-size:10px; color:maroon"></i></a></td>
	            		<td><input type="text" class="form-control icd" style="width:50px;float:left;display:inline-flex;" id="diag4" /><a href="#" class="linkICD" style="padding:5px 0 0 5px;"><i class="icon icon-plus" style="font-size:10px; color:maroon"></i></a></td>
	            		<td><input type="text" class="form-control phy" style="width:50px;float:left;display:inline-flex;" id="provider" /><a href="#" id="linkPrimaryPhysi" style="padding:5px 0 0 5px;"><i class="icon icon-plus" style="font-size:10px; color:maroon"></i></a></td>
	            		<td><input type="text" class="form-control loc" style="width:50px;float:left;display:inline-flex;" id="location" /><a href="#" id="linkLOC" style="padding:5px 0 0 5px;"><i class="icon icon-plus" style="font-size:10px; color:maroon"></i></a></td></td>
	            		<td><select id="ddlStatus" class="form-control status"><option value="COMPLETE">Complete</option><option value="DRAFT">Draft</option><option value="REVIEW">Review</option></select></td>
	            	</tr>
	            </tbody>
            </table>
		</div>
	</div>
	 <div class="modal fade" id="CPTModal" tabindex="-1">
        <div class="modal-dialog" style="width:40%">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Create New Procedure Code</h4>
                </div>
                <div class="modal-body" style="min-height:150px;">
		            <div class="form-group col-md-12" style="padding-bottom:5%;">
		                <label class="control-label col-md-6">CPT Code </label>
		                <div class="col-md-6">
		                    <input type="text" class="form-control required" id="cptCode" />
		                </div>
		            </div>
		            <div class="form-group col-md-12" style="padding-bottom:5%;">
		                <label class="control-label col-md-6">Description </label>
		                <div class="col-md-6">
		                    <input type="text" class="form-control required" id="description" />
		                </div>
		            </div>
		            <div class="form-group col-md-12" style="padding-bottom:5%;">
		                <label class="control-label col-md-6">Charge Amount </label>
		                <div class="col-md-6">
		                    <input type="text" class="form-control required" id="charge" />
		                </div>
		            </div>
            	</div>
                <div class="modal-footer">
                    <input type="button" class="btn btn-default" data-dismiss="modal" value="Exit" />
                    <input type="button" id="btnCPT" class="btn btn-primary" data-dismiss="modal" value="Save" />
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>

    <div class="modal fade" id="ICDModal" tabindex="-1">
        <div class="modal-dialog" style="width:40%">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Create New Diagnosis Code</h4>
                </div>
                <div class="modal-body" style="min-height:150px;">
		            <div class="form-group col-md-12" style="padding-bottom:5%;">
		                <label class="control-label col-md-6">ICD Code </label>
		                <div class="col-md-6">
		                    <input type="text" class="form-control required" id="icdCode"  />
		                </div>
		            </div>
		            <div class="form-group col-md-12" style="padding-bottom:5%;">
		                <label class="control-label col-md-6">DX Code </label>
		                <div class="col-md-6">
		                    <input type="text" class="form-control required" id="dxCode" />
		                </div>
		            </div>
		            <div class="form-group col-md-12" style="padding-bottom:5%;">
		                <label class="control-label col-md-6">Long Description </label>
		                <div class="col-md-6">
		                    <input type="text" class="form-control required" id="longDescription" />
		                </div>
		            </div>
		            <div class="form-group col-md-12" style="padding-bottom:5%;">
		                <label class="control-label col-md-6">Short Description </label>
		                <div class="col-md-6">
		                    <input type="text" class="form-control required" id="shortDescription" />
		                </div>
		            </div>
		            <div class="form-group col-md-12" style="padding-bottom:5%;">
		                <label class="control-label col-md-6">Type </label>
		                <div class="col-md-6">
		                    <select id="ddlICDType" class="form-control">
		                    	<option value="9">9</option>
		                    	<option value="10">10</option>
		                    </select>
		                </div>
		            </div>
            	</div>
                <div class="modal-footer">
                    <input type="button" class="btn btn-default" data-dismiss="modal" value="Exit" />
                    <input type="button" id="btnICD" class="btn btn-primary" data-dismiss="modal" value="Save" />
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>

    <div class="modal fade" id="PrimaryPhysi" tab-index="-1">
	    <div class="modal-dialog" style="width:70%">
	        <div class="modal-content">
	            <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	                <h4 class="modal-title">Add Primary Physician</h4>
	            </div>
	            <div class="modal-body" style="min-height:150px; padding-bottom:110px;">
	                <div class="form-group col-md-6">
	                    <label class="control-label col-md-4">Full Name </label>
	                    <div class="col-md-8">
	                        <input type="text" class="form-control required" id="PrimaryPhysiName" name="PrimaryPhysiName" placeholder="Name of the Primary Physiciam" value="" />
	                    </div>
	                </div>

	                <div class="form-group col-md-6">
	                    <label class="control-label col-md-4">Speciality </label>
	                    <div class="col-md-8">
	                        <select class="form-control" id="phyEmploymentStatus">
	                            <option></option>
	                            <option>Psychologist</option>
	                            <option>Counselor</option>
	                        </select>
	                    </div>
	                </div>
	                <div class="clearfix"></div>

	                <div class="form-group col-md-6">
	                    <label class="control-label col-md-4">Date Of Birth </label>
	                    <div class="col-md-8">
	                        <input type="text" class="form-control required" id="phyDOB" name="txtPTNumber" placeholder="Date Of Birth of the Primary Physiciam" value="" />
	                    </div>
	                </div>

	                <div class="form-group col-md-6">
	                    <label class="control-label col-md-4">Individual NPI </label>
	                    <div class="col-md-8">
	                        <input type="text" class="form-control required" id="phyNPI" name="txtPTNumber" placeholder="Individual NPI of the Primary Physiciam" value="" />
	                    </div>
	                </div>


	                <div class="form-group col-md-6">
	                    <label class="control-label col-md-4">Social Security # </label>
	                    <div class="col-md-8">
	                        <input type="text" class="form-control required" id="phySSN" name="txtPTNumber" placeholder="SSN of the Primary Physiciam" value="" />
	                    </div>
	                </div>
	                <div class="clearfix"></div>
	                <hr/>
	                <div class="form-group col-md-6">
	                    <label class="control-label col-md-4">Address </label>
	                    <div class="col-md-8">
	                        <textarea class="form-control" cols="5" rows="4" id="phyAddr"></textarea>
	                    </div>
	                </div>

	                <div class="form-group col-md-6" >
	                    <label class="control-label col-md-4">Home</label>
	                    <div class="col-md-8">
	                         <input type="text" class="form-control required" id="phyHomePhone" name="txtPTNumber" placeholder="Enter the Home Phone" value="" />
	                    </div>
	                </div>

	                 <div class="form-group col-md-6">
	                    <label class="control-label col-md-4">Work </label>
	                    <div class="col-md-8">
	                        <input type="text" class="form-control required" id="phyWorkPhone" name="txtPTNumber" placeholder="Enter the Work Phone" value="" />
	                    </div>
	                </div>

	                <div class="form-group col-md-6" >
	                    <label class="control-label col-md-4">Mobile </label>
	                    <div class="col-md-8">
	                        <input type="text" class="form-control required" id="phyMobilePhone" name="txtPTNumber" placeholder="Enter the Mobile Phone" value="" />
	                    </div>
	                </div>
	                <div class="clearfix"></div>
	                <div class="form-group col-md-6">
	                    <label class="control-label col-md-4">Email </label>
	                    <div class="col-md-8">
	                        <input type="text" class="form-control required" id="phyEmail" name="txtPTNumber" placeholder="Enter the Email" value="" />
	                    </div>
	                </div>
	                <div class="form-group col-md-6">
	                    <label class="control-label col-md-4">Fax </label>
	                    <div class="col-md-8">
	                        <input type="text" class="form-control required" id="phyFax" name="txtPTNumber" placeholder="Enter the Fax" value="" />
	                    </div>
	                </div>
	                <div class="clearfix"></div>
	                <hr/>
	                <div class="form-group col-md-12">
	                    <label class="control-label col-md-2">Notes </label>
	                    <div class="col-md-8">
	                        <textarea class="form-control" cols="20" rows="6" id="phyNotes"></textarea>
	                    </div>
	                </div>

	            </div>
	            <div class="modal-footer">
	                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	                <input type="button" value="Add Primary Physician" class="btn btn-primary" id="btnAddPriPhy"/>
	            </div>
	        </div><!-- /.modal-content -->
	    </div><!-- /.modal-dialog -->
	</div>

    <div class="modal fade" id="AddServLoc" tabindex="-1">
		<div class="modal-dialog" style="width:60%">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Add New Facility</h4>
				</div>
				<div class="modal-body" style="min-height:400px;">
                    <div class="form-group col-md-6">
                        <label class="control-label col-md-12">Internal Name </label>
                        <div class="col-md-12">
                            <input type="text" id="FacInternalName" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label class="control-label col-md-12">Time Zone </label>
                        <div class="col-md-12">
                            <input type="text" id="FacTimeZone" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label class="control-label col-md-12">NPI </label>
                        <div class="col-md-12">
                            <input type="text" id="FacNPI" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label class="control-label col-md-12">Legacy Number Type </label>
                        <div class="col-md-12">
                            <input type="text" id="FacLegNoType" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group col-md-6" style="margin-top:20px;">
                        <label class="checkbox-inline col-md-5">
                            <input type="checkbox" class="uniform" style="margin-left:5px;" value="" id="EIN"> &nbsp;&nbsp;Override EIN ?
                        </label>
                    </div>
                    <div class="form-group col-md-6">
                        <label class="control-label col-md-12">Legacy Number </label>
                        <div class="col-md-12">
                            <input type="text" id="FacLegNo" class="form-control" />
                        </div>
                    </div>
                    <div class="clearfix"></div>
                                                    
                    <div class="form-group col-md-6">
                        <label class="control-label col-md-6" style="text-align:left;"><span style="color:blue">Billing Name &amp; Address</span></label><br/>
                    </div>
                    <div class="clearfix"></div>
                    <hr style="margin-top:0;margin-bottom:0;">
                    
                    <div class="form-group col-md-6">
                        <label class="control-label col-md-12">Billing Name </label>
                        <div class="col-md-12">
                            <input type="text" id="FacBillingName" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label class="control-label col-md-12">Place of Service </label>
                        <div class="col-md-12">
                            <input type="text" id="FacPOS" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label class="control-label col-md-12">Phone Number </label>
                        <div class="col-md-12">
                            <input type="text" id="FacPhone" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label class="control-label col-md-12">Address </label>
                        <div class="col-md-12">
                            <textarea class="form-control" id="FacAddr"></textarea>
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label class="control-label col-md-12">Fax </label>
                        <div class="col-md-12">
                            <input type="text" id="FacFax" class="form-control" />
                        </div>
                    </div>


				</div>
				<div class="modal-footer">
					<input type="button" class="btn btn-default" data-dismiss="modal" value="Exit" />
					<input type="button" id="btnSaveServLoc" class="btn btn-primary" data-dismiss="modal" value="Save" />
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div>

	<link href="plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
	<link href="https://cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css" rel='stylesheet' type='text/css'>
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="assets/js/alertify.js"></script>
</body>
</html>
