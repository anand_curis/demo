<?php
 session_start();

 if (!isset($_SESSION['userID'])) {
    header("Location: index.html");
 }
 include('header.html');
 ?>


<div class="row">
	<div class="clearfix"></div>
		<form action="UploadFileGen.php" method="post" enctype="multipart/form-data">
	        <h2>Upload File</h2>
	        <input type="hidden" id="hdnPatID" name="patientID"/>
            <input type="hidden" id="hdnUserID" name="userID"/>
            <input type="hidden" id="hdnPracticeID" name="practiceID"/>
            <div class="col-sm-12">
    	        <div class="col-sm-12">
                    <div class="form-group row">
                        <div class="col-5">
                            <label for="input-text" class="control-label float-right txt_media1">Document Name : </label>
                        </div>
                        <div class="col-7">
                            <input type="text" class="form-control" name="txtDocName" id="txtDocName" style="float:left;">
                        </div>
                    </div>
                </div>
    	        <div class="col-sm-12">
                    <div class="form-group row">
                        <div class="col-5">
                            <label for="input-text" class="control-label float-right txt_media1">Status: </label>
                        </div>
                        <div class="col-7">
                            <select id="ddlDocStatus" name="ddlDocStatus" class="form-control">
                                <option value=""></option>
                                <option value="New">New</option>
                                <option value="In Process">In Process</option>
                                <option value="Ready for Entry">Ready for Entry</option>
                                <option value="Error">Error</option>
                                <option value="Processed">Processed</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group row">
                        <div class="col-5">
                            <label for="input-text" class="control-label float-right txt_media1">Filename : </label>
                        </div>
                        <div class="col-7">
                            <input type="file" name="photo" style="float:left;" id="fileSelect">
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group row">
                        <div class="col-5">
                            <label for="input-text" class="control-label float-right txt_media1">Notes : </label>
                        </div>
                        <div class="col-7">
                            <textarea class="form-control" name="notes" cols="10" rows="7"></textarea>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group row">
                        <div class="col-5">
                            <input type="submit" class="btn btn-primary" name="submit" value="Upload">
                        </div>
                        <div class="col-7">
                            <a href="EditPatient.php" class="btn btn-primary">Home</a>
                        </div>
                    </div>
                </div>
            </div>
	    </form><br/>
	    
</div>


<script>

    $(document).ready(function(){
        var temp = sessionStorage.getItem("patientId");
        var userID = sessionStorage.getItem("userID");
        var practiceID = sessionStorage.getItem("practiceId");
        document.getElementById("hdnPatID").value = temp;
        document.getElementById("hdnUserID").value = userID;
        document.getElementById("hdnPracticeID").value = practiceID;
    });
    </script>
    <?php
    include('footer.html');
 ?>