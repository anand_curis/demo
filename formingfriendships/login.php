<?php

 session_start();



 session_unset();

 ?>

<!DOCTYPE html>

<html lang="en">

<head>

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0" />

	<title>Login | AMROMED LLC</title>



	<!--=== CSS ===-->



	<!-- Bootstrap -->

	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />



	<!-- Theme -->

	<link href="assets/css/main.css" rel="stylesheet" type="text/css" />

	<link href="assets/css/plugins.css" rel="stylesheet" type="text/css" />

	<link href="assets/css/responsive.css" rel="stylesheet" type="text/css" />

	<link href="assets/css/icons.css" rel="stylesheet" type="text/css" />



	<!-- Login -->

	<link href="assets/css/login.css" rel="stylesheet" type="text/css" />



	<link rel="stylesheet" href="assets/css/fontawesome/font-awesome.min.css">

	<!--[if IE 7]>

		<link rel="stylesheet" href="assets/css/fontawesome/font-awesome-ie7.min.css">

	<![endif]-->



	<!--[if IE 8]>

		<link href="assets/css/ie8.css" rel="stylesheet" type="text/css" />

	<![endif]-->

	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>



	<!--=== JavaScript ===-->



	<script type="text/javascript" src="assets/js/libs/jquery-1.10.2.min.js"></script>



	<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>

	<script type="text/javascript" src="assets/js/libs/lodash.compat.min.js"></script>



	<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->

	<!--[if lt IE 9]>

		<script src="assets/js/libs/html5shiv.js"></script>

	<![endif]-->



	<!-- Beautiful Checkboxes -->

	<script type="text/javascript" src="plugins/uniform/jquery.uniform.min.js"></script>



	<!-- Form Validation -->

	<script type="text/javascript" src="plugins/validation/jquery.validate.min.js"></script>



	<!-- Slim Progress Bars -->

	<script type="text/javascript" src="plugins/nprogress/nprogress.js"></script>



	<!-- App -->

	<script type="text/javascript" src="assets/js/login.js"></script>

	<script>

	$(document).ready(function(){

		"use strict";



		Login.init(); // Init login JavaScript

			$('#txtPswd').keypress(function(e){

			  if(e.keyCode==13)

			  $('#btnSignIn').click();

		});

		$('#btnSignIn').click(function(){

			var username = $('#txtUser').val();

			var password = $('#txtPswd').val();

			$.ajax({

                type: "POST",

                url:"checkLogin.php",

                data:{

                	username : username,

                	password : password,

            	},success:function(result){

            		var res = JSON.parse(result);

            		if(res.length != 0){

	            		if(res[0].status == "success"){

	            			sessionStorage.setItem('userID',res[0].userID);

	            			sessionStorage.setItem('userName',res[0].firstName + ' ' +res[0].lastName);

	            			var ID = res[0].userID;

	            			$.ajax({

				                type: "POST",

				                async:false,

				                url:"checkSession.php",

				                data:{

				                	ID : ID,

				            	},success:function(result){

				            		if(res[0].IsLogged == "0"){

			            				window.location.href = "changePswd.html";

			            			}

			            			else{

			            				window.location.href = "index.html";

			            			}

				                 }

				            });

	            		}

	            		else if(res[0].status == "failed"){

	            			alert("Login Invalid");

	            		}

	            		else if(res[0].status == "exists"){

	            			alert("Username not available");

	            		}

            		}

            		else{

            			alert("Username not available");

            		}

                 }

            });

		});

		$('#btnReset').click(function(){

			var resetEmail = $('#txtResetEmail').val();

			$.ajax({

                type: "POST",

                url:"resetpswd.php",

                data:{

                	resetEmail : resetEmail,

            	},success:function(result){

            		if(result == "success"){

            			$("#notyEmail").removeClass("hide-default");

            		}

            		else if(result == "failed"){

            			alert("Problem in sending Email");

            		}

            		else if(result == "wrong"){

            			alert("Email ID mismatch");

            		}

                 }

            });

		});

	});

	</script>

</head>



<body class="login" style="background:url('assets/bgimg2.jpg');">

	<!-- Logo -->

	<div class="logo" style="margin:60px 0 0 0; color:#fff;">

		

		<strong>AMROMED LLC</strong>

	</div>

	<!-- /Logo -->



	<!-- Login Box -->

	<div class="box">

		<div class="content">

			<!-- Login Formular -->

			<form class="form-vertical login-form">

				<!-- Title -->

				<h3 class="form-title">Sign In to your Account</h3>



				<!-- Error Message -->

				<div class="alert fade in alert-danger" style="display: none;">

					<i class="icon-remove close" data-dismiss="alert"></i>

					Enter any username and password.

				</div>



				<!-- Input Fields -->

				<div class="form-group">

					<!--<label for="username">Username:</label>-->

					<div class="input-icon">

						<i class="icon-user"></i>

						<input type="text" name="username" id="txtUser" style="height:30px;" class="form-control" placeholder="Username" autofocus="autofocus" data-rule-required="true" data-msg-required="Please enter your username." />

					</div>

				</div>

				<div class="form-group">

					<!--<label for="password">Password:</label>-->

					<div class="input-icon">

						<i class="icon-lock"></i>

						<input type="password" name="password" id="txtPswd" style="height:30px;" class="form-control" placeholder="Password" data-rule-required="true" data-msg-required="Please enter your password." />

					</div>

				</div>

				<!-- /Input Fields -->



				<!-- Form Actions -->

				<div class="form-actions">

					<label class="checkbox pull-left"><input type="checkbox" class="uniform" name="remember"> Remember me</label>

					<a href="javascript:void(0);" id="btnSignIn"><button type="button" class="submit btn btn-primary pull-right">

						Sign In <i class="icon-angle-right"></i>

					</button></a>

				</div>

			</form>

			<!-- /Login Formular -->

		</div> <!-- /.content -->



		<!-- Forgot Password Form -->

		<div class="inner-box">

			<div class="content">

				<!-- Close Button -->

				<i class="icon-remove close hide-default"></i>



				<!-- Link as Toggle Button -->

				<a href="#" class="forgot-password-link">Forgot Password?</a>



				<!-- Forgot Password Formular -->

				<form class="form-vertical forgot-password-form hide-default" action="login.html" method="post">

					<!-- Input Fields -->

					<div class="form-group">

						<!--<label for="email">Email:</label>-->

						<div class="input-icon">

							<i class="icon-envelope"></i>

							<input type="text" name="email" id="txtResetEmail" style="height:30px" class="form-control" placeholder="Enter email address" data-rule-required="true" data-rule-email="true" data-msg-required="Please enter your email." />

						</div>

					</div>

					<!-- /Input Fields -->



					<a href="javascript:void(0);" id="btnReset"><button type="button" class="submit btn btn-default btn-block">

						Reset your Password

					</button></a>

				</form>

				<!-- /Forgot Password Formular -->



				<!-- Shows up if reset-button was clicked -->

				<div class="forgot-password-done hide-default" id="notyEmail">

					<i class="icon-ok success-icon"></i> 

					<span>Great. We have sent you an email.</span>

				</div>

			</div> <!-- /.content -->

		</div>

		<!-- /Forgot Password Form -->

	</div>

	<!-- /Login Box -->



	



	<!-- Footer -->

	<!-- <div class="footer">

		<a href="#" class="sign-up">Don't have an account yet? <strong>Sign Up</strong></a>

	</div> -->

	<!-- /Footer -->

</body>

</html>