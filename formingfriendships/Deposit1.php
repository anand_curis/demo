<?php
 session_start();

 if (!isset($_SESSION['userID'])) {
 	header("Location: login.php");
 }
 ?>
 <!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0" />
	<title>Deposit | AMROMED LLC</title>

	<!--=== CSS ===-->

	<!-- Bootstrap -->
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

	<!-- jQuery UI -->
	<!--<link href="plugins/jquery-ui/jquery-ui-1.10.2.custom.css" rel="stylesheet" type="text/css" />-->
	<!--[if lt IE 9]>
		<link rel="stylesheet" type="text/css" href="plugins/jquery-ui/jquery.ui.1.10.2.ie.css"/>
	<![endif]-->

	<!-- Theme -->
	<link href="assets/css/main.css" rel="stylesheet" type="text/css" />
	<link href="assets/css/plugins.css" rel="stylesheet" type="text/css" />
	<link href="assets/css/responsive.css" rel="stylesheet" type="text/css" />
	<link href="assets/css/icons.css" rel="stylesheet" type="text/css" />

	<link rel="stylesheet" href="assets/css/fontawesome/font-awesome.min.css">
	<!--[if IE 7]>
		<link rel="stylesheet" href="assets/css/fontawesome/font-awesome-ie7.min.css">
	<![endif]-->

	<!--[if IE 8]>
		<link href="assets/css/ie8.css" rel="stylesheet" type="text/css" />
	<![endif]-->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>

	<!--=== JavaScript ===-->

	<script type="text/javascript" src="assets/js/libs/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" src="plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>

	<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="assets/js/libs/lodash.compat.min.js"></script>

	<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
		<script src="assets/js/libs/html5shiv.js"></script>
	<![endif]-->

	<!-- Smartphone Touch Events -->
	<script type="text/javascript" src="plugins/touchpunch/jquery.ui.touch-punch.min.js"></script>
	<script type="text/javascript" src="plugins/event.swipe/jquery.event.move.js"></script>
	<script type="text/javascript" src="plugins/event.swipe/jquery.event.swipe.js"></script>

	<!-- General -->
	<script type="text/javascript" src="assets/js/libs/breakpoints.js"></script>
	<script type="text/javascript" src="plugins/respond/respond.min.js"></script> <!-- Polyfill for min/max-width CSS3 Media Queries (only for IE8) -->
	<script type="text/javascript" src="plugins/cookie/jquery.cookie.min.js"></script>
	<script type="text/javascript" src="plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script type="text/javascript" src="plugins/slimscroll/jquery.slimscroll.horizontal.min.js"></script>

	<!-- Page specific plugins -->
	<!-- Charts -->
	<script type="text/javascript" src="plugins/sparkline/jquery.sparkline.min.js"></script>

	<script type="text/javascript" src="plugins/daterangepicker/moment.min.js"></script>
	<script type="text/javascript" src="plugins/daterangepicker/daterangepicker.js"></script>
	<script type="text/javascript" src="plugins/blockui/jquery.blockUI.min.js"></script>

	<!-- Pickers -->
	<script type="text/javascript" src="plugins/pickadate/picker.js"></script>
	<script type="text/javascript" src="plugins/pickadate/picker.date.js"></script>
	<script type="text/javascript" src="plugins/pickadate/picker.time.js"></script>
	<script type="text/javascript" src="plugins/bootstrap-colorpicker/bootstrap-colorpicker.min.js"></script>

	<!-- Noty -->
	<script type="text/javascript" src="plugins/noty/jquery.noty.js"></script>
	<script type="text/javascript" src="plugins/noty/layouts/top.js"></script>
	<script type="text/javascript" src="plugins/noty/themes/default.js"></script>

	<!-- Slim Progress Bars -->
	<script type="text/javascript" src="plugins/nprogress/nprogress.js"></script>
	<script type="text/javascript" src="assets/js/jquery.mask.min.js"></script>

	<!-- Bootbox -->
	<script type="text/javascript" src="plugins/bootbox/bootbox.min.js"></script>

	<!-- App -->
	<script type="text/javascript" src="assets/js/app.js"></script>
	<script type="text/javascript" src="assets/js/plugins.js"></script>
	<script type="text/javascript" src="assets/js/plugins.form-components.js"></script>

	<script>
	$(document).ready(function(){
		"use strict";

		App.init(); // Init layout and core plugins
		Plugins.init(); // Init all plugins
		FormComponents.init(); // Init all form-specific plugins
		document.getElementById('practiceName').innerHTML = sessionStorage.getItem('practiceName');
		$('#instrumentDate').mask('00-00-0000');
		$('#depositDate').mask('00-00-0000');
		$('#EditdepositDate').mask('00-00-0000');
		$('#EditinstrumentDate').mask('00-00-0000');
	});
	</script>

	<!-- Demo JS -->
	<script type="text/javascript" src="assets/js/custom.js"></script>
	<script type="text/javascript" src="assets/js/demo/ui_general.js"></script>
</head>

<body>

	<!-- Header -->
    <header class="header navbar navbar-fixed-top" role="banner" style="background-image: url('assets/bg.jpg'); background-repeat: repeat-x;">
        <!-- Top Navigation Bar -->
        <div class="container">

            <!-- Only visible on smartphones, menu toggle -->
            <ul class="nav navbar-nav">
                <li class="nav-toggle"><a href="javascript:void(0);" title=""><i class="icon-reorder"></i></a></li>
            </ul>

            <!-- Logo -->
            <a class="navbar-brand" style="text-align:center;padding-right:50px;" href="javascript:void(0);">
                <img src="assets/logo2.png"/>
                <strong>medABA</strong>
            </a>
            <!-- /logo -->

            <!-- Sidebar Toggler -->
            <a href="#" class="toggle-sidebar bs-tooltip" data-placement="bottom" data-original-title="Toggle navigation">
                <i class="icon-reorder"></i>
            </a>
            <!-- /Sidebar Toggler -->

            <!-- Top Left Menu -->
            <ul class="nav navbar-nav navbar-left hidden-xs hidden-sm" style="list-style:none;">
                <li>
                    <a href="ViewPatient.php" class="dropdown-toggle">
                        Clients
                    </a>
                </li>
                <li>
                    <a href="scheduler.php" class="dropdown-toggle">
                        Scheduler
                    </a>
                </li>
                <li>
                    <a href="Deposit.php" class="dropdown-toggle">
                        Deposit
                    </a>
                </li>
                <li>
                    <a href="Ledger.php" class="dropdown-toggle">
                        Ledger
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                        EDI
                        <i class="icon-caret-down small"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="generateAllEDI.php">
                            <i class="icon-angle-right"></i>
                            Generate EDI
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="EDIList.php">
                            <i class="icon-angle-right"></i>
                            EDI List
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="charts.html" class="dropdown-toggle" data-toggle="dropdown">
                        Settings
                        <i class="icon-caret-down small"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="ViewPractice.php">
                            <i class="icon-angle-right"></i>
                            Practices
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="ViewPhysician.php">
                            <i class="icon-angle-right"></i>
                            Physicians
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="ViewServiceLoc.php">
                            <i class="icon-angle-right"></i>
                            Locations &amp; Facilities
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="javascript:void(0);" style="font-weight:600;">
                            Codes
                            </a>
                            <li>
                                <a href="ViewCPT.php">
                                <i class="icon-angle-right"></i>
                                CPT/Procedure
                                </a>
                            </li>
                            <li>
                                <a href="ViewDX.php">
                                <i class="icon-angle-right"></i>
                                ICD 10/ICD 9 Library
                                </a>
                            </li>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="insurances.php">
                            <i class="icon-angle-right"></i>
                            Insurances
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- /Top Left Menu -->

            <!-- Top Right Menu -->
            <ul class="nav navbar-nav navbar-right">
                <!-- Notifications -->
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-warning-sign"></i>
                        <span class="badge">5</span>
                    </a>
                    <ul class="dropdown-menu extended notification">
                        <li class="title">
                            <p>You have 5 new notifications</p>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="label label-success"><i class="icon-plus"></i></span>
                                <span class="message">New user registration.</span>
                                <span class="time">1 mins</span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="label label-danger"><i class="icon-warning-sign"></i></span>
                                <span class="message">High CPU load on cluster #2.</span>
                                <span class="time">5 mins</span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="label label-success"><i class="icon-plus"></i></span>
                                <span class="message">New user registration.</span>
                                <span class="time">10 mins</span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="label label-info"><i class="icon-bullhorn"></i></span>
                                <span class="message">New items are in queue.</span>
                                <span class="time">25 mins</span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="label label-warning"><i class="icon-bolt"></i></span>
                                <span class="message">Disk space to 85% full.</span>
                                <span class="time">55 mins</span>
                            </a>
                        </li>
                        <li class="footer">
                            <a href="javascript:void(0);">View all notifications</a>
                        </li>
                    </ul>
                </li>

                <!-- Tasks -->
                <li class="dropdown hidden-xs hidden-sm">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-tasks"></i>
                        <span class="badge">7</span>
                    </a>
                    <ul class="dropdown-menu extended notification">
                        <li class="title">
                            <p>You have 7 pending tasks</p>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="task">
                                    <span class="desc">Preparing new release</span>
                                    <span class="percent">30%</span>
                                </span>
                                <div class="progress progress-small">
                                    <div style="width: 30%;" class="progress-bar progress-bar-info"></div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="task">
                                    <span class="desc">Change management</span>
                                    <span class="percent">80%</span>
                                </span>
                                <div class="progress progress-small progress-striped active">
                                    <div style="width: 80%;" class="progress-bar progress-bar-danger"></div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="task">
                                    <span class="desc">Mobile development</span>
                                    <span class="percent">60%</span>
                                </span>
                                <div class="progress progress-small">
                                    <div style="width: 60%;" class="progress-bar progress-bar-success"></div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="task">
                                    <span class="desc">Database migration</span>
                                    <span class="percent">20%</span>
                                </span>
                                <div class="progress progress-small">
                                    <div style="width: 20%;" class="progress-bar progress-bar-warning"></div>
                                </div>
                            </a>
                        </li>
                        <li class="footer">
                            <a href="javascript:void(0);">View all tasks</a>
                        </li>
                    </ul>
                </li>

                <!-- Messages -->
                <li class="dropdown hidden-xs hidden-sm">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-envelope"></i>
                        <span class="badge">1</span>
                    </a>
                    <ul class="dropdown-menu extended notification">
                        <li class="title">
                            <p>You have 3 new messages</p>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="photo"><img src="assets/img/demo/avatar-1.jpg" alt="" /></span>
                                <span class="subject">
                                    <span class="from">Bob Carter</span>
                                    <span class="time">Just Now</span>
                                </span>
                                <span class="text">
                                    Consetetur sadipscing elitr...
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="photo"><img src="assets/img/demo/avatar-2.jpg" alt="" /></span>
                                <span class="subject">
                                    <span class="from">Jane Doe</span>
                                    <span class="time">45 mins</span>
                                </span>
                                <span class="text">
                                    Sed diam nonumy...
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="photo"><img src="assets/img/demo/avatar-3.jpg" alt="" /></span>
                                <span class="subject">
                                    <span class="from">Patrick Nilson</span>
                                    <span class="time">6 hours</span>
                                </span>
                                <span class="text">
                                    No sea takimata sanctus...
                                </span>
                            </a>
                        </li>
                        <li class="footer">
                            <a href="javascript:void(0);">View all messages</a>
                        </li>
                    </ul>
                </li>

                <!-- .row .row-bg Toggler -->
                <li>
                    <a href="#" class="dropdown-toggle row-bg-toggle">
                        <i class="icon-resize-vertical"></i>
                    </a>
                </li>


                <!-- User Login Dropdown -->
                <li class="dropdown user">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <!--<img alt="" src="assets/img/avatar1_small.jpg" />-->
                        <i class="icon-male"></i>
                        <span class="username" id="practiceName"></span>
                        <i class="icon-caret-down small"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="javascript:void(0);"><i class="icon-user"></i> My Profile</a></li>
                        <li><a href="javascript:void(0);"><i class="icon-calendar"></i> My Calendar</a></li>
                        <li><a href="javascript:void(0);"><i class="icon-tasks"></i> My Tasks</a></li>
                        <li class="divider"></li>
                        <li><a href="login.php"><i class="icon-key"></i> Log Out</a></li>
                    </ul>
                </li>
                <!-- /user login dropdown -->
            </ul>
            <!-- /Top Right Menu -->
        </div>
        <!-- /top navigation bar -->

    </header> <!-- /.header -->

	<div id="container">
		<div id="content">
			<div class="container">
				<!-- Breadcrumbs line -->
				<div class="crumbs">
					<ul id="breadcrumbs" class="breadcrumb">
						<li class="current">
							<i class="icon-home"></i>
							<a href="index.html">Dashboard</a>
						</li>
						
					</ul>
					<a href="javascript:void(0);"><img src="assets/icons/Settings.png" title="Settings" style="float:right; padding-top:0.3em; padding-right:2em;"/></a>
					<a href="Ledger.php"><img src="assets/icons/Claim_search.png" title="Claim Search" style="float:right; padding-top:0.3em; padding-right:2em;"/></a>
					<a href="AddClaim.php"><img src="assets/icons/Claim_add.png" title="Add Claim" style="float:right; padding-top:0.3em; padding-right:2em;"/></a>
					<a href="ViewPatient.php"><img src="assets/icons/Patient_search.png" title="Patient Search" style="float:right; padding-top:0.3em; padding-right:2em;"/></a>
					<a href="AddPatient.php"><img src="assets/icons/Patient_add.png" title="Add Patient" style="float:right; padding-top:0.3em; padding-right:2em;"/></a>
					<a href="dashboard.html"><img src="assets/icons/Home.png" title="Home" style="float:right; padding-top:0.3em; padding-right:2em;"/></a>

				</div>
				<!-- /Breadcrumbs line -->


				<!--=== Page Content ===-->
				<!--=== Modals ===-->
				<div class="row">
					<img src="assets/beat.gif" id="loader" style="position:absolute; left:50%; z-index:99999; top:35%; width:160px; height:24px;" />
					<h2 style="color: #251367; margin-left:20px;">Deposit</h2>
					<div class="col-md-12" style="margin-top:20px;">
			        <div class="widget box" >
			            <div class="widget-content">
			            	<a href="#" id="linkAddDep" class="btn btn-primary" style="text-decoration:none"><i class="icon icon-plus"></i>&nbsp;&nbsp;Add Deposit</a>
			            	<form action="EditPatient.php" method="post">
								<table id="deposit" class="table table-striped table-bordered table-hover table-checkable" cellspacing="0" style="overflow-x:scroll" data-horizontal-width="150%">
								</table>
							</form>
                            <table id="depositDetails" class="table table-striped table-bordered table-hover table-checkable" cellspacing="0" style="overflow-x:scroll" data-horizontal-width="150%">
                            </table>
			            </div>
			        </div>

				</div>

				</div>
				<!-- /Page Content -->
			</div>
			<!-- /.container -->

		</div>
	</div>
	<div class="modal fade" id="AddDeposit" tabindex="-1">
		<div class="modal-dialog" style="width:75%">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Add New Deposit</h4>
				</div>
				<div class="modal-body" style="min-height:280px;">
					<div class="form-group col-md-6">
                        <label class="control-label col-md-12">Select Payor Type </label>
                        <div class="col-md-6">
                            <select class="form-control" id="payorType">
                            	<option value="0"></option>
                            	<option value="1">Payor</option>
                            	<option value="2">Client</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-md-6" id="divPayorType">
                        <label class="control-label col-md-12" id="lblType">Payor </label>
                        <div class="col-md-6">
                            <select class="form-control" id="payor">
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label class="control-label col-md-12">Deposit Date </label>
                        <div class="col-md-6">
                            <input type="text" id="depositDate" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label class="control-label col-md-12" id="lblType">Payment Method </label>
                        <div class="col-md-6">
                            <select class="form-control" id="paymentType">
                            	<option>Check</option>
                            	<option>EFT</option>
                            	<option>Credit Card</option>
                            	<option>Cash</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-md-4">
                        <label class="control-label col-md-12">Check # </label>
                        <div class="col-md-6">
                            <input type="text" id="instrumentNo" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group col-md-4">
                        <label class="control-label col-md-12">Check Date </label>
                        <div class="col-md-6">
                            <input type="text" id="instrumentDate" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group col-md-4">
                        <label class="control-label col-md-12" id="lblType">Amount </label>
                        <div class="col-md-6">
                            <input type="text" id="amount" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group col-md-8">
                        <label class="control-label col-md-12" id="lblType">Notes </label>
                        <div class="col-md-10">
                            <textarea class="form-control" id="notes" col="5" row="6"></textarea>
                        </div>
                    </div>
				</div>
				<div class="modal-footer">
					<input type="button" class="btn btn-default" data-dismiss="modal" value="Exit" />
					<input type="button" id="btnSaveDep" class="btn btn-primary" data-dismiss="modal" value="Save" />
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div>
	<div class="modal fade" id="EditDeposit" tabindex="-1">
		<div class="modal-dialog" style="width:75%">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Edit Deposit</h4>
				</div>
				<div class="modal-body" style="min-height:280px;">
					<input type="hidden" id="_hdnDepID"/>
					<div class="form-group col-md-6">
                        <label class="control-label col-md-12">Select Payor Type </label>
                        <div class="col-md-6">
                            <select class="form-control" id="EditpayorType">
                            	<option value="0"></option>
                            	<option value="1">Payor</option>
                            	<option value="2">Client</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-md-6" id="divPayorType">
                        <label class="control-label col-md-12" id="lblType">Payor </label>
                        <div class="col-md-6">
                            <select class="form-control" id="Editpayor">
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label class="control-label col-md-12">Deposit Date </label>
                        <div class="col-md-6">
                            <input type="text" id="EditdepositDate" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label class="control-label col-md-12" id="lblType">Payment Method </label>
                        <div class="col-md-6">
                            <select class="form-control" id="EditpaymentType">
                            	<option>Check</option>
                            	<option>EFT</option>
                            	<option>Credit Card</option>
                            	<option>Cash</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-md-4">
                        <label class="control-label col-md-12">Check # </label>
                        <div class="col-md-6">
                            <input type="text" id="EditinstrumentNo" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group col-md-4">
                        <label class="control-label col-md-12">Check Date </label>
                        <div class="col-md-6">
                            <input type="text" id="EditinstrumentDate" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group col-md-4">
                        <label class="control-label col-md-12" id="lblType">Amount </label>
                        <div class="col-md-6">
                            <input type="text" id="Editamount" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group col-md-8">
                        <label class="control-label col-md-12" id="lblType">Notes </label>
                        <div class="col-md-10">
                            <textarea class="form-control" id="Editnotes" col="5" row="6"></textarea>
                        </div>
                    </div>
				</div>
				<div class="modal-footer">
					<input type="button" class="btn btn-default" data-dismiss="modal" value="Exit" />
					<input type="button" id="btnEditDep" class="btn btn-primary" data-dismiss="modal" value="Save" />
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div>
	<link href="https://cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css" rel='stylesheet' type='text/css'>
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
	<script>


	$(document).ready(function(){
		$('#divPayorType').hide();
		var practiceId = sessionStorage.getItem('practiceId');
        var temp = sessionStorage.getItem("patientId");
        $(document).on('focusout','#amount',function() {
            var amount = $(this).val();
            amount = parseInt(amount).toFixed(2);
            document.getElementById("amount").value = amount;
        });
		$('#btnSaveDep').click(function(){
			var payorType = $('#payorType').val();
			var payor = $('#payor').val();
			var depositDate = changeDate($('#depositDate').val());
			var paymentType = $('#paymentType').val();
			var instrumentNo = $('#instrumentNo').val();
			var instrumentDate = changeDate($('#instrumentDate').val());
			var amount = $('#amount').val();
			var notes = $('#notes').val();
			$.post("http://curismed.com/medService/deposits/create",
		    {
		        practiceID: practiceId,
		        chequeNo : instrumentNo,
		        chequeDate : instrumentDate,
		        payorType :payorType,
		        paymentType :paymentType,
		        payorID : payor,
		        amount : amount,
		        depositDate : depositDate,
		        notes : notes
		    },
		    function(data1, status){
		    	//alert(JSON.stringify(data1));
		    	window.location.reload();
			});
		});
		$('#btnEditDep').click(function(){
			var depositID = document.getElementById('_hdnDepID').value;
			var payorType = $('#EditpayorType').val();
			var payor = $('#Editpayor').val();
			var depositDate = changeDate($('#EditdepositDate').val());
			var paymentType = $('#EditpaymentType').val();
			var instrumentNo = $('#EditinstrumentNo').val();
			var instrumentDate = changeDate($('#EditinstrumentDate').val());
			var amount = $('#Editamount').val();
			var notes = $('#Editnotes').val();
			$.post("updateDeposit.php",
		    {
		    	depositID : depositID,
		        chequeNo : instrumentNo,
		        chequeDate : instrumentDate,
		        payorType :payorType,
		        paymentType :paymentType,
		        payorID : payor,
		        amount : amount,
		        depositDate : depositDate,
		        notes : notes
		    },
		    function(data1, status){
		    	//alert(JSON.stringify(data1));
		    	window.location.reload();
			});
		});
		$('#payorType').change(function(){
			//alert($(this).val());
			if($(this).val() == '1'){
				$('#divPayorType').show();
				document.getElementById('lblType').innerHTML = 'Payor';
				populatePayor();
			}
			else if($(this).val() == '2'){
				$('#divPayorType').show();
				document.getElementById('lblType').innerHTML = 'Client';
				var data = <?php include('patientListwithID.php'); ?>;
				//alert(data);
				var list = [];
		        $('#payor').html('');
		        for(var x in data){
				  list.push(data[x]);
				}
				//alert(list);
				$.each(list,function(i,v) {
					$('#payor').html('');
					list.forEach(function(t) { 
			            $('#payor').append('<option value="'+t.patientID+'">'+t.patientID +' - '+t.fullName+'</option>');
			        });
				});
			}
		});
		$("#linkAddDep").click(function(){
	    	$('#AddDeposit').modal('show');
	    });
		$('#loader').show();
		var pracID = sessionStorage.getItem('practiceId');
		loadDep(pracID);
	});

	function populatePayor(){
		var data = <?php include('insuranceListwithID.php'); ?>;
		//alert(data);
		var list = [];
        $('#payor').html('');
        for(var x in data){
		  list.push(data[x]);
		}
		//alert(list);
		$.each(list,function(i,v) {
			$('#payor').html('');
			list.forEach(function(t) { 
	            $('#payor').append('<option value="'+t.insuranceID+'">'+t.insuranceID +' - '+t.payerName+'</option>');
	        });
		});
	}
	function populateEditPayor(){
		var data = <?php include('insuranceListwithID.php'); ?>;
		//alert(data);
		var list = [];
        $('#Editpayor').html('');
        for(var x in data){
		  list.push(data[x]);
		}
		//alert(list);
		$.each(list,function(i,v) {
			$('#Editpayor').html('');
			list.forEach(function(t) { 
	            $('#Editpayor').append('<option value="'+t.insuranceID+'">'+t.insuranceID +' - '+t.payerName+'</option>');
	        });
		});
	}

	function loadDep(pracID){
		$.post("http://curismed.com/medService/deposits",
		    {
		        practiceID: pracID
		    },
		    function(data1, status){
				var dt = [];
				$.each(data1,function(i,v) {
					if(data1[i].Balance == null){
						data1[i].Balance = data1[i].amount;
					}
					if(data1[i].payorType == '1'){
						data1[i].payorType = "Payor";
					}
					else{
						data1[i].payorType = "Client";
					}
					dt.push([data1[i].depositID,data1[i].depositDate,data1[i].chequeNo,data1[i].chequeDate,data1[i].payerName,data1[i].payorType,data1[i].amount,data1[i].Balance,data1[i].paymentType,data1[i].notes,data1[i].notes,data1[i].payorID]);
				});
				var table = $('#deposit').DataTable({
		        "data": dt,
		        "bPaginate": false,
                "bDestroy": true,
		        "bProcessing": true,
		         "aoColumns": [
		         	{"mdata": "depositID","title":"Deposit ID", visible:false},
		            {"title":"Deposit Date","mdata": "Date",
		            	"render": function ( data, type, full, meta ) {
					      return changeDateFormat(data);
					    }
					},
		            {"title":"Cheque No","mdata": "Cheque No"},
		            {"title":"Cheque Date","mdata": "Cheque Date",
		            	"render": function ( data, type, full, meta ) {
					      return changeDateFormat(data);
					    }
					},
		            {"title":"Payor Name","mdata": "payorName"},
		            {"title":"Payor Type","mdata": "Payor Type"},
		            {"title":"Payment","mdata": "Payment",
		            	"render": function ( data, type, full, meta ) {
					      return '$'+data;
					    }
					},
		            {"title":"UnApplied","mdata": "Balance",
		            	"render": function ( data, type, full, meta ) {
					      return '$'+data;
					    }
					},
		            {"title":"Payment Method","mdata": "Payment Method"},
		            {"title":"Description","mdata": "Description"},
		            {
		            	"title":"Actions",
		            	"mdata": "Actions",
		            	mRender: function (data, type, row) { return '<a href="javascript:void(0);" onclick="apply('+row[0]+','+row[11]+','+row[6]+')">Apply</a>&nbsp;|&nbsp;<a href="javascript:void(0);" onclick="viewDepDetails('+row[0]+')">View Details</a>&nbsp;|&nbsp;<a href="javascript:void(0);" onclick="editDep('+row[0]+')"><i class="icon icon-edit"></i></a>&nbsp;|&nbsp;<a href="javascript:void(0);" onclick="deleteDep('+row[0]+')"><i class="icon icon-trash"></i></a>'; }
		        	},
		        	{"title":"payorID","mdata": "payorID", "visible": false}
			    ]
		        });
		        $('#loader').hide();
		    });
	}

    function viewDepDetails(id){
        $.ajax({
          type: "POST",
          url:"http://curismed.com/medService/claims/listbydeposit",
          data:{
            depositID : id
          },success:function(result){
            $('#depositDetails').show();
            var data1 = result;
            var dt = [];
            $.each(data1,function(i,v) {
                dt.push([id,data1[i].claimID,data1[i].fromDt,data1[i].toDt,data1[i].total,data1[i].allowed,data1[i].paid,data1[i].adjustment,data1[i].claimBalance,data1[i].reason,data1[i].status,data1[i].insuranceID]);
            });
            var table = $('#depositDetails').DataTable({                
            "data": dt,
            "bPaginate": false,
            "bDestroy": true,
            "bProcessing": true,
             "aoColumns": [
                {"mdata": "depositID","title":"Deposit ID", visible:false},
                {"mdata": "claimID","title":"Claim ID", visible:false},
                {"title":"From Date","mdata": "fromDt",
                    "render": function ( data, type, full, meta ) {
                      return changeDateFormat(data);
                    }
                },
                {"title":"To Date","mdata": "toDt",
                    "render": function ( data, type, full, meta ) {
                      return changeDateFormat(data);
                    }
                },
                {"title":"Total","mdata": "total",
                    "render": function ( data, type, full, meta ) {
                      return '$'+data;
                    }
                },
                {"title":"Allowed","mdata": "allowed",
                    "render": function ( data, type, full, meta ) {
                      return '$'+data;
                    }
                },
                {"title":"Paid","mdata": "paid",
                    "render": function ( data, type, full, meta ) {
                      return '$'+data;
                    }
                },
                {"title":"Adjustment","mdata": "adjustment",
                    "render": function ( data, type, full, meta ) {
                      return '$'+data;
                    }
                },
                {"title":"Balance","mdata": "claimBalance",
                    "render": function ( data, type, full, meta ) {
                      return '$'+data;
                    }
                },
                {"title":"Reason","mdata": "reason"},
                {"title":"Status","mdata": "status"},
                {"title":"Insurance","mdata": "insuranceID"}
            ]
            });
         }
      });
    }

	function editDep(id){
		$.ajax({
          type: "POST",
          url:"getDepInfo.php",
          async : false,
          data:{
            depositID : id
          },success:function(result){
            $('#EditDeposit').modal('show');
            populateEditPayor();
            var res = JSON.parse(result);
            if(res[0].payorType == '1'){
            	var ID = res[0].payorID;
	            $("#EditpayorType").val(res[0].payorType);
	            $.ajax({
                    type: "POST",
                    url:"getInsuranceName.php",
                    async: false,
                    data:{
                        "insuranceID" : res[0].payorID
                        },success:function(result){
                            var res = JSON.parse(result);
                            $("#Editpayor").val(ID);
                    }
                });
	        }
	        else{
	        	$("#EditpayorType option:selected").text("Client");
	        }
	    	document.getElementById('EditpaymentType').value = res[0].paymentType;
	    	document.getElementById('EditinstrumentNo').value = res[0].chequeNo;
	    	document.getElementById('EditinstrumentDate').value = changeDateFormat(res[0].chequeDate);
	    	document.getElementById('Editamount').value = res[0].amount;
	    	document.getElementById('Editnotes').value = res[0].notes;
	    	document.getElementById('EditdepositDate').value = changeDateFormat(res[0].depositDate);
	    	document.getElementById('_hdnDepID').value = res[0].depositID;
         }
      });
	}

	function deleteDep(id){
		var x = confirm("Are you sure you want to delete?");
	    if (x){
	    	$.ajax({
	          type: "POST",
	          url:"deleteDep.php",
	          async : false,
	          data:{
	            depositID : id
	          },success:function(result){
	          	alert(result);
	          	window.location.reload();
	         }
	      });
	    }
	    else{
	      return false;
	  	}
	}

	function apply(depositId,payorId,depAmt){
		sessionStorage.setItem("depID",depositId);
		sessionStorage.setItem("depPayor",payorId);
		sessionStorage.setItem("depAmt",depAmt);
		window.location.href = "DepositSelectClient.php";
	}
	function changeDateFormat(inputDate){  // expects Y-m-d
        var splitDate = inputDate.split('-');
        if(splitDate.count == 0){
            return null;
        }

        var year = splitDate[0];
        var month = splitDate[1];
        var day = splitDate[2]; 

        return month + '-' + day + '-' + year;
    }
    function changeDate(inputDate){  // expects Y-m-d
        var splitDate = inputDate.split('-');
        if(splitDate.count == 0){
            return null;
        }

        var year = splitDate[2];
        var month = splitDate[0];
        var day = splitDate[1]; 

        return year+ '-' + month + '-' + day;
    }
	</script>
</body>
</html>