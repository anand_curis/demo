<?php


// Include the main TCPDF library (search for installation path).

require_once('SearchPT/tcpdf_include.php');





// Extend the TCPDF class to create custom Header and Footer

class MYPDF extends TCPDF {

    //Page header

    public function Header() {

        // get the current page break margin

        $bMargin = $this->getBreakMargin();

        // get current auto-page-break mode

        $auto_page_break = $this->AutoPageBreak;

        // disable auto-page-break

        $this->SetAutoPageBreak(false, 0);

        // set bacground image

        //$img_file = K_PATH_IMAGES.'image_demo.jpg';

        //$this->Image($img_file, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);

        // restore auto-page-break status

        $this->SetAutoPageBreak($auto_page_break, $bMargin);

        // set the starting point for the page content

        $this->setPageMark();



    }

}



// create new PDF document

$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);



// set document information

$pdf->SetCreator(PDF_CREATOR);

$pdf->SetAuthor('Nicola Asuni');

$pdf->SetTitle('HCFA-1500');

$pdf->SetSubject('TCPDF Tutorial');

$pdf->SetKeywords('TCPDF, PDF, example, test, guide');



// set header and footer fonts

$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));



// set default monospaced font

$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);



// set margins

$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

$pdf->SetHeaderMargin(0);

$pdf->SetFooterMargin(0);



// remove default footer

$pdf->setPrintFooter(false);



// set auto page breaks

$pdf->SetAutoPageBreak(TRUE, 0);



// set image scale factor

$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);





// set some language-dependent strings (optional)

if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {

    require_once(dirname(__FILE__).'/lang/eng.php');

    $pdf->setLanguageArray($l);

}



// ---------------------------------------------------------



session_start(); 

$fullName = $_POST["fullName"];

$patientID = $_POST["patientID"];

$dob = $_POST["dob"];

$gender = $_POST["gender"];

if($gender == "M"){

  $Male = 'X';

  $Female = '';

}

else{

  $Male = '';

  $Female = 'X';

}

$invoiceNo = $_POST["invoiceNo"];

$relationship = $_POST["relationship"];

$ssn = $_POST["ssn"];

$martialStatus = $_POST["martialStatus"];

$medicalRecord = $_POST["medicalRecord"];

$employment = $_POST["employment"];

$employer = $_POST["employer"];

$reference = $_POST["reference"];

$addrStreet1 = $_POST["addrStreet1"];

$addrStreet2 = $_POST["addrStreet2"];

$addrCity = $_POST["addrCity"];

$addrState = $_POST["addrState"];

$addrCountry = $_POST["addrCountry"];

$addrZip = $_POST["addrZip"];

$email = $_POST["email"];

$phoneHome = $_POST["phoneHome"];

$phoneWork = $_POST["phoneWork"];

$phoneMobile = $_POST["phoneMobile"];

$notifyEmail = $_POST["notifyEmail"];

$notifyPhone = $_POST["notifyPhone"];

$primaryCarePhy = $_POST["primaryCarePhy"];

$referringPhy = $_POST["referringPhy"];

$defaultRenderPhy = $_POST["defaultRenderPhy"];

$defaultServiceLoc = $_POST["defaultServiceLoc"];

$chartNo = $_POST["chartNo"];

$emergencyContact = $_POST["emergencyContact"];

$emergencyPhone = $_POST["emergencyPhone"];

$tempName = explode(" ", $fullName);

if($tempName[0]!=""){

  $firstName = $tempName[0];

}

else{

  $firstName = "";

}

if($tempName[1]!=""){

  $middleName = $tempName[1];

}

else{

  $middleName = "";

}

if($tempName[2]!=""){

  $lastName = $tempName[2];

}

else{

  $lastName = "";

}

$insureID1 = $_POST["insureID"];

$insureID2 = $_POST["secinsureID"];

if($insureID2 != ""){

  $insureID2 = str_pad($insureID2, 29);

  $insuranceAddr2 = $_POST["secinsuranceAddr"];

  $insName2 = $_POST["secinsuranceID"];

  $insName2 = str_pad($insName2, 49);

  $seclastName = $lastName.',';

  $secfirstName = $firstName;

  $secmiddleName = $middleName;

  $IsOtherHealthYes = "X";

  $IsOtherHealthNo = "";

}

else{

  $insureID2 = str_pad($insureID2, 29);

  $insuranceAddr2 = "";

  $insName2 = "";

  $insName2 = str_pad($insName2, 49);

  $seclastName = "";

  $secfirstName = "";

  $secmiddleName = "";

  $IsOtherHealthYes = "";

  $IsOtherHealthNo = "X";

}

$payor = $_POST["payor"];

$insuranceAddr1 = $_POST["insuranceAddr"];

$insName1 = $_POST["insuranceID"];

$individualNPI = $_POST["individualNPI"];

$providerName= $_POST["providerName"];

$chartNo= $_POST["chartNo"];



$facilityName = $_POST["facilityName"];

$facilityAddr = $_POST["facilityAddr"];

$practiceEIN = $_POST["practiceEIN"];

$practiceName = $_POST["practiceName"];

$practiceAddr = $_POST["practiceAddr"];





$assignedProvider = $_POST["assignedProvider"];

$principalDiag = $_POST["principalDiag"];

$defDiag2 = $_POST["defDiag2"];

$defDiag3 = $_POST["defDiag3"];

$defDiag4 = $_POST["defDiag4"];

$fromDt = $_POST["fromDt"];

$toDt = $_POST["toDt"];

$PrimaryCarePhysician = $_POST["PrimaryCarePhysician"];

$placeOfService = $_POST["placeOfService"];

$proced = $_POST["proced"];

$mod1 = $_POST["mod1"];

$charge = $_POST["total"].' 00';

$units = $_POST["units"];



$insuranceAddr1 = explode(",", $insuranceAddr1);

$cou = count($insuranceAddr1);

if($cou == 3){

  $concatInsAddr1 = $insuranceAddr1[0].', '.$insuranceAddr1[1];

  $nextInsAddr1 = $insuranceAddr1[2];

}

else{

  $concatInsAddr1 = $insuranceAddr1[0];

  $nextInsAddr1 = $insuranceAddr1[1]; 

}

$practiceAddr = explode(",", $practiceAddr);

$facilityAddr = explode(",", $facilityAddr);





$tempDOB = explode("-", $dob);

if($tempDOB[0]!=""){

  $year = $tempDOB[0];

}

else{

  $year = "";

}

if($tempDOB[1]!=""){

  $month = $tempDOB[1];

}

else{

  $month = "";

}

if($tempDOB[2]!=""){

  $date = $tempDOB[2];

}

else{

  $date = "";

}



$tempfromDt = explode("-", $fromDt);

if($tempfromDt[0]!=""){

  $Smonth = $tempfromDt[0];

}

else{

  $Smonth = "";

}

if($tempfromDt[1]!=""){

  $Sdate = $tempfromDt[1];

}

else{

  $Sdate = "";

}

if($tempfromDt[2]!=""){

  $Syear = substr($tempfromDt[2],-2);

}

else{

  $Syear = "";

}



$temptoDt = explode("-", $toDt);

if($temptoDt[0]!=""){

  $Emonth = $temptoDt[0];

}

else{

  $Emonth = "";

}

if($temptoDt[1]!=""){

  $Edate = $temptoDt[1];

}

else{

  $Edate = "";

}

if($temptoDt[2]!=""){

  $Eyear = substr($temptoDt[2],-2);

}

else{

  $Eyear = "";

}

$Lname= strlen($lastName);

$Fname= strlen($firstName);

$Mname= strlen($middleName);

if($Mname !=""){

  $tot = 27 - ($Lname + $Fname + $Mname);

}

else{

  $tot = 26 - ($Lname + $Fname + $Mname);

}

$addrStreet = str_pad($addrStreet1, 31);

$addrCity = str_pad($addrCity, 25);

$addrState = str_pad($addrState, 20);

$addrZip = str_pad($addrZip, 13);

$phoneHome = str_pad($phoneHome, 35);

$facilityName = str_pad($facilityName, 27);



if($principalDiag != "" && $defDiag2 != "" && $defDiag3 != "" && $defDiag4 != "" ){

  $pointer = "ABCD";

}

else if($principalDiag != "" && $defDiag2 != "" && $defDiag3 != "" && $defDiag4 == "" ){

  $pointer = "ABC";

}

else if($principalDiag != "" && $defDiag2 != "" && $defDiag3 == "" && $defDiag4 == "" ){

  $pointer = "AB";

}

else if($principalDiag != "" && $defDiag2 == "" && $defDiag3 == "" && $defDiag4 == "" ){

  $pointer = "A";

}

else if($principalDiag == "" && $defDiag2 == "" && $defDiag3 == "" && $defDiag4 == "" ){

  $pointer = "";

}





$providerName = str_pad($providerName, 22);

$facilityAddr[0] = str_pad($facilityAddr[0], 27);

$practiceAddr[0] = str_pad($practiceAddr[0], 30);



$middleName = str_pad($middleName, $tot-1);

$CurrNow = date('Y-m-d');

$txtFileName = $fullName.'_'.$CurrNow.".txt";

$CurrDate = date('Y-m-d');

$tempCurrDate = explode("-", $CurrDate);

if($tempCurrDate[0]!=""){

  $Curryear = $tempCurrDate[0];

}

else{

  $Curryear = "";

}

if($tempCurrDate[1]!=""){

  $Currmonth = $tempCurrDate[1];

}

else{

  $Currmonth = "";

}

if($tempCurrDate[2]!=""){

  $Currdate = $tempCurrDate[2];

}

else{

  $Currdate = "";

}



// set font

$pdf->SetFont('times', '', 9);



// add a page

$pdf->AddPage();



// Print a text

$html = '';

//$pdf->Cell(60, 0, 'Top-Center', 1, $ln=0, 'C', 0, '', 0, false, 'T', 'C');

$pdf->Text  ( 100, 15, $insName1, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 100, 21, $concatInsAddr1, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 100, 27, $nextInsAddr1, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 100, 33, '', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->SetFont('times', '', 10);



$pdf->Text  ( 116.5, 46, 'x', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 130, 46, $insureID1, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 130, 63, $addrStreet, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 130, 71, $addrCity, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 70, 71, $addrState, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 130, 80, $addrZip, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 130, 89, '', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 122, 54, 'x', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 130, 54, $fullName, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 8, 54, $fullName, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 136, 98, $tempDOB[1], false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 144, 98, $tempDOB[2], false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 154, 98, $tempDOB[0], false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 78, 54.5, $tempDOB[1], false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 87, 54.5, $tempDOB[2], false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 95, 54.5, $tempDOB[0], false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 8, 63, $addrStreet, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 85.5, 63, 'x', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 8, 71, $addrCity, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 192, 71, $addrState, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 170, 80, $phoneHome, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 8, 80, $addrZip, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 40.5, 80, $phoneHome, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 8, 88, '', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 8, 97, '', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 105.5, 97, 'x', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 193, 97, 'x', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 105.5, 105, 'x', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 105.5, 114, 'x', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 146, 122.5, 'x', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );







$pdf->Text  ( 20, 139, 'SIGNATURE ON FILE', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 95, 139, $Currmonth.'/'.$Currdate.'/'.$Curryear, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 150, 139, 'SIGNATURE ON FILE', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 107.5, 169, '0', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 10, 174, $principalDiag, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 10, 182, '', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 76, 172.5, $defDiag3, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 107.5, 174, $defDiag4, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 43, 174, $defDiag2, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 40, 182, '', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 147, 165, 'x', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 168, 165, '', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );









$pdf->Text  ( 5, 199, $Smonth, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 13, 199, $Sdate, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 20.5, 199, $Syear, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 28, 199, $Emonth, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 36, 199, $Edate, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 44, 199, $Eyear, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 52, 199, $placeOfService, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 70, 199, $proced, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 86, 199, $mod1, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 116, 199, $pointer, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 134, 199, $charge, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 146, 199, '', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 155.5, 199, $units, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 180, 199, $individualNPI, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );





$pdf->Text  ( 5, 208, '', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 13, 208, '', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 20.5, 208, '', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 28, 208, '', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 36, 208, '', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 44, 208, '', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 52, 208, '', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 70, 208, '', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 86, 208, '', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 116, 208, '', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 134, 208, '', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 146, 208, '', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 155.5, 208, '', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 180, 208, '', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );





$pdf->Text  ( 5, 217, '', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 13, 217, '', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 20.5, 217, '', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 28, 217, '', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 36, 217, '', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 44, 217, '', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 52, 217, '', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 70, 217, '', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 86, 217, '', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 116, 217, '', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 134, 217, '', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 146, 217, '', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 155.5, 217, '', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 180, 217, '', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );





$pdf->Text  ( 5, 225, '', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 13, 225, '', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 20.5, 225, '', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 28, 225, '', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 36, 225, '', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 44, 225, '', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 52, 225, '', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 70, 225, '', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 86, 225, '', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 116, 225, '', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 134, 225, '', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 146, 225, '', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 155.5, 225, '', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 180, 225, '', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );





$pdf->Text  ( 5, 233, '', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 13, 233, '', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 20.5, 233, '', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 28, 233, '', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 36, 233, '', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 44, 233, '', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 52, 233, '', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 70, 233, '', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 86, 233, '', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 116, 233, '', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 134, 233, '', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 146, 233, '', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 155.5, 233, '', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 180, 233, '', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );







$pdf->Text  ( 10, 249.5, $practiceEIN, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 50, 249.5, 'x', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 64, 249.5, $chartNo, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 99, 249.5, 'x', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 136, 249.5, $charge, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 149, 249.5, '', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 164, 249.5, '0', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 175, 249.5, '00', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 188, 249.5, '0', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 198, 249.5, '00', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->SetFont('times', '', 9);



$pdf->Text  ( 5, 266, $providerName, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 40, 269, $Currmonth.'/'.$Currdate.'/'.$Curryear, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 61, 259, $facilityName, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 61, 263, $facilityAddr[0], false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 61, 267, $facilityAddr[1], false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 61, 272, '1962856823', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 135, 259, $practiceName, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 135, 263, $practiceAddr[0], false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 135, 267, $practiceAddr[1], false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );



$pdf->Text  ( 136, 272, '1962856823', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );





$pdf->writeHTML($html, true, false, true, false, '');









// --- example with background set on page ---



// remove default header

$pdf->setPrintHeader(false);



// add a page

$pdf->AddPage();





// -- set new background ---



// get the current page break margin

$bMargin = $pdf->getBreakMargin();

// get current auto-page-break mode

$auto_page_break = $pdf->getAutoPageBreak();

// disable auto-page-break

$pdf->SetAutoPageBreak(false, 0);

// set bacground image

$img_file = K_PATH_IMAGES.'image_demo.jpg';

$pdf->Image($img_file, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);

// restore auto-page-break status

$pdf->SetAutoPageBreak($auto_page_break, $bMargin);

// set the starting point for the page content

$pdf->setPageMark();



$pdf->deletePage(2);





// Print a text

//$html = '<span style="color:white;text-align:center;font-weight:bold;font-size:80pt;">PAGE 3</span>';

//$pdf->writeHTML($html, true, false, true, false, '');



// ---------------------------------------------------------



//Close and output PDF document

$pdf->Output($_SERVER['DOCUMENT_ROOT'].'/HCFA-500.pdf', 'FI');



//============================================================+

// END OF FILE

//============================================================+



