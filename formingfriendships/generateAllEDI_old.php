<?php
 session_start();

 if (!isset($_SESSION['userID'])) {
  header("Location: login.php");
 }
 ?>
 <!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0" />
  <title>Generate EDI | AMROMED LLC</title>

  <!--=== CSS ===-->

  <!-- Bootstrap -->
  <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

  <!-- jQuery UI -->
  <!--<link href="plugins/jquery-ui/jquery-ui-1.10.2.custom.css" rel="stylesheet" type="text/css" />-->
  <!--[if lt IE 9]>
    <link rel="stylesheet" type="text/css" href="plugins/jquery-ui/jquery.ui.1.10.2.ie.css"/>
  <![endif]-->
  <!-- Theme -->
  <link href="assets/css/main.css" rel="stylesheet" type="text/css" />
  <link href="assets/css/plugins.css" rel="stylesheet" type="text/css" />
  <link href="assets/css/responsive.css" rel="stylesheet" type="text/css" />
  <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
  <link href="assets/css/alertify.css" rel='stylesheet' type='text/css'>
  <link href="assets/css/themes/default.css" rel='stylesheet' type='text/css'>

  <link rel="stylesheet" href="assets/css/fontawesome/font-awesome.min.css">
  <!--[if IE 7]>
    <link rel="stylesheet" href="assets/css/fontawesome/font-awesome-ie7.min.css">
  <![endif]-->

  <!--[if IE 8]>
    <link href="assets/css/ie8.css" rel="stylesheet" type="text/css" />
  <![endif]-->
  <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>

  <!--=== JavaScript ===-->

  <script type="text/javascript" src="assets/js/libs/jquery-1.10.2.min.js"></script>
  <script type="text/javascript" src="plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>
  <script type="text/javascript" src="plugins/bootstrap-switch/bootstrap-switch.min.js"></script>

  <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="assets/js/libs/lodash.compat.min.js"></script>

  <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
  <!--[if lt IE 9]>
    <script src="assets/js/libs/html5shiv.js"></script>
  <![endif]-->

  <!-- Smartphone Touch Events -->
  <script type="text/javascript" src="plugins/touchpunch/jquery.ui.touch-punch.min.js"></script>
  <script type="text/javascript" src="plugins/event.swipe/jquery.event.move.js"></script>
  <script type="text/javascript" src="plugins/event.swipe/jquery.event.swipe.js"></script>

  <!-- General -->
  <script type="text/javascript" src="assets/js/libs/breakpoints.js"></script>
  <script type="text/javascript" src="plugins/respond/respond.min.js"></script> <!-- Polyfill for min/max-width CSS3 Media Queries (only for IE8) -->
  <script type="text/javascript" src="plugins/cookie/jquery.cookie.min.js"></script>
  <script type="text/javascript" src="plugins/slimscroll/jquery.slimscroll.min.js"></script>
  <script type="text/javascript" src="plugins/slimscroll/jquery.slimscroll.horizontal.min.js"></script>

  <!-- Page specific plugins -->
  <!-- Charts -->
  <script type="text/javascript" src="plugins/sparkline/jquery.sparkline.min.js"></script>

  <script type="text/javascript" src="plugins/daterangepicker/moment.min.js"></script>
  <script type="text/javascript" src="plugins/daterangepicker/daterangepicker.js"></script>
  <script type="text/javascript" src="plugins/blockui/jquery.blockUI.min.js"></script>

  <!-- Pickers -->
  <script type="text/javascript" src="plugins/pickadate/picker.js"></script>
  <script type="text/javascript" src="plugins/pickadate/picker.date.js"></script>
  <script type="text/javascript" src="plugins/pickadate/picker.time.js"></script>
  <script type="text/javascript" src="plugins/bootstrap-colorpicker/bootstrap-colorpicker.min.js"></script>

  <!-- Noty -->
  <script type="text/javascript" src="plugins/noty/jquery.noty.js"></script>
  <script type="text/javascript" src="plugins/noty/layouts/top.js"></script>
  <script type="text/javascript" src="plugins/noty/themes/default.js"></script>

  <!-- Slim Progress Bars -->
  <script type="text/javascript" src="plugins/nprogress/nprogress.js"></script>

  <!-- Bootbox -->
  <script type="text/javascript" src="plugins/bootbox/bootbox.min.js"></script>

  <!-- App -->
  <script type="text/javascript" src="assets/js/app.js"></script>
  <script type="text/javascript" src="assets/js/plugins.js"></script>
  <script type="text/javascript" src="assets/js/plugins.form-components.js"></script>
  <script src="assets/js/jquery.datetimepicker.js"></script>
  <link href="assets/css/jquery.datetimepicker.css" rel="stylesheet" />

  <script>
  $(window).load(function() {
    "use strict";

    App.init(); // Init layout and core plugins
    Plugins.init(); // Init all plugins
    FormComponents.init(); // Init all form-specific plugins
    document.getElementById('practiceName').innerHTML = sessionStorage.getItem('practiceName');
    $('#loader').show();
    $.post("http://curismed.com/medService/patients",
      {
          practiceID: '1',
      },
      function(data, status){
          $('#patient').html('');
          $('#loader').hide();
          data.forEach(function(t) { 
              $('#patient').append('<option value="'+t.patientID+'">'+t.fullName+'</option>');
          });
      });
    var inv = "";
    $.ajax({
          type: "GET",
          url:"getLastEDI.php",
          async : false,
          data:{
          },success:function(result){
            inv = parseInt(result)+1;
            inv = pad(inv, 5);
         }
      });
    $('#btnGenerate').click(function(){
      $.ajax({
          type: "POST",
          url:"getClaimNoList.php",
          data:{
            practiceID : "1"
          },success:function(result){
            var n = result.length;
            result = result.substr(0,n-1);
            var res = JSON.parse(result);
            if(res.length != 0){
              for(var i=0; i< res.length;i++){
                var ClNo = res[i].ClaimNumber;
                $.ajax({
                  type: "POST",
                  url:"EDI_new.php",
                  data:{
                    ClaimNumber : ClNo
                  },success:function(result){
                    alertify.success("EDI generated successfully");
                  }
                });
              }
            }
            else{
              alert("No Claims are to be generated");
            }
         }
      });
    });
      // $('#btnGenerate').click(function(){
      //   $.ajax({
      //   type: "POST",
      //   url:"http://eibahealth.com/AkronService/generateall",
      //   data:{
      //     "practiceID" : '1',
      //     },success:function(result){
      //       var i,j,k,pat,invoice;
      //       var tempInc = 0;
      //       var inc = 0;
      //       var practiceID = sessionStorage.getItem('practiceId')
      //       var assignedProvider = "";
      //       var principalDiag = "";
      //       var defDiag2 = "";
      //       var defDiag3 = "";
      //       var defDiag4 = "";
      //       var fromDt = "";
      //       var toDt = "";
      //       var PrimaryCarePhysician = "";
      //       var placeOfService = "";
      //       var proced = "";
      //       var mod1 = "";
      //       var charge = "";
      //       var units = "";
      //       var payor = "";
      //       var unsent = [];
      //       var insName1 = "";
      //       var insAddr1 = "";
      //       var insName2 = "";
      //       var insAddr2 = "";
      //       var insureID1 = "";
      //       var insureID2 = "";
      //       var individualNPI = "";
      //       var providerName = "";
      //       var facilityName = "";
      //       var facilityAddr = "";
      //       var practiceEIN = "";
      //       var practiceAddr = "";
      //       var practiceName = "";
      //       var principalDiag = "";
      //       var defDiag2 = "";
      //       var defDiag3 = "";
      //       var defDiag4 = "";

      //       $.ajax({
      //           type: "GET",
      //           url:"getLastEDI.php",
      //           async : false,
      //           data:{
      //           },success:function(result){
      //             invoice = parseInt(result)+1;
      //             invoice = pad(invoice, 5);
      //          }
      //       });

      //       for(pat=0;pat<result.length;pat++){
      //         var chartNo = result[pat].chartNo;
      //         var len = result[pat].cases.length;
      //         if(len>=1){
      //           for(i=0;i<len;i++){
      //             var assignedProvider = result[pat].cases[i].assignedProvider;
      //             // var principalDiag = result[pat].cases[i].principalDiag;
      //             // var defDiag2 = result[pat].cases[i].defDiag2;
      //             // var defDiag3 = result[pat].cases[i].defDiag3;
      //             // var defDiag4 = result[pat].cases[i].defDiag4;
      //             var lenUnsent = result[pat].cases[i].unsentclaims.length;
      //             $.ajax({
      //                     type: "POST",
      //                     url:"physicianInfo.php",
      //                     async : false,
      //                     data:{
      //                       phyID : assignedProvider
      //                     },success:function(result){
      //                       var res = JSON.parse(result);
      //                       if(res.length!= ""){
      //                           individualNPI = res[0].individualNPI;
      //                           providerName = res[0].phyName;
      //                       }
      //                     }
      //                 });
      //                 $.ajax({
      //                     type: "POST",
      //                     url:"practiceInfo.php",
      //                     async : false,
      //                     data:{
      //                       practiceID : practiceID
      //                     },success:function(result){
      //                       var res = JSON.parse(result);
      //                       if(res.length!= ""){
      //                           practiceEIN = res[0].EIN;
      //                           practiceAddr = res[0].address;
      //                           practiceName = res[0].practiceName;
      //                        }
      //                    }
      //                 });

      //             if(lenUnsent>0){
      //               inc = inc+1;
      //               var lenPoli = result[pat].cases[i].policies.length;
      //               if(lenPoli>0){
      //                 var j = "";
      //                 if(lenPoli == 1){
      //                   insureID2 = "";
      //                 }
      //                 for(j=0;j<lenPoli;j++){
      //                 window['insureID'+(j+1)] = result[pat].cases[i].policies[j].policyNo;
      //                 window['insuranceID'+(j+1)] = result[pat].cases[i].policies[j].insuranceID;
      //                 //window['startDt'+(j+1)] = changeDateFormat(result[pat].cases[i].policies[j].startDt);
      //                 //window['endDt'+(j+1)] = changeDateFormat(result[pat].cases[i].policies[j].endDt);
      //                 var insID = window['insuranceID'+(j+1)];
      //                   $.ajax({
      //                           type: "POST",
      //                           url:"getInsuranceName.php",
      //                           async : false,
      //                           data:{
      //                             insuranceID : insID
      //                           },success:function(result){
      //                             var res11 = JSON.parse(result);
      //                             if(res11.length!= ""){
      //                                 window['insName'+(j+1)] = res11[0].payerName;
      //                                 window['insAddr'+(j+1)] = res11[0].addr;
      //                              }
      //                          }
      //                       });
      //                 }
      //               }
      //               else{
      //                 //alertify.error("There is no policies added for this case "+result[pat].cases[i].caseChartNo);
      //               }
      //               for(k=0;k<lenUnsent;k++){
      //                 var fromDt = changeDateFormat(result[pat].cases[i].unsentclaims[k].fromDt);
      //                 var toDt = changeDateFormat(result[pat].cases[i].unsentclaims[k].toDt);
      //                 var PrimaryCarePhysician = result[pat].cases[i].unsentclaims[k].PrimaryCarePhysician;
      //                 var placeOfService = result[pat].cases[i].unsentclaims[k].placeOfService;
      //                 var proced = result[pat].cases[i].unsentclaims[k].proced;
      //                 var mod1 = result[pat].cases[i].unsentclaims[k].mod1;
      //                 var total = result[pat].cases[i].unsentclaims[k].total;
      //                 var units = result[pat].cases[i].unsentclaims[k].units;
      //                 var servLocID = result[pat].cases[i].unsentclaims[k].serviceLocID;

      //                 var principalDiag = result[pat].cases[i].unsentclaims[k].diag1;
      //                 var defDiag2 = result[pat].cases[i].unsentclaims[k].diag2;
      //                 var defDiag3 = result[pat].cases[i].unsentclaims[k].diag3;
      //                 var defDiag4 = result[pat].cases[i].unsentclaims[k].diag4;

      //                 $.ajax({
      //                     type: "POST",
      //                     url:"facilityInfo.php",
      //                     async : false,
      //                     data:{
      //                       servLocID : servLocID
      //                     },success:function(result){
      //                       var res = JSON.parse(result);
      //                       if(res.length!= ""){
      //                           facilityName = res[0].billingName;
      //                           facilityAddr = res[0].address;
      //                        }
      //                    }
      //                 });
      //                 //unsent[k] = ['"'+fromDt+'"'+toDt+'"'+placeOfService+'"'+proced+'"'+mod1+'"'+charge+'"'+units+'"'];
      //                 $.ajax({
      //                     type: "POST",
      //                     url:"EDI.php",
      //                     async : false,
      //                     data:{
      //                       facilityName : facilityName,
      //                       facilityAddr : facilityAddr,
      //                       practiceEIN : practiceEIN,
      //                       practiceName : practiceName,
      //                       practiceAddr : practiceAddr,
      //                         fullName: result[pat].fullName,
      //                         dob : result[pat].dob,
      //                         gender : result[pat].gender,
      //                       relationship : result[pat].relationship,
      //                       ssn : result[pat].ssn,
      //                       martialStatus : result[pat].martialStatus,
      //                       medicalRecord : result[pat].medicalRecord,
      //                       employment : result[pat].employment,
      //                       employer : result[pat].employer,
      //                       reference : result[pat].reference,
      //                       addrStreet1 : result[pat].addrStreet1,
      //                       addrStreet2 : result[pat].addrStreet2,
      //                       addrCity : result[pat].addrCity,
      //                       addrState : result[pat].addrState,
      //                       addrCountry : result[pat].addrCountry,
      //                       addrZip : result[pat].addrZip,
                   
      //                       phoneHome : result[pat].phoneHome,
      //                       insureID : window['insureID1'],
      //                       secinsureID : window['insureID2'],
      //                       insuranceID : window['insName1'],
      //                       insuranceAddr : window['insAddr1'],
      //                       secinsuranceID : window['insName2'],
      //                       secinsuranceAddr : window['insAddr2'],
      //                       payor : payor,
      //                       //startDt : startDt,
      //                       //endDt : endDt,
      //                       assignedProvider : assignedProvider,
      //                       principalDiag : principalDiag,
      //                       defDiag2 : defDiag2,
      //                       defDiag3 : defDiag3,
      //                       defDiag4 : defDiag4,
      //                       fromDt : fromDt,
      //                       toDt : toDt,
      //                       PrimaryCarePhysician : PrimaryCarePhysician,
      //                       placeOfService : placeOfService,
      //                       proced : proced,
      //                       mod1 : mod1,
      //                       total : total,
      //                       units : units,
      //                       individualNPI : individualNPI,
      //                       providerName : providerName,
      //                       chartNo : chartNo,
      //                       invoiceNo : invoice
      //                   },success:function(result){
      //                     tempInc = tempInc +1;
      //                     console.log(inc + ' ' +tempInc);
      //                     if(tempInc != inc){
      //                       alertify.success("EDI generated successfully");
      //                       window.location.reload();
      //                     }
      //                     else{
      //                       alertify.success(inc+" EDI generated successfully");
      //                       window.location.reload();
      //                     }
      //                   }
      //                 });
      //               }
      //             }
      //             else{
      //               //alertify.error("There is no Claims available for this case "+result[pat].cases[i].caseChartNo);
      //             }
      //           }
      //         }
      //         else{
      //           //alertify.error("There is no policies added for this case")
      //         }
      //     }
      //     }
      // });
    //});
  });
  
  function pad(number, length) {
   
    var str = '' + number;
    while (str.length < length) {
        str = '0' + str;
    }
   
    return str;

}
  function changeDateFormat(inputDate){  // expects Y-m-d
        var splitDate = inputDate.split('-');
        if(splitDate.count == 0){
            return null;
        }

        var year = splitDate[0];
        var month = splitDate[1];
        var day = splitDate[2]; 

        return month + '-' + day + '-' + year;
    }
  </script>
  <style>
  .icon:hover{
    text-decoration: none;
  }
  a:hover{
    text-decoration: none;
  }
  </style>
  
  <!-- Demo JS -->
  <script type="text/javascript" src="assets/js/custom.js"></script>
  <script type="text/javascript" src="assets/js/demo/ui_general.js"></script>
</head>

<body>

  <!-- Header -->
    <header class="header navbar navbar-fixed-top" role="banner" style="background-image: url('assets/bg.jpg'); background-repeat: repeat-x;">
        <!-- Top Navigation Bar -->
        <div class="container">

            <!-- Only visible on smartphones, menu toggle -->
            <ul class="nav navbar-nav">
                <li class="nav-toggle"><a href="javascript:void(0);" title=""><i class="icon-reorder"></i></a></li>
            </ul>

            <!-- Logo -->
            <a class="navbar-brand" style="text-align:center;padding-right:50px;" href="javascript:void(0);">
                <img src="assets/logo2.png"/>
                <strong>medABA</strong>
            </a>
            <!-- /logo -->

            <!-- Sidebar Toggler -->
            <a href="#" class="toggle-sidebar bs-tooltip" data-placement="bottom" data-original-title="Toggle navigation">
                <i class="icon-reorder"></i>
            </a>
            <!-- /Sidebar Toggler -->

            <!-- Top Left Menu -->
            <ul class="nav navbar-nav navbar-left hidden-xs hidden-sm" style="list-style:none;">
                <li>
                    <a href="ViewPatient.php" class="dropdown-toggle">
                        Clients
                    </a>
                </li>
                <li>
                    <a href="scheduler.php" class="dropdown-toggle">
                        Scheduler
                    </a>
                </li>
                <li>
                    <a href="Deposit.php" class="dropdown-toggle">
                        Deposit
                    </a>
                </li>
                <li>
                    <a href="Ledger.php" class="dropdown-toggle">
                        Ledger
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                        EDI
                        <i class="icon-caret-down small"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="generateAllEDI.php">
                            <i class="icon-angle-right"></i>
                            Generate EDI
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="EDIList.php">
                            <i class="icon-angle-right"></i>
                            EDI List
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="charts.html" class="dropdown-toggle" data-toggle="dropdown">
                        Settings
                        <i class="icon-caret-down small"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="ViewPractice.php">
                            <i class="icon-angle-right"></i>
                            Practices
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="ViewPhysician.php">
                            <i class="icon-angle-right"></i>
                            Physicians
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="ViewServiceLoc.php">
                            <i class="icon-angle-right"></i>
                            Locations &amp; Facilities
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="javascript:void(0);" style="font-weight:600;">
                            Codes
                            </a>
                            <li>
                                <a href="ViewCPT.php">
                                <i class="icon-angle-right"></i>
                                CPT/Procedure
                                </a>
                            </li>
                            <li>
                                <a href="ViewDX.php">
                                <i class="icon-angle-right"></i>
                                ICD 10/ICD 9 Library
                                </a>
                            </li>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="insurances.php">
                            <i class="icon-angle-right"></i>
                            Insurances
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- /Top Left Menu -->

            <!-- Top Right Menu -->
            <ul class="nav navbar-nav navbar-right">
                <!-- Notifications -->
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-warning-sign"></i>
                        <span class="badge">5</span>
                    </a>
                    <ul class="dropdown-menu extended notification">
                        <li class="title">
                            <p>You have 5 new notifications</p>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="label label-success"><i class="icon-plus"></i></span>
                                <span class="message">New user registration.</span>
                                <span class="time">1 mins</span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="label label-danger"><i class="icon-warning-sign"></i></span>
                                <span class="message">High CPU load on cluster #2.</span>
                                <span class="time">5 mins</span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="label label-success"><i class="icon-plus"></i></span>
                                <span class="message">New user registration.</span>
                                <span class="time">10 mins</span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="label label-info"><i class="icon-bullhorn"></i></span>
                                <span class="message">New items are in queue.</span>
                                <span class="time">25 mins</span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="label label-warning"><i class="icon-bolt"></i></span>
                                <span class="message">Disk space to 85% full.</span>
                                <span class="time">55 mins</span>
                            </a>
                        </li>
                        <li class="footer">
                            <a href="javascript:void(0);">View all notifications</a>
                        </li>
                    </ul>
                </li>

                <!-- Tasks -->
                <li class="dropdown hidden-xs hidden-sm">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-tasks"></i>
                        <span class="badge">7</span>
                    </a>
                    <ul class="dropdown-menu extended notification">
                        <li class="title">
                            <p>You have 7 pending tasks</p>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="task">
                                    <span class="desc">Preparing new release</span>
                                    <span class="percent">30%</span>
                                </span>
                                <div class="progress progress-small">
                                    <div style="width: 30%;" class="progress-bar progress-bar-info"></div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="task">
                                    <span class="desc">Change management</span>
                                    <span class="percent">80%</span>
                                </span>
                                <div class="progress progress-small progress-striped active">
                                    <div style="width: 80%;" class="progress-bar progress-bar-danger"></div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="task">
                                    <span class="desc">Mobile development</span>
                                    <span class="percent">60%</span>
                                </span>
                                <div class="progress progress-small">
                                    <div style="width: 60%;" class="progress-bar progress-bar-success"></div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="task">
                                    <span class="desc">Database migration</span>
                                    <span class="percent">20%</span>
                                </span>
                                <div class="progress progress-small">
                                    <div style="width: 20%;" class="progress-bar progress-bar-warning"></div>
                                </div>
                            </a>
                        </li>
                        <li class="footer">
                            <a href="javascript:void(0);">View all tasks</a>
                        </li>
                    </ul>
                </li>

                <!-- Messages -->
                <li class="dropdown hidden-xs hidden-sm">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-envelope"></i>
                        <span class="badge">1</span>
                    </a>
                    <ul class="dropdown-menu extended notification">
                        <li class="title">
                            <p>You have 3 new messages</p>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="photo"><img src="assets/img/demo/avatar-1.jpg" alt="" /></span>
                                <span class="subject">
                                    <span class="from">Bob Carter</span>
                                    <span class="time">Just Now</span>
                                </span>
                                <span class="text">
                                    Consetetur sadipscing elitr...
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="photo"><img src="assets/img/demo/avatar-2.jpg" alt="" /></span>
                                <span class="subject">
                                    <span class="from">Jane Doe</span>
                                    <span class="time">45 mins</span>
                                </span>
                                <span class="text">
                                    Sed diam nonumy...
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="photo"><img src="assets/img/demo/avatar-3.jpg" alt="" /></span>
                                <span class="subject">
                                    <span class="from">Patrick Nilson</span>
                                    <span class="time">6 hours</span>
                                </span>
                                <span class="text">
                                    No sea takimata sanctus...
                                </span>
                            </a>
                        </li>
                        <li class="footer">
                            <a href="javascript:void(0);">View all messages</a>
                        </li>
                    </ul>
                </li>

                <!-- .row .row-bg Toggler -->
                <li>
                    <a href="#" class="dropdown-toggle row-bg-toggle">
                        <i class="icon-resize-vertical"></i>
                    </a>
                </li>


                <!-- User Login Dropdown -->
                <li class="dropdown user">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <!--<img alt="" src="assets/img/avatar1_small.jpg" />-->
                        <i class="icon-male"></i>
                        <span class="username" id="practiceName"></span>
                        <i class="icon-caret-down small"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="javascript:void(0);"><i class="icon-user"></i> My Profile</a></li>
                        <li><a href="javascript:void(0);"><i class="icon-calendar"></i> My Calendar</a></li>
                        <li><a href="javascript:void(0);"><i class="icon-tasks"></i> My Tasks</a></li>
                        <li class="divider"></li>
                        <li><a href="login.php"><i class="icon-key"></i> Log Out</a></li>
                    </ul>
                </li>
                <!-- /user login dropdown -->
            </ul>
            <!-- /Top Right Menu -->
        </div>
        <!-- /top navigation bar -->

    </header> <!-- /.header -->

  <div id="container">
    <!-- Breadcrumbs line -->
    <div class="crumbs">
      <ul id="breadcrumbs" class="breadcrumb">
        <li class="current">
          <i class="icon-home"></i>
          <a href="index.html">Dashboard</a>
        </li>
        
      </ul>
      <a href="javascript:void(0);"><img src="assets/icons/Settings.png" title="Settings" style="float:right; padding-top:0.3em; padding-right:2em;"/></a>
          <a href="Ledger.php"><img src="assets/icons/Claim_search.png" title="Claim Search" style="float:right; padding-top:0.3em; padding-right:2em;"/></a>
          <a href="AddClaim.php"><img src="assets/icons/Claim_add.png" title="Add Claim" style="float:right; padding-top:0.3em; padding-right:2em;"/></a>
          <a href="ViewPatient.php"><img src="assets/icons/Patient_search.png" title="Patient Search" style="float:right; padding-top:0.3em; padding-right:2em;"/></a>
          <a href="AddPatient.php"><img src="assets/icons/Patient_add.png" title="Add Patient" style="float:right; padding-top:0.3em; padding-right:2em;"/></a>
          <a href="dashboard.html"><img src="assets/icons/Home.png" title="Home" style="float:right; padding-top:0.3em; padding-right:2em;"/></a>

    </div>
    <div class="clearfix"></div>
    <!-- /Breadcrumbs line -->

    <div id="content">
      <img src="assets/beat.gif" id="loader" style="position:absolute; left:50%; top:30%;" />
      <h3 style="margin:20px 20px 20px 20px;">Generate EDI</h3>
            <div class="clearfix"></div>
            <div class="form-group col-md-12" style="margin-bottom:50px;">
                <input type="button" class="btn btn-primary" value="Generate Bulk EDI" id="btnGenerate"/>
            </div>
    </div>
  </div>
  <link href="plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
  <link href="https://cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css" rel='stylesheet' type='text/css'>
  <script type="text/javascript" src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" src="assets/js/alertify.js"></script>
</body>
</html>