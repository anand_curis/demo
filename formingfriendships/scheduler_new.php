<?php
 session_start();

 if (!isset($_SESSION['userID'])) {
    header("Location: index.html");
 }
 include('header.html');
 ?>

<div id="scheduler_here" class="dhx_cal_container" style='width:100%; height:100%;'>
	<div class="dhx_cal_navline">
		<div class="dhx_cal_prev_button">&nbsp;</div>
		<div class="dhx_cal_next_button">&nbsp;</div>
		<div class="dhx_cal_today_button"></div>
		<div class="dhx_cal_date"></div>
		<div class="dhx_cal_tab" name="day_tab" style="right:204px;"></div>
		<div class="dhx_cal_tab" name="week_tab" style="right:140px;"></div>
		<div class="dhx_cal_tab" name="month_tab" style="right:76px;"></div>
	</div>
	<div class="dhx_cal_header">
	</div>
	<div class="dhx_cal_data">
	</div>
</div>
<style type="text/css">
html, body{
        margin:0px;
        padding:0px;
        height:100%;
        overflow:hidden;
    } 
    .dhx_cal_event div.dhx_footer,
		.dhx_cal_event.past_event div.dhx_footer,
		.dhx_cal_event.event_UnConfirmed div.dhx_footer,
		.dhx_cal_event.event_math div.dhx_footer,
		.dhx_cal_event.event_Confirmed div.dhx_footer{
			background-color: transparent !important;
		}
		.dhx_cal_event .dhx_body{
			-webkit-transition: opacity 0.1s;
			transition: opacity 0.1s;
			opacity: 0.7;
		}
		.dhx_cal_event .dhx_title{
			line-height: 12px;
		}
		.dhx_cal_event_line:hover,
		.dhx_cal_event:hover .dhx_body,
		.dhx_cal_event.selected .dhx_body,
		.dhx_cal_event.dhx_cal_select_menu .dhx_body{
			opacity: 1;
		}

		.dhx_cal_event.event_NoShow div, .dhx_cal_event_line.event_NoShow{
			background-color: orange !important;
			border-color: #a36800 !important;
		}
		.dhx_cal_event_clear.event_NoShow{
			color:orange !important;
		}

		.dhx_cal_event.event_Confirm div, .dhx_cal_event_line.event_Confirm{
			background-color: #36BD14 !important;
			border-color: #698490 !important;
		}
		.dhx_cal_event_clear.event_Confirm{
			color:#36BD14 !important;
		}

		.dhx_cal_event.event_UnConfirm div, .dhx_cal_event_line.event_UnConfirm{
			background-color: #FC5BD5 !important;
			border-color: #839595 !important;
		}
		.dhx_cal_event_clear.event_UnConfirm{
			color:#B82594 !important;
		}
		.highlighted_timespan {
			background-color: #87cefa;
			opacity:0.5;
			filter:alpha(opacity=50);
			cursor: pointer;
			z-index: 0;
		}
		.dhx_menu_icon.icon_location{
		  background-image: url('location_icon.png');  
		}  
</style>


	<script type="text/javascript" charset="utf-8">
	$(window).load(function(){
		init();
	});

		function init() {
			scheduler.config.xml_date = "%Y-%m-%d %H:%i";
			var appointments = <?php include('appointments.php'); ?>;

			scheduler.parse(appointments,"json");

		}

	</script>

<?php
include('footer.html');
 ?>
