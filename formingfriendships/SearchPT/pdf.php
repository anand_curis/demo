<?php
//============================================================+
// File name   : example_048.php
// Begin       : 2009-03-20
// Last Update : 2013-05-14
//
// Description : Example 048 for TCPDF class
//               HTML tables and table headers
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com LTD
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: HTML tables and table headers
 * @author Nicola Asuni
 * @since 2009-03-20
 */

// Include the main TCPDF library (search for installation path).
require_once('tcpdf_include.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Nicola Asuni');
$pdf->SetTitle('TCPDF Example 048');
$pdf->SetSubject('TCPDF Tutorial');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('helvetica', 'B', 20);

// add a page
$pdf->AddPage();

$pdf->SetFont('helvetica', '', 8);

// -----------------------------------------------------------------------------

$tbl = <<<EOD
  <table style="width:100%;  margin:100px;">
  <tr>
     <th style="border:1px solid #000; background-color:#ccc;">DUNS #</th>
     <th style="border:1px solid #00; background-color:#ccc;">Page</th>
    <th rowspan="2" style="border-top:none; border-bottom:none; text-align:center">PNA Associates<br>SMITH &amp; GRAHAM<br>1407 BROADWAY ROON NO.1709<br>NEW YORK, NY 10018</th>
     <th style="border:1px solid #000; background-color:#ccc;">INVOICE #</th>
     <th style="border:1px solid #000; background-color:#ccc;">DATE</th>
  </tr>
  <tr>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
  </tr>
</table>
<br>
  <table style="width:100%;">
  <tr>
     <td style="border:1px solid #000; background-color:#ccc;">SOLD TO</td>
     <td style="border:1px solid #000">PREMIUM DENIM<br>3611 GERMANTOWN AVE<br>PHILADELPHIA, PA 

19140</td>
    <td  style="border-top:none; border-bottom:none; text-align:center;"><br><br>TEL : (212) 869-6275<br>FAX : (212) 869-6284<br><h1>INVOICE</h1></td>
     <td style="border:1px solid #000; background-color:#ccc;">SHIP TO</td>
     <td style="border:1px solid #000">PREMIUM DENIM<br>3611 GERMANTOWN AVE<br>PHILADELPHIA, PA 

19140</td>
  </tr>
</table>

<br>

  <table style="width:100%; border:1px solid #000;">
  <tr>
     <th style="border:1px solid #000; background-color:#ccc;">CUST. ORD</th>
     <th style="border:1px solid #000; background-color:#ccc;">DEPT. #</th>
     <th style="border:1px solid #000; background-color:#ccc;">STORE</th>
     <th style="border:1px solid #000; background-color:#ccc;">CUST. #</th>
     <th style="border:1px solid #000; background-color:#ccc;">OUR PO</th>
     <th style="border:1px solid #000; background-color:#ccc;">VNDR. #</th>
     <th style="border:1px solid #000; background-color:#ccc;">SALESMAN</th>
     <th style="border:1px solid #000; background-color:#ccc;">TERMS</th>
     <th style="border:1px solid #000; background-color:#ccc;">CTN</th>
     <th style="border:1px solid #000; background-color:#ccc;">SHIP VIA</th>
  </tr>
  <tr>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
  </tr>
</table>

<br>

<table style="width:100%; padding:10px;">
  <tr>
    <th style="border:none;">Payment Due :</th>
    <th style="border:none;">                      </th>
    <th style="border:none;">Total Pcs : </th>
    <th style="border:none;">                         </th>
    <th style="border:none;">Order # :</th>
    <th style="border:none;">                       </th>
    <th style="border:none;">Weight :</th>
    <th style="border:none;">                  </th>
    <th style="border:none;">Cancel Date :</th>
    <th style="border:none;"></th>
  </tr>
</table>

<br>

  <table style="width:100%; border:1px solid #000;">
  <tr>
    <th rowspan="2" style="border:1px solid #000">STYLE</th>
    <th rowspan="2" style="border:1px solid #000">COLOR</th>
    <th rowspan="2" style="border:1px solid #000">DESCRIPTION</th>
    <th colspan="15" style="border:1px solid #000">SIZES BREAKDOWN</th>
    <th rowspan="2" style="border:1px solid #000">QTY</th>
    <th rowspan="2" style="border:1px solid #000">PRICE</th>
    <th rowspan="2" style="border:1px solid #000">DISCT</th>
    <th rowspan="2" style="border:1px solid #000">AMOUNT</th>
  </tr>
  <tr>
     <td style="border:1px solid #000; background-color:#ccc;">S</td>
     <td style="border:1px solid #000; background-color:#ccc;">M</td>
     <td style="border:1px solid #000; background-color:#ccc;">L</td>
     <td style="border:1px solid #000; background-color:#ccc;">XL</td>
     <td style="border:1px solid #000; background-color:#ccc;">XXL</td>
     <td style="border:1px solid #000; background-color:#ccc;">3X</td>
     <td style="border:1px solid #000; background-color:#ccc;">4X</td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
  </tr>
  <tr>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
  </tr>
  <tr>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
  </tr>
  <tr>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
  </tr>
  <tr>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
  </tr>
  <tr>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
  </tr>
  <tr>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
  </tr>
  <tr>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
  </tr>
  <tr>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
  </tr>
  <tr>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
  </tr>
  <tr>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
  </tr>
  <tr>
    <td colspan="18" style="border:1px solid #000">All sales are Final.<br>All claims must be presented within 15 days of receipt of Merchandise. Merchandise returned for credit must be freight  prepaid and must have prior written consent of the seller.</td>
    <td colspan="2" style="border:1px solid #000">SUB TOTAL:</td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
  </tr>
  <tr>
    <td colspan="18" rowspan="2" style="border:1px solid #000">TRACKING # : </td>
    <td colspan="2" style="border:1px solid #000">FREIGHT:</td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
  </tr>
  <tr>
    <td colspan="2">TOTAL DUE: $</td>
     <td style="border:1px solid #000"></td>
     <td style="border:1px solid #000"></td>
  </tr>
</table>

EOD;

$pdf->writeHTML($tbl, true, false, false, false, '');

// -----------------------------------------------------------------------------

//Close and output PDF document
$pdf->Output('example_048.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+