<?php
//============================================================+
// File name   : example_048.php
// Begin       : 2009-03-20
// Last Update : 2013-05-14
//
// Description : Example 048 for TCPDF class
//               HTML tables and table headers
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com LTD
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: HTML tables and table headers
 * @author Nicola Asuni
 * @since 2009-03-20
 */

// Include the main TCPDF library (search for installation path).
require_once('tcpdf_include.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Nicola Asuni');
$pdf->SetTitle('TCPDF Example 048');
$pdf->SetSubject('TCPDF Tutorial');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('helvetica', 'B', 20);

// add a page
$pdf->AddPage();

$pdf->SetFont('helvetica', '', 8);

// -----------------------------------------------------------------------------

$tbl = <<<EOD
<h1 style="text-align: center">Tracking Info</h1>

    <h5 style="text-align: center"> For Customer</h5>
    <table class="table table-bordered"  style="font-size: 12px;text-align: center" border="1">
        <tr >
            <th colspan="2" >
                <span style="font-weight:100">Order Number </span> : 1893  <br>
                <span style="font-weight:100">Customer PO</span> : T262294
            </th>
            <th colspan="6">
                <span style="font-weight:100">Invoice Number</span>  :  12485675<br>
                <span style="font-weight:100">Printed Date</span>: {{nowDate}}
            </th>
        </tr>
        <tr >
            <th colspan="2">
                <span style="font-weight:100">Customer Name </span>  : Anandharaman <br>
                <span style="font-weight:100">Status</span> : Shipped<br>
                <span style="font-weight:100">Shipping Via</span> :  UPS
            </th>
            <th colspan="6">
                <span style="font-weight:100">Customer Email</span> : admire.ananth@gmail.com<br>
                <span style="font-weight:100">Terms</span> : TO BE ANNOUNCED<br>
                <span style="font-weight:100">COD </span>  : Yes
            </th>
        </tr>
        <tr align="center">
            <th rowspan="2" style="text-align:center;">Product Code</th>
            <th rowspan="2" style="text-align:center;">Product Details</th>
        </tr>
        <tr >
            <th colspan="1" style="text-align:center;">Nos</th>
            <th colspan="2" style="text-align:center;">Cost</th>
            <th colspan="1" style="text-align:center;">Total</th>
        </tr>
        <tbody>
        <tr >
            <td><span style="font-weight:600">TEES453</span></td>
            <td><span style="font-weight:600">Yellow Tees</span></td>
            <td>3</td>
            <td colspan="2">8</td>
            <td style="font-weight:600">24</td>

        </tr>
        <tr>
            <td><span style="font-weight:600">SHR5657</span></td>
            <td><span style="font-weight:600">Blue Shirt</span></td>
            <td><span style="">2</span></td>
            <td colspan="2"> 12</td>
            <td style="font-weight:600">24</td>

        </tr>
        </tbody>
        <tfoot style="text-align: left">
        <tr>
           <td colspan="5" style="text-align: right;font-weight:600">Total</td>
           <td colspan="1" style="text-align: center; font-weight:600">$48</td>
        </tr>

        </tfoot>
    </table>
EOD;

$pdf->writeHTML($tbl, true, false, false, false, '');

// -----------------------------------------------------------------------------

//Close and output PDF document
$pdf->Output('example_048.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+