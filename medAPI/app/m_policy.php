<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class m_policy extends Model
{
    //
    protected $table = 'm_policies';
    protected $fillable = array('caseID','authNo','policyEntity','insuranceID','policyHolder','relationshipToInsured','relationID','policyNo','groupNo','groupName','claimNo','startDt','endDt','assigned','crossoverClaim','deductibleMet','annualDeductible','coPayment','treatmentAuth','docCtrlNo','insClassA','insClassB','insClassC','insClassD','insClassE','insClassF','insClassG','insClassH');

    public function _case(){
        return $this->belongsTo('App\m_case','caseID','caseID');
    }
}
