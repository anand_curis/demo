<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class m_codeicd extends Model
{
    protected $table = 'm_codeicd';
    protected $fillable = array('icdCode','dxCode','longDescription','shortDescription','codeType');
}
