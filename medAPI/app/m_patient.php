<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class m_patient extends Model
{
    protected $fillable = array('fullName', 'dob', 'gender','ssn', 'isActive','email','phoneMobile','practiceID','relationship','martialStatus','medicalRecord','employment','employer','reference','addrStreet1','addrStreet2','addrCity','addrState','addrCountry','addrZip','phoneHome','phoneWork','notifyEmail','notifyPhone','IsFinancialResp','emergencyContact','emergencyPhone','createdBy','lastEditedBy','primaryCarePhy','referringPhy','defaultRenderPhy','defaultServiceLoc');

    public function cases(){
        return $this->hasMany('App\m_case','patientID','patientID')->where('isDeleted',0);
    }

    //public function claims()
    //{
     //   return $this->hasManyThrough('App\m_claim', 'App\m_case', 'patientID', 'caseID');
    //}
}
