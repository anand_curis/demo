<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class m_insurance extends Model
{
    protected $fillable = array('payerName','enr','typ','st','lob','rte','rts','era','sec','note','clearingHousePayorID','addr','city','state','zip','phone','fax','abforbilling','practiceID','isActive','createdBy','created_At','updated_At');
}
