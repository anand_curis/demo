<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class m_placeofservice extends Model
{
    //
    protected $fillable = array('placeCode','placeName','description');
}
