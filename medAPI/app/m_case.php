<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class m_case extends Model
{
    //
    protected $fillable = array('patientID',	'isClosed',	'description',	'allowPrint',	'isDeleted',	'caseDt',	'caseChartNo',	'globalCoverageUntil',	'employer',	'empstatus',	'retirementDate',	'location',	'workPhone',	'extension',	'studStatus',	'guarantor',	'maritalStatus',	'assignedProvider',	'referringProvider',	'supervisingProvider',	'operatingProvider',	'otherProvider',	'referralSource',	'attorney',	'facility',	'caseBillingCode',	'priceCode',	'otherArrangements',	'authorizedThrough',	'authNo',	'lastVisit',	'noOfVisits',	'authID',	'lastVisitNo',	'principalDiag',	'defDiag2',	'defDiag3',	'defDiag4',	'poa1',	'poa2',	'poa3',	'poa4',	'allergiesNotes',	'ediNotes',	'repTypeCode',	'attachCtrlNo',	'repTransCode',	'injuryDt',	'illnessIndicator',	'firstConsultDate',	'similarSymptoms',	'sameSymptom',	'empRelated',	'emergency',	'relatedTo',	'relatedState',	'nature',	'lastXRay',	'deathStat',	'unableToWorkFrm',	'unableToWorkTo',	'totalDisabFrm',	'totDisabTo',	'partDisabFrm',	'partDisabTo',	'hospFrm',	'hospTo',	'returnWorkIndicator',	'disabilityPercent',	'lastWorkDate',	'pregnant',	'estimatedDOB',	'dateAssumedCare',	'dateRelinquishedCare',	'outsideLab',	'labCharges',	'localUseA',	'localUseB',	'indicator',	'refDate',	'prescripDate',	'priorAuthNo',	'extra1',	'extra2',	'extra3',	'extra4',	'outsideProvider',	'dateLastSeen',	'epsdt',	'resubmissionDt',	'familyPlanning',	'originalRefNo',	'serviceAuth',	'nonAvailIndicator',	'branchService',	'sponsorStat',	'splProgram',	'sponsorGuide',	'effStartDt',	'effEndDt',	'carePlanOversight',	'hospiceNo',	'CLIANo',	'mamoCertification',	'refAccessNo',	'demoCode',	'assignIndicator',	'insuranceTypeCode',	'timelyFillingIndicator',	'EPSDTRefCode1',	'EPSDTRefCode2',	'EPSDTRefCode3',	'homebound',	'IDENo',	'conditionIndicator',	'certCodeApplies',	'codeCategory',	'totalVisitRendered',	'totalVisitProjected',	'noOfVisitsHomeHealth',	'noOfUnits',	'duration',	'disciplineTypeCode',	'deliveryPatternCode',	'deliveryTimeCode',	'freqPeriod',	'freqCount',	'createdBy',	'isCash');

    public function patient(){
        return $this->belongsTo('App\m_patient','patientID','patientID');
    }

    public function claims(){
        return $this->hasMany('App\m_claim','caseID','caseID')->where('isDeleted',0);
    }

    public function unsentclaims(){
        return $this->hasMany('App\m_claim','caseID','caseID')->where(['isDeleted'=>0,'isSent'=>0])->selectRaw('m_claims.*, (select phyName from m_physicians where physicianID=m_claims.primaryCarePhy) PrimaryCarePhysician');
    }

    public function allclaims(){
        //return $this->hasMany('App\m_claim','caseID','caseID')->selectRaw('m_claims.*, (select phyName from m_physicians where physicianID=m_claims.primaryCarePhy) PrimaryCarePhysician')->groupBy('m_claims.ClaimNumber');
        return $this->hasMany('App\m_claim','caseID','caseID')->selectRaw('m_claims.*, (select phyName from m_physicians where physicianID=m_claims.primaryCarePhy) PrimaryCarePhysician');
        //return $this->hasMany('App\m_claim','caseID','caseID')->selectRaw('m_claims.*, (select phyName from m_physicians where physicianID=m_claims.primaryCarePhy) PrimaryCarePhysician');
    }

    public function groupedclaimsnumbers(){
        return $this->hasMany('App\m_claim','caseID','caseID')->selectRaw('m_claims.claimID, m_claims.caseID, m_claims.ClaimNumber')->groupBy('m_claims.ClaimNumber');
    }

/*    public function claimnumbers(){
        return $this->hasMany('App\m_claim','caseID','caseID')->selectRaw('m_claims.ClaimNumber');
    }*/

    public function policies(){
        return $this->hasMany('App\m_policy','caseID','caseID');
    }
}
