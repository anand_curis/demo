<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class m_serviceLocation extends Model
{
    //
    protected $table = 'm_serviceLocations';
    protected $fillable = array('internalName','NPI','overrideEIN','timeZone','legacyNoType','legacyNo','billingName','address','phone','fax','placeOfService','CLIANo','typeOfBill','payToName','instAddress','instPhone','instFax','createdBy','practiceID','isActive');
}
