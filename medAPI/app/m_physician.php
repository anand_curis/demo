<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class m_physician extends Model
{
    //
    protected $fillable = array('phyName','phoneHome','phoneWork','phoneMobile','phyEmail','dob','individualNPI','ssno','speciality','degree','address','fax','notes','createdBy','practiceID');
}
