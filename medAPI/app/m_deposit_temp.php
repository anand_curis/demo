<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class m_deposit extends Model
{
    protected $fillable = array('depositDate','chequeNo','chequeDate','payorID','payorType','amount','paymentType','notes','createdBy','isDeleted','practiceID');
}
