<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class m_codecpt extends Model
{
    protected $table = 'm_codecpt';
    protected $fillable = array('cptCode','description','charge');
    //
}
