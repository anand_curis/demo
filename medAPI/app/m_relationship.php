<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class m_relationship extends Model
{
    //
    protected $fillable = array('fullName', 'dob', 'gender','ssn', 'isActive','email','phoneMobile','practiceID','relationship','martialStatus','medicalRecord','employment','employer','reference','addrStreet1','addrStreet2','addrCity','addrState','addrCountry','addrZip','phoneHome','phoneWork','notifyEmail','notifyPhone','emergencyContact','emergencyPhone','createdBy','lastEditedBy','	referralSource');
}
