<?php
/**
 * Created by PhpStorm.
 * User: Emmanuel
 * Date: 24-May-16
 * Time: 7:28 AM
 */

namespace App\Classes;

use App\m_log;

class Common{
    public static function WriteLog($module, $logdata, $createdBy, $action, $refID)
    {
        m_log::create(['logModule' => $module, 'logDetail' => $logdata, 'createdBy' => $createdBy, 'logAction' => $action, 'logRefID' => $refID]);
        return;
    }
}