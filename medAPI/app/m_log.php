<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class m_log extends Model
{
    protected $fillable = array('logModule','logDetail','createdBy','logAction','logRefID');

}
