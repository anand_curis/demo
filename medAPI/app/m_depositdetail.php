<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class m_depositdetail extends Model
{
    protected $fillable = array('depositID','claimID','patientID','amountAdjusted','adjustment','claimBalanceIns','claimBalancePat','isDeleted','createdBy','adjustmentDetails','adjustmentCode','adjustmentNotes');
}
