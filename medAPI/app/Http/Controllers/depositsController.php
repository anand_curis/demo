<?php

namespace App\Http\Controllers;

use App\m_claim;
use App\m_deposit;
use App\m_depositdetail;
use App\m_insurance;
use App\m_patient;
use App\m_practice;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use Response;
use Illuminate\Support\Facades\Input;
use App\Classes\Common;

class depositsController extends Controller
{
    public function loadDeposits(Request $request)
    {
        $validator = Validator::make(Input::all(), ['practiceID'=>'required']);
        if ($validator->fails()) {
            return \Response::json(array('errors' => true, 'message' => $validator->messages()));
        }
        else {
            $pid = (int)Input::get('practiceID');
            if ($pid <= 0) { return \Response::json(array('errors' => true, 'message' => 'Invalid Practice ID.')); }

            if (m_practice::where(['practiceID'=>Input::get('practiceID')])->exists()) {
                //$deposits = m_deposit::where(['practiceID' => $pid,'isDeleted'=>0])->get();
                $deposits = m_deposit::join('m_insurances', 'm_insurances.insuranceID', '=', 'm_deposits.payorID')
                            ->where(['m_deposits.practiceID' => $pid,'m_deposits.isDeleted'=>0])
                            ->select(['m_deposits.*','m_insurances.payerName'])
                            ->selectRaw('(m_deposits.amount - (select sum(amountAdjusted) from m_depositdetails where m_depositdetails.depositID=m_deposits.depositID and m_depositdetails.isDeleted=0)) as Balance')
                            ->get();
                //Common::WriteLog('Deposits','Deposits Listed. Practice ID:' . $pid,0);
                //->select(['m_deposits.*','m_insurances.payerName'])

/*                $claims = m_claim::join('m_cases', 'm_claims.caseID', '=', 'm_cases.caseID')
                    ->join('m_patients', 'm_cases.patientID', '=', 'm_patients.patientID')
                    ->where(['m_patients.patientID' => $pid,'m_patients.isDeleted'=>0,'m_cases.isDeleted'=>0,'m_claims.isDeleted'=>0])
                    ->select('m_claims.*')
                    ->get();*/
                return $deposits;
            }else{
                return \Response::json(array('errors' => true, 'message' => 'Invalid Practice ID.'));
            }
        }
    }

    public function createDeposit(Request $request)
    {
        $validator = Validator::make(Input::all(), ['practiceID'=>'required','amount'=>'required','chequeNo'=>'required','chequeDate'=>'required','payorID'=>'required','payorType'=>'required','paymentType'=>'required']);
        if ($validator->fails()) {
            return \Response::json(array('errors' => true, 'message' => $validator->messages()));
        }
        else {
            $pid = (int)Input::get('practiceID');
            if ($pid <= 0) { return \Response::json(array('errors' => true, 'message' => 'Invalid Practice ID.')); }

            if (m_practice::where(['practiceID'=>Input::get('practiceID')])->exists()) {
                if (m_insurance::where(['insuranceID'=>Input::get('payorID')])->exists()){} else { return \Response::json(array('errors' => true, 'message' => 'Invalid Payor ID.')); }

                $request->merge( array( 'isDeleted' => 0) );
                $depositID = m_deposit::create($request->all())->id;
                Common::WriteLog('Deposits','Deposit Created. Deposit ID:' . $depositID . ' Practice ID:' . $pid,0,'CreateDeposit',$depositID);
                return \Response::json(array('errors' => false, 'message' => 'Deposit Created Successfully!', 'DepositID' => $depositID));
            }else{
                return \Response::json(array('errors' => true, 'message' => 'Invalid Practice ID.'));
            }
        }
    }


    public function adjustDeposit(Request $request)
    {
        $validator = Validator::make(Input::all(), ['depositID'=>'required','amountAdjusted'=>'required','adjustment'=>'required','claimBalanceIns'=>'required','claimBalancePat'=>'required','patientID'=>'required','claimID'=>'required']);
        if ($validator->fails()) {
            return \Response::json(array('errors' => true, 'message' => $validator->messages()));
        }
        else {
            $did = (int)Input::get('depositID');
            if ($did <= 0) { return \Response::json(array('errors' => true, 'message' => 'Invalid Deposit ID.')); }

            if (m_deposit::where(['depositID'=>Input::get('depositID'),'isDeleted'=>0])->exists()) {

                $claimID = (int)Input::get('claimID');
                $patientID = (int)Input::get('patientID');
                if($claimID<=0 && $patientID<=0){
                    return \Response::json(array('errors' => true, 'message' => 'ClaimID or PatientID is necessary!'));
                }
                if($claimID>0 && $patientID>0){
                    return \Response::json(array('errors' => true, 'message' => 'Cannot use both ClaimID and PatientID!'));
                }
                if($claimID>0) {
                    $request->merge( array( 'patientID' => -1) );
                    if (m_claim::where(['claimID' => Input::get('claimID'), 'isDeleted' => 0])->exists()) {
                    } else {
                        return \Response::json(array('errors' => true, 'message' => 'Invalid ClaimID!'));
                    }
                }
                if($patientID>0) {
                    $request->merge( array( 'claimID' => -1) );
                    if (m_patient::where(['patientID' => Input::get('patientID'), 'isDeleted' => 0])->exists()) {
                    } else {
                        return \Response::json(array('errors' => true, 'message' => 'Invalid patientID!'));
                    }
                }
                $request->merge( array( 'isDeleted' => 0) );
                $depositdetID = m_depositdetail::create($request->all())->id;


                $tempPaid = m_claim::join('m_cases', 'm_claims.caseID', '=', 'm_cases.caseID')
                            ->join('m_policies', 'm_policies.caseID', '=', 'm_cases.caseID')
                            ->where(['claimID' => Input::get('claimID')])
                            ->get();
                $tempFinPaid = $tempPaid->count(); 
                if ($tempFinPaid >= 2) {
                    $tempPaid = m_claim::join('m_cases', 'm_claims.caseID', '=', 'm_cases.caseID')
                            ->join('m_policies', 'm_policies.caseID', '=', 'm_cases.caseID')
                            ->where(['claimID' => Input::get('claimID'), 'm_policies.policyEntity'=>2])
                            ->get(['m_policies.insuranceID']);
                    $tempFinPaid = $tempPaid->pluck('insuranceID')[0];

                    if (Input::has('insuranceID')){ m_claim::where(['claimID' => Input::get('claimID'), 'isDeleted' => 0])->update(['insuranceID' => $tempFinPaid]); }
                    m_claim::where(['claimID' => Input::get('claimID'), 'isDeleted' => 0])->update(['isSecClaim' => 1]);
                    m_claim::where(['claimID' => Input::get('claimID'), 'isDeleted' => 0])->update(['status' => 1]);

                }
                else{
                    if (Input::has('insuranceID')){ m_claim::where(['claimID' => Input::get('claimID'), 'isDeleted' => 0])->update(['insuranceID' => Input::get('insuranceID')]); }
                }

                //Update Flag in Claims  allowed,paid,adjustment,claimBalance,reason,status,copay,deductible,coins
                m_claim::where(['claimID' => Input::get('claimID'), 'isDeleted' => 0])->update(['isPosted' => 1]);
                if (Input::has('allowed')){ m_claim::where(['claimID' => Input::get('claimID'), 'isDeleted' => 0])->update(['allowed' => Input::get('allowed')]); }
                if (Input::has('paid')){ m_claim::where(['claimID' => Input::get('claimID'), 'isDeleted' => 0])->update(['paid' => 'paid' + Input::get('paid')]); }
                if (Input::has('adjustment')){ m_claim::where(['claimID' => Input::get('claimID'), 'isDeleted' => 0])->update(['adjustment' => Input::get('adjustment')]); }
                if (Input::has('claimBalanceIns')){ m_claim::where(['claimID' => Input::get('claimID'), 'isDeleted' => 0])->update(['claimBalance' => Input::get('claimBalanceIns') ]); }
                if (Input::has('claimBalancePat')){ m_claim::where(['claimID' => Input::get('claimID'), 'isDeleted' => 0])->update(['claimBalancePat' => Input::get('claimBalancePat')]); }
                if (Input::has('reason')){ m_claim::where(['claimID' => Input::get('claimID'), 'isDeleted' => 0])->update(['reason' => Input::get('reason')]); }
                if (Input::has('status')){ m_claim::where(['claimID' => Input::get('claimID'), 'isDeleted' => 0])->update(['status' => Input::get('status')]); }
                if (Input::has('copay')){ m_claim::where(['claimID' => Input::get('claimID'), 'isDeleted' => 0])->update(['copay' => Input::get('copay')]); }
                if (Input::has('deductible')){ m_claim::where(['claimID' => Input::get('claimID'), 'isDeleted' => 0])->update(['deductible' => Input::get('deductible')]); }
                if (Input::has('coins')){ m_claim::where(['claimID' => Input::get('claimID'), 'isDeleted' => 0])->update(['coins' => Input::get('coins')]); }

                Common::WriteLog('Deposits','Deposit Detail Created. Deposit Detail ID:' . $depositdetID . ' Claim ID:' .$claimID . ' Patient ID:' .$patientID,0,'CreateDepositDetail',$depositdetID);

                return \Response::json(array('errors' => false, 'message' => 'Deposit Detail Created Successfully!', 'DepositDetailID' => $depositdetID));
            }else{
                return \Response::json(array('errors' => true, 'message' => 'Invalid Deposit ID.'));
            }
        }
    }

    public function adjustPatDeposit(Request $request)
    {
        $validator = Validator::make(Input::all(), ['depositID'=>'required','amountAdjusted'=>'required','adjustment'=>'required','claimBalanceIns'=>'required','claimBalancePat'=>'required','patientID'=>'required','claimID'=>'required']);
        if ($validator->fails()) {
            return \Response::json(array('errors' => true, 'message' => $validator->messages()));
        }
        else {
            $did = (int)Input::get('depositID');
            if ($did <= 0) { return \Response::json(array('errors' => true, 'message' => 'Invalid Deposit ID.')); }

            if (m_deposit::where(['depositID'=>Input::get('depositID'),'isDeleted'=>0])->exists()) {

                $claimID = (int)Input::get('claimID');
                $patientID = (int)Input::get('patientID');
                if($claimID<=0 && $patientID<=0){
                    return \Response::json(array('errors' => true, 'message' => 'ClaimID or PatientID is necessary!'));
                }
                if($claimID>0 && $patientID>0){
                    return \Response::json(array('errors' => true, 'message' => 'Cannot use both ClaimID and PatientID!'));
                }
                if($claimID>0) {
                    $request->merge( array( 'patientID' => -1) );
                    if (m_claim::where(['claimID' => Input::get('claimID'), 'isDeleted' => 0])->exists()) {
                    } else {
                        return \Response::json(array('errors' => true, 'message' => 'Invalid ClaimID!'));
                    }
                }
                if($patientID>0) {
                    $request->merge( array( 'claimID' => -1) );
                    if (m_patient::where(['patientID' => Input::get('patientID'), 'isDeleted' => 0])->exists()) {
                    } else {
                        return \Response::json(array('errors' => true, 'message' => 'Invalid patientID!'));
                    }
                }
                $request->merge( array( 'isDeleted' => 0) );
                $depositdetID = m_depositdetail::create($request->all())->id;

                $tempPaid = m_claim::where(['claimID' => Input::get('claimID')])->get(['paid']);
                $tempFinPaid = $tempPaid->pluck('paid')[0];
                $tempBalIns = m_claim::where(['claimID' => Input::get('claimID')])->get(['claimBalance']);
                $tempFinBalIns = $tempBalIns->pluck('claimBalance')[0];
                $tempBalPat = m_claim::where(['claimID' => Input::get('claimID')])->get(['claimBalancePat']);
                $tempFinBalPat = $tempBalPat->pluck('claimBalancePat')[0];

                //Update Flag in Claims  allowed,paid,adjustment,claimBalance,reason,status,copay,deductible,coins
                m_claim::where(['claimID' => Input::get('claimID'), 'isDeleted' => 0])->update(['isPosted' => 1]);
                if (Input::has('allowed')){ m_claim::where(['claimID' => Input::get('claimID'), 'isDeleted' => 0])->update(['allowed' => Input::get('allowed')]); }
                if (Input::has('paid')){ m_claim::where(['claimID' => Input::get('claimID'), 'isDeleted' => 0])->update(['paid' => Input::get('paid') + $tempFinPaid]); }
                if (Input::has('adjustment')){ m_claim::where(['claimID' => Input::get('claimID'), 'isDeleted' => 0])->update(['adjustment' => Input::get('adjustment')]); }
                if (Input::has('claimBalanceIns')){ m_claim::where(['claimID' => Input::get('claimID'), 'isDeleted' => 0])->update(['claimBalance' => $tempFinBalIns - Input::get('paid') ]); }
                if (Input::has('claimBalancePat')){ m_claim::where(['claimID' => Input::get('claimID'), 'isDeleted' => 0])->update(['claimBalancePat' => $tempFinBalPat - Input::get('paid') ]); }
                if (Input::has('reason')){ m_claim::where(['claimID' => Input::get('claimID'), 'isDeleted' => 0])->update(['reason' => Input::get('reason')]); }
                if (Input::has('status')){ m_claim::where(['claimID' => Input::get('claimID'), 'isDeleted' => 0])->update(['status' => Input::get('status')]); }
                if (Input::has('copay')){ m_claim::where(['claimID' => Input::get('claimID'), 'isDeleted' => 0])->update(['copay' => Input::get('copay')]); }
                if (Input::has('deductible')){ m_claim::where(['claimID' => Input::get('claimID'), 'isDeleted' => 0])->update(['deductible' => Input::get('deductible')]); }
                if (Input::has('coins')){ m_claim::where(['claimID' => Input::get('claimID'), 'isDeleted' => 0])->update(['coins' => Input::get('coins')]); }
                if (Input::has('insuranceID')){ m_claim::where(['claimID' => Input::get('claimID'), 'isDeleted' => 0])->update(['insuranceID' => Input::get('insuranceID')]); }

                Common::WriteLog('Deposits','Deposit Detail Created. Deposit Detail ID:' . $depositdetID . ' Claim ID:' .$claimID . ' Patient ID:' .$patientID,0,'CreateDepositDetail',$depositdetID);

                return \Response::json(array('errors' => false, 'message' => 'Deposit Detail Created Successfully!', 'DepositDetailID' => $depositdetID));
            }else{
                return \Response::json(array('errors' => true, 'message' => 'Invalid Deposit ID.'));
            }
        }
    }

    public function depositDetails(Request $request)
    {
    $validator = Validator::make(Input::all(), ['depositID'=>'required']);
    if ($validator->fails()) {
        return \Response::json(array('errors' => true, 'message' => $validator->messages()));
    }
    else {
        $did = (int)Input::get('depositID');
        if ($did <= 0) { return \Response::json(array('errors' => true, 'message' => 'Invalid Deposit ID.')); }

        if (m_depositdetail::where(['depositID'=>Input::get('depositID')])->exists()) {
            $depositdets = m_depositdetail::where(['depositID' => $did,'isDeleted'=>0])->get();

            //Common::WriteLog('Deposits','Deposit Detail Listed. Deposit ID:' . $did,0);
            return $depositdets;
        }else{
            return \Response::json(array('errors' => true, 'message' => 'Invalid Deposit ID.'));
        }
    }
    }

    public function findBalance(Request $request)
    {
        $validator = Validator::make(Input::all(), ['depositID'=>'required']);
        if ($validator->fails()) {
            return \Response::json(array('errors' => true, 'message' => $validator->messages()));
        }
        else {
            $did = (int)Input::get('depositID');
            if ($did <= 0) { return \Response::json(array('errors' => true, 'message' => 'Invalid DepositID.')); }

            if (m_deposit::where(['depositID'=>Input::get('depositID'),'isDeleted'=>0])->exists()) {
                $dt = m_deposit::where(['depositID' => $did])->get(['amount']);
                $depositTotal = $dt->pluck('amount')[0];
                $bal = $depositTotal;
                if (m_depositdetail::where(['depositID'=>Input::get('depositID'),'isDeleted'=>0])->exists()) {
                    $detAdj = m_depositdetail::where(['depositID'=>Input::get('depositID'),'isDeleted'=>0])->sum('amountAdjusted');
                    $bal = $bal - $detAdj;
                }

                Common::WriteLog('Deposits','Balance Calculated. Deposit ID:' . $did . ' Balance:' . $bal,0,'Balance',$did);
                return $bal;
            }else{
                return \Response::json(array('errors' => true, 'message' => 'Invalid Deposit ID.'));
            }
        }
    }



    public function reverseAdjustDeposit(Request $request)
    {
        $validator = Validator::make(Input::all(), ['depositID'=>'required','patientID'=>'required','claimID'=>'required','amountAdjusted'=>'required']);
        if ($validator->fails()) {
            return \Response::json(array('errors' => true, 'message' => $validator->messages()));
        }
        else {
            $did = (int)Input::get('depositID');
            if ($did <= 0) { return \Response::json(array('errors' => true, 'message' => 'Invalid Deposit ID.')); }

            if (m_deposit::where(['depositID'=>Input::get('depositID'),'isDeleted'=>0])->exists()) {

                $claimID = (int)Input::get('claimID');
                $patientID = (int)Input::get('patientID');
                if($claimID<=0 && $patientID<=0){
                    return \Response::json(array('errors' => true, 'message' => 'ClaimID or PatientID is necessary!'));
                }
                if($claimID>0 && $patientID>0){
                    return \Response::json(array('errors' => true, 'message' => 'Cannot use both ClaimID and PatientID!'));
                }
                if($claimID>0) {
                    $request->merge( array( 'patientID' => -1) );
                    if (m_claim::where(['claimID' => Input::get('claimID'), 'isDeleted' => 0])->exists()) {
                    } else {
                        return \Response::json(array('errors' => true, 'message' => 'Invalid ClaimID!'));
                    }
                }
                if($patientID>0) {
                    $request->merge( array( 'claimID' => -1) );
                    if (m_patient::where(['patientID' => Input::get('patientID'), 'isDeleted' => 0])->exists()) {
                    } else {
                        return \Response::json(array('errors' => true, 'message' => 'Invalid patientID!'));
                    }
                }


                if (m_depositdetail::where(['patientID' => Input::get('patientID'), 'claimID' => Input::get('claimID'), 'IsDeleted' => 0, 'amountAdjusted' => Input::get('amountAdjusted')])->exists()) {
                    //Delete Deposit Details
                    m_depositdetail::where(['patientID' => Input::get('patientID'), 'claimID' => Input::get('claimID'), 'IsDeleted' => 0, 'amountAdjusted' => Input::get('amountAdjusted')])->update(['IsDeleted' => 1]);

                    //Update Flag in Claims  allowed,paid,adjustment,claimBalance,reason,status,copay,deductible,coins
                    m_claim::where(['claimID' => Input::get('claimID'), 'isDeleted' => 0])->update(['isPosted' => 0, 'allowed' => '', 'paid' => '', 'adjustment' => '', 'claimBalance' => 0, 'reason' => '', 'status' => '', 'copay' => '', 'deductible' => '', 'coins' => '']);

                    Common::WriteLog('Deposits','Deposit Detail Reversed. Deposit ID:' . $did . ' Claim ID:' .$claimID . ' Patient ID:' .$patientID,0,'DepositID',$did);
                    return \Response::json(array('errors' => false, 'message' => 'Deposit Detail Removed Successfully!', 'DepositID' => $did));
                } else {
                    return \Response::json(array('errors' => true, 'message' => 'No Deposit Detail Found!'));
                 }
            }else{
                return \Response::json(array('errors' => true, 'message' => 'Invalid Deposit ID.'));
            }
        }
    }
}
