<?php

namespace App\Http\Controllers;

use App\m_practice;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use Response;
use Illuminate\Support\Facades\Input;
use App\Classes\Common;

class practiceController extends Controller
{
    //
    public function createPractice(Request $request){

        $validator = Validator::make(Input::all(), ['practiceName'=>'required|unique:m_practices,practiceName','phoneMobile'=>'required']);

        // if the validator fails, redirect back to the form
        if ($validator->fails()) {
            return \Response::json(array('errors' => true, 'message' => $validator->messages()));
        }
        else{
            $request->merge( array( 'isActive' => 1) );
            m_practice::create($request->all());
            Common::WriteLog('Practices','Practice Created.' ,0,'Create',0);
            return \Response::json(array('errors' => false, 'message' => 'Practice Created Successfully!'));
        }
    }

    public function list_all()
    {
        $provider = m_practice::where('isActive',1)->get();
        //Common::WriteLog('Practices','Practices Listed.' ,0);
        return $provider;
    }
}
