<?php

namespace App\Http\Controllers;

use App\m_deposit;
use App\m_depositdetail;
use App\m_insurance;
use App\m_patient;
use App\m_practice;
use App\m_detail;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\m_case;
use App\m_claim;
use Illuminate\Support\Facades\DB;
use Validator;
use Response;
use Illuminate\Support\Facades\Input;
use App\Classes\Common;

class claimsController extends Controller
{
    //
    public function loadClaim(Request $request)
    {
        $validator = Validator::make(Input::all(), ['caseID'=>'required']);
        if ($validator->fails()) {
            return \Response::json(array('errors' => true, 'message' => $validator->messages()));
        }
        else {
            $cid = (int)Input::get('caseID');
            if ($cid <= 0) { return \Response::json(array('errors' => true, 'message' => 'Invalid Case ID.')); }

            if (m_case::where(['caseID'=>Input::get('caseID')])->exists()) {
                $claims = m_claim::where(['caseID' => $cid,'isDeleted'=>0])->selectRaw('m_claims.*, (select phyName from m_physicians where physicianID=m_claims.primaryCarePhy) PrimaryCarePhysician, (select depositID from m_depositdetails where m_depositdetails.claimID=m_claims.claimID LIMIT 1) DepositIDIfPosted')->get();
                //Common::WriteLog('Claims','Claim Loaded. Case ID:' . $cid,0);
                return $claims;
            }else{
                return \Response::json(array('errors' => true, 'message' => 'Invalid Case ID.'));
            }
        }
    }

    public function createClaim(Request $request)
    {
        $claimStatus = array('DRAFT','REVIEW','COMPLETE');

        $validator = Validator::make(Input::all(), ['caseID'=>'required','proced'=>'required','fromDt'=>'required','toDt'=>'required','claimStatus'=>'required']);
        if ($validator->fails()) {
            return \Response::json(array('errors' => true, 'message' => $validator->messages()));
        }
        else {
            if(!in_array(Input::get('claimStatus'),$claimStatus)){
                return \Response::json(array('errors' => true, 'message' => 'Invalid ClaimStatus. Should be DRAFT, REVIEW or COMPLETE.'));
            }
            else if (m_case::where(['caseID'=>Input::get('caseID')])->exists()) {
                $request->merge( array( 'isDeleted' => 0) );
                $request->merge( array( 'isPosted' => 0) );
                $request->merge( array( 'isSent' => 0) );
                $claimID = m_claim::create($request->all())->id;
                Common::WriteLog('Claims','Claim Created. Claim ID:' . $claimID,0,'Create',$claimID);
                return \Response::json(array('errors' => false, 'message' => 'Claim Created Successfully!', 'ClaimID' => $claimID));
            }else{
                return \Response::json(array('errors' => true, 'message' => 'Invalid Case ID.'));
            }
        }
    }

    public function saveClaim(Request $request)
    {
        $claimStatus = array('DRAFT','REVIEW','COMPLETE');

        $validator = Validator::make(Input::all(), ['caseID'=>'required','proced'=>'required','fromDt'=>'required','toDt'=>'required','claimStatus'=>'required']);
        if ($validator->fails()) {
            return \Response::json(array('errors' => true, 'message' => $validator->messages()));
        }
        else {
            if(!in_array(Input::get('claimStatus'),$claimStatus)){
                return \Response::json(array('errors' => true, 'message' => 'Invalid ClaimStatus. Should be DRAFT, REVIEW or COMPLETE.'));
            }
            else if (m_case::where(['caseID'=>Input::get('caseID')])->exists()) {
                if (m_claim::where(['claimID'=>Input::get('claimID')])->exists()) {
                    $updt =  m_claim::where('claimID', '=',Input::get('claimID'))->update($request->all());
                    Common::WriteLog('Claims','Claim Edited. Case ID:' . Input::get('caseID') . ' Claim ID:' . Input::get('claimID'),0, 'Edit',Input::get('claimID'));
                    return \Response::json(array('errors' => false, 'message' => 'Claim Saved Successfully!'));
                }else{
                    return \Response::json(array('errors' => true, 'message' => 'Invalid Claim ID.'));
                }
            }else{
                return \Response::json(array('errors' => true, 'message' => 'Invalid Case ID.'));
            }
        }
    }

/*    public function listClaim(Request $request)
    {
        $validator = Validator::make(Input::all(), ['patientID'=>'required']);
        if ($validator->fails()) {
            return \Response::json(array('errors' => true, 'message' => $validator->messages()));
        }
        else {
            $pid = (int)Input::get('patientID');
            if ($pid <= 0) { return \Response::json(array('errors' => true, 'message' => 'Invalid Patient ID.')); }

            if (m_patient::where(['patientID'=>$pid])->exists()) {
                $claims = m_claim::join('m_cases', 'm_claims.caseID', '=', 'm_cases.caseID')
                    ->join('m_patients', 'm_cases.patientID', '=', 'm_patients.patientID')
                    ->where(['m_patients.patientID' => $pid,'m_patients.isDeleted'=>0,'m_cases.isDeleted'=>0,'m_claims.isDeleted'=>0])
                    ->selectRaw('m_claims.*, (select phyName from m_physicians where physicianID=m_claims.primaryCarePhy) PrimaryCarePhysician')
                    ->get();
                //Common::WriteLog('Claims','Claims Loaded. Patient ID:' . $pid,0);
                return $claims;
            }else{
                return \Response::json(array('errors' => true, 'message' => 'Invalid Patient ID.'));
            }
        }
    }*/

    public function listDepositClaim(Request $request)
    {
        $validator = Validator::make(Input::all(), ['patientID'=>'required','fromDt'=>'required','toDt'=>'required']);
        if ($validator->fails()) {
            return \Response::json(array('errors' => true, 'message' => $validator->messages()));
        }
        else {
            $pid = (int)Input::get('patientID');
            if ($pid <= 0) { return \Response::json(array('errors' => true, 'message' => 'Invalid Patient ID.')); }

            if (m_patient::where(['patientID'=>$pid])->exists()) {
                $claims = m_claim::join('m_cases', 'm_claims.caseID', '=', 'm_cases.caseID')
                    ->join('m_patients', 'm_cases.patientID', '=', 'm_patients.patientID')
                    ->where(['m_patients.patientID' => $pid,'m_patients.isDeleted'=>0,'m_cases.isDeleted'=>0,'m_claims.isDeleted'=>0,'m_claims.isSent'=>1])
                    ->whereBetween('m_claims.fromDt',[Input::get('fromDt'), Input::get('toDt')])
                    ->selectRaw('m_claims.isPosted,m_claims.ClaimNumber, m_claims.caseID, m_claims.fromDt, m_claims.toDt, m_claims.claimID, m_claims.proced, m_claims.claimBalance, m_claims.claimBalancePat, m_claims.allowed, m_claims.paid, m_claims.adjustment, m_claims.mod1, sum(m_claims.units) as units, sum(m_claims.total) as total , m_claims.created_At,(select phyName from m_physicians where physicianID=m_claims.primaryCarePhy) PrimaryCarePhysician')
                    ->groupBy(['m_claims.ClaimNumber','m_claims.fromDt','m_claims.toDt','m_claims.proced','m_claims.units','m_claims.total','m_claims.created_At'])
                    ->get();
                //Common::WriteLog('Claims','Claims Loaded. Patient ID:' . $pid,0);
                return $claims;
            }else{
                return \Response::json(array('errors' => true, 'message' => 'Invalid Patient ID.'));
            }
        }
    }

    public function listClaim(Request $request)
    {
        $validator = Validator::make(Input::all(), ['patientID'=>'required']);
        if ($validator->fails()) {
            return \Response::json(array('errors' => true, 'message' => $validator->messages()));
        }
        else {
            $pid = (int)Input::get('patientID');
            if ($pid <= 0) { return \Response::json(array('errors' => true, 'message' => 'Invalid Patient ID.')); }

            if (m_patient::where(['patientID'=>$pid])->exists()) {
                $claims = m_claim::join('m_cases', 'm_claims.caseID', '=', 'm_cases.caseID')
                    ->join('m_patients', 'm_cases.patientID', '=', 'm_patients.patientID')
                    ->where(['m_patients.patientID' => $pid,'m_patients.isDeleted'=>0,'m_cases.isDeleted'=>0,'m_claims.isSent'=>1])
                    ->selectRaw('m_claims.isPosted,m_claims.ClaimNumber, m_claims.fromDt, m_claims.toDt, m_claims.claimID, m_claims.proced, m_claims.claimBalance, m_claims.claimBalancePat, m_claims.allowed, m_claims.paid, m_claims.adjustment, m_claims.mod1, sum(m_claims.units) as units, sum(m_claims.total) as total , m_claims.created_At,(select phyName from m_physicians where physicianID=m_claims.primaryCarePhy) PrimaryCarePhysician')
                    ->groupBy(['m_claims.ClaimNumber','m_claims.fromDt','m_claims.toDt','m_claims.proced','m_claims.units','m_claims.total','m_claims.created_At'])
                    ->get();
                //Common::WriteLog('Claims','Claims Loaded. Patient ID:' . $pid,0);
                return $claims;
            }else{
                return \Response::json(array('errors' => true, 'message' => 'Invalid Patient ID.'));
            }
        }
    }

    public function listPatientClaim(Request $request)
    {
        $validator = Validator::make(Input::all(), ['patientID'=>'required']);
        if ($validator->fails()) {
            return \Response::json(array('errors' => true, 'message' => $validator->messages()));
        }
        else {
            $pid = (int)Input::get('patientID');
            if ($pid <= 0) { return \Response::json(array('errors' => true, 'message' => 'Invalid Patient ID.')); }

            if (m_patient::where(['patientID'=>$pid])->exists()) {
                $claims = m_claim::join('m_cases', 'm_claims.caseID', '=', 'm_cases.caseID')
                    ->join('m_patients', 'm_cases.patientID', '=', 'm_patients.patientID')
                    ->where(['m_patients.patientID' => $pid, 'm_patients.isDeleted'=>0, 'm_cases.isDeleted'=>0, 'm_claims.isSent'=>1])
                    ->selectRaw('m_claims.ClaimNumber, m_claims.caseID, m_claims.fromDt, m_claims.toDt, m_claims.claimID, m_claims.proced, m_claims.isPosted, m_claims.status, m_claims.claimBalance, m_claims.claimBalancePat, m_claims.allowed, m_claims.copay, m_claims.coins, m_claims.deductible, m_claims.paid, m_claims.adjustment, m_claims.mod1, sum(m_claims.units) as units, sum(m_claims.total) as total , m_claims.created_At,(select phyName from m_physicians where physicianID=m_claims.primaryCarePhy) PrimaryCarePhysician')
                    ->groupBy(['m_claims.ClaimNumber','m_claims.fromDt','m_claims.toDt','m_claims.proced','m_claims.units','m_claims.total','m_claims.created_At'])
                    ->get();
                //Common::WriteLog('Claims','Claims Loaded. Patient ID:' . $pid,0);
                return $claims;
            }else{
                return \Response::json(array('errors' => true, 'message' => 'Invalid Patient ID.'));
            }
        }
    }


    public function listClaimByNumber(Request $request)
    {
        $validator = Validator::make(Input::all(), ['ClaimNumber'=>'required']);
        if ($validator->fails()) {
            return \Response::json(array('errors' => true, 'message' => $validator->messages()));
        }
        else {
            $cid = Input::get('ClaimNumber');
            if ($cid <= 0) { return \Response::json(array('errors' => true, 'message' => 'Invalid Claim Number.')); }

            if (m_claim::where(['ClaimNumber'=>$cid])->exists()) {
                $claims = m_claim::join('m_cases', 'm_claims.caseID', '=', 'm_cases.caseID')
                    ->join('m_patients', 'm_cases.patientID', '=', 'm_patients.patientID')
                    ->where(['m_claims.ClaimNumber' => $cid,'m_patients.isDeleted'=>0,'m_cases.isDeleted'=>0,'m_claims.isDeleted'=>0])
                    ->selectRaw('m_claims.ClaimNumber, m_claims.fromDt, m_claims.toDt, m_claims.claimID, m_claims.proced, m_claims.claimBalance, m_claims.claimBalancePat, m_claims.allowed, m_claims.paid, m_claims.adjustment, m_claims.mod1, m_claims.units, m_claims.total , m_claims.created_At,(select phyName from m_physicians where physicianID=m_claims.primaryCarePhy) PrimaryCarePhysician,(select payerName from m_insurances where insuranceID=m_claims.insuranceID) insuranceName,(select fullName from m_patients where patientID=m_cases.patientID) fullName')
                    ->get();
                //Common::WriteLog('Claims','Claims Loaded. Patient ID:' . $pid,0);
                return $claims;
            }else{
                return \Response::json(array('errors' => true, 'message' => 'Invalid Claim Number.'));
            }
        }
    }


    public function listAllClaims(Request $request)
    {
        $validator = Validator::make(Input::all(), ['practiceID'=>'required']);
        if ($validator->fails()) {
            return \Response::json(array('errors' => true, 'message' => $validator->messages()));
        }
        else {
            $pid = (int)Input::get('practiceID');
            if ($pid <= 0) { return \Response::json(array('errors' => true, 'message' => 'Invalid Practice ID.')); }

            if (m_practice::where(['practiceID'=>$pid])->exists()) {
                $claims = m_claim::join('m_cases', 'm_claims.caseID', '=', 'm_cases.caseID')
                    ->join('m_patients', 'm_cases.patientID', '=', 'm_patients.patientID')
                    ->where(['m_patients.practiceID' => $pid,'m_patients.isDeleted'=>0,'m_cases.isDeleted'=>0,'m_claims.isDeleted'=>0])
                    ->selectRaw('m_claims.*, (select phyName from m_physicians where physicianID=m_claims.primaryCarePhy) PrimaryCarePhysician')
                    ->get();
                return $claims;
            }else{
                return \Response::json(array('errors' => true, 'message' => 'Invalid Practice ID.'));
            }
        }
    }

    public function listByDeposit(Request $request)
    {
        $validator = Validator::make(Input::all(), ['depositID'=>'required']);
        if ($validator->fails()) {
            return \Response::json(array('errors' => true, 'message' => $validator->messages()));
        }
        else {
            $did = (int)Input::get('depositID');
            if ($did <= 0) { return \Response::json(array('errors' => true, 'message' => 'Invalid Deposit ID.')); }

            if (m_deposit::where(['depositID'=>$did])->exists()) {
                $claims = m_claim::join('m_cases', 'm_claims.caseID', '=', 'm_cases.caseID')
                    ->join('m_patients', 'm_cases.patientID', '=', 'm_patients.patientID')
                    ->join('m_depositdetails', 'm_depositdetails.claimID', '=', 'm_claims.claimID')
                    ->where(['m_depositdetails.depositID' => $did,'m_patients.isDeleted'=>0,'m_cases.isDeleted'=>0,'m_claims.isDeleted'=>0,'m_depositdetails.isDeleted'=>0,'m_claims.claimStatus'=>'Submit'])
                    ->selectRaw('m_claims.*,m_depositdetails.*, (select phyName from m_physicians where physicianID=m_claims.primaryCarePhy) PrimaryCarePhysician, (select fullName from m_patients where patientID=m_cases.patientID) fullName, (select payerName from m_insurances where insuranceID=m_claims.insuranceID) insuranceName')
                    ->get();
                //Common::WriteLog('Claims','Listed by Deposit. Deposit ID:' . $did,0);
                return $claims;
            }else{
                return \Response::json(array('errors' => true, 'message' => 'Invalid Deposit ID.'));
            }
        }
    }

    public function ledgerPatientOnly(Request $request)
    {
        $validator = Validator::make(Input::all(), ['patientID'=>'required']);
        if ($validator->fails()) {
            return \Response::json(array('errors' => true, 'message' => $validator->messages()));
        }
        else {
            $pid = (int)Input::get('patientID');
            if ($pid <= 0) { return \Response::json(array('errors' => true, 'message' => 'Invalid Patient ID.')); }

            if (m_patient::where(['patientID'=>$pid])->exists()) {
                $claims = m_claim::join('m_cases', 'm_claims.caseID', '=', 'm_cases.caseID')
                    ->join('m_patients', 'm_cases.patientID', '=', 'm_patients.patientID')
                    ->where(['m_patients.patientID' => $pid,'m_patients.isDeleted'=>0,'m_cases.isDeleted'=>0,'m_claims.isDeleted'=>0,'m_claims.insuranceID' => -100,'m_claims.ClaimStatus' => 'COMPLETE'])
                    ->selectRaw('m_claims.*,m_patients.fullName,(select phyName from m_physicians where physicianID=m_claims.primaryCarePhy) PrimaryCarePhysician')
                    ->get();
                //Common::WriteLog('Claims','Loaded Ledgers by Patient. Patient ID:' . $pid,0);
                return $claims;
            }else{
                return \Response::json(array('errors' => true, 'message' => 'Invalid Patient ID.'));
            }
        }
    }

    public function ledgerInsuranceOnly(Request $request)
    {
        $validator = Validator::make(Input::all(), ['insuranceID'=>'required']);
        if ($validator->fails()) {
            return \Response::json(array('errors' => true, 'message' => $validator->messages()));
        }
        else {
            $iid = (int)Input::get('insuranceID');
            if ($iid <= 0) { return \Response::json(array('errors' => true, 'message' => 'Invalid Insurance ID.')); }

            if (m_insurance::where(['insuranceID'=>$iid])->exists()) {
                $claims = m_claim::join('m_cases', 'm_claims.caseID', '=', 'm_cases.caseID')
                    ->join('m_patients', 'm_cases.patientID', '=', 'm_patients.patientID')
                    ->where(['m_claims.insuranceID' => $iid,'m_patients.isDeleted'=>0,'m_cases.isDeleted'=>0,'m_claims.isDeleted'=>0,'m_claims.ClaimStatus' => 'COMPLETE'])
                    ->selectRaw('m_claims.*,(select payerName from m_insurances where m_insurances.insuranceID=m_claims.insuranceID) InsuranceName,(select phyName from m_physicians where physicianID=m_claims.primaryCarePhy) PrimaryCarePhysician')
                    ->get();
                //Common::WriteLog('Claims','Loaded Ledgers by Insurance. Insurance ID:' . $iid,0);
                return $claims;
            }else{
                return \Response::json(array('errors' => true, 'message' => 'Invalid Insurance ID.'));
            }
        }
    }

    public function loadLocations(Request $request){
        $loc = m_claim::select('placeOfService')->groupBy('placeOfService')->get();
        //Common::WriteLog('Claims','Places of Service Loaded.',0);
        return $loc;
    }


    public function generate(Request $request)
    {
        $validator = Validator::make(Input::all(), ['patientID' => 'required', 'SentBatchNumber' => 'required']);
        if ($validator->fails()) {
            return \Response::json(array('errors' => true, 'message' => $validator->messages()));
        } else {
            $pid = (int)Input::get('patientID');
            if ($pid <= 0) {
                return \Response::json(array('errors' => true, 'message' => 'Invalid Patient ID.'));
            }

            if (m_patient::where(['patientID' => $pid])->exists()) {
                //$claimsList = m_claim::join('m_cases', 'm_claims.caseID', '=', 'm_cases.caseID')
                //    ->join('m_patients', 'm_cases.patientID', '=', 'm_patients.patientID')
                //    ->where(['m_patients.patientID' => $pid,'m_patients.isDeleted'=>0,'m_cases.isDeleted'=>0,'m_claims.isDeleted'=>0,'m_claims.isSent'=>0])
                //    ->get();

                $id1 = (int)Input::get('SentBatchNumber');

/*                $claimsList = m_patient::with('cases')->with('cases.unsentclaims')->with('cases.policies')
                    ->where(['m_patients.patientID' => $pid, 'm_patients.isDeleted' => 0])
                    ->get();*/
                $claimsList = m_patient::with('cases')->with('cases.groupedclaimsnumbers')->with('cases.unsentclaims')->with('cases.policies')
                    ->where(['m_patients.patientID' => $pid, 'm_patients.isDeleted' => 0])
                    ->get();

                $claims = m_claim::join('m_cases', 'm_claims.caseID', '=', 'm_cases.caseID')
                    ->join('m_patients', 'm_cases.patientID', '=', 'm_patients.patientID')
                    ->where(['m_patients.patientID' => $pid, 'm_patients.isDeleted' => 0, 'm_cases.isDeleted' => 0, 'm_claims.isDeleted' => 0, 'm_claims.isSent' => 0])
                    ->select('m_claims.claimID')
                    ->get();
                $claims1 = m_claim::whereIn('claimID', $claims)
                    ->update(['isSent' => 1,'SentBatchNumber'=>$id1]);

                Common::WriteLog('Claims','Claims Generated. Patient ID:' . $pid,0,'Generate',$pid);
                return $claimsList;
                //return \Response::json(array('errors' => false, 'message' => 'Generated Successfully.'));
            } else {
                return \Response::json(array('errors' => true, 'message' => 'Invalid Patient ID.'));
            }
        }
    }


        public function generate_all(Request $request){
            $validator = Validator::make(Input::all(), ['practiceID'=>'required','SentBatchNumber'=>'required']);
            if ($validator->fails()) {
                return \Response::json(array('errors' => true, 'message' => $validator->messages()));
            }
            else {
                $pid = (int)Input::get('practiceID');
                if ($pid <= 0) { return \Response::json(array('errors' => true, 'message' => 'Invalid Practice ID.')); }

                if (m_practice::where(['practiceID'=>$pid])->exists()) {
                    //$claimsList = m_claim::join('m_cases', 'm_claims.caseID', '=', 'm_cases.caseID')
                    //    ->join('m_patients', 'm_cases.patientID', '=', 'm_patients.patientID')
                    //    ->where(['m_patients.patientID' => $pid,'m_patients.isDeleted'=>0,'m_cases.isDeleted'=>0,'m_claims.isDeleted'=>0,'m_claims.isSent'=>0])
                    //    ->get();

                    //m_detail::where(['detail'=>'BatchNumber'])->increment('detailValue1');
                    //$id1 = m_detail::where(['detail'=>'BatchNumber'])->get(['detailValue1'])[0]['detailValue1'];
                    $id1 = (int)Input::get('SentBatchNumber');

                    $claims = m_claim::join('m_cases', 'm_claims.caseID', '=', 'm_cases.caseID')
                        ->join('m_patients', 'm_cases.patientID', '=', 'm_patients.patientID')
                        ->where(['m_patients.practiceID' => $pid,'m_patients.isDeleted'=>0,'m_cases.isDeleted'=>0,'m_claims.isDeleted'=>0,'m_claims.isSent'=>0])
                        ->select('m_claims.claimID')
                        ->get();

                    $claims1 = m_claim::whereIn('claimID',$claims)
                        ->update(['isSent'=>1,'SentBatchNumber'=>$id1]);

/*                    $claimsList = m_patient::with('cases')->with('cases.unsentclaims')->with('cases.policies')
                        ->where(['m_patients.practiceID' => $pid,'m_patients.isDeleted'=>0])
                        ->get();*/

                    $claimsList = m_patient::with('cases')->with('cases.groupedclaimsnumbers')->with('cases.unsentclaims')->with('cases.policies')
                        ->where(['m_patients.patientID' => $pid, 'm_patients.isDeleted' => 0])
                        ->get();

                  /*  $claimsList = m_claim::join('m_cases', 'm_claims.caseID', '=', 'm_cases.caseID')
                        ->join('m_patients', 'm_cases.patientID', '=', 'm_patients.patientID')
                        ->join('m_policies', 'm_policies.caseID', '=', 'm_cases.caseID')
                        ->where(['SentBatchNumber'=>$id1,'m_patients.practiceID'=>$pid,'m_patients.isDeleted'=>0,'m_cases.isDeleted'=>0])
                        ->selectRaw('*, (select phyName from m_physicians where physicianID=m_claims.primaryCarePhy) PrimaryCarePhysician');
*/

                    Common::WriteLog('Claims','All unsent Claims Generated. Practice ID:' . $pid,0,'GenerateAll',$pid);

                    return $claimsList;
                    //return \Response::json(array('errors' => false, 'message' => 'Generated Successfully.'));
                }else{
                    return \Response::json(array('errors' => true, 'message' => 'Invalid Practice ID.'));
                }
            }
    }


    public function generatePaper(Request $request)
    {
        $validator = Validator::make(Input::all(), ['patientID' => 'required']);
        if ($validator->fails()) {
            return \Response::json(array('errors' => true, 'message' => $validator->messages()));
        } else {
            $pid = (int)Input::get('patientID');
            if ($pid <= 0) {
                return \Response::json(array('errors' => true, 'message' => 'Invalid Patient ID.'));
            }

            if (m_patient::where(['patientID' => $pid])->exists()) {
                //$claimsList = m_claim::join('m_cases', 'm_claims.caseID', '=', 'm_cases.caseID')
                //    ->join('m_patients', 'm_cases.patientID', '=', 'm_patients.patientID')
                //    ->where(['m_patients.patientID' => $pid,'m_patients.isDeleted'=>0,'m_cases.isDeleted'=>0,'m_claims.isDeleted'=>0,'m_claims.isSent'=>0])
                //    ->get();

                $claimsList = m_patient::with('cases')->with('cases.groupedclaimsnumbers')->with('cases.allclaims')->with('cases.policies')
                    ->where(['m_patients.patientID' => $pid, 'm_patients.isDeleted' => 0])
                    ->get();

               /* $claimsList = m_patient::with('cases')->with(['cases.allclaims' => function($query){
                  $query->selectRaw('m_claims.claimID, m_claims.caseID, m_claims.ClaimNumber')->groupBy('m_claims.ClaimNumber')
                      ->get();
                }])->with('cases.policies')
                    ->where(['m_patients.patientID' => $pid, 'm_patients.isDeleted' => 0])
                    ->get();*/

                Common::WriteLog('Claims','Claim Papers Generated. Patient ID:' . $pid,0,'Generate',$pid);
                return $claimsList;
                //return \Response::json(array('errors' => false, 'message' => 'Generated Successfully.'));
            } else {
                return \Response::json(array('errors' => true, 'message' => 'Invalid Patient ID.'));
            }
        }
    }


/*    public function generatePaper(Request $request)
    {
        $validator = Validator::make(Input::all(), ['patientID' => 'required']);
        if ($validator->fails()) {
            return \Response::json(array('errors' => true, 'message' => $validator->messages()));
        } else {
            $pid = (int)Input::get('patientID');
            if ($pid <= 0) {
                return \Response::json(array('errors' => true, 'message' => 'Invalid Patient ID.'));
            }

            if (m_patient::where(['patientID' => $pid])->exists()) {
                //$claimsList = m_claim::join('m_cases', 'm_claims.caseID', '=', 'm_cases.caseID')
                //    ->join('m_patients', 'm_cases.patientID', '=', 'm_patients.patientID')
                //    ->where(['m_patients.patientID' => $pid,'m_patients.isDeleted'=>0,'m_cases.isDeleted'=>0,'m_claims.isDeleted'=>0,'m_claims.isSent'=>0])
                //    ->get();

                $claimsList = m_patient::with('cases')->with('cases.allclaims')->with('cases.policies')
                    ->where(['m_patients.patientID' => $pid, 'm_patients.isDeleted' => 0])
                    ->get();

                Common::WriteLog('Claims','Claim Papers Generated. Patient ID:' . $pid,0,'Generate',$pid);
                return $claimsList;
                //return \Response::json(array('errors' => false, 'message' => 'Generated Successfully.'));
            } else {
                return \Response::json(array('errors' => true, 'message' => 'Invalid Patient ID.'));
            }
        }
    }*/

    public function removeClaim(Request $request)
    {
        $validator = Validator::make(Input::all(), ['claimID'=>'required']);
        if ($validator->fails()) {
            return \Response::json(array('errors' => true, 'message' => $validator->messages()));
        }
        else {
            $claimid = (int)Input::get('claimID');
            if ($claimid <= 0) { return \Response::json(array('errors' => true, 'message' => 'Invalid Claim ID.')); }

            if (m_claim::where(['claimID'=>$claimid,'isDeleted'=>0])->exists()) {

                m_claim::where(['claimID' => $claimid, 'isDeleted' => 0])->update(['isDeleted' => 1]);
                m_depositdetail::where(['claimID' => $claimid])->update(['isDeleted' => 1]);

                Common::WriteLog('Claims','Claim Removed and Deposit Details Removed. Claim ID:' . $claimid,0,'Remove',$claimid);
                return \Response::json(array('errors' => false, 'message' => 'Claim Removed Successfully.'));
            }else{
                return \Response::json(array('errors' => true, 'message' => 'Invalid Claim ID.'));
            }
        }
    }

    public function undoRemoveClaim(Request $request)
    {
        $validator = Validator::make(Input::all(), ['claimID'=>'required']);
        if ($validator->fails()) {
            return \Response::json(array('errors' => true, 'message' => $validator->messages()));
        }
        else {
            $claimid = (int)Input::get('claimID');
            if ($claimid <= 0) { return \Response::json(array('errors' => true, 'message' => 'Invalid Claim ID.')); }

            if (m_claim::where(['claimID'=>$claimid,'isDeleted'=>1])->exists()) {

                m_claim::where(['claimID' => $claimid, 'isDeleted' => 1])->update(['isDeleted' => 0]);
                m_depositdetail::where(['claimID' => $claimid])->update(['isDeleted' => 0]);

                Common::WriteLog('Claims','Removed Claim Reversed Back and Deposit Details also Enabled. Claim ID:' . $claimid,0,'UndoRemove',$claimid);
                return \Response::json(array('errors' => false, 'message' => 'Claim Removed Successfully.'));
            }else{
                return \Response::json(array('errors' => true, 'message' => 'Invalid Claim ID.'));
            }
        }
    }





    public function chargesSummaryReport(Request $request){
        $validator = Validator::make(Input::all(), ['FromDate'=>'required', 'ToDate'=>'required']);
        if ($validator->fails()) {
            return \Response::json(array('errors' => true, 'message' => $validator->messages()));
        }
        else{
            $insurances = m_insurance::where(['m_insurances.isActive' => 1])
                        ->select(['m_insurances.*'])
                        ->selectRaw('(select sum(charge) from m_claims inner join m_cases on m_cases.caseID=m_claims.caseID where m_cases.assignedProvider=m_insurances.insuranceID and m_claims.isDeleted=0 and m_cases.isDeleted=0 and m_claims.created_At>=\'' . Input::get('FromDate') . '\' and m_claims.created_At<=\'' . Input::get('ToDate') . '\' and m_claims.claimStatus=\'COMPLETE\') as TotalChargesComplete
                        ,(select sum(charge) from m_claims inner join m_cases on m_cases.caseID=m_claims.caseID where m_cases.assignedProvider=m_insurances.insuranceID and m_claims.isDeleted=0 and m_cases.isDeleted=0 and m_claims.created_At>=\'' . Input::get('FromDate') . '\' and m_claims.created_At<=\'' . Input::get('ToDate') . '\' and m_claims.claimStatus=\'REVIEW\') as TotalChargesReview
                        ,(select sum(charge) from m_claims inner join m_cases on m_cases.caseID=m_claims.caseID where m_cases.assignedProvider=m_insurances.insuranceID and m_claims.isDeleted=0 and m_cases.isDeleted=0 and m_claims.created_At>=\'' . Input::get('FromDate') . '\' and m_claims.created_At<=\'' . Input::get('ToDate') . '\' and m_claims.claimStatus=\'DRAFT\') as TotalChargesDraft
                        ,(select count(charge) from m_claims inner join m_cases on m_cases.caseID=m_claims.caseID where m_cases.assignedProvider=m_insurances.insuranceID and m_claims.isDeleted=0 and m_cases.isDeleted=0 and m_claims.created_At>=\'' . Input::get('FromDate') . '\' and m_claims.created_At<=\'' . Input::get('ToDate') . '\' and m_claims.claimStatus=\'COMPLETE\') as CountofChargesComplete
                        ,(select count(charge) from m_claims inner join m_cases on m_cases.caseID=m_claims.caseID where m_cases.assignedProvider=m_insurances.insuranceID and m_claims.isDeleted=0 and m_cases.isDeleted=0 and m_claims.created_At>=\'' . Input::get('FromDate') . '\' and m_claims.created_At<=\'' . Input::get('ToDate') . '\' and m_claims.claimStatus=\'REVIEW\') as CountofChargesReview
                        ,(select count(charge) from m_claims inner join m_cases on m_cases.caseID=m_claims.caseID where m_cases.assignedProvider=m_insurances.insuranceID and m_claims.isDeleted=0 and m_cases.isDeleted=0 and m_claims.created_At>=\'' . Input::get('FromDate') . '\' and m_claims.created_At<=\'' . Input::get('ToDate') . '\' and m_claims.claimStatus=\'DRAFT\') as CountofChargesDraft')
                        ->get();
            return $insurances;
        }
    }

    public function settledChargesReport(Request $request){
        $validator = Validator::make(Input::all(), ['FromDate'=>'required', 'ToDate'=>'required']);
        if ($validator->fails()) {
            return \Response::json(array('errors' => true, 'message' => $validator->messages()));
        }
        else{
            $practices = m_practice::where(['m_practices.isActive' => 1])
                ->select(['m_practices.*'])
                ->selectRaw('(select sum(amountAdjusted) from m_depositdetails inner join m_deposits on m_deposits.depositID=m_depositdetails.depositID where m_depositdetails.isDeleted=0 and m_deposits.isDeleted=0 and m_deposits.practiceID=m_practices.practiceID and m_depositdetails.created_At>=\'' . Input::get('FromDate') . '\' and m_depositdetails.created_At<=\'' . Input::get('ToDate') . '\' and m_depositdetails.ClaimID>0) as PayorsPaid
                            ,(select sum(amountAdjusted) from m_depositdetails inner join m_deposits on m_deposits.depositID=m_depositdetails.depositID where m_depositdetails.isDeleted=0 and m_deposits.isDeleted=0 and m_deposits.practiceID=m_practices.practiceID and m_depositdetails.created_At>=\'' . Input::get('FromDate') . '\' and m_depositdetails.created_At<=\'' . Input::get('ToDate') . '\' and m_depositdetails.PatientID>0) as PatientsPaid')
                ->get();
            return $practices;
        }
    }
/*
    public function arAgingReport(Request $request){
        $validator = Validator::make(Input::all(), ['AsofDate'=>'required']);
        if ($validator->fails()) {
            return \Response::json(array('errors' => true, 'message' => $validator->messages()));
        }
        else{
            $claims = m_claim::where(['m_claims.isDeleted' => 0])
                ->join('m_cases','m_cases.caseID','=','m_claims.caseID')
                ->join('m_patients','m_patients.patientID','=','m_cases.patientID')
                ->where(['m_cases.isDeleted'=>0])
                ->where('m_claims.claimBalance','>','0')
                ->where('m_claims.created_At','<=',Input::get('AsofDate'))
                ->select(['m_claims.*', 'm_cases.*', 'm_patients.*'])
                ->get();
            return $claims;
        }
    }
*/
    public function arAgingDetailReport(Request $request){
        $validator = Validator::make(Input::all(), ['AsofDate'=>'required']);
        if ($validator->fails()) {
            return \Response::json(array('errors' => true, 'message' => $validator->messages()));
        }
        else{
            $claims = m_claim::where(['m_claims.isDeleted' => 0])
                ->join('m_cases','m_cases.caseID','=','m_claims.caseID')
                ->join('m_patients','m_patients.patientID','=','m_cases.patientID')
                ->join('m_practices','m_practices.practiceID','=','m_patients.patientID')
                ->where(['m_cases.isDeleted'=>0])
                ->where('m_claims.claimBalance','>','0')
                ->where('m_claims.created_At','<=',Input::get('AsofDate'))
                ->select(['m_claims.*', 'm_cases.*', 'm_patients.*'])
                ->get();
            return $claims;
        }
    }

    public function unpaidClaimsReport(Request $request){
        $validator = Validator::make(Input::all(), ['AsofDate'=>'required']);
        if ($validator->fails()) {
            return \Response::json(array('errors' => true, 'message' => $validator->messages()));
        }
        else{
            $claims = m_claim::where(['m_claims.isDeleted' => 0])
                ->join('m_cases','m_cases.caseID','=','m_claims.caseID')
                ->join('m_patients','m_patients.patientID','=','m_cases.patientID')
                ->join('m_practices','m_practices.practiceID','=','m_patients.patientID')
                ->where(['m_cases.isDeleted'=>0])
                ->where('m_claims.paid','=','0')
                ->where('m_claims.created_At','<=',Input::get('AsofDate'))
                ->select(['m_claims.*', 'm_cases.*', 'm_patients.*'])
                ->get();
            return $claims;
        }
    }


    public function arAgingReport(Request $request){
        $validator = Validator::make(Input::all(), ['AsofDate'=>'required']);
        if ($validator->fails()) {
            return \Response::json(array('errors' => true, 'message' => $validator->messages()));
        }
        else{
            //Date must be in 2016-09-01 Format
            $dateStart = (new \DateTime(Input::get('AsofDate')))->modify('-30 day')->format('Y-m-d');
            $dateEnd = (new \DateTime(Input::get('AsofDate')))->format('Y-m-d');
            $rsName = 'Current';
            $insCurrent = $this->AgingQuery($dateStart, $dateEnd, true, true, $rsName);
            //$insCurrentVal = intval($insCurrent[$rsName]);

            $dateStart = (new \DateTime(Input::get('AsofDate')))->modify('-60 day')->format('Y-m-d');
            $dateEnd = (new \DateTime(Input::get('AsofDate')))->modify('-31 day')->format('Y-m-d');
            $rsName = '31_60';
            $ins31 = $this->AgingQuery($dateStart, $dateEnd, true, true, $rsName);
            //$ins31Val = intval($ins31[$rsName]);

            $dateStart = (new \DateTime(Input::get('AsofDate')))->modify('-90 day')->format('Y-m-d');
            $dateEnd = (new \DateTime(Input::get('AsofDate')))->modify('-61 day')->format('Y-m-d');
            $rsName = '61_90';
            $ins61 = $this->AgingQuery($dateStart, $dateEnd, true, true, $rsName);
            //$ins61Val = intval($ins61[$rsName]);

            $dateStart = (new \DateTime(Input::get('AsofDate')))->modify('-120 day')->format('Y-m-d');
            $dateEnd = (new \DateTime(Input::get('AsofDate')))->modify('-91 day')->format('Y-m-d');
            $rsName = '91_120';
            $ins91 = $this->AgingQuery($dateStart, $dateEnd, true, true, $rsName);
            //$ins91Val = intval($ins91[$rsName]);

            $dateStart = (new \DateTime('2000-01-01'))->format('Y-m-d');
            $dateEnd = (new \DateTime(Input::get('AsofDate')))->modify('-121 day')->format('Y-m-d');
            $rsName = '121_above';
            $ins121 = $this->AgingQuery($dateStart, $dateEnd, true, true, $rsName);
            //$ins121Val = intval($ins121[$rsName]);

            $dateStart = (new \DateTime('2000-01-01'))->format('Y-m-d');
            $dateEnd = (new \DateTime(Input::get('AsofDate')))->format('Y-m-d');
            $rsName = 'Unbilled';
            $insUnbill = $this->AgingQuery($dateStart, $dateEnd, true, false, $rsName);
            //$insUnbillVal = intval($ins121[$rsName]);

            $iResult = array_merge($insCurrent->toArray(),$ins31->toArray(),$ins61->toArray(),$ins91->toArray(),$ins121->toArray(),$insUnbill->toArray());





            $dateStart = (new \DateTime(Input::get('AsofDate')))->modify('-30 day')->format('Y-m-d');
            $dateEnd = (new \DateTime(Input::get('AsofDate')))->format('Y-m-d');
            $rsName = 'Current';
            $pCurrent = $this->AgingQuery($dateStart, $dateEnd, false, true, $rsName);
            //$insCurrentVal = intval($insCurrent[$rsName]);

            $dateStart = (new \DateTime(Input::get('AsofDate')))->modify('-60 day')->format('Y-m-d');
            $dateEnd = (new \DateTime(Input::get('AsofDate')))->modify('-31 day')->format('Y-m-d');
            $rsName = '31_60';
            $p31 = $this->AgingQuery($dateStart, $dateEnd, false, true, $rsName);
            //$ins31Val = intval($ins31[$rsName]);

            $dateStart = (new \DateTime(Input::get('AsofDate')))->modify('-90 day')->format('Y-m-d');
            $dateEnd = (new \DateTime(Input::get('AsofDate')))->modify('-61 day')->format('Y-m-d');
            $rsName = '61_90';
            $p61 = $this->AgingQuery($dateStart, $dateEnd, false, true, $rsName);
            //$ins61Val = intval($ins61[$rsName]);

            $dateStart = (new \DateTime(Input::get('AsofDate')))->modify('-120 day')->format('Y-m-d');
            $dateEnd = (new \DateTime(Input::get('AsofDate')))->modify('-91 day')->format('Y-m-d');
            $rsName = '91_120';
            $p91 = $this->AgingQuery($dateStart, $dateEnd, false, true, $rsName);
            //$ins91Val = intval($ins91[$rsName]);

            $dateStart = (new \DateTime('2000-01-01'))->format('Y-m-d');
            $dateEnd = (new \DateTime(Input::get('AsofDate')))->modify('-121 day')->format('Y-m-d');
            $rsName = '121_above';
            $p121 = $this->AgingQuery($dateStart, $dateEnd, false, true, $rsName);
            //$ins121Val = intval($ins121[$rsName]);

            $dateStart = (new \DateTime('2000-01-01'))->format('Y-m-d');
            $dateEnd = (new \DateTime(Input::get('AsofDate')))->format('Y-m-d');
            $rsName = 'Unbilled';
            $pUnbill = $this->AgingQuery($dateStart, $dateEnd, false, false, $rsName);
            //$insUnbillVal = intval($ins121[$rsName]);

            $pResult = array_merge($pCurrent->toArray(),$p31->toArray(),$p61->toArray(),$p91->toArray(),$p121->toArray(),$pUnbill->toArray());

            return array(array('Insurance' =>$iResult),array('Patient' =>$pResult));
        }
    }

    public function AgingQuery($DateStart, $DateEnd, $IsInsurance, $IsBilled, $ResultName)
    {
        $rs = m_claim::where(['m_claims.isDeleted' => 0])
            ->join('m_cases','m_cases.caseID','=','m_claims.caseID')
            ->join('m_patients','m_patients.patientID','=','m_cases.patientID')
            ->where(['m_cases.isDeleted'=>0]);
        if($IsInsurance == true){
            $rs = $rs->where('m_claims.InsuranceID','!=',-100);
        }else{
            $rs = $rs->where('m_claims.InsuranceID','=',-100);
        }
        if($IsBilled == true){
            $rs = $rs->where('m_claims.ClaimStatus','=','COMPLETE');
        }else{
            $rs = $rs->where('m_claims.ClaimStatus','!=','COMPLETE');
        }
        $rs = $rs->where('m_claims.created_At','>=',$DateStart)
            ->where('m_claims.created_At','<=',$DateEnd)
            ->selectRaw('sum(claimBalance) as ' . $ResultName)->get()[0];
        return $rs;
    }

    public function ledgerSummaryReport(Request $request){
        $validator = Validator::make(Input::all(), ['AsofDate'=>'required','PracticeID'=>'required']);
        if ($validator->fails()) {
            return \Response::json(array('errors' => true, 'message' => $validator->messages()));
        }
        else {
            $FilterPatientID = -500;
            $FilterInsuranceID = -500;

            if (Input::has('patientID')){ $FilterPatientID = Input::get('PracticeID'); }
            if (Input::has('insuranceID')){ $FilterInsuranceID = Input::get('insuranceID'); }

            $practiceID = Input::get('PracticeID');
            $dateStart = (new \DateTime(Input::get('AsofDate')))->modify('-30 day')->format('Y-m-d');
            $dateEnd = (new \DateTime(Input::get('AsofDate')))->format('Y-m-d');
            $rsName = '0_30';
            $i30 = $this->ClaimLedgerQuery($dateStart, $dateEnd, true, $practiceID, $rsName, $FilterPatientID, $FilterInsuranceID);


            $dateStart = (new \DateTime(Input::get('AsofDate')))->modify('-60 day')->format('Y-m-d');
            $dateEnd = (new \DateTime(Input::get('AsofDate')))->modify('-31 day')->format('Y-m-d');
            $rsName = '31_60';
            $i31 = $this->ClaimLedgerQuery($dateStart, $dateEnd, true, $practiceID, $rsName, $FilterPatientID, $FilterInsuranceID);


            $dateStart = (new \DateTime(Input::get('AsofDate')))->modify('-90 day')->format('Y-m-d');
            $dateEnd = (new \DateTime(Input::get('AsofDate')))->modify('-61 day')->format('Y-m-d');
            $rsName = '61_90';
            $i61 = $this->ClaimLedgerQuery($dateStart, $dateEnd, true, $practiceID, $rsName, $FilterPatientID, $FilterInsuranceID);


            $dateStart = (new \DateTime(Input::get('AsofDate')))->modify('-120 day')->format('Y-m-d');
            $dateEnd = (new \DateTime(Input::get('AsofDate')))->modify('-91 day')->format('Y-m-d');
            $rsName = '91_120';
            $i91 = $this->ClaimLedgerQuery($dateStart, $dateEnd, true, $practiceID, $rsName, $FilterPatientID, $FilterInsuranceID);

            $dateStart = (new \DateTime('2000-01-01'))->format('Y-m-d');
            $dateEnd = (new \DateTime(Input::get('AsofDate')))->modify('-121 day')->format('Y-m-d');
            $rsName = '121_above';
            $i121 = $this->ClaimLedgerQuery($dateStart, $dateEnd, true, $practiceID, $rsName, $FilterPatientID, $FilterInsuranceID);



            $dateStart = (new \DateTime(Input::get('AsofDate')))->modify('-30 day')->format('Y-m-d');
            $dateEnd = (new \DateTime(Input::get('AsofDate')))->format('Y-m-d');
            $rsName = '0_30';
            $p30 = $this->ClaimLedgerQuery($dateStart, $dateEnd, false, $practiceID, $rsName, $FilterPatientID, $FilterInsuranceID);


            $dateStart = (new \DateTime(Input::get('AsofDate')))->modify('-60 day')->format('Y-m-d');
            $dateEnd = (new \DateTime(Input::get('AsofDate')))->modify('-31 day')->format('Y-m-d');
            $rsName = '31_60';
            $p31 = $this->ClaimLedgerQuery($dateStart, $dateEnd, false, $practiceID, $rsName, $FilterPatientID, $FilterInsuranceID);



            $dateStart = (new \DateTime(Input::get('AsofDate')))->modify('-90 day')->format('Y-m-d');
            $dateEnd = (new \DateTime(Input::get('AsofDate')))->modify('-61 day')->format('Y-m-d');
            $rsName = '61_90';
            $p61 = $this->ClaimLedgerQuery($dateStart, $dateEnd, false, $practiceID, $rsName, $FilterPatientID, $FilterInsuranceID);


            $dateStart = (new \DateTime(Input::get('AsofDate')))->modify('-120 day')->format('Y-m-d');
            $dateEnd = (new \DateTime(Input::get('AsofDate')))->modify('-91 day')->format('Y-m-d');
            $rsName = '91_120';
            $p91 = $this->ClaimLedgerQuery($dateStart, $dateEnd, false, $practiceID, $rsName, $FilterPatientID, $FilterInsuranceID);

            $dateStart = (new \DateTime('2000-01-01'))->format('Y-m-d');
            $dateEnd = (new \DateTime(Input::get('AsofDate')))->modify('-121 day')->format('Y-m-d');
            $rsName = '121_above';
            $p121 = $this->ClaimLedgerQuery($dateStart, $dateEnd, false, $practiceID, $rsName, $FilterPatientID, $FilterInsuranceID);


            $iResult = array_merge($i30->toArray(),$i31->toArray(),$i61->toArray(),$i91->toArray(),$i121->toArray());
            $pResult = array_merge($p30->toArray(),$p31->toArray(),$p61->toArray(),$p91->toArray(),$p121->toArray());

            return array(array('Insurance' =>$iResult),array('Patient' =>$pResult));
        }
    }

    public function ClaimLedgerQuery($DateStart, $DateEnd, $IsInsurance, $Practice, $ResultName, $FilterPatientID = -500, $FilterInsuranceID = -500)
    {
        $rs = m_claim::where(['m_claims.isDeleted' => 0])
            ->join('m_cases','m_cases.caseID','=','m_claims.caseID')
            ->join('m_patients','m_patients.patientID','=','m_cases.patientID')
            ->where(['m_cases.isDeleted'=>0, 'm_patients.practiceID'=>$Practice]);
        if($IsInsurance == true){
            $rs = $rs->where('m_claims.InsuranceID','!=',-100);
        }else{
            $rs = $rs->where('m_claims.InsuranceID','=',-100);
        }
        If($FilterInsuranceID != -500){  $rs = $rs->where('m_claims.InsuranceID','=',$FilterInsuranceID); }
        If($FilterPatientID != -500){ $rs = $rs->where('m_patients.patientID','=',$FilterPatientID); }

        $rs = $rs->where('m_claims.created_At','>=',$DateStart)
            ->where('m_claims.created_At','<=',$DateEnd)
            ->selectRaw('sum(claimBalance) as ' . $ResultName)->get()[0];
/*        $rs = $rs->where('m_claims.created_At','>=',$DateStart)
            ->where('m_claims.created_At','<=',$DateEnd)
            ->selectRaw('sum(adjustment) as ' . $ResultName)->get()[0];*/
        return $rs;
    }


    public function chargesReport(Request $request)
    {
        $validator = Validator::make(Input::all(), ['AsofDate' => 'required', 'PracticeID' => 'required']);
        if ($validator->fails()) {
            return \Response::json(array('errors' => true, 'message' => $validator->messages()));
        } else {
            $practiceID = Input::get('PracticeID');

            $rs = m_claim::where(['m_claims.isDeleted' => 0])
                ->join('m_cases','m_cases.caseID','=','m_claims.caseID')
                ->join('m_patients','m_patients.patientID','=','m_cases.patientID')
                ->join('m_physicians','m_physicians.physicianID','=','m_claims.primaryCarePhy')
                ->join('m_codecpt','m_codecpt.cptCode','=','m_claims.proced')
                ->where(['m_cases.isDeleted'=>0, 'm_patients.practiceID'=>$practiceID])
                ->selectRaw('m_physicians.phyName, m_claims.proced, m_codecpt.description, sum(m_claims.total) as \'ChargeTotal\'')
                ->groupBy(['m_physicians.phyName', 'm_claims.proced', 'm_codecpt.description'])
                ->get();
            return $rs;
        }
    }


    public function incrementClaimNumber(Request $request){
        $validator = Validator::make(Input::all(), ['caseID'=>'required']);
        if ($validator->fails()) {
            return \Response::json(array('errors' => true, 'message' => $validator->messages()));
        }
        else{
            $caseid = (int)Input::get('caseID');
            if ($caseid <= 0) { return \Response::json(array('errors' => true, 'message' => 'Invalid Case ID.')); }
            if (m_case::where(['caseID'=>Input::get('caseID')])->exists()) {

                m_detail::where(['detail'=>'ClaimNumber'])->increment('detailValue1');
                $id1 = m_detail::where(['detail'=>'ClaimNumber'])->get(['detailValue1'])[0]['detailValue1'];
                $idstring = sprintf('%05d', $id1);

                Common::WriteLog('Claims','ClaimNumber Created. ClaimNumber:' . $idstring, 0,'ClaimNumber',$caseid);
                return \Response::json(array('errors' => false, 'message' => 'ClaimNumber created!','ClaimNumber'=> $idstring));
            }
            return \Response::json(array('errors' => true, 'message' => 'Invalid CaseID.'));
        }
    }
}
