<?php

namespace App\Http\Controllers;

use App\m_insurance;
use App\m_patient;
use App\m_detail;
use App\m_claim;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use Response;
use Illuminate\Support\Facades\Input;
use App\Classes\Common;

class patientsController extends Controller
{
    public function list_all(Request $request)
    {
        $validator = Validator::make(Input::all(), ['practiceID'=>'required']);
        if ($validator->fails()) {
            return \Response::json(array('errors' => true, 'message' => $validator->messages()));
        }
        else {
            $pid = (int)Input::get('practiceID');
            if ($pid <= 0) { return \Response::json(array('errors' => true, 'message' => 'Invalid Practice ID.')); }

            //$patients = m_patient::where(['practiceID' => $pid,'isActive'=>1])->get();
            //return $patients;
            //Common::WriteLog('Patients','Patients Listed. Practice ID:' . $pid,0);

            $patients = m_patient::where(['practiceID' => $pid,'isActive'=>1])->selectRaw('m_patients.*, (select phyName from m_physicians where physicianID=primaryCarePhy) PrimaryCarePhysician,(select phyName from m_physicians where physicianID=referringPhy) ReferringPhysician,(select phyName from m_physicians where physicianID=defaultRenderPhy) DefaultPhysician')->get();
            return $patients;
        }
    }

    public function _list()
    {
       // $validator = Validator::make(Input::all(), ['practiceID'=>'required']);
       // if ($validator->fails()) {
        //    return \Response::json(array('errors' => true, 'message' => $validator->messages()));
        //}
        //else {
            //$pid = (int)Input::get('practiceID');
            //if ($pid <= 0) { return \Response::json(array('errors' => true, 'message' => 'Invalid Practice ID.')); }

            //$patients = m_patient::where(['practiceID' => $pid,'isActive'=>1])->get();
            //$patients = m_patient::where(['isActive'=>1])->get();

            $patients = m_patient::where(['isActive'=>1])->selectRaw('m_patients.*, (select phyName from m_physicians where physicianID=primaryCarePhy) PrimaryCarePhysician,(select phyName from m_physicians where physicianID=referringPhy) ReferringPhysician,(select phyName from m_physicians where physicianID=defaultRenderPhy) DefaultPhysician')->get();
            return $patients;
       // }
    }

    public function createPatient(Request $request){
        $validator = Validator::make(Input::all(), ['fullName'=>'required|min:3|max:50','gender'=>'required|in:Male,Female','email'=>'email','practiceID'=>'required']);

        // if the validator fails, redirect back to the form
        if ($validator->fails()) {
            return \Response::json(array('errors' => true, 'message' => $validator->messages()));
        }
        else{
            $user = m_patient::where('fullName', Input::get('fullName'))
                            ->where('email', Input::get('email'))
                            ->where('addrZip', Input::get('addrZip'))
                            ->where('dob', Input::get('dob'))->count();
            if($user < 1 ) {
                $request->merge(array('isActive' => 1));
                $patientID = m_patient::create($request->all())->id;
                Common::WriteLog('Patients', 'New Patient Created. Patient ID:' . $patientID, 0, 'Create', $patientID);
                return \Response::json(array('errors' => false, 'message' => 'Patient Created Successfully!', 'patientID' => $patientID));
            }
            else{
                return \Response::json(array('errors' => false, 'message' => 'Patient already exists'));
            }
        }
    }

    public function LoadPatient(Request $request)
    {
        $validator = Validator::make(Input::all(), ['patientID'=>'required']);

        // if the validator fails, redirect back to the form
        if ($validator->fails()) {
            return \Response::json(array('errors' => true, 'message' => $validator->messages()));
        }
        else{
            //$patients = m_patient::where(['isActive'=>1,'patientID'=>$request['patientID']])->get();
            $patients = m_patient::where(['isActive'=>1,'patientID'=>$request['patientID']])->selectRaw('m_patients.*, (select phyName from m_physicians where physicianID=primaryCarePhy) PrimaryCarePhysician,(select phyName from m_physicians where physicianID=referringPhy) ReferringPhysician,(select phyName from m_physicians where physicianID=defaultRenderPhy) DefaultPhysician')->get();

            //Common::WriteLog('Patients','Patient Loaded. Patient ID:' . $request['patientID'],0);
            return $patients;
        }
    }

    public function savePatient(Request $request){
        $validator = Validator::make(Input::all(), ['fullName'=>'required|min:3|max:50','gender'=>'required|in:Male,Female','email'=>'email','practiceID'=>'required','patientID'=>'required']);

        // if the validator fails, redirect back to the form
        if ($validator->fails()) {
            return \Response::json(array('errors' => true, 'message' => $validator->messages()));
        }
        else{
            $pid = (int)Input::get('patientID');
            if ($pid <= 0) { return \Response::json(array('errors' => true, 'message' => 'Invalid Patient ID.')); }
            if (m_patient::where(['patientID'=>Input::get('patientID')])->exists()) {
                Common::WriteLog('Patients','Patient Edited. Patient ID:' . $pid, 0, 'Edit',$pid);
                $updatePatient = m_patient::where('patientID', '=', $pid)->update($request->all());
                return \Response::json(array('errors' => false, 'message' => 'Patient updated Successfully!'));
            }else{ return \Response::json(array('errors' => true, 'message' => 'Invalid Patient ID.')); }
        }
    }

    public function chartNo(Request $request){
        $validator = Validator::make(Input::all(), ['patientID'=>'required', 'prefix'=>'required|min:3|max:6']);
        if ($validator->fails()) {
            return \Response::json(array('errors' => true, 'message' => $validator->messages()));
        }
        else{
            $pid = (int)Input::get('patientID');
            if ($pid <= 0) { return \Response::json(array('errors' => true, 'message' => 'Invalid Patient ID.')); }
            if (m_patient::where(['patientID'=>Input::get('patientID'),'chartNo'=>['']])->exists()) {

                m_detail::where(['detail'=>'ChartNumber'])->increment('detailValue1');
                $id1 = m_detail::where(['detail'=>'ChartNumber'])->get(['detailValue1'])[0]['detailValue1'];
                $idstring = sprintf('%04d', $id1);

                m_patient::where(['patientID'=>Input::get('patientID')])->update(['chartNo'=>Input::get('prefix') . $idstring]);

                Common::WriteLog('Patients','ChartNumber Created. Patient ID:' . $pid . ' Chart Number:' . Input::get('prefix') . $idstring, 0,'Chart',$pid);
                return \Response::json(array('errors' => false, 'message' => 'ChartNumber updated!','ChartNumber'=>Input::get('prefix') . $idstring));
            }
            return \Response::json(array('errors' => true, 'message' => 'Invalid Patient ID / Chart Number already set.'));
        }
    }


    public function patientsSummaryReport(Request $request){
        $validator = Validator::make(Input::all(), ['FromDate'=>'required', 'ToDate'=>'required']);
        if ($validator->fails()) {
            return \Response::json(array('errors' => true, 'message' => $validator->messages()));
        }
        else{

            $payors = m_insurance::where(['isActive'=>1])
                ->join('m_deposits', 'm_deposits.payorID', '=', 'm_insurances.insuranceID')
                ->join('m_depositdetails', 'm_depositdetails.depositID', '=', 'm_deposits.depositID')
                ->where(['m_deposits.isDeleted'=>0, 'm_depositdetails.isDeleted'=>0])
                ->where('m_depositdetails.created_At','>=',Input::get('FromDate'))
                ->where('m_depositdetails.created_At','<=',Input::get('ToDate'))
                ->get();

            return $payors;
        }
    }

    public function patientsContactListReport(Request $request){
        $patients = m_patient::where(['m_patients.isActive' => 1, 'm_patients.isDeleted' => 0])
            ->join('m_practices','m_practices.practiceID','=','m_patients.practiceID')
            ->where(['m_practices.isActive'=>1])
            ->get();
        return $patients;
    }

    public function patientsTransactionReport(Request $request){
        $validator = Validator::make(Input::all(), ['FromDate'=>'required', 'ToDate'=>'required']);
        if ($validator->fails()) {
            return \Response::json(array('errors' => true, 'message' => $validator->messages()));
        }
        else{
            $patients = m_patient::where(['m_patients.isActive' => 1, 'm_patients.isDeleted' => 0])
                ->select(['m_patients.patientID', 'm_patients.fullName', 'm_patients.phoneHome', 'm_patients.phoneMobile', 'm_patients.email'])
                ->selectRaw('(select sum(charge) from m_claims inner join m_cases on m_cases.caseID=m_claims.caseID where m_cases.patientID=m_patients.patientID and m_claims.isDeleted=0 and m_cases.isDeleted=0 and m_claims.created_At>=\'' . Input::get('FromDate') . '\' and m_claims.created_At<=\'' . Input::get('ToDate') . '\') as TotalCharge
                ,(select sum(paid) from m_claims inner join m_cases on m_cases.caseID=m_claims.caseID where m_cases.patientID=m_patients.patientID and m_claims.isDeleted=0 and m_cases.isDeleted=0 and m_claims.created_At>=\'' . Input::get('FromDate') . '\' and m_claims.created_At<=\'' . Input::get('ToDate') . '\') as TotalPaid
                ,(select sum(allowed) from m_claims inner join m_cases on m_cases.caseID=m_claims.caseID where m_cases.patientID=m_patients.patientID and m_claims.isDeleted=0 and m_cases.isDeleted=0 and m_claims.created_At>=\'' . Input::get('FromDate') . '\' and m_claims.created_At<=\'' . Input::get('ToDate') . '\') as TotalAllowed
                ,(select sum(adjustment) from m_claims inner join m_cases on m_cases.caseID=m_claims.caseID where m_cases.patientID=m_patients.patientID and m_claims.isDeleted=0 and m_cases.isDeleted=0 and m_claims.created_At>=\'' . Input::get('FromDate') . '\' and m_claims.created_At<=\'' . Input::get('ToDate') . '\') as TotalAdjustment')
                ->get();
            return $patients;
        }
    }


    public function patientsBalanceSummaryReport(Request $request){
        $validator = Validator::make(Input::all(), ['AsofDate'=>'required']);
        if ($validator->fails()) {
            return \Response::json(array('errors' => true, 'message' => $validator->messages()));
        }
        else{
            $patients = m_patient::where(['m_patients.isActive' => 1, 'm_patients.isDeleted' => 0])
                ->select(['m_patients.patientID', 'm_patients.fullName', 'm_patients.phoneHome', 'm_patients.phoneMobile', 'm_patients.email'])
                ->selectRaw('(select sum(charge) from m_claims inner join m_cases on m_cases.caseID=m_claims.caseID where m_cases.patientID=m_patients.patientID and m_claims.isDeleted=0 and m_cases.isDeleted=0 and m_claims.created_At<=\'' . Input::get('AsofDate') . '\') as TotalCharge
                ,(select sum(paid) from m_claims inner join m_cases on m_cases.caseID=m_claims.caseID where m_cases.patientID=m_patients.patientID and m_claims.isDeleted=0 and m_cases.isDeleted=0  and m_claims.created_At<=\'' . Input::get('AsofDate') . '\') as TotalPaid
                ,(select sum(allowed) from m_claims inner join m_cases on m_cases.caseID=m_claims.caseID where m_cases.patientID=m_patients.patientID and m_claims.isDeleted=0 and m_cases.isDeleted=0  and m_claims.created_At<=\'' . Input::get('AsofDate') . '\') as TotalAllowed
                ,(select sum(adjustment) from m_claims inner join m_cases on m_cases.caseID=m_claims.caseID where m_cases.patientID=m_patients.patientID and m_claims.isDeleted=0 and m_cases.isDeleted=0  and m_claims.created_At<=\'' . Input::get('AsofDate') . '\') as TotalAdjustment')
                ->get();
            return $patients;
        }
    }


    public function arAgingReport(Request $request)
    {
        $validator = Validator::make(Input::all(), ['AsofDate' => 'required']);
        if ($validator->fails()) {
            return \Response::json(array('errors' => true, 'message' => $validator->messages()));
        } else {
            $dateEndUB = (new \DateTime(Input::get('AsofDate')))->format('Y-m-d');

            $dateStartC = (new \DateTime(Input::get('AsofDate')))->modify('-30 day')->format('Y-m-d');
            $dateEndC = (new \DateTime(Input::get('AsofDate')))->format('Y-m-d');

            $dateStartC31 = (new \DateTime(Input::get('AsofDate')))->modify('-60 day')->format('Y-m-d');
            $dateEndC31 = (new \DateTime(Input::get('AsofDate')))->modify('-31 day')->format('Y-m-d');

            $dateStartC61 = (new \DateTime(Input::get('AsofDate')))->modify('-90 day')->format('Y-m-d');
            $dateEndC61 = (new \DateTime(Input::get('AsofDate')))->modify('-61 day')->format('Y-m-d');

            $dateStartC91 = (new \DateTime(Input::get('AsofDate')))->modify('-120 day')->format('Y-m-d');
            $dateEndC91 = (new \DateTime(Input::get('AsofDate')))->modify('-91 day')->format('Y-m-d');

            $dateStartC121 = (new \DateTime('2000-01-01'))->format('Y-m-d');
            $dateEndC121 = (new \DateTime(Input::get('AsofDate')))->modify('-121 day')->format('Y-m-d');

            $ins = m_claim::where(['m_claims.isDeleted' => 0])
                ->join('m_cases','m_cases.caseID','=','m_claims.caseID')
                ->join('m_patients','m_patients.patientID','=','m_cases.patientID')
                ->where(['m_cases.isDeleted'=>0])
                ->where('m_claims.created_At','<=',$dateEndUB)
                ->where('m_claims.InsuranceID','=',-100)
                ->selectRaw('m_patients.patientID,m_patients.fullName,
                (select sum(claimBalance) from m_claims t1 inner join m_cases t2 on t2.caseID=t1.caseID where t1.isDeleted=0 and t2.isDeleted=0 and t1.insuranceID=-100 and t2.patientID=m_patients.patientID and t1.ClaimStatus!=\'COMPLETE\' and t1.created_At<=\'' . $dateEndUB . '\') Unbilled,
                (select sum(claimBalance) from m_claims t1 inner join m_cases t2 on t2.caseID=t1.caseID where t1.isDeleted=0 and t2.isDeleted=0 and t1.insuranceID=-100 and t2.patientID=m_patients.patientID and t1.ClaimStatus=\'COMPLETE\' and t1.created_At>=\'' . $dateStartC . '\' and t1.created_At<=\'' . $dateEndC . '\') Current,
                (select sum(claimBalance) from m_claims t1 inner join m_cases t2 on t2.caseID=t1.caseID where t1.isDeleted=0 and t2.isDeleted=0 and t1.insuranceID=-100 and t2.patientID=m_patients.patientID and t1.ClaimStatus=\'COMPLETE\' and t1.created_At>=\'' . $dateStartC31 . '\' and t1.created_At<=\'' . $dateEndC31 . '\') C_31_60,
                (select sum(claimBalance) from m_claims t1 inner join m_cases t2 on t2.caseID=t1.caseID where t1.isDeleted=0 and t2.isDeleted=0 and t1.insuranceID=-100 and t2.patientID=m_patients.patientID and t1.ClaimStatus=\'COMPLETE\' and t1.created_At>=\'' . $dateStartC61 . '\' and t1.created_At<=\'' . $dateEndC61 . '\') C_61_90,
                (select sum(claimBalance) from m_claims t1 inner join m_cases t2 on t2.caseID=t1.caseID where t1.isDeleted=0 and t2.isDeleted=0 and t1.insuranceID=-100 and t2.patientID=m_patients.patientID and t1.ClaimStatus=\'COMPLETE\' and t1.created_At>=\'' . $dateStartC91 . '\' and t1.created_At<=\'' . $dateEndC91 . '\') C_91_120,
                (select sum(claimBalance) from m_claims t1 inner join m_cases t2 on t2.caseID=t1.caseID where t1.isDeleted=0 and t2.isDeleted=0 and t1.insuranceID=-100 and t2.patientID=m_patients.patientID and t1.ClaimStatus=\'COMPLETE\' and t1.created_At>=\'' . $dateStartC121 . '\' and t1.created_At<=\'' . $dateEndC121 . '\') C_121_Above
                ')->distinct()
                ->get();
            return $ins;
        }
    }


    public function arAgingDetailReport(Request $request)
    {
        $validator = Validator::make(Input::all(), ['AsofDate' => 'required', 'patientID'=>'required']);
        if ($validator->fails()) {
            return \Response::json(array('errors' => true, 'message' => $validator->messages()));
        } else {
            $dateEnd = (new \DateTime(Input::get('AsofDate')))->format('Y-m-d');

            $ins = m_claim::where(['m_claims.isDeleted' => 0])
                ->join('m_cases','m_cases.caseID','=','m_claims.caseID')
                ->join('m_patients','m_patients.patientID','=','m_cases.patientID')
                ->where(['m_cases.isDeleted'=>0])
                ->where('m_claims.InsuranceID','=',-100)
                ->where(['m_cases.patientID'=>Input::get('patientID')])
                ->where('m_claims.created_At','<=',$dateEnd)
                ->selectRaw('m_claims.claimID,m_claims.claimBalance,m_claims.claimStatus
                ')->distinct()
                ->get();
            return $ins;
        }
    }
}

