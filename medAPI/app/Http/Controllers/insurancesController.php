<?php

namespace App\Http\Controllers;

use App\m_claim;
use App\m_insurance;
use App\m_practice;
use App\m_patient;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use Response;
use Illuminate\Support\Facades\Input;
use App\Classes\Common;

class insurancesController extends Controller
{
    public function list_all(Request $request)
    {
        $validator = Validator::make(Input::all(), ['practiceID'=>'required']);
        if ($validator->fails()) {
            return \Response::json(array('errors' => true, 'message' => $validator->messages()));
        }
        else {
            $cid = (int)Input::get('practiceID');
            if ($cid <= 0) { return \Response::json(array('errors' => true, 'message' => 'Invalid Practice ID.')); }

            if (m_practice::where(['practiceID'=>Input::get('practiceID')])->exists()) {
                $insurances = m_insurance::where(['practiceID' => $cid,'isActive'=>1])->get();
                //Common::WriteLog('Insurance','Insurances Listed. Practice ID:' . $cid,0);
                return $insurances;
            }else{
                return \Response::json(array('errors' => true, 'message' => 'Invalid Practice ID.'));
            }
        }
    }

    public function loadInsurance(Request $request)
    {
        $validator = Validator::make(Input::all(), ['insuranceID'=>'required']);
        if ($validator->fails()) {
            return \Response::json(array('errors' => true, 'message' => $validator->messages()));
        }
        else {
            $cid = (int)Input::get('insuranceID');
            if ($cid <= 0) { return \Response::json(array('errors' => true, 'message' => 'Invalid Insurance ID.')); }

            if (m_insurance::where(['insuranceID'=>Input::get('insuranceID')])->exists()) {
                $insurances = m_insurance::where(['insuranceID' => $cid,'isActive'=>1])->get();
                //Common::WriteLog('Insurance','Insurance Loaded. Insurance ID:' . $cid,0);
                return $insurances;
            }else{
                return \Response::json(array('errors' => true, 'message' => 'Invalid Insurance ID.'));
            }
        }
    }

    public function createInsurance(Request $request)
    {
        $validator = Validator::make(Input::all(), ['payerName'=>'required','practiceID'=>'required']);
        if ($validator->fails()) {
            return \Response::json(array('errors' => true, 'message' => $validator->messages()));
        }
        else {
            if (m_practice::where(['practiceID'=>Input::get('practiceID')])->exists()) {
                $request->merge( array( 'isActive' => 1) );
                $insuranceID = m_insurance::create($request->all())->id;
                Common::WriteLog('Insurance','Insurance Created. Insurance ID:' . $insuranceID,0,'Create',$insuranceID);
                return \Response::json(array('errors' => false, 'message' => 'Insurance Created Successfully!', 'InsuranceID' => $insuranceID));
            }else{
                return \Response::json(array('errors' => true, 'message' => 'Invalid practice ID.'));
            }
        }
    }


    public function listPatients(Request $request)
    {
        $validator = Validator::make(Input::all(), ['insuranceID'=>'required']);
        if ($validator->fails()) {
            return \Response::json(array('errors' => true, 'message' => $validator->messages()));
        }
        else {
            $insuranceID = Input::get('insuranceID');
            if (m_insurance::where(['insuranceID'=> $insuranceID])->exists()) {

                $patients = m_patient::join('m_cases', 'm_cases.patientID', '=', 'm_patients.patientID')
                    ->join('m_policies', 'm_policies.caseID', '=', 'm_cases.caseID')
                    ->where(['m_policies.insuranceID' => $insuranceID,'m_cases.isDeleted'=>0,'m_patients.isDeleted'=>0,'m_patients.isActive'=>1])
                    ->select(['m_patients.*'])
                    ->distinct()
                    ->get();
                //Common::WriteLog('Insurance','Patients Listed. Insurance ID:' . $insuranceID,0);
                return $patients;
            }else{
                return \Response::json(array('errors' => true, 'message' => 'Invalid insurance ID.'));
            }
        }
    }



    public function arAgingReport(Request $request)
    {
        $validator = Validator::make(Input::all(), ['AsofDate' => 'required']);
        if ($validator->fails()) {
            return \Response::json(array('errors' => true, 'message' => $validator->messages()));
        } else {
            $dateEndUB = (new \DateTime(Input::get('AsofDate')))->format('Y-m-d');

            $dateStartC = (new \DateTime(Input::get('AsofDate')))->modify('-30 day')->format('Y-m-d');
            $dateEndC = (new \DateTime(Input::get('AsofDate')))->format('Y-m-d');

            $dateStartC31 = (new \DateTime(Input::get('AsofDate')))->modify('-60 day')->format('Y-m-d');
            $dateEndC31 = (new \DateTime(Input::get('AsofDate')))->modify('-31 day')->format('Y-m-d');

            $dateStartC61 = (new \DateTime(Input::get('AsofDate')))->modify('-90 day')->format('Y-m-d');
            $dateEndC61 = (new \DateTime(Input::get('AsofDate')))->modify('-61 day')->format('Y-m-d');

            $dateStartC91 = (new \DateTime(Input::get('AsofDate')))->modify('-120 day')->format('Y-m-d');
            $dateEndC91 = (new \DateTime(Input::get('AsofDate')))->modify('-91 day')->format('Y-m-d');

            $dateStartC121 = (new \DateTime('2000-01-01'))->format('Y-m-d');
            $dateEndC121 = (new \DateTime(Input::get('AsofDate')))->modify('-121 day')->format('Y-m-d');

            $ins = m_claim::where(['m_claims.isDeleted' => 0])
                ->join('m_cases','m_cases.caseID','=','m_claims.caseID')
                ->join('m_insurances','m_insurances.insuranceID','=','m_claims.insuranceID')
                ->where(['m_cases.isDeleted'=>0])
                ->where('m_claims.InsuranceID','!=',-100)
                ->where('m_claims.created_At','<=',$dateEndUB)
                ->selectRaw('m_claims.InsuranceID,m_insurances.payerName,
                (select sum(claimBalance) from m_claims t1 inner join m_cases on m_cases.caseID=t1.caseID where t1.isDeleted=0 and m_cases.isDeleted=0 and t1.insuranceID!=-100 and t1.insuranceID=m_insurances.insuranceID and t1.ClaimStatus!=\'COMPLETE\' and t1.created_At<=\'' . $dateEndUB . '\') Unbilled,
                (select sum(claimBalance) from m_claims t1 inner join m_cases on m_cases.caseID=t1.caseID where t1.isDeleted=0 and m_cases.isDeleted=0 and t1.insuranceID!=-100 and t1.insuranceID=m_insurances.insuranceID and t1.ClaimStatus=\'COMPLETE\' and t1.created_At>=\'' . $dateStartC . '\' and t1.created_At<=\'' . $dateEndC . '\') Current,
                (select sum(claimBalance) from m_claims t1 inner join m_cases on m_cases.caseID=t1.caseID where t1.isDeleted=0 and m_cases.isDeleted=0 and t1.insuranceID!=-100 and t1.insuranceID=m_insurances.insuranceID and t1.ClaimStatus=\'COMPLETE\' and t1.created_At>=\'' . $dateStartC31 . '\' and t1.created_At<=\'' . $dateEndC31 . '\') C_31_60,
                (select sum(claimBalance) from m_claims t1 inner join m_cases on m_cases.caseID=t1.caseID where t1.isDeleted=0 and m_cases.isDeleted=0 and t1.insuranceID!=-100 and t1.insuranceID=m_insurances.insuranceID and t1.ClaimStatus=\'COMPLETE\' and t1.created_At>=\'' . $dateStartC61 . '\' and t1.created_At<=\'' . $dateEndC61 . '\') C_61_90,
                (select sum(claimBalance) from m_claims t1 inner join m_cases on m_cases.caseID=t1.caseID where t1.isDeleted=0 and m_cases.isDeleted=0 and t1.insuranceID!=-100 and t1.insuranceID=m_insurances.insuranceID and t1.ClaimStatus=\'COMPLETE\' and t1.created_At>=\'' . $dateStartC91 . '\' and t1.created_At<=\'' . $dateEndC91 . '\') C_91_120,
                (select sum(claimBalance) from m_claims t1 inner join m_cases on m_cases.caseID=t1.caseID where t1.isDeleted=0 and m_cases.isDeleted=0 and t1.insuranceID!=-100 and t1.insuranceID=m_insurances.insuranceID and t1.ClaimStatus=\'COMPLETE\' and t1.created_At>=\'' . $dateStartC121 . '\' and t1.created_At<=\'' . $dateEndC121 . '\') C_121_Above
                ')->distinct()
                ->get();
            return $ins;
        }
    }

    public function arAgingDetailReport(Request $request)
    {
        $validator = Validator::make(Input::all(), ['AsofDate' => 'required', 'insuranceID'=>'required']);
        if ($validator->fails()) {
            return \Response::json(array('errors' => true, 'message' => $validator->messages()));
        } else {
            $dateEndUB = (new \DateTime(Input::get('AsofDate')))->format('Y-m-d');

            $ins = m_claim::where(['m_claims.isDeleted' => 0])
                ->join('m_cases','m_cases.caseID','=','m_claims.caseID')
                ->join('m_insurances','m_insurances.insuranceID','=','m_claims.insuranceID')
                ->where(['m_cases.isDeleted'=>0])
                ->where('m_claims.InsuranceID','!=',-100)
                ->where('m_claims.InsuranceID','=',Input::get('insuranceID'))
                ->where('m_claims.created_At','<=',$dateEndUB)
                ->selectRaw('m_claims.claimID,m_claims.claimBalance,m_claims.claimStatus')->distinct()
                ->get();
            return $ins;
        }
    }
}
