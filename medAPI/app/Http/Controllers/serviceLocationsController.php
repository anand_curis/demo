<?php

namespace App\Http\Controllers;

use App\m_serviceLocation;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use Response;
use Illuminate\Support\Facades\Input;
use App\Classes\Common;

class serviceLocationsController extends Controller
{
    //

    public function create(Request $request){
        $validator = Validator::make(Input::all(), ['internalName'=>'required|min:3|max:100','practiceID'=>'required']);

        // if the validator fails, redirect back to the form
        if ($validator->fails()) {
            return \Response::json(array('errors' => true, 'message' => $validator->messages()));
        }
        else{
            $request->merge( array( 'isActive' => 1) );
            $serviceLocID = m_serviceLocation::create($request->all())->id;
            Common::WriteLog('ServiceLocations','Service Location Created. Service Location ID:' . $serviceLocID,0, 'Create',$serviceLocID);
            return \Response::json(array('errors' => false, 'message' => 'Service Location Created Successfully!', 'servcieLocID' => $serviceLocID));
        }
    }

    public function load(Request $request)
    {
        $validator = Validator::make(Input::all(), ['serviceLocID'=>'required']);

        // if the validator fails, redirect back to the form
        if ($validator->fails()) {
            return \Response::json(array('errors' => true, 'message' => $validator->messages()));
        }
        else{
            $serviceLocation = m_serviceLocation::where(['isActive'=>1,'serviceLocID'=>$request['serviceLocID']])->get();
            //Common::WriteLog('ServiceLocations','Service Location Loaded. Service Location ID:' . $request['serviceLocID'],0);
            return $serviceLocation;
        }
    }


    public function save(Request $request){
        $validator = Validator::make(Input::all(), ['internalName'=>'required|min:3|max:100','practiceID'=>'required','serviceLocID'=>'required']);

        // if the validator fails, redirect back to the form
        if ($validator->fails()) {
            return \Response::json(array('errors' => true, 'message' => $validator->messages()));
        }
        else{
            $rid = (int)Input::get('serviceLocID');
            if ($rid <= 0) { return \Response::json(array('errors' => true, 'message' => 'Invalid Service Location ID.')); }
            if (m_serviceLocation::where(['serviceLocID'=>Input::get('serviceLocID')])->exists()) {
                $updateLocation = m_serviceLocation::where('serviceLocID', '=', $rid)->update($request->all());
                Common::WriteLog('ServiceLocations','Service Location Updated. Service Location ID:' . $rid,0, 'Edit', $rid);
                return \Response::json(array('errors' => false, 'message' => 'Location updated Successfully!'));
            }else{ return \Response::json(array('errors' => true, 'message' => 'Invalid Service Location ID.')); }
        }
    }
}
