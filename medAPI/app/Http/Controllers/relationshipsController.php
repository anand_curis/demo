<?php

namespace App\Http\Controllers;

use App\m_relationship;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use Response;
use Illuminate\Support\Facades\Input;
use App\Classes\Common;

class relationshipsController extends Controller
{
    //

    public function createRelation(Request $request){
        $validator = Validator::make(Input::all(), ['fullName'=>'required|min:3|max:50','gender'=>'required|in:Male,Female','practiceID'=>'required']);

        // if the validator fails, redirect back to the form
        if ($validator->fails()) {
            return \Response::json(array('errors' => true, 'message' => $validator->messages()));
        }
        else{
            $request->merge( array( 'isActive' => 1) );
            $relationID = m_relationship::create($request->all())->id;
            Common::WriteLog('Relationships','Relationship Created. Relation ID:' . $relationID,0,'Create',$relationID);
            return \Response::json(array('errors' => false, 'message' => 'Relation Created Successfully!', 'relationID' => $relationID));
        }
    }

    public function loadRelation(Request $request)
    {
        $validator = Validator::make(Input::all(), ['relationID'=>'required']);

        // if the validator fails, redirect back to the form
        if ($validator->fails()) {
            return \Response::json(array('errors' => true, 'message' => $validator->messages()));
        }
        else{
            $relation = m_relationship::where(['isActive'=>1,'relationID'=>$request['relationID']])->get();
            //Common::WriteLog('Relationships','Relationship Loaded. Relation ID:' . $request['relationID'],0);
            return $relation;
        }
    }


    public function saveRelation(Request $request){
        $validator = Validator::make(Input::all(), ['fullName'=>'required|min:3|max:50','gender'=>'required|in:Male,Female','practiceID'=>'required','relationID'=>'required']);

        // if the validator fails, redirect back to the form
        if ($validator->fails()) {
            return \Response::json(array('errors' => true, 'message' => $validator->messages()));
        }
        else{
            $rid = (int)Input::get('relationID');
            if ($rid <= 0) { return \Response::json(array('errors' => true, 'message' => 'Invalid Relation ID.')); }
            if (m_relationship::where(['relationID'=>Input::get('relationID')])->exists()) {
                $updateRelation = m_relationship::where('relationID', '=', $rid)->update($request->all());
                Common::WriteLog('Relationships','Relationship Saved. Relation ID:' . $rid,0,'Edit',$rid);
                return \Response::json(array('errors' => false, 'message' => 'Relation updated Successfully!'));
            }else{ return \Response::json(array('errors' => true, 'message' => 'Invalid Relation ID.')); }
        }
    }
}
