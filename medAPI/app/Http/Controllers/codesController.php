<?php

namespace App\Http\Controllers;

use App\m_codecpt;
use App\m_codeicd;
use Illuminate\Http\Request;

use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Classes\Common;

class codesController extends Controller
{
    //
    public function list_cpt(Request $request)
    {   $query = m_codecpt::whereNotNull('cptCode');
        if (Input::has('description')){ $query = $query->where('description','LIKE','%'.Input::get('description').'%'); }
        if (Input::has('cptCode')){ $query = $query->where(['cptCode'=>Input::get('cptCode')]); }
        //$cpts = $query->take(500)->get();
        $cpts = $query->get();
        //Common::WriteLog('Codes','CPT Listed.',0);
        return $cpts;
    }

    public function list_icd(Request $request)
    {
        $validator = Validator::make(Input::all(), ['codeType'=>'required']);
        if ($validator->fails()) {
            return \Response::json(array('errors' => true, 'message' => $validator->messages()));
        }
        else {
            $query = m_codeicd::where(['codeType' => Input::get('codeType')]);
            if (Input::has('icdCode')){ $query = $query->where(['icdCode'=>Input::get('icdCode')]); }
            if (Input::has('longDescription')){ $query = $query->where('longDescription','LIKE','%'.Input::get('longDescription').'%'); }
            if (Input::has('shortDescription')){ $query = $query->where('shortDescription','LIKE','%'.Input::get('shortDescription').'%'); }
            $icds = $query->get();
            //Common::WriteLog('Codes','ICD Listed. Code Type:' . Input::get('codeType'),0);
            return $icds;
        }
    }

    public function cptcreate(Request $request){
        $validator = Validator::make(Input::all(), ['cptCode'=>'required','description'=>'required','charge'=>'required']);
        if ($validator->fails()) {
            return \Response::json(array('errors' => true, 'message' => $validator->messages()));
        }
        else{
            if (m_codecpt::where(['cptCode'=>Input::get('cptCode')])->exists()) {
                return \Response::json(array('errors' => true, 'message' => 'This CPT Code Exist Already.'));
            }else{
                $relationID = m_codecpt::create($request->all())->id;
                Common::WriteLog('Codes','CPT Created. ID:' . $relationID,0,'CreateCPT', $relationID);
                return \Response::json(array('errors' => false, 'message' => 'CPT Created Successfully!', 'ID' => $relationID)); }
        }
    }

    public function icdcreate(Request $request){
        $validator = Validator::make(Input::all(), ['icdCode'=>'required','dxCode'=>'required','longDescription'=>'required','shortDescription'=>'required','codeType'=>'required']);
        if ($validator->fails()) {
            return \Response::json(array('errors' => true, 'message' => $validator->messages()));
        }
        else{
            if (m_codeicd::where(['icdCode'=>Input::get('icdCode')])->exists()) {
                return \Response::json(array('errors' => true, 'message' => 'This ICD Code Exist Already.'));
            }else{
                $relationID = m_codeicd::create($request->all())->id;
                Common::WriteLog('Codes','ICD Created. ID:' . $relationID,0,'CreateICD',$relationID);
                return \Response::json(array('errors' => false, 'message' => 'ICD Created Successfully!', 'ID' => $relationID)); }
        }
    }

    public function cptsave(Request $request){
        $validator = Validator::make(Input::all(), ['id'=>'required','cptCode'=>'required','description'=>'required','charge'=>'required']);
        if ($validator->fails()) {
            return \Response::json(array('errors' => true, 'message' => $validator->messages()));
        }
        else{
            $rid = (int)Input::get('id');
            if ($rid <= 0) { return \Response::json(array('errors' => true, 'message' => 'Invalid Code ID.')); }
            if (m_codecpt::where(['id'=>$rid])->exists()) {
                if (m_codecpt::where('id','!=',$rid)->where(['cptCode'=>Input::get('cptCode')])->exists()) {
                    return \Response::json(array('errors' => true, 'message' => 'This CPT Code Exist Already.'));
                }else{
                    $updateRelation = m_codecpt::where('id', '=', $rid)->update($request->all());
                    Common::WriteLog('Codes','CPT Saved. ID:' . $rid,0,'EditCPT',$rid);
                    return \Response::json(array('errors' => false, 'message' => 'CPT updated Successfully!'));
                }
            }else{ return \Response::json(array('errors' => true, 'message' => 'Invalid Code ID.')); }
        }
    }

    public function icdsave(Request $request){
        $validator = Validator::make(Input::all(), ['id'=>'required','icdCode'=>'required','dxCode'=>'required','longDescription'=>'required','shortDescription'=>'required','codeType'=>'required']);
        if ($validator->fails()) {
            return \Response::json(array('errors' => true, 'message' => $validator->messages()));
        }
        else{
            $rid = (int)Input::get('id');
            if ($rid <= 0) { return \Response::json(array('errors' => true, 'message' => 'Invalid Code ID.')); }
            if (m_codeicd::where(['id'=>$rid])->exists()) {
                if (m_codeicd::where('id','!=',$rid)->where(['icdCode'=>Input::get('icdCode')])->exists()) {
                    return \Response::json(array('errors' => true, 'message' => 'This ICD Code Exist Already.'));
                }else{
                    $updateRelation = m_codeicd::where('id', '=', $rid)->update($request->all());
                    Common::WriteLog('Codes','CPT Saved. ID:' . $rid,0,'EditICD',$rid);
                    return \Response::json(array('errors' => false, 'message' => 'ICD updated Successfully!'));
                }
            }else{ return \Response::json(array('errors' => true, 'message' => 'Invalid Code ID.')); }
        }
    }
}
