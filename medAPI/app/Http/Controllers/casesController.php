<?php

namespace App\Http\Controllers;

use App\m_policy;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\m_patient;
use App\m_case;
use Validator;
use Response;
use Illuminate\Support\Facades\Input;
use App\Classes\Common;

class casesController extends Controller
{
    //
    public function createCase(Request $request){
        $validator = Validator::make(Input::all(), ['description'=>'required|min:3|max:50','patientID'=>'required']);

        // if the validator fails, redirect back to the form
        if ($validator->fails()) {
            return \Response::json(array('errors' => true, 'message' => $validator->messages()));
        }
        else{
            if (m_patient::where(['patientID'=>Input::get('patientID')])->exists()) {

                $casecol = m_case::where(['patientID'=>Input::get('patientID')])->get();
                $totC = count($casecol) + 1;
                $prefix = m_patient::where(['patientID'=>Input::get('patientID')])->get(['ChartNo'])[0]['ChartNo'];
                $prefix = $prefix . '_' . $totC;
                $request->merge( array( 'caseChartNo' => $prefix) );
                $request->merge( array( 'isDeleted' => 0) );
                $caseID = m_case::create($request->all())->id;

                Common::WriteLog('Cases','Case Created. Case ID:' . $caseID . ' Case Chart Number:' . $prefix,0, 'Create', $caseID);
                return \Response::json(array('errors' => false, 'message' => 'Case Created Successfully!', 'CaseID' => $caseID, 'CaseChartNo' => $prefix));
            }else{
                return \Response::json(array('errors' => true, 'message' => 'Invalid Patient ID.'));
            }
        }
    }


    public function editCase(Request $request){
        $validator = Validator::make(Input::all(), ['description'=>'required|min:3|max:50','patientID'=>'required','caseID'=>'required']);

        // if the validator fails, redirect back to the form
        if ($validator->fails()) {
            return \Response::json(array('errors' => true, 'message' => $validator->messages()));
        }
        else{
            if (m_patient::where(['patientID'=>Input::get('patientID')])->exists()) {
                if (m_case::where(['caseID'=>Input::get('caseID')])->exists()) {

                    //TODO: Update the case

                    //$casecol = m_case::where(['patientID'=>Input::get('patientID')])->get();
                    //$totC = count($casecol) + 1;
                    //$prefix = m_patient::where(['patientID'=>Input::get('patientID')])->get(['ChartNo'])[0]['ChartNo'];
                    //$prefix = $prefix . '_' . $totC;
                    //$request->merge( array( 'caseChartNo' => $prefix) );
                    //$request->merge( array( 'isDeleted' => 0) );
                    //$caseID = m_case::create($request->all())->id;
                    //return \Response::json(array('errors' => false, 'message' => 'Case Created Successfully!', 'CaseID' => $caseID, 'CaseChartNo' => $prefix));

                }else{ return \Response::json(array('errors' => true, 'message' => 'Invalid Case ID.')); }

            }else{
                return \Response::json(array('errors' => true, 'message' => 'Invalid Patient ID.'));
            }
        }
    }


    public function list_all(Request $request)
    {
        $validator = Validator::make(Input::all(), ['patientID'=>'required']);
        if ($validator->fails()) {
            return \Response::json(array('errors' => true, 'message' => $validator->messages()));
        }
        else {
            $pid = (int)Input::get('patientID');
            if ($pid <= 0) { return \Response::json(array('errors' => true, 'message' => 'Invalid Patient ID.')); }

            if (m_patient::where(['patientID'=>Input::get('patientID')])->exists()) {
                $cases = m_case::where(['patientID' => $pid,'isDeleted'=>0])->get();
                //Common::WriteLog('Cases','Cases Listed. Patient ID:' . $pid,0);
                return $cases;
            }else{
                return \Response::json(array('errors' => true, 'message' => 'Invalid Patient ID.'));
            }
        }
    }


    public function loadCase(Request $request)
    {
        $validator = Validator::make(Input::all(), ['caseChartNo'=>'required']);

        // if the validator fails, redirect back to the form
        if ($validator->fails()) {
            return \Response::json(array('errors' => true, 'message' => $validator->messages()));
        }
        else{
            if (m_case::where(['caseChartNo'=>Input::get('caseChartNo')])->exists()) {
                //$case = m_case::where(['isDeleted'=>0,'caseChartNo'=>$request['caseChartNo']])->get();
                $case = m_case::where(['isDeleted'=>0,'caseChartNo'=>$request['caseChartNo']])->selectRaw('m_cases.*,(select phyName from m_physicians where physicianID=assignedProvider) assignedProviderName,(select phyName from m_physicians where physicianID=referringProvider) referringProviderName,(select phyName from m_physicians where physicianID=supervisingProvider) supervisingProviderName,(select phyName from m_physicians where physicianID=operatingProvider) operatingProviderName,(select phyName from m_physicians where physicianID=otherProvider) otherProviderName')->get();
                //Common::WriteLog('Cases','Case Loaded. Case Chart Number:' . Input::get('caseChartNo'),0);
                return $case;
            }else{
                return \Response::json(array('errors' => true, 'message' => 'Invalid CaseChartNo.'));
            }
        }
    }

    public function caseChart(Request $request){
        $validator = Validator::make(Input::all(), ['caseChartNo'=>'required']);

        // if the validator fails, redirect back to the form
        if ($validator->fails()) {
            return \Response::json(array('errors' => true, 'message' => $validator->messages()));
        }
        else {
            $policies = m_policy::join('m_cases', 'm_policies.caseID', '=', 'm_cases.caseID')
                ->where(['caseChartNo'=>$request['caseChartNo']])
                ->get();
            //Common::WriteLog('Cases','Policies Loaded. Case Chart Number:' . Input::get('caseChartNo'),0);
            return $policies;
        }
    }
}
