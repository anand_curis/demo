<?php

namespace App\Http\Controllers;

use App\m_practice;
use App\m_physician;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use Response;
use Illuminate\Support\Facades\Input;
use App\Classes\Common;

class physiciansController extends Controller
{
    //
    public function list_all(Request $request)
    {
        $validator = Validator::make(Input::all(), ['practiceID'=>'required']);

        // if the validator fails, redirect back to the form
        if ($validator->fails()) {
            return \Response::json(array('errors' => true, 'message' => $validator->messages()));
        }
        else {
            $did = (int)Input::get('practiceID');
            if ($did <= 0) { return \Response::json(array('errors' => true, 'message' => 'Invalid practiceID.')); }
            if (m_practice::where(['practiceID'=>Input::get('practiceID')])->exists()) {
                $physicians = m_physician::where('practiceID', $did)->get();
                //Common::WriteLog('Phyisicians','Physicians Listed. Practice ID:' . $did,0);
                return $physicians;
            }else
            { return \Response::json(array('errors' => true, 'message' => 'Invalid practiceID.')); }
        }
    }

    public function createPhysician(Request $request){
        $validator = Validator::make(Input::all(), ['phyName'=>'required','practiceID'=>'required']);

        // if the validator fails, redirect back to the form
        if ($validator->fails()) {
            return \Response::json(array('errors' => true, 'message' => $validator->messages()));
        }
        else{
            m_practice::create($request->all());
            Common::WriteLog('Phyisicians','Physician Created.',0,'Create',0);
            return \Response::json(array('errors' => false, 'message' => 'Physician Created Successfully!'));
        }
    }

    public function loadPhysician(Request $request)
    {
        $validator = Validator::make(Input::all(), ['physicianID'=>'required']);

        // if the validator fails, redirect back to the form
        if ($validator->fails()) {
            return \Response::json(array('errors' => true, 'message' => $validator->messages()));
        }
        else {
            $did = (int)Input::get('physicianID');
            if ($did <= 0) { return \Response::json(array('errors' => true, 'message' => 'Invalid physicianID.')); }
            if (m_physician::where(['physicianID'=>Input::get('physicianID')])->exists()) {
                $physicians = m_physician::where('physicianID', $did)->get();
                //Common::WriteLog('Phyisicians','Physician Loaded. Physician ID:' . $did,0);
                return $physicians;
            }else
            { return \Response::json(array('errors' => true, 'message' => 'Invalid physicianID.')); }
        }
    }
}
