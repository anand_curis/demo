<?php

namespace App\Http\Controllers;

use App\m_case;
use App\m_policy;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use Response;
use Illuminate\Support\Facades\Input;
use App\Classes\Common;

class policiesController extends Controller
{
    //
    /*public function LoadPolicy(Request $request)
    {
        $validator = Validator::make(Input::all(), ['caseID'=>'required','policyEntity'=>'required' ]);

        // if the validator fails, redirect back to the form
        if ($validator->fails()) {
            return \Response::json(array('errors' => true, 'message' => $validator->messages()));
        }
        else{
            $policies = m_policy::where(['caseID'=>$request['caseID'], 'policyEntity'=>$request['policyEntity']])->get();
            return $policies;
        }
    }*/

    public function LoadPolicy(Request $request)
    {
        $validator = Validator::make(Input::all(), ['caseChartNo'=>'required','policyEntity'=>'required' ]);

        // if the validator fails, redirect back to the form
        if ($validator->fails()) {
            return \Response::json(array('errors' => true, 'message' => $validator->messages()));
        }
        else{
            if (m_case::where(['caseChartNo'=>Input::get('caseChartNo')])->exists()) {
                $caseid = m_case::where(['caseChartNo'=>Input::get('caseChartNo')])->pluck('caseID')[0];
                if ($caseid <= 0) { return \Response::json(array('errors' => true, 'message' => 'Invalid Case Chart Number.')); }

                $policies = m_policy::where(['caseID'=>$caseid, 'policyEntity'=>$request['policyEntity']])->selectRaw('m_policies.*, (select payerName from m_insurances where m_insurances.insuranceID=m_policies.insuranceID) InsuranceName')->get();
                //Common::WriteLog('Policies','Policy Loaded. Case ID:' . $caseid,0);
                return $policies;
            }
            else{
                return \Response::json(array('errors' => true, 'message' => 'Invalid Case Chart Number.'));
            }
        }
    }

    public function createPolicy(Request $request)
    {
        $validator = Validator::make(Input::all(), ['policyEntity' => 'required', 'caseChartNo' => 'required', 'insuranceID' => 'required']);

        // if the validator fails, redirect back to the form
        if ($validator->fails()) {
            return \Response::json(array('errors' => true, 'message' => $validator->messages()));
        } else {
            if (m_case::where(['caseChartNo' => Input::get('caseChartNo')])->exists()) {
                $caseid = m_case::where(['caseChartNo' => Input::get('caseChartNo')])->pluck('caseID')[0];
                if ($caseid <= 0) {
                    return \Response::json(array('errors' => true, 'message' => 'Invalid Case Chart Number.'));
                }

                if (m_policy::where(['caseID' => $caseid, 'policyEntity' => Input::get('policyEntity')])->exists()) {
                    // Grab all the input passed in
                    $data = Input::all();

                    $policy = m_policy::where('caseID', $caseid)->where('policyEntity', Input::get('policyEntity'));
                    $policy->update($data);
                    Common::WriteLog('Policies','Policy Updated. Case ID:' . $caseid . 'Policy Entity:'.Input::get('policyEntity'),0,'Edit',$caseid);
                    return \Response::json(array('errors' => false, 'message' => 'Policy Updated Successfully!'));
                } else {  //create
                    $policyID = m_policy::create($request->all())->id;
                    Common::WriteLog('Policies','Policy Created. Policy ID:' . $policyID,0,'Create',$policyID);
                    return \Response::json(array('errors' => false, 'message' => 'Policy Created Successfully!', 'PolicyID' => $policyID));
                }
            } else {
                return \Response::json(array('errors' => true, 'message' => 'Invalid Case Chart Number.'));
            }
        }
    }

    /*public function createPolicy(Request $request){
        $validator = Validator::make(Input::all(), ['policyEntity'=>'required','caseID'=>'required','insuranceID'=>'required']);

        // if the validator fails, redirect back to the form
        if ($validator->fails()) {
            return \Response::json(array('errors' => true, 'message' => $validator->messages()));
        }
        else{
            if (m_policy::where(['caseID'=>Input::get('caseID'),'policyEntity'=>Input::get('policyEntity')])->exists()) {
                // Grab all the input passed in
                $data = Input::all();

                $policy = m_policy::where('caseID',Input::get('caseID'))->where('policyEntity',Input::get('policyEntity'));
                $policy->update($data);
                return \Response::json(array('errors' => false, 'message' => 'Policy Updated Successfully!'));
            }else{  //create
                $policyID = m_policy::create($request->all())->id;
                return \Response::json(array('errors' => false, 'message' => 'Policy Created Successfully!', 'PolicyID' => $policyID));
            }
        }
    }*/
}
