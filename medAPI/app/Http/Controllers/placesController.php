<?php

namespace App\Http\Controllers;

use App\m_placeofservice;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use Response;
use Illuminate\Support\Facades\Input;
use App\Classes\Common;

class placesController extends Controller
{
    //
    public function createPlace(Request $request){
        $validator = Validator::make(Input::all(), ['placeCode'=>'required','placeName'=>'required']);

        // if the validator fails, redirect back to the form
        if ($validator->fails()) {
            return \Response::json(array('errors' => true, 'message' => $validator->messages()));
        }
        else{
            $placeID = m_placeofservice::create($request->all())->id;
            Common::WriteLog('Places','Place Created. Place ID:' . $placeID,0,'Create',$placeID);
            return \Response::json(array('errors' => false, 'message' => 'Place Created Successfully!', 'relationID' => $placeID));
        }
    }

    public function loadPlace(Request $request)
    {
        $validator = Validator::make(Input::all(), ['placeCode'=>'required']);

        // if the validator fails, redirect back to the form
        if ($validator->fails()) {
            //return \Response::json(array('errors' => true, 'message' => $validator->messages()));
            $places = m_placeofservice::get();
            return $places;
        }
        else{
            $places = m_placeofservice::where(['placeCode'=>$request['placeCode']])->get();
            //Common::WriteLog('Places','Place Loaded. Place Code:' . $request['placeCode'],0);
            return $places;
        }
    }
}
