<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Custom Routes - Ashish
//Practice
Route::get('practice','practiceController@list_all');
Route::post('practice/create','practiceController@createPractice');


//Patients
//Route::get('patients','patientsController@_list');
Route::post('patients','patientsController@list_all');
Route::post('patients/load','patientsController@loadPatient');
Route::post('patients/create','patientsController@createPatient');
Route::post('patients/chartnumber','patientsController@chartNo');
Route::post('patients/save','patientsController@savePatient');


//Cases
Route::post('cases','casesController@list_all');
Route::post('cases/create','casesController@createCase');
Route::post('cases/load','casesController@loadCase');
Route::post('cases/save','casesController@saveCase');
Route::post('cases/casechart','casesController@caseChart');


//Claims
Route::post('claims','claimsController@loadClaim');
Route::post('claims/create','claimsController@createClaim');
Route::post('claims/save','claimsController@saveClaim');
Route::post('claims/ledger','claimsController@listClaim');
Route::post('claims/Depositledger','claimsController@listDepositClaim');
Route::post('claims/patientledger','claimsController@listPatientClaim');
Route::post('claims/allledgers','claimsController@listAllClaims');
Route::post('claims/incrementclaimnumber','claimsController@incrementClaimNumber');
Route::post('claims/remove','claimsController@removeClaim');
Route::post('claims/undoremove','claimsController@undoRemoveClaim');
Route::post('claims/listbydeposit','claimsController@listByDeposit');
Route::post('claims/ledgerpatientonly','claimsController@ledgerPatientOnly');
Route::post('claims/ledgerinsuranceonly','claimsController@ledgerInsuranceOnly');
Route::post('claims/ledgerdetail','claimsController@listClaimByNumber');


//generate
Route::post('generate','claimsController@generate');
Route::post('generateall','claimsController@generate_all');
Route::post('generatePapers','claimsController@generatePaper');


//placeOfService
Route::post('service_locations','claimsController@loadLocations');

//Deposits
Route::post('deposits','depositsController@loadDeposits');
Route::post('deposits/create','depositsController@createDeposit');
Route::post('deposits/adjust','depositsController@adjustDeposit');
Route::post('deposits/adjustPat','depositsController@adjustPatDeposit');
Route::post('deposits/balance','depositsController@findBalance');
Route::post('deposits/details','depositsController@depositDetails');
Route::post('deposits/undoadjust','depositsController@reverseAdjustDeposit');


//Physicians
Route::post('physicians','physiciansController@list_all');
Route::post('physicians/create','physiciansController@createPhysician');
Route::post('physicians/load','physiciansController@loadPhysician');


//Policies
Route::post('policies/load','policiesController@loadPolicy');
Route::post('policies/create','policiesController@createPolicy');


//Insurances
Route::post('insurances','insurancesController@list_all');
Route::post('insurances/create','insurancesController@createInsurance');
Route::post('insurances/load','insurancesController@loadInsurance');
Route::post('insurances/listpatients','insurancesController@listPatients');


//Codes
Route::post('codes/cpt','codesController@list_cpt');
Route::post('codes/icd','codesController@list_icd');
Route::post('codes/cptcreate','codesController@cptcreate');
Route::post('codes/icdcreate','codesController@icdcreate');
Route::post('codes/cptsave','codesController@cptsave');
Route::post('codes/icdsave','codesController@icdsave');


//Relationships
Route::post('relations/create','relationshipsController@createRelation');
Route::post('relations/load','relationshipsController@loadRelation');
Route::post('relations/save','relationshipsController@saveRelation');


//Place of Services
Route::post('places/create','placesController@createPlace');
Route::post('places/load','placesController@loadPlace');


//Service Locations
Route::post('serviceloc/create','serviceLocationsController@create');
Route::post('serviceloc/load','serviceLocationsController@load');
Route::post('serviceloc/save','serviceLocationsController@save');


Route::post('reports/patients/summary','patientsController@patientsSummaryReport');
Route::post('reports/patients/contactlist','patientsController@patientsContactListReport');
Route::post('reports/patients/transactions','patientsController@patientsTransactionReport');
Route::post('reports/patients/balancesummary','patientsController@patientsBalanceSummaryReport');

Route::post('reports/claims/summary','claimsController@chargesSummaryReport');
Route::post('reports/claims/settledCharges','claimsController@settledChargesReport');

Route::post('reports/claims/araging','claimsController@arAgingReport');
Route::post('reports/claims/aragingdetail','claimsController@arAgingDetailReport');
Route::post('reports/claims/unpaidClaims','claimsController@unpaidClaimsReport');
Route::post('reports/insurances/araging','insurancesController@arAgingReport');
Route::post('reports/insurances/aragingdetail','insurancesController@arAgingDetailReport');
Route::post('reports/patients/araging','patientsController@arAgingReport');
Route::post('reports/patients/aragingdetail','patientsController@arAgingDetailReport');
Route::post('reports/claims/ledger','claimsController@ledgerSummaryReport');
Route::post('reports/claims/charges','claimsController@chargesReport');



/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    //
});
