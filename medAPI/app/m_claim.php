<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class m_claim extends Model
{
    //
    protected $fillable = array('caseID','appID','authNo',	'fromDt',	'toDt',	'proced',	'mod1',	'units',	'charge',	'total',	'diag1',	'diag2',	'diag3',	'diag4',	'applied',	'notes','placeOfService',	'createdBy',	'isDeleted', 'isPosted','isSent','allowed','paid','adjustment','claimBalance','claimBalancePat','reason','status','copay','deductible','coins','primaryCarePhy','serviceLocID','claimStatus','insuranceID','ClaimNumber');

    public function _case(){
       return $this->belongsTo('App\m_case','caseID','caseID');
    }

    public function cases(){
        return $this->belongsTo('App\m_case','caseID','caseID')->select(array('claimID','ClaimNumber'));
    }
}
