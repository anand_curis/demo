<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class m_practice extends Model
{
    protected $fillable = array('practiceName','speciality','dob','degree','ssn','pType','EIN','address','phoneHome','phoneWork','phoneMobile','pager','dept','fax','encounter','pUser','npi','notes','isActive');
}
