Reference : https://github.com/modulr/api-laravel-passport


htdocs/oauth>

htdocs/oauth> composer install

htdocs/oauth> php artisan key:generate


htdocs/oauth>

Configure .env file, edit file with next command

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=database
DB_USERNAME=user
DB_PASSWORD=secret


htdocs/oauth> php artisan migrate

Create client

htdocs/oauth> php artisan passport:install


Rest API:

http://localhost:8000/api/auth/signup

{
  "name": "user1",
  "email": "user1@gmail.com",
  "password": "u!23",
  "password_confirmation": "u!23"
}

Authentication
POST /auth/login
GET /auth/logout
POST /auth/signup
GET /auth/signup/activate/{token}
GET /auth/user
Password Reset
POST /password/create
GET /password/find/{token}
POST /password/reset



